import sys
import os
import numpy as np

PATH_CURRENT = os.path.dirname(os.path.realpath(__file__))
PATH_FILES = os.path.abspath(os.path.join(PATH_CURRENT, '../../testing/images'))
PATH_TESTS = os.path.abspath(os.path.join(PATH_CURRENT, '../../testing/tests'))

sys.path.append(os.path.join(os.path.dirname(PATH_CURRENT), 'src'))
import libbioimage.libbioimage as bim

from utils import ensure_local_file

class TestLibBioImageFilters():

    @classmethod
    def setup_class(cls):
        cls.filename_label_i32 = os.path.join(PATH_FILES, 'label_i32.tif')
        cls.filename_label_i32_grown100 = os.path.join(PATH_FILES, 'label_i32_grown100.tif')
        cls.filename_label_i32_grown5000 = os.path.join(PATH_FILES, 'label_i32_grown5000.tif')
        ensure_local_file(cls.filename_label_i32)
        ensure_local_file(cls.filename_label_i32_grown100)
        ensure_local_file(cls.filename_label_i32_grown5000)

        cls.filename_out = os.path.join(PATH_TESTS, 'out.tif')
        cls.filename_out_prefix = os.path.join(PATH_TESTS, 'out_')
        cls.bim_cache = bim.Cache(size=1)

    @classmethod
    def teardown_class(cls):
        if cls.bim_cache is not None:
            cls.bim_cache.free()

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    def test_resizing (self):
        x = np.empty((4,200,400), dtype=np.float32)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)

        y = bim.resize(x, (100,200))
        assert (y.shape == (4,100,200))
        assert (y.dtype == np.float32)

    def test_resampling (self):
        x = np.empty((4,200,400), dtype=np.float32)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)

        y = bim.resample(x, (100,200))
        assert (y.shape == (4,100,200))
        assert (y.dtype == np.float32)

    def test_resampling_hyperspectral (self):
        x = np.ones((66,10,30), dtype=np.float32)

        y = bim.resample(x, (5,20))
        assert (y.shape == (66,5,20))
        assert (y.dtype == np.float32)

    def test_regiongrow_sz100 (self):
        x = bim.read(self.filename_label_i32)
        assert (x.shape == (1,550,550))
        assert (x.dtype == np.int32)

        y_expected100 = bim.read(self.filename_label_i32_grown100)
        assert (y_expected100.shape == (1,550,550))
        assert (y_expected100.dtype == np.int32)

        y100 = bim.filter_regiongrow(x, region_size = 100, minimum_convexity = 0.95, boundary_probability=None)
        assert (y100.shape == y_expected100.shape)
        assert (y100.dtype == y_expected100.dtype)
        assert (np.array_equal(y100, y_expected100))

    def test_regiongrow_sz5000 (self):
        x = bim.read(self.filename_label_i32)
        assert (x.shape == (1,550,550))
        assert (x.dtype == np.int32)

        y_expected5000 = bim.read(self.filename_label_i32_grown5000)
        assert (y_expected5000.shape == (1,550,550))
        assert (y_expected5000.dtype == np.int32)

        y5000 = bim.filter_regiongrow(x, region_size = 5000, minimum_convexity = 0.95, boundary_probability=None)
        assert (y5000.shape == y_expected5000.shape)
        assert (y5000.dtype == y_expected5000.dtype)
        assert (np.array_equal(y5000, y_expected5000))

    # def test_gaussian (self):
    #     images, labels = self.gen[0]
    #     assert (np.unique(labels).shape[0] > 1)

    # def test_mrs (self):
    #     images, labels = self.gen[8]
    #     assert (images.shape[0] == 20)
    #     assert (labels.shape[0] == 20)
