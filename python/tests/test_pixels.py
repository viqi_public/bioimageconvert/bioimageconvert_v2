import sys
import os
from pathlib import Path
import numpy as np
import skimage.io

PATH_CURRENT = os.path.dirname(os.path.realpath(__file__))
PATH_FILES = os.path.abspath(os.path.join(PATH_CURRENT, '../../testing/images'))
PATH_TESTS = os.path.abspath(os.path.join(PATH_CURRENT, '../../testing/tests'))

sys.path.append(os.path.join(os.path.dirname(PATH_CURRENT), 'src'))
import libbioimage.libbioimage as bim

from utils import ensure_local_file

def _create_io_target_filename(filename_in):
    return os.path.join(PATH_FILES, '%s_io_test_target.tiff'%Path(filename_in).stem)

def _ensure_local_pair(filename):
    ensure_local_file(filename)
    ensure_local_file(_create_io_target_filename(filename))

def _shift_image_dims(X, Y):
    # X is in proper order, read by bim.read()
    # Y is read by skimage.io.imread() and needs to be shuffled
    # assume for now that it's maximum 3 dims: [C, Y, X]
    if Y.ndim<X.ndim:
        Y = np.expand_dims(Y, axis=2)
    return np.transpose(Y, (2,0,1))

class TestLibBioImagePixels():

    @classmethod
    def setup_class(cls):
        cls.filename_hca_0 = os.path.join(PATH_FILES, 'MD_HTD_OLD_w1.TIF')
        cls.filename_hca_1 = os.path.join(PATH_FILES, 'MD_HTD_XML_w1.TIF')
        cls.filename_hca_2 = os.path.join(PATH_FILES, 'AS_09125_050117050001_P24f00d0.DIB')
        cls.filename_hca_3 = os.path.join(PATH_FILES, 'localhost210727110002_B06f01d0.C01')
        cls.filename_hca_4 = os.path.join(PATH_FILES, 'A3_01_1_2_Phase_Contrast_001.tif')
        _ensure_local_pair(cls.filename_hca_0)
        _ensure_local_pair(cls.filename_hca_1)
        _ensure_local_pair(cls.filename_hca_2)
        _ensure_local_pair(cls.filename_hca_3)
        _ensure_local_pair(cls.filename_hca_4)

        cls.filename_cellomics = os.path.join(PATH_FILES, 'HTS-CELLOMICS_210402120001_K46f00d0.TIFF')
        cls.filename_cellomics_val = os.path.join(PATH_FILES, 'HTS-CELLOMICS_210402120001_K46f00d0.TIFF.bimr')
        ensure_local_file(cls.filename_cellomics)
        ensure_local_file(cls.filename_cellomics_val)


        cls.filename_codex_dapi_520 = os.path.join(PATH_FILES, 'codex_dapi_520.png')
        cls.filename_codex_dapi_520_val = os.path.join(PATH_FILES, 'codex_dapi_520_RGB_H_DAB.png')
        ensure_local_file(cls.filename_codex_dapi_520)
        ensure_local_file(cls.filename_codex_dapi_520_val)

        cls.filename_codex_dapi_520_570 = os.path.join(PATH_FILES, 'codex_dapi_520_570.png')
        cls.filename_codex_dapi_520_570_val = os.path.join(PATH_FILES, 'codex_dapi_520_570_RGB_H_DAB_E_1.5.png')
        ensure_local_file(cls.filename_codex_dapi_520_570)
        ensure_local_file(cls.filename_codex_dapi_520_570_val)

        cls.filename_codex_16bit = os.path.join(PATH_FILES, 'codex_16bit.tif')
        cls.filename_codex_16bit_val = os.path.join(PATH_FILES, 'codex_16bit_RGB_H_E_1.5.tif')
        ensure_local_file(cls.filename_codex_16bit)
        ensure_local_file(cls.filename_codex_16bit_val)


        # cls.filename_3d_0 = os.path.join(PATH_FILES, '161pkcvampz1Live2-17-2004_11-57-21_AM.tif')
        # cls.filename_3d_1 = os.path.join(PATH_FILES, 'combinedsubtractions.lsm')
        # cls.filename_3d_2 = os.path.join(PATH_FILES, 'Mouse_stomach_20x_ROI_3chZTiles(WF).czi')
        # cls.filename_3d_3 = os.path.join(PATH_FILES, 'old_cells_02.nd2')

        # # whole slide:
        # cls.filename_whole_0 = os.path.join(PATH_FILES, '17.czi')
        # cls.filename_whole_1 = os.path.join(PATH_FILES, 'CMU-1.svs')
        # cls.filename_whole_2 = os.path.join(PATH_FILES, 'CMU-1.ndpi')
        # cls.filename_whole_3 = os.path.join(PATH_FILES, '2017SM09941_3_EVG.tiff')
        # cls.filename_whole_4 = os.path.join(PATH_FILES, 'HandEcompressed_Scan1.qptiff')
        # cls.filename_whole_5 = os.path.join(PATH_FILES, 'LuCa-7color_Scan1.qptiff')

        # # VQI:
        # cls.filename_vqi_0 = os.path.join(PATH_FILES, 'vqi_objects_CYX.h5')

        # cls.filename_16b_2ch_3D = os.path.join(PATH_FILES, '161pkcvampz1Live2-17-2004_11-57-21_AM.tif')
        # cls.filename_16b_2ch_page6 = os.path.join(PATH_FILES, '161pkcvampz1Live2-17-2004_11-57-21_AM_page6.bimr')
        # cls.filename_label_i32 = os.path.join(PATH_FILES, 'label_i32.tif')
        # cls.filename_label_i32_grown100 = os.path.join(PATH_FILES, 'label_i32_grown100.tif')
        # cls.filename_label_i32_grown5000 = os.path.join(PATH_FILES, 'label_i32_grown5000.tif')

        # _ensure_local_file('161pkcvampz1Live2-17-2004_11-57-21_AM.tif')
        # _ensure_local_file('combinedsubtractions.lsm')
        # _ensure_local_file('Mouse_stomach_20x_ROI_3chZTiles(WF).czi')
        # _ensure_local_file('old_cells_02.nd2')

        # _ensure_local_file('17.czi')
        # _ensure_local_file('CMU-1.svs')
        # _ensure_local_file('CMU-1.ndpi')
        # _ensure_local_file('2017SM09941_3_EVG.tiff')
        # _ensure_local_file('HandEcompressed_Scan1.qptiff')
        # _ensure_local_file('LuCa-7color_Scan1.qptiff')

        # _ensure_local_file('vqi_objects_CYX.h5')

        # unmixing
        cls.command_unmix = '-unmix 1.000491544852187,0.002879363199948744,0.04368511314014342,0.022317307697762946,-0.16941721507234714,0.00032799076554887826,1.0052610998282334,-0.10120540485868176,-0.02733443773671807,-0.11304620894980533,8.580832860191718e-05,-0.012560988755842377,1.0111489766359532,0.00911622454479735,-0.02957493705196368,0.00023501452941464604,0.002453131970639706,0.025502667001285367,1.0227920338487106,-0.08100076096370544,-0.0030080751763112996,-0.03139893266808622,-0.3264221141030526,-0.29172728770796436,1.036771551634045'
        cls.filename_mixed_16bit = os.path.join(PATH_FILES, 'mixed.16bit.tif')
        cls.filename_unmixed_16bit = os.path.join(PATH_FILES, 'unmixed.16bit.tif')
        cls.filename_unmixed_16bit_round = os.path.join(PATH_FILES, 'unmixed.16bit.round.tif')
        cls.filename_unmixed_16bit_int16 = os.path.join(PATH_FILES, 'unmixed.16bit.i16.tif')
        cls.filename_unmixed_16bit_float32 = os.path.join(PATH_FILES, 'unmixed.16bit.f32.tif')
        ensure_local_file(cls.filename_mixed_16bit)
        ensure_local_file(cls.filename_unmixed_16bit)
        ensure_local_file(cls.filename_unmixed_16bit_round)
        ensure_local_file(cls.filename_unmixed_16bit_int16)
        ensure_local_file(cls.filename_unmixed_16bit_float32)

        cls.filename_out = os.path.join(PATH_TESTS, 'temp.tif')
        cls.bim_cache = bim.Cache(size=1)


    @classmethod
    def teardown_class(cls):
        if cls.bim_cache is not None:
            cls.bim_cache.free()

    #-----------------------------------------------------------------
    # READ
    #
    # Compare with TIFF files previously written
    #-----------------------------------------------------------------

    def test_read_hca_0 (self):
        X = bim.read(self.filename_hca_0)
        Y = skimage.io.imread(_create_io_target_filename(self.filename_hca_0))
        Y = _shift_image_dims(X, Y)
        assert(np.array_equal(X, Y))

    def test_read_hca_1 (self):
        X = bim.read(self.filename_hca_1)
        Y = skimage.io.imread(_create_io_target_filename(self.filename_hca_1))
        Y = _shift_image_dims(X, Y)
        assert(np.array_equal(X, Y))

    def test_read_hca_2 (self):
        X = bim.read(self.filename_hca_2)
        Y = skimage.io.imread(_create_io_target_filename(self.filename_hca_2))
        Y = _shift_image_dims(X, Y)
        assert(np.array_equal(X, Y))

    def test_read_hca_3 (self):
        X = bim.read(self.filename_hca_3)
        Y = skimage.io.imread(_create_io_target_filename(self.filename_hca_3))
        Y = _shift_image_dims(X, Y)
        assert(np.array_equal(X, Y))

    def test_read_hca_4 (self):
        X = bim.read(self.filename_hca_4)
        Y = skimage.io.imread(_create_io_target_filename(self.filename_hca_4))
        Y = _shift_image_dims(X, Y)
        assert(np.array_equal(X, Y))

    def test_read_cellomics_tiff (self):
        X = bim.read(self.filename_cellomics)
        Y = bim.read(self.filename_cellomics_val)
        assert(np.array_equal(X, Y))

    #-----------------------------------------------------------------
    # Writing
    # 1. Read TIFF file
    # 2. write to a temp file
    # 3. Read back in from the temp file
    # 4. Compate to make sure it's the same
    #-----------------------------------------------------------------

    def test_write_hca_0 (self):
        X = bim.read(_create_io_target_filename(self.filename_hca_0))
        bim.write(X, self.filename_out)
        Y = bim.read(self.filename_out)
        assert(np.array_equal(X, Y))

    def test_write_hca_1 (self):
        X = bim.read(_create_io_target_filename(self.filename_hca_1))
        bim.write(X, self.filename_out)
        Y = bim.read(self.filename_out)
        assert(np.array_equal(X, Y))

    def test_write_hca_2 (self):
        X = bim.read(_create_io_target_filename(self.filename_hca_2))
        bim.write(X, self.filename_out)
        Y = bim.read(self.filename_out)
        assert(np.array_equal(X, Y))

    def test_write_hca_3 (self):
        X = bim.read(_create_io_target_filename(self.filename_hca_3))
        bim.write(X, self.filename_out)
        Y = bim.read(self.filename_out)
        assert(np.array_equal(X, Y))

    def test_write_hca_4 (self):
        X = bim.read(_create_io_target_filename(self.filename_hca_4))
        bim.write(X, self.filename_out)
        Y = bim.read(self.filename_out)
        assert(np.array_equal(X, Y))

    #-----------------------------------------------------------------
    # Filters
    #-----------------------------------------------------------------

    # mixing (convolving) fluorescent singal
    
    def test_mixing_brightfield_8bit_2ch (self):
        img = bim.read(self.filename_codex_dapi_520)
        Y = bim.read(self.filename_codex_dapi_520_val)
        X = bim.process(img, pipeline='-mixbf 0:h;1:dab;k:0.0')
        assert(np.array_equal(X, Y))

    def test_mixing_brightfield_8bit_3ch (self):
        img = bim.read(self.filename_codex_dapi_520_570)
        Y = bim.read(self.filename_codex_dapi_520_570_val)
        X = bim.process(img, pipeline='-mixbf 0:h;1:dab;2:e;k:1.5')
        assert(np.array_equal(X, Y))

    def test_mixing_brightfield_16bit_2ch (self):
        img = bim.read(self.filename_codex_16bit)
        Y = bim.read(self.filename_codex_16bit_val)
        X = bim.process(img, pipeline='-mixbf 0:e;1:h;k:1.5')
        assert(np.array_equal(X, Y))

    # unmixing fluorescent singal

    def test_unmixing_16bit_5ch (self):
        img = bim.read(self.filename_mixed_16bit)
        Y = bim.read(self.filename_unmixed_16bit)
        pipeline = self.command_unmix
        X = bim.process(img, pipeline=pipeline)
        assert(np.array_equal(X, Y))

    def test_unmixing_16bit_5ch_round (self):
        img = bim.read(self.filename_mixed_16bit)
        Y = bim.read(self.filename_unmixed_16bit_round)
        pipeline = '%s;;;round'%self.command_unmix
        X = bim.process(img, pipeline=pipeline)
        assert(np.array_equal(X, Y))

    def test_unmixing_16bit_5ch_int16 (self):
        img = bim.read(self.filename_mixed_16bit)
        Y = bim.read(self.filename_unmixed_16bit_int16)
        pipeline = '%s;;int16'%self.command_unmix
        X = bim.process(img, pipeline=pipeline)
        assert(np.array_equal(X, Y))

    def test_unmixing_16bit_5ch_float32 (self):
        img = bim.read(self.filename_mixed_16bit)
        Y = bim.read(self.filename_unmixed_16bit_float32)
        pipeline = '%s;;float32'%self.command_unmix
        X = bim.process(img, pipeline=pipeline)
        assert(np.array_equal(X, Y))
