import sys
import os
import numpy as np

PATH_CURRENT = os.path.dirname(os.path.realpath(__file__))
PATH_FILES = os.path.abspath(os.path.join(PATH_CURRENT, '../../testing/images'))
PATH_TESTS = os.path.abspath(os.path.join(PATH_CURRENT, '../../testing/tests'))

sys.path.append(os.path.join(os.path.dirname(PATH_CURRENT), 'src'))
import libbioimage.libbioimage as bim

from utils import ensure_local_file
import pytest

class TestLibBioImageRead():

    @classmethod
    def setup_class(cls):
        cls.filename_16b_2ch_3D = os.path.join(PATH_FILES, '161pkcvampz1Live2-17-2004_11-57-21_AM.tif')
        cls.filename_16b_2ch_page6 = os.path.join(PATH_FILES, '161pkcvampz1Live2-17-2004_11-57-21_AM_page6.bimr')
        cls.filename_tiff_pyramid = os.path.join(PATH_FILES, 'monument_imgcnv_subdirs.tif')
        ensure_local_file(cls.filename_16b_2ch_3D)
        ensure_local_file(cls.filename_16b_2ch_page6)
        ensure_local_file(cls.filename_tiff_pyramid)

        cls.filename_out = os.path.join(PATH_TESTS, 'out.tif')
        cls.filename_out_prefix = os.path.join(PATH_TESTS, 'out_')
        cls.bim_cache = bim.Cache(size=1)

    @classmethod
    def teardown_class(cls):
        if cls.bim_cache is not None:
            cls.bim_cache.free()

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    def test_reading_meta (self):
        meta = bim.meta(self.filename_16b_2ch_3D, bim_cache=self.bim_cache)
        assert (meta.get('format') == 'FLUOVIEW')
        assert (meta.get('image_mode') == 'multichannel')
        assert (meta.get('image_num_c') == 2)
        assert (meta.get('image_num_t') == 1)
        assert (meta.get('image_num_x') == 512)
        assert (meta.get('image_num_y') == 512)
        assert (meta.get('image_num_z') == 13)
        assert (meta.get('image_pixel_depth') == 16)
        assert (meta.get('image_pixel_format') == 'unsigned integer')
        assert (meta.get('pixel_resolution_x') == 0.20716)
        assert (meta.get('pixel_resolution_y') == 0.20716)
        assert (meta.get('pixel_resolution_z') == 1)
        assert (meta.get('pixel_resolution_unit_x') == 'microns')
        assert (meta.get('pixel_resolution_unit_y') == 'microns')
        assert (meta.get('pixel_resolution_unit_z') == 'microns')
        assert (meta.get('objectives/objective:0/magnification') == 40)
        assert (meta.get('objectives/objective:0/name') == 'UPLAPO 40XO')
        assert (meta.get('document/acquisition_date') == '2004-02-17T11:54:50')
        assert (meta.get('channels/channel:0/color') == '0.00,1.00,0.00')
        assert (meta.get('channels/channel:0/modality') == 'Fluorescence')
        assert (meta.get('channels/channel:0/name') == 'FITC')
        assert (meta.get('channels/channel:1/color') == '1.00,0.00,0.00')
        assert (meta.get('channels/channel:1/modality') == 'Fluorescence')
        assert (meta.get('channels/channel:1/name') == 'Cy3')

    def test_reading_pixels (self):
        x = bim.read(self.filename_16b_2ch_3D, pipeline='-page 5', bim_cache=self.bim_cache)
        assert (x.shape == (2,512,512))
        assert (x.dtype == np.uint16)

        y = bim.read(self.filename_16b_2ch_page6)
        assert (y.shape == x.shape)
        assert (y.dtype == x.dtype)
        assert (np.array_equal(x, y))

    def test_reading_tiles (self):
        x = bim.read(self.filename_tiff_pyramid, pipeline='-tile 512,0,0,1', bim_cache=self.bim_cache)
        assert (x.shape == (3,512,512))
        assert (x.dtype == np.uint8)

        x = bim.read(self.filename_tiff_pyramid, pipeline='-tile 512,1,0,1', bim_cache=self.bim_cache)
        assert (x.shape == (3,512,512))
        assert (x.dtype == np.uint8)

    def test_reading_rois (self):
        x = bim.read(self.filename_tiff_pyramid, pipeline='-tile-roi 0,0,255,255,3', bim_cache=self.bim_cache)
        assert (x.shape == (3,256,256))
        assert (x.dtype == np.uint8)

        x = bim.read(self.filename_tiff_pyramid, pipeline='-tile-roi 256,0,511,255,3', bim_cache=self.bim_cache)
        assert (x.shape == (3,256,256))
        assert (x.dtype == np.uint8)

    def test_reading_nd (self):
        x = bim.read_nd(self.filename_16b_2ch_3D, ranges={'z': None}, bim_cache=self.bim_cache)
        assert (x.shape == (13, 2, 512, 512))
        assert (x.dims == ('z', 'c', 'y', 'x'))

        meta = x.attrs
        assert (meta.get('format') == 'FLUOVIEW')
        assert (meta.get('image_mode') == 'multichannel')
        assert (meta.get('image_num_c') == 2)
        assert (meta.get('image_num_t') == 1)
        assert (meta.get('image_num_x') == 512)
        assert (meta.get('image_num_y') == 512)
        assert (meta.get('image_num_z') == 13)
        assert (meta.get('image_pixel_depth') == 16)
        assert (meta.get('image_pixel_format') == 'unsigned integer')
        assert (meta.get('pixel_resolution_x') == 0.20716)
        assert (meta.get('pixel_resolution_y') == 0.20716)
        assert (meta.get('pixel_resolution_z') == 1)
        assert (meta.get('pixel_resolution_unit_x') == 'microns')
        assert (meta.get('pixel_resolution_unit_y') == 'microns')
        assert (meta.get('pixel_resolution_unit_z') == 'microns')
        assert (meta.get('objectives/objective:0/magnification') == 40)
        assert (meta.get('objectives/objective:0/name') == 'UPLAPO 40XO')
        assert (meta.get('document/acquisition_date') == '2004-02-17T11:54:50')
        assert (meta.get('channels/channel:0/color') == '0.00,1.00,0.00')
        assert (meta.get('channels/channel:0/modality') == 'Fluorescence')
        assert (meta.get('channels/channel:0/name') == 'FITC')
        assert (meta.get('channels/channel:1/color') == '1.00,0.00,0.00')
        assert (meta.get('channels/channel:1/modality') == 'Fluorescence')
        assert (meta.get('channels/channel:1/name') == 'Cy3')

        plane = x.isel(z=5)
        y = bim.read(self.filename_16b_2ch_page6)
        assert (y.shape == plane.shape)
        assert (y.dtype == plane.dtype)
        assert (np.array_equal(plane, y))

    def test_read_region (self):
        # read using bbox
        x_roi = bim.read_region(self.filename_tiff_pyramid, scale=1.0, roi=[0, 0, 255, 255], bim_cache=self.bim_cache)
        x_bbox = bim.read_region(self.filename_tiff_pyramid, scale=1.0, bbox=[0, 0, 256, 256], bim_cache=self.bim_cache)

        assert (x_roi.shape == (3,256,256))
        assert (x_roi.dtype == np.uint8)

        assert (x_bbox.shape == (3,256,256))
        assert (x_bbox.dtype == np.uint8)

        assert(np.array_equal(x_roi, x_bbox))

        with pytest.raises(ValueError):
            bim.read_region(self.filename_tiff_pyramid, scale=1.0, bbox=[0, 0, 256, 256], roi=[0, 0, 255, 255], bim_cache=self.bim_cache)

        with pytest.raises(ValueError):
            bim.read_region(self.filename_tiff_pyramid, scale=1.0, level=1, bbox=[0, 0, 256, 256], bim_cache=self.bim_cache)

        with pytest.raises(ValueError):
            bim.read_region(self.filename_tiff_pyramid, bbox=[0, 0, 256, 256], bim_cache=self.bim_cache)

        with pytest.raises(ValueError):
            bim.read_region(self.filename_tiff_pyramid, scale=1.0, bim_cache=self.bim_cache)

    def test_read_tile (self):
        # read using bbox
        x = bim.read_tile(self.filename_tiff_pyramid, level=0, tile_id=(0,0), tile_size=256, bim_cache=self.bim_cache)
        assert (x.shape == (3,256,256))
        assert (x.dtype == np.uint8)

        with pytest.raises(ValueError):
            bim.read_tile(self.filename_tiff_pyramid, tile_id=(0,0), tile_size=256, bim_cache=self.bim_cache)

        with pytest.raises(ValueError):
            bim.read_tile(self.filename_tiff_pyramid, level=0, tile_size=256, bim_cache=self.bim_cache)

        with pytest.raises(ValueError):
            bim.read_tile(self.filename_tiff_pyramid, level=0, tile_id=(0,0), bim_cache=self.bim_cache)

        x1 = bim.read_tile(self.filename_tiff_pyramid, level=1, tile_id=(0,0), tile_size=256, bim_cache=self.bim_cache)
        x2 = bim.read(self.filename_tiff_pyramid, pipeline='-tile 256,0,0,1', bim_cache=self.bim_cache)

        assert(np.array_equal(x1, x2))