import sys
import os
import numpy as np

PATH_CURRENT = os.path.dirname(os.path.realpath(__file__))
PATH_FILES = os.path.abspath(os.path.join(PATH_CURRENT, '../../testing/images'))
PATH_TESTS = os.path.abspath(os.path.join(PATH_CURRENT, '../../testing/tests'))

sys.path.append(os.path.join(os.path.dirname(PATH_CURRENT), 'src'))
import libbioimage.libbioimage as bim

# from utils import ensure_local_file

class TestLibBioImageWrite():

    @classmethod
    def setup_class(cls):
        # cls.filename_16b_2ch_3D = os.path.join(PATH_FILES, '161pkcvampz1Live2-17-2004_11-57-21_AM.tif')
        # ensure_local_file(cls.filename_16b_2ch_3D)

        cls.filename_out = os.path.join(PATH_TESTS, 'out.tif')
        cls.filename_out_prefix = os.path.join(PATH_TESTS, 'out_')
        cls.bim_cache = bim.Cache(size=1)

    @classmethod
    def teardown_class(cls):
        if cls.bim_cache is not None:
            cls.bim_cache.free()

    #-----------------------------------------------------------------
    # uint
    #-----------------------------------------------------------------

    def test_writing_pixels_uint8 (self):
        x = np.empty((4,200,400), dtype=np.uint8)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        filename_out = '%suint8.tif'%(self.filename_out_prefix)
        bim.write(x, filename_out, 'tiff')

        y = bim.read(filename_out)
        assert (y.shape == x.shape)
        assert (y.dtype == x.dtype)
        assert (np.array_equal(x, y))

    def test_writing_pixels_uint16 (self):
        x = np.empty((4,200,400), dtype=np.uint16)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        filename_out = '%suint16.tif'%(self.filename_out_prefix)
        bim.write(x, filename_out, 'tiff')

        y = bim.read(filename_out)
        assert (y.shape == x.shape)
        assert (y.dtype == x.dtype)
        assert (np.array_equal(x, y))

    def test_writing_pixels_uint32 (self):
        x = np.empty((4,200,400), dtype=np.uint32)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        filename_out = '%suint32.tif'%(self.filename_out_prefix)
        bim.write(x, filename_out, 'tiff')

        y = bim.read(filename_out)
        assert (y.shape == x.shape)
        assert (y.dtype == x.dtype)
        assert (np.array_equal(x, y))

    def test_writing_pixels_uint64 (self):
        x = np.empty((4,200,400), dtype=np.uint64)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        filename_out = '%suint64.tif'%(self.filename_out_prefix)
        bim.write(x, filename_out, 'tiff')

        y = bim.read(filename_out)
        assert (y.shape == x.shape)
        assert (y.dtype == x.dtype)
        assert (np.array_equal(x, y))

    #-----------------------------------------------------------------
    # int
    #-----------------------------------------------------------------

    def test_writing_pixels_int8 (self):
        x = np.empty((4,200,400), dtype=np.int8)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        filename_out = '%sint8.tif'%(self.filename_out_prefix)
        bim.write(x, filename_out, 'tiff')

        y = bim.read(filename_out)
        assert (y.shape == x.shape)
        assert (y.dtype == x.dtype)
        assert (np.array_equal(x, y))

    def test_writing_pixels_int16 (self):
        x = np.empty((4,200,400), dtype=np.int16)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        filename_out = '%sint16.tif'%(self.filename_out_prefix)
        bim.write(x, filename_out, 'tiff')

        y = bim.read(filename_out)
        assert (y.shape == x.shape)
        assert (y.dtype == x.dtype)
        assert (np.array_equal(x, y))

    def test_writing_pixels_int32 (self):
        x = np.empty((4,200,400), dtype=np.int32)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        filename_out = '%sint32.tif'%(self.filename_out_prefix)
        bim.write(x, filename_out, 'tiff')

        y = bim.read(filename_out)
        assert (y.shape == x.shape)
        assert (y.dtype == x.dtype)
        assert (np.array_equal(x, y))

    def test_writing_pixels_int64 (self):
        x = np.empty((4,200,400), dtype=np.int64)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        filename_out = '%sint64.tif'%(self.filename_out_prefix)
        bim.write(x, filename_out, 'tiff')

        y = bim.read(filename_out)
        assert (y.shape == x.shape)
        assert (y.dtype == x.dtype)
        assert (np.array_equal(x, y))

    #-----------------------------------------------------------------
    # float
    #-----------------------------------------------------------------

    def test_writing_pixels_float32 (self):
        x = np.empty((4,200,400), dtype=np.float32)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        filename_out = '%sfloat32.tif'%(self.filename_out_prefix)
        bim.write(x, filename_out, 'tiff')

        y = bim.read(filename_out)
        assert (y.shape == x.shape)
        assert (y.dtype == x.dtype)
        assert (np.array_equal(x, y))

    def test_writing_pixels_float64 (self):
        x = np.empty((4,200,400), dtype=np.float64)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        filename_out = '%sfloat64.tif'%(self.filename_out_prefix)
        bim.write(x, filename_out, 'tiff')

        y = bim.read(filename_out)
        assert (y.shape == x.shape)
        assert (y.dtype == x.dtype)
        assert (np.array_equal(x, y))
