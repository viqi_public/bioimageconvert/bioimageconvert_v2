/*****************************************************************************
  OpenSlide support
  Copyright (c) 2019, ViQI Inc

  Author: Dmitry Fedorov <mailto:dima@viqi.org> <http://www.viqi.org/>

  History:
  2019-05-23 11:50:40 - First creation

  ver : 1
  *****************************************************************************/

#ifndef BIM_OPENSLIDE_FORMAT_H
#define BIM_OPENSLIDE_FORMAT_H

#include <cstdio>
#include <fstream>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <vector>

#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>

#ifdef BIM_WIN
#include "openslide.h"
#else
//#include "openslide/openslide.h"
#include "openslide.h" // using our own openslide build
#endif

// DLL EXPORT FUNCTION
extern "C" {
bim::FormatHeader *openslideGetFormatHeader(void);
}

namespace bim {

//----------------------------------------------------------------------------
// CZIParams - decoding params
//----------------------------------------------------------------------------

class OpenSlideParams {
public:
    static const int virtual_tile_width = 256;
    static const int virtual_tile_height = 256;
    static const int region_max_side = 2048; //2048 4096 8192;
    const std::vector<std::string> channel_names = { "Red", "Green", "Blue", "Alpha" };
    const std::vector<std::vector<int>> channel_colors = { { 255, 0, 0, 255 }, { 0, 255, 0, 255 }, { 0, 0, 255, 255 }, { 0, 0, 0, 0 } };

public:
    OpenSlideParams();
    ~OpenSlideParams();

    ImageInfo i;
    openslide_t *osr = NULL;
    bim::uint64 image_width;
    bim::uint64 image_height;
    std::vector<double> downsamples;
    std::vector<double> scales;
    std::vector<bim::uint64> W;
    std::vector<bim::uint64> H;
    xstring background_color;
    bim::uint8 background_value = 255;
    bim::xstring path;

    std::vector<char> buffer_icc;

    void open(const char *filename, bim::ImageIOModes mode);
};

} // namespace bim

#endif // BIM_OPENSLIDE_FORMAT_H
