/*******************************************************************************

  Manager for Image Formats

  Uses DimFiSDK version: 1.2

  Programmer: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  Notes:
    Session: during session any session wide operation will be performed
    with current session!!!
    If you want to start simultaneous sessions then create new manager
    using = operator. It will copy all necessary initialization data
    to the new manager.

    Non-session wide operation might be performed simultaneously with
    an open session.

  History:
    03/23/2004 18:03 - First creation
    08/04/2004 18:22 - custom stream managment compliant

  ver: 2

*******************************************************************************/

#ifndef BIM_FORMAT_MANAGER_H
#define BIM_FORMAT_MANAGER_H

#include <ctime>
#include <iterator>
#include <list>
#include <map>
#include <set>
#include <string>
#include <vector>

#include <xtypes.h>
#include <bim_image.h>
#include <bim_img_format_interface.h>
#include <tag_map.h>
#include <xstring.h>

namespace bim {


class FormatManager;

//----------------------------------------------------------------------------
// SessionCache is the cache of Format Managers
//----------------------------------------------------------------------------

class SessionCache {
public:
    SessionCache(int max_size = 3) {
        this->max_size = max_size;
    };
    ~SessionCache() {
        this->destroy();
    };

    bool cached(const std::string &filename) const;
    FormatManager *pop(const std::string &filename);
    void push(FormatManager *cache_item);

    void optimize(time_t max_delta_hours = 4);
    void destroy();
    void clear_buffers() {
        this->image.clear();
        this->output.clear();
    }

public:
    Image image;
    xstring output;

protected:
    size_t max_size = 3;
    std::list<FormatManager *> cache;
};

//----------------------------------------------------------------------------
// SessionCacher is a helper to access and properly close or cache FormatManager
//----------------------------------------------------------------------------

class FormatManagerCacher {
public:
    FormatManagerCacher(const std::string &filename, SessionCache *cache_p = 0, XConf *c = 0);
    ~FormatManagerCacher();

    FormatManager *get() const { return this->fm; };
    //void set(FormatManager *item) { this->fm = item; }

    void forget();   // remove from cache and destory FM
    void remember(); // store to cache

    void destroy_forgotten(); // destroy FM if not in cache
    void destroy();           // destroy FM
    void clear();             // clear FM pointer without destroing FM

protected:
    FormatManager *fm = 0;
    SessionCache *cache = 0;
    std::string filename;
};

//----------------------------------------------------------------------------
// FormatManager
//----------------------------------------------------------------------------

class FormatManager {
public:
    FormatManager(XConf *c = NULL);
    ~FormatManager();
    //FormatManager &operator=(FormatManager fm);

    void addNewFormatHeader(FormatHeader *nfh);
    FormatHeader *getFormatHeader(int i);
    unsigned int countInstalledFormats();
    void setConfiguration(XConf *c = NULL);
    bool isPathSameAsConfig() const;

    //bool              canWriteMulti (const char *formatName);
    bool isFormatSupported(const char *formatName);
    bool isFormatSupportsR(const char *formatName);
    bool isFormatSupportsW(const char *formatName);
    bool isFormatSupportsWMP(const char *formatName);
    bool isFormatSupportsBpcW(const char *formatName, int bpc);

    void printAllFormats();
    void printAllFormatsXML();
    void printAllFormatsHTML();
    std::string getAllFormatsHTML();

    const char *getNeededFormatByFileExt(const char *extension);

    std::vector<std::string> getReadFormats();
    std::vector<std::string> getWriteFormats();
    std::vector<std::string> getWriteMPFormats();
    std::string getAllExtensions();
    std::string getFormatFilter(const char *formatName);
    std::string getFilterExtensions(const char *formatName);
    std::string getFilterExtensionFirst(const char *formatName);

    std::string getQtFilters();
    std::string getQtReadFilters();
    std::string getQtWriteFilters();
    std::string getQtWriteMPFilters();

    std::set<std::string> getExtensions();
    std::set<std::string> getReadExtensions();
    std::set<std::string> getWriteExtensions();
    std::set<std::string> getWriteMPFExtensions();

    // Simple Read/Write
    int loadImage(const bim::Filename fileName, ImageBitmap *bmp);
    int loadImage(const bim::Filename fileName, Image &img) { return loadImage(fileName, img.imageBitmap()); }
    int loadImage(const bim::Filename fileName, ImageBitmap *bmp, int page);
    int loadImage(const bim::Filename fileName, Image &img, int page) { return loadImage(fileName, img.imageBitmap(), page); }


    int loadImage(BIM_STREAM_CLASS *stream,
                  ReadProc readProc, SeekProc seekProc, SizeProc sizeProc,
                  TellProc tellProc, EofProc eofProc, CloseProc closeProc,
                  const bim::Filename fileName, ImageBitmap *bmp, int page);
    //void loadBuffer (void *p, int buf_size, ImageBitmap *bmp);

    void readImagePreview(const bim::Filename fileName, ImageBitmap *bmp,
                          bim::uint roiX, bim::uint roiY, bim::uint roiW, bim::uint roiH,
                          bim::uint w, bim::uint h);
    void readImageThumb(const bim::Filename fileName, ImageBitmap *bmp, bim::uint w, bim::uint h);


    int writeImage(BIM_STREAM_CLASS *stream, ReadProc readProc, WriteProc writeProc, FlushProc flushProc,
                   SeekProc seekProc, SizeProc sizeProc, TellProc tellProc,
                   EofProc eofProc, CloseProc closeProc, const bim::Filename fileName,
                   ImageBitmap *bmp, const char *formatName, int quality, TagMap *meta = NULL, const char *options = NULL);
    int writeImage(const bim::Filename fileName, ImageBitmap *bmp, const char *formatName, int quality, TagMap *meta = NULL);
    int writeImage(const bim::Filename fileName, ImageBitmap *bmp, const char *formatName, const char *options = NULL, TagMap *meta = NULL);
    int writeImage(const bim::Filename fileName, Image &img, const char *formatName, const char *options = NULL, TagMap *meta = NULL) {
        return writeImage(fileName, img.imageBitmap(), formatName, options, meta);
    }

    //unsigned char *writeBuffer ( ImageBitmap *bmp, const char *formatName, int quality, TagList *meta, int &buf_size );
    //unsigned char *writeBuffer ( ImageBitmap *bmp, const char *formatName, int quality, int &buf_size );
    //unsigned char *writeBuffer ( ImageBitmap *bmp, const char *formatName, int &buf_size );

    //--------------------------------------------------------------------------------------
    // Callbacks
    //--------------------------------------------------------------------------------------

    ProgressProc progress_proc;
    ErrorProc error_proc;
    TestAbortProc test_abort_proc;
    void setCallbacks(ProgressProc _progress_proc,
                      ErrorProc _error_proc,
                      TestAbortProc _test_abort_proc) {
        progress_proc = _progress_proc;
        error_proc = _error_proc;
        test_abort_proc = _test_abort_proc;
    }

    //--------------------------------------------------------------------------------------
    // session-wide operations
    //--------------------------------------------------------------------------------------
    bool sessionActive() const { return session_active; }

    time_t sessionGetAccessTime() const { return this->access_time; };
    const std::string &sessionFilename() const { return sessionFileName; }
    const std::string &sessionPath() const { return this->session_path; }
    bool sessionIsReading(const std::string &fileName = "") const;
    bool sessionIsWriting(const std::string &fileName = "") const;

    int sessionStartRead(BIM_STREAM_CLASS *stream, ReadProc readProc, SeekProc seekProc,
                         SizeProc sizeProc, TellProc tellProc, EofProc eofProc,
                         CloseProc closeProc, const bim::Filename fileName, const char *formatName = NULL);

    int sessionStartWrite(BIM_STREAM_CLASS *stream, WriteProc writeProc, FlushProc flushProc,
                          SeekProc seekProc, SizeProc sizeProc, TellProc tellProc, EofProc eofProc,
                          CloseProc closeProc, const bim::Filename fileName, const char *formatName, const char *options = NULL);

    // if formatName spesified then forces specific format, used for RAW
    int sessionStartRead(const bim::Filename fileName, const char *formatName = NULL);
    int sessionStartReadRAW(const bim::Filename fileName, unsigned int header_offset = 0, bool big_endian = false, bool interleaved = false);
    int sessionStartWrite(const bim::Filename fileName, const char *formatName, const char *options = NULL);

    int sessionGetFormat();
    int sessionGetSubFormat();
    const char *sessionGetCodecName();
    const char *sessionGetFormatName();
    bool sessionIsCurrentCodec(const char *name);
    bool sessionIsCurrentFormat(const char *name);
    uint sessionGetNumberOfPages();
    const ImageInfo &sessionGetInfo() const { return this->info; }
    int sessionGetCurrentPage();
    //TagList* sessionReadMetaData ( bim::uint page, int group, int tag, int type);
    int sessionReadImage(ImageBitmap *bmp, bim::uint page);
    int sessionReadImage(Image &img, bim::uint page) {
        return sessionReadImage(img.imageBitmap(), page);
    }

    void sessionReadImagePreview(ImageBitmap *bmp,
                                 bim::uint roiX, bim::uint roiY, bim::uint roiW, bim::uint roiH,
                                 bim::uint w, bim::uint h);
    void sessionReadImageThumb(ImageBitmap *bmp, bim::uint w, bim::uint h);


    int sessionReadLevel(ImageBitmap *bmp, bim::uint page, uint level);
    int sessionReadLevel(Image &img, bim::uint page, uint level) {
        return sessionReadLevel(img.imageBitmap(), page, level);
    }

    int sessionReadTile(ImageBitmap *bmp, bim::uint page, uint64 xid, uint64 yid, uint level);
    int sessionReadTile(Image &img, bim::uint page, uint64 xid, uint64 yid, uint level) {
        return sessionReadTile(img.imageBitmap(), page, xid, yid, level);
    }

    int sessionReadRegion(ImageBitmap *bmp, bim::uint page, bim::uint64 x1, bim::uint64 y1, bim::uint64 x2, bim::uint64 y2, bim::uint level);
    int sessionReadRegion(Image &img, bim::uint page, bim::uint64 x1, bim::uint64 y1, bim::uint64 x2, bim::uint64 y2, bim::uint level) {
        return sessionReadRegion(img.imageBitmap(), page, x1, y1, x2, y2, level);
    }

    void sessionSetQuality(int quality);
    void sessionSetNumberOfPages(int num_pages);
    int sessionWriteImage(ImageBitmap *bmp, bim::uint page);
    int sessionWriteImage(Image &img, bim::uint page) { return sessionWriteImage(img.imageBitmap(), page); }

    void sessionEnd();

    //SessionCacheItem* FormatManager::sessionTransferToCacheItem();
    //void sessionTransferFromCacheItem(SessionCacheItem *cache_item);

    //--------------------------------------------------------------------------------------
    // metadata
    //--------------------------------------------------------------------------------------
public:
    inline const TagMap *meta() const { return &metadata; }
    inline const TagMap &get_metadata() const { return metadata; }

    inline std::string get_metadata_tag(const std::string &key, const std::string &def) const { return metadata.get_value(key, def); }
    inline int get_metadata_tag_int(const std::string &key, const int &def) const { return metadata.get_value_int(key, def); }
    inline double get_metadata_tag_double(const std::string &key, const double &def) const { return metadata.get_value_double(key, def); }

    void delete_metadata_tag(const std::string &key) { metadata.delete_tag(key); }

    void set_metadata_tag(const std::string &key, const int &v) { metadata.set_value(key, v); }
    void set_metadata_tag(const std::string &key, const double &v) { metadata.set_value(key, v); }
    void set_metadata_tag(const std::string &key, const std::string &v) { metadata.set_value(key, v); }

    // construct OME-XML from the internal meta data, and return it as a std::string
    std::string constructOMEXML();

    // session-level metadata
    void sessionParseMetaData(bim::uint page);
    void sessionWriteSetMetadata(const TagMap &hash);
    void sessionWriteSetOMEXML(const std::string &omexml);

private:
    std::vector<std::string> display_channel_tag_names;

    // key-value pairs for metadata
    inline void appendMetadata(const std::string &key, const std::string &value) { metadata.append_tag(key, value); }
    inline void appendMetadata(const std::string &key, const int &value) { metadata.append_tag(key, value); }
    inline void appendMetadata(const std::string &key, const double &value) { metadata.append_tag(key, value); }

    void append_channel_names(const std::vector<std::string> &names);

protected:
    std::vector<unsigned char> magic_number;
    ImageInfo info;
    XConf *conf = NULL;

    // session vars
    bool session_active = false;
    FormatHandle sessionHandle;
    int sessionFormatIndex = 0;
    int sessionSubIndex = 0;
    int sessionCurrentPage = 0;
    std::string sessionFileName;
    std::string session_path;

    int got_meta_for_session = -1;
    TagMap metadata;
    time_t access_time=0;

    std::vector<FormatHeader *> formatList;

    void setMaxMagicSize();
    bool loadMagic(const bim::Filename fileName);
    bool loadMagic(BIM_STREAM_CLASS *stream, SeekProc seekProc, ReadProc readProc);
    void getNeededFormatByMagic(const bim::Filename fileName, int &format_index, int &sub_index);
    void getNeededFormatByName(const bim::xstring &formatName, int &format_index, int &sub_index);
    void getNeededFormatByFileExt(const bim::Filename fileName, int &format_index, int &sub_index);

    FormatItem *getFormatItem(const char *formatName);

private:
    FormatManager &operator=(FormatManager fm) = delete;
};

} // namespace bim

#endif // BIM_FORMAT_MANAGER_H
