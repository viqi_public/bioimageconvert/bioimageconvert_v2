/*****************************************************************************
  Composite file support - some simple header describing multiple image files
  Copyright (c) 2020, ViQi Inc
  
  Author: Dima Fedorov <mailto:dima@viqi.org> <http://viqi.org>

  History:
    2020-03-25 15:48:00 - First creation
        
  Ver : 1
*****************************************************************************/

#ifndef BIM_COMPOSITE_FORMAT_H
#define BIM_COMPOSITE_FORMAT_H

#include <vector>

#include <bim_image.h>
#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>
#include <tag_map.h>
#include <xstring.h>

// DLL EXPORT FUNCTION
extern "C" {
bim::FormatHeader *compositeGetFormatHeader(void);
}

namespace bim {

//----------------------------------------------------------------------------
// bim::CompositeFormat
//----------------------------------------------------------------------------

class CompositeFormat {
public:
    // Plug-in functions
    typedef bool (*CompValidateProc)(const bim::xstring &filename);
    typedef void (*CompOpenProc)(const bim::xstring &filename, bim::ImageIOModes io_mode, CompositeFormat *f, FormatHandle *fmtHndl);
    typedef void (*CompParseMetaPerFileProc)(bim::TagMap &meta, size_t sample, const bim::xstring &filename, CompositeFormat *f);
    typedef void (*CompParseMetaProc)(bim::TagMap &meta, CompositeFormat *f, FormatHandle *fmtHndl);

    struct SubFormatPlugin {
        CompValidateProc validate;
        CompOpenProc open;
        CompParseMetaPerFileProc parseMetaPerFile;
        CompParseMetaProc parseMeta;
    };

public:
    static int validate(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName);
    static int validate(const bim::xstring &filename);

public:
    CompositeFormat();
    ~CompositeFormat();

    void open(const bim::xstring &filename, bim::ImageIOModes io_mode, FormatHandle *fmtHndl);

    ImageInfo *getInfo() { return &this->i; };

    // operations
    void read_image_info(FormatHandle *fmtHndl);
    void read_metadata(bim::TagMap *hash, FormatHandle *fmtHndl);

    void read_level(Image &img, uint64 level, FormatHandle *fmtHndl);
    void read_tile(Image &img, uint64 xid, uint64 yid, uint64 level, FormatHandle *fmtHndl);
    void read_region(Image &img, uint64 x1, uint64 y1, uint64 x2, uint64 y2, uint64 level, FormatHandle *fmtHndl);

public:
    ImageInfo i;
    TagMap metadata;
    bim::xstring path;
    std::vector<bim::xstring> files;
    int sub_format = -1;

    bool channels_in_files = false;
    std::vector<bim::uint64> dimensions;
    std::vector<bim::uint64> positions;

    uint64 get_file_pos(FormatHandle *fmtHndl) const;
};

} // namespace bim

#endif // BIM_COMPOSITE_FORMAT_H
