/*****************************************************************************
RAW support
Copyright (c) 2004 by Dmitry V. Fedorov <www.dimin.net> <dima@dimin.net>
Copyright (c) 2021 ViQi Inc

Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

History:
2005-12-01 15:27 - First creation
2007-07-12 21:01 - reading raw
2021-08-25 15:45 - Cellomics C01/DIB support

Ver: 4
*****************************************************************************/

#ifndef BIM_RAW_FORMAT_H
#define BIM_RAW_FORMAT_H

#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>

// DLL EXPORT FUNCTION
extern "C" {
bim::FormatHeader *rawGetFormatHeader(void);
}

namespace bim {

class RawParams {
public:
    RawParams();
    ~RawParams();

    ImageInfo i;
    unsigned int compressed_stream_offset = 0; // pre-compression stream offset
    unsigned int header_offset = 0;
    bool big_endian = false;
    bool interleaved = false;

    std::vector<double> res;
    std::vector<xstring> units;
    xstring datafile;
    void *datastream = 0;
    TagMap header;
    std::vector<char> uncompressed;

    std::vector<bim::uint8> buffer;
    bim::uint buffer_pos = 0;
};

} // namespace bim

#endif // BIM_BMP_FORMAT_H
