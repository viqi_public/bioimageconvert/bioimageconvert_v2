/*****************************************************************************
  OME Type definitions
  Copyright (c) 2019, Center for Bio-Image Informatics, UCSB

  Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

  History:
    2019-07-09 23:16 - First creation

  Ver : 1
*****************************************************************************/

#include <bim_img_format_utils.h>
#include <bim_metatags.h>
#include <bim_ome_types.h>

#include <tag_map.h>
#include <xstring.h>
#include <xtypes.h>
#include <xdatetime.h>

#include <pugixml.hpp>

namespace bim {

std::string colorRGBStringToOMEString(const xstring &c) {
    //std::vector<int> v = c.splitInt(",");
    std::vector<double> v = c.splitDouble(",");
    OmeColor omec;
    if (v.size() > 0) omec.setRed(static_cast<OmeColor::composed_type>(v[0] * 255));
    if (v.size() > 1) omec.setGreen(static_cast<OmeColor::composed_type>(v[1] * 255));
    if (v.size() > 2) omec.setBlue(static_cast<OmeColor::composed_type>(v[2] * 255));
    if (v.size() > 3) omec.setAlpha(static_cast<OmeColor::composed_type>(v[3] * 255));
    return omec.toString();
}

std::string colorOMEStringToRGBString(const xstring &c) {
    OmeColor omec(c.toInt());
    //return xstring::xprintf("%d,%d,%d", omec.getRed(), omec.getGreen(), omec.getBlue());
    return xstring::xprintf("%.2f,%.2f,%.2f", omec.getRed() / 255.0, omec.getGreen() / 255.0, omec.getBlue() / 255.0);
}

std::string omeTiffPixelType(const bim::uint32 depth, const bim::DataFormat pixel_type) {
    // See for example the documentation at https://www.javadoc.io/doc/org.openmicroscopy/ome-xml/latest/ome/xml/model/enums/PixelType.html
    // or the implementation at https://github.com/ome/bioformats/blob/master/components/formats-api/src/loci/formats/FormatTools.java#L116
    if (depth == 1 && pixel_type == bim::DataFormat::FMT_UNSIGNED)
        return "bit";
    else if (depth == 8 && pixel_type == bim::DataFormat::FMT_UNSIGNED)
        return "uint8";
    else if (depth == 16 && pixel_type == bim::DataFormat::FMT_UNSIGNED)
        return "uint16";
    else if (depth == 32 && pixel_type == bim::DataFormat::FMT_UNSIGNED)
        return "uint32";
    else if (depth == 8 && pixel_type == bim::DataFormat::FMT_SIGNED)
        return "int8";
    else if (depth == 16 && pixel_type == bim::DataFormat::FMT_SIGNED)
        return "int16";
    else if (depth == 32 && pixel_type == bim::DataFormat::FMT_SIGNED)
        return "int32";
    else if (depth == 32 && pixel_type == bim::DataFormat::FMT_FLOAT)
        return "float";
    else if (depth == 64 && pixel_type == bim::DataFormat::FMT_FLOAT)
        return "double";

    throw std::runtime_error("omeTiffPixelType(): Unsupported depth/pixel type combination");
}

std::string omeTiffPixelType(const bim::ImageBitmap *img) {
    return omeTiffPixelType(img->i.depth, img->i.pixelType);
}

std::string constructOMEXML(const std::string &filename, const bim::uint64 width, const bim::uint64 height, const bim::uint32 samples,
                            const bim::uint32 depth, const bim::DataFormat pixel_type, const bim::TagMap *hash,
                            const bool short_ome_format) {

    bim::uint64 number_z = hash->get_value_unsigned(bim::IMAGE_NUM_Z, 1);
    bim::uint64 number_t = hash->get_value_unsigned(bim::IMAGE_NUM_T, 1);

    // Header
    std::string str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    str += "<!-- Warning: this comment is an OME-XML metadata block, which contains crucial dimensional parameters and other important metadata. Please edit cautiously (if at all), and back up the original data before doing so. For more information, see the OME-TIFF web site: http://loci.wisc.edu/ome/ome-tiff.html. -->";
    // version <=4
    //str += "<OME xmlns=\"http://www.openmicroscopy.org/XMLschemas/OME/FC/ome.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.openmicroscopy.org/XMLschemas/OME/FC/ome.xsd http://www.openmicroscopy.org/XMLschemas/OME/FC/ome.xsd\">";
    // version >= 5
    str += "<OME xmlns=\"http://www.openmicroscopy.org/Schemas/OME/2013-06\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.openmicroscopy.org/Schemas/OME/2013-06 http://www.openmicroscopy.org/Schemas/OME/2013-06/ome.xsd\">";

    // Image tag
    str += "<Image ID=\"Image:0\" Name=\"" + filename + "\">";

    if (hash && hash->hasKey(bim::DOCUMENT_DATETIME))
        str += bim::xstring::xprintf("<AcquisitionDate>%s</AcquisitionDate>", hash->get_value(bim::DOCUMENT_DATETIME).c_str());

    str += bim::xstring::xprintf("<Description>Constructed by libbioimage ome-tiff encoder v4.0.3</Description>");


    str += "<Pixels ID=\"Pixels:0\"";
    str += " DimensionOrder=\"XYCZT\"";
    str += bim::xstring::xprintf(" Type=\"%s\"", omeTiffPixelType(depth, pixel_type).c_str());
    str += bim::xstring::xprintf(" SignificantBits=\"%d\"", depth);
    str += " BigEndian=\"false\" Interleaved=\"false\"";

    str += bim::xstring::xprintf(" SizeX=\"%d\"", width);
    str += bim::xstring::xprintf(" SizeY=\"%d\"", height);
    str += bim::xstring::xprintf(" SizeC=\"%d\"", samples);
    if (hash && hash->size() > 0) {
        str += bim::xstring::xprintf(" SizeZ=\"%d\"", hash->get_value_int(bim::IMAGE_NUM_Z, (int)number_z));
        str += bim::xstring::xprintf(" SizeT=\"%d\"", hash->get_value_int(bim::IMAGE_NUM_T, (int)number_t));
    } else {
        str += bim::xstring::xprintf(" SizeZ=\"%d\"", number_z);
        str += bim::xstring::xprintf(" SizeT=\"%d\"", number_t);
    }

    // writing physical sizes
    if (hash && hash->size() > 0) {
        if (hash->hasKey(bim::PIXEL_RESOLUTION_X))
            str += bim::xstring::xprintf(" PhysicalSizeX=\"%s\"", hash->get_value(bim::PIXEL_RESOLUTION_X).c_str());
        if (hash->hasKey(bim::PIXEL_RESOLUTION_Y))
            str += bim::xstring::xprintf(" PhysicalSizeY=\"%s\"", hash->get_value(bim::PIXEL_RESOLUTION_Y).c_str());
        if (hash->hasKey(bim::PIXEL_RESOLUTION_Z))
            str += bim::xstring::xprintf(" PhysicalSizeZ=\"%s\"", hash->get_value(bim::PIXEL_RESOLUTION_Z).c_str());
        if (hash->hasKey(bim::PIXEL_RESOLUTION_T))
            str += bim::xstring::xprintf(" TimeIncrement=\"%s\"", hash->get_value(bim::PIXEL_RESOLUTION_T).c_str());
    }

    str += " >";

    // channel names
    if (hash && hash->size() > 0) {
        for (bim::uint i = 0; i < samples; ++i) {
            bim::xstring path = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i);
            bim::xstring name = path + bim::CHANNEL_INFO_NAME;
            bim::xstring color = path + bim::CHANNEL_INFO_COLOR;
            if (hash->hasKey(name)) {
                str += bim::xstring::xprintf("<Channel ID=\"Channel:0:%d\" Name=\"%s\" SamplesPerPixel=\"1\"", i, hash->get_value(name).c_str());
                if (hash->hasKey(color)) {
                    str += bim::xstring::xprintf(" Color=\"%s\"", colorRGBStringToOMEString(hash->get_value(color)).c_str());
                }
                str += ">";
                str += bim::xstring::xprintf("<DetectorSettings ID=\"Detector:0:%d\" /><LightPath/>", i);
                str += "</Channel>";
            }
        }
    }


    str += "<TiffData/></Pixels>";
    str += "</Image>";

    const std::string ome_key_prefix = "OME/";

    // custom attributes
    if (hash && hash->size() > 0) {
        str += "<StructuredAnnotations xmlns=\"http://www.openmicroscopy.org/Schemas/SA/2013-06\">";
        if (short_ome_format) {
            str += "<MapAnnotation ID=\"Annotation:1\">";
            str += "<Value>";
            for (bim::TagMap::const_iterator it = hash->begin(); it != hash->end(); ++it) {
                const bim::xstring &key = it->first;
                if (key.startsWith(ome_key_prefix) && key != xstring(ome_key_prefix + "Image Description")) {
                    str += "<M K=\"" + key.right(ome_key_prefix.size()) + "\">" + it->second.as_string() + "</M>";
                }
            }
            str += "</Value>";
            str += "</MapAnnotation>";
        } else {
            int i = 0;
            bim::TagMap::const_iterator it;
            for (it = hash->begin(); it != hash->end(); ++it) {
                const bim::xstring &key = it->first;
                if (key.startsWith(ome_key_prefix) && key != xstring(ome_key_prefix + "Image Description")) {
                    str += bim::xstring::xprintf("<XMLAnnotation ID=\"Annotation:%d\" Namespace=\"openmicroscopy.org/OriginalMetadata\">", i);
                    str += "<Value xmlns=\"\"><OriginalMetadata>";
                    // dima: here we should be encoding strings into utf-8, though bioformats seems to expect latin1, skip encoding for now
                    str += bim::xstring::xprintf("<Key>%s</Key>", key.right(ome_key_prefix.size()).c_str());
                    str += bim::xstring::xprintf("<Value>%s</Value>", it->second.as_string().c_str());
                    str += "</OriginalMetadata></Value></XMLAnnotation>";
                    i++;
                }
            }
        }
        str += "</StructuredAnnotations>";
    }

    str += "</OME>";
    return str;
}

std::string constructOMEXML(const std::string &filename, const bim::ImageBitmap *img, const bim::TagMap *hash, const bool short_ome_format) {
    if (!img) return "";
    return constructOMEXML(filename, img->i.width, img->i.height, img->i.samples, img->i.depth, img->i.pixelType, hash, short_ome_format);
}

void walker(pugi::xml_node node, bim::xstring path, TagMap *hash) {

    bim::xstring tag = node.name();
    if (tag.size() < 1) return;
    // use tag name
    path += path.size() > 0 ? "/" + tag : tag;

    // add text node as a value
    bim::xstring value = node.text().as_string();
    if (value.size() > 0)
        hash->set_value(path, value);

    // iterate over attributes
    for (pugi::xml_attribute_iterator ait = node.attributes_begin(); ait != node.attributes_end(); ++ait) {
        hash->set_value(path + "/" + ait->name(), ait->value());
    }

    // iterate over children
    for (pugi::xml_node child = node.first_child(); child; child = child.next_sibling()) {
        walker(child, path, hash);
    }
}

bim::TagMap parseOMEXML(const bim::xstring &tag_270, const bim::uint64 number_z, const bim::uint samples) {
    bim::TagMap hash;

    if (tag_270.empty()) return hash;

    //----------------------------------------------------------------------------
    // parse OME-XML if possible and add as OME/ tags
    //----------------------------------------------------------------------------

    // parse the whole XML into tags
    if (tag_270.size() > 10) {
        pugi::xml_document doc;
        if (doc.load_buffer(tag_270.c_str(), tag_270.size())) {
            // parse all tags from the document
            pugi::xml_node child = doc.first_child();
            bim::xstring path = "";
            walker(doc.first_child(), path, &hash);
        }
    }

    //----------------------------------------------------------------------------
    // Channel names and preferred mapping
    //----------------------------------------------------------------------------

    // channel names may also be stored in Logical channel in the Image
    // v <=4
    std::string::size_type p = tag_270.find("<LogicalChannel ");
    if (p != std::string::npos)
        for (unsigned int i = 0; i < samples; ++i) {

            //if (p == std::string::npos) continue;
            bim::xstring tag = tag_270.section("<LogicalChannel", ">", p);
            if (tag.size() <= 0) continue;
            tag = ometiff_normalize_xml_spaces(tag);
            bim::xstring medium = tag.section(" Name=\"", "\"");
            if (medium.size() <= 0) continue;
            bim::xstring wavelength = tag.section(" ExWave=\"", "\"");
            bim::xstring wavelength_em = tag.section(" EmWave=\"", "\"");
            bim::xstring illumination = tag.section(" IlluminationType=\"", "\"");
            bim::xstring color = tag.section(" Color=\"", "\"");
            int chan = i;
            p = tag_270.find("<ChannelComponent", p);
            tag = tag_270.section("<ChannelComponent", ">", p);
            if (tag.size() > 0) {
                tag = ometiff_normalize_xml_spaces(tag);
                bim::xstring index = tag.section(" Index=\"", "\"");
                chan = index.toInt(i);
            }

            xstring fullname = (wavelength.size() > 0) ? medium + " - " + wavelength + "nm" : medium;
            xstring path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i);
            //hash.set_value(bim::xstring::xprintf(bim::CHANNEL_NAME_TEMPLATE.c_str(), i), fullname);
            hash.set_value(path + bim::CHANNEL_INFO_NAME, fullname);
            //hash.set_value(path + bim::CHANNEL_INFO_FLUOR, medium);
            hash.set_value(path + bim::CHANNEL_INFO_EX_WAVELENGTH, wavelength);
            hash.set_value(path + bim::CHANNEL_INFO_EM_WAVELENGTH, wavelength_em);
            hash.set_value(path + bim::CHANNEL_INFO_MODALITY, illumination);
            if (color.size() > 0) {
                //hash.set_value(xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), chan), colorOMEStringToRGBString(color));
                hash.set_value(path + bim::CHANNEL_INFO_COLOR, colorOMEStringToRGBString(color));
            }

            p += 15;
        }

    // channel names properly stored in LightSource
    // v <=4
    for (unsigned int i = 0; i < samples; ++i) {
        bim::xstring heading = bim::xstring::xprintf("<LightSource ID=\"LightSource:%d\">", i);
        std::string::size_type p = tag_270.find(heading);
        if (p == std::string::npos) {
            heading = bim::xstring::xprintf("<LightSource ID=\"LightSource:0:%d\">", i);
            p = tag_270.find(heading);
        }
        if (p == std::string::npos) continue;
        bim::xstring tag_laser = tag_270.section("<Laser", ">", p);
        if (tag_laser.size() <= 0) continue;
        bim::xstring medium = tag_laser.section(" LaserMedium=\"", "\"");
        if (medium.size() <= 0) continue;
        bim::xstring wavelength = tag_laser.section(" Wavelength=\"", "\"");

        xstring fullname = (wavelength.size() > 0) ? medium + " - " + wavelength + "nm" : medium;
        xstring path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i);
        //hash.set_value(bim::xstring::xprintf(bim::CHANNEL_NAME_TEMPLATE.c_str(), i), fullname);
        hash.set_value(path + bim::CHANNEL_INFO_NAME, fullname);
        hash.set_value(path + bim::CHANNEL_INFO_FLUOR, medium);
        hash.set_value(path + bim::CHANNEL_INFO_EX_WAVELENGTH, wavelength);
    }

    // channel names may also be stored in Logical channel in the Image
    // v >=5
    // read using pugixml
    bim::uint red_channels = 0;
    pugi::xml_document doc;
    if (doc.load_buffer(tag_270.c_str(), tag_270.size())) {
        try {
            pugi::xpath_node_set channels = doc.select_nodes("/OME/Image/Pixels/Channel");
            for (pugi::xpath_node_set::const_iterator it = channels.begin(); it != channels.end(); ++it) {
                pugi::xpath_node node = *it;
                bim::xstring medium = node.node().attribute("Name").value();
                bim::xstring index = node.node().attribute("ID").value();
                bim::xstring wavelength = node.node().attribute("ExcitationWavelength").value();
                bim::xstring color = node.node().attribute("Color").value();

                // parse index
                std::vector<bim::xstring> ids = index.split(":");
                if (ids.size() > 2) {
                    int chan = ids[2].toInt(0);
                    xstring fullname = (wavelength.size() > 0) ? medium + " - " + wavelength + "nm" : medium;
                    xstring path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), chan);
                    //hash.set_value(bim::xstring::xprintf(bim::CHANNEL_NAME_TEMPLATE.c_str(), chan), fullname);
                    hash.set_value(path + bim::CHANNEL_INFO_NAME, fullname);
                    hash.set_value(path + bim::CHANNEL_INFO_FLUOR, medium);
                    hash.set_value(path + bim::CHANNEL_INFO_EX_WAVELENGTH, wavelength);
                    if (color.size() > 0) {
                        //hash.set_value(xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), chan), colorOMEStringToRGBString(color));
                        hash.set_value(path + bim::CHANNEL_INFO_COLOR, colorOMEStringToRGBString(color));
                    }
                    ++red_channels;
                }
            }
        } catch (pugi::xpath_exception ) {
            // do nothing
        }
    }

    // OME-XML may contain errors, try old way of reading
    if (red_channels < samples) {
        p = tag_270.find("<Channel ");
        if (p != std::string::npos)
            for (unsigned int i = 0; i < samples; ++i) {

                bim::xstring heading = bim::xstring::xprintf("<Channel ID=\"Channel:0:%d\"", i);
                bim::xstring tag = tag_270.section(heading, ">", p);
                if (tag.size() <= 0) continue;
                tag = ometiff_normalize_xml_spaces(tag);
                bim::xstring medium = tag.section(" Name=\"", "\"");
                if (medium.size() <= 0) continue;

                int chan = i;
                bim::xstring wavelength = tag.section(" ExcitationWavelength=\"", "\"");
                bim::xstring color = tag.section(" Color=\"", "\"");

                xstring fullname = (wavelength.size() > 0) ? medium + " - " + wavelength + "nm" : medium;
                xstring path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), chan);
                //hash.set_value(bim::xstring::xprintf(bim::CHANNEL_NAME_TEMPLATE.c_str(), chan), fullname);
                hash.set_value(path + bim::CHANNEL_INFO_NAME, fullname);
                hash.set_value(path + bim::CHANNEL_INFO_FLUOR, medium);
                hash.set_value(path + bim::CHANNEL_INFO_EX_WAVELENGTH, wavelength);
                if (color.size() > 0) {
                    //hash.set_value(xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), chan), colorOMEStringToRGBString(color));
                    hash.set_value(path + bim::CHANNEL_INFO_COLOR, colorOMEStringToRGBString(color));
                }
            }
    }

    // in FLUIDIGM variant channel XMLs are stored in individual pages
    /*
    <OME xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.openmicroscopy.org/Schemas/OME/2009-09">
    <Instrument ID="Hyperion Tissue Imager"/>
    <Image ID="Pankeratin(Nd148Di)" Name="Pankeratin(Nd148Di)">
        <AcquiredDate>2019-10-17T15:50:47.3813675</AcquiredDate>
        <Pixels DimensionOrder="XYZTC" Type="float" SizeX="500" SizeY="500">
            <Channel ID="Pankeratin(Nd148Di)" Name="Pankeratin(Nd148Di)" SamplesPerPixel="1"/>
        </Pixels>
    </Image></OME>
    */

    //----------------------------------------------------------------------------
    // stage position
    //----------------------------------------------------------------------------
    std::vector<bim::xstring> stage_positions;
    p = tag_270.find("<Plane ");
    while (p != std::string::npos) {
        bim::xstring tag = tag_270.section("<Plane", ">", p);
        if (tag.size() > 0) {
            tag = ometiff_normalize_xml_spaces(tag);
            int c = tag.section(" TheC=\"", "\"").toInt(0);
            int t = tag.section(" TheT=\"", "\"").toInt(0);
            int z = tag.section(" TheZ=\"", "\"").toInt(0);

            tag = tag_270.section("<StagePosition", ">", p);
            if (tag.size() > 0) {
                tag = ometiff_normalize_xml_spaces(tag);
                double sx = tag.section(" PositionX=\"", "\"").toDouble(0);
                double sy = tag.section(" PositionY=\"", "\"").toDouble(0);
                double sz = tag.section(" PositionZ=\"", "\"").toDouble(0);
                bim::uint64 page = t * number_z + z;
                //hash.set_value(bim::xstring::xprintf(bim::STAGE_POSITION_TEMPLATE_X.c_str(), page), sx);
                //hash.set_value(bim::xstring::xprintf(bim::STAGE_POSITION_TEMPLATE_Y.c_str(), page), sy);
                //hash.set_value(bim::xstring::xprintf(bim::STAGE_POSITION_TEMPLATE_Z.c_str(), page), sz);
                stage_positions.push_back(xstring::xprintf("%f,%f,%f", sx, sy, sz));
            }
        }
        p = tag_270.find("<Plane ", p + 5);
    }
    if (stage_positions.size() > 0)
        hash.set_value(bim::COORDINATES_POSITIONS_STAGE, xstring::join(stage_positions, ";"));

    //----------------------------------------------------------------------------
    // more stuff
    //----------------------------------------------------------------------------

    // v <=4
    bim::xstring tag = tag_270.section("<CreationDate>", "</CreationDate>");
    if (tag.size() >= 19) {
        hash.set_value(bim::DOCUMENT_DATETIME, tag);
    }

    // v 5
    tag = tag_270.section("<AcquisitionDate>", "</AcquisitionDate>");
    if (tag.size() >= 19) {
        hash.set_value(bim::DOCUMENT_DATETIME, tag);
    }

    p = tag_270.find("<Instrument ID=\"Instrument:0\">");

    bim::xstring tag_objective = tag_270.section("<Objective", ">", p);
    tag_objective = ometiff_normalize_xml_spaces(tag_objective);
    if (tag_objective.size() > 0) {
        // v4
        //<Objective Model="UPLFLN    40X O  NA:1.30"><LensNA>1.3</LensNA><NominalMagnification>40</NominalMagnification><Correction>Unknown</Correction>
        bim::xstring s = tag_objective.section(" Model=\"", "\"");
        if (s.size() > 0)
            bim::parse_objective_from_string(s, &hash);

        // v5
        //<Objective Correction="PlanApo" Immersion="Other" Iris="false" LensNA="0.8" NominalMagnification="20.0"/>
        if (s.size() == 0) {
            s = tag_objective.section(" Correction=\"", "\"");
            if (s.size() > 0)
                bim::parse_objective_from_string(s, &hash);
        }

        s = tag_objective.section(" NominalMagnification=\"", "\"");
        if (s.size() > 0)
            hash.set_value(bim::OBJECTIVE_MAGNIFICATION, s.toDouble());

        s = tag_objective.section(" LensNA=\"", "\"");
        if (s.size() > 0)
            hash.set_value(bim::OBJECTIVE_NUM_APERTURE, s.toDouble());
    }

    //v4
    bim::xstring tag_magnification = tag_270.section("<NominalMagnification>", "</NominalMagnification>", p);
    if (tag_magnification.size() > 0)
        hash.set_value(bim::OBJECTIVE_MAGNIFICATION, tag_magnification.toDouble());

    bim::xstring tag_NA = tag_270.section("<LensNA>", "</LensNA>", p);
    if (tag_NA.size() > 0)
        hash.set_value(bim::OBJECTIVE_NUM_APERTURE, tag_NA.toDouble());

    //----------------------------------------------------------------------------
    // read all custom attributes
    //----------------------------------------------------------------------------

    if (tag_270.contains("<StructuredAnnotations")) {
        bim::xstring tag_original_meta;
        // new OME-TIFF annotations format >= 5
        p = tag_270.find("<StructuredAnnotations");

        // Short annotation format:
        p = tag_270.find("<MapAnnotation", p);
        tag_original_meta = tag_270.section("<M K=\"", "</M>", p);
        while (tag_original_meta.size() > 0) {
            tag_original_meta = ometiff_normalize_xml_spaces(tag_original_meta);
            const bim::xstring name = tag_original_meta.left("\">");
            const bim::xstring val = tag_original_meta.right("\">");
            if (!name.empty()) {
                // replace all / here with some other character
                hash.set_value(bim::xstring("OME/") + name, val);
            }
            p += tag_original_meta.size();
            tag_original_meta = tag_270.section("<M K=\"", "</M>", p);
        }

        // Long annotation format:
        p = tag_270.find("<OriginalMetadata", p);
        tag_original_meta = tag_270.section("<OriginalMetadata>", "</OriginalMetadata>", p);
        while (tag_original_meta.size() > 0) {
            tag_original_meta = ometiff_normalize_xml_spaces(tag_original_meta);
            const bim::xstring name = tag_original_meta.section("<Key>", "</Key>");
            const bim::xstring val = tag_original_meta.section("<Value>", "</Value>");
            if (!name.empty()) {
                // replace all / here with some other character
                hash.set_value(bim::xstring("OME/") + name, val);
            }
            p += tag_original_meta.size();
            tag_original_meta = tag_270.section("<OriginalMetadata>", "</OriginalMetadata>", p);
        }
    } else {
        // old format <=4
        p = tag_270.find("<CustomAttributes");
        p = tag_270.find("<OriginalMetadata", p);
        bim::xstring tag_original_meta = tag_270.section("<OriginalMetadata", ">", p);
        while (tag_original_meta.size() > 0) {
            tag_original_meta = ometiff_normalize_xml_spaces(tag_original_meta);
            bim::xstring name = tag_original_meta.section(" Name=\"", "\"");
            bim::xstring val = tag_original_meta.section(" Value=\"", "\"");
            if (name.size() > 0 && val.size() > 0) {
                // replace all / here with some other character
                hash.set_value(bim::xstring("OME/") + name, val);
            }
            p += tag_original_meta.size();
            tag_original_meta = tag_270.section("<OriginalMetadata", ">", p);
        }
    }


    //----------------------------------------------------------------------------
    // Add pre-parsed tags for plate
    //----------------------------------------------------------------------------
   
    hash.set_value_from_old_key("OME/Image/Pixels/SignificantBits", bim::PIXEL_SIGNIFICANT_BITS);

    hash.set_value_from_old_key("OME/Plate/Name", bim::DOCUMENT_PLATE);
    hash.set_value_from_old_key("OME/Plate/ID", bim::DOCUMENT_PLATE_ID);
    hash.set_value_from_old_key("OME/Plate/Description/Description", bim::DOCUMENT_PLATE_DESCR);

    hash.set_value_from_old_key("OME/Plate/Well/Row", bim::DOCUMENT_WELL_ROW);
    hash.set_value_from_old_key("OME/Plate/Well/Column", bim::DOCUMENT_WELL_COL);
    hash.set_value_from_old_key("OME/Plate/Well/WellSample/Index", bim::DOCUMENT_WELL_SITE);

    bim::xstring wi = " ";
    wi[0] = static_cast<char>(hash.get_value_int("OME/Plate/Well/Row", 0) + 64);
    int wj = hash.get_value_int("OME/Plate/Well/Column", 0);
    hash.set_value(bim::DOCUMENT_WELL_NAME, xstring::xprintf("%s%d", wi.c_str(), wj));

    if (hash.hasKey("OME/Image/Pixels/Plane/PositionX") && hash.hasKey("OME/Image/Pixels/Plane/PositionY")) {
        float sx = hash.get_value_float("OME/Image/Pixels/Plane/PositionX", 0.0);
        float sy = hash.get_value_float("OME/Image/Pixels/Plane/PositionY", 0.0);
        float sz = hash.get_value_float("OME/Image/Pixels/Plane/PositionZ", 0.0);
        hash.set_value(bim::COORDINATES_POSITIONS_STAGE, xstring::xprintf("%f,%f,%f", sx, sy, sz));
        if (sz != 0)        
            hash.set_value(bim::COORDINATES_POSITIONS_FOCUS, sz);
    }

    if (samples == 1 && hash.hasKey("OME/Image/Pixels/Plane/ExposureTime")) {
        bim::xstring path = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), 0);
        hash.set_value_from_old_key("OME/Image/Pixels/Plane/ExposureTime" , path + bim::CHANNEL_INFO_EXPOSURE);
        hash.set_value_from_old_key("OME/Image/Pixels/Plane/ExposureTimeUnit", path + bim::CHANNEL_INFO_EXPOSURE_UNITS);
    }

    return hash;
}

void ome_xml_append_metadata(const bim::xstring &tag_270, const bim::uint64 number_z, const bim::uint samples, bim::TagMap *hash) {
    if (tag_270.empty() || !hash) return;

    const bim::TagMap ome_hash = parseOMEXML(tag_270, number_z, samples);

    // store the ome meta data:
    hash->set_value(bim::RAW_TAGS_PREFIX + "ome-tiff", tag_270);
    hash->insert(ome_hash.begin(), ome_hash.end());
}

bim::xstring ometiff_normalize_xml_spaces(const bim::xstring &s) {
    bim::xstring o = s;
    bim::xstring::size_type b = 0;

    while (b != std::string::npos) {
        b = o.find("=", b);
        if (b != std::string::npos) {
            while (o[b + 1] == ' ')
                o = o.erase(b + 1, 1);

            while (b > 0 && o[b - 1] == ' ') {
                o = o.erase(b - 1, 1);
                --b;
            }

            ++b;
        }
    }

    return o;
}

} // namespace bim