/*****************************************************************************
  NANOSCOPE
  Copyright (c) 2004 by Dmitry V. Fedorov <www.dimin.net> <dima@dimin.net>
  Copyright (c) 2005 Center for Bio-Image Informatics, UCSB
  Copyright (c) 2021 ViQi Inc

  Author: Dima Fedorov <dima@viqi.org>

  History:
    01/10/2005 12:17 - First creation
    02/08/2005 22:30 - Support for incomplete image sections
    09/12/2005 17:34 - updated to api version 1.3
    2021-11-09 15:50 - cleaned-up

  ver : 4
*****************************************************************************/


#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>

#include <algorithm>
#include <string>
#include <limits>

#include <bim_metatags.h>
#include <tag_map.h>
#include <xstring.h>
#include <xdatetime.h>
#include <xconf.h>

#include "bim_nanoscope_format.h"

using namespace bim;

//----------------------------------------------------------------------------
// READ PROC
//----------------------------------------------------------------------------

bim::uint nanoscopeUpdatePageNumberND(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    NanoscopeParams *par = (NanoscopeParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;
    if (page > 0)
        return page;

    int z = -1;
    if (conf && conf->keyExists("-slice-z"))
        z = conf->getValueInt("-slice-z");

    if (info->number_z > 1 && info->number_t <= 1 && z >= 0) {
        page = z;
    }

    return page;
}

static int read_nanoscope_image(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    NanoscopeParams *par = (NanoscopeParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    if (fmtHndl->stream == NULL) return 1;

    fmtHndl->pageNumber = nanoscopeUpdatePageNumberND(fmtHndl, (bim::uint) fmtHndl->pageNumber);
    size_t page = std::min<size_t>(fmtHndl->pageNumber * par->channels, par->imgs.size() - 1);
    NanoscopeImg *nimg = &par->imgs[page];

    ImageBitmap *img = fmtHndl->image;
    info->width = nimg->width;
    info->height = nimg->height;
    info->samples = (bim::uint32)par->channels;
    info->imageMode = info->samples == 1 ? bim::ImageModes::IM_GRAYSCALE : bim::ImageModes::IM_MULTI;
    info->depth = 16;
    info->pixelType = bim::DataFormat::FMT_SIGNED;

    size_t img_size_bytes = nimg->width * nimg->height * 2;

    // init the image
    if (allocImg(fmtHndl, info, img) != 0) return 1;
    size_t line_size = getLineSizeInBytes(img);
    std::vector<unsigned char> buffer(line_size);
    unsigned char *pbuf = &buffer[0];
    for (int sample = 0; sample < par->channels; ++sample) {
        nimg = &par->imgs[std::min<size_t>(page + sample, par->imgs.size() - 1)];

        if (xseek(fmtHndl, nimg->data_offset, SEEK_SET) != 0) return 1;
        if (xread(fmtHndl, img->bits[sample], img_size_bytes, 1) != 1) return 1;
        if ((img->i.depth == 16) && (bim::bigendian))
            swapArrayOfShort((uint16 *)img->bits[sample], img_size_bytes / 2);

        // keep original signed short format
        /*
        // now convert from SHORT to USHORT
        if (img->i.depth == 16) {
            int16 *pS = (int16 *)img->bits[sample];
            uint16 *pU = (uint16 *)img->bits[sample];
            for (unsigned int x = 0; x < img_size_bytes / 2; x++)
                pU[x] = (unsigned short)(pS[x] + std::numeric_limits<short>::max());
        }
        */

        // now convert from bottom-top to top-bottom
        unsigned char *pt = ((unsigned char *)img->bits[sample]) + (line_size * ((size_t)nimg->height - 1));
        unsigned char *pb = (unsigned char *)img->bits[sample];
        for (int y = 0; y < floor(info->height / 2.0); y++) {
            xprogress(fmtHndl, y, (long)floor(info->height / 2.0), "Reading NANOSCOPE");
            if (xtestAbort(fmtHndl) == 1) break;
            memcpy(pbuf, pb, line_size);
            memcpy(pb, pt, line_size);
            memcpy(pt, pbuf, line_size);
            pb += line_size;
            pt -= line_size;
        }
    } // sample

    return 0;
}

//----------------------------------------------------------------------------
// META DATA PROC
//----------------------------------------------------------------------------

bim::uint nanoscope_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;
    NanoscopeParams *par = (NanoscopeParams *)fmtHndl->internalParams;

    // geometry
    hash->set_value(bim::IMAGE_NUM_Z, static_cast<unsigned int>(par->imgs.size() / par->channels));
    hash->set_value(bim::IMAGE_NUM_P, static_cast<unsigned int>(par->imgs.size() / par->channels));
    hash->set_value(bim::IMAGE_NUM_T, 1);

    // resolution
    NanoscopeImg *nimg = &par->imgs[0];
    double mpp_x = nimg->xR / nimg->width;
    double mpp_y = nimg->yR / nimg->height;
    hash->set_value(bim::PIXEL_RESOLUTION_X, mpp_x);
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    hash->set_value(bim::PIXEL_RESOLUTION_Y, mpp_y);
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y, bim::PIXEL_RESOLUTION_UNIT_MICRONS);

    // Parse some of the notes
    hash->parse_ini(par->metaText, ":", "Nanoscope/");

    // datetime
    if (hash->hasKey("Nanoscope/File list/Date")) {
        //Date:  02:27:29 PM Fri Dec 17 2004
        bim::xstring date = hash->get_value("Nanoscope/File list/Date").strip(" ");
        hash->set_value(bim::DOCUMENT_DATETIME, bim::DateTime::from_string(date, "%H:%M:%S %p %a %b %d %Y").to_string_iso8601());
    }

    // channel names
    for (size_t i = 0; i < par->channels; ++i) {
        hash->set_value(xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i) + bim::CHANNEL_INFO_NAME, par->imgs.at(i).data_type);
    }

    return 0;
}


//****************************************************************************
// INTERNAL STRUCTURES
//****************************************************************************

void addDefaultKeyTag(const char *token, NanoscopeParams *par) {
    par->metaText.append("[");
    par->metaText.append(token + 2);
    par->metaText.append("]\n");
}

void addDefaultTag(const char *token, NanoscopeParams *par) {
    par->metaText.append(token + 1);
    par->metaText.append("\n");
}

void addImgKeyTag(const char *token, NanoscopeParams *par) {
    par->imgs.rbegin()->metaText.append("[");
    par->imgs.rbegin()->metaText.append(token + 2);
    par->imgs.rbegin()->metaText.append("]\n");
}

void addImgTag(const char *token, NanoscopeParams *par) {
    par->imgs.rbegin()->metaText.append(token + 1);
    par->imgs.rbegin()->metaText.append("\n");
}

void nanoscopeGetImageInfo(FormatHandle *fmtHndl) {
    bool inimage = false;

    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    NanoscopeParams *par = (NanoscopeParams *)fmtHndl->internalParams;

    ImageInfo *info = &par->i;
    info->samples = 1;
    info->depth = 16;
    info->pixelType = bim::DataFormat::FMT_SIGNED;
    info->number_pages = 1;

    if (fmtHndl->stream == NULL) return;

    TokenReader tr(fmtHndl);
    tr.readNextLine();

    while (!tr.isEOLines()) {
        if (tr.isImageTag()) {
            // now add new image
            par->imgs.push_back(NanoscopeImg());
            addImgKeyTag(tr.line_c_str(), par);
            inimage = true;
            tr.readNextLine();
            continue;
        }

        if (!inimage) { // if capturing default parameters
            if (tr.isKeyTag())
                addDefaultKeyTag(tr.line_c_str(), par); // add key meta data tag
            else
                addDefaultTag(tr.line_c_str(), par); // add simple meta data tag
        } else {                                         // if capturing image parameters
            if (tr.isTag("\\Data offset:"))
                par->imgs.rbegin()->data_offset = tr.readParamInt("\\Data offset:");
            else if (tr.isTag("\\Samps/line:"))
                par->imgs.rbegin()->width = tr.readParamInt("\\Samps/line:");
            else if (tr.isTag("\\Number of lines:"))
                par->imgs.rbegin()->height = tr.readParamInt("\\Number of lines:");
            else
                addImgTag(tr.line_c_str(), par);


            // parse metadata
            //\Scan size: 15 15 ~m - microns
            //\Scan size: 353.381 353.381 nm - nanometers
            if (tr.isTag("\\Scan size:")) {
                //tr.readTwoParamDouble ( "\\Scan size:", &par->imgs.rbegin()->xR, &par->imgs.rbegin()->yR );

                bim::xstring str;
                tr.readTwoDoubleRemString("\\Scan size:", par->imgs.rbegin()->xR, par->imgs.rbegin()->yR, str);

                // if size is in nm then convert to um
                if (strncmp(str.c_str(), "nm", 2) == 0) {
                    par->imgs.rbegin()->xR /= 1000.0;
                    par->imgs.rbegin()->yR /= 1000.0;
                }
            }

            //\@2:Image Data: S [Height] "Height"
            if (tr.isTag("\\@2:Image Data:")) {
                par->imgs.rbegin()->data_type = tr.readImageDataString("\\@2:Image Data:");
            }
        }

        tr.readNextLine();
    } // while

    // walk trough image list and verify it's consistency
    NanoImgVector::iterator imgit = par->imgs.begin();

    while (imgit < par->imgs.end()) {
        if ((imgit->width <= 0) || (imgit->height <= 0) || (imgit->data_offset <= 0)) {
            // before removing add metadata to image pool
            par->metaText.append(imgit->metaText.c_str());
            par->imgs.erase(imgit);
        } else
            ++imgit;
    }

    NanoscopeImg *nimg = &par->imgs[0];

    // find number of channels
    // should we use meta?
    //Nanoscope/Ciao scan list/Color table: 2 
    par->channels = 1;
    xstring first_data_type = nimg->data_type;
    for (size_t i = 1; i < par->imgs.size(); ++i) {
        if (par->imgs.at(i).data_type != first_data_type) {
            par->channels += 1;
        } else {
            break;
        }
    }

    // now finalize info
    size_t img_n = fmtHndl->pageNumber * par->channels;
    if (img_n > 0 && img_n < par->imgs.size())
        nimg = &par->imgs[img_n];

    par->num_pages = par->imgs.size() / par->channels;
    info->width = nimg->width;
    info->height = nimg->height;
    info->samples = (bim::uint32)par->channels;
    info->number_pages = par->num_pages;
    info->number_z = par->num_pages;
    info->imageMode = info->samples == 1 ? bim::ImageModes::IM_GRAYSCALE : bim::ImageModes::IM_MULTI;
}

//****************************************************************************
// FORMAT DEMANDED FUNTIONS
//****************************************************************************


//----------------------------------------------------------------------------
// PARAMETERS, INITS
//----------------------------------------------------------------------------

int nanoscopeValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    if (length < BIM_NANOSCOPE_MAGIC_SIZE) return -1;
    if (memcmp(magic, nanoscopeMagic, BIM_NANOSCOPE_MAGIC_SIZE) == 0) return 0;
    if (memcmp(magic, nanoscopeMagicF, BIM_NANOSCOPE_MAGIC_SIZE) == 0) return 0;
    return -1;
}

FormatHandle nanoscopeAquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void nanoscopeCloseImageProc(bim::FormatHandle *fmtHndl);
void nanoscopeReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    nanoscopeCloseImageProc(fmtHndl);
}


//----------------------------------------------------------------------------
// OPEN/CLOSE
//----------------------------------------------------------------------------
void nanoscopeCloseImageProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    xclose(fmtHndl);
    if (fmtHndl->internalParams != NULL) {
        NanoscopeParams *par = (NanoscopeParams *)fmtHndl->internalParams;
        delete par;
    }
    fmtHndl->internalParams = NULL;
}

bim::uint nanoscopeOpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams != NULL) nanoscopeCloseImageProc(fmtHndl);
    fmtHndl->internalParams = (void *)new NanoscopeParams();

    fmtHndl->io_mode = io_mode;
    if (io_mode == bim::ImageIOModes::IO_READ) {
        xopen(fmtHndl);
        if (!fmtHndl->stream) {
            nanoscopeCloseImageProc(fmtHndl);
            return 1;
        }
        nanoscopeGetImageInfo(fmtHndl);
        return 0;
    }
    return 1;
}

//----------------------------------------------------------------------------
// INFO for OPEN image
//----------------------------------------------------------------------------

bim::uint nanoscopeGetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    NanoscopeParams *par = (NanoscopeParams *)fmtHndl->internalParams;
    return (bim::uint)par->num_pages;
}


ImageInfo nanoscopeGetImageInfoProc(FormatHandle *fmtHndl, bim::uint) {
    if (fmtHndl == NULL) return ImageInfo();
    NanoscopeParams *par = (NanoscopeParams *)fmtHndl->internalParams;
    return par->i;
}

//----------------------------------------------------------------------------
// READ/WRITE
//----------------------------------------------------------------------------

bim::uint nanoscopeReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->stream == NULL) return 1;
    fmtHndl->pageNumber = page;
    return read_nanoscope_image(fmtHndl);
}

bim::uint nanoscopeWriteImageProc(FormatHandle *) {
    return 1;
}





//****************************************************************************
// LineReader
//****************************************************************************

constexpr unsigned int BIM_LINE_BUF_SIZE = 2048;

LineReader::LineReader(FormatHandle *newHndl) {
    fmtHndl = newHndl;
    this->buf.resize(BIM_LINE_BUF_SIZE);

    if (fmtHndl == NULL) return;
    if (fmtHndl->stream == NULL) return;
    if (xseek(fmtHndl, 0, SEEK_SET) != 0) return;
    eolines = false;
    loadBuff();
}

LineReader::~LineReader() {
}

bool LineReader::loadBuff() {
    bufsize = xread(fmtHndl, &buf[0], 1, BIM_LINE_BUF_SIZE);
    bufpos = 0;
    if (bufsize <= 0) return false;
    return true;
}

bool LineReader::readNextLine() {
    if (eolines) return false;
    prline = "";

    while (true) {
        if (bufpos >= bufsize) loadBuff();
        if (bufpos >= bufsize) {
            eolines = true;
            break;
        }
        if ((buf[bufpos] == 0xA) || (buf[bufpos] == 0xD) || (buf[bufpos] == 0x1A)) break;
        prline += buf[bufpos];
        ++bufpos;
    }

    if (buf[bufpos] == 0xA) ++bufpos;
    if (buf[bufpos] == 0xD) bufpos += 2;
    if (bufpos >= bufsize) loadBuff();
    if (bufpos >= bufsize) eolines = true;
    if (buf[bufpos] == 0x1A) eolines = true;

    return true;
}

bool LineReader::isEOLines() {
    return eolines;
}

bim::xstring LineReader::line() {
    return prline;
}

const char *LineReader::line_c_str() {
    return prline.c_str();
}

//****************************************************************************
// TokenReader
//****************************************************************************

bool TokenReader::isKeyTag() {
    if ((prline[0] == '\\') && (prline[1] == '*')) return true;
    return false;
}

bool TokenReader::isImageTag() {
    if (isKeyTag()) {
        //if (prline.compare("\\*Ciao force image list") == 0) return false;
        if (prline.contains("image list"))
            return true;
    }

    return false;
}

bool TokenReader::isEndTag() {
    if (prline.compare("\\*File list end") == 0) return true;
    return false;
}


bool TokenReader::compareTag(const bim::xstring &tag) {
    if (prline.compare(tag) == 0) return true;
    return false;
}

bool TokenReader::isTag(const bim::xstring &tag) {
    if (prline.contains(tag)) return true;
    return false;
}

int TokenReader::readParamInt(const bim::xstring &tag) {
    xstring v = prline.right(tag + " ");
    return v.toInt();
}

void TokenReader::readTwoParamDouble(const bim::xstring &tag, double &p1, double &p2) {
    xstring v = prline.right(tag + " ");
    std::vector<double> vv = v.splitDouble(" ");
    if (vv.size() > 0) p1 = vv[0];
    if (vv.size() > 1) p2 = vv[1];
}

void TokenReader::readTwoDoubleRemString(const bim::xstring &tag, double &p1, double &p2, bim::xstring &str) {
    xstring v = prline.right(tag + " ");
    std::vector<xstring> vv = v.split(" ");
    
    if (vv.size() > 0) p1 = vv[0].toDouble();
    if (vv.size() > 1) p2 = vv[1].toDouble();
    if (vv.size() > 2) str = vv[2];
}

std::string TokenReader::readImageDataString(const bim::xstring &tag) {
    return prline.section("\"", "\"");
}


TokenReader::TokenReader(FormatHandle *newHndl): LineReader(newHndl) {
}


//****************************************************************************
// EXPORTED 
//****************************************************************************

FormatItem nanoscopeItems[1] = {{ 
    "NANOSCOPE",        // short name, no spaces
    "AFM: NanoScope II/III", // Long format name
    "nan",              // pipe "|" separated supported extension list
    1,                  //canRead;      // 0 - NO, 1 - YES
    0,                  //canWrite;     // 0 - NO, 1 - YES
    1,                  //canReadMeta;  // 0 - NO, 1 - YES
    0,                  //canWriteMeta; // 0 - NO, 1 - YES
    0,                  //canWriteMultiPage;   // 0 - NO, 1 - YES
    { 0, 0, 0, 1, 1, 16, 16, 1 } // constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
}};

FormatHeader nanoscopeHeader = {

    sizeof(FormatHeader),
    "2.0.0",
    "NANOSCOPE",
    "NANOSCOPE",

    12,                       // 0 or more, specify number of bytes needed to identify the file
    { 1, 1, nanoscopeItems }, //dimJpegSupported,

    nanoscopeValidateFormatProc,
    // begin
    nanoscopeAquireFormatProc, //AquireFormatProc
    // end
    nanoscopeReleaseFormatProc, //ReleaseFormatProc

    // params
    NULL, //AquireIntParamsProc
    NULL, //LoadFormatParamsProc
    NULL, //StoreFormatParamsProc

    // image begin
    nanoscopeOpenImageProc,  //OpenImageProc
    nanoscopeCloseImageProc, //CloseImageProc

    // info
    nanoscopeGetNumPagesProc,  //GetNumPagesProc
    nanoscopeGetImageInfoProc, //GetImageInfoProc

    // read/write
    nanoscopeReadImageProc, //ReadImageProc
    NULL,                   //WriteImageProc
    NULL,                   //ReadImageTileProc
    NULL,                   //WriteImageTileProc
    NULL,                   //ReadImageLevelProc
    NULL,                   //WriteImageLevelProc
    NULL,                   //ReadImageRegionProc
    NULL,                   //WriteImageRegionProc
    nanoscope_append_metadata, //AppendMetaDataProc
};

extern "C" {

FormatHeader *nanoscopeGetFormatHeader(void) {
    return &nanoscopeHeader;
}

} // extern C
