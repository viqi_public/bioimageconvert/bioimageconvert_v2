/*****************************************************************************
  OME XML file format (Open Microscopy Environment)
  Copyright (c) 2005 Center for Bio-Image Informatics, UCSB
  Copyright (c) 2021 ViQi Inc

  Author: Dima Fedorov <dima@viqi.org>

  History:
    11/21/2005 15:43 - First creation
    2021-11-09 15:50 - cleaned-up

  ver : 2
*****************************************************************************/

#ifndef BIM_OME_FORMAT_H
#define BIM_OME_FORMAT_H

#include <cstdio>
#include <string>
#include <vector>

#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>

// DLL EXPORT FUNCTION
extern "C" {
bim::FormatHeader *omeGetFormatHeader(void);
}

namespace bim {

/*****************************************************************************
  OME XML file format - quick reference

  OME/src/xml/schemas/BinaryFile.xsd

*****************************************************************************/

#define BIM_OME_MAGIC_SIZE 6

const char omeMagic[BIM_OME_MAGIC_SIZE] = "<?xml";

class OmeParams {
public:
    OmeParams();
    ~OmeParams();

    ImageInfo i;
};

} // namespace bim

#endif // BIM_OME_FORMAT_H
