/*****************************************************************************
STK file format

Author: Dmitry Fedorov <www.dimin.net> <dima@dimin.net>
Copyright (C) 2020 ViQi Inc

History:
04/20/2004 23:33 - First creation
2020-03-27 23:46 - Complete rewrite and support for new formats

ver: 2
*****************************************************************************/

#ifndef BIM_STK_FORMAT_H
#define BIM_STK_FORMAT_H

#include <string>
#include <vector>

#include <tag_map.h>

#include "bim_tiff_format.h"

namespace bim {

#define BIM_STK_AutoScale 0
#define BIM_STK_MinScale 1
#define BIM_STK_MaxScale 2
#define BIM_STK_SpatialCalibration 3
#define BIM_STK_XCalibration 4
#define BIM_STK_YCalibration 5
#define BIM_STK_CalibrationUnits 6
#define BIM_STK_Name 7
#define BIM_STK_ThreshState 8
#define BIM_STK_ThreshStateRed_check 9

#define BIM_STK_ThreshStateRed 10
#define BIM_STK_ThreshStateGreen 11
#define BIM_STK_ThreshStateBlue 12
#define BIM_STK_ThreshStateLo 13
#define BIM_STK_ThreshStateHi 14
#define BIM_STK_Zoom 15
#define BIM_STK_CreateTime 16
#define BIM_STK_LastSavedTime 17
#define BIM_STK_currentBuffer 18
#define BIM_STK_grayFit 19
#define BIM_STK_grayPointCount 20
#define BIM_STK_grayX 21
#define BIM_STK_grayY 22
#define BIM_STK_grayMin 23
#define BIM_STK_grayMax 24
#define BIM_STK_grayUnitName 25
#define BIM_STK_StandardLUT 26
#define BIM_STK_wavelength 27
#define BIM_STK_StagePosition 28
#define BIM_STK_CameraChipOffset 29
#define BIM_STK_OverlayMask 30
#define BIM_STK_OverlayCompress 31
#define BIM_STK_Overlay 32
#define BIM_STK_SpecialOverlayMask 33
#define BIM_STK_SpecialOverlayCompress 34
#define BIM_STK_SpecialOverlay 35
#define BIM_STK_ImageProperty 36
#define BIM_STK_StageLabel 37
#define BIM_STK_AutoScaleLoInfo 38
#define BIM_STK_AutoScaleHiInfo 39
#define BIM_STK_AbsoluteZ 40
#define BIM_STK_AbsoluteZValid 41
#define BIM_STK_Gamma 42
#define BIM_STK_GammaRed 43
#define BIM_STK_GammaGreen 44
#define BIM_STK_GammaBlue 45
#define BIM_STK_CameraBin 46
#define BIM_STK_NewLUT 47
#define BIM_STK_PlaneProperty 49

#define BIM_STK_ImagePropertyEx 48
#define BIM_STK_UserLutTable 50
#define BIM_STK_RedAutoScaleInfo 51
#define BIM_STK_RedAutoScaleLoInfo 52
#define BIM_STK_RedAutoScaleHiInfo 53
#define BIM_STK_RedMinScaleInfo 54
#define BIM_STK_RedMaxScaleInfo 55
#define BIM_STK_GreenAutoScaleInfo 56
#define BIM_STK_GreenAutoScaleLoInfo 57
#define BIM_STK_GreenAutoScaleHiInfo 58
#define BIM_STK_GreenMinScaleInfo 59
#define BIM_STK_GreenMaxScaleInfo 60
#define BIM_STK_BlueAutoScaleInfo 61
#define BIM_STK_BlueAutoScaleLoInfo 62
#define BIM_STK_BlueAutoScaleHiInfo 63
#define BIM_STK_BlueMinScaleInfo 64
#define BIM_STK_BlueMaxScaleInfo 65
#define BIM_STK_OverlayPlaneColor 66


#pragma pack(push, 1)
typedef struct StkRational {
    int32 num;
    int32 den;
} StkRational;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct StkDateTime {
    int32 date;
    int32 time;
} StkDateTime;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct StkEntryUIC2 {
    StkRational zDistance;
    int32 creationDate;
    int32 creationTime;
    int32 modificationDate;
    int32 modificationTime;
} StkEntryUIC2;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct StkPosition {
    StkRational x;
    StkRational y;
} StagePosition;
#pragma pack(pop)


class StkInfo {
public:
    StkInfo();
    ~StkInfo();

    tiff_strp_t strips_per_plane;              // strips per plane
    std::vector<tiff_offs_t> strip_offsets;    // offsets of each strip
    std::vector<tiff_bcnt_t> strip_bytecounts; // offsets of each strip

    int N = 1;
    bim::TagMap tags;

public:
    void init(int32 N = 1);
};

} // namespace bim

#endif // BIM_STK_FORMAT_H
