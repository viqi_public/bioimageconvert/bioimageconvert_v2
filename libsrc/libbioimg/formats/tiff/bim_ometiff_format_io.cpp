/*****************************************************************************
  OME-TIFF definitions
  Copyright (c) 2009, Center for Bio-Image Informatics, UCSB

  Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

  History:
    2009-07-09 12:01 - First creation

  Ver : 1
*****************************************************************************/

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>

#include <Jzon.h>
#include <pugixml.hpp>

#include <bim_image.h>
#include <bim_img_format_utils.h>
#include <bim_metatags.h>
#include <bim_ome_types.h>
#include <tag_map.h>
#include <xstring.h>

#include "bim_tiff_format.h"
#include "bim_tiny_tiff.h"
#include "xtiffio.h"

using namespace bim;

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

unsigned int tiffGetNumberOfPages(bim::TiffParams *par);
int tiff_update_subifd_next_pointer(TIFF *tif, bim::uint64 dir_offset, bim::uint64 to_offset);
void detectTiffPyramid(bim::TiffParams *tiffParams);
int read_tiff_image_level(bim::FormatHandle *fmtHndl, bim::TiffParams *tifParams, bim::uint page, bim::uint level);
int read_tiff_image_tile(bim::FormatHandle *fmtHndl, bim::TiffParams *tifParams, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level);
void pyramid_append_metadata(bim::FormatHandle *fmtHndl, bim::TagMap *hash);
void generic_append_metadata(FormatHandle *fmtHndl, TagMap *hash);
void generic_write_metadata(FormatHandle *fmtHndl, TagMap *hash);

//----------------------------------------------------------------------------
// OME-TIFF MISC FUNCTIONS
//----------------------------------------------------------------------------

bool omeTiffIsValid(bim::TiffParams *par) {
    if (!par) return false;
    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return false;
    if (!ifd->tagPresent(TIFFTAG_IMAGEDESCRIPTION)) return false;
    bim::xstring tag_270 = ifd->readTagString(TIFFTAG_IMAGEDESCRIPTION);
    if (tag_270.contains("<OME") && tag_270.contains("<Image") && tag_270.contains("<Pixels")) return true;
    return false;
}

int omeTiffGetInfo(bim::TiffParams *par) {
    if (!par) return 1;
    if (!par->tiff) return 1;
    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return false;

    TIFF *tif = par->tiff;
    bim::ImageInfo *info = &par->info;
    bim::OMETiffInfo *ome = &par->omeTiffInfo;

    // Read OME-XML from image description tag
    if (!ifd->tagPresent(TIFFTAG_IMAGEDESCRIPTION)) return false;
    bim::xstring tag_270 = ifd->readTagString(TIFFTAG_IMAGEDESCRIPTION);
    if (tag_270.size() <= 0) return false;

    //---------------------------------------------------------------
    // image geometry
    //---------------------------------------------------------------
    bim::xstring tag_pixels = tag_270.section("<Pixels", ">");
    if (tag_pixels.size() <= 0) return false;
    tag_pixels = ometiff_normalize_xml_spaces(tag_pixels);

    //info->number_pages = tiffGetNumberOfPages( par ); // dima: enumerating all pages in an ome-tiff file with many pages is very slow
    //bim::uint64 real_tiff_pages = info->number_pages;
    ome->channels = tag_pixels.section(" SizeC=\"", "\"").toInt(1);
    ome->number_t = tag_pixels.section(" SizeT=\"", "\"").toInt(1);
    ome->number_z = tag_pixels.section(" SizeZ=\"", "\"").toInt(1);
    ome->width = tag_pixels.section(" SizeX=\"", "\"").toInt(1);
    ome->height = tag_pixels.section(" SizeY=\"", "\"").toInt(1);
    ome->bim_order = tag_pixels.section(" DimensionOrder=\"", "\"");
    ome->pages = ome->number_t * ome->number_z;
    bim::uint64 real_tiff_pages = ome->pages * ome->channels; // dima: enumerating all pages in an ome-tiff file with many pages is very slow

    if (ifd->tagPresent(TIFFTAG_SOFTWARE)) {
        bim::xstring tag_305 = ifd->readTagString(TIFFTAG_SOFTWARE);
        if (tag_305.toLowerCase().startsWith("fluidigm")) {
            ome->variant = bim::otvFLUIDIGM;
            // read the real number of pages
            real_tiff_pages = TIFFNumberOfDirectories(tif);
            //ome->pages = ;
            ome->channels = (int)real_tiff_pages;

            // this format will require reading and parsing xml form every page individually
        }
    }

    //info->width    = ome->width;    // tiff image will have the most correct width and height parameters
    //info->height   = ome->height;   // tiff image will have the most correct width and height parameters
    info->samples = ome->channels; // no way to estimate number of channels, they come as spearate pages
    info->number_pages = ome->pages;
    info->number_t = ome->number_t;
    info->number_z = ome->number_z;

    if (info->samples > 1)
        info->imageMode = bim::ImageModes::IM_MULTI;
    else
        info->imageMode = bim::ImageModes::IM_GRAYSCALE;

    bim::uint16 bitspersample = 1;
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
    info->depth = bitspersample;

    //---------------------------------------------------------------
    // fix for the case of multiple files, with many channels defined
    // but not provided in the particular TIFF
    //---------------------------------------------------------------
    bim::uint16 samplesperpixel = 1;
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
    if (samplesperpixel == 1) {
        // test if number of channels is correct given the number of pages
        if (real_tiff_pages < ome->pages * ome->channels) {
            info->samples = 1;
            ome->channels = 1;
        }

        if (real_tiff_pages < ome->pages) {
            ome->pages = real_tiff_pages;
            info->number_pages = real_tiff_pages;
        }

        if (info->number_t * info->number_z < ome->pages) {
            if (info->number_z > info->number_t)
                info->number_z = ome->pages;
            else
                info->number_t = ome->pages;
        }
    }

    //---------------------------------------------------------------
    // fix for the case of standard TIFF file with incorrect OME header
    //---------------------------------------------------------------
    if (samplesperpixel > 1 && samplesperpixel != ome->channels) {
        // this format will require reading like a normal TIFF image
        
        //ome->variant = bim::otvTIFFwithOMEHeader;
        //ome->pages = 1;
        //info->number_pages = 1;
        //real_tiff_pages = TIFFNumberOfDirectories32(tif);
        //ome->channels = samplesperpixel;
        // otvTIFFwithOMEHeader
        // info->imageMode = bim::ImageModes::IM_GRAYSCALE;
        // info->imageMode = bim::ImageModes::IM_GRAYSCALE;
    }


    //---------------------------------------------------------------
    // define dims
    //---------------------------------------------------------------
    if (info->number_z > 1) {
        info->number_dims = 4;
        //info->dimensions[3].dim = bim::DIM_Z;
    }
    if (info->number_t > 1) {
        info->number_dims = 4;
        //info->dimensions[3].dim = bim::DIM_T;
    }
    if (info->number_z > 1 && info->number_t > 1) {
        info->number_dims = 5;
        //info->dimensions[3].dim = bim::DIM_Z;
        //info->dimensions[4].dim = bim::DIM_T;
    }

    //--------------------------------------------------------------------
    // pixel resolution
    //--------------------------------------------------------------------

    ome->pixel_resolution[0] = tag_pixels.section(" PhysicalSizeX=\"", "\"").toDouble(0.0);
    ome->pixel_resolution[1] = tag_pixels.section(" PhysicalSizeY=\"", "\"").toDouble(0.0);
    ome->pixel_resolution[2] = tag_pixels.section(" PhysicalSizeZ=\"", "\"").toDouble(0.0);
    ome->pixel_resolution[3] = tag_pixels.section(" TimeIncrement=\"", "\"").toDouble(0.0);

    // Fix for old style OME-TIFF images
    bim::xstring tag_image = tag_270.section("<Image", ">");
    if (tag_image.size() > 0) {
        tag_image = ometiff_normalize_xml_spaces(tag_image);

        if (ome->pixel_resolution[0] == 0.0)
            ome->pixel_resolution[0] = tag_image.section(" PixelSizeX=\"", "\"").toDouble(0.0);
        if (ome->pixel_resolution[1] == 0.0)
            ome->pixel_resolution[1] = tag_image.section(" PixelSizeY=\"", "\"").toDouble(0.0);
        if (ome->pixel_resolution[2] == 0.0)
            ome->pixel_resolution[2] = tag_image.section(" PixelSizeZ=\"", "\"").toDouble(0.0);
        if (ome->pixel_resolution[3] == 0.0)
            ome->pixel_resolution[3] = tag_image.section(" PixelSizeT=\"", "\"").toDouble(0.0);
    }

    return 0;
}

void omeTiffGetCurrentPageInfo(bim::TiffParams *par) {
    if (!par) return;
    bim::ImageInfo *info = &par->info;
    bim::OMETiffInfo *ome = &par->omeTiffInfo;

    info->samples = ome->channels;
    info->number_pages = ome->pages;

    info->resUnits = bim::ResolutionUnits::RES_um;
    if (ome->pixel_resolution[0] > 0)
        info->xRes = ome->pixel_resolution[0];
    if (ome->pixel_resolution[1] > 0)
        info->yRes = ome->pixel_resolution[1];
}


//----------------------------------------------------------------------------
// READ/WRITE FUNCTIONS
//----------------------------------------------------------------------------

bim::uint64 computeTiffDirectory(bim::FormatHandle *fmtHndl, size_t page, size_t sample) {
    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->info;
    bim::OMETiffInfo *ome = &par->omeTiffInfo;

    bim::uint64 nz = std::max<bim::uint64>(info->number_z, 1);
    bim::uint64 nt = std::max<bim::uint64>(info->number_t, 1);
    bim::uint64 nc = std::max<bim::uint64>(info->samples, 1);

    //XYLZT file will be named as �xxx_C00mL00qZ00nT00p.tif�
    bim::uint64 c = sample;
    bim::uint64 l = (page / nz) / nt;
    bim::uint64 t = (page - l * nt * nz) / nz;
    bim::uint64 z = page - nt * l - nz * t;

    // compute directory position based on order: XYCZT
    bim::uint64 dirNum = (t * nz + z) * nc + c;

    // Possible orders: XYCZT XYCTZ XYZCT XYZTC XYTCZ XYTZC
    if (ome->bim_order.startsWith("XYCZ")) dirNum = (t * nz + z) * nc + c;
    if (ome->bim_order.startsWith("XYCT")) dirNum = (z * nt + t) * nc + c;
    if (ome->bim_order.startsWith("XYZC")) dirNum = (t * nc + c) * nz + z;
    if (ome->bim_order.startsWith("XYZT")) dirNum = (c * nt + t) * nz + z;
    if (ome->bim_order.startsWith("XYTC")) dirNum = (z * nc + c) * nt + t;
    if (ome->bim_order.startsWith("XYTZ")) dirNum = (t * nz + z) * nt + t;

    return dirNum;
}

// this is here due to some OME-TIFF do not conform with the standard and come with all channels in the same IFD
bim::uint64 computeTiffDirectoryNoChannels(bim::FormatHandle *fmtHndl, size_t page, size_t sample) {
    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->info;
    bim::OMETiffInfo *ome = &par->omeTiffInfo;

    bim::uint64 nz = std::max<bim::uint64>(info->number_z, 1);
    bim::uint64 nt = std::max<bim::uint64>(info->number_t, 1);
    bim::uint64 nc = 1;

    //XYLZT file will be named as �xxx_C00mL00qZ00nT00p.tif�
    bim::uint64 c = 0;
    bim::uint64 l = (page / nz) / nt;
    bim::uint64 t = (page - l * nt * nz) / nz;
    bim::uint64 z = page - nt * l - nz * t;

    // compute directory position based on order: XYCZT
    bim::uint64 dirNum = (t * nz + z) * nc + c;

    // Possible orders: XYCZT XYCTZ XYZCT XYZTC XYTCZ XYTZC
    if (ome->bim_order.startsWith("XYCZ")) dirNum = (t * nz + z) * nc + c;
    if (ome->bim_order.startsWith("XYCT")) dirNum = (z * nt + t) * nc + c;
    if (ome->bim_order.startsWith("XYZC")) dirNum = (t * nc + c) * nz + z;
    if (ome->bim_order.startsWith("XYZT")) dirNum = (c * nt + t) * nz + z;
    if (ome->bim_order.startsWith("XYTC")) dirNum = (z * nc + c) * nt + t;
    if (ome->bim_order.startsWith("XYTZ")) dirNum = (t * nz + z) * nt + t;

    return dirNum;
}

bool ometiff_read_striped(TIFF *tif, bim::ImageBitmap *img, bim::FormatHandle *fmtHndl, const size_t &sample) {
    size_t lineSize = getLineSizeInBytes(img);
    bim::uchar *p = (bim::uchar *)img->bits[sample];
    for (bim::uint64 y = 0; y < img->i.height; y++) {
        xprogress(fmtHndl, y * (sample + 1), img->i.height * img->i.samples, "Reading OME-TIFF");
        if (xtestAbort(fmtHndl) == 1) return false;
        if (TIFFReadScanline(tif, p, (uint32_t)y, 0) < 0) return false;
        p += lineSize;
    } // for y
    return true;
}

bool ometiff_read_tiled(TIFF *tif, bim::ImageBitmap *img, bim::FormatHandle *fmtHndl, const size_t &sample) {
    if (!tif || !img) return false;

    size_t lineSize = getLineSizeInBytes(img);
    size_t bpp = (size_t)ceil((double)img->i.depth / 8.0);
    bim::uint32 columns=0, rows=0;
    TIFFGetField(tif, TIFFTAG_TILEWIDTH, &columns);
    TIFFGetField(tif, TIFFTAG_TILELENGTH, &rows);

    std::vector<bim::uchar> buffer(TIFFTileSize64(tif));
    bim::uchar *buf = &buffer[0];

    for (bim::uint y = 0; y < img->i.height; y += rows) {
        xprogress(fmtHndl, y, img->i.height, "Reading TIFF");
        if (xtestAbort(fmtHndl) == 1) return false;

        bim::uint tile_height = (img->i.height - y >= rows) ? rows : (bim::uint)img->i.height - y;
        for (bim::uint x = 0; x < (bim::uint)img->i.width; x += columns) {
            bim::uint tile_width = (img->i.width - x < columns) ? (bim::uint)img->i.width - x : (bim::uint)columns;
            if (TIFFReadTile(tif, buf, x, y, 0, 0) < 0) return false;
            for (bim::uint yi = 0; yi < tile_height; yi++) {
                bim::uchar *p = (bim::uchar *)img->bits[sample] + (lineSize * (y + yi));
                _TIFFmemcpy(p + (x * bpp), buf + (yi * columns * bpp), tile_width * bpp);
            }
        } // for x
    }     // for y
    return true;
}


bim::uint omeTiffReadPlane(bim::FormatHandle *fmtHndl, bim::TiffParams *par, size_t plane) {
    if (!par) return 1;
    if (!par->tiff) return 1;
    if (par->subType != bim::tstOmeTiff && par->subType != bim::tstOmeBigTiff) return 1;
    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return 1;
    TIFF *tif = par->tiff;
    if (!tif) return false;

    bim::ImageInfo *info = &par->info;
    bim::OMETiffInfo *ome = &par->omeTiffInfo;

    //--------------------------------------------------------------------
    // read image parameters
    //--------------------------------------------------------------------
    bim::uint64 tiff_page = computeTiffDirectory(fmtHndl, fmtHndl->pageNumber, 0);
    bim::ImageBitmap *img = fmtHndl->image;
    TIFFSetDirectory(tif, (tdir_t)tiff_page);

    bim::uint16 bitspersample = 1;
    bim::uint32 height = 0;
    bim::uint32 width = 0;
    bim::uint16 samplesperpixel = 1;
    bim::uint16 sampleformat = 1;

    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
    TIFFGetField(tif, TIFFTAG_SAMPLEFORMAT, &sampleformat);

    if (samplesperpixel > 1) {
        tiff_page = computeTiffDirectoryNoChannels(fmtHndl, fmtHndl->pageNumber, 0);
        TIFFSetDirectory(tif, (tdir_t)tiff_page);
        return 2;
    }

    if (img->i.depth != bitspersample || img->i.width != width || img->i.height != height) {
        //info->samples = ome->ch;
        info->depth = bitspersample;
        info->width = width;
        info->height = height;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
        if (sampleformat == SAMPLEFORMAT_INT)
            info->pixelType = bim::DataFormat::FMT_SIGNED;
        else if (sampleformat == SAMPLEFORMAT_IEEEFP)
            info->pixelType = bim::DataFormat::FMT_FLOAT;

        if (allocImg(fmtHndl, info, img) != 0) return 1;
    }


    //--------------------------------------------------------------------
    // read data
    //--------------------------------------------------------------------
    for (unsigned int sample = 0; sample < (unsigned int)info->samples; ++sample) {
        tiff_page = computeTiffDirectory(fmtHndl, fmtHndl->pageNumber, sample);
        TIFFSetDirectory(tif, (tdir_t)tiff_page);
        if (!TIFFIsTiled(tif)) {
            if (!ometiff_read_striped(tif, img, fmtHndl, sample)) return 1;
        } else {
            if (!ometiff_read_tiled(tif, img, fmtHndl, sample)) return 1;
        }
    } // for sample

    //TIFFSetDirectory64(tif, fmtHndl->pageNumber);
    return 0;
}


//--------------------------------------------------------------------------------------------
// Levels and Tiles functions
//--------------------------------------------------------------------------------------------

int ometiff_read_image_level(bim::FormatHandle *fmtHndl, bim::TiffParams *par, bim::uint page, bim::uint level) {
    if (!par) return 1;
    if (!par->tiff) return 1;
    if (par->subType != bim::tstOmeTiff && par->subType != bim::tstOmeBigTiff) return 1;
    TIFF *tif = par->tiff;
    if (!tif) return 1;

    bim::ImageInfo *info = &par->info;
    bim::OMETiffInfo *ome = &par->omeTiffInfo;
    bim::PyramidInfo *pyramid = &par->pyramid;
    bim::ImageBitmap *img = fmtHndl->image;


    //--------------------------------------------------------------------
    // detect levels
    //--------------------------------------------------------------------

    bim::uint64 tiff_page = computeTiffDirectory(fmtHndl, fmtHndl->pageNumber, 0);
    TIFFSetDirectory(tif, (tdir_t)tiff_page);
    detectTiffPyramid(par);
    if (pyramid->number_levels > 0 && pyramid->number_levels <= level) return 1;
    bim::uint64 subdiroffset = pyramid->directory_offsets[level];
    if (TIFFSetSubDirectory(tif, subdiroffset) == 0) return 1;

    //--------------------------------------------------------------------
    // read image parameters
    //--------------------------------------------------------------------

    bim::uint16 bitspersample = 1;
    bim::uint32 height = 0;
    bim::uint32 width = 0;
    bim::uint16 samplesperpixel = 1;
    bim::uint16 sampleformat = 1;

    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
    TIFFGetField(tif, TIFFTAG_SAMPLEFORMAT, &sampleformat);

    // fix for ome-tiff files with all channels within one image
    if (samplesperpixel > 1) {
        tiff_page = computeTiffDirectoryNoChannels(fmtHndl, fmtHndl->pageNumber, 0);
        TIFFSetDirectory(tif, (tdir_t)tiff_page);
        detectTiffPyramid(par);
        return read_tiff_image_level(fmtHndl, par, page, level);
    }

    if (img->i.depth != bitspersample || img->i.width != width || img->i.height != height) {
        //info->samples = ome->channels;
        info->depth = bitspersample;
        info->width = width;
        info->height = height;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
        if (sampleformat == SAMPLEFORMAT_INT)
            info->pixelType = bim::DataFormat::FMT_SIGNED;
        else if (sampleformat == SAMPLEFORMAT_IEEEFP)
            info->pixelType = bim::DataFormat::FMT_FLOAT;

        if (allocImg(fmtHndl, info, img) != 0) return 1;
    }


    //--------------------------------------------------------------------
    // read data
    //--------------------------------------------------------------------
    for (bim::uint64 sample = 0; sample < info->samples; ++sample) {
        tiff_page = computeTiffDirectory(fmtHndl, fmtHndl->pageNumber, sample);
        if (TIFFSetDirectory(tif, (tdir_t)tiff_page) == 0) return 1;
        detectTiffPyramid(par);
        if (pyramid->number_levels > 0 && pyramid->number_levels <= level) return 1;
        bim::uint64 subdiroffset = pyramid->directory_offsets[level];
        if (TIFFSetSubDirectory(tif, subdiroffset) == 0) return 1;

        if (!TIFFIsTiled(tif)) {
            if (!ometiff_read_striped(tif, img, fmtHndl, sample)) return 1;
        } else {
            if (!ometiff_read_tiled(tif, img, fmtHndl, sample)) return 1;
        }
    } // for sample

    TIFFSetDirectory(tif, (tdir_t)tiff_page);
    return 0;
}

int ometiff_read_image_tile(bim::FormatHandle *fmtHndl, bim::TiffParams *par, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level) {
    if (!par) return 1;
    if (!par->tiff) return 1;
    if (par->subType != bim::tstOmeTiff && par->subType != bim::tstOmeBigTiff) return 1;
    TIFF *tif = par->tiff;
    if (!tif) return 1;

    bim::OMETiffInfo *ome = &par->omeTiffInfo;
    bim::PyramidInfo *pyramid = &par->pyramid;
    bim::ImageBitmap *img = fmtHndl->image;

    if (!TIFFIsTiled(tif)) return 1;

    // set correct level
    bim::uint64 tiff_page = computeTiffDirectory(fmtHndl, fmtHndl->pageNumber, 0);
    TIFFSetDirectory(tif, (tdir_t)tiff_page);
    detectTiffPyramid(par);
    if (pyramid->number_levels > 0 && pyramid->number_levels <= level) return 1;
    bim::uint64 subdiroffset = pyramid->directory_offsets[level];
    if (TIFFSetSubDirectory(tif, subdiroffset) == 0) return 1;

    // set tile parameters
    bim::uint32 height = 0;
    bim::uint32 width = 0;
    bim::uint16 planarConfig;
    bim::uint32 columns, rows;
    bim::uint16 photometric = PHOTOMETRIC_MINISWHITE;
    bim::uint16 bitspersample = 1;
    bim::uint16 samplesperpixel = 1;

    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
    TIFFGetField(tif, TIFFTAG_TILEWIDTH, &columns);
    TIFFGetField(tif, TIFFTAG_TILELENGTH, &rows);
    TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &planarConfig);
    TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);

    // fix for ome-tiff files with all channels within one image
    if (samplesperpixel > 1) {
        tiff_page = computeTiffDirectoryNoChannels(fmtHndl, fmtHndl->pageNumber, 0);
        TIFFSetDirectory(tif, (tdir_t)tiff_page);
        detectTiffPyramid(par);
        return read_tiff_image_tile(fmtHndl, par, page, xid, yid, level);
    }

    // tile sizes may be smaller at the border
    bim::uint64 x = xid * columns;
    bim::uint64 y = yid * rows;
    size_t tile_width = (width - x >= columns) ? columns : (bim::uint)width - x;
    size_t tile_height = (height - y >= rows) ? rows : (bim::uint)height - y;

    //img->i.samples = samplesperpixel;
    img->i.samples = ome->channels;
    img->i.depth = bitspersample;
    img->i.width = tile_width;
    img->i.height = tile_height;
    size_t bpp = (size_t)ceil((double)img->i.depth / 8.0);
    if (allocImg(fmtHndl, &img->i, img) != 0) return 1;

    std::vector<bim::uint8> buffer(TIFFTileSize64(tif));
    bim::uint8 *buf = &buffer[0];

    // libtiff tiles will always have columns hight with empty pixels
    // we need to copy only the usable portion
    for (bim::uint sample = 0; sample < img->i.samples; sample++) {
        tiff_page = computeTiffDirectory(fmtHndl, fmtHndl->pageNumber, sample);
        if (TIFFSetDirectory(tif, (tdir_t)tiff_page) == 0) return 1;
        detectTiffPyramid(par);
        if (pyramid->number_levels > 0 && pyramid->number_levels <= level) return 1;
        bim::uint64 subdiroffset = pyramid->directory_offsets[level];
        if (TIFFSetSubDirectory(tif, subdiroffset) == 0) return 1;

        if (TIFFReadTile(tif, buf, (uint32_t)x, (uint32_t)y, 0, 0) < 0) return 1;

        //#pragma omp parallel for default(shared)
        for (bim::uint64 y = 0; y < tile_height; ++y) {
            bim::uint8 *from = buf + y * bpp * columns;
            bim::uint8 *to = ((bim::uint8 *)img->bits[sample]) + y * bpp * tile_width;
            memcpy(to, from, bpp * tile_width);
        }
    } // for sample

    TIFFSetDirectory(tif, (tdir_t)tiff_page);
    return 0;
}



//----------------------------------------------------------------------------
// Metadata hash
//----------------------------------------------------------------------------

void parse_json_object(bim::TagMap *hash, Jzon::Node &parent_node, const std::string &path) {
    for (Jzon::Node::iterator it = parent_node.begin(); it != parent_node.end(); ++it) {
        std::string name = (*it).first;
        Jzon::Node &node = (*it).second;
        if (node.isValid()) {
            parse_json_object(hash, node, path + name + '/');
        }
        std::string value = node.toString();
        hash->set_value(path + name, value);
    }
}

bim::uint append_metadata_ometiff(bim::FormatHandle *fmtHndl, bim::TagMap *hash) {

    if (!fmtHndl) return 1;
    if (!fmtHndl->internalParams) return 1;
    if (!hash) return 1;

    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->info;
    bim::OMETiffInfo *ome = &par->omeTiffInfo;


    //----------------------------------------------------------------------------
    // Dimensions
    //----------------------------------------------------------------------------
    hash->set_value(bim::IMAGE_NUM_Z, (unsigned int)info->number_z);
    hash->set_value(bim::IMAGE_NUM_T, (unsigned int)info->number_t);
    hash->set_value(bim::IMAGE_NUM_C, ome->channels);

    //----------------------------------------------------------------------------
    // Resolution
    //----------------------------------------------------------------------------
    if (ome->pixel_resolution[0] > 0) {
        hash->set_value(bim::PIXEL_RESOLUTION_X, ome->pixel_resolution[0]);
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    }

    if (ome->pixel_resolution[1] > 0) {
        hash->set_value(bim::PIXEL_RESOLUTION_Y, ome->pixel_resolution[1]);
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    }

    if (ome->pixel_resolution[2] > 0) {
        hash->set_value(bim::PIXEL_RESOLUTION_Z, ome->pixel_resolution[2]);
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Z, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    }

    if (ome->pixel_resolution[3] > 0) {
        hash->set_value(bim::PIXEL_RESOLUTION_T, ome->pixel_resolution[3]);
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_T, bim::PIXEL_RESOLUTION_UNIT_SECONDS);
    }

    //----------------------------------------------------------------------------
    // Reading OME-TIFF tag
    //----------------------------------------------------------------------------

    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return 1;

    bim::xstring tag_270 = ifd->readTagString(TIFFTAG_IMAGEDESCRIPTION);
    if (tag_270.size() <= 0) return 0;

    ome_xml_append_metadata(tag_270, info->number_z, (bim::uint)ome->channels, hash);

    if (ome->variant == bim::otvFLUIDIGM) {
        for (unsigned int chan = 0; chan < (bim::uint)ome->channels; ++chan) {
            TinyTiff::IFD *page_ifd = par->ifds.getIfd(chan);
            if (!page_ifd) break;
            bim::xstring page_descr = page_ifd->readTagString(TIFFTAG_IMAGEDESCRIPTION);
            if (page_descr.size() <= 0) break;
            // parse XML and extract channel names and other properties

            pugi::xml_document doc;
            if (doc.load_buffer(page_descr.c_str(), page_descr.size())) {
                try {
                    pugi::xml_node node = doc.select_node("/OME/Image/Pixels/Channel").node();
                    bim::xstring name = node.attribute("Name").value();
                    bim::xstring wavelength = node.attribute("ExcitationWavelength").value();
                    bim::xstring color = node.attribute("Color").value();
                    std::vector<xstring> names = name.replace(")", "").split("(");
                    name = name.replace("(", " (");
                    xstring path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), chan);

                    hash->set_value(path + bim::CHANNEL_INFO_NAME, name);
                    if (wavelength.size() > 0)
                        hash->set_value(path + bim::CHANNEL_INFO_EX_WAVELENGTH, wavelength);
                    if (color.size() > 0) {
                        //hash->set_value(xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), chan), colorOMEStringToRGBString(color));
                        hash->set_value(path + bim::CHANNEL_INFO_COLOR, colorOMEStringToRGBString(color));
                    }
                    if (names.size() > 0)
                        hash->set_value(path + bim::CHANNEL_INFO_DYE, names[0]);
                    if (names.size() > 1)
                        hash->set_value(path + bim::CHANNEL_INFO_SPECTRAL_BAND, names[1]);

                    node = doc.select_node("/OME/Instrument").node();
                    bim::xstring instrument = node.attribute("ID").value();
                    hash->set_value(bim::DOCUMENT_INSTRUMENT, instrument);

                } catch (pugi::xpath_exception) {
                    // do nothing
                }
            }
        }
    }


    //----------------------------------------------------------------------------
    // pyramid info
    //----------------------------------------------------------------------------

    pyramid_append_metadata(fmtHndl, hash);


    //----------------------------------------------------------------------------
    // Reading Micro-Manager tag
    //----------------------------------------------------------------------------

    bim::xstring tag_MM = ifd->readTagString(TIFFTAG_MICROMANAGER);
    if (tag_MM.size() > 0) {
        tag_MM = tag_MM.erase_zeros();
        hash->set_value(bim::RAW_TAGS_PREFIX + "micro-manager-raw", tag_MM);

        // parse micro-manager tags and append
        Jzon::Parser jparser;
        Jzon::Node rootNode = jparser.parseString(tag_MM);
        if (rootNode.isValid()) {
            std::string path = "MicroManager/";
            parse_json_object(hash, rootNode, path);
        }
    }

    //----------------------------------------------------------------------------
    // Reading generic tags
    //----------------------------------------------------------------------------

    generic_append_metadata(fmtHndl, hash);

    return 0;
}

bim::uint write_ometiff_metadata(bim::FormatHandle *fmtHndl, bim::TiffParams *tifParams) {
    TIFF *tif = tifParams->tiff;
    bim::TagMap *hash = fmtHndl->metaData;
    bim::ImageBitmap *img = fmtHndl->image;

    generic_write_metadata(fmtHndl, hash);

    if (hash && hash->hasKey(bim::RAW_TAGS_OMEXML)) {
        TIFFSetField(tif, TIFFTAG_IMAGEDESCRIPTION, hash->get_value_bin(bim::RAW_TAGS_OMEXML));
    } else if (hash) {
        std::string xml = constructOMEXML(fmtHndl->fileName, img, hash);
        TIFFSetField(tif, TIFFTAG_IMAGEDESCRIPTION, xml.c_str());
    } else {
        std::string xml = constructOMEXML(fmtHndl->fileName, img, nullptr);
        TIFFSetField(tif, TIFFTAG_IMAGEDESCRIPTION, xml.c_str());
    }

    return 0;
}

//****************************************************************************
// OME-TIFF WRITER
//****************************************************************************

void ometiff_write_striped_plane(TIFF *out, bim::ImageBitmap *img, bim::FormatHandle *fmtHndl, const bim::uint &sample) {
    bim::uint64 height = (bim::uint32)img->i.height;
    bim::uchar *bits = (bim::uchar *)img->bits[sample];
    size_t line_size = getLineSizeInBytes(img);
    for (bim::uint64 y = 0; y < height; y++) {
        xprogress(fmtHndl, y * (sample + 1), height * img->i.samples, "Writing OME-TIFF");
        if (xtestAbort(fmtHndl) == 1) break;
        TIFFWriteScanline(out, bits, (uint32_t)y, 0);
        bits += line_size;
    } // for y
}

void ometiff_write_tiled_plane(TIFF *tif, bim::ImageBitmap *img, bim::FormatHandle *fmtHndl, const bim::uint &sample) {
    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;

    bim::uint32 width = (bim::uint32)img->i.width;
    bim::uint32 height = (bim::uint32)img->i.height;
    bim::uint32 columns = (uint32_t)par->info.tileWidth, rows = (uint32_t)par->info.tileHeight;
    size_t bpp = (size_t)ceil((double)img->i.depth / 8.0);

    TIFFSetField(tif, TIFFTAG_TILEWIDTH, columns);
    TIFFSetField(tif, TIFFTAG_TILELENGTH, rows);

    std::vector<bim::uint8> buffer(TIFFTileSize64(tif));
    bim::uint8 *buf = &buffer[0];

    for (bim::uint y = 0; y < img->i.height; y += rows) {
        xprogress(fmtHndl, y, img->i.height, "Writing OME-TIFF");
        if (xtestAbort(fmtHndl) == 1) break;
        for (bim::uint x = 0; x < (bim::uint)img->i.width; x += columns) {
            bim::uint tile_width = (width - x >= columns) ? columns : (bim::uint)width - x;
            bim::uint tile_height = (height - y >= rows) ? rows : (bim::uint)height - y;

            // libtiff tiles will always have columns hight with empty pixels
            // we need to copy only the usable portion
            //#pragma omp parallel for default(shared)
            for (bim::int64 i = 0; i < tile_height; ++i) {
                bim::uint8 *to = buf + i * bpp * columns;
                bim::uint8 *from = ((bim::uint8 *)img->bits[sample]) + (y + i) * bpp * width + x * bpp;
                memcpy(to, from, bpp * tile_width);
            }
            if (TIFFWriteTile(tif, buf, x, y, 0, 0) < 0) break;
        } // for x
    }     // for y
}

int omeTiffWritePlane(bim::FormatHandle *fmtHndl, bim::TiffParams *par, bim::ImageBitmap *img = NULL, bool subscale = false) {
    TIFF *out = par->tiff;
    if (!img) img = fmtHndl->image;

    bim::uint32 height = (uint32_t)img->i.height;
    bim::uint32 width = (uint32_t)img->i.width;
    bim::uint32 rowsperstrip = (bim::uint32)-1;
    bim::uint16 bitspersample = img->i.depth;
    bim::uint16 samplesperpixel = 1;
    bim::uint16 photometric = PHOTOMETRIC_MINISBLACK;
    if (bitspersample == 1 && samplesperpixel == 1) photometric = PHOTOMETRIC_MINISWHITE;
    bim::uint16 compression;
    bim::uint16 planarConfig = PLANARCONFIG_SEPARATE; // separated planes

    // ignore storing pyramid for small images
    bim::int64 sz = bim::max<bim::int64>(width, height);
    if (!subscale && par->pyramid.format != PyramidInfo::pyrFmtNone && sz < PyramidInfo::min_level_size) {
        par->pyramid.format = PyramidInfo::pyrFmtNone;
    }

    // only allow sub-dirs in ome-tiff for pyramid storage to maximize compatibility with other readers
    if (par->pyramid.format == PyramidInfo::pyrFmtTopDirs) {
        par->pyramid.format = PyramidInfo::pyrFmtSubDirs;
    }

    // samples in OME-TIFF are stored in separate IFDs
    for (bim::uint sample = 0; sample < img->i.samples; sample++) {

        // handle standard width/height/bpp stuff
        TIFFSetField(out, TIFFTAG_IMAGEWIDTH, width);
        TIFFSetField(out, TIFFTAG_IMAGELENGTH, height);
        TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, samplesperpixel);
        TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, bitspersample);
        TIFFSetField(out, TIFFTAG_PHOTOMETRIC, photometric);
        TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
        TIFFSetField(out, TIFFTAG_PLANARCONFIG, planarConfig); // separated planes
        TIFFSetField(out, TIFFTAG_SOFTWARE, "libbioimage");

        // set pixel format
        bim::uint16 sampleformat = SAMPLEFORMAT_UINT;
        if (img->i.pixelType == bim::DataFormat::FMT_SIGNED) sampleformat = SAMPLEFORMAT_INT;
        if (img->i.pixelType == bim::DataFormat::FMT_FLOAT) sampleformat = SAMPLEFORMAT_IEEEFP;
        TIFFSetField(out, TIFFTAG_SAMPLEFORMAT, sampleformat);

        //if( TIFFGetField( out, TIFFTAG_DOCUMENTNAME, &pszText ) )
        //if( TIFFGetField( out, TIFFTAG_DATETIME, &pszText ) )


        //------------------------------------------------------------------------------
        // resolution pyramid
        //------------------------------------------------------------------------------

        if (par->info.tileWidth > 0 && par->pyramid.format != PyramidInfo::pyrFmtNone) {
            TIFFSetField(out, TIFFTAG_SUBFILETYPE, subscale ? FILETYPE_REDUCEDIMAGE : 0);

            if (!subscale) {
                par->pyramid.directory_offsets.resize(0);
                bim::int64 num_levels = static_cast<bim::int64>(ceil(bim::log2<double>((double)sz)) - ceil(bim::log2<double>((double)PyramidInfo::min_level_size)) + 1);
                if (par->pyramid.format == PyramidInfo::pyrFmtSubDirs) {
                    // if pyramid levels are to be written into SUBIFDs, write the tag and indicate to libtiff how many subifds are coming
                    bim::uint16 num_sub_ifds = static_cast<bim::uint16>(num_levels - 1); // number of pyramidal levels - 1
                    std::vector<bim::uint64> offsets_sub_ifds(num_levels - 1, 0UL);
                    TIFFSetField(out, TIFFTAG_SUBIFD, num_sub_ifds, &offsets_sub_ifds[0]);
                }
            }
        }

        //------------------------------------------------------------------------------
        // compression
        //------------------------------------------------------------------------------

        compression = fmtHndl->compression;
        if (compression == 0) compression = COMPRESSION_NONE;

        if (compression == COMPRESSION_CCITTFAX4 && bitspersample != 1) {
            compression = COMPRESSION_NONE;
        }
        if (compression == COMPRESSION_JPEG && (bitspersample != 8 && bitspersample != 16)) {
            compression = COMPRESSION_NONE;
        }
        TIFFSetField(out, TIFFTAG_COMPRESSION, compression);

        // set compression parameters
        bim::uint32 strip_size = bim::max<bim::uint32>(TIFFDefaultStripSize(out, -1), 1);
        if (compression == COMPRESSION_JPEG) {
            // rowsperstrip must be multiple of 8 for JPEG
            TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, strip_size + (8 - (strip_size % 8)));
            TIFFSetField(out, TIFFTAG_JPEGQUALITY, fmtHndl->quality);
        } else if (compression == COMPRESSION_ADOBE_DEFLATE) {
            //TIFFSetField( out, TIFFTAG_ROWSPERSTRIP, height );
            if (planarConfig == PLANARCONFIG_SEPARATE || samplesperpixel == 1)
                TIFFSetField(out, TIFFTAG_PREDICTOR, PREDICTOR_NONE);
            else
                TIFFSetField(out, TIFFTAG_PREDICTOR, PREDICTOR_HORIZONTAL);
            TIFFSetField(out, TIFFTAG_ZIPQUALITY, 9);
        } else if (compression == COMPRESSION_CCITTFAX4) {
            //TIFFSetField( out, TIFFTAG_ROWSPERSTRIP, height );
        } else if (compression == COMPRESSION_LZW) {
            //TIFFSetField( out, TIFFTAG_ROWSPERSTRIP, strip_size );
            if (planarConfig == PLANARCONFIG_SEPARATE || samplesperpixel == 1)
                TIFFSetField(out, TIFFTAG_PREDICTOR, PREDICTOR_NONE);
            else
                TIFFSetField(out, TIFFTAG_PREDICTOR, PREDICTOR_HORIZONTAL);
        } else {
            //TIFFSetField( out, TIFFTAG_ROWSPERSTRIP, strip_size );
        }

        //------------------------------------------------------------------------------
        // writing meta data
        //------------------------------------------------------------------------------
        if (fmtHndl->pageNumber == 0 && sample == 0 && !subscale) {
            write_ometiff_metadata(fmtHndl, par);
        }

        //------------------------------------------------------------------------------
        // writing image
        //------------------------------------------------------------------------------

        if (par->info.tileWidth < 1 || par->pyramid.format == PyramidInfo::pyrFmtNone) {
            ometiff_write_striped_plane(out, img, fmtHndl, sample);
        } else {
            ometiff_write_tiled_plane(out, img, fmtHndl, sample);
        }

        // correct libtiff writing of subifds by linking sibling ifds through nextifd offset
        if (subscale && par->pyramid.format == PyramidInfo::pyrFmtSubDirs) {
            bim::uint64 dir_offset = (TIFFSeekFile(out, 0, SEEK_END) + 1) & ~1;
            par->pyramid.directory_offsets.push_back(dir_offset);
        }


        TIFFWriteDirectory(out);

        //------------------------------------------------------------------------------
        // write pyramid levels
        //------------------------------------------------------------------------------

        bim::ImageBitmap temp;
        if (!subscale && par->info.tileWidth > 0 && par->pyramid.format != PyramidInfo::pyrFmtNone) {
            //bim::ImageBitmap temp;
            temp.i = img->i;
            temp.i.samples = 1;
            temp.bits[0] = img->bits[sample];
            bim::Image image(&temp);
            int i = 0;
            while (bim::max<bim::uint64>(image.width(), image.height()) > PyramidInfo::min_level_size) {
                image = image.downSampleBy2x();
                if (omeTiffWritePlane(fmtHndl, par, image.imageBitmap(), true) != 0) break;
                ++i;
            }

            // correct libtiff writing of subifds by linking sibling ifds through nextifd offset
            if (par->pyramid.format == PyramidInfo::pyrFmtSubDirs)
                for (ptrdiff_t i = 0; i < ((ptrdiff_t)par->pyramid.directory_offsets.size()) - 1; ++i) {
                    if (!tiff_update_subifd_next_pointer(out, par->pyramid.directory_offsets[i], par->pyramid.directory_offsets[i + 1])) break;
                }
        }

    } // for sample

    if (!subscale) {
        TIFFFlushData(out);
        TIFFFlush(out);
    }
    return 0;
}
