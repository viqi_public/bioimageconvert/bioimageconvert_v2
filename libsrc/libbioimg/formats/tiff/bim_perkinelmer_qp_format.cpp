/*****************************************************************************
  PerkinElmer QP definitions 
  Copyright (c) 2021-2022, ViQi Inc
 
  Author: Dima V. Fedorov <mailto:dima@viqi.org>
    
  History:
    2021-10-05 16:01 - First creation

  Ver : 1
*****************************************************************************/

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>

#include <pugixml.hpp>

#include <bim_image.h>
#include <bim_img_format_utils.h>
#include <bim_metatags.h>
#include <tag_map.h>
#include <xstring.h>

#include "bim_tiff_format.h"
#include "bim_tiny_tiff.h"
#include "xtiffio.h"
#include "bim_perkinelmer_qp_format.h"

using namespace bim;

unsigned int tiffGetNumberOfPages(bim::TiffParams *par);
int tiff_update_subifd_next_pointer(TIFF *tif, bim::uint64 dir_offset, bim::uint64 to_offset);
void detectTiffPyramid(bim::TiffParams *tiffParams);
int read_tiff_image(bim::FormatHandle *fmtHndl, bim::TiffParams *tifParams);
int read_tiff_image_level(bim::FormatHandle *fmtHndl, bim::TiffParams *tifParams, bim::uint page, bim::uint level);
int read_tiff_image_tile(bim::FormatHandle *fmtHndl, bim::TiffParams *tifParams, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level);
void pyramid_append_metadata(bim::FormatHandle *fmtHndl, bim::TagMap *hash);
void generic_append_metadata(FormatHandle *fmtHndl, TagMap *hash);

//----------------------------------------------------------------------------
// QP MISC FUNCTIONS
//----------------------------------------------------------------------------

bool qpTiffIsValid(bim::TiffParams *par) {
    if (!par) return false;
    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return false;
    if (!ifd->tagPresent(TIFFTAG_IMAGEDESCRIPTION)) return false;
    bim::xstring tag_270 = ifd->readTagString(TIFFTAG_IMAGEDESCRIPTION);
    if (tag_270.contains("QPI-ImageDescription") || tag_270.contains("PerkinElmer-QPI-ImageDescription")) return true;
    return false;
}


//----------------------------------------------------------------------------
// QP METADATA
//----------------------------------------------------------------------------

void walker_qp(pugi::xml_node node, bim::xstring path, bim::TagMap *hash) {
    // iterate over tags
    for (pugi::xml_node child = node.first_child(); child; child = child.next_sibling()) {
        bim::xstring name = child.name();
        bim::xstring value = child.text().as_string();

        // add tag with text node value first
        if (name.size() > 0 && value.size() > 0) {
            hash->set_value(path + "/" + name, value);
        }

        // iterate over attributes
        for (pugi::xml_attribute_iterator ait = child.attributes_begin(); ait != child.attributes_end(); ++ait) {
            hash->set_value(path + "/" + name + "/" + ait->name(), ait->value());
        }
    }

    // iterate over folders
    for (pugi::xml_node child = node.first_child(); child; child = child.next_sibling()) {
        bim::xstring tag = child.name();
        walker_qp(child, path + "/" + tag, hash);
    }
}

void parse_qp_xml(const bim::xstring &tag_description, const bim::xstring &path, bim::TagMap *hash) {
    // parse metaimaging XML doc into custom tags
    pugi::xml_document doc;
    if (doc.load_buffer(tag_description.c_str(), tag_description.size())) {
        pugi::xml_node child = doc.first_child();
        walker_qp(doc.first_child(), path, hash);
    }
}

void read_qp_xml(bim::TiffParams *par, bim::TagMap *hash, const bim::xstring &path) {
    char *descr;
    TIFFGetField(par->tiff, TIFFTAG_IMAGEDESCRIPTION, &descr);
    bim::xstring tag_description = descr;

    parse_qp_xml(tag_description, path, hash);
}

void qpDescribeDirectories(bim::TiffParams *par) {
    TIFF *tif = par->tiff;
    ImageInfo *info = &par->info;
    PyramidInfo *pyramid = &par->pyramid;
    PerkinElmerQPInfo *qp = &par->qp_info;

    int dir_num = 0;
    bim::uint32 sub_file_type = 10000;
    bim::uint64 current_dir = TIFFCurrentDirectory(tif);
    bim::uint32 width = 0, height = 0, w = 0, h = 0;
    bim::uint32 tile_width = 0, tile_height = 0;
    if (!TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width)) width = 0;
    if (!TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height)) height = 0;
    if (!TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tile_width)) tile_width = 0;
    if (!TIFFGetField(tif, TIFFTAG_TILELENGTH, &tile_height)) tile_height = 0;
    pyramid->init(width, height, tile_width, tile_height, (tile_width > 0 && tile_height>0));
    qp->num_sep_channels = 1;
    int current_channel = -1;
    int level = 1;

    qp->meta.set_value("ch0_l0_dir_num", dir_num);
    qp->meta.set_value("ch0_l0_dir_offset", (unsigned int)tif->tif_diroff);

    //----------------------------------------------------------------
    // read directories
    //----------------------------------------------------------------
    double prev_sz[2] = { (double)width, (double)height };
    if (tif->tif_nextdiroff > 0) {
        bim::uint64 subdiroffset = tif->tif_nextdiroff;
        while (TIFFSetSubDirectory(tif, subdiroffset) > 0) {
            ++dir_num;
            xstring path = bim::xstring::xprintf("%.5d", dir_num);
            read_qp_xml(par, &qp->meta, path);

            xstring image_type = qp->meta.get_value(path + "/ImageType").toLowerCase();
            if (image_type == "thumbnail") {
                qp->meta.set_value("thumbnail_dir_num", dir_num);
                qp->meta.set_value("thumbnail_dir_offset", (unsigned int)subdiroffset);
                if (dir_num == 1)
                    qp->separated_channels = false;
            } else if (image_type == "overview") {
                qp->meta.set_value("macro_dir_num", dir_num);
                qp->meta.set_value("macro_dir_offset", (unsigned int)subdiroffset);
            } else if (image_type == "label") {
                qp->meta.set_value("label_dir_num", dir_num);
                qp->meta.set_value("label_dir_offset", (unsigned int)subdiroffset);
            } else if (image_type == "fullresolution") {
                qp->meta.set_value(xstring::xprintf("ch%d_l0_dir_num", qp->num_sep_channels), dir_num);
                qp->meta.set_value(xstring::xprintf("ch%d_l0_dir_offset", qp->num_sep_channels), (unsigned int) subdiroffset);
                read_qp_xml(par, &qp->meta_channels, bim::xstring::xprintf("Band:%d", qp->num_sep_channels));
                qp->num_sep_channels += 1;
                if (dir_num > 0) 
                    qp->separated_channels = true;
            } else if (image_type == "reducedresolution") {
                if (qp->separated_channels) {
                    current_channel += 1;
                    if (current_channel >= qp->num_sep_channels) {
                        current_channel = 0;
                        level += 1;
                    }
                }

                if (TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w) > 0 && TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h) > 0) {
                    bool is_tiled = true;
                    if (TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tile_width) < 1) {
                        tile_width = w;
                        is_tiled = false;
                    }
                    if (TIFFGetField(tif, TIFFTAG_TILELENGTH, &tile_height) < 1) {
                        tile_height = h;
                        is_tiled = false;
                    }

                    double scale = (double)w / (double)width;
                    if (current_channel < 1) {
                        pyramid->addLevel(scale, subdiroffset, tile_width, tile_height, is_tiled);
                    }

                    qp->meta.set_value(xstring::xprintf("ch%d_l%d_dir_num", current_channel, level), dir_num);
                    qp->meta.set_value(xstring::xprintf("ch%d_l%d_dir_offset", current_channel, level), (unsigned int)subdiroffset);
                }
            }

            subdiroffset = tif->tif_nextdiroff;
        }
    }
    if (pyramid->number_levels > 1) {
        pyramid->format = PyramidInfo::pyrFmtTopDirs; // imagemagick style multi-page pyramid
    }

    //----------------------------------------------------------------
    // finish
    //----------------------------------------------------------------

    if (current_dir != TIFFCurrentDirectory(tif))
        TIFFSetDirectory(tif, (tdir_t)current_dir);
    info->number_levels = pyramid->number_levels;
}

int qpTiffGetInfo(bim::TiffParams *par) {
    bim::ImageInfo *info = &par->info;
    bim::PerkinElmerQPInfo *qp = &par->qp_info;
    PyramidInfo *pyramid = &par->pyramid;
    TIFF *tif = par->tiff;

    qpDescribeDirectories(par);
    //std::cout << qp->meta.toString();
    TIFFSetDirectory(tif, 0);

    //---------------------------------------------------------------
    // image geometry
    //---------------------------------------------------------------

    info->width = pyramid->width;
    info->height = pyramid->height;
    info->number_pages = 1;
    info->number_t = 1;
    info->number_z = 1;
    info->tileWidth = pyramid->tile_sizes_w[0];
    info->tileHeight = pyramid->tile_sizes_h[0];

    if (qp->separated_channels) {
        info->samples = qp->num_sep_channels;
    } else {
        bim::uint16 samplesperpixel = 1;
        TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
        info->samples = samplesperpixel;     
    }

    if (info->samples > 1 && qp->separated_channels)
        info->imageMode = bim::ImageModes::IM_MULTI;
    else if (info->samples > 1 && !qp->separated_channels)
        info->imageMode = bim::ImageModes::IM_RGB;
    else
        info->imageMode = bim::ImageModes::IM_GRAYSCALE;

    bim::uint16 bitspersample = 1;
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
    info->depth = bitspersample;

    // pixel resolution
    float xresolution = 0;
    float yresolution = 0;
    short resolutionunit = 0;
    if ((TIFFGetField(tif, TIFFTAG_RESOLUTIONUNIT, &resolutionunit)) &&
        (TIFFGetField(tif, TIFFTAG_XRESOLUTION, &xresolution)) &&
        (TIFFGetField(tif, TIFFTAG_YRESOLUTION, &yresolution))) {
        if (resolutionunit == RESUNIT_CENTIMETER) {
            info->xRes = 10000.0 / xresolution;
            info->yRes = 10000.0 / yresolution;
            qp->pix_resolution_in_tiff_tag = true;
        }
    }

    float xpos = 0;
    float ypos = 0;
    if (resolutionunit == RESUNIT_CENTIMETER &&
        (TIFFGetField(tif, TIFFTAG_XPOSITION, &xpos)) &&
        (TIFFGetField(tif, TIFFTAG_YPOSITION, &ypos))) {
        double offset_um_x = 10000.0 * xpos;
        double offset_um_y = 10000.0 * ypos;
        qp->meta.set_value("image_offset_um_x", offset_um_x);
        qp->meta.set_value("image_offset_um_y", offset_um_y);
    }

    return 0;
}


//----------------------------------------------------------------------------
// READ/WRITE FUNCTIONS
//----------------------------------------------------------------------------

bool qp_read_striped(TIFF *tif, bim::ImageBitmap *img, bim::FormatHandle *fmtHndl, const unsigned int &sample) {
    size_t lineSize = getLineSizeInBytes(img);
    bim::uchar *p = (bim::uchar *)img->bits[sample];
    for (bim::uint64 y = 0; y < img->i.height; y++) {
        xprogress(fmtHndl, y * (sample + 1), img->i.height * img->i.samples, "Reading OME-TIFF");
        if (xtestAbort(fmtHndl) == 1) return false;
        if (TIFFReadScanline(tif, p, (uint32_t)y, 0) < 0) return false;
        p += lineSize;
    } // for y
    return true;
}

bool qp_read_tiled(TIFF *tif, bim::ImageBitmap *img, bim::FormatHandle *fmtHndl, const int &sample) {
    if (!tif || !img) return false;

    size_t lineSize = getLineSizeInBytes(img);
    size_t bpp = (size_t)ceil((double)img->i.depth / 8.0);
    bim::uint32 columns=0, rows=0;
    TIFFGetField(tif, TIFFTAG_TILEWIDTH, &columns);
    TIFFGetField(tif, TIFFTAG_TILELENGTH, &rows);

    std::vector<bim::uchar> buffer(TIFFTileSize64(tif));
    bim::uchar *buf = &buffer[0];

    for (bim::uint y = 0; y < img->i.height; y += rows) {
        xprogress(fmtHndl, y, img->i.height, "Reading TIFF");
        if (xtestAbort(fmtHndl) == 1) return false;

        bim::uint tile_height = (img->i.height - y >= rows) ? rows : (bim::uint)img->i.height - y;
        for (bim::uint x = 0; x < (bim::uint)img->i.width; x += columns) {
            bim::uint tile_width = (img->i.width - x < columns) ? (bim::uint)img->i.width - x : (bim::uint)columns;
            if (TIFFReadTile(tif, buf, x, y, 0, 0) < 0) return false;
            for (bim::uint yi = 0; yi < tile_height; yi++) {
                bim::uchar *p = (bim::uchar *)img->bits[sample] + (lineSize * (y + yi));
                _TIFFmemcpy(p + (x * bpp), buf + (yi * columns * bpp), tile_width * bpp);
            }
        } // for x
    }     // for y
    return true;
}

bim::uint qp_read_current_tile(bim::FormatHandle *fmtHndl, bim::TiffParams *par, bim::uint64 xid, bim::uint64 yid, bim::uint level = 0) {
    TIFF *tif = par->tiff;
    bim::ImageInfo *info = &par->info;
    bim::PerkinElmerQPInfo *qp = &par->qp_info;

    bim::uint64 dir_offset = qp->meta.get_value_int(xstring::xprintf("ch%d_l%d_dir_offset", 0, level), 0);
    TIFFSetSubDirectory(tif, dir_offset);

    bim::uint16 bitspersample = 1;
    bim::uint16 samplesperpixel = 1;
    bim::uint16 sampleformat = 1;

    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
    TIFFGetField(tif, TIFFTAG_SAMPLEFORMAT, &sampleformat);
    if (samplesperpixel != 1) return 1;

    bim::uint32 tile_original_width = 0, tile_original_height = 0;
    TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tile_original_width);
    TIFFGetField(tif, TIFFTAG_TILELENGTH, &tile_original_height);
    if (tile_original_width == 0 || tile_original_height == 0) return 1;

    bim::uint64 x = xid * tile_original_width;
    bim::uint64 y = yid * tile_original_height;

    bim::uint32 width = 0, height = 0;
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);

    bim::uint64 tile_width = (width - x < tile_original_width) ? width - x : tile_original_width;
    bim::uint64 tile_height = (height - y < tile_original_height) ? height - y : tile_original_height;
    bool tile_original_size = (tile_width == tile_original_width) && (tile_height == tile_original_height);
    
    bim::ImageBitmap *img = fmtHndl->image;
    if (img->i.depth != bitspersample || img->i.width != tile_width || img->i.height != tile_height || img->i.samples != qp->num_sep_channels) {
        info->samples = qp->num_sep_channels;
        info->depth = bitspersample;
        info->width = tile_width;
        info->height = tile_height;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
        if (sampleformat == SAMPLEFORMAT_INT)
            info->pixelType = bim::DataFormat::FMT_SIGNED;
        else if (sampleformat == SAMPLEFORMAT_IEEEFP)
            info->pixelType = bim::DataFormat::FMT_FLOAT;

        if (allocImg(fmtHndl, info, img) != 0) return 1;
    }

    //--------------------------------------------------------------------
    // read data
    //--------------------------------------------------------------------
    for (unsigned int sample = 0; sample < (unsigned int)info->samples; ++sample) {
        dir_offset = qp->meta.get_value_int(xstring::xprintf("ch%d_l%d_dir_offset", sample, level), 0);
        TIFFSetSubDirectory(tif, dir_offset);
        if (tile_original_size) {
            if (TIFFReadTile(tif, img->bits[sample], (uint32_t)x, (uint32_t)y, 0, 0) < 0) return 1;
        } else {
            size_t bpp = (size_t)ceil((double)img->i.depth / 8.0);
            size_t stride_in = bpp * tile_original_width;
            size_t stride_out = bpp * tile_width;
            std::vector<bim::uint8> buffer(TIFFTileSize64(tif), 0);
            if (TIFFReadTile(tif, &buffer[0], (uint32_t)x, (uint32_t)y, 0, 0) < 0) return 1;
            bim::uint8 *from = &buffer[0];
            bim::uint8 *to = (bim::uint8 *) img->bits[sample];
            for (bim::uint64 yy = 0; yy < tile_height; ++yy) {
                memcpy(to, from, stride_out);
                from += stride_in;
                to += stride_out;
            }
        }

    } // for sample

    return 0;
}

bim::uint qp_read_current_plane(bim::FormatHandle *fmtHndl, bim::TiffParams *par, bim::uint level = 0) {
    TIFF *tif = par->tiff;
    bim::ImageInfo *info = &par->info;
    bim::PerkinElmerQPInfo *qp = &par->qp_info;

    //--------------------------------------------------------------------
    // read image parameters
    //--------------------------------------------------------------------

    bim::uint64 dir_offset = qp->meta.get_value_int(xstring::xprintf("ch%d_l%d_dir_offset", 0, level), 0);
    TIFFSetSubDirectory(tif, dir_offset);

    bim::uint16 bitspersample = 1;
    bim::uint32 height = 0;
    bim::uint32 width = 0;
    bim::uint16 samplesperpixel = 1;
    bim::uint16 sampleformat = 1;

    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
    TIFFGetField(tif, TIFFTAG_SAMPLEFORMAT, &sampleformat);
    if (samplesperpixel != 1) return 1;

    bim::uint32 columns=0, rows=0;
    TIFFGetField(tif, TIFFTAG_TILEWIDTH, &columns);
    TIFFGetField(tif, TIFFTAG_TILELENGTH, &rows);

    bim::ImageBitmap *img = fmtHndl->image;
    if (img->i.depth != bitspersample || img->i.width != width || img->i.height != height || img->i.samples != qp->num_sep_channels) {
        info->samples = qp->num_sep_channels;
        info->depth = bitspersample;
        info->width = width;
        info->height = height;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
        if (sampleformat == SAMPLEFORMAT_INT)
            info->pixelType = bim::DataFormat::FMT_SIGNED;
        else if (sampleformat == SAMPLEFORMAT_IEEEFP)
            info->pixelType = bim::DataFormat::FMT_FLOAT;

        if (allocImg(fmtHndl, info, img) != 0) return 1;
    }

    //--------------------------------------------------------------------
    // read data
    //--------------------------------------------------------------------
    for (unsigned int sample = 0; sample < (unsigned int)info->samples; ++sample) {
        dir_offset = qp->meta.get_value_int(xstring::xprintf("ch%d_l%d_dir_offset", sample, level), 0);
        TIFFSetSubDirectory(tif, dir_offset);
        if (!TIFFIsTiled(tif) || columns == 0 || rows == 0) {
            if (!qp_read_striped(tif, img, fmtHndl, sample)) return 1;
        } else {
            if (!qp_read_tiled(tif, img, fmtHndl, sample)) return 1;
        }
    } // for sample

    return 0;
}

//--------------------------------------------------------------------------------------------
// Levels and Tiles functions
//--------------------------------------------------------------------------------------------

int qp_read_image_level(bim::FormatHandle *fmtHndl, bim::TiffParams *par, bim::uint page, bim::uint level) {
    if (!par) return 1;
    if (!par->tiff) return 1;
    if (par->subType != bim::tstPerkinElmerQP) return 1;
    TIFF *tif = par->tiff;
    if (!tif) return 1;

    XConf *conf = fmtHndl->arguments;
    bim::PerkinElmerQPInfo *qp = &par->qp_info;

    // reading of sub-images
    if (conf && conf->is_path_requested("/preview") && qp->meta.hasKey("macro_dir_num")) {
        fmtHndl->pageNumber = qp->meta.get_value_int("macro_dir_num", 0);
        return read_tiff_image(fmtHndl, par);
    } else if (conf && conf->is_path_requested("/label") && qp->meta.hasKey("label_dir_num")) {
        fmtHndl->pageNumber = qp->meta.get_value_int("label_dir_num", 0);
        return read_tiff_image(fmtHndl, par);
    } else if (conf && conf->is_path_requested("/thumbnail") && qp->meta.hasKey("thumbnail_dir_num")) {
        fmtHndl->pageNumber = qp->meta.get_value_int("thumbnail_dir_num", 0);
        return read_tiff_image(fmtHndl, par);
    }

    // RGB reading
    if (!qp->separated_channels) {

        if (level == 0) {
            fmtHndl->pageNumber = 0;
            return read_tiff_image(fmtHndl, par);
        }

        return read_tiff_image_level(fmtHndl, par, page, level);
    }

    // reading channels from separate pages
    return qp_read_current_plane(fmtHndl, par, level);
}

int qp_read_image_tile(bim::FormatHandle *fmtHndl, bim::TiffParams *par, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level) {
    if (!par) return 1;
    if (!par->tiff) return 1;
    if (par->subType != bim::tstPerkinElmerQP) return 1;
    TIFF *tif = par->tiff;
    if (!tif) return 1;
    bim::PyramidInfo *pyramid = &par->pyramid;
    bim::PerkinElmerQPInfo *qp = &par->qp_info;

    if (!pyramid->isLevelTiled(level)) {
        return qp_read_image_level(fmtHndl, par, page, level);
    }

    if (!qp->separated_channels) {
        return read_tiff_image_tile(fmtHndl, par, page, xid, yid, level);
    }

    return qp_read_current_tile(fmtHndl, par, xid, yid, level);
}

//----------------------------------------------------------------------------
// Metadata hash
//----------------------------------------------------------------------------

bim::uint qp_append_metadata(bim::FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (!fmtHndl) return 1;
    if (!fmtHndl->internalParams) return 1;
    if (!hash) return 1;

    bim::TiffParams *par = (bim::TiffParams *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->info;
    XConf *conf = fmtHndl->arguments;
    bim::PerkinElmerQPInfo *qp = &par->qp_info;
    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return 1;

    bim::xstring tag_270 = ifd->readTagString(TIFFTAG_IMAGEDESCRIPTION);
    if (tag_270.size() <= 0) return 0;

    parse_qp_xml(tag_270, "Akoya", hash);
    parse_qp_xml(tag_270, "00000", &qp->meta);

    // pyramid info
    pyramid_append_metadata(fmtHndl, hash);

    // generic tags
    generic_append_metadata(fmtHndl, hash);

    // sub-images
    int sub_images = 0;
    hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), 0), "/image");
    if (qp->meta.hasKey("label_dir_num")) {
        hash->set_value(bim::IMAGE_NUM_LABELS, 1);
        hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), ++sub_images), "/label");
    }
    if (qp->meta.hasKey("macro_dir_num")) {
        hash->set_value(bim::IMAGE_NUM_PREVIEWS, 1);
        hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), ++sub_images), "/preview");
    }
    if (qp->meta.hasKey("thumbnail_dir_num")) {
        hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), ++sub_images), "/thumbnail");
    }

    //----------------------------------------------------------------------------
    // Resolution
    //----------------------------------------------------------------------------

    if (qp->pix_resolution_in_tiff_tag) {
        hash->set_value(bim::PIXEL_RESOLUTION_X, info->xRes);
        hash->set_value(bim::PIXEL_RESOLUTION_Y, info->yRes);
    } else {
        hash->set_value_from_old_key("Akoya/ScanProfile/root/MsiResolution/PixelSizeMicrons", bim::PIXEL_RESOLUTION_X);
        hash->set_value_from_old_key("Akoya/ScanProfile/root/MsiResolution/PixelSizeMicrons", bim::PIXEL_RESOLUTION_Y);
    }
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y, bim::PIXEL_RESOLUTION_UNIT_MICRONS);

    // position
    hash->set_value(bim::COORDINATES_AXIS_ORIGIN, "top_left");
    hash->set_value(bim::COORDINATES_AXIS_ORIENTATION, "ascending,ascending");
    hash->set_value(bim::COORDINATES_UNITS, "microns");
    hash->set_value(bim::COORDINATES_POINTS_TOPLEFT, xstring::xprintf("%f,%f", qp->meta.get_value_double("image_offset_um_x", 0), qp->meta.get_value_double("image_offset_um_y", 0)));

    // objective
    hash->set_value_from_old_key("Akoya/Objective", bim::OBJECTIVE_DESCRIPTION);
    bim::parse_objective_from_string(hash->get_value(bim::OBJECTIVE_DESCRIPTION), hash);
   
    hash->set_value_from_old_key("Akoya/ScanProfile/root/MsiResolution/Magnification", bim::OBJECTIVE_MAGNIFICATION);
    hash->set_value_from_old_key("Akoya/ScanProfile/root/MsiResolution/ObjectiveName", bim::OBJECTIVE_DESCRIPTION);

    // signal units, as per documentation
    bim::uint8 su = (bim::uint8)hash->get_value_unsigned("Akoya/SignalUnits", 0);
    bim::uint8 su1 = (su & 0x0F);
    bim::uint8 su2 = (su & 0xF0) >> 4;

    if (su1 == 0)
        hash->set_value("Akoya/SignalUnitsType", "raw counts");
    else if (su1 == 1)
        hash->set_value("Akoya/SignalUnitsType", "normalized");
    else if (su1 == 4)
        hash->set_value("Akoya/SignalUnitsType", "optical density");
    else if (su1 == 5)
        hash->set_value("Akoya/SignalUnitsType", "dark corrected counts");

    if (su2 == 0)
        hash->set_value("Akoya/SignalUnitsWeighting", "average across all bands");
    else if (su2 == 1)
        hash->set_value("Akoya/SignalUnitsWeighting", "total summed signal across all bands");
    else if (su2 == 4)
        hash->set_value("Akoya/SignalUnitsWeighting", "peak signal in highest-valued band");


    // store specific qp tags in BIM schema
    hash->set_value(bim::DOCUMENT_VENDOR, "Akoya");
    hash->set_value_from_old_key("Akoya/AcquisitionSoftware", bim::DOCUMENT_APPLICATION);
    hash->set_value_from_old_key("Akoya/AcquisitionSoftware", bim::DOCUMENT_INSTRUMENT);
    hash->set_value_from_old_key("Akoya/SlideID", bim::DOCUMENT_SLIDE_ID);
    hash->set_value_from_old_key("Akoya/Responsivity/Filter/Date", bim::DOCUMENT_DATETIME);
    hash->set_value_from_old_key("Akoya/Identifier", bim::DOCUMENT_GUID);
    hash->set_value_from_old_key("Akoya/OperatorName", bim::DOCUMENT_USER);
    hash->set_value_from_old_key("Akoya/Barcode", bim::DOCUMENT_BARCODE);
    hash->set_value_from_old_key("Akoya/ComputerName", bim::DOCUMENT_COMPUTER);

    // channel info
    bim::xstring modality("Fluorescence");
    if (hash->hasKey("Akoya/ScanProfile/root/Mode"))
        modality = hash->get_value("Akoya/ScanProfile/root/Mode").replace("im_", "");
    else if (hash->hasKey("Akoya/ScanProfile/root/ScanBands/ScanBands-i/FilterPair/EmissionFilter/FixedFilter/Name"))
        modality = hash->get_value("Akoya/ScanProfile/root/ScanBands/ScanBands-i/FilterPair/EmissionFilter/FixedFilter/Name");    
    else if (hash->hasKey("Akoya/ScanProfile/root/ScanBands/ScanBands-i/FilterPair/ExcitationFilter/FixedFilter/Name"))
        modality = hash->get_value("Akoya/ScanProfile/root/ScanBands/ScanBands-i/FilterPair/ExcitationFilter/FixedFilter/Name");   

    double bits = hash->get_value_double("Akoya/CameraSettings/BitDepth", 0);
    bim::DataFormat pixel_format = info->pixelType;
    int bits_actual = info->depth;

    if (!qp->separated_channels) {
        bim::xstring path = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), 0);
        hash->set_value(path + bim::CHANNEL_INFO_NAME, "Red");
        hash->set_value(path + bim::CHANNEL_INFO_COLOR, "1.0,0,0");
        hash->set_value(path + bim::CHANNEL_INFO_MODALITY, modality);
        hash->set_value_from_old_key("Akoya/CameraName", path + bim::CHANNEL_INFO_CAMERA);

        path = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), 1);
        hash->set_value(path + bim::CHANNEL_INFO_NAME, "Green");
        hash->set_value(path + bim::CHANNEL_INFO_COLOR, "0,1.0,0");
        hash->set_value(path + bim::CHANNEL_INFO_MODALITY, modality);
        hash->set_value_from_old_key("Akoya/CameraName", path + bim::CHANNEL_INFO_CAMERA);

        path = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), 2);
        hash->set_value(path + bim::CHANNEL_INFO_NAME, "Blue");
        hash->set_value(path + bim::CHANNEL_INFO_COLOR, "0,0,1.0");
        hash->set_value(path + bim::CHANNEL_INFO_MODALITY, modality);
        hash->set_value_from_old_key("Akoya/CameraName", path + bim::CHANNEL_INFO_CAMERA);
    } else {
        for (int c = 0; c < qp->num_sep_channels; ++c) {
            bim::xstring path = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), c);
            bim::xstring pin = xstring::xprintf("%.5d", c);

            hash->set_value(path + bim::CHANNEL_INFO_MODALITY, modality);
            hash->set_value_from_external(pin + "/CameraName", qp->meta, path + bim::CHANNEL_INFO_CAMERA);
            hash->set_value_from_external(pin + "/ExposureTime", qp->meta, path + bim::CHANNEL_INFO_EXPOSURE);
            hash->set_value(path + bim::CHANNEL_INFO_EXPOSURE_UNITS, "us");

            hash->set_value_from_external(pin + "/Name", qp->meta, path + bim::CHANNEL_INFO_NAME);
            hash->set_value_from_external(pin + "/Responsivity/Filter/FilterID", qp->meta, path + bim::CHANNEL_INFO_FILTER_NAME);
            hash->set_value_from_external(pin + "/Responsivity/Filter/Date", qp->meta, path + bim::CHANNEL_INFO_DATETIME);
            hash->set_value_from_external(pin + "/LampType", qp->meta, path + bim::CHANNEL_INFO_LAMP);
            hash->set_value_from_external(pin + "/InstrumentType", qp->meta, path + bim::CHANNEL_INFO_INSTRUMENT);
            hash->set_value_from_external(pin + "/CameraType", qp->meta, path + bim::CHANNEL_INFO_CAMERA);
            hash->set_value_from_external(pin + "/Responsivity/Filter/Response", qp->meta, path + bim::CHANNEL_INFO_RESPONSE);
            
            bim::xstring Name = hash->get_value(path + bim::CHANNEL_INFO_NAME);
            bim::xstring Biomarker = qp->meta.get_value(pin + "/Biomarker");
            if (Biomarker.size() > 0) {
                hash->set_value(path + bim::CHANNEL_INFO_BIOMARKER, Biomarker);
            } 
            if (Biomarker.size() > 0 && Name.size()>0) {
                hash->set_value(path + bim::CHANNEL_INFO_DYE, Name);
                hash->set_value(path + bim::CHANNEL_INFO_FLUOR, Name);
                hash->set_value(path + bim::CHANNEL_INFO_NAME, xstring::xprintf("%s (%s)", Biomarker.c_str(), Name.c_str()));
            } 

            hash->set_value_from_external(pin + "/Responsivity/Filter/Name", qp->meta, path + bim::CHANNEL_INFO_DYE);
            hash->set_value_from_external(pin + "/Responsivity/Filter/Name", qp->meta, path + bim::CHANNEL_INFO_FLUOR);


            //hash->set_value_from_external(pin + "/Color", qp->meta, path + bim::CHANNEL_INFO_COLOR);
            hash->set_value_from_external(pin + "/Color", qp->meta, path + "color_original");
            std::vector<int> rgb = qp->meta.get_value(pin + "/Color").splitInt(",");
            if (rgb.size()>=3) 
                hash->set_value(path + bim::CHANNEL_INFO_COLOR, bim::xstring::xprintf("%.2f,%.2f,%.2f", rgb[0] / 255.0, rgb[1] / 255.0, rgb[2] / 255.0));
         
            if (bits_actual == 8 && pixel_format == DataFormat::FMT_UNSIGNED) {
                hash->set_value(path + bim::CHANNEL_INFO_COLOR_RANGE, "0,255");
            } else if (bits_actual == 16 && pixel_format == DataFormat::FMT_UNSIGNED) {
                int low = 0;
                int high = int(std::pow((float)2.0, (float)bits) - 1);
                hash->set_value(path + bim::CHANNEL_INFO_COLOR_RANGE, bim::xstring::xprintf("%d,%d", low, high));
            }
        }
    }

    if (qp->num_sep_channels > 1) {
        hash->append_tags(qp->meta_channels, "Akoya/.Bands/");
    }

    return 0;
}
