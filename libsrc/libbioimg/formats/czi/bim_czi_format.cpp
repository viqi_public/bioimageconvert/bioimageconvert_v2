/*****************************************************************************
  CZI support
  Copyright (c) 2017, Center for Bio-Image Informatics, UCSB
  Copyright (c) 2017, Dmitry Fedorov <www.dimin.net> <dima@dimin.net>

  Author: Dmitry Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

  History:
  2017-10-25 11:50:40 - First creation

  ver : 1
  *****************************************************************************/

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include <bim_format_misc.h>
#include <bim_lcms_parse.h>
#include <bim_metatags.h>
#include <tag_map.h>
#include <xconf.h>
#include <xstring.h>
#include <xtypes.h>
#include <xunits.h>
#ifdef BIM_USE_EXIV2
#include <bim_exiv_parse.h>
#endif

#include "bim_czi_format.h"

// wrapping libCZI in order to build proper static/dynamic links
#if defined WIN32 //&& defined _DEBUG
#define _STATICLIBBUILD 1
#define _LIBCZISTATICLIB 1
#define _LIB 1
#endif

#include <libCZI.h>

#if defined _STATICLIBBUILD
#undef _STATICLIBBUILD
#endif
#if defined _LIB
#undef _LIB
#endif
// wrapping libCZI in order to build proper static/dynamic links

#define pugi pugim // libCZI loads its own and incompatible instance of pugixml, we use a different namespace
#include <pugixml.hpp>

using namespace bim;

//****************************************************************************
// serialization
//****************************************************************************

namespace bim {

const char spc_vector[4] = { 'C', 'Z', 'V', '1' };
const char spc_set[4] = { 'C', 'Z', 'S', '1' };
const char spc_map[4] = { 'C', 'Z', 'M', '1' };
const char spc_block[4] = { 'C', 'Z', 'B', '1' };

template<typename T>
void write(const T &x, std::ostream *s) {
    s->write((const char *)&x, sizeof(T));
}

template<typename T>
void write(const std::vector<T> &x, std::ostream *s) {
    s->write(spc_vector, 4);
    write<bim::uint64>(x.size(), s);
    s->write((const char *)&x[0], sizeof(T) * x.size());
}

template<typename T>
void write(const std::set<T> &x, std::ostream *s) {
    s->write(spc_set, 4);
    write<bim::uint64>(x.size(), s);
    for (typename std::set<T>::const_iterator it = x.begin(); it != x.end(); ++it) {
        write<T>(*it, s);
    }
}

template<typename Tk, typename T>
void write(const std::map<Tk, T> &x, std::ostream *s) {
    s->write(spc_map, 4);
    write<bim::uint64>(x.size(), s);
    for (typename std::map<Tk, T>::const_iterator it = x.begin(); it != x.end(); ++it) {
        write<Tk>(it->first, s);
        write<T>(it->second, s);
    }
}

template<>
void write(const std::vector<CZIBlock> &x, std::ostream *s) {
    s->write(spc_block, 4);
    write<bim::uint64>(x.size(), s);
    s->write((const char *)&x[0], sizeof(CZIBlock) * x.size());
}


template<typename T>
void read(T *x, std::istream *s) {
    s->read((char *)x, sizeof(T));
}

template<typename T>
void read(std::vector<T> *x, std::istream *s) {
    char spc[4];
    s->read(spc, 4);
    if (memcmp(spc, spc_vector, sizeof(spc_vector)) != 0) return;

    bim::uint64 sz = 0;
    s->read((char *)&sz, 8);

    x->resize(sz);
    s->read((char *)&x->at(0), sizeof(T) * sz);
}

template<typename T>
void read(std::set<T> *x, std::istream *s) {
    char spc[4];
    s->read(spc, 4);
    if (memcmp(spc, spc_set, sizeof(spc_set)) != 0) return;

    bim::uint64 sz = 0;
    s->read((char *)&sz, 8);

    T v;
    for (int i = 0; i < sz; ++i) {
        read<T>(&v, s);
        x->insert(v);
    }
}

template<typename Tk, typename T>
void read(std::map<Tk, T> *x, std::istream *s) {
    char spc[4];
    s->read(spc, 4);
    if (memcmp(spc, spc_map, sizeof(spc_map)) != 0) return;

    bim::uint64 sz = 0;
    s->read((char *)&sz, 8);

    Tk k;
    T v;
    for (int i = 0; i < sz; ++i) {
        read<Tk>(&k, s);
        read<T>(&v, s);
        x->insert(std::make_pair(k, v));
    }
}

template<>
void read(std::vector<CZIBlock> *x, std::istream *s) {
    char spc[4];
    s->read(spc, 4);
    if (memcmp(spc, spc_block, sizeof(spc_block)) != 0) return;

    bim::uint64 sz = 0;
    s->read((char *)&sz, 8);

    x->resize(sz);
    s->read((char *)&x->at(0), sizeof(CZIBlock) * x->size());
}

} // namespace bim

//****************************************************************************
// bim::CZIBlock
//****************************************************************************

bim::CZIBlock::CZIBlock(const libCZI::SubBlockInfo &bi, const int &idx) {
    this->idx = idx;
    if (bi.mIndex >= 0) this->mindx = bi.mIndex;
    this->scale = bi.GetZoom();

    this->logical_x = bi.logicalRect.x;
    this->logical_y = bi.logicalRect.y;
    this->logical_w = bi.logicalRect.w;
    this->logical_h = bi.logicalRect.h;
    this->physical_w = bi.physicalSize.w;
    this->physical_h = bi.physicalSize.h;

    this->pixel_type = (bim::uint64)bi.pixelType;

    libCZI::CDimCoordinate coord = bi.coordinate;
    int value;
    if (coord.TryGetPosition(libCZI::DimensionIndex::Z, &value))
        this->Z = (bim::float32)value;
    if (coord.TryGetPosition(libCZI::DimensionIndex::C, &value))
        this->C = (bim::float32)value;
    if (coord.TryGetPosition(libCZI::DimensionIndex::T, &value))
        this->T = (bim::float32)value;
    if (coord.TryGetPosition(libCZI::DimensionIndex::R, &value))
        this->R = (bim::float32)value;
    if (coord.TryGetPosition(libCZI::DimensionIndex::S, &value))
        this->S = (bim::float32)value;
    if (coord.TryGetPosition(libCZI::DimensionIndex::I, &value))
        this->I = (bim::float32)value;
    if (coord.TryGetPosition(libCZI::DimensionIndex::H, &value))
        this->H = (bim::float32)value;
    if (coord.TryGetPosition(libCZI::DimensionIndex::V, &value))
        this->V = (bim::float32)value;
};

//****************************************************************************
// bim::CZIBlocks
//****************************************************************************

//bim::CZIBlocks::CZIBlocks() {}
//bim::CZIBlocks::~CZIBlocks() {}

void CZIBlocks::add(const CZIBlock &block) {
    this->blocks.push_back(block);
    // dima: init search trees here when needed
}

const CZIBlock &CZIBlocks::get(const int &idx) const {
    return this->blocks[idx];
}

std::vector<CZIBlock *> CZIBlocks::find(int mindx, double scale, float c, float z, float t, float r, float s, float i, float h, float v) const {
    std::vector<CZIBlock *> res;

    // dima: use trees if this gets slow
    // dima: brute force our way for now
    for (std::vector<CZIBlock>::const_iterator it = this->blocks.begin(); it != this->blocks.end(); ++it) {
        CZIBlock *block = (CZIBlock *)&(*it);
        if (mindx >= 0 && block->mindx >= 0 && block->mindx != mindx) continue;
        if (mindx >= 0 && block->mindx < 0 && block->idx >= 0 && block->idx != mindx) continue;
        if (!bim::isnan<double>(scale) && block->scale != scale) continue;
        if (!bim::isnan<float>(c) && block->C != c) continue;
        if (!bim::isnan<float>(z) && block->Z != z) continue;
        if (!bim::isnan<float>(t) && block->T != t) continue;
        if (!bim::isnan<float>(r) && block->R != r) continue;
        if (!bim::isnan<float>(s) && block->S != s) continue;
        if (!bim::isnan<float>(i) && block->I != i) continue;
        if (!bim::isnan<float>(h) && block->H != h) continue;
        if (!bim::isnan<float>(v) && block->V != v) continue;

        res.push_back(block);
    }
    return res;
}

bool bim::CZIBlocks::to(std::ostream *s) const {
    bim::write<CZIBlock>(this->blocks, s);
    return true;
}

bool bim::CZIBlocks::from(std::istream *s) {
    bim::read<CZIBlock>(&this->blocks, s);
    return true;
}

//****************************************************************************
// bim::CZIOverview
//****************************************************************************

/*
Histogram binary content:
0x00 'BIM1' - 4 bytes header
0x04 'CZO1' - 4 bytes spec
0x07        - XX bytes CZIOverview variables
0xXX NUM    - 1xUINT64 number of elements in Block list
0xXX        - histogram vector Block * NUM
*/

bool bim::CZIOverview::to(const std::string &fileName) const {
    std::ofstream f(fileName.c_str(), std::ios_base::binary);
    return this->to(&f);
}

bool bim::CZIOverview::to(std::ostream *s) const {
    // write header
    s->write(bim::CZIOverview::mgk, sizeof(bim::CZIOverview::mgk));
    s->write(bim::CZIOverview::spc, sizeof(bim::CZIOverview::spc));

    // write vars
    bim::write<double>(this->scales_virtual, s);
    bim::write<double>(this->scales, s);
    bim::write<double, int>(this->tiles_at_scales, s);

    bim::write<int>(this->mindices, s);

    bim::write<int>(this->zs, s);
    bim::write<int>(this->cs, s);
    bim::write<int>(this->ts, s);
    bim::write<int>(this->rotations, s);
    bim::write<int>(this->scenes, s);
    bim::write<int>(this->illuminations, s);
    bim::write<int>(this->phases, s);
    bim::write<int>(this->views, s);

    bim::write<int>(this->x0, s);
    bim::write<int>(this->y0, s);
    bim::write<int>(this->x1, s);
    bim::write<int>(this->y1, s);

    bim::write<int>(this->fov_width, s);
    bim::write<int>(this->fov_height, s);
    bim::write<int>(this->image_width, s);
    bim::write<int>(this->image_height, s);
    bim::write<int>(this->num_fovs, s);

    // write blocks
    this->blocks.to(s);

    s->flush();
    return true;
}

bool bim::CZIOverview::from(const std::string &fileName) {
    std::ifstream f(fileName.c_str(), std::ios_base::binary);
    return this->from(&f);
}

bool bim::CZIOverview::from(std::istream *s) {
    // read header
    char spc[4];
    s->read(spc, 4);
    if (memcmp(spc, bim::CZIOverview::mgk, sizeof(bim::CZIOverview::mgk)) != 0) return false;
    s->read(spc, 4);
    if (memcmp(spc, bim::CZIOverview::spc, sizeof(bim::CZIOverview::spc)) != 0) return false;

    // read vars
    bim::read<double>(&this->scales_virtual, s);
    bim::read<double>(&this->scales, s);
    bim::read<double, int>(&this->tiles_at_scales, s);

    bim::read<int>(&this->mindices, s);

    bim::read<int>(&this->zs, s);
    bim::read<int>(&this->cs, s);
    bim::read<int>(&this->ts, s);
    bim::read<int>(&this->rotations, s);
    bim::read<int>(&this->scenes, s);
    bim::read<int>(&this->illuminations, s);
    bim::read<int>(&this->phases, s);
    bim::read<int>(&this->views, s);

    bim::read<int>(&this->x0, s);
    bim::read<int>(&this->y0, s);
    bim::read<int>(&this->x1, s);
    bim::read<int>(&this->y1, s);

    bim::read<int>(&this->fov_width, s);
    bim::read<int>(&this->fov_height, s);
    bim::read<int>(&this->image_width, s);
    bim::read<int>(&this->image_height, s);
    bim::read<int>(&this->num_fovs, s);

    // read blocks
    this->blocks.from(s);

    return true;
}

std::vector<CZIBlock *> bim::CZIOverview::boundary() const {
    std::vector<CZIBlock *> res;
    std::vector<CZIBlock *> blocks = this->blocks.find(-1, 1.0, 0.0);
    int xD = this->fov_width / 10;
    int yD = this->fov_height / 10;
    int x0 = this->x0 + xD;
    int x1 = this->x1 - this->fov_width - xD;
    int y0 = this->y0 + yD;
    int y1 = this->y1 - this->fov_height - yD;
    for (int i = 0; i < blocks.size(); ++i) {
        CZIBlock *b = blocks.at(i);
        if (b->logical_x > x0 && b->logical_x < x1 && b->logical_y > y0 && b->logical_y < y1) continue;
        res.push_back(b);
    }
    return res;
}

//----------------------------------------------------------------------------
// CZI stream
//----------------------------------------------------------------------------

namespace bim {

class CZIStreamImpl : public libCZI::IStream {
private:
    std::ifstream infile;

public:
    CZIStreamImpl() = delete;
    CZIStreamImpl(const char *filename); // linux only utf-8 version
    ~CZIStreamImpl();

public: // interface libCZI::IStream
    virtual void Read(std::uint64_t offset, void *pv, std::uint64_t size, std::uint64_t *ptrBytesRead);
};

} // namespace bim

bim::CZIStreamImpl::CZIStreamImpl(const char *filename) {
    this->infile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    this->infile.open(filename, std::ios::binary | std::ios::in); // linux only utf-8 version
}

bim::CZIStreamImpl::~CZIStreamImpl() {
    this->infile.close();
}

void bim::CZIStreamImpl::Read(std::uint64_t offset, void *pv, std::uint64_t size, std::uint64_t *ptrBytesRead) {
    this->infile.seekg(offset, std::ios::beg);
    this->infile.read((char *)pv, size);
    if (ptrBytesRead != nullptr) {
        *ptrBytesRead = this->infile.gcount();
    }
}


//****************************************************************************
// bim::CZIParams
//****************************************************************************

bim::CZIParams::CZIParams() {
    //i = ImageInfo();
}

bim::CZIParams::~CZIParams() {
    if (this->cziReader) {
        this->cziReader->Close();
        this->cziReader.reset();
    }
    if (this->stream) {
        this->stream.reset();
    }
}

void bim::CZIParams::open(const char *filename, bim::ImageIOModes io_mode) {
#if defined(BIM_WIN)
    bim::xstring fn(filename);
    this->stream = libCZI::CreateStreamFromFile((const wchar_t *)fn.toUTF16().c_str());
#else
    this->stream = std::make_shared<bim::CZIStreamImpl>(filename);
#endif

    this->cziReader = libCZI::CreateCZIReader();
    this->cziReader->Open(this->stream);
}

//----------------------------------------------------------------------------
// utils
//----------------------------------------------------------------------------

void init_pixel_type(ImageInfo *info, libCZI::PixelType &pt, bim::ImageModes mode = bim::ImageModes::IM_MULTI, size_t samples = 1) {
    info->imageMode = mode;
    info->samples = (bim::uint32)samples;
    if (pt == libCZI::PixelType::Gray8) {
        info->depth = 8;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    } else if (pt == libCZI::PixelType::Gray16) {
        info->depth = 16;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    } else if (pt == libCZI::PixelType::Gray32) {
        info->depth = 32;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    } else if (pt == libCZI::PixelType::Gray32Float) {
        info->depth = 32;
        info->pixelType = bim::DataFormat::FMT_FLOAT;
    } else if (pt == libCZI::PixelType::Gray64Float) {
        info->depth = 64;
        info->pixelType = bim::DataFormat::FMT_FLOAT;
    } else if (pt == libCZI::PixelType::Gray64ComplexFloat) {
        info->depth = 64;
        info->pixelType = bim::DataFormat::FMT_COMPLEX;
    } else if (pt == libCZI::PixelType::Bgr192ComplexFloat) {
        info->imageMode = bim::ImageModes::IM_RGB;
        info->depth = 192;
        info->pixelType = bim::DataFormat::FMT_COMPLEX;
    } else if (pt == libCZI::PixelType::Bgr24) {
        info->imageMode = bim::ImageModes::IM_RGB;
        info->depth = 8;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
        info->samples = 3;
    } else if (pt == libCZI::PixelType::Bgr48) {
        info->imageMode = bim::ImageModes::IM_RGB;
        info->depth = 16;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
        info->samples = 3;
    } else if (pt == libCZI::PixelType::Bgr96Float) {
        info->imageMode = bim::ImageModes::IM_RGB;
        info->depth = 32;
        info->pixelType = bim::DataFormat::FMT_FLOAT;
        info->samples = 3;
    } else if (pt == libCZI::PixelType::Bgra32) {
        info->imageMode = bim::ImageModes::IM_RGBA;
        info->depth = 8;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
        info->samples = 4;
    }
}

//****************************************************************************
// required funcs
//****************************************************************************

#define BIM_FORMAT_CZI_MAGIC_SIZE 10
const char czi_magic_number[11] = "ZISRAWFILE";

int cziValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    if (length < BIM_FORMAT_CZI_MAGIC_SIZE) return -1;
    unsigned char *mag_num = (unsigned char *)magic;
    if (memcmp(mag_num, czi_magic_number, BIM_FORMAT_CZI_MAGIC_SIZE) == 0) return 0;
    return -1;
}

FormatHandle cziAquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void cziCloseImageProc(FormatHandle *fmtHndl);
void cziReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    cziCloseImageProc(fmtHndl);
}


//----------------------------------------------------------------------------
// OPEN/CLOSE
//----------------------------------------------------------------------------

void cziReadAttachmentInfo(FormatHandle *fmtHndl, int idx) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    if (idx < 0) return;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    CZIOverview *o = &par->o;
    ImageInfo *info = &par->i;

    // attachmnets are stored as embedded CZI images, init a new stream and reader
    std::shared_ptr<libCZI::IAttachment> attachment = par->cziReader->ReadAttachment(idx);
    std::shared_ptr<libCZI::IStream> stream = libCZI::CreateStreamFromMemory(attachment.get());
    std::shared_ptr<libCZI::ICZIReader> reader = libCZI::CreateCZIReader();
    reader->Open(stream);

    // read the attachment block image
    std::shared_ptr<libCZI::ISubBlock> sbBlk = reader->ReadSubBlock(0);
    std::shared_ptr<libCZI::IBitmapData> ibmp = sbBlk->CreateBitmap();
    libCZI::PixelType pt = ibmp->GetPixelType();
    libCZI::IntSize sz = ibmp->GetSize();

    // copy image
    info->width = sz.w;
    info->height = sz.h;
    init_pixel_type(info, pt, bim::ImageModes::IM_MULTI, 1);
    if (info->samples == 3)
        info->imageMode = bim::ImageModes::IM_RGB;
    info->number_z = 1;
    info->number_t = 1;
    info->number_pages = 1;
    info->number_dims = 2;
}

void cziGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    //*info = ImageInfo();
    CZIOverview *o = &par->o;
    XConf *conf = fmtHndl->arguments;

    if (conf)
        par->path = conf->getValue("-path");

    // enumerate attachments
    if (!par->attachments_loaded) {
        par->cziReader->EnumerateAttachments([&par](int idx, const libCZI::AttachmentInfo &ai) {
            if (bim::xstring(ai.contentFileType) == "CZI") {
                try {
                    if (ai.name == "Label")
                        par->label_idx = idx;
                    else if (ai.name == "SlidePreview")
                        par->preview_idx = idx;
                } catch (...) {
                    if (par->label_idx < 0)
                        par->label_idx = idx;
                    else if (par->preview_idx < 0)
                        par->preview_idx = idx;
                }
            }

            return true;
        }); // enumerate attachments
        par->attachments_loaded = true;
    }

    // dima: use path here to overwrite some info
    if (conf && conf->is_path_requested("/preview")) {
        return cziReadAttachmentInfo(fmtHndl, par->preview_idx);
    } else if (conf && conf->is_path_requested("/label")) {
        return cziReadAttachmentInfo(fmtHndl, par->label_idx);
    }

    /*
    // dima: these estimates and info are not quite correct
    libCZI::SubBlockStatistics statistics = par->cziReader->GetStatistics();
    //libCZI::PyramidStatistics stats_pyr = par->cziReader->GetPyramidStatistics();
    if (statistics.subBlockCount < 1) return;
    // get image dimensions
    libCZI::IntRect *r = &statistics.boundingBox;
    info->width = r->w;
    info->height = r->h;
    */

    bool used_speed = false;
    if (conf && conf->keyExists("-speed") && !o->is_loaded()) {
        o->from(conf->getValue("-speed"));
        if (o->is_loaded())
            used_speed = true;
    }

    if (!o->is_loaded()) {
        par->cziReader->EnumerateSubBlocks([&o](int idx, const libCZI::SubBlockInfo &bi) {
            CZIBlock block(bi, idx);
            o->blocks.add(block);

            std::map<double, int>::iterator it = o->tiles_at_scales.find(block.scale);
            if (it != o->tiles_at_scales.end()) {
                it->second += 1;
            } else {
                o->tiles_at_scales.insert(std::pair<double, int>(block.scale, 1));
            }
            o->scales.insert(block.scale);

            if (block.scale == 1.0) {
                o->x0 = bim::min<int>(o->x0, static_cast<int>(block.logical_x));
                o->y0 = bim::min<int>(o->y0, static_cast<int>(block.logical_y));
                o->x1 = bim::max<int>(o->x1, static_cast<int>(block.logical_x + block.logical_w - 1));
                o->y1 = bim::max<int>(o->y1, static_cast<int>(block.logical_y + block.logical_h - 1));
                o->fov_width = static_cast<int>(block.physical_w);
                o->fov_height = static_cast<int>(block.physical_h);

                if (bi.mIndex >= 0) o->mindices.insert(bi.mIndex);
            }

            if (!bim::isnan<float>(block.Z)) o->zs.insert(static_cast<int>(block.Z));
            if (!bim::isnan<float>(block.C)) o->cs.insert(static_cast<int>(block.C));
            if (!bim::isnan<float>(block.T)) o->ts.insert(static_cast<int>(block.T));
            if (!bim::isnan<float>(block.R)) o->rotations.insert(static_cast<int>(block.R));
            if (!bim::isnan<float>(block.S)) o->scenes.insert(static_cast<int>(block.S));
            if (!bim::isnan<float>(block.I)) o->illuminations.insert(static_cast<int>(block.I));
            if (!bim::isnan<float>(block.H)) o->phases.insert(static_cast<int>(block.H));
            if (!bim::isnan<float>(block.V)) o->views.insert(static_cast<int>(block.V));

            return true;
        });

        // dima: there's some disparity between enumerating tiles and what libCZI stats are returning
        // though our size is the same with the metadata stored in the CZI XML
        o->image_width = o->x1 - o->x0 + 1;
        o->image_height = o->y1 - o->y0 + 1;

        o->num_fovs = static_cast<int>(o->mindices.size() / bim::max<size_t>(1, o->scenes.size()));
        if (o->num_fovs < 1) {
            // mindices were not defined, estimate from actual blocks
            std::map<double, int>::iterator it = o->tiles_at_scales.find(1.0);
            if (it != o->tiles_at_scales.end()) {
                o->num_fovs = it->second;
                o->num_fovs /= bim::max<int>(1, static_cast<int>(o->cs.size()));
                o->num_fovs /= bim::max<int>(1, static_cast<int>(o->zs.size()));
                o->num_fovs /= bim::max<int>(1, static_cast<int>(o->ts.size()));
                o->num_fovs /= bim::max<int>(1, static_cast<int>(o->rotations.size()));
                o->num_fovs /= bim::max<int>(1, static_cast<int>(o->illuminations.size()));
                o->num_fovs /= bim::max<int>(1, static_cast<int>(o->phases.size()));
                o->num_fovs /= bim::max<int>(1, static_cast<int>(o->views.size()));
            }
        }

        // the pyramid structure is virtual and so the scales are also virtual
        double scale = 1.0;
        bim::int64 sz = bim::max<bim::int64>(o->image_width, o->image_height);
        while (sz > 10) {
            o->scales_virtual.push_back(scale);
            scale /= 2.0;
            sz /= 2;
        }
    } // enumerate blocks

    info->width = o->image_width;
    info->height = o->image_height;

    info->number_z = bim::max<size_t>(1, o->zs.size());
    info->number_t = bim::max<size_t>(1, o->ts.size());

    //info->number_pages = 1;
    info->number_pages = info->number_z * info->number_t;
    info->number_dims = 2;
    if (info->number_z > 1) ++info->number_dims;
    if (info->number_t > 1) ++info->number_dims;

    if (o->scales.size() > 1) {
        info->tileWidth = bim::CZIParams::virtual_tile_width;   // virtual tile size imitating the hierarchical pyramid
        info->tileHeight = bim::CZIParams::virtual_tile_height; // virtual tile size imitating the hierarchical pyramid
        info->number_levels = o->scales_virtual.size();
    }

    libCZI::PixelType pt = (libCZI::PixelType)o->blocks.get(0).pixel_type;
    init_pixel_type(info, pt, bim::ImageModes::IM_MULTI, bim::max<size_t>(1, o->cs.size()));
    // preserve image parameters since preview and label may reset these
    par->imageMode = info->imageMode;
    par->depth = info->depth;
    par->pixelType = info->pixelType;
    par->samples = info->samples;
    //enum class CompressionMode : std::uint8_t

    info->resUnits = bim::ResolutionUnits::RES_IN;
    info->xRes = 0;
    info->yRes = 0;

    if (!used_speed && conf && conf->keyExists("-speed")) {
        o->to(conf->getValue("-speed"));
    }
}

void cziCloseImageProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    xclose(fmtHndl);
    if (fmtHndl->internalParams == NULL) return;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    fmtHndl->internalParams = 0;
    delete par;
}

bim::uint cziOpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams != NULL) cziCloseImageProc(fmtHndl);
    bim::CZIParams *par = new bim::CZIParams();
    fmtHndl->internalParams = (void *)par;

    if (io_mode == bim::ImageIOModes::IO_WRITE) {
        return 1;
    }

    try {
        par->open(fmtHndl->fileName, io_mode);
        cziGetImageInfo(fmtHndl);
    } catch (...) {
        cziCloseImageProc(fmtHndl);
        return 1;
    }

    return 0;
}


//----------------------------------------------------------------------------
// INFO for OPEN image
//----------------------------------------------------------------------------

bim::uint cziGetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    return (bim::uint)info->number_pages;
}

ImageInfo cziGetImageInfoProc(FormatHandle *fmtHndl, bim::uint page_num) {
    if (fmtHndl == NULL) return ImageInfo();
    fmtHndl->pageNumber = page_num;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    XConf *conf = fmtHndl->arguments;
    if (conf && par->path != conf->getValue("-path")) {
        cziGetImageInfo(fmtHndl);
    }
    return par->i;
}


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

void walker(pugi::xml_node node, bim::xstring path, TagMap *hash) {

    bim::xstring tag = node.name();
    if (tag.size() < 1) return;
    // use tag name
    path += "/" + tag; // +tag.replace(":", ".");

    // use Name and Id as special attributes that augment the path
    bim::xstring Name(node.attribute("Name").value());
    bim::xstring Id = node.attribute("Id").value();
    if (Id.size() > 0) {
        path += ":" + Id;
    } else if (Name.size() > 0) {
        path += ":" + Name;
    }

    // add text node as a value
    bim::xstring value = node.text().as_string();
    if (value.size() > 0)
        hash->set_value(path, value);

    // iterate over attributes
    for (pugi::xml_attribute_iterator ait = node.attributes_begin(); ait != node.attributes_end(); ++ait) {
        hash->set_value(path + "/" + ait->name(), ait->value());
    }

    // iterate over children
    for (pugi::xml_node child = node.first_child(); child; child = child.next_sibling()) {
        walker(child, path, hash);
    }
}

xstring get_value(pugi::xml_node *parent, const xstring &tag_from, TagMap *hash = NULL, const xstring &tag_to = "") {
    pugi::xml_node node = parent->child(tag_from.c_str());
    if (node.empty()) return "";
    bim::xstring value = node.text().as_string();
    if (hash && tag_to.size() > 0 && value.size() > 0)
        hash->set_value(tag_to, value);
    return value;
}

void parse_channels(FormatHandle *fmtHndl, TagMap *hash, pugi::xml_document *doc) {
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    /*
    <Channel Id="Channel:0" Name="DAPI">
        <ExcitationWavelength>353</ExcitationWavelength>
        <EmissionWavelength>465</EmissionWavelength>
        <DyeId>McNamara-Boswell-0434</DyeId>
        <DyeDatabaseId>66071726-cbd4-4c41-b371-0a6eee4ae9c5</DyeDatabaseId>
        <Color>#FF0000FF</Color>
        <Fluor>DAPI</Fluor>
        <AcquisitionMode>WideField</AcquisitionMode>
        <ExposureTime>53000000</ExposureTime>
        <IlluminationType>Epifluorescence</IlluminationType>
        <ContrastMethod>Fluorescence</ContrastMethod>
        <PixelType>Gray16</PixelType>
        <ComponentBitCount>16</ComponentBitCount>
        <DetectorSettings>
            <Binning>1,1</Binning>
            <EMGain>0</EMGain>
            <Detector Id="Detector:Hamamatsu Orca Flash"/>
        </DetectorSettings>
    </Channel>

    in display:
    <Channel Id="Channel:1" Name="Alexa Fluor 488">
        <Low>0.0024109254596780347</Low>
        <High>0.015243762874799725</High>
        <Gamma>0.51067250904232075</Gamma>
        <BitCountRange>12</BitCountRange>
        <DyeName>Alexa Fluor 488</DyeName>
        <ShortName>AF480</ShortName>
        <IlluminationType>Fluorescence</IlluminationType>
        <DyeMaxEmission>517</DyeMaxEmission>
        <DyeMaxExcitation>493</DyeMaxExcitation>
        <DyeId>McNamara-Boswell-0038</DyeId>
        <DyeDatabaseId>66071726-cbd4-4c41-b371-0a6eee4ae9c5</DyeDatabaseId>
        <Color>#FFFFFF00</Color>
        <OriginalColor>#FFFFFF00</OriginalColor>
    </Channel>

    older variant with most of meta in display and no correlation between ids:

    <Dimensions>/<Channels>/
    <Channel Id="Channel:0">
        <PixelType>Gray16</PixelType>
        <ComponentBitCount>12</ComponentBitCount>
    </Channel>

    <DisplaySetting>/<Channels>/
    <Channel Id="634366668200823702" Name="Membranes">
        <Low>0.0014854835964822222</Low>
        <High>0.027573052567330426</High>
        <Gamma>0.82637731318369845</Gamma>
        <UpperBestFitThreshold>1.0408340855860843E-17</UpperBestFitThreshold>
        <BitCountRange>12</BitCountRange>
        <DyeName>Alexa Fluor 350</DyeName>
        <ShortName>AF350</ShortName>
        <IlluminationType>Fluorescence</IlluminationType>
        <DyeMaxEmission>441</DyeMaxEmission>
        <DyeMaxExcitation>343</DyeMaxExcitation>
        <DyeId>McNamara-Boswell-0030</DyeId>
        <DyeDatabaseId>66071726-cbd4-4c41-b371-0a6eee4ae9c5</DyeDatabaseId>
        <Color>#FF0049FF</Color>
        <OriginalColor>#FF0049FF</OriginalColor>
    </Channel>

    */

    // compute channel map between ids in XML and enumerated channels
    std::map<std::string, int> ids;
    std::map<int, std::string> ids_disp;
    try {
        int pos = 0;
        pugi::xpath_node_set channels = doc->select_nodes("ImageDocument/Metadata/Information/Image/Dimensions/Channels/Channel");
        for (pugi::xpath_node_set::const_iterator it = channels.begin(); it != channels.end(); ++it) {
            pugi::xml_node node = it->node();
            std::string id = node.attribute("Id").value();
            ids.insert(std::make_pair(id, pos));
            ++pos;
        }
        pos = 0;
        channels = doc->select_nodes("ImageDocument/Metadata/DisplaySetting/Channels/Channel");
        for (pugi::xpath_node_set::const_iterator it = channels.begin(); it != channels.end(); ++it) {
            pugi::xml_node node = it->node();
            std::string id = node.attribute("Id").value();
            ids_disp.insert(std::make_pair(pos, id));
            ++pos;
        }
    } catch (...) {
        // do nothing
    }

    // BGR and BGRA data is stored in one sample and has only one channel description
    // we'll need to replicate these channels with propper color mapping info
    int passes = 1;
    if (info->imageMode == bim::ImageModes::IM_RGB)
        passes = 3;
    else if (info->imageMode == bim::ImageModes::IM_RGBA)
        passes = 4;
    for (int pass = 0; pass < passes; ++pass) {
        try {
            pugi::xpath_node_set channels = doc->select_nodes("ImageDocument/Metadata/Information/Image/Dimensions/Channels/Channel");
            for (pugi::xpath_node_set::const_iterator it = channels.begin(); it != channels.end(); ++it) {
                pugi::xml_node node = it->node();

                bim::xstring name = par->channel_names[pass];
                bim::xstring name_original;
                int id = pass;
                std::string id_channel = node.attribute("Id").value();
                if (passes == 1) {
                    // find channel id to use
                    std::map<std::string, int>::iterator it = ids.find(id_channel);
                    if (it != ids.end())
                        id = it->second;

                    // find display channel id to use
                    std::map<int, std::string>::iterator it2 = ids_disp.find(id);
                    if (it2 != ids_disp.end())
                        id_channel = it2->second;

                    name = node.attribute("Name").value();
                    name_original = name;
                }
                xstring path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), id);
                xstring xpath_display = bim::xstring::xprintf("ImageDocument/Metadata/DisplaySetting/Channels/Channel[@Id=\"%s\"]", id_channel.c_str());

                xstring color_display;
                try {
                    pugi::xml_node display = doc->select_node(xpath_display.c_str()).node();

                    xstring display_name = display.attribute("Name").value();
                    if (display_name.size() > 0 && display_name != name) {
                        if (name.size() > 0) name += " ";
                        name += display_name;
                    }

                    xstring dye_name = get_value(&display, "DyeName");
                    if (dye_name.size() > 0 && name.size() < 1)
                        name = dye_name;
                    else if (dye_name.size() > 0 && dye_name != name_original && dye_name != display_name)
                        name += " (" + dye_name + ")";

                    get_value(&display, "ShortName", hash, path + "name_short");
                    xstring lowval = get_value(&display, "Low");
                    xstring highval = get_value(&display, "High");
                    if (lowval.size() > 0 && highval.size())
                        hash->set_value(path + bim::CHANNEL_INFO_DISPLAY_RANGE, lowval + "," + highval);

                    color_display = get_value(&display, "Color");
                    double gamma = get_value(&display, "Gamma").toDouble(1.0);
                    hash->set_value(path + bim::CHANNEL_INFO_GAMMA, gamma);

                    // set values from old style display settings
                    get_value(&display, "DyeName", hash, path + bim::CHANNEL_INFO_FLUOR);
                    get_value(&display, "DyeMaxExcitation", hash, path + bim::CHANNEL_INFO_EX_WAVELENGTH);
                    get_value(&display, "DyeMaxEmission", hash, path + bim::CHANNEL_INFO_EM_WAVELENGTH);

                    get_value(&display, "DyeId", hash, path + "dye_id");
                    get_value(&display, "DyeDatabaseId", hash, path + "dye_database_id");

                    get_value(&display, "AcquisitionMode", hash, path + "acquisition_mode");
                    get_value(&display, "IlluminationType", hash, path + "illumination_type");
                } catch (pugi::xpath_exception) {
                    // do nothing
                }

                hash->set_value(path + bim::CHANNEL_INFO_NAME, name);

                pugi::xml_node node_exc = node.select_node("IlluminationWavelength/SinglePeak").node();
                std::string excstr = node_exc.text().as_string();
                if (excstr.size() > 0)
                    hash->set_value(path + bim::CHANNEL_INFO_EX_WAVELENGTH, excstr);

                get_value(&node, "Fluor", hash, path + bim::CHANNEL_INFO_FLUOR);
                get_value(&node, "ExcitationWavelength", hash, path + bim::CHANNEL_INFO_EX_WAVELENGTH);
                get_value(&node, "EmissionWavelength", hash, path + bim::CHANNEL_INFO_EM_WAVELENGTH);

                get_value(&node, "DyeId", hash, path + "dye_id");
                get_value(&node, "DyeDatabaseId", hash, path + "dye_database_id");

                get_value(&node, "AcquisitionMode", hash, path + "acquisition_mode");
                get_value(&node, "IlluminationType", hash, path + "illumination_type");

                // contrast method, needed to define the background color for tile composition
                //get_value(&node, "ContrastMethod", hash, path + "contrast_method");
                par->contrast_method = get_value(&node, "ContrastMethod");
                hash->set_value(path + "contrast_method", par->contrast_method);
                hash->set_value(path + bim::CHANNEL_INFO_MODALITY, par->contrast_method);

                // exposure
                xstring exposurestr = get_value(&node, "ExposureTime");
                if (exposurestr.size() > 0) {
                    double exposure = exposurestr.toDouble() / 1000000.0;
                    hash->set_value(path + bim::CHANNEL_INFO_EXPOSURE, exposure);
                    hash->set_value(path + bim::CHANNEL_INFO_EXPOSURE_UNITS, "ms");
                }

                // color
                bim::ColorF32 c = bim::ColorF32::from_RGBA(par->channel_colors[pass][0], par->channel_colors[pass][1], par->channel_colors[pass][2], par->channel_colors[pass][3]); 
                if (passes == 1) {
                    xstring colorstr = get_value(&node, "Color");
                    if (colorstr.size() > 0) {
                        c = bim::ColorF32::from_string_hex_ARGB(colorstr);
                        hash->set_value(path + "color_original", c.to_string_float());
                    }

                    if (color_display.size() == 9) {
                        c = bim::ColorF32::from_string_hex_ARGB(color_display);
                    } else if (color_display.size() > 0) {
                        c = bim::ColorF32::from_string_hex(color_display);
                    }
                }
                hash->set_value(path + bim::CHANNEL_INFO_COLOR, c.to_string_float());
                hash->set_value(path + bim::CHANNEL_INFO_OPACITY, c.getAlpha() / 255.0);
            }
        } catch (...) {
            // do nothing
        }
    } // for passes
}

void parse_image(FormatHandle *fmtHndl, TagMap *hash, pugi::xml_document *doc) {
    /*
    ImageDocument/Metadata/Information
    <Application>
        <Name>ZEN 2 (blue edition)</Name>
        <Version>2.0.0.0</Version>
    </Application>
    <Document>
        <CreationDate>2017-09-23T05:45:49.3743657-07:00</CreationDate>
        <UserName>cancer</UserName>
    </Document>
    ...

    <ImageDocument> / <Metadata> / <Experiment Version="1.1"> / <ExperimentBlocks> / <AcquisitionBlock> / <ProcessingGraph> / <Filters>
    ...
    <Filter FullTypeName="Zeiss.Micro.Acquisition.Processing.EpicAnalysisConnectorFilter" Name="EPICConnector" IsActive="true">
        <AnalysisDLL>
        C:\Program Files\Carl Zeiss\ZEN 2\ZEN 2 (blue edition)\Epic.AnalysisHost.dll
        </AnalysisDLL>
        <Argument>
        INSTRUMENT_NAME:Axio2;ASSAY_NAME:ARv7;FILTER_NAME:Epic.AnalysisFilters.ARv7.dll
        </Argument>
    </Filter>
    */

    try {
        pugi::xml_node application = doc->select_node("ImageDocument/Metadata/Information/Application").node();
        xstring app_name = get_value(&application, "Name");
        xstring app_ver = get_value(&application, "Version");
        hash->set_value(bim::DOCUMENT_APPLICATION, app_name + " v" + app_ver);

        pugi::xml_node document = doc->select_node("ImageDocument/Metadata/Information/Document").node();
        xstring datetime = get_value(&document, "CreationDate");
        hash->set_value(bim::DOCUMENT_DATETIME, datetime);

        get_value(&document, "Description", hash, bim::DOCUMENT_DESCRIPTION);
        get_value(&document, "UserName", hash, bim::DOCUMENT_USER);
        get_value(&document, "Keywords", hash, "document/keywords");

        pugi::xml_node filter = doc->select_node("ImageDocument/Metadata/Experiment/ExperimentBlocks/AcquisitionBlock/ProcessingGraph/Filters/Filter[@Name=\"EPICConnector\"]/Argument").node();
        bim::xstring arg = filter.text().as_string();
        std::vector<bim::xstring> args = arg.split(";");
        for (int i = 0; i < args.size(); ++i) {
            std::vector<bim::xstring> v = args[i].split(":");
            hash->set_value("document/" + v[0].toLowerCase(), v[1]);
        }

        pugi::xml_node barcode = doc->select_node("ImageDocument/Metadata/AttachmentInfos/AttachmentInfo/Label/Barcodes/Barcode[@Id=\"Barcode:1\"]").node();
        get_value(&barcode, "Content", hash, bim::DOCUMENT_SLIDE_ID);
    } catch (...) {
        // do nothing
    }
}

void parse_scale(FormatHandle *fmtHndl, TagMap *hash, pugi::xml_document *doc) {
    /*
    <Distance Id="X">
        <Value>6.5275724660967933E-07</Value>
        <DefaultUnitFormat>�m</DefaultUnitFormat>
    </Distance>
    */
    try {
        bim::xstring path_v = "pixel_resolution_";
        bim::xstring path_u = "pixel_resolution_unit_";
        pugi::xpath_node_set nodes = doc->select_nodes("ImageDocument/Metadata/Scaling/Items/Distance");
        for (pugi::xpath_node_set::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
            pugi::xml_node node = it->node();

            bim::xstring id = node.attribute("Id").value();
            double v = get_value(&node, "Value").toDouble();
            bim::xstring u = get_value(&node, "DefaultUnitFormat");
            if (u.size() < 1 && v < 0.001) {
                // a guess that typically the units are um
                u = "um";
            }
            v = bim::from_meters<double>(v, u); // the value is in meters and should be scaled to the default format

            hash->set_value(path_v + id.toLowerCase(), v);
            hash->set_value(path_u + id.toLowerCase(), u);
        }
    } catch (...) {
        // do nothing
    }
}

void parse_objective(FormatHandle *fmtHndl, TagMap *hash, pugi::xml_document *doc) {
    /*
    <Objectives>
        <Objective Id="Objective:1" Name="Plan-Apochromat 10x/0.45 M27">
            <LensNA>0.45</LensNA>
            <NominalMagnification>10</NominalMagnification>
            <WorkingDistance>2100</WorkingDistance>
            <PupilGeometry>Circular</PupilGeometry>
            <Immersion>Air</Immersion>

            May have:
            <Manufacturer>
                <Model>W Plan-Apochromat 20x/1.0 UV-VIS_4909000017</Model>
            </Manufacturer>

        </Objective>
    </Objectives>

    older style:
    <Information>/<Acquisition>/
    <PSF>
        <NAObjective>0.8</NAObjective>
        <DesignImmersionIndex>1</DesignImmersionIndex>
        <LateralMagnification>20</LateralMagnification>
        <OptovarMagnification>1</OptovarMagnification>
    */

    // compute objective offset for first element since objectives may start at 1 or other number
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    std::vector<int> ids;
    try {
        pugi::xpath_node_set nodes = doc->select_nodes("ImageDocument/Metadata/Information/Instrument/Objectives/Objective");
        for (pugi::xpath_node_set::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
            pugi::xml_node node = it->node();
            int id = xstring(node.attribute("Id").value()).replace("Objective:", "").toInt(1);
            ids.push_back(id);
        }
        // sort and get the first one
        if (ids.size() > 0) {
            std::sort(ids.begin(), ids.end());
            par->offset_meta_objective = ids[0];
        }
    } catch (...) {
        // do nothing
    }

    std::string xpath = "ImageDocument/Metadata/Information/Instrument/Objectives/Objective";
    if (ids.size() < 1) {
        // old style PSF definition
        xpath = "ImageDocument/Metadata/Information/Acquisition/PSF";
    }

    // parse objectives
    try {
        pugi::xpath_node_set nodes = doc->select_nodes(xpath.c_str());
        for (pugi::xpath_node_set::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
            pugi::xml_node node = it->node();

            int id = xstring(node.attribute("Id").value()).replace("Objective:", "").toInt(0) - par->offset_meta_objective;
            xstring path = bim::xstring::xprintf(bim::OBJECTIVE_INFO_TEMPLATE.c_str(), id);

            bim::xstring name = node.attribute("Name").value();
            double na = get_value(&node, "LensNA").toDouble();
            double mag = get_value(&node, "NominalMagnification").toDouble();
            hash->set_value(path + bim::OBJECTIVE_NUMERICAL_APERTURE, na);
            hash->set_value(path + bim::OBJECTIVE_MAGNIFICATION_X, mag);

            get_value(&node, "Immersion", hash, path + bim::OBJECTIVE_IMMERSION);
            get_value(&node, "PupilGeometry", hash, path + "pupil_geometry");
            get_value(&node, "WorkingDistance", hash, path + "working_distance");

            if (name.size() < 1) {
                pugi::xml_node node_model = node.select_node("Manufacturer/Model").node();
                name = node_model.text().as_string();
            }
            if (name.size() > 0)
                hash->set_value(path + bim::OBJECTIVE_NAME, name);
        }
    } catch (...) {
        // do nothing
    }
}

void parse_coordinates(FormatHandle *fmtHndl, TagMap *hash, pugi::xml_document *doc) {
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    CZIOverview *o = &par->o;

    hash->set_value(bim::COORDINATES_AXIS_ORIGIN, "top_left");
    hash->set_value(bim::COORDINATES_AXIS_ORIENTATION, "ascending,descending");
    hash->set_value(bim::COORDINATES_POINTS_TOPLEFT, bim::xstring::xprintf("%d,%d", o->x0, o->y0));
    hash->set_value(bim::COORDINATES_POINTS_BOTTOMRIGHT, bim::xstring::xprintf("%d,%d", o->x1, o->y1));

    int cnt_x = bim::round<int>(((double)o->x1 + o->x0 + 1) / 2.0);
    int cnt_y = bim::round<int>(((double)o->y1 + o->y0 + 1) / 2.0);
    hash->set_value(bim::COORDINATES_POINTS_CENTER, bim::xstring::xprintf("%d,%d", cnt_x, cnt_y));

    std::vector<bim::xstring> positions;
    std::vector<CZIBlock *> blocks = o->blocks.find(-1, 1.0, 0.0);
    for (int i = 0; i < blocks.size(); ++i) {
        CZIBlock *b = blocks.at(i);
        bim::xstring pt = bim::xstring::xprintf("%d,%d", b->logical_x, b->logical_y);
        positions.push_back( pt );
        //hash->set_value(bim::COORDINATES_POINTS_FOVS + bim::xstring::xprintf("/%.5d", i), pt);
    }
    hash->set_value(bim::COORDINATES_POSITIONS_FOVS, bim::xstring::join(positions, ";"));
}

bim::uint czi_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (!hash) return 1;
    if (isCustomReading(fmtHndl)) return 1;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    CZIOverview *o = &par->o;
    XConf *conf = fmtHndl->arguments;

    if (conf && conf->is_path_requested("/preview") || conf && conf->is_path_requested("/label")) {
        return 0;
    }

    // append image pyramid meta
    if (o->scales.size() > 1) {
        std::vector<double> scales(o->scales.begin(), o->scales.end());
        std::sort(scales.begin(), scales.end(), std::greater<double>());
        hash->set_value(bim::IMAGE_NUM_RES_L_ACTUAL, (int)scales.size());
        hash->set_value(bim::IMAGE_RES_L_SCALES_ACTUAL, xstring::join(scales, ","));

        // dima: virtual structure
        //hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_HIERARCHICAL);
        hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_ARBITRARY);
        hash->set_value(bim::IMAGE_NUM_RES_L, (int)o->scales_virtual.size());
        hash->set_value(bim::IMAGE_RES_L_SCALES, xstring::join(o->scales_virtual, ","));
    }

    // return all available dimensions in the image
    if (o->rotations.size() > 0) hash->set_value(bim::IMAGE_NUM_RT, (int)o->rotations.size());
    //if (o->scenes.size()>0) hash->set_value(bim::IMAGE_NUM_SC, (int)o->scenes.size()); // scenes are series in our definition
    if (o->illuminations.size() > 0) hash->set_value(bim::IMAGE_NUM_IL, (int)o->illuminations.size());
    if (o->phases.size() > 0) hash->set_value(bim::IMAGE_NUM_PH, (int)o->phases.size());
    if (o->views.size() > 0) hash->set_value(bim::IMAGE_NUM_VIEW, (int)o->views.size());

    hash->set_value(bim::IMAGE_NUM_SERIES, bim::max<unsigned int>(1, (unsigned int)o->scenes.size()));
    hash->set_value(bim::IMAGE_NUM_FOV, (int)o->num_fovs);
    hash->set_value("fov_width", (int)o->fov_width);
    hash->set_value("fov_height", (int)o->fov_height);
    hash->set_value("image_logical_x", (int)o->x0);
    hash->set_value("image_logical_y", (int)o->y0);

    // write all paths within the file
    int sub_images = 0;
    hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), 0), "/image");
    if (par->label_idx >= 0) {
        hash->set_value(bim::IMAGE_NUM_LABELS, 1);
        hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), ++sub_images), "/label");
    }
    if (par->preview_idx >= 0) {
        hash->set_value(bim::IMAGE_NUM_PREVIEWS, 1);
        hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), ++sub_images), "/preview");
    }

    // read xml metadata
    std::string xml;
    {
        std::shared_ptr<libCZI::IMetadataSegment> ms = par->cziReader->ReadMetadataSegment();
        size_t sz = 0;
        std::shared_ptr<const void> pxml = ms->GetRawData(libCZI::IMetadataSegment::XmlMetadata, &sz);
        xml.resize(sz+1);
        memcpy(&xml[0], pxml.get(), sz);
    }

    // parse the whole XML into tags
    pugi::xml_document doc;
    if (doc.load_buffer(xml.c_str(), xml.size())) {
        // parse specific elements
        parse_channels(fmtHndl, hash, &doc);
        parse_scale(fmtHndl, hash, &doc);
        parse_objective(fmtHndl, hash, &doc);
        parse_image(fmtHndl, hash, &doc);
        parse_coordinates(fmtHndl, hash, &doc);

        // parse all tags from the document
        if (conf && conf->hasKeyWith("-meta")) {
            pugi::xml_node child = doc.first_child();
            bim::xstring path = "CZI";
            walker(doc.first_child(), path, hash);
        }
    }


    /*
    if (par->buffer_icc.size()>0)
        hash->set_value(bim::RAW_TAGS_ICC, par->buffer_icc, bim::RAW_TYPES_ICC);
    if (par->buffer_xmp.size()>0)
        hash->set_value(bim::RAW_TAGS_XMP, par->buffer_xmp, bim::RAW_TYPES_XMP);
    if (par->buffer_iptc.size()>0)
        hash->set_value(bim::RAW_TAGS_IPTC, par->buffer_iptc, bim::RAW_TYPES_IPTC);
    if (par->buffer_photoshop.size()>0)
        hash->set_value(bim::RAW_TAGS_PHOTOSHOP, par->buffer_photoshop, bim::RAW_TYPES_PHOTOSHOP);

    czi_create_proper_exif(fmtHndl, hash); // parse and write proper EXIF block
    */

    /*
    // use LCMS2 to parse color profile
    lcms_append_metadata(fmtHndl, hash);

    // use EXIV2 to read metadata
    #ifdef BIM_USE_EXIV2
    exiv_append_metadata(fmtHndl, hash);
    #endif
    */
    return 0;
}

//----------------------------------------------------------------------------
// READ
//----------------------------------------------------------------------------

bim::uint samples_to_copy(ImageInfo *info) {
    if (info->imageMode == bim::ImageModes::IM_RGB) return 1;
    if (info->imageMode == bim::ImageModes::IM_RGBA) return 1;
    return info->samples;
}

float get_background_color(bim::CZIParams *par) {
    if (par->contrast_method != "Brightfield") return 0;
    CZIOverview *o = &par->o;

    libCZI::PixelType pt = (libCZI::PixelType)o->blocks.get(0).pixel_type;
    if (pt == libCZI::PixelType::Gray8)
        return std::numeric_limits<bim::uint8>::max();
    else if (pt == libCZI::PixelType::Gray16)
        return std::numeric_limits<bim::uint16>::max();
    else if (pt == libCZI::PixelType::Gray32)
        return (float)std::numeric_limits<bim::uint32>::max();
    else if (pt == libCZI::PixelType::Gray32Float)
        return std::numeric_limits<bim::float32>::max();
    else if (pt == libCZI::PixelType::Gray64Float)
        //return (float)std::numeric_limits<bim::float64>::max();
        return (float)std::numeric_limits<bim::float32>::max();
    else if (pt == libCZI::PixelType::Gray64ComplexFloat)
        return std::numeric_limits<bim::float32>::max();
    else if (pt == libCZI::PixelType::Bgr192ComplexFloat)
        //return (float)std::numeric_limits<bim::float64>::max();
        return (float)std::numeric_limits<bim::float32>::max();
    else if (pt == libCZI::PixelType::Bgr24)
        return std::numeric_limits<bim::uint8>::max();
    else if (pt == libCZI::PixelType::Bgr48)
        return std::numeric_limits<bim::uint16>::max();
    else if (pt == libCZI::PixelType::Bgr96Float)
        return std::numeric_limits<bim::float32>::max();
    else if (pt == libCZI::PixelType::Bgra32)
        return std::numeric_limits<bim::uint8>::max();
    return 0;
}

template<typename T>
void copy_bitmap_sample(const bim::uint64 &WW, const bim::uint64 &HH, const size_t sample, libCZI::IBitmapData *in, std::vector<void *> &out, ImageInfo *info) {
    bim::uint64 W = bim::min<bim::uint64>(WW, in->GetWidth());
    bim::uint64 H = bim::min<bim::uint64>(HH, in->GetHeight());
    libCZI::ScopedBitmapLocker<libCZI::IBitmapData *> bmp{ in }; // <- will call in->Lock here
    size_t stride_out = WW;
    size_t stride_in = bmp.stride;
    size_t line_sz = W * sizeof(T);

    if (info->imageMode == bim::ImageModes::IM_RGB) {
        memset(out[0], 0, WW * HH * sizeof(T)); // fill the output image with 0 in case it is larger than the red tile
        memset(out[1], 0, WW * HH * sizeof(T));
        memset(out[2], 0, WW * HH * sizeof(T));

        //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (W > BIM_OMP_FOR2 && H > BIM_OMP_FOR2)
        for (bim::int64 y = 0; y < (bim::int64)H; ++y) {
            const T *pin = (const T *)((const char *)bmp.ptrDataRoi + (y * stride_in));
            T *pr = ((T *)out[0]) + (y * stride_out);
            T *pg = ((T *)out[1]) + (y * stride_out);
            T *pb = ((T *)out[2]) + (y * stride_out);
            for (bim::int64 x = 0; x < (bim::int64)W; ++x) {
                pr[x] = pin[2];
                pg[x] = pin[1];
                pb[x] = pin[0];
                pin += 3;
            }
        } // for y
    } else if (info->imageMode == bim::ImageModes::IM_RGBA) {
        memset(out[0], 0, WW * HH * sizeof(T)); // fill the output image with 0 in case it is larger than the red tile
        memset(out[1], 0, WW * HH * sizeof(T));
        memset(out[2], 0, WW * HH * sizeof(T));
        memset(out[3], 0, WW * HH * sizeof(T));

        //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (W > BIM_OMP_FOR2 && H > BIM_OMP_FOR2)
        for (bim::int64 y = 0; y < (bim::int64)H; ++y) {
            const T *pin = (const T *)((const char *)bmp.ptrDataRoi + (y * stride_in));
            T *pr = ((T *)out[0]) + (y * stride_out);
            T *pg = ((T *)out[1]) + (y * stride_out);
            T *pb = ((T *)out[2]) + (y * stride_out);
            T *pa = ((T *)out[3]) + (y * stride_out);
            for (bim::int64 x = 0; x < (bim::int64)W; ++x) {
                pr[x] = pin[2];
                pg[x] = pin[1];
                pb[x] = pin[0];
                pa[x] = pin[3];
                pin += 4;
            }
        } // for y
    } else {
        memset(out[sample], 0, WW * HH * sizeof(T)); // fill the output image with 0 in case it is larger than the red tile

        //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (W > BIM_OMP_FOR2 && H > BIM_OMP_FOR2)
        for (bim::int64 y = 0; y < (bim::int64)H; ++y) {
            const T *pin = (const T *)((const char *)bmp.ptrDataRoi + (y * stride_in));
            T *p = ((T *)out[sample]) + (y * stride_out);
            memcpy(p, pin, line_sz);
        } // for y
    }
}

void init_coordinate(FormatHandle *fmtHndl, const bim::uint &page, int &mindx, double &scale, float &c, float &z, float &t, float &r, float &s, float &i, float &h, float &v) {
    fmtHndl->pageNumber = page;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    CZIOverview *o = &par->o;
    XConf *conf = fmtHndl->arguments;

    mindx = -1;
    scale = 1.0;
    c = std::numeric_limits<float>::quiet_NaN();
    z = std::numeric_limits<float>::quiet_NaN();
    t = std::numeric_limits<float>::quiet_NaN();
    r = std::numeric_limits<float>::quiet_NaN();
    s = std::numeric_limits<float>::quiet_NaN();
    i = std::numeric_limits<float>::quiet_NaN();
    h = std::numeric_limits<float>::quiet_NaN();
    v = std::numeric_limits<float>::quiet_NaN();

    if (conf && conf->keyExists("-slice-fov")) mindx = conf->getValueInt("-slice-fov");
    if (conf && conf->keyExists("-slice-z")) z = (float)conf->getValueInt("-slice-z");
    if (conf && conf->keyExists("-slice-t")) t = (float)conf->getValueInt("-slice-t");
    if (conf && conf->keyExists("-slice-rotation")) r = (float)conf->getValueInt("-slice-rotation");
    if (conf && conf->keyExists("-slice-serie")) s = (float)conf->getValueInt("-slice-serie");
    if (conf && conf->keyExists("-slice-illumination")) i = (float)conf->getValueInt("-slice-illumination");
    if (conf && conf->keyExists("-slice-phase")) h = (float)conf->getValueInt("-slice-phase");
    if (conf && conf->keyExists("-slice-view")) v = (float)conf->getValueInt("-slice-view");

    if (info->number_z > 1 && info->number_t <= 1 && bim::isnan<float>(z)) {
        z = (float)page;
    } else if (info->number_z <= 1 && info->number_t > 1 && bim::isnan<float>(t)) {
        t = (float)page;
    } else if (info->number_z > 1 && info->number_t > 1 && bim::isnan<float>(z) && bim::isnan<float>(t)) {
        t = (float)floor(page / info->number_z);
        z = static_cast<float>(page - t * info->number_z);
    }
}

void init_coordinate(FormatHandle *fmtHndl, const bim::uint &page, libCZI::CDimCoordinate &coord) {
    int mindx = 0;
    double scale = 1.0;
    float c, z, t, r, s, i, h, v;
    init_coordinate(fmtHndl, page, mindx, scale, c, z, t, r, s, i, h, v);

    if (!bim::isnan<float>(c)) coord.Set(libCZI::DimensionIndex::C, static_cast<int>(c));
    if (!bim::isnan<float>(z)) coord.Set(libCZI::DimensionIndex::Z, static_cast<int>(z));
    if (!bim::isnan<float>(t)) coord.Set(libCZI::DimensionIndex::T, static_cast<int>(t));
    if (!bim::isnan<float>(r)) coord.Set(libCZI::DimensionIndex::R, static_cast<int>(r));
    if (!bim::isnan<float>(s)) coord.Set(libCZI::DimensionIndex::S, static_cast<int>(s));
    if (!bim::isnan<float>(i)) coord.Set(libCZI::DimensionIndex::I, static_cast<int>(i));
    if (!bim::isnan<float>(h)) coord.Set(libCZI::DimensionIndex::H, static_cast<int>(h));
    if (!bim::isnan<float>(v)) coord.Set(libCZI::DimensionIndex::V, static_cast<int>(v));
}

bim::uint cziReadAttachmentAsImage(FormatHandle *fmtHndl, int idx) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (idx < 0) return 1;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    CZIOverview *o = &par->o;
    ImageInfo *info = &par->i;

    // attachmnets are stored as embedded CZI images, init a new stream and reader
    std::shared_ptr<libCZI::IAttachment> attachment = par->cziReader->ReadAttachment(idx);
    std::shared_ptr<libCZI::IStream> stream = libCZI::CreateStreamFromMemory(attachment.get());
    std::shared_ptr<libCZI::ICZIReader> reader = libCZI::CreateCZIReader();
    reader->Open(stream);

    // read the attachment block image
    std::shared_ptr<libCZI::ISubBlock> sbBlk = reader->ReadSubBlock(0);
    std::shared_ptr<libCZI::IBitmapData> ibmp = sbBlk->CreateBitmap();
    libCZI::PixelType pt = ibmp->GetPixelType();
    libCZI::IntSize sz = ibmp->GetSize();

    // copy image
    info->width = sz.w;
    info->height = sz.h;
    init_pixel_type(info, pt, bim::ImageModes::IM_MULTI, 1);
    if (info->samples == 3)
        info->imageMode = bim::ImageModes::IM_RGB;

    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    if (info->depth == 8)
        copy_bitmap_sample<bim::uint8>(info->width, info->height, 0, ibmp.get(), bmp->bits, info);
    else if (info->depth == 16)
        copy_bitmap_sample<bim::uint16>(info->width, info->height, 0, ibmp.get(), bmp->bits, info);
    else if (info->depth == 32)
        copy_bitmap_sample<bim::uint32>(info->width, info->height, 0, ibmp.get(), bmp->bits, info);
    else if (info->depth == 64)
        copy_bitmap_sample<bim::uint64>(info->width, info->height, 0, ibmp.get(), bmp->bits, info);

    return 0;
}

bim::uint cziReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    fmtHndl->pageNumber = page;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    CZIOverview *o = &par->o;
    XConf *conf = fmtHndl->arguments;

    // read label or preview images
    if (conf && conf->is_path_requested("/preview")) {
        return cziReadAttachmentAsImage(fmtHndl, par->preview_idx);
    } else if (conf && conf->is_path_requested("/label")) {
        return cziReadAttachmentAsImage(fmtHndl, par->label_idx);
    }

    // normal read
    int mindx = -1;
    double scale = 1.0;
    float c, z, t, r, s, i, h, v;
    init_coordinate(fmtHndl, page, mindx, scale, c, z, t, r, s, i, h, v);

    int blocks_expected = info->samples;
    if (info->imageMode == bim::ImageModes::IM_RGB || info->imageMode == bim::ImageModes::IM_RGBA) blocks_expected = 1;

    std::vector<CZIBlock *> blocks = o->blocks.find(mindx, scale, c, z, t, r, s, i, h, v);
    if (blocks.size() < blocks_expected) return 1; // found an improper number of blocks

    info->width = blocks[0]->logical_w;
    info->height = blocks[0]->logical_h;
    info->imageMode = par->imageMode;
    info->depth = par->depth;
    info->pixelType = par->pixelType;
    info->samples = par->samples;
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    for (int i = 0; i < blocks.size(); ++i) {
        CZIBlock *block = blocks[i];
        int s = static_cast<int>(block->C);
        if (s >= static_cast<int>(info->samples)) continue;

        std::shared_ptr<libCZI::ISubBlock> sbBlk = par->cziReader->ReadSubBlock(static_cast<int>(block->idx));
        std::shared_ptr<libCZI::IBitmapData> ibmp = sbBlk->CreateBitmap();

        if (info->depth == 8)
            copy_bitmap_sample<bim::uint8>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 16)
            copy_bitmap_sample<bim::uint16>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 32)
            copy_bitmap_sample<bim::uint32>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 64)
            copy_bitmap_sample<bim::uint64>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
    } // for sample

    return 0;
}

bim::uint cziReadImageLevelProc(FormatHandle *fmtHndl, bim::uint page, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    CZIOverview *o = &par->o;
    float scale = static_cast<float>(1.0 / pow(2.0, level));

    // setting resolution level does not change image size in opj_decode, a little hack to
    // update image size based on the resolution level
    info->width = (bim::uint64)floor((double)o->image_width * scale);
    info->height = (bim::uint64)floor((double)o->image_height * scale);
    info->imageMode = par->imageMode;
    info->depth = par->depth;
    info->pixelType = par->pixelType;
    info->samples = par->samples;
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    libCZI::ISingleChannelScalingTileAccessor::Options opts;
    opts.Clear();
    opts.backGroundColor.r = opts.backGroundColor.g = opts.backGroundColor.b = get_background_color(par);

    std::shared_ptr<libCZI::ISingleChannelScalingTileAccessor> accessor = par->cziReader->CreateSingleChannelScalingTileAccessor();
    libCZI::IntRect r{ o->x0, o->y0, o->image_width, o->image_height };
    libCZI::CDimCoordinate coord;
    init_coordinate(fmtHndl, page, coord);

    for (size_t s = 0; s < samples_to_copy(info); ++s) {
        if (o->cs.size() > 0) coord.Set(libCZI::DimensionIndex::C, (int)s);
        std::shared_ptr<libCZI::IBitmapData> ibmp = accessor->Get(r, &coord, scale, &opts);

        if (info->depth == 8)
            copy_bitmap_sample<bim::uint8>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 16)
            copy_bitmap_sample<bim::uint16>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 32)
            copy_bitmap_sample<bim::uint32>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 64)
            copy_bitmap_sample<bim::uint64>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
    } // for sample

    return 0;
}

bim::uint cziReadImageTileProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    CZIOverview *o = &par->o;

    float scale = static_cast<float>(1.0 / pow(2.0, level));
    int num_tiles_x = int(ceil((double)o->image_width * scale / double(info->tileWidth)));
    int num_tiles_y = int(ceil((double)o->image_height * scale / double(info->tileHeight)));
    if (xid >= num_tiles_x || yid >= num_tiles_y) return 1;

    // compute sizes and position in the scaled space
    int scaled_iw = int(floor(o->image_width * scale));
    int scaled_ih = int(floor(o->image_height * scale));
    int scaled_x = static_cast<int>(xid * bim::CZIParams::virtual_tile_width);
    int scaled_y = static_cast<int>(yid * bim::CZIParams::virtual_tile_width);
    int scaled_tw = bim::min<int>(bim::CZIParams::virtual_tile_width, scaled_iw - scaled_x);
    int scaled_th = bim::min<int>(bim::CZIParams::virtual_tile_height, scaled_ih - scaled_y);

    // compute tile position in the 100% image as required by libCZI
    int x = o->x0 + bim::round<int>(scaled_x / scale);
    int y = o->y0 + bim::round<int>(scaled_y / scale);
    int w = bim::round<int>(scaled_tw / scale);
    int h = bim::round<int>(scaled_th / scale);

    // allocate output image
    info->width = scaled_tw;
    info->height = scaled_th;
    info->imageMode = par->imageMode;
    info->depth = par->depth;
    info->pixelType = par->pixelType;
    info->samples = par->samples;
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    libCZI::ISingleChannelScalingTileAccessor::Options opts;
    opts.Clear();
    opts.backGroundColor.r = opts.backGroundColor.g = opts.backGroundColor.b = get_background_color(par);

    std::shared_ptr<libCZI::ISingleChannelScalingTileAccessor> accessor = par->cziReader->CreateSingleChannelScalingTileAccessor();
    libCZI::IntRect r{ x, y, w, h };
    libCZI::CDimCoordinate coord;
    init_coordinate(fmtHndl, page, coord);

    // read scaled samples out
    for (size_t s = 0; s < samples_to_copy(info); ++s) {
        if (o->cs.size() > 0) coord.Set(libCZI::DimensionIndex::C, (int)s);
        std::shared_ptr<libCZI::IBitmapData> ibmp = accessor->Get(r, &coord, scale, &opts);

        if (info->depth == 8)
            copy_bitmap_sample<bim::uint8>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 16)
            copy_bitmap_sample<bim::uint16>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 32)
            copy_bitmap_sample<bim::uint32>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 64)
            copy_bitmap_sample<bim::uint64>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
    } // for sample

    return 0;
}

bim::uint cziReadImageRegionProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 x1, bim::uint64 y1, bim::uint64 x2, bim::uint64 y2, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    bim::CZIParams *par = (bim::CZIParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    CZIOverview *o = &par->o;

    // compute sizes and position in the scaled space
    float scale = static_cast<float>(1.0 / pow(2.0, level));
    int scaled_iw = int(floor(o->image_width * scale));
    int scaled_ih = int(floor(o->image_height * scale));
    int scaled_x = (int)x1;
    int scaled_y = (int)y1;
    //int scaled_tw = x2 - x1 + 1;
    //int scaled_th = y2 - y1 + 1;
    int scaled_tw = bim::min<int>(static_cast<int>(x2 - x1 + 1), scaled_iw - scaled_x);
    int scaled_th = bim::min<int>(static_cast<int>(y2 - y1 + 1), scaled_ih - scaled_y);

    // compute tile position in the 100% image as required by libCZI
    int x = o->x0 + bim::round<int>(scaled_x / scale);
    int y = o->y0 + bim::round<int>(scaled_y / scale);
    int w = bim::round<int>(scaled_tw / scale);
    int h = bim::round<int>(scaled_th / scale);

    // allocate output image
    info->width = scaled_tw;
    info->height = scaled_th;
    info->imageMode = par->imageMode;
    info->depth = par->depth;
    info->pixelType = par->pixelType;
    info->samples = par->samples;
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    libCZI::ISingleChannelScalingTileAccessor::Options opts;
    opts.Clear();
    opts.backGroundColor.r = opts.backGroundColor.g = opts.backGroundColor.b = get_background_color(par);

    std::shared_ptr<libCZI::ISingleChannelScalingTileAccessor> accessor = par->cziReader->CreateSingleChannelScalingTileAccessor();
    libCZI::IntRect r{ x, y, w, h };
    libCZI::CDimCoordinate coord;
    init_coordinate(fmtHndl, page, coord);

    // read scaled samples out
    for (size_t s = 0; s < samples_to_copy(info); ++s) {
        if (o->cs.size() > 0) coord.Set(libCZI::DimensionIndex::C, (int)s);
        std::shared_ptr<libCZI::IBitmapData> ibmp = accessor->Get(r, &coord, scale, &opts);

        if (info->depth == 8)
            copy_bitmap_sample<bim::uint8>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 16)
            copy_bitmap_sample<bim::uint16>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 32)
            copy_bitmap_sample<bim::uint32>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
        else if (info->depth == 64)
            copy_bitmap_sample<bim::uint64>(info->width, info->height, s, ibmp.get(), bmp->bits, info);
    } // for sample

    return 0;
}

//****************************************************************************
// exported
//****************************************************************************

constexpr unsigned int BIM_CZI_NUM_FORMATS = 1;

FormatItem cziItems[BIM_CZI_NUM_FORMATS] = {{
    "CZI",       // short name, no spaces
    "Zeiss CZI", // Long format name
    "czi",       // pipe "|" separated supported extension list
    1,           //canRead;      // 0 - NO, 1 - YES
    0,           //canWrite;     // 0 - NO, 1 - YES
    1,           //canReadMeta;  // 0 - NO, 1 - YES
    0,           //canWriteMeta; // 0 - NO, 1 - YES
    0,           //canWriteMultiPage;   // 0 - NO, 1 - YES
    { 0, 0, 0, 0, 0, 0, 0, 0 } // constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
}};

FormatHeader cziHeader = {

    sizeof(FormatHeader),
    "1.0.0",
    "ZEISS-CZI",
    "Zeiss CZI",

    BIM_FORMAT_CZI_MAGIC_SIZE,
    { 1, BIM_CZI_NUM_FORMATS, cziItems },

    cziValidateFormatProc,
    cziAquireFormatProc, //AquireFormatProc
    cziReleaseFormatProc, //ReleaseFormatProc

    // params
    NULL, //AquireIntParamsProc
    NULL, //LoadFormatParamsProc
    NULL, //StoreFormatParamsProc

    // image begin
    cziOpenImageProc,  //OpenImageProc
    cziCloseImageProc, //CloseImageProc

    // info
    cziGetNumPagesProc,  //GetNumPagesProc
    cziGetImageInfoProc, //GetImageInfoProc

    // read/write
    cziReadImageProc,       //ReadImageProc
    NULL,                   //WriteImageProc
    cziReadImageTileProc,   //ReadImageTileProc
    NULL,                   //WriteImageTileProc
    cziReadImageLevelProc,  //ReadImageLevelProc
    NULL,                   //WriteImageLineProc
    cziReadImageRegionProc, //ReadImageRegionProc
    NULL,                   //WriteImageRegionProc
    czi_append_metadata,    // AppendMetaDataProc
};

extern "C" {
FormatHeader *cziGetFormatHeader(void) {
    return &cziHeader;
}
} // extern C
