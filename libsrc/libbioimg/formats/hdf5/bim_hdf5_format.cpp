/*****************************************************************************
HDF5 support
Copyright (c) 2018, Center for Bio-Image Informatics, UCSB
Copyright (c) 2019-2021, ViQi Inc

VQI is a proprietary format and requires licese from ViQi Inc for writing

Author: Dmitry Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

History:
2018-04-10 09:12:40 - First creation

ver : 10
*****************************************************************************/

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <vector>
#include <set>
#include <random>

#include <Jzon.h>
//#include <pugixml.hpp>

#include <bim_format_misc.h>
#include <bim_image.h>
#include <bim_lcms_parse.h>
#include <bim_metatags.h>
#include <tag_map.h>
#include <xconf.h>
#include <xstring.h>
#include <xtypes.h>
#include <xunits.h>
#include <xdatetime.h>

#include "bim_hdf5_format.h"

//#include <hdf5.h>
#include <H5Cpp.h>

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

using namespace bim;


std::vector<bim::uint> hdf5_get_attribute_uint_vector(H5::H5Object &dataset, const std::string &name);
bim::xstring hdf5_get_attribute_string(H5::H5Object &dataset, const std::string &name, const bim::xstring &def = "");
void viqi_init_coordinate(FormatHandle *fmtHndl, int x = 0, int y = 0, int sample = 0, bool dense = false);

//----------------------------------------------------------------------------
// misc
//----------------------------------------------------------------------------

bim::Image::ResizeMethod str_to_interpolation(const xstring &s) {
    Image::ResizeMethod resize_method = Image::ResizeMethod::szNearestNeighbor;
    xstring ss = s.toLowerCase();
    if (ss == "nn") resize_method = Image::ResizeMethod::szNearestNeighbor;
    else if (ss == "bl") resize_method = Image::ResizeMethod::szBiLinear;
    else if (ss == "bc") resize_method = Image::ResizeMethod::szBiCubic;
    else if (ss == "lanczos") resize_method = Image::ResizeMethod::szLanczos;
    else if (ss == "bessel") resize_method = Image::ResizeMethod::szBessel;
    else if (ss == "point") resize_method = Image::ResizeMethod::szPoint;
    else if (ss == "box") resize_method = Image::ResizeMethod::szBox;
    else if (ss == "hermite") resize_method = Image::ResizeMethod::szHermite;
    else if (ss == "hanning") resize_method = Image::ResizeMethod::szHanning;
    else if (ss == "hamming") resize_method = Image::ResizeMethod::szHamming;
    else if (ss == "blackman") resize_method = Image::ResizeMethod::szBlackman;
    else if (ss == "gaussian") resize_method = Image::ResizeMethod::szGaussian;
    else if (ss == "quadratic") resize_method = Image::ResizeMethod::szQuadratic;
    else if (ss == "catrom") resize_method = Image::ResizeMethod::szCatrom;
    else if (ss == "mitchell") resize_method = Image::ResizeMethod::szMitchell;
    else if (ss == "sinc") resize_method = Image::ResizeMethod::szSinc;
    else if (ss == "blackmanbessel") resize_method = Image::ResizeMethod::szBlackmanBessel;
    else if (ss == "blackmansinc") resize_method = Image::ResizeMethod::szBlackmanSinc;
    return resize_method;
}

xstring interpolation_to_str(bim::Image::ResizeMethod resize_method) {
    xstring ss = "nn";
    if (resize_method == Image::ResizeMethod::szNearestNeighbor) ss = "nn";
    else if (resize_method == Image::ResizeMethod::szBiLinear) ss = "bl";
    else if (resize_method == Image::ResizeMethod::szBiCubic) ss = "bc";
    else if (resize_method == Image::ResizeMethod::szLanczos) ss = "lanczos";
    else if (resize_method == Image::ResizeMethod::szBessel) ss = "bessel";
    else if (resize_method == Image::ResizeMethod::szPoint) ss = "point";
    else if (resize_method == Image::ResizeMethod::szBox) ss = "box";
    else if (resize_method == Image::ResizeMethod::szHermite) ss = "hermite";
    else if (resize_method == Image::ResizeMethod::szHanning) ss = "hanning";
    else if (resize_method == Image::ResizeMethod::szHamming) ss = "hamming";
    else if (resize_method == Image::ResizeMethod::szBlackman) ss = "blackman";
    else if (resize_method == Image::ResizeMethod::szGaussian) ss = "gaussian";
    else if (resize_method == Image::ResizeMethod::szQuadratic) ss = "quadratic";
    else if (resize_method == Image::ResizeMethod::szCatrom) ss = "catrom";
    else if (resize_method == Image::ResizeMethod::szMitchell) ss = "mitchell";
    else if (resize_method == Image::ResizeMethod::szSinc) ss = "sinc";
    else if (resize_method == Image::ResizeMethod::szBlackmanBessel) ss = "blackmanbessel";
    else if (resize_method == Image::ResizeMethod::szBlackmanSinc) ss = "blackmansinc";
    return ss;
}


//****************************************************************************
// bim::HDF5Params
//****************************************************************************

bim::HDF5Params::HDF5Params() {
    //i = ImageInfo();
    this->dimensions_image.resize(BIM_MAX_DIMS, 1);
    this->dimensions_tile.resize(BIM_MAX_DIMS, 0);
    this->dimensions_element.resize(BIM_MAX_DIMS, 1);
    this->positions.resize(BIM_MAX_DIMS, 0);
    this->dimensions_indices.resize(BIM_MAX_DIMS, (bim::uint64) bim::ImageDims::DIM_C);
}

bim::HDF5Params::~HDF5Params() {
    this->close();
}

void bim::HDF5Params::open(const char *filename, bim::ImageIOModes io_mode) {
    H5::Exception::dontPrint();
    this->file_name = filename;
    if (io_mode == bim::ImageIOModes::IO_READ) {
        this->file = new H5::H5File(filename, H5F_ACC_RDONLY);
    } else if (io_mode == bim::ImageIOModes::IO_WRITE) {
        //this->file = new H5::H5File(filename, H5F_ACC_RDONLY);
    }
}

void bim::HDF5Params::close() {
    if (this->file) {
        this->file->close();
        delete this->file;
        this->file = NULL;
    }
}


//----------------------------------------------------------------------------
// VQITableColumn
//----------------------------------------------------------------------------

VQITableColumn::VQITableColumn(const H5::CompType &cmpt, int i) {
    this->idx = i;
    this->ct = cmpt.getMemberClass(i);
    this->offset = cmpt.getMemberOffset(i);
    //H5std_string cmp_name = cmpt.getMemberName(i); // under windows exporting std stuff from dlls may not work due to version clashes
    char *cmp_name = H5Tget_member_name(cmpt.getId(), i);
    this->name = cmp_name;

    this->dt = cmpt.getMemberDataType(i);
    this->size_bytes = this->dt.getSize();
    this->visualizable = false;
}

VQITableColumn::~VQITableColumn() {

}

//----------------------------------------------------------------------------
// VQITableMeasure
//----------------------------------------------------------------------------

VQITableMeasure::VQITableMeasure(const VQITableColumn &col, const xstring &name, int idx) {
    this->idx = idx;
    this->idx_column = col.idx;
    this->name = name.size() > 0 ? name : col.name;
    this->column = col.name;
    this->ct = col.ct;
    this->dt = col.dt;
    this->offset = col.offset;
    this->offset_read = this->offset; // this offsets specifically for reading from sub-composite type
    this->size_bytes = col.size_bytes;

    if (col.interpolation != "") {
        this->interpolation = bim::Image::resize_method_from_string(col.interpolation);
    }
}

VQITableMeasure::~VQITableMeasure() {

}

//----------------------------------------------------------------------------
// VQITableMeasureReader - buffered reader for VQI table columns
// the reader will read a specific set of columns in chunks of chunk_size rows and store them in the buffer indexed by the object id
// max_buffer_size allows control of the buffer size in rows, reaching max_buffer_size will offload data of the oldest accessed chunk from the buffer
//----------------------------------------------------------------------------

void VQITableMeasureReader::init(H5::H5File *file, const bim::xstring &path, const std::vector<VQITableMeasure> &measures, bim::uint chunk_size, bim::uint max_buffer_size) {
    
    this->file = file;
    this->path = path;
    this->path_table = path + "/table";
    this->measures = measures;
    this->chunk_size = chunk_size > 0 ? chunk_size : this->chunk_size;
    this->max_buffer_size = max_buffer_size > 0 ? max_buffer_size : this->max_buffer_size;

    try {
        xstring path_level = this->path + "/level_000/";
        H5::Group group = this->file->openGroup(path_level.c_str());
        this->viqi_block_item_id_offsets = hdf5_get_attribute_uint_vector(group, "viqi_block_item_id_offsets");
        this->viqi_block_first_row = hdf5_get_attribute_uint_vector(group, "viqi_block_first_row");
    } catch (H5::FileIException error) {
    }
    this->viqi_block_first_row.resize(this->viqi_block_item_id_offsets.size(), 0);

    try {
        //H5::DataSet dataset = file->openDataSet(path_table.c_str());
        this->dataset = file->openDataSet(path_table.c_str());

        H5::DataType dt = dataset.getDataType();
        this->row_sz = dt.getSize();

        //H5T_class_t ct = dt.getClass();
        //if (ct != H5T_COMPOUND) return;

        H5::DataSpace dataspace = dataset.getSpace();
        int rank = dataspace.getSimpleExtentNdims();
        std::vector<hsize_t> dims(rank);
        dataspace.getSimpleExtentDims(&dims[0]);

        this->nrows = dims[0];
        this->nchunks = (hsize_t) ceil(double(this->nrows) / this->chunk_size);

    } catch (H5::FileIException error) {
        return;
    }

    this->init_measures();
}

void VQITableMeasureReader::init_measures() {
    bim::xstring col_object_id;
    bim::xstring col_class_label;
    try {
        H5::Group group = file->openGroup(this->path.c_str());
        col_object_id = hdf5_get_attribute_string(group, "viqi_columns_id");
        col_class_label = hdf5_get_attribute_string(group, "viqi_columns_class");
    } catch (H5::FileIException error) {
    }

    this->m_object_id = NULL;
    this->m_class_label = NULL;
    for (bim::uint i = 0; i < this->measures.size(); ++i) {
        VQITableMeasure *m = &(this->measures[i]);
        if (m->column == col_object_id) {
            this->m_object_id = m;
        } else if (m->column == col_class_label) {
            this->m_class_label = m;
        }
    }
    int a = 0;
}

bim::uint VQITableMeasureReader::find_first_chunk_id(bim::uint block_id) {
    bim::uint r = 0;
    if (this->viqi_block_first_row.size() > block_id) {
        r = this->viqi_block_first_row[block_id];
    }
    return (bim::uint) floor(double(r) / this->chunk_size);
}

void VQITableMeasureReader::load_measures(bim::uint measure_id, bim::uint object_id, bim::uint block_id) {
    measure_id = std::min<int>(measure_id, static_cast<int>(this->measures.size()) - 1);

    bim::uint chunk_id = this->find_first_chunk_id(block_id);
    while (chunk_id < this->nchunks) {
        this->load_chunk(chunk_id);
        std::map<unsigned int, unsigned int>::const_iterator it = this->measure_index.find(object_id);
        if (it != this->measure_index.end()) return;
        ++chunk_id;
    }
}

void VQITableMeasureReader::load_chunk(bim::uint chunk_id) {
    std::map<bim::uint, time_t>::const_iterator it = this->chunk_map.find(chunk_id);
    if (it != this->chunk_map.end()) return;

    // load starting at offset
    bim::uint first_row = chunk_id * this->chunk_size;
    int rows_read = std::min<int>((int) this->chunk_size, (int) this->nrows - first_row);
    if (rows_read == 0) return;

    //xstring path_table = this->path + "/table";
    try {
        //H5::DataSet dataset = this->file->openDataSet(path_table.c_str());

        H5::DataType dt = dataset.getDataType();
        //H5T_class_t ct = dt.getClass();
        //size_t sz = dt.getSize();

        H5::DataSpace dataspace = dataset.getSpace();
        int rank = dataspace.getSimpleExtentNdims();
        std::vector<hsize_t> dims(rank);
        dataspace.getSimpleExtentDims(&dims[0]);

        // Define the memory dataspace
        H5::DataSpace memspace(1, &dims[0]);

        // Define memory hyperslab
        std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
        std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
        count_out[0] = rows_read;
        memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

        std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
        std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file
        offset[0] = first_row;
        count[0] = rows_read;
        dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

        // read measures in blocks of 1K rows until fetched the one needed
        int inc = (int) this->row_sz * rows_read;
        int buf_offset = (int) this->measure_buffer.size();
        this->measure_buffer.resize(buf_offset + inc, 0);

        // Read data from hyperslab in the file into the hyperslab in memory
        //dataset.read(&this->measure_buffer[0] + buf_offset, dt, memspace, dataspace);
        dataset.read(&this->measure_buffer[0] + buf_offset, this->subtype, memspace, dataspace); // only read columns of interest
    } catch (H5::FileIException error) {
        return;
    }

    // update objects id and offset map
    this->parse_measures(rows_read);
    this->chunk_map[chunk_id] = time(NULL);
}

bim::uint _subtype_addmeasure(H5::CompType &subtype, VQITableMeasure *m, const H5::CompType &parentDt, bim::uint sub_offset) {
    H5::DataType memberDt = parentDt.getMemberDataType(m->idx_column);
    H5Tinsert(subtype.getId(), m->column.c_str(), sub_offset, memberDt.getId());
    m->offset_read = sub_offset;
    return (bim::uint) memberDt.getSize();
}

// sets up this->m and composite reading class if needed
void VQITableMeasureReader::init_measure(bim::uint measure_id) {
    if (this->measure_id == measure_id) return;
    this->measure_id = measure_id;

    // first reset all reading buffers
    this->chunk_map.clear();
    this->buffer_last_row = 0;
    this->measure_buffer.clear();
    this->measure_index.clear();

    // set reading measure
    this->m = &this->measures[measure_id];
   
    // create composite datatype limited to only needed columns
    this->row_sz = 0;
    if (this->m_object_id) 
        this->row_sz += this->m_object_id->size_bytes;
    if (this->m_class_label) 
        this->row_sz += this->m_class_label->size_bytes;
    if (this->m && this->m != this->m_object_id && this->m != this->m_class_label)
        this->row_sz += this->m->size_bytes;

    this->subtype = H5::CompType((size_t) this->row_sz);
    H5::CompType parentDt = H5::CompType(this->dataset);
    bim::uint suboffset = 0;
    if (this->m_object_id) 
        suboffset += _subtype_addmeasure(this->subtype, this->m_object_id, parentDt, suboffset);
    if (this->m_class_label) 
        suboffset += _subtype_addmeasure(this->subtype, this->m_class_label, parentDt, suboffset);
    if (this->m && this->m != this->m_object_id && this->m != this->m_class_label)
        suboffset += _subtype_addmeasure(this->subtype, this->m, parentDt, suboffset);
}

template<typename T, typename Tw>
void _index_value_offsets(const std::vector<bim::uint8> &buffer, std::map<T, bim::uint> &measure_index, bim::uint offset, bim::uint row_sz, bim::uint offset_measure) {
    const bim::uint8 *p = &buffer[offset] + offset_measure;
    while (offset < buffer.size()) {
        measure_index[static_cast<T>(*(Tw*)p)] = offset;
        offset += row_sz;
        p += row_sz;
    }
}

template<class T>
void _index_measure_offsets(const std::vector<bim::uint8> &buffer, std::map<T, bim::uint> &measure_index, VQITableMeasure *m, bim::uint offset, bim::uint row_sz) {
    bim::uint offset_measure = static_cast<bim::uint>(m->offset_read);
    if (m->ct == H5T_INTEGER && m->size_bytes == 1) {
        _index_value_offsets<T, bim::uint8>(buffer, measure_index, offset, row_sz, offset_measure);
    } else if (m->ct == H5T_INTEGER && m->size_bytes == 2) {
        _index_value_offsets<T, bim::uint16>(buffer, measure_index, offset, row_sz, offset_measure);
    } else if (m->ct == H5T_INTEGER && m->size_bytes == 4) {
        _index_value_offsets<T, bim::uint32>(buffer, measure_index, offset, row_sz, offset_measure);
    } else if (m->ct == H5T_INTEGER && m->size_bytes == 8) {
        _index_value_offsets<T, bim::uint64>(buffer, measure_index, offset, row_sz, offset_measure);
    } else if (m->ct == H5T_FLOAT && m->size_bytes == 4) {
        _index_value_offsets<T, bim::float32>(buffer, measure_index, offset, row_sz, offset_measure);
    } else if (m->ct == H5T_FLOAT && m->size_bytes == 8) {
        _index_value_offsets<T, bim::float64>(buffer, measure_index, offset, row_sz, offset_measure);
    }
}

template<typename Tw>
inline Tw _get_measure_value(const std::vector<bim::uint8> &buffer, bim::uint offset, VQITableMeasure *m, Tw def = 0) {
    Tw v = def;
    const bim::uint8 *p = &buffer[0] + offset + m->offset_read;
    if (m->ct == H5T_INTEGER && m->size_bytes == 1) {
        v = (Tw) *(bim::uint8*) p;
    } else if (m->ct == H5T_INTEGER && m->size_bytes == 2) {
        v = (Tw) *(bim::uint16*) p;
    } else if (m->ct == H5T_INTEGER && m->size_bytes == 4) {
        v = (Tw) *(bim::uint32*) p;
    } else if (m->ct == H5T_INTEGER && m->size_bytes == 8) {
        v = (Tw) *(bim::uint64*) p;
    } else if (m->ct == H5T_FLOAT && m->size_bytes == 4) {
        v = (Tw) *(bim::float32*) p;
    } else if (m->ct == H5T_FLOAT && m->size_bytes == 8) {
        v = (Tw) *(bim::float64*) p;
    }
    return v;
}

void VQITableMeasureReader::parse_measures(bim::uint rows) {
    bim::uint offset = static_cast<bim::uint>(this->buffer_last_row * this->row_sz);
    /*
    while (offset < this->measure_buffer.size()) {
        bim::uint object_id = _get_measure_value<bim::uint>(this->measure_buffer, offset, this->m_object_id, 0);
        this->measure_index[object_id] = offset;
        offset += this->row_sz;
    }
    */
    _index_measure_offsets<bim::uint>(this->measure_buffer, this->measure_index, this->m_object_id, offset, (bim::uint) this->row_sz);
    this->buffer_last_row += rows;
}

template<typename T>
T VQITableMeasureReader::get_measure_value(bim::uint object_id, bim::uint measure_id, bim::uint block_id, const T &def) {
    measure_id = std::min<int>(measure_id, static_cast<int>(this->measures.size()) - 1);
    this->init_measure(measure_id); // sets up this->m and composite reading class if needed

    std::map<unsigned int, unsigned int>::const_iterator it = this->measure_index.find(object_id);
    if (it == this->measure_index.end()) {
        this->load_measures(measure_id, object_id, block_id);
        it = this->measure_index.find(object_id);
    }
    if (it == this->measure_index.end()) return def;

    unsigned int offset = (*it).second;

    // if class selection is required
    if (this->m->class_id >= 0 && this->m_class_label != NULL) {
        bim::uint32 v = _get_measure_value<bim::uint32>(this->measure_buffer, offset, this->m_class_label, 0);
        if (v != m->class_id) return 0;
    }

    return _get_measure_value<T>(this->measure_buffer, offset, m, def);
}


//----------------------------------------------------------------------------
// VQITable
//----------------------------------------------------------------------------

VQITable::VQITable() {

}

VQITable::VQITable(H5::H5File *file, const bim::xstring &path) {
    this->init(file, path);
}

VQITable::~VQITable() {

}

template<typename T>
T VQITable::get_measure_value(bim::uint object_id, bim::uint measure_id, bim::uint block_id, const T &def) {
    T v = this->measure_reader.get_measure_value<T>(object_id, measure_id, block_id, def);
    return v;
}

void VQITable::init(H5::H5File *file, const bim::xstring &path) {
    this->file = file;
    this->path = path;
    xstring path_table = path + "/table";
    try {
        H5::Group group = file->openGroup(path.c_str());

        this->col_object_id = hdf5_get_attribute_string(group, "viqi_columns_id");
        this->col_roi_id = hdf5_get_attribute_string(group, "viqi_columns_roi_id");
        this->col_class_label = hdf5_get_attribute_string(group, "viqi_columns_class");
        this->col_class_confidence = hdf5_get_attribute_string(group, "viqi_columns_confidence");

        std::vector<bim::xstring> vis = hdf5_get_attribute_string(group, "viqi_columns_vis").replace(" ", "").toLowerCase().split(",", false);
        std::vector<bim::xstring> interpol = hdf5_get_attribute_string(group, "viqi_columns_interpolation").replace(" ", "").toLowerCase().split(",", false);

        std::vector<bim::xstring> label_mapping = hdf5_get_attribute_string(group, "viqi_class_label_mapping").split(",");
        for (std::vector<bim::xstring>::iterator it = label_mapping.begin(); it != label_mapping.end(); ++it) {
            std::vector<bim::xstring> v = it->split(":");
            if (v.size() > 1) {
                int class_id = v[0].toInt(0);
                if (class_id > 0)
                    this->class_id_to_label_mapping[class_id] = v[1];
            }
        }

        H5::DataSet dataset = file->openDataSet(path_table.c_str());
        H5::DataType dt = dataset.getDataType();
        H5T_class_t ct = dt.getClass();
        //size_t sz = dt.getSize();
        //this->row_sz = sz;

        H5::DataSpace dataspace = dataset.getSpace();
        int rank = dataspace.getSimpleExtentNdims();
        std::vector<hsize_t> dims(rank);
        dataspace.getSimpleExtentDims(&dims[0]);

        if (ct != H5T_COMPOUND) return;

        this->load_column_info(dataset);

        if (vis.size() == 0) {
            // old format wihout viqi_columns_vis tag
            for (VQITableColumn &column : columns) {
                column.visualizable = true;
            }
        } else {
            const size_t szz = std::min<size_t>(this->columns.size(), vis.size());
            for (size_t i = 0; i < szz; ++i) {
                this->columns[i].visualizable = (vis[i] == "true");
            }
        }

        if (interpol.size() > 0) {
            const size_t szz = std::min<size_t>(this->columns.size(), interpol.size());
            for (size_t i = 0; i < szz; ++i) {
                this->columns[i].interpolation = (interpol[i] != "") ? interpol[i] : "bc";
            }
        }

        this->nrows = dims[0];
        this->init_measures();

    } catch (H5::FileIException error) {
        return;
    }
}

void VQITable::load_column_info(const H5::DataSet &dataset) {
    H5::DataType dt = dataset.getDataType();
    H5T_class_t ct = dt.getClass();
    if (ct != H5T_COMPOUND) return;

    H5::CompType cmpt = H5::CompType(dataset);
    int num_members = cmpt.getNmembers();
    this->columns.reserve(num_members);
    for (int i = 0; i < num_members; ++i) {
        VQITableColumn c(cmpt, i);
        this->columns.push_back(c);
        /*if (c.name == this->col_object_id) {
            this->column_id = i;
        } else if (c.name == this->col_roi_id) {
            this->column_roi_id = i;
        }*/
    }
}

VQITableColumn* VQITable::getColumn(const xstring &name) const {
    for (unsigned int i = 0; i < this->columns.size(); ++i) {
        if (this->columns[i].name == name)
            return (VQITableColumn*) &(this->columns[i]);
    }
    return NULL;
}

VQITableMeasure* VQITable::getMeasure(const xstring &name) const {
    for (unsigned int i = 0; i < this->measures.size(); ++i) {
        if (this->measures[i].name == name)
            return (VQITableMeasure*) &(this->measures[i]);
    }
    return NULL;
}

VQITableMeasure* VQITable::getMeasure(size_t measure_id) const {
    if (measure_id >= this->measures.size()) return NULL;
    return (VQITableMeasure*) &(this->measures[measure_id]);
}

void VQITable::init_measures() {
    this->measures.reserve(this->columns.size());
    int measure_id = -1;
    VQITableColumn *col = 0;

    // measures will always have an id randomizer measure
    col = this->getColumn(this->col_object_id);
    if (col) {
        //VQITableMeasure m(*col, xstring::xprintf("%s_random", this->col_object_id.c_str()), ++measure_id);
        VQITableMeasure m(*col, this->col_object_id.c_str(), ++measure_id);
        m.interpolation = bim::Image::ResizeMethod::szNearestNeighbor;
        m.randomizer = true;
        this->measures.push_back(m);

        //VQITableMeasure m1(*col, this->col_object_id, ++measure_id);
        //this->measures.push_back(m1);
    }

    // measures will have an ROI ID randomizer measure if column present
    col = this->getColumn(this->col_roi_id);
    if (col) {
        //VQITableMeasure m(*col, xstring::xprintf("%s_random", this->col_roi_id.c_str()), ++measure_id);
        VQITableMeasure m(*col, this->col_roi_id.c_str(), ++measure_id);
        m.interpolation = bim::Image::ResizeMethod::szNearestNeighbor;
        m.randomizer = true;
        this->measures.push_back(m);
    }

    // if class label and classes present
    if (this->col_class_label.size() > 0 && this->class_id_to_label_mapping.size() > 0) {

        col = this->getColumn(this->col_class_label);
        if (col) {
            VQITableMeasure m(*col, "class_labels", ++measure_id);
            m.interpolation = bim::Image::ResizeMethod::szNearestNeighbor;
            m.values_are_class_id = true;
            this->measures.push_back(m);

            //this->class_label_offset = col->offset;
            //this->class_label_ct = col->ct;
            //this->class_label_bytes = col->size_bytes;
        }

        // if confidence
        col = this->getColumn(this->col_class_confidence);
        if (col) {
            for (std::map<unsigned int, xstring>::iterator it = this->class_id_to_label_mapping.begin(); it != this->class_id_to_label_mapping.end(); ++it) {
                xstring name = xstring::xprintf("%s.class_id.%d", this->col_class_confidence.c_str(), it->first);
                VQITableMeasure m(*col, name, ++measure_id);
                m.class_id = it->first;
                this->measures.push_back(m);
            }
        }
    }

    // all measures exposed in the table
    for (unsigned int i = 0; i < this->columns.size(); ++i) {
        if (this->columns[i].visualizable) {
            VQITableMeasure m(this->columns[i], "", ++measure_id);
            this->measures.push_back(m);
        }
    }

    // init measure reader
    this->measure_reader.init(this->file, this->path, this->measures);
}

bool VQITable::has_metadata() const {
    if (this->nrows > 0)
        return true;
    if (this->class_id_to_label_mapping.size()>0)
        return true;
    return false;
}

void VQITable::append_metadata(TagMap *hash) const {

    if (this->measures.size() > 0) {
        hash->set_value(bim::IMAGE_NUM_MEASURES, (int)this->measures.size());

        for (unsigned int i = 0; i < this->measures.size(); ++i) {
            const VQITableMeasure *m = &this->measures[i];
            int idx = m->idx;
            hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_NAME.c_str(), idx), m->name);

            H5::DataType dt = m->dt;
            xstring fmt;
            if (dt.getClass() == H5T_INTEGER) fmt += "int";
            if (dt.getClass() == H5T_FLOAT) fmt += "float";
            fmt += xstring::xprintf("%d", dt.getSize() * 8);
            hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_FORMAT.c_str(), idx), fmt);

            //DECLARE_STR(MEASURE_TEMPLATE_RANGE, "measures/measure:%d/range")
            //hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_NAME.c_str(), idx), m->name);

            hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_INTERPOLATION.c_str(), idx), interpolation_to_str(m->interpolation));
            if (m->randomizer)
                hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_RANDOMIZER.c_str(), idx), true);

            if (m->values_are_class_id)
                hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_VALUES_LABEL_IDS.c_str(), idx), true);

            if (m->class_id >= 0) {
                hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_CLASS_ID.c_str(), idx), m->class_id);

                std::map<unsigned int, xstring>::const_iterator it = this->class_id_to_label_mapping.find(m->class_id);
                if (it != this->class_id_to_label_mapping.end()) {
                    hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_CLASS_LABEL.c_str(), idx), it->second);
                }
            }
        }
    }

    int i = 0;
    for (std::map<unsigned int, xstring>::const_iterator it = this->class_id_to_label_mapping.begin(); it != this->class_id_to_label_mapping.end(); ++it) {
        hash->set_value(xstring::xprintf(bim::CLASS_LABELS_TEMPLATE_NAME.c_str(), i), it->second);
        hash->set_value(xstring::xprintf(bim::CLASS_LABELS_TEMPLATE_ID.c_str(), i), it->first);
        ++i;
    }

    // dense mask with class labels: produce one measure with class labels
    if (this->measures.size() == 0 && this->class_id_to_label_mapping.size() > 0) {
        int idx = 0;
        hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_NAME.c_str(), idx), "class_labels");
        hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_INTERPOLATION.c_str(), idx), "nn");
        hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_VALUES_LABEL_IDS.c_str(), idx), true);
        hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_FORMAT.c_str(), idx), hash->get_value("pixel_format"));
    }
}

//****************************************************************************
// required funcs
//****************************************************************************

#define BIM_FORMAT_HDF5_MAGIC_SIZE 8

const char hdf5_magic_number[9] = "\x89\x48\x44\x46\x0d\x0a\x1a\x0a"; //89 48 44 46 0d 0a 1a 0a

int hdf5ValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    if (length < BIM_FORMAT_HDF5_MAGIC_SIZE) return -1;
    unsigned char *mag_num = (unsigned char *)magic;
    if (memcmp(mag_num, hdf5_magic_number, BIM_FORMAT_HDF5_MAGIC_SIZE) == 0) return 0;
    return -1;
}

FormatHandle hdf5AquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void hdf5CloseImageProc(FormatHandle *fmtHndl);
void hdf5ReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    hdf5CloseImageProc(fmtHndl);
}


//----------------------------------------------------------------------------
// hdf5 utils
//----------------------------------------------------------------------------

static const int BIM_STR_SIZE = 2048;

//std::shared_ptr<bim::xstring> get_attribute_name(H5::DataSet &dataset, int idx) {
bim::xstring hdf5_get_attribute_name(H5::H5Object &dataset, int idx, const bim::xstring &def = "") {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(idx);
    } catch (...) {
        return def;
    }

    bim::xstring name(BIM_STR_SIZE, 0);
    ssize_t sz = a.getName(&name[0], BIM_STR_SIZE);
    name.resize(sz);

    //return std::make_shared<bim::xstring>(name);
    return name;
}

//std::shared_ptr<bim::xstring> get_attribute_value(H5::DataSet &dataset, const std::string &name) {
bim::xstring hdf5_get_attribute_string(H5::H5Object &dataset, const std::string &name, const bim::xstring &def) {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return def;
    }

    H5::DataType dt = a.getDataType();
    if (dt.getClass() != H5T_STRING)
        //return std::make_shared<bim::xstring>(def);
        return def;

    hsize_t sz = a.getStorageSize();
    bim::xstring s(sz, 0);
    a.read(dt, &s[0]);
    if (s[sz - 1] == 0) s.resize(sz - 1);
    //return std::make_shared<bim::xstring>(s);
    return s;
}

int hdf5_get_attribute_int(H5::H5Object &dataset, const std::string &name, const int &def = 0) {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return def;
    }
    H5::DataType dt = a.getDataType();
    hsize_t vsz = a.getStorageSize();
    size_t esz = dt.getSize();
    if (dt.getClass() != H5T_INTEGER || vsz < 1 || esz < 1)
        return def;

    std::vector<int> v(vsz / esz, 0);
    a.read(H5::PredType::NATIVE_INT, &v[0]);
    return v[0];
}

std::vector<int> hdf5_get_attribute_int_vector(H5::H5Object &dataset, const std::string &name) {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return std::vector<int>();
    }
    H5::DataType dt = a.getDataType();
    hsize_t vsz = a.getStorageSize();
    size_t esz = dt.getSize();
    if (dt.getClass() != H5T_INTEGER || vsz < 1 || esz < 1)
        return std::vector<int>();

    std::vector<int> v(vsz / esz, 0);
    a.read(H5::PredType::NATIVE_INT, &v[0]);
    return v;
}

std::vector<bim::uint> hdf5_get_attribute_uint_vector(H5::H5Object &dataset, const std::string &name) {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return std::vector<bim::uint>();
    }
    H5::DataType dt = a.getDataType();
    hsize_t vsz = a.getStorageSize();
    size_t esz = dt.getSize();
    if (dt.getClass() != H5T_INTEGER || vsz < 1 || esz < 1)
        return std::vector<bim::uint>();

    std::vector<bim::uint> v(vsz / esz, 0);
    a.read(H5::PredType::NATIVE_UINT, &v[0]);
    return v;
}

double hdf5_get_attribute_double(H5::H5Object &dataset, const std::string &name, const double &def = 0) {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return def;
    }
    H5::DataType dt = a.getDataType();
    hsize_t vsz = a.getStorageSize();
    size_t esz = dt.getSize();
    if (dt.getClass() != H5T_FLOAT || vsz < 1 || esz < 1)
        return def;

    std::vector<double> v(vsz / esz, 0);
    a.read(H5::PredType::NATIVE_DOUBLE, &v[0]);
    return v[0];
}

std::vector<double> hdf5_get_attribute_double_vector(H5::H5Object &dataset, const std::string &name) {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return std::vector<double>();
    }
    H5::DataType dt = a.getDataType();
    hsize_t vsz = a.getStorageSize();
    size_t esz = dt.getSize();
    if (dt.getClass() != H5T_FLOAT || vsz < 1 || esz < 1)
        return std::vector<double>();

    std::vector<double> v(vsz / esz, 0);
    a.read(H5::PredType::NATIVE_DOUBLE, &v[0]);
    return v;
}

H5::DataSet hdf5_get_referenced_object(H5::H5Object &dataset, const std::string &name) {
    H5::DataSet o;
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return o;
    }
    H5::DataType dt = a.getDataType();
    if (dt.getClass() != H5T_REFERENCE)
        return o;

    hid_t v;
    a.read(H5::PredType::STD_REF_OBJ, &v);

    o.dereference(a, &v);
    return o;
}

void hdf5_append_object_attributes(H5::H5Object &obj, TagMap *hash, const xstring &meta_path) {
    for (int i = 0; i < obj.getNumAttrs(); ++i) {
        H5::Attribute a = obj.openAttribute(i);
        bim::xstring name(BIM_STR_SIZE, 0);
        ssize_t sz = a.getName(&name[0], BIM_STR_SIZE);
        if (sz < 1) continue;
        name.resize(sz);

        H5::DataType dt = a.getDataType();
        hsize_t vsz = a.getStorageSize();
        size_t esz = dt.getSize();
        if (vsz < 1 || esz < 1) continue;
        if (dt.getClass() == H5T_STRING) {
            bim::xstring v(vsz, 0);
            a.read(dt, &v[0]);
            if (v[vsz - 1] == 0) v.resize(vsz - 1);
            hash->set_value(meta_path + "/" + name, v);
        } else if (dt.getClass() == H5T_INTEGER) {
            std::vector<int> v(vsz / esz, 0);
            a.read(H5::PredType::NATIVE_INT, &v[0]);
            if (v.size() == 1)
                hash->set_value(meta_path + "/" + name, v[0]);
            else if (v.size() < 100)
                hash->set_value(meta_path + "/" + name, v);
        } else if (dt.getClass() == H5T_FLOAT) {
            std::vector<double> v(vsz / esz, 0);
            a.read(H5::PredType::NATIVE_DOUBLE, &v[0]);
            if (v.size() == 1)
                hash->set_value(meta_path + "/" + name, v[0]);
            else if (v.size() < 100)
                hash->set_value(meta_path + "/" + name, v);
        }
    }
}

void hdf5_append_attributes(bim::HDF5Params *par, TagMap *hash, const xstring &h5_path, const xstring &meta_path) {
    xstring path = h5_path;
    if (path.size() < 1) path = "/";

    try {
        H5::Group group = par->file->openGroup(path.c_str());
        hdf5_append_object_attributes(group, hash, meta_path);
    } catch (H5::FileIException error) {
        try {
            H5::DataSet dataset = par->file->openDataSet(path.c_str());
            hdf5_append_object_attributes(dataset, hash, meta_path);
        } catch (H5::FileIException error) {
            // pass
        }
    }
}

void hdf5_walker(bim::HDF5Params *par, H5::Group &obj, const xstring &h5_path, std::vector<xstring> &objects, const int maybe_max_objects = -1) {
    const size_t max_objects = maybe_max_objects >= 0 ? maybe_max_objects : 0;

    // first find all datasets at the current level
    for (size_t i = 0; i < obj.getNumObjs(); ++i) {
        bim::xstring name(BIM_STR_SIZE, 0);
        ssize_t sz = obj.getObjnameByIdx(i, &name[0], BIM_STR_SIZE);
        name.resize(sz);

        bim::xstring path = h5_path + "/" + name;
        try {
            H5::DataSet dataset = par->file->openDataSet(path.c_str());

            // get sizes and types and see if its valid for inclusion
            H5::DataType dt = dataset.getDataType();
            H5T_class_t type_class = dt.getClass();
            if (type_class != H5T_FLOAT && type_class != H5T_INTEGER) continue;

            // Get filespace for rank and dimension
            H5::DataSpace dataspace = dataset.getSpace();
            int rank = dataspace.getSimpleExtentNdims();
            if (rank < 2) continue;

            std::vector<hsize_t> dims(rank);
            rank = dataspace.getSimpleExtentDims(&dims[0]);
            if (rank == 2 && (dims[0] < HDF5_GUESS_MAX_CHANNELS || dims[1] < HDF5_GUESS_MAX_CHANNELS)) continue;

            objects.push_back(path);
            if (maybe_max_objects >= 0 && objects.size() >= max_objects) return;
        } catch (...) { //catch (H5::FileIException error) {
        }
    }

    // then explore depth
    for (size_t i = 0; i < obj.getNumObjs(); ++i) {
        bim::xstring name(BIM_STR_SIZE, 0);
        ssize_t sz = obj.getObjnameByIdx(i, &name[0], BIM_STR_SIZE);
        name.resize(sz);

        bim::xstring path = h5_path + "/" + name;
        try {
            H5::Group group = par->file->openGroup(path.c_str());
            hdf5_walker(par, group, path, objects, (int) max_objects);
            if (maybe_max_objects >= 0 && objects.size() >= max_objects) return;
        } catch (H5::FileIException error) {
            // do nothing
        }
    }
}

void hdf5_meta_walker(bim::HDF5Params *par, TagMap *hash, const xstring &h5_path, const xstring &meta_path) {
    try {
        H5::Group group = par->file->openGroup(h5_path.c_str());
        hdf5_append_object_attributes(group, hash, meta_path);

        for (size_t i = 0; i < group.getNumObjs(); ++i) {
            bim::xstring name(BIM_STR_SIZE, 0);
            ssize_t sz = group.getObjnameByIdx(i, &name[0], BIM_STR_SIZE);
            name.resize(sz);

            bim::xstring mpath = meta_path + "/" + name;
            bim::xstring hpath = h5_path + "/" + name;
            hdf5_meta_walker(par, hash, hpath, mpath);
        }

    } catch (H5::FileIException error) {
    }
}

//----------------------------------------------------------------------------
// reading
//----------------------------------------------------------------------------

void hdf5_get_palette(bim::HDF5Params *par, H5::DataSet &dataset, ImageInfo *info) {
    H5::DataSet pallette = hdf5_get_referenced_object(dataset, "PALETTE");
    H5::DataType dt = pallette.getDataType();

    bim::xstring tag_class = hdf5_get_attribute_string(pallette, std::string("CLASS"));
    if (tag_class != "PALETTE") return;

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = pallette.getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    // Define the memory dataspace
    H5::DataSpace memspace(2, &dims[0]);

    // Define memory hyperslab
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = dims[0];
    count_out[1] = dims[1];
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file
    count[0] = dims[0];
    count[1] = dims[1];
    dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

    // allocate image
    std::vector<bim::uint8> data(dims[0] * dims[1], 0);

    // Read data from hyperslab in the file into the hyperslab in memory
    pallette.read(&data[0], dt, memspace, dataspace);

    // copy values
    info->lut.count = bim::min<bim::uint>(256, (bim::uint)dims[0]);
    int x = 0;
    for (bim::uint i = 0; i < info->lut.count; ++i) {
        info->lut.rgba[i] = bim::xRGB(data[x + 0], data[x + 1], data[x + 2]);
        x += 3;
    }
}

void hdf5_read_vector(bim::HDF5Params *par, const xstring &h5_path, std::vector<double> &data) {
    H5::DataSet dataset = par->file->openDataSet(h5_path.c_str());
    //H5::DataType dt = dataset.getDataType();

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = dataset.getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    // Define the memory dataspace
    H5::DataSpace memspace(1, &dims[0]);

    // Define memory hyperslab
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = dims[0];
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file
    count[0] = dims[0];
    dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

    // allocate vector
    data.resize(dims[0], 0);

    // Read data from hyperslab in the file into the hyperslab in memory
    //dataset.read(&data[0], dt, memspace, dataspace);
    dataset.read(&data[0], H5::PredType::NATIVE_DOUBLE, memspace, dataspace);
}

void hdf5_read_vector_int(bim::HDF5Params *par, const xstring &h5_path, std::vector<int> &data) {
    H5::DataSet dataset = par->file->openDataSet(h5_path.c_str());
    //H5::DataType dt = dataset.getDataType();

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = dataset.getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    // Define the memory dataspace
    H5::DataSpace memspace(2, &dims[0]);

    // Define memory hyperslab
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = dims[0];
    count_out[1] = dims[1];
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file
    count[0] = dims[0];
    count[1] = dims[1];
    dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

    // allocate vector
    data.resize(dims[0] * dims[1], 0);

    // Read data from hyperslab in the file into the hyperslab in memory
    //dataset.read(&data[0], dt, memspace, dataspace);
    dataset.read(&data[0], H5::PredType::NATIVE_INT, memspace, dataspace);
}

void viqi2PixelFormat(const std::string &pf, ImageInfo *info) {
    if (pf == "uchar" || pf == "unsigned char" || pf == "uint8" || pf == "uint8_t") {
        info->depth = 8;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    } else if (pf == "ushort" || pf == "unsigned short" || pf == "unsigned short int" || pf == "uint16" || pf == "uint16_t") {
        info->depth = 16;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    } else if (pf == "uint" || pf == "unsigned int" || pf == "uint32" || pf == "uint32_t") {
        info->depth = 32;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    } else if (pf == "ulonglong" || pf == "unsigned long long" || pf == "unsigned long long int" || pf == "uint64" || pf == "uint64_t") {
        info->depth = 64;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    } else if (pf == "signed char" || pf == "int8" || pf == "int8_t") {
        info->depth = 8;
        info->pixelType = bim::DataFormat::FMT_SIGNED;
    } else if (pf == "short" || pf == "short int" || pf == "signed short" || pf == "signed short int" || pf == "int16" || pf == "int16_t") {
        info->depth = 16;
        info->pixelType = bim::DataFormat::FMT_SIGNED;
    } else if (pf == "int" || pf == "signed int" || pf == "int32" || pf == "int32_t") {
        info->depth = 32;
        info->pixelType = bim::DataFormat::FMT_SIGNED;
    } else if (pf == "longlong" || pf == "long long" || pf == "long long int" || pf == "signed long long" || pf == "signed long long int" || pf == "int64" || pf == "int64_t") {
        info->depth = 64;
        info->pixelType = bim::DataFormat::FMT_SIGNED;
    } else if (pf == "float" || pf == "float32") {
        info->depth = 32;
        info->pixelType = bim::DataFormat::FMT_FLOAT;
    } else if (pf == "double" || pf == "float64") {
        info->depth = 64;
        info->pixelType = bim::DataFormat::FMT_FLOAT;
    }
}

const std::string pixelFormat2viqi(ImageInfo *info) {
    if (info->depth == 8 && info->pixelType == bim::DataFormat::FMT_UNSIGNED) {
        return "uint8";
    } else if (info->depth == 16 && info->pixelType == bim::DataFormat::FMT_UNSIGNED) {
        return "uint16";
    } else if (info->depth == 32 && info->pixelType == bim::DataFormat::FMT_UNSIGNED) {
        return "uint32";
    } else if (info->depth == 64 && info->pixelType == bim::DataFormat::FMT_UNSIGNED) {
        return "uint64";
    } else if (info->depth == 8 && info->pixelType == bim::DataFormat::FMT_SIGNED) {
        return "int8";
    } else if (info->depth == 16 && info->pixelType == bim::DataFormat::FMT_SIGNED) {
        return "int16";
    } else if (info->depth == 32 && info->pixelType == bim::DataFormat::FMT_SIGNED) {
        return "int32";
    } else if (info->depth == 64 && info->pixelType == bim::DataFormat::FMT_SIGNED) {
        return "int64";
    } else if (info->depth == 32 && info->pixelType == bim::DataFormat::FMT_FLOAT) {
        return "float32";
    } else if (info->depth == 64 && info->pixelType == bim::DataFormat::FMT_FLOAT) {
        return "float64";
    }
    return "uint8";
}

void setDataConfiguration(FormatHandle *fmtHndl, H5::DataSet &dataset, ImageInfo *info) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    VQITableMeasure *m = 0;

    // get data type from a measure, only apply this for sparse and heatmap storage, not dense!
    H5::DataType dt = dataset.getDataType();
    if (fmtHndl->subFormat == FMT_VIQI && par->values_from_measures && par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_MEASURE] > 0 && par->table.hasMeasures()) {
        viqi_init_coordinate(fmtHndl, 0, 0, 0);
        m = par->table.getMeasure(par->positions[(bim::uint64) bim::ImageDims::DIM_MEASURE]);
        if (m) {
            dt = m->dt; // dima: use measure datatype
        }
    }

    info->depth = (bim::uint32)dt.getSize() * 8;
    info->pixelType = bim::DataFormat::FMT_UNDEFINED;
    H5T_class_t type_class = dt.getClass();
    par->hdf_pixel_format = (int)type_class;
    if (type_class == H5T_FLOAT) {
        info->pixelType = bim::DataFormat::FMT_FLOAT;
    } else if (type_class == H5T_BITFIELD) {
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    } else if (type_class == H5T_INTEGER) {
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
        if (!m) {
            H5::IntType int_type = dataset.getIntType();
            if (int_type.getSign() != H5T_SGN_NONE) {
                info->pixelType = bim::DataFormat::FMT_SIGNED;
            }
        }
    }

    // get data dimensions
    info->imageMode = bim::ImageModes::IM_GRAYSCALE;
    info->samples = 1;
    info->number_z = 1;
    info->number_t = 1;
    info->number_dims = 2;
    info->number_pages = 1;

    // Get filespace for rank and dimension
    H5::DataSpace dataspace = dataset.getSpace();
    // Get number of dimensions in the file dataspace
    int rank = dataspace.getSimpleExtentNdims();
    assert(rank >= 0);

    // Get the dimension sizes of the file dataspace
    std::vector<hsize_t> dims(rank);
    rank = dataspace.getSimpleExtentDims(&dims[0]);

    // Check if dataset is chunked
    H5::DSetCreatPropList cparms = dataset.getCreatePlist();
    std::vector<hsize_t> chunk_dims(rank, 0);
    int rank_chunk = 0;
    if (cparms.getLayout() == H5D_CHUNKED) {
        rank_chunk = cparms.getChunk(rank, &chunk_dims[0]);
    }

    if (fmtHndl->subFormat == FMT_VIQI) {

        // support for external data blocks
        bim::xstring datatype = hdf5_get_attribute_string(dataset, "viqi_block_external_datatype");
        std::vector<int> block_dims = hdf5_get_attribute_int_vector(dataset, "viqi_block_external_dimensions");
        std::vector<int> block_chunks = hdf5_get_attribute_int_vector(dataset, "viqi_block_external_chunking");
        int dim_x = !par->viqi_image_interleaved ? rank - 1 : 1;
        int dim_y = !par->viqi_image_interleaved ? rank - 2 : 0;

        if (rank > 2 && !par->viqi_image_interleaved) {
            par->dimensions_indices[0] = (bim::uint64)bim::ImageDims::DIM_C;
            par->dimensions_indices[1] = (bim::uint64)bim::ImageDims::DIM_Y;
            par->dimensions_indices[2] = (bim::uint64)bim::ImageDims::DIM_X;
        } else {
            par->dimensions_indices[0] = (bim::uint64)bim::ImageDims::DIM_Y;
            par->dimensions_indices[1] = (bim::uint64)bim::ImageDims::DIM_X;
            par->dimensions_indices[2] = (bim::uint64)bim::ImageDims::DIM_C;
        }

        if (datatype.size() > 0) {
            viqi2PixelFormat(datatype, info);
        }
        if (block_dims.size() > 0) {
            rank = static_cast<int>(block_dims.size());
            dims.resize(rank);
            for (int i = 0; i < rank; ++i) dims[i] = block_dims[i];
        }
        if (block_chunks.size() > 0) {
            rank_chunk = static_cast<int>(block_chunks.size());
            chunk_dims.resize(rank_chunk);
            for (int i = 0; i < rank_chunk; ++i) chunk_dims[i] = block_chunks[i];
            chunk_dims[dim_y] = 128;
            chunk_dims[dim_x] = 128;
        }

        // ViQi interleaved: [height][width][z][t][c]
        // ViQi separated: [z][t][c][height][width]
        if (rank > 1) {
            par->dimensions_element[(bim::uint64)bim::ImageDims::DIM_X] = static_cast<int>(dims[dim_y]);
            par->dimensions_element[(bim::uint64)bim::ImageDims::DIM_Y] = static_cast<int>(dims[dim_x]);
            info->number_dims = 2;
        }

        // do a proper dim test using strings: Y,X,Z,C,...
        for (size_t d = 0; d < static_cast<size_t>(rank); ++d) {
            if (par->dimensions_indices.size() <= d) break;
            int dim_idx = par->dimensions_indices[d];
            par->dimensions_element[dim_idx] = (int)dims[d];
            par->dimensions_tile[dim_idx] = (int)chunk_dims[d];
            if (dim_idx > 2)
                ++info->number_dims;
        }

        if (rank > 1) {
            info->width = par->dimensions_element[(bim::uint64) bim::ImageDims::DIM_X];
            info->height = par->dimensions_element[(bim::uint64) bim::ImageDims::DIM_Y];
            info->number_z = par->dimensions_element[(bim::uint64) bim::ImageDims::DIM_Z];
            info->number_t = par->dimensions_element[(bim::uint64) bim::ImageDims::DIM_T];
            info->samples = par->dimensions_element[(bim::uint64) bim::ImageDims::DIM_C];
            par->image_num_samples = par->dimensions_element[(bim::uint64) bim::ImageDims::DIM_C];
            info->imageMode = info->samples > 1 ? bim::ImageModes::IM_MULTI : bim::ImageModes::IM_GRAYSCALE;
        }

        // figure if the last dim is samples
        //if (par->dimensions_indices[rank - 1] == (bim::uint64) bim::ImageDims::DIM_C)
        //    par->image_num_samples = dims[rank - 1];

        // deal with spectral case here
        if (par->viqi_image_content == "spectral") {
            par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_SPECTRA] = info->samples;
            if (par->position_spectrum.size() == 0)
                par->position_spectrum.push_back(info->samples / 2);
            par->image_num_samples = std::max<int>(1, static_cast<int>(par->position_spectrum.size()));
            info->samples = par->image_num_samples;
            info->number_pages = info->number_pages * par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_SPECTRA];
            info->imageMode = bim::ImageModes::IM_SPECTRAL;
            info->number_dims += 1;
        }

        // deal with the measures case
        if (par->table.hasMeasures()) {
            //par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_MEASURE] = info->samples;
            info->samples = 1;
        }
    } else if (fmtHndl->subFormat == FMT_DREAM3D) {
        if (rank == 2) { // Dream3d: [height][width] - GUESS
            info->width = dims[1];
            info->height = dims[0];
            info->number_z = 1;
            info->samples = 1;
            info->number_dims = 2;
            if (rank_chunk > 2) {
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] = (int)chunk_dims[1];
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] = (int)chunk_dims[0];
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Z] = 0;
            }
        } else if (rank == 3) { // Dream3d: [z][height][width] - GUESS
            info->width = dims[2];
            info->height = dims[1];
            info->number_z = dims[0];
            info->samples = 1;
            info->number_dims = 3;
            if (rank_chunk > 2) {
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] = (int)chunk_dims[2];
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] = (int)chunk_dims[1];
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Z] = (int)chunk_dims[0];
            }
        } else if (rank == 4) { // Dream3d: [z][height][width][pixel components] - FACT
            info->width = dims[2];
            info->height = dims[1];
            info->number_z = dims[0];
            info->samples = (bim::uint32)dims[3];
            info->number_dims = 3;
            if (rank_chunk > 2) {
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] = (int)chunk_dims[2];
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] = (int)chunk_dims[1];
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Z] = (int)chunk_dims[0];
            }
        }
        info->imageMode = info->samples > 1 ? bim::ImageModes::IM_MULTI : bim::ImageModes::IM_GRAYSCALE;

    } else if (fmtHndl->subFormat == FMT_IMS) {
        // Imaris: [z][height][width]
        info->width = dims[2];
        info->height = dims[1];
        info->number_z = dims[0];
        if (rank_chunk > 2) {
            par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] = (int)chunk_dims[2];
            par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] = (int)chunk_dims[1];
            par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Z] = (int)chunk_dims[0];
        }
    } else {
        // Generic HDF5 case - this has many heuristics, require more tweaking
        if (rank == 2) {
            info->width = dims[1];
            info->height = dims[0];
            if (rank_chunk > 1) {
                //info->tileWidth = chunk_dims[1];
                //info->tileHeight = chunk_dims[0];
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] = (int)chunk_dims[1];
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] = (int)chunk_dims[0];
            }
        } else if (rank == 3) {
            if (dims[2] < bim::HDF5_GUESS_MAX_CHANNELS) { // guess that last dim is radiometric
                info->width = dims[1];
                info->height = dims[0];
                info->samples = (bim::uint32)dims[2];
                if (rank_chunk > 1) {
                    par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] = (int)chunk_dims[1];
                    par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] = (int)chunk_dims[0];
                }
                par->interlaced = true;
            } else {
                info->width = dims[2];
                info->height = dims[1];
                info->number_z = dims[0];
                info->number_t = 1;
                info->samples = 1;
                info->number_dims = 3;
                if (rank_chunk > 2) {
                    par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] = (int)chunk_dims[2];
                    par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] = (int)chunk_dims[1];
                }
            }
        } else if (rank == 4) {
            if (dims[3] < bim::HDF5_GUESS_MAX_CHANNELS) { // guess that last dim is radiometric
                info->width = dims[2];
                info->height = dims[1];
                info->number_z = dims[0];
                info->samples = (bim::uint32)dims[3];
                info->number_dims = 3;
                if (rank_chunk > 2) {
                    par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] = (int)chunk_dims[2];
                    par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] = (int)chunk_dims[1];
                }
                par->interlaced = true;
            } else {
                info->width = dims[3];
                info->height = dims[2];
                info->number_z = dims[1];
                info->number_t = dims[0];
                info->samples = 1;
                info->number_dims = 4;
                if (rank_chunk > 2) {
                    par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] = (int)chunk_dims[3];
                    par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] = (int)chunk_dims[2];
                }
            }
        } else if (rank > 4) {
            if (dims[4] < bim::HDF5_GUESS_MAX_CHANNELS) { // guess that last dim is radiometric
                info->width = dims[3];
                info->height = dims[2];
                info->number_z = dims[1];
                info->number_t = dims[0];
                info->samples = (bim::uint32)dims[4];
                info->number_dims = 4;
                par->interlaced = true;
            } else {
                // will be skipping other dims since we don't know what they are
                info->width = dims[3];
                info->height = dims[2];
                info->number_z = dims[1];
                info->number_t = dims[0];
                info->samples = 1;
                info->number_dims = 4;
            }
            if (rank_chunk > 2) {
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] = (int)chunk_dims[3];
                par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] = (int)chunk_dims[2];
            }
        }

        par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X] = static_cast<int>(info->width);
        par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y] = static_cast<int>(info->height);
        par->resolution_levels = 1;
        par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Z] = static_cast<int>(info->number_z);
        info->imageMode = info->samples > 1 ? bim::ImageModes::IM_MULTI : bim::ImageModes::IM_GRAYSCALE;
    }
    info->number_pages = info->number_z * info->number_t;

    // check attributes if dataset is an image and set proper colorspaces
    if (dataset.attrExists("IMAGE_VERSION")) {
        bim::xstring tag_class = hdf5_get_attribute_string(dataset, std::string("CLASS"));
        if (tag_class == "IMAGE") {
            par->image_class = true;

            bim::xstring tag_interlace = hdf5_get_attribute_string(dataset, std::string("INTERLACE_MODE"));
            par->interlaced = (tag_interlace == "INTERLACE_PIXEL");

            bim::xstring tag_subclass = hdf5_get_attribute_string(dataset, std::string("IMAGE_SUBCLASS"));
            if (rank > 2 && tag_subclass == "IMAGE_TRUECOLOR" && par->interlaced) { //	[height][width][pixel components]
                info->width = dims[1];
                info->height = dims[0];
                info->samples = (bim::uint32)dims[2];
            } else if (rank > 2 && tag_subclass == "IMAGE_TRUECOLOR" && !par->interlaced) { // [pixel components][height][width]
                info->width = dims[2];
                info->height = dims[1];
                info->samples = (bim::uint32)dims[0];
            }

            if (tag_subclass == "IMAGE_TRUECOLOR" && info->samples == 3)
                info->imageMode = bim::ImageModes::IM_RGB;
            else if (tag_subclass == "IMAGE_TRUECOLOR" && info->samples == 4)
                info->imageMode = bim::ImageModes::IM_RGBA;

            if (tag_subclass == "IMAGE_INDEXED")
                info->imageMode = bim::ImageModes::IM_INDEXED;
            else if (tag_subclass == "IMAGE_BITMAP")
                info->imageMode = bim::ImageModes::IM_BITMAP;
            else if (tag_subclass == "IMAGE_GRAYSCALE")
                info->imageMode = bim::ImageModes::IM_GRAYSCALE;
        }
    } // if image class

    /*
    par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X] = info->width;
    par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y] = info->height;
    par->resolution_levels = 1;
    par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Z] = info->number_z;
    */

    // read pallette if referenced
    if (dataset.attrExists("PALETTE")) {
        hdf5_get_palette(par, dataset, info);
    }
}

void hdf5_init_coordinate(FormatHandle *fmtHndl, const bim::uint &page, int &z, int &t) {
    fmtHndl->pageNumber = page;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    z = -1;
    t = -1;
    //int spectrum = 0;
    if (conf) {
        z = conf->getValueInt("-slice-z", -1);
        t = conf->getValueInt("-slice-t", -1);
        //spectrum = conf->getValueInt("-slice-spectrum", 0);
    }

    if (info->number_z > 1 && info->number_t <= 1 && z < 0) {
        z = page;
        t = 0;
    } else if (info->number_z <= 1 && info->number_t > 1 && t < 0) {
        t = page;
        z = 0;
    } else if (info->number_z > 1 && info->number_t > 1 && z < 0 && t < 0) {
        t = (int)floor(page / info->number_z);
        z = (int)(page - t * info->number_z);
    }
    z = bim::max(0, z);
    t = bim::max(0, t);
}

void hdf5_set_offset_count(FormatHandle *fmtHndl, bim::ImageInfo *info, const std::vector<hsize_t> &dims, const int &rank, const int &sample, const int &z, const int &t, std::vector<hsize_t> &offset, std::vector<hsize_t> &count, bim::uint x = 0, bim::uint y = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    // init sizes to the original volume
    for (size_t i = 0; i < count.size(); ++i)
        count[i] = dims[i];

    // Imaris case
    if (fmtHndl->subFormat == FMT_IMS) {
        // Imaris: [z][height][width]
        if (rank == 3) {
            offset[0] = z;
            offset[1] = y;
            offset[2] = x;
            count[0] = 1;
            count[1] = info->height;
            count[2] = info->width;
        } else { // unrecognized case, zero everything
            for (size_t i = 0; i < count.size(); ++i)
                count[i] = 0;
        }

        return;
    }

    // dream3d case
    if (fmtHndl->subFormat == FMT_DREAM3D) {
        if (rank == 2) { // Dream3d: [height][width] - GUESS
            offset[0] = y;
            offset[1] = x;
            count[0] = info->height;
            count[1] = info->width;
        } else if (rank == 3) { // Dream3d: [z][height][width] - GUESS
            offset[0] = bim::max<int>(z, t);
            offset[1] = y;
            offset[2] = x;
            count[0] = 1;
            count[1] = info->height;
            count[2] = info->width;
        } else if (rank == 4 && info->samples >= 1) { // [z][height][width][pixel components]
            offset[0] = bim::max<int>(z, t);
            offset[1] = y;
            offset[2] = x;
            offset[3] = sample;
            count[0] = 1;
            count[1] = info->height;
            count[2] = info->width;
            count[3] = 1;
        } else { // unrecognized case, zero everything
            for (size_t i = 0; i < count.size(); ++i)
                count[i] = 0;
        }

        return;
    }

    // reset some dimensions to produce 2D planes of channels, z and t
    if (rank == 2) {
        // here we simply use the original dimensionality
    } else if (rank == 3 && info->samples > 1 && par->interlaced) {
        // [height][width][pixel components]
        offset[0] = y;
        offset[1] = x;
        offset[2] = sample;
        count[0] = info->height;
        count[1] = info->width;
        count[2] = 1;
    } else if (rank == 3 && info->samples > 1 && !par->interlaced) {
        // [pixel components][height][width]
        offset[0] = sample;
        offset[1] = y;
        offset[2] = x;
        count[0] = 1;
        count[1] = info->height;
        count[2] = info->width;
    } else if (rank == 3 && info->samples == 1 && par->interlaced) {
        // [height][width][z or t]
        offset[0] = y;
        offset[1] = x;
        offset[2] = bim::max<int>(z, t);
        count[0] = info->height;
        count[1] = info->width;
        count[2] = 1;
    } else if (rank == 3 && info->samples == 1 && !par->interlaced) {
        // [z or t][height][width]
        offset[0] = bim::max<int>(z, t);
        offset[1] = y;
        offset[2] = x;
        count[0] = 1;
        count[1] = info->height;
        count[2] = info->width;
    } else if (rank == 4 && info->samples > 1 && par->interlaced) {
        // [z or t][height][width][pixel components]
        offset[0] = bim::max<int>(z, t);
        offset[1] = y;
        offset[2] = x;
        offset[3] = sample;
        count[0] = 1;
        count[1] = info->height;
        count[2] = info->width;
        count[3] = 1;
    } else if (rank == 4 && info->samples > 1 && !par->interlaced) {
        // [pixel components][z or t][height][width]
        offset[0] = sample;
        offset[1] = bim::max<int>(z, t);
        offset[2] = y;
        offset[3] = x;
        count[0] = 1;
        count[1] = 1;
        count[2] = info->height;
        count[3] = info->width;
    } else if (rank == 4 && info->samples == 1) {
        // [t][z][height][width]
        offset[0] = t;
        offset[1] = z;
        offset[2] = y;
        offset[3] = x;
        count[0] = 1;
        count[1] = 1;
        count[2] = info->height;
        count[3] = info->width;
    } else if (rank == 5 && info->samples > 1 && par->interlaced) {
        // [height][width][z][t][pixel components]
        offset[0] = y;
        offset[1] = x;
        offset[2] = z;
        offset[3] = t;
        offset[4] = sample;
        count[0] = info->height;
        count[1] = info->width;
        count[2] = 1;
        count[3] = 1;
        count[4] = 1;
    } else if (rank == 5 && info->samples > 1 && !par->interlaced) {
        // [pixel components][t][z][height][width]
        offset[0] = sample;
        offset[1] = t;
        offset[2] = z;
        offset[3] = y;
        offset[4] = x;
        count[0] = 1;
        count[1] = 1;
        count[2] = 1;
        count[3] = info->height;
        count[4] = info->width;
    } else { // unrecognized case, zero everything
        for (size_t i = 0; i < count.size(); ++i)
            count[i] = 0;
    }
}

//****************************************************************************
// sub-formats: Imaris
//****************************************************************************

void imsGetMultiResolutionPyramidalSizes(const std::vector<size_t> &aDataSize, std::vector<double> &zscales) {
    const float mMinVolumeSizeMB = 1.f;
    std::vector<std::vector<size_t>> aResolutionSizes;
    std::vector<size_t> vNewResolution = aDataSize;
    int N = 3;
    float vVolumeMB;
    do {
        aResolutionSizes.push_back(vNewResolution);
        std::vector<size_t> vLastResolution = vNewResolution;
        size_t vLastVolume = vLastResolution[0] * vLastResolution[1] * vLastResolution[2];
        for (int d = 0; d < N; ++d) {
            if ((10 * vLastResolution[d]) * (10 * vLastResolution[d]) > vLastVolume / vLastResolution[d])
                vNewResolution[d] = vLastResolution[d] / 2;
            else
                vNewResolution[d] = vLastResolution[d];
            // make sure we don't have zero-size dimension
            vNewResolution[d] = std::max((size_t)1, vNewResolution[d]);
        }
        vVolumeMB = (vNewResolution[0] * vNewResolution[1] * vNewResolution[2]) / (1024.f * 1024.f);
    } while (vVolumeMB > mMinVolumeSizeMB);

    zscales.resize(aResolutionSizes.size(), 0);
    double z = (double)aResolutionSizes[0][2];
    for (size_t i = 0; i < aResolutionSizes.size(); ++i) {
        zscales[i] = aResolutionSizes[i][2] / z;
    }
}

void imsGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    //*info = initImageInfo();
    XConf *conf = fmtHndl->arguments;

    H5::Group image = par->file->openGroup("/DataSetInfo/Image");
    bim::xstring tag_num_c = hdf5_get_attribute_string(image, std::string("Noc"));
    if (tag_num_c.size() < 1) {
        tag_num_c = hdf5_get_attribute_string(image, std::string("NumberOfChannels"));
    }
    bim::xstring tag_num_x = hdf5_get_attribute_string(image, std::string("X"));
    bim::xstring tag_num_y = hdf5_get_attribute_string(image, std::string("Y"));
    bim::xstring tag_num_z = hdf5_get_attribute_string(image, std::string("Z"));

    H5::Group time = par->file->openGroup("/DataSetInfo/TimeInfo");
    bim::xstring tag_num_t = hdf5_get_attribute_string(time, std::string("FileTimePoints"));
    if (tag_num_t.size() < 1) {
        tag_num_t = hdf5_get_attribute_string(time, std::string("DatasetTimePoints"));
    }

    // read the very first image to get reliable X,Y,Z sizes and pixel depths
    H5::DataSet dataset = par->file->openDataSet("/DataSet/ResolutionLevel 0/TimePoint 0/Channel 0/Data");
    setDataConfiguration(fmtHndl, dataset, info);

    info->width = tag_num_x.toInt(0);
    info->height = tag_num_y.toInt(0);
    int z = tag_num_z.toInt(0);
    //if (x > 0) info->width = x; // there are cases with bogus values
    //if (y > 0) info->height = y; // there are cases with bogus values
    if (z > 0) { // attempt to safeguard against bogus value
        info->number_z = z;
    }

    info->samples = tag_num_c.toInt(1);
    info->imageMode = info->samples > 1 ? bim::ImageModes::IM_MULTI : bim::ImageModes::IM_GRAYSCALE;
    info->number_t = tag_num_t.toInt();
    info->number_pages = info->number_z * info->number_t;
    info->number_dims = 2;
    if (info->number_z > 1) ++info->number_dims;
    if (info->number_t > 1) ++info->number_dims;

    par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X] = static_cast<int>(info->width);
    par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y] = static_cast<int>(info->height);
    par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Z] = static_cast<int>(info->number_z);
    info->tileWidth = par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X];
    info->tileHeight = par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y];

    par->resolution_levels = 1;
    for (int r = 0; r < 1000; ++r) {
        try {
            bim::xstring path = xstring::xprintf("/DataSet/ResolutionLevel %d", par->resolution_levels);
            H5::Group res = par->file->openGroup(path.c_str());
            ++par->resolution_levels;
        } catch (H5::FileIException error) {
            break;
        }
    }
    info->number_levels = par->resolution_levels;
    par->resolution_levels = (int)info->number_levels;

    std::vector<size_t> aDataSize = {info->width, info->height, info->number_z};
    imsGetMultiResolutionPyramidalSizes(aDataSize, par->zscales);

    info->resUnits = bim::ResolutionUnits::RES_IN;
    info->xRes = 0;
    info->yRes = 0;
}

//----------------------------------------------------------------------------
// Imaris Metadata
//----------------------------------------------------------------------------

void ims_getset_metadata(TagMap *hash, const xstring &hpath, const xstring &mpath) {
    if (!hash->hasKey(hpath)) return;
    xstring s = hash->get_value(hpath);
    hash->set_value(mpath, s);
}

void ims_parse_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;

    // parse channels
    for (int i = 0; i < (int)info->samples; ++i) {
        xstring hpath = xstring::xprintf("Imaris/Channel %d/", i);
        xstring mpath = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i);

        ims_getset_metadata(hash, hpath + "Name", mpath + bim::CHANNEL_INFO_NAME);
        ims_getset_metadata(hash, hpath + "Description", mpath + bim::CHANNEL_INFO_DESCRIPTION);
        ims_getset_metadata(hash, hpath + "GammaCorrection", mpath + bim::CHANNEL_INFO_GAMMA);
        ims_getset_metadata(hash, hpath + "ColorOpacity", mpath + bim::CHANNEL_INFO_OPACITY);
        ims_getset_metadata(hash, hpath + "Pinhole", mpath + bim::CHANNEL_INFO_PINHOLE_RADIUS);
        ims_getset_metadata(hash, hpath + "LSMPinhole", mpath + bim::CHANNEL_INFO_PINHOLE_RADIUS);
        ims_getset_metadata(hash, hpath + "LSMEmissionWavelength", mpath + bim::CHANNEL_INFO_EM_WAVELENGTH);
        ims_getset_metadata(hash, hpath + "LSMExcitationWavelength", mpath + bim::CHANNEL_INFO_EM_WAVELENGTH);

        if (hash->hasKey(hpath + "Color")) {
            xstring s = hash->get_value(hpath + "Color");
            std::vector<double> rgb = s.splitDouble(" ");
            hash->set_value(mpath + bim::CHANNEL_INFO_COLOR, bim::xstring::xprintf("%.2f,%.2f,%.2f", rgb[0], rgb[1], rgb[2]));

            // write old-style tags, to be removed by version 3
            //hash->set_value(bim::xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), i), bim::xstring::xprintf("%.0f,%.0f,%.0f", rgb[0] * 255, rgb[1] * 255, rgb[2] * 255));
        }

        // write old-style tags, to be removed by version 3
        //ims_getset_metadata(hash, hpath + "Name", bim::xstring::xprintf(bim::CHANNEL_NAME_TEMPLATE.c_str(), i));
    }

    // objective and resolutioin
    //ims_getset_metadata(hash, "Imaris/Image/RecordingDate", bim::DOCUMENT_DATETIME);
    bim::xstring date = hash->get_value("Imaris/Image/RecordingDate");
    if (date.size() > 0) {
        hash->set_value(bim::DOCUMENT_DATETIME, bim::DateTime::from_string(date, "%Y-%m-%d %H:%M:%S").to_string_iso8601());
    }

    ims_getset_metadata(hash, "Imaris/Image/Lenspower", xstring::xprintf(bim::OBJECTIVE_INFO_TEMPLATE.c_str(), 0) + bim::OBJECTIVE_NAME);
    ims_getset_metadata(hash, "Imaris/Image/NumericalAperture", xstring::xprintf(bim::OBJECTIVE_INFO_TEMPLATE.c_str(), 0) + bim::OBJECTIVE_NUMERICAL_APERTURE);

    xstring units = hash->get_value("Imaris/Image/Unit");
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X.c_str(), units);
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y.c_str(), units);
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Z.c_str(), units);

    double min0 = hash->get_value_double("Imaris/Image/ExtMin0", 0);
    double max0 = hash->get_value_double("Imaris/Image/ExtMax0", 0);
    double min1 = hash->get_value_double("Imaris/Image/ExtMin1", 0);
    double max1 = hash->get_value_double("Imaris/Image/ExtMax1", 0);
    double min2 = hash->get_value_double("Imaris/Image/ExtMin2", 0);
    double max2 = hash->get_value_double("Imaris/Image/ExtMax2", 0);

    double xr = (max0 - min0) / info->width;
    double yr = (max1 - min1) / info->height;
    double zr = (max2 - min2) / info->number_z;
    hash->set_value(bim::PIXEL_RESOLUTION_X.c_str(), xr);
    hash->set_value(bim::PIXEL_RESOLUTION_Y.c_str(), yr);
    hash->set_value(bim::PIXEL_RESOLUTION_Z.c_str(), zr);
}

bim::uint ims_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    //bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    // append image pyramid meta
    if (par->resolution_levels > 1) {
        std::vector<double> scales;
        double scale = 1.0;
        int level = 0;
        for (int level = 0; level < par->resolution_levels; ++level) {
            scales.push_back(scale);
            scale /= 2.0;
        }

        hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_HIERARCHICAL);
        hash->set_value(bim::IMAGE_NUM_RES_L, par->resolution_levels);
        hash->set_value(bim::IMAGE_RES_L_SCALES, xstring::join(scales, ","));
    }

    // parse all metadata
    if (conf && conf->hasKeyWith("-meta")) {
        xstring h5path = "/DataSetInfo";
        bim::xstring meta_path = "Imaris";
        hdf5_meta_walker(par, hash, h5path, meta_path);

        // parse channels, resolution, etc...
        ims_parse_metadata(fmtHndl, hash);
    }

    return 0;
}

//----------------------------------------------------------------------------------------
// Imaris pixels reader
//----------------------------------------------------------------------------------------

bim::uint ims_read_pixels(FormatHandle *fmtHndl, bim::uint page, uint64 x = 0, uint64 y = 0, uint64 w = 0, uint64 h = 0, uint64 l = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;

    int z, t;
    hdf5_init_coordinate(fmtHndl, page, z, t);

    // update scale info
    float scale = (float)(1.0 / pow(2.0, (float)l));
    int plane_width = (int)floor(par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X] * scale);
    int plane_height = (int)floor(par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y] * scale);
    if (scale < 1) {
        info->width = plane_width;
        info->height = plane_height;
        // Z is interpolated only after the volume is isotropic
        if (par->zscales.size() > l) {
            info->number_z = (bim::uint64)floor(par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Z] * par->zscales[l]);
            z = (int)floor(z * par->zscales[l]);
        }
    }

    // update tile info
    if (w > 0 && h > 0) {
        info->width = bim::min<bim::uint64>(w, plane_width - x);
        info->height = bim::min<bim::uint64>(h, plane_height - y);
    }

    // allocate image
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    // Define the memory dataspace - 2D plane for one channel
    std::vector<hsize_t> dimsm(2, 1);
    dimsm[0] = info->height;
    dimsm[1] = info->width;

    // define dimfloat scale = 1.0 / pow(2.0, (float)level);ensions in the dataspace
    int rank = 3;
    std::vector<hsize_t> dims(rank); // Imaris: [z][height][width]
    dims[0] = info->number_z;
    dims[1] = plane_height;
    dims[2] = plane_width;

    // Define memory hyperslab - 2D plane for one channel
    H5::DataSpace memspace(2, &dimsm[0]);
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = info->height;
    count_out[1] = info->width;
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file

    // read slabs sample by sample
    for (int sample = 0; sample < (int)info->samples; ++sample) {
        xstring path = xstring::xprintf("/DataSet/ResolutionLevel %d/TimePoint %d/Channel %d/Data", l, t, sample);
        H5::DataSet dataset = par->file->openDataSet(path.c_str());
        H5::DataType dt = dataset.getDataType();
        H5::DataSpace dataspace = dataset.getSpace();

        // Define hyperslab in the dataset; implicitly passing stride and block as NULL
        hdf5_set_offset_count(fmtHndl, info, dims, rank, sample, z, t, offset, count, static_cast<bim::uint>(x), static_cast<bim::uint>(y));
        dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

        // Read data from hyperslab in the file into the hyperslab in memory
        dataset.read(bmp->bits[sample], dt, memspace, dataspace);
    }

    return 0;
}

//****************************************************************************
// sub-formats: VQI
//****************************************************************************


bool check_image_node(bim::HDF5Params *par, const xstring &h5_path) {
    try {
        H5::DataSet dataset = par->file->openDataSet(h5_path.c_str());
        bim::xstring h5_class = hdf5_get_attribute_string(dataset, std::string("CLASS"));
        if (h5_class != "IMAGE") return false;
    } catch (...) { //catch (H5::FileIException error) {
        return false;
    }
    return true;
}

void viqi_walker(bim::HDF5Params *par, H5::Group &obj, const xstring &h5_path, std::vector<xstring> &objects, const int maybe_max_objects = -1) {
    const size_t max_objects = maybe_max_objects >= 0 ? maybe_max_objects : 0;

    // first find all datasets at the current level
    for (size_t i = 0; i < obj.getNumObjs(); ++i) {
        bim::xstring name(BIM_STR_SIZE, 0);
        ssize_t sz = obj.getObjnameByIdx(i, &name[0], BIM_STR_SIZE);
        name.resize(sz);

        bim::xstring path = h5_path + "/" + name;
        try {
            H5::Group group = par->file->openGroup(path.c_str());

            bim::xstring viqi_image_type = hdf5_get_attribute_string(group, std::string("viqi_image_type"));
            if (viqi_image_type.size() < 1) continue;

            objects.push_back(path);
            if (maybe_max_objects >= 0 && objects.size() >= max_objects) return;
        } catch (...) { //catch (H5::FileIException error) {
            if (!check_image_node(par, path)) continue;
            objects.push_back(path);
            if (maybe_max_objects >= 0 && objects.size() >= max_objects) return;
        }
    }

    // then explore depth
    for (size_t i = 0; i < obj.getNumObjs(); ++i) {
        bim::xstring name(BIM_STR_SIZE, 0);
        ssize_t sz = obj.getObjnameByIdx(i, &name[0], BIM_STR_SIZE);
        name.resize(sz);

        bim::xstring path = h5_path + "/" + name;
        try {
            H5::Group group = par->file->openGroup(path.c_str());

            bim::xstring viqi_image_type = hdf5_get_attribute_string(group, std::string("viqi_image_type"));
            if (viqi_image_type.size() > 0) continue;

            viqi_walker(par, group, path, objects, (int) max_objects);
            if (maybe_max_objects >= 0 && objects.size() >= max_objects) return;
        } catch (H5::FileIException error) {
            // do nothing
        }
    }
}

std::vector<int> viqi_get_item_info(bim::HDF5Params *par, xstring path, int block_id, int item_id) {
    int bboxsz = par->num_spatial_dimensions * 2;
    int indexsz = 2 + bboxsz;
    std::vector<int> item_info(indexsz, 0);
    xstring path_index = path + "_sparse_index";

    std::vector<int> block_index;
    hdf5_read_vector_int(par, path_index.c_str(), block_index);

    for (size_t i = 0; i < block_index.size(); i += indexsz) {
        if (block_index[i] == item_id) {
            memcpy(&item_info[0], &block_index[i + 0], sizeof(int) * indexsz);
            break;
        }
    }

    return item_info;
}

int viqi_get_first_block_id(bim::HDF5Params *par, xstring path, int level=0) {
    xstring path_level = path + "/level_000/";

    // item_id is not defined so find first available block
    for (int i = 0; i < 1000; ++i) {
        xstring path_item = path + xstring::xprintf("/level_%.3d/block_%.4d", level, i);
        try {
#if H5_VERSION_LE(1, 10, 4)
            H5::DataSet dataset = par->file->openDataSet(path_item.c_str());
            return i;
#else
            if (par->file->nameExists(path_item.c_str()))
                return i;
#endif
        } catch (H5::FileIException error) {
            // do nothing and try the next one
        }
    } // for i

    return -1;
}

int viqi_get_item_block(bim::HDF5Params *par, xstring path, int item_id) {
    int block_id = item_id / bim::VIQI_MAX_ITEMS_PER_BLOCK; // quick estimate knowing max number of items per block
    xstring path_level = path + "/level_000/";

    if (item_id >= 0) {
        // find the value in the offsets index
        try {
            H5::Group group = par->file->openGroup(path_level.c_str());
            std::vector<int> item_id_offsets = hdf5_get_attribute_int_vector(group, "viqi_block_item_id_offsets");
            for (int i = (int)item_id_offsets.size() - 1; i >= 0; --i) {
                if (item_id >= item_id_offsets[i])
                    return i;
            }
            return 0;
        } catch (H5::FileIException error) {
        }
    } else {
        // item_id is not defined so find first available block
        for (int i = 0; i < 1000; ++i) {
            xstring path_item = path_level + xstring::xprintf("block_%.4d", i);
            try {
#if H5_VERSION_LE(1, 10, 4)
                H5::DataSet dataset = par->file->openDataSet(path_item.c_str());
                return i;
#else
                if (par->file->nameExists(path_item.c_str()))
                    return i;
#endif
            } catch (H5::FileIException error) {
                // do nothing and try the next one
            }
        } // for i
    }
    return block_id;
}

bool viqiGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return false;
    if (fmtHndl->internalParams == NULL) return false;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    //*info = initImageInfo();
    XConf *conf = fmtHndl->arguments;

    par->block_id = -1;
    par->item_id = -1;

    // detect first element to list if path is empty
    if (conf) {
        par->path = conf->getValue("-path");
    }
    if (par->path.size() < 1) {
        std::vector<xstring> objects;
        H5::Group root = par->file->openGroup("/");
        viqi_walker(par, root, "", objects, 1);
        par->path = objects[0];
    }

    info->resUnits = bim::ResolutionUnits::RES_IN;
    info->xRes = 0;
    info->yRes = 0;

    // default 2D image configuration, in case no metadata is defined
    par->num_spatial_dimensions = 2;
    if (par->viqi_image_interleaved) {
        par->dimensions_indices[0] = (bim::uint64)bim::ImageDims::DIM_Y;
        par->dimensions_indices[1] = (bim::uint64)bim::ImageDims::DIM_X;
        par->dimensions_indices[2] = (bim::uint64)bim::ImageDims::DIM_C;
    } else {
        par->dimensions_indices[0] = (bim::uint64)bim::ImageDims::DIM_C;
        par->dimensions_indices[1] = (bim::uint64)bim::ImageDims::DIM_Y;
        par->dimensions_indices[2] = (bim::uint64)bim::ImageDims::DIM_X;
    }

    if (conf && conf->keyExists("-slice-spectrum")) {
        //par->position_spectrum = conf->getValueInt("-slice-spectrum");
        par->position_spectrum = conf->getDimensionSlices("-slice-spectrum");
    }

    if (conf && conf->keyExists("-slice-item")) {
        par->item_id = conf->getValueInt("-slice-item");
        // read the first available block in order to define pixel-level meta
        par->block_id = viqi_get_item_block(par, par->path, par->item_id);
    }

    if (conf && conf->keyExists("-interpolation")) {
        par->interpolation = str_to_interpolation(conf->getValue("-interpolation"));
        par->user_given_interpolation = true;
    }

    // init table info
    par->table.init(par->file, par->path);
    if (par->table.measures.size() > 0)
        par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_MEASURE] = static_cast<int>(par->table.measures.size());

    // try to read provided path, in case it does not exist it must be a paged item, attemp to find
    try {
        H5::Group group = par->file->openGroup(par->path.c_str());
        par->viqi_image_size = hdf5_get_attribute_int_vector(group, "viqi_image_size");
        par->viqi_image_dimensions = hdf5_get_attribute_string(group, "viqi_image_dimensions", "Y,X").replace(" ", "").toUpperCase().split(",");
        if (par->viqi_image_dimensions[par->viqi_image_dimensions.size() - 1] == "C")
            par->viqi_image_interleaved = true;
        par->viqi_image_content = hdf5_get_attribute_string(group, "viqi_image_content");
        if (par->viqi_image_content == "spectral") {
            par->spectral_wavelengths = hdf5_get_attribute_int_vector(group, "viqi_spectral_wavelengths");
            par->spectral_wavelength_units = hdf5_get_attribute_string(group, "viqi_spectral_wavelengths_units");
        }

        //par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X] = par->viqi_image_size[1];
        //par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y] = par->viqi_image_size[0];
        //par->plane_width = par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X];
        //par->plane_height = par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y];

        // default 2D image configuration, in case no metadata is defined
        if (par->viqi_image_size.size() > 1) {
            par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y] = par->viqi_image_size[0];
            par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X] = par->viqi_image_size[1];
        }

        // supplied image size does not contain channels, adjust accordingly
        if (par->viqi_image_size.size() < par->viqi_image_dimensions.size()) {
            if (par->viqi_image_dimensions[0].toLowerCase() == "c")
                par->viqi_image_size.insert(par->viqi_image_size.begin(), 1);
            if (par->viqi_image_dimensions[par->viqi_image_dimensions.size()-1].toLowerCase() == "c")
                par->viqi_image_size.push_back(1);
        }

        // do a proper dim test using strings: Y,X,Z,C,...
        for (size_t d = 0; d < par->viqi_image_dimensions.size(); ++d) {
            if (d >= par->viqi_image_size.size()) break;
            xstring dim_name = par->viqi_image_dimensions[d].toLowerCase();
            //if (dim_name == "c") continue; // dima: for now channels are not written into image size
            std::map<std::string, int>::const_iterator it = bim::meta::dimension_names.find(dim_name);
            if (it == bim::meta::dimension_names.end()) continue;
            int dim_idx = (*it).second;
            if (dim_idx > 1) { // safeguard for initially wrong strings: XYC while need to be YXC
                par->dimensions_indices[d] = dim_idx;
                par->dimensions_image[dim_idx] = par->viqi_image_size[d];
            }
            if (dim_idx > 2) {
                ++par->num_spatial_dimensions;
            }
        }
        info->width = par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X];
        info->height = par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y];
        info->number_z = par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Z];
        info->number_t = par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_T];
        info->samples = par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_C];
        info->number_dims = par->num_spatial_dimensions;

        par->mask_mode = par->viqi_image_content == "mask"; // && par->hdf_pixel_format == H5T_BITFIELD;
        par->values_from_measures = par->viqi_image_content == "mask" || par->viqi_image_content == "heatmap";

        xstring path_level = par->path + xstring::xprintf("/level_%.3d", 0);
        int block_id = viqi_get_first_block_id(par, par->path, 0);
        xstring path_block = path_level + xstring::xprintf("/block_%.4d", block_id);

        H5::DataSet dataset = par->file->openDataSet(path_block.c_str());
        fmtHndl->subFormat = FMT_VIQI;
        setDataConfiguration(fmtHndl, dataset, info);

        group = par->file->openGroup(path_level.c_str());
        par->num_blocks = hdf5_get_attribute_int(group, "viqi_storage_number_blocks");
        if (par->num_blocks < 1) {
            par->num_blocks = (int)hdf5_get_attribute_double(group, "viqi_storage_number_blocks");
        }
        par->num_items = hdf5_get_attribute_int(group, "viqi_storage_sparse_items");
        par->block_bboxs = hdf5_get_attribute_int_vector(group, "viqi_storage_block_bboxes");
        par->sparse_mode = par->num_items > 0;
    } catch (H5::FileIException error) {
        if (conf && conf->keyExists("-slice-item")) {
            par->item_id = conf->getValueInt("-slice-item");
        } else {
            // process path containing element within
            std::vector<xstring> p = par->path.split("/");
            if (p.size() >= 2) {
                xstring path_suffix = p[p.size() - 1];
                par->path = par->path.replace(xstring("/") + path_suffix, "");
                par->item_id = path_suffix.toInt();
            }
        }

        // read the first available block in order to define pixel-level meta
        try {
            par->block_id = viqi_get_item_block(par, par->path, par->item_id);
        } catch (...) {
            return false;
        }
    }

    fmtHndl->subFormat = FMT_VIQI;

    // if item is requested
    if (par->item_id >= 0 && par->block_id >= 0) {
        xstring path_item = par->path + "/level_000/" + xstring::xprintf("block_%.4d", par->block_id);
        try {
            H5::DataSet dataset = par->file->openDataSet(path_item.c_str());
            par->sparse_element_read = true;
            setDataConfiguration(fmtHndl, dataset, info);

            xstring block_content = hdf5_get_attribute_string(dataset, "viqi_block_content");
            par->mask_mode = block_content == "mask";
            //par->values_from_measures = block_content == "mask" || block_content == "heatmap";
            par->values_from_measures = false;

            par->sparse_mode = hdf5_get_attribute_string(dataset, "viqi_block_format") == "sparse";
            std::vector<int> item_info = viqi_get_item_info(par, path_item, par->block_id, par->item_id);
            bim::Bbox<int> item_bbox(&item_info[2], par->num_spatial_dimensions);

            info->width = item_bbox.getWidth();
            info->height = item_bbox.getHeight();
            par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X] = static_cast<int>(info->width);
            par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y] = static_cast<int>(info->height);
            //par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] = std::min(info->width, par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X]);
            //par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] = std::min(info->height, par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y]);
        } catch (H5::FileIException error) {
            return false;
        }
    }

    //par->plane_width = info->width; // dima: not sure why to set this?
    //par->plane_height = info->height;

    // read resolution levels
    par->resolution_levels = 0;
    for (int r = 0; r < 1000; ++r) {
        try {
            xstring path_res = par->path + xstring::xprintf("/level_%.3d", r);
            H5::Group group = par->file->openGroup(path_res.c_str());
            double scale = hdf5_get_attribute_double(group, "viqi_level_scale");
            if (scale == 0) break;
            par->scales.push_back(scale);
            ++par->resolution_levels;
        } catch (H5::FileIException error) {
            break;
        }
    }
    par->resolution_levels = std::max<int>(1, par->resolution_levels);
    info->number_levels = par->resolution_levels;
    info->tileWidth = par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] > 0 ? par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] : 128;
    info->tileHeight = par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] > 0 ? par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] : 128;

    info->imageMode = info->samples > 1 ? bim::ImageModes::IM_MULTI : bim::ImageModes::IM_GRAYSCALE;
    if (par->viqi_image_content == "spectral") {
        info->imageMode = bim::ImageModes::IM_SPECTRAL;
    } else if (par->viqi_image_content == "mask") {
        info->imageMode = bim::ImageModes::IM_MASK;
    } else if (par->viqi_image_content == "heatmap") {
        info->imageMode = bim::ImageModes::IM_HEATMAP;
    }

    return true;
}

//----------------------------------------------------------------------------
// VQI Metadata
//----------------------------------------------------------------------------

void viqi_parse_json_object(bim::TagMap *hash, Jzon::Node &parent_node, const xstring &path) {
    int i = 0;
    for (Jzon::Node::iterator it = parent_node.begin(); it != parent_node.end(); ++it) {
        std::string name = (*it).first;
        Jzon::Node &node = (*it).second;

        if (node.isValid() && node.isArray() && path.endsWith("tie_points/fovs/")) {
            name = xstring::xprintf("%.5d", i);
            if (node.isArray()) {
                size_t sz = node.getCount();
                std::vector<xstring> coord;
                for (size_t j = 0; j < sz; ++j) {
                    Jzon::Node vn = node.get(j);
                    coord.push_back(vn.toString());
                }
                hash->set_value(path + name, xstring::join(coord, ","));
            }
        } else {
            if (node.isValid()) {
                if (name.size() < 1 && path.endsWith("channels/")) {
                    name = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i).replace("channels/", "").replace("/", "");
                }
                viqi_parse_json_object(hash, node, path + name + '/');
            }
            std::string value = node.toString();
            if (value.size() > 0)
                hash->set_value(path + name, value);
        }
        ++i;
    }
}

void viqi_parse_json(const xstring &str, TagMap *hash) {
    if (str.size() < 1) return;
    Jzon::Parser jparser;
    Jzon::Node rootNode = jparser.parseString(str);
    if (rootNode.isValid()) {
        std::string path = "";
        viqi_parse_json_object(hash, rootNode, path);
    }
}

void viqi_parse_xml(const xstring &str, TagMap *hash) {
    if (str.size() < 1) return;
}

bim::uint viqi_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    // append image pyramid meta
    if (par->resolution_levels > 1 || par->viqi_image_content == "heatmap") {
        //hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_HIERARCHICAL);
        hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_ARBITRARY);
        hash->set_value(bim::IMAGE_NUM_RES_L, par->resolution_levels);
        hash->set_value(bim::IMAGE_RES_L_SCALES, xstring::join(par->scales, ","));
    }

    hash->set_value(bim::IMAGE_NUM_X, (int)par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X]);
    hash->set_value(bim::IMAGE_NUM_Y, (int)par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y]);

    // return the number of items, fovs (blocks), resolutions, etc...
    if (par->num_blocks > 0) hash->set_value(bim::IMAGE_NUM_FOV, (int)par->num_blocks);
    if (par->num_items > 0) hash->set_value(bim::IMAGE_NUM_ITEMS, (int)par->num_items);

    if (par->viqi_image_content == "spectral") {
        hash->set_value(bim::IMAGE_NUM_SPECTRA, (int)par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_SPECTRA]);
        hash->set_value(bim::ICC_TAGS_COLORSPACE, bim::ICC_TAGS_COLORSPACE_SPECTRAL);
    } else if (par->viqi_image_content == "mask") {
        hash->set_value(bim::IMAGE_MODE, par->viqi_image_content);
        hash->set_value(bim::ICC_TAGS_COLORSPACE, bim::ICC_TAGS_COLORSPACE_MASK);
    } else if (par->viqi_image_content == "heatmap") {
        hash->set_value(bim::IMAGE_MODE, par->viqi_image_content);
        hash->set_value(bim::ICC_TAGS_COLORSPACE, bim::ICC_TAGS_COLORSPACE_HEATMAP);
    }

    // parse all metadata
    if (!conf || !conf->hasKeyWith("-meta")) return 0;

    if (par->spectral_wavelengths.size() > 0) {
        hash->set_value(bim::IMAGE_SPECTRAL_WAVELENGTHS, par->spectral_wavelengths);
        hash->set_value(bim::IMAGE_SPECTRAL_WAVELENGTH_UNITS, par->spectral_wavelength_units);
    }

    if (par->table.has_metadata()) {
        hash->set_value("pixel_format", pixelFormat2viqi(info));
        par->table.append_metadata(hash);
    }

    if (conf && !conf->hasKey("-path")) {
        std::vector<xstring> objects;
        H5::Group root = par->file->openGroup("/");
        viqi_walker(par, root, "", objects, 100);

        // augment with pre-defined internal objects
        for (int i = static_cast<int>(objects.size()) - 1; i >= 0; --i) {
            xstring name = objects[i] + "/correction_flatfield";
            if (check_image_node(par, name)) objects.push_back(name);

            name = objects[i] + "/correction_darknoise";
            if (check_image_node(par, name)) objects.push_back(name);

            name = objects[i] + "/correction_background";
            if (check_image_node(par, name)) objects.push_back(name);
        }

        int num_labels = 0;
        int num_previews = 0;
        for (size_t i = 0; i < objects.size(); ++i) {
            if (objects[i].endsWith("/label")) ++num_labels;
            if (objects[i].endsWith("/preview")) ++num_previews;
            hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), i), objects[i]);
        }
        hash->set_value(bim::IMAGE_NUM_SERIES, (int)objects.size());
        hash->set_value(bim::IMAGE_NUM_LABELS, num_labels);
        hash->set_value(bim::IMAGE_NUM_PREVIEWS, num_previews);
    }

    try {
        H5::Group group = par->file->openGroup(par->path.c_str());
        xstring metadata_json = hdf5_get_attribute_string(group, "viqi_metadata_json");
        viqi_parse_json(metadata_json, hash);

        xstring metadata_xml = hdf5_get_attribute_string(group, "viqi_metadata_xml");
        viqi_parse_xml(metadata_xml, hash);
    } catch (H5::FileIException error) {
    }

    // update old schema color tags
    /*
    for (int i = 0; i < par->image_num_samples; ++i) {
        xstring new_path = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i) + bim::CHANNEL_INFO_COLOR;
        xstring old_path = xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), i);
        if (hash->hasKey(old_path)) continue;
        xstring val = hash->get_value(new_path);
        if (!val.contains(".")) { // if values are ints
            hash->set_value(old_path, val);
            continue;
        }
        //if values are new format 0-1
        std::vector<double> vd = val.splitDouble(",");
        std::vector<int> vi(vd.size(), 0);
        for (size_t p = 0; p < vd.size(); ++p) {
            vi[p] = bim::trim<int>(bim::round<double>(vd[p] * 255.0), 0, 255);
        }
        hash->set_value(old_path, xstring::join(vi, ","));
    }
    */

    return 0;
}

//----------------------------------------------------------------------------------------
// ViQi pixels reader
//----------------------------------------------------------------------------------------

void viqi_init_coordinate(FormatHandle *fmtHndl, int x, int y, int sample, bool dense) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    std::fill(par->positions.begin(), par->positions.end(), 0);
    par->positions[(bim::uint64) bim::ImageDims::DIM_X] = x;
    par->positions[(bim::uint64) bim::ImageDims::DIM_Y] = y;
    par->positions[(bim::uint64) bim::ImageDims::DIM_C] = sample;
    if (conf) {
        par->positions[(bim::uint64) bim::ImageDims::DIM_Z] = conf->getValueInt("-slice-z", -1);
        par->positions[(bim::uint64) bim::ImageDims::DIM_T] = conf->getValueInt("-slice-t", -1);
        par->positions[(bim::uint64) bim::ImageDims::DIM_SERIE] = conf->getValueInt("-slice-serie", 0);
        par->positions[(bim::uint64) bim::ImageDims::DIM_FOV] = conf->getValueInt("-slice-fov", 0);
        par->positions[(bim::uint64) bim::ImageDims::DIM_ROTATION] = conf->getValueInt("-slice-rotation", 0);
        par->positions[(bim::uint64) bim::ImageDims::DIM_SCENE] = conf->getValueInt("-slice-scene", 0);
        par->positions[(bim::uint64) bim::ImageDims::DIM_ILLUM] = conf->getValueInt("-slice-illum", 0);
        par->positions[(bim::uint64) bim::ImageDims::DIM_PHASE] = conf->getValueInt("-slice-phase", 0);
        par->positions[(bim::uint64) bim::ImageDims::DIM_VIEW] = conf->getValueInt("-slice-view", 0);
        par->positions[(bim::uint64) bim::ImageDims::DIM_ITEM] = conf->getValueInt("-slice-item", 0);
        par->positions[(bim::uint64) bim::ImageDims::DIM_SPECTRA] = std::min<int>(conf->getValueInt("-slice-spectrum", 0), par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_SPECTRA] - 1);
        par->positions[(bim::uint64) bim::ImageDims::DIM_MEASURE] = std::min<int>(conf->getValueInt("-slice-measure", 0), par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_MEASURE] - 1);
    }

    if (info->number_z > 1 && info->number_t <= 1 && par->positions[(bim::uint64) bim::ImageDims::DIM_Z] < 0) {
        par->positions[(bim::uint64) bim::ImageDims::DIM_Z] = static_cast<int>(fmtHndl->pageNumber);
        par->positions[(bim::uint64) bim::ImageDims::DIM_T] = 0;
    } else if (info->number_z <= 1 && info->number_t > 1 && par->positions[(bim::uint64) bim::ImageDims::DIM_T] < 0) {
        par->positions[(bim::uint64) bim::ImageDims::DIM_T] = static_cast<int>(fmtHndl->pageNumber);
        par->positions[(bim::uint64) bim::ImageDims::DIM_Z] = 0;
    } else if (info->number_z > 1 && info->number_t > 1 && par->positions[(bim::uint64) bim::ImageDims::DIM_Z] < 0 && par->positions[(bim::uint64) bim::ImageDims::DIM_T] < 0) {
        par->positions[(bim::uint64) bim::ImageDims::DIM_T] = (int)floor(fmtHndl->pageNumber / info->number_z);
        par->positions[(bim::uint64) bim::ImageDims::DIM_Z] = (int)(fmtHndl->pageNumber - par->positions[(bim::uint64) bim::ImageDims::DIM_T] * info->number_z);
    }
    par->positions[(bim::uint64) bim::ImageDims::DIM_Z] = bim::max(0, par->positions[(bim::uint64) bim::ImageDims::DIM_Z]);
    par->positions[(bim::uint64) bim::ImageDims::DIM_T] = bim::max(0, par->positions[(bim::uint64) bim::ImageDims::DIM_T]);

    // deal with the measures case
    if (dense && par->table.hasMeasures()) {
        par->positions[(bim::uint64) bim::ImageDims::DIM_C] = par->positions[(bim::uint64) bim::ImageDims::DIM_MEASURE];
    }
}

void viqi_set_offset_count(FormatHandle *fmtHndl, bim::ImageInfo *info, std::vector<hsize_t> &dims, std::vector<hsize_t> &offset, std::vector<hsize_t> &count, bool dense = false) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;

    int rank = static_cast<int>(dims.size());
    std::fill(offset.begin(), offset.end(), 0);
    std::fill(count.begin(), count.end(), 1);

    // set proper XY
    if (rank >= 2) {
        int dim_x = !par->viqi_image_interleaved ? rank - 1 : 1;
        int dim_y = !par->viqi_image_interleaved ? rank - 2 : 0;
        offset[dim_y] = par->positions[(bim::uint64)bim::ImageDims::DIM_Y];
        offset[dim_x] = par->positions[(bim::uint64)bim::ImageDims::DIM_X];
        count[dim_y] = std::min<hsize_t>(info->height, dims[dim_y]);
        count[dim_x] = std::min<hsize_t>(info->width, dims[dim_x]);
    }

    // set all other dimensions
    // dima: this needs to change but for now we don't have other ND dims
    /*for (int d = 2; d < rank; ++d) { // dima:  <= rnk
        offset[d] = par->positions[par->dimensions_indices[d]];
        count[d] = 1;
    }*/

    // set channels
    if (info->samples > 1) {
        int dim_c = par->viqi_image_interleaved ? rank - 1 : rank - 3;
        offset[dim_c] = par->positions[(bim::uint64)bim::ImageDims::DIM_C];
        count[dim_c] = 1;
    }

    // deal with the measures case
    if (dense && rank>2 && par->table.hasMeasures()) {
        int dim_c = par->viqi_image_interleaved ? rank - 1 : rank - 3;
        if (static_cast<hssize_t>(dims[dim_c]) > par->positions[(bim::uint64)bim::ImageDims::DIM_C]) {
            offset[dim_c] = par->positions[(bim::uint64)bim::ImageDims::DIM_C];
        } else {
            offset[dim_c] = 0;
        }
        count[dim_c] = 1;
    }

    // set spectral slices
    int sample = par->positions[(bim::uint64) bim::ImageDims::DIM_C];
    if (rank > 2 && par->viqi_image_content == "spectral" && static_cast<int>(par->position_spectrum.size()) > sample) {
        int dim_c = par->viqi_image_interleaved ? rank - 1 : rank - 3;
        offset[dim_c] = par->position_spectrum[sample];
        count[dim_c] = 1;
    }
}

template<typename T, typename Tw>
void buf_mult(T *buf, bim::int64 sz, double v) {
    T minv = std::numeric_limits<T>::is_integer ? std::numeric_limits<T>::min() : (T)(-1 * (double)std::numeric_limits<T>::max());
    T maxv = std::numeric_limits<T>::max();
    for (bim::int64 x = 0; x < sz; ++x) {
        buf[x] = bim::trim<T, Tw>((Tw)buf[x] * v, minv, maxv);
    }
}

void buffer_multiply(void *buf, bim::int64 sz, double v, int depth, bim::DataFormat pf) {
    if (depth == 8 && pf == bim::DataFormat::FMT_UNSIGNED)
        buf_mult<bim::uint8, double>((bim::uint8 *)buf, sz, v);
    else if (depth == 16 && pf == bim::DataFormat::FMT_UNSIGNED)
        buf_mult<bim::uint16, double>((bim::uint16 *)buf, sz, v);
    else if (depth == 32 && pf == bim::DataFormat::FMT_UNSIGNED)
        buf_mult<bim::uint32, double>((bim::uint32 *)buf, sz, v);
    else if (depth == 8 && pf == bim::DataFormat::FMT_SIGNED)
        buf_mult<bim::int8, double>((bim::int8 *)buf, sz, v);
    else if (depth == 16 && pf == bim::DataFormat::FMT_SIGNED)
        buf_mult<bim::int16, double>((bim::int16 *)buf, sz, v);
    else if (depth == 32 && pf == bim::DataFormat::FMT_SIGNED)
        buf_mult<bim::int32, double>((bim::int32 *)buf, sz, v);
    else if (depth == 32 && pf == bim::DataFormat::FMT_FLOAT)
        buf_mult<bim::float32, double>((bim::float32 *)buf, sz, v);
    else if (depth == 64 && pf == bim::DataFormat::FMT_FLOAT)
        buf_mult<bim::float64, double>((bim::float64 *)buf, sz, v);
}

bim::uint viqi_fill_item(H5::DataSet *dataset, FormatHandle *fmtHndl, int *item_info, bim::Image &item_image, bim::Image &item_mask, bim::uint block_id) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    H5::DataType dt = dataset->getDataType();

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = dataset->getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    int dim_x = !par->viqi_image_interleaved ? rank - 1 : 1;
    int dim_y = !par->viqi_image_interleaved ? rank - 2 : 0;

    // Define the memory dataspace - 2D plane for one channel
    std::vector<hsize_t> dimsm(2, 1);
    dimsm[dim_y] = item_image.height();
    dimsm[dim_x] = item_image.width();
    H5::DataSpace memspace(2, &dimsm[0]);

    // Define memory hyperslab - 2D plane for one channel
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[dim_y] = std::min<hsize_t>(item_image.height(), dims[dim_y]);
    count_out[dim_x] = std::min<hsize_t>(item_image.width(), dims[dim_x]);
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file

    bool buffer_in_place = false;
    bim::Image buffer;
    if (item_image.depth() == dt.getSize() * 8) {
        buffer_in_place = true;
        buffer = item_image;
    } else {
        buffer.alloc(item_image.width(), item_image.height(), item_image.samples(), dt.getSize() * 8, dt.getClass() != H5T_FLOAT ? bim::DataFormat::FMT_UNSIGNED : bim::DataFormat::FMT_FLOAT);
    }
    buffer.fill(0);

    // read slabs sample by sample
    for (int sample = 0; sample < (int)item_image.samples(); ++sample) {
        viqi_init_coordinate(fmtHndl, 0, item_info[1], sample);
        viqi_set_offset_count(fmtHndl, item_image.imageInfo(), dims, offset, count);

        dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

        // Read data from hyperslab in the file into the hyperslab in memory
        dataset->read(buffer.sampleBits(sample), dt, memspace, dataspace);
    }

    if (!buffer_in_place) {
        item_image = buffer.convertToDepth(item_image.depth(), bim::Lut::ltTypecast, item_image.pixelType());
        item_mask = dt.getSize() == 1 ? buffer : buffer.convertToDepth(8, bim::Lut::ltTypecast, bim::DataFormat::FMT_UNSIGNED);
    } else {
        item_mask = dt.getSize() == 1 ? buffer.deepCopy() : buffer.convertToDepth(8, bim::Lut::ltTypecast, bim::DataFormat::FMT_UNSIGNED);
    }
    //item_mask.fill(255, 0);
    item_mask.binarize(128, 0, 255);

    if (par->values_from_measures && par->table.nrows > 0 && par->table.measures.size() > 0 && item_image.samples() == 1) {
        //buffer_multiply(bmp->bits[sample], (bim::int64)(info->width * info->height), 255, info->depth, info->pixelType);
        int object_id = item_info[0];
        int measure_id = par->positions[(bim::uint64) bim::ImageDims::DIM_MEASURE];
        double v = par->table.get_measure_value<double>(object_id, measure_id, block_id, 0);
        item_image.fill(v, 0);
    }

    return 0;
}

bim::uint viqi_read_item(FormatHandle *fmtHndl, bim::uint item_id, int level = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;

    xstring path_item;
    H5::DataSet dataset;

    // some levels may not have sparse info, check if requested level is dense and walk up the resolution levels until sparse level is found
    while (level >= 0) {
        path_item = par->path + xstring::xprintf("/level_%.3d/block_%.4d", level, par->block_id);
        try {
            dataset = par->file->openDataSet(path_item.c_str());
            xstring block_format = hdf5_get_attribute_string(dataset, "viqi_block_format");
            if (block_format == "sparse") break;
        } catch (...) {
        }
        --level;
    }

    setDataConfiguration(fmtHndl, dataset, info);

    std::vector<int> item_info = viqi_get_item_info(par, path_item, par->block_id, item_id);
    bim::Bbox<int> item_bbox(&item_info[2], par->num_spatial_dimensions);

    // allocate image
    ImageBitmap *bmp = fmtHndl->image;
    info->width = item_bbox.getWidth();
    info->height = item_bbox.getHeight();
    info->samples = par->image_num_samples;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;
    bim::Image item_image(bmp);
    //item_image.alloc(item_bbox.getWidth(), item_bbox.getHeight(), info->depth, par->image_num_samples, info->pixelType);
    bim::Image item_mask(item_bbox.getWidth(), item_bbox.getHeight(), 8, 1, bim::DataFormat::FMT_UNSIGNED);

    try {
        return viqi_fill_item(&dataset, fmtHndl, &item_info[0], item_image, item_mask, par->block_id);
    } catch (...) {
        return 1;
    }
}

bim::uint viqi_fill_sparse_block(H5::DataSet *dataset, FormatHandle *fmtHndl, bim::uint block_id, const xstring &block_path, bim::Image &block_image, bim::Image &mask_image,
    bim::uint x = 0, bim::uint y = 0, bim::uint w = 0, bim::uint h = 0) {

    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;

    bim::Bbox<int> block_bbox(hdf5_get_attribute_int_vector(*dataset, "viqi_block_bbox"));
    int index_sz = par->num_spatial_dimensions * 2 + 2;
    w = w > 0 ? w : block_bbox.getWidth();
    h = h > 0 ? h : block_bbox.getHeight();
    bim::Bbox<int> region_bbox((int)y, (int)x, (int)h, (int)w); //bim::Bbox<int> region_bbox((int)x, (int)y, (int)w, (int)h);

    // reading sparse block
    std::vector<int> block_index;
    xstring path_index = block_path + "_sparse_index";
    hdf5_read_vector_int(par, path_index.c_str(), block_index);
    for (size_t i = 0; i < block_index.size(); i += index_sz) {
        bim::Bbox<int> item_bbox(&block_index[i + 2], par->num_spatial_dimensions);
        if (!region_bbox.is_intersecting(item_bbox))
            continue;
        if (item_bbox.getWidth() == 0 || item_bbox.getHeight() == 0)
            continue;

        bim::Image item_image(item_bbox.getWidth(), item_bbox.getHeight(), block_image.depth(), block_image.samples(), block_image.pixelType());
        bim::Image item_mask(item_bbox.getWidth(), item_bbox.getHeight(), 8, 1, bim::DataFormat::FMT_UNSIGNED);

        // write into block image
        try {
            if (viqi_fill_item(dataset, fmtHndl, &block_index[i], item_image, item_mask, block_id) == 0) { // dima: properly offset N-D planes, need to find proper Z, T, etc offsets
                block_image.renderROI((double)item_bbox.getX() - x, (double)item_bbox.getY() - y, item_image, item_mask);
                mask_image.renderROI((double)item_bbox.getX() - x, (double)item_bbox.getY() - y, item_mask, item_mask);
            }
        } catch (H5::DataTypeIException error) {
            const char * errM = error.getCDetailMsg();
            std::cerr << errM << std::endl;
        } catch (H5::FileIException error) {
            const char * errM = error.getCDetailMsg();
            std::cerr << errM << std::endl;
        } catch (H5::AttributeIException error) {
            const char * errM = error.getCDetailMsg();
            std::cerr << errM << std::endl;
        } catch (...) {
            // do nothing
        }        
    }

    return 0;
}


bim::uint viqi_fill_dense_block(H5::DataSet *dataset, FormatHandle *fmtHndl, const xstring &block_path, bim::Image &block_image, bim::Image &mask_image,
                                bim::uint x = 0, bim::uint y = 0, bim::uint w = 0, bim::uint h = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    mask_image.fill(255);
    block_image.fill(0);

    H5::DataType dt = dataset->getDataType();

    ImageBitmap *bmp = block_image.imageBitmap();
    bim::ImageInfo *info = &bmp->i;

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = dataset->getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    int dim_x = !par->viqi_image_interleaved ? rank - 1 : 1;
    int dim_y = !par->viqi_image_interleaved ? rank - 2 : 0;

    // Define the memory dataspace - 2D plane for one channel
    /*std::vector<hsize_t> dimsm(2, 1);
    dimsm[0] = info->height;
    dimsm[1] = info->width;
    H5::DataSpace memspace(2, &dimsm[0]);*/

    std::vector<hsize_t> dimsm(3, 1);
    if (!par->viqi_image_interleaved) {
        dimsm[0] = 1;
        dimsm[1] = info->height;
        dimsm[2] = info->width;
    } else {
        dimsm[0] = info->height;
        dimsm[1] = info->width;
        dimsm[2] = 1;
    }
    H5::DataSpace memspace(3, &dimsm[0]);

    // Define memory hyperslab - 2D plane for one channel
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[dim_y] = std::min<hsize_t>(info->height, dims[dim_y]);
    count_out[dim_x] = std::min<hsize_t>(info->width, dims[dim_x]);
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file

    //if (par->values_from_measures && par->table.nrows > 0 && par->table.measures.size()>0 && item_image.samples() == 1) {

    // read slabs sample by sample
    for (int sample = 0; sample < (int)info->samples; ++sample) {
        viqi_init_coordinate(fmtHndl, x, y, sample, true);
        viqi_set_offset_count(fmtHndl, info, dims, offset, count, true); // need to add block_bbox

        dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

        // Read data from hyperslab in the file into the hyperslab in memory
        dataset->read(bmp->bits[sample], dt, memspace, dataspace);
    }

    return 0;
}

bim::uint viqi_fill_external_block(H5::DataSet *dataset, FormatHandle *fmtHndl, const xstring &block_path, bim::Image &block_image, bim::Image &mask_image,
                                   bim::uint x = 0, bim::uint y = 0, bim::uint w = 0, bim::uint h = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    mask_image.fill(255);

    H5::DataType dt = dataset->getDataType();
    if (dt.getClass() != H5T_STRING) return 0;
    size_t sz = dt.getSize();

    // Get the number of strings
    H5::DataSpace dataspace = dataset->getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    if (rank > 1) return 0;
    std::vector<hsize_t> dims(rank);
    dataspace.getSimpleExtentDims(&dims[0]);
    bim::uint N = static_cast<bim::uint>(dims[0]);

    std::vector<char> rdata(dims[0] * sz);
    dataset->read(&rdata[0], dt);

    std::vector<std::string> paths(dims[0]);
    for (size_t i = 0; i < dims[0]; ++i) {
        xstring s(sz, 0);
        memcpy((void *)&s[0], (void *)&rdata[i * sz], sz);
        paths[i] = bim::fspath_fix_slashes(bim::fspath_replace(par->file_name, s));
    }

    Image img(paths[0]); // , 0, 0, x, y, x + w, y + h);
    for (bim::uint i = 1; i < N; ++i) {
        Image img2(paths[i]); //, 0, 0, x, y, x + w, y + h);
        img = img.appendChannels(img2);
    }

    // rotate and crop
    std::vector<int> transform = hdf5_get_attribute_int_vector(*dataset, "viqi_block_transform");
    if (transform.size() >= 6 && transform[0] == 0 && transform[1] == -1 && transform[2] == 0 && transform[3] == 1 && transform[4] == 0 && transform[5] == 0) // Clockwise: [[0, -1, 0], [1, 0, 0]]
        img = img.rotate(90);
    else if (transform.size() >= 6 && transform[0] == 0 && transform[1] == 1 && transform[2] == 0 && transform[3] == -1 && transform[4] == 0 && transform[5] == 0) //Counter clockwise : [[0, 1, 0], [-1, 0, 0]]
        img = img.rotate(-90);

    img = img.ROI(x, y, w, h);
    block_image.copy(img);

    return 0;
}

//------------------------------------------------------------------------------------------------------------------------
// Heatmap
// heatmap is defined as a regular grid, starting at 0 of the block and ending at the end of the block
// grid points may be missing though although they will affect higher order interpolation
// tiles may have a different step than the tile size creating either overlapping or non touching tiles
// those oncfigurations will require slower processing with more RAM and tile generations with steps equivalent
// to tile sizes are strongly recommended,
// proper finish for the overlapping and non-continuous tiles will require a two-step interpolation where
// a uniform grid portion is interpolated independently from its sides that will have a different tile size
//------------------------------------------------------------------------------------------------------------------------

template<typename T>
T compute_step(const std::set<T> &pos, const T &def ) {
    if (pos.size() <= 1) return def;
    //std::vector<T> steps(pos.size()-1, 0);
    std::set<int>::const_iterator itx = pos.begin();
    T p = *(itx);
    std::map<int, int> histogram;

    // blocks may missing and so the minimum distance between blocks will define the step
    while (++itx != pos.end()) {
        double dist = abs(p - *(itx));
        if (dist > 0) {
            ++histogram[(int) dist];
        }
        p = *(itx);
    }

    if (histogram.size() == 0) {
        return def;
    }

    // find the most occuring distance
    //std::map<int, int>::const_iterator ite = std::max_element(histogram.begin(), histogram.end(), histogram.value_comp());

    // find the shortest distance
    std::map<int, int>::const_iterator ite = histogram.begin();

    if (ite == histogram.end()) return def;
    return ite->first;
}

// estimate heatmap info like grid spacing, grid size, positions of first elements in 100% image space
void compute_heatmap_info(bim::HDF5Params *par, bim::uint block_id, int level0,
    int &grid_width, int &grid_height, // number of elements in the grid
    int &step_x, int &step_y, // grid spacing at 100% image scale
    int &tile_w, int &tile_h) { // size of the heatmap tile as defined

    xstring block_path = par->path + xstring::xprintf("/level_%.3d/block_%.4d", level0, block_id);

    bim::Bbox<int> block_bbox;
    H5::DataSet dataset;
    try {
        dataset = par->file->openDataSet(block_path.c_str());
        block_bbox.set(hdf5_get_attribute_int_vector(dataset, "viqi_block_bbox"));
    } catch (H5::FileIException error) {
        //return;
    }

    // read the block data
    std::vector<int> block_index;
    xstring path_index = block_path + "_sparse_index";
    hdf5_read_vector_int(par, path_index.c_str(), block_index);

    int index_sz = par->num_spatial_dimensions * 2 + 2;
    int samples_num = static_cast<int>(block_index.size() / index_sz);

    // dima: we currently only support gridded heatmaps
    // find the heatmap grid extent
    bim::Bbox<int> bbox1(&block_index[0 * index_sz + 2], par->num_spatial_dimensions);
    tile_w = bbox1.getWidth();
    tile_h = bbox1.getHeight();

    std::set<int> samples_x;
    std::set<int> samples_y;
    for (size_t i = 0; i < block_index.size(); i += index_sz) {
        bim::Bbox<int> item_bbox(&block_index[i + 2], par->num_spatial_dimensions);
        // we can be resilient to missing samples in the grid but all coordinates must be exactly the same!!!!!
        samples_x.insert(item_bbox.getX());
        samples_y.insert(item_bbox.getY());
    }

    // compute steps
    //int first_x = *(samples_x.begin());
    //int first_y = *(samples_y.begin());
    int last_x = *(samples_x.rbegin());
    int last_y = *(samples_y.rbegin());
    step_x = compute_step<int>(samples_x, tile_w);
    step_y = compute_step<int>(samples_y, tile_h);

    //grid_width  = (int) floor((last_x - first_x) / (float)step_x) + 1;
    //grid_height = (int) floor((last_y - first_y) / (float)step_y) + 1;
    //last_x += tile_w;
    //last_y += tile_h;
    int W = std::max<int>(block_bbox.getWidth(), last_x + tile_w);
    int H = std::max<int>(block_bbox.getHeight(), last_y + tile_h);

    grid_width = (int) floor((W - tile_w) / (float)step_x) + 1;
    grid_height = (int) floor((H - tile_h) / (float)step_y) + 1;
    //first_x = 0;
    //first_y = 0;
    //last_x = block_bbox.getX2();
    //last_y = block_bbox.getY2();
}

// fill a grid at the heatmap scale with measure values
void fill_heatmap_values(bim::HDF5Params *par, bim::uint block_id, int level0, int measure_id,
    bim::Image &img, bim::Image &msk,
    const int &offset_x, const int &offset_y, // in grid step coordinates // the 100% image coordinates
    const int &step_x, const int &step_y) { // grid steps in 100% image coordinates

    xstring block_path = par->path + xstring::xprintf("/level_%.3d/block_%.4d", level0, block_id);

    // read the block data
    xstring path_index = block_path + "_sparse_index";
    std::vector<int> block_index;
    hdf5_read_vector_int(par, path_index.c_str(), block_index);

    int index_sz = par->num_spatial_dimensions * 2 + 2;
    int samples_num = (int) block_index.size() / index_sz;

    int NW = (int) img.width();
    int NH = (int) img.height();

    // fill in the grid with values
    for (size_t i = 0; i < block_index.size(); i += index_sz) {
        bim::Bbox<int> item_bbox(&block_index[i + 2], par->num_spatial_dimensions);
        int object_id = block_index[i + 0];

        double xx = bim::round<float>(item_bbox.getX() / (float)step_x) + offset_x;
        double yy = bim::round<float>(item_bbox.getY() / (float)step_y) + offset_y;
        double v = par->table.get_measure_value<double>(object_id, measure_id, block_id, 0);
        if (v != 0) {
            img.setPixelValue(0, xx, yy, v);
            msk.setPixelValue(0, xx, yy, 255.0);
        }
    }
}

bim::uint viqi_fill_heatmap_block(H5::DataSet *dataset, FormatHandle *fmtHndl, bim::uint block_id, bim::Image &block_image, bim::Image &mask_image,
                                  bim::uint x = 0, bim::uint y = 0, bim::uint w = 0, bim::uint h = 0, int level = 0) {

    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;

    // get current scale
    double scale = par->scales[level];
    viqi_init_coordinate(fmtHndl, 0, 0, 0);
    int measure_id = par->positions[(bim::uint64) bim::ImageDims::DIM_MEASURE];

    // use scale 1.0 level to read the heatmap data
    size_t level0 = 0;
    for (size_t l = 0; l < par->scales.size(); ++l) {
        if (par->scales[l] == 1.0) {
            level0 = l;
            break;
        }
    }

    // detect needed interpolation
    if (!par->user_given_interpolation) {
        VQITableMeasure *m = par->table.getMeasure(measure_id);
        par->interpolation = m->interpolation;
    }
    //par->interpolation = bim::Image::ResizeMethod::szBiCubic;

    // fetch grid in the original scale
    int spacing = par->interpolation == bim::Image::ResizeMethod::szNearestNeighbor ? 0 : VIQI_HEATMAP_INTERPOLATION_SPACING; // create padding with neightboring blocks if using higher order interpolation
    int step_x = 0, step_y = 0, tile_w = 0, tile_h = 0;
    int NW = 0, NH = 0;
    compute_heatmap_info(par, block_id, (int) level0, NW, NH, step_x, step_y, tile_w, tile_h);
    NW += spacing * 2;
    NH += spacing * 2;
    int offset_x = spacing;
    int offset_y = spacing;

    // create original measure grid
    bim::Image img(NW, NH, block_image.depth(), block_image.samples(), block_image.pixelType());
    bim::Image msk(NW, NH, 8, 1, bim::DataFormat::FMT_UNSIGNED);
    img.fill(0);
    msk.fill(0);
    fill_heatmap_values(par, block_id, (int)level0, measure_id, img, msk, offset_x, offset_y, step_x, step_y);

    // find block extensions with neighboring blocks if needed for smooth interpolation
    if (spacing > 0) {
        xstring path_level = par->path + xstring::xprintf("/level_%.3d", level0);

        try {
            H5::Group group = par->file->openGroup(path_level.c_str());
            std::vector<int> bboxs = hdf5_get_attribute_int_vector(group, "viqi_storage_block_bboxes");

            int bboxsz = par->num_spatial_dimensions * 2;
            bim::Bbox<int> exact_bbox(&bboxs[block_id*bboxsz], par->num_spatial_dimensions);

            bim::Bbox<int> intersect_bbox(&bboxs[block_id*bboxsz], par->num_spatial_dimensions);
            intersect_bbox.offset_by(-2); // look for immediately neighboring blocks
            intersect_bbox.resize_by(4);

            for (size_t i = 0, idx = 0; i < bboxs.size(); i += bboxsz, ++idx) {
                bim::Bbox<int> block_bbox(&bboxs[i], par->num_spatial_dimensions);
                if (idx == block_id || !intersect_bbox.is_intersecting(block_bbox)) {
                    continue;
                }

                int BW = 0, BH = 0, bst_x = 0, bst_y = 0, bt_w = 0, bt_h = 0;
                compute_heatmap_info(par, (bim::uint)idx, (int)level0, BW, BH, bst_x, bst_y, bt_w, bt_h);
                if (bst_x != step_x || bst_y != step_y) continue;
                if (bt_w != tile_w || bt_h != tile_h) continue;

                // fetch block values into the current image directly
                int boff_x = 0;
                int boff_y = 0;
                if (exact_bbox.getX2() <= block_bbox.getX()) {
                    boff_x = static_cast<int>(NW - spacing + ((block_bbox.getX() - exact_bbox.getX2()) / (float)step_x));
                } else if (block_bbox.getX() < exact_bbox.getX()) {
                    boff_x = static_cast<int>(spacing - (floor(((exact_bbox.getX() - block_bbox.getX()) - tile_w) / (float)step_x) + 1));
                } else {
                    boff_x = static_cast<int>(spacing + floor( (block_bbox.getX() - exact_bbox.getX()) / (float)step_x));
                }

                if (exact_bbox.getY2() <= block_bbox.getY()) {
                    boff_y = static_cast<int>(NH - spacing + ((block_bbox.getY() - exact_bbox.getY2()) / (float)step_y));
                } else if (block_bbox.getY() < exact_bbox.getY()) {
                    boff_y = static_cast<int>(spacing - (floor(((exact_bbox.getY() - block_bbox.getY()) - tile_h) / (float)step_y) + 1));
                } else {
                    boff_y = static_cast<int>(spacing + floor((block_bbox.getY() - exact_bbox.getY()) / (float)step_y));
                }

                fill_heatmap_values(par, (bim::uint)idx, (int)level0, measure_id, img, msk, boff_x, boff_y, step_x, step_y);
            }
        } catch (H5::FileIException error) {
            // skip filling respective blocks in
        }
    }

    // compute grid size in current pixel size, bboxes may be sized 0
    // it is important to note that since the step may be smaller or larger than the tile size the last tile may end up being smaller than the step
    // and it will be cut during the render ROI step
    int heatmap_w = (NW - 1) * step_x + tile_w;
    int heatmap_h = (NH - 1) * step_y + tile_h;

    // -------------------------------------------------------------------------
    // crop the grid prior to interpolation if the requested window is smaller than block-spacing
    // skip if the step is different from tile size, it is too difficult to compute the needed cropping
    // -------------------------------------------------------------------------

    if (step_x == tile_w && step_y == tile_h) {
        int xx = 0, yy = 0, ww = 0, hh = 0;
        int xf = bim::round<int>(x / scale);
        int yf = bim::round<int>(y / scale);
        int xf2 = bim::round<int>((x + w) / scale);
        int yf2 = bim::round<int>((y + h) / scale);
        if (xf >= step_x*(spacing + 1)) {
            xx = (int)floor(xf / (float)step_x) - spacing;
            x -= bim::round<bim::uint>(xx * step_x * scale);
            heatmap_w -= xx * step_x;
        }
        if (yf >= step_y*(spacing + 1)) {
            yy = (int)floor(yf / (float)step_y) - spacing;
            y -= bim::round<bim::uint>(yy * step_y * scale);
            heatmap_h -= yy * step_y;
        }
        if (xf2 < heatmap_w - ((spacing+1)*step_x + tile_w)) {
            ww = (int)floor((heatmap_w - xf2) / step_x) - (spacing + 1);
            heatmap_w -= (ww)* step_x;
        }
        if (yf2 < heatmap_h - ((spacing+1)*step_y + tile_h)) {
            hh = (int)floor((heatmap_h - yf2) / step_y) - (spacing + 1);
            heatmap_h -= (hh)* step_y;
        }

        if (xx > 0 || yy > 0 || ww > 0 || hh > 0) {
            int www = static_cast<int>(img.width() - xx - ww);
            int hhh = static_cast<int>(img.height() - yy - hh);
            img = img.ROI(xx, yy, www, hhh);
            msk = msk.ROI(xx, yy, www, hhh);
        }
    }

    // -------------------------------------------------------------------------
    // render into the block at the proper scale
    // -------------------------------------------------------------------------

    // rescale everything from scale 1.0 to current scale
    heatmap_w = bim::round<int>(heatmap_w * scale);
    heatmap_h = bim::round<int>(heatmap_h * scale);

    //first_x = bim::round<float>(first_x * scale);
    //first_y = bim::round<float>(first_y * scale);
    //double rx = first_x - (double)x - bim::round<float>(spacing * step_x * scale);
    //double ry = first_y - (double)y - bim::round<float>(spacing * step_y * scale);

    double rx = 0 - (double)x - bim::round<float>(spacing * step_x * scale);
    double ry = 0 - (double)y - bim::round<float>(spacing * step_y * scale);

    // optimization - skip resample
    if (rx >= block_image.width()) return 0;
    if (ry >= block_image.height()) return 0;

    img = img.resample(heatmap_w, heatmap_h, par->interpolation);
    msk = msk.resample(heatmap_w, heatmap_h, par->interpolation);

    block_image.renderROI(rx, ry, img, msk);
    mask_image.renderROI(rx, ry, msk, msk);

    return 0;
}

bim::uint viqi_fill_block(bim::Image &block_image, bim::Image &mask_image, FormatHandle *fmtHndl, bim::uint block_id,
                          bim::uint x = 0, bim::uint y = 0, bim::uint w = 0, bim::uint h = 0, int level = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    block_image.fill(0);
    if (mask_image.depth() != 8)
        mask_image = mask_image.normalize();
    mask_image.fill(0);

    xstring path_block = par->path + xstring::xprintf("/level_%.3d/block_%.4d", level, block_id);
    // sparse block may be absent and means an empty image
    H5::DataSet dataset;
    try {
        dataset = par->file->openDataSet(path_block.c_str());
    } catch (H5::FileIException error) {
        return 0;
    }

    //bool sparse_mode = hdf5_get_attribute_string(dataset, "viqi_block_format") == "sparse";
    xstring block_format = hdf5_get_attribute_string(dataset, "viqi_block_format");
    xstring external_mode = hdf5_get_attribute_string(dataset, "viqi_block_type");

    if (external_mode == "libbioimage") {
        return viqi_fill_external_block(&dataset, fmtHndl, path_block, block_image, mask_image, x, y, w, h);
    } else if (block_format == "heatmap") { //|| par->viqi_image_content == "heatmap") { // dima: temporary test
        return viqi_fill_heatmap_block(&dataset, fmtHndl, block_id, block_image, mask_image, x, y, w, h, level);
    } else if (block_format == "sparse") {
        return viqi_fill_sparse_block(&dataset, fmtHndl, block_id, path_block, block_image, mask_image, x, y, w, h);
    } else {
        return viqi_fill_dense_block(&dataset, fmtHndl, path_block, block_image, mask_image, x, y, w, h); // dima: properly offset N-D planes
    }

    // unknown block type
    return 1;
}

bim::uint viqi_read_block(FormatHandle *fmtHndl, bim::uint block_id, bim::uint x, bim::uint y, bim::uint w, bim::uint h, int level = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;

    uint bboxsz = par->num_spatial_dimensions * 2;

    // sparse block may be absent and means an empty image
    xstring path_block = par->path + xstring::xprintf("/level_%.3d/block_%.4d", level, block_id);
    H5::DataSet dataset;
    try {
        dataset = par->file->openDataSet(path_block.c_str());
    } catch (H5::FileIException error) {
        bim::Bbox<int> block_bbox(par->num_spatial_dimensions);

        // read bbox from level index if available, otherwise return 1x1 image
        if (par->block_bboxs.size() == 0 || par->loaded_level != level) {
            par->loaded_level = level;
            xstring path_level = par->path + xstring::xprintf("/level_%.3d", level);

            try {
                H5::Group group = par->file->openGroup(path_level.c_str());
                par->num_blocks = hdf5_get_attribute_int(group, "viqi_storage_number_blocks");
                if (par->num_blocks < 1) {
                    par->num_blocks = (int)hdf5_get_attribute_double(group, "viqi_storage_number_blocks");
                }
                par->num_items = hdf5_get_attribute_int(group, "viqi_storage_sparse_items");
                par->block_bboxs = hdf5_get_attribute_int_vector(group, "viqi_storage_block_bboxes");
            } catch (H5::FileIException error) {
                return 1;
            }
        }

        uint offset = block_id * bboxsz;
        if (par->block_bboxs.size() >= offset + bboxsz) {
            block_bbox.set(&par->block_bboxs[offset], par->num_spatial_dimensions);
        }

        info->width = block_bbox.getWidth();
        info->height = block_bbox.getHeight();
        info->samples = par->image_num_samples;
        ImageBitmap *bmp = fmtHndl->image;
        if (allocImg(fmtHndl, info, bmp) != 0) return 1;
        bim::Image block_image(bmp);
        block_image.fill(0);
        return 0;
    }

    // ok, block was found
    par->values_from_measures = hdf5_get_attribute_string(dataset, "viqi_block_format") == "dense" ? false : par->values_from_measures;
    setDataConfiguration(fmtHndl, dataset, info);

    bim::Bbox<int> block_bbox(hdf5_get_attribute_int_vector(dataset, "viqi_block_bbox"));
    info->width = block_bbox.getWidth();
    info->height = block_bbox.getHeight();

    // allocate output image
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;
    bim::Image block_image(bmp);
    bim::Image mask_image(block_image.width(), block_image.height(), 8, 1, bim::DataFormat::FMT_UNSIGNED);
    //bim::Image mask_image; // set empty mask

    return viqi_fill_block(block_image, mask_image, fmtHndl, block_id, x, y, w, h, level);
}

bim::uint viqi_read_region(FormatHandle *fmtHndl, bim::int64 x, bim::int64 y, bim::int64 w, bim::int64 h, int level = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;

    XConf *conf = fmtHndl->arguments;
    if (conf && conf->keyExists("-slice-spectrum")) {
        //par->position_spectrum = conf->getValueInt("-slice-spectrum");
        par->position_spectrum = conf->getDimensionSlices("-slice-spectrum");
    }

    // constrain to image dimensions
    float scale = (float)(1.0 / pow(2.0, (float)level));
    int plane_width = bim::round<int>(par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X] * scale);
    int plane_height = bim::round<int>(par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y] * scale);
    w = w > 0 ? w : plane_width;
    h = h > 0 ? h : plane_height;
    w = bim::min<bim::uint64>(w, plane_width - x);
    h = bim::min<bim::uint64>(h, plane_height - y);

    if (par->block_bboxs.size() == 0 || par->loaded_level != level) {
        par->loaded_level = level;
        xstring path_level = par->path + xstring::xprintf("/level_%.3d", level);

        try {
            H5::Group group = par->file->openGroup(path_level.c_str());
            par->num_blocks = hdf5_get_attribute_int(group, "viqi_storage_number_blocks");
            if (par->num_blocks < 1) {
                par->num_blocks = (int)hdf5_get_attribute_double(group, "viqi_storage_number_blocks");
            }
            par->num_items = hdf5_get_attribute_int(group, "viqi_storage_sparse_items");
            par->block_bboxs = hdf5_get_attribute_int_vector(group, "viqi_storage_block_bboxes");
        } catch (H5::FileIException error) {
            return 1;
        }
    }

    // find the first existing block and define the data configuration for the regional image
    bool first_block_found = false;
    for (int i = 0; i < par->num_blocks; ++i) {
        try {
            xstring path_block = par->path + xstring::xprintf("/level_%.3d/block_%.4d", level, i);
            H5::DataSet dataset = par->file->openDataSet(path_block.c_str());
            par->values_from_measures = hdf5_get_attribute_string(dataset, "viqi_block_format") == "dense" ? false : par->values_from_measures;
            setDataConfiguration(fmtHndl, dataset, info);
            first_block_found = true;
            break;
        } catch (H5::FileIException error) {
            // do nothing, the block node may not exist if empty
        }
    }
    if (!first_block_found) return 1;

    // allocate output image
    info->width = w;
    info->height = h;

    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;
    bim::Image region_image(bmp);
    region_image.fill(0);

    int bboxsz = par->num_spatial_dimensions * 2;
    int block_id = 0;
    bim::Bbox<int> region_bbox((int)y, (int)x, (int)h, (int)w);
    for (size_t i = 0, idx = 0; i < par->block_bboxs.size(); i += bboxsz, ++idx) {
        bim::Bbox<int> block_bbox(&par->block_bboxs[i], par->num_spatial_dimensions);
        if (!region_bbox.is_intersecting(block_bbox))
            continue;

        // compute in-block offsets and counts
        double bx = std::max<double>((double)block_bbox.getX() - x, 0);
        double by = std::max<double>((double)block_bbox.getY() - y, 0);
        double sx = fabs(std::min<double>((double)block_bbox.getX() - x, 0));
        double sy = fabs(std::min<double>((double)block_bbox.getY() - y, 0));
        double sw = std::min<double>(block_bbox.getWidth() - sx, w - bx);
        double sh = std::min<double>(block_bbox.getHeight() - sy, h - by);
        if (sw < 1 || sh < 1) continue;

        bim::Image block_image((bim::uint64)sw, (bim::uint64)sh, region_image.depth(), region_image.samples(), region_image.pixelType());
        bim::Image mask_image((bim::uint64)sw, (bim::uint64)sh, 8, 1, bim::DataFormat::FMT_UNSIGNED);
        viqi_fill_block(block_image, mask_image, fmtHndl, (bim::uint)idx, (bim::uint)sx, (bim::uint)sy, (bim::uint)sw, (bim::uint)sh, level);

        region_image.renderROI(bx, by, block_image, mask_image);
        ++block_id;
    }

    return 0;
}

bim::uint viqi_read_pixels(FormatHandle *fmtHndl, bim::uint page, int x = 0, int y = 0, int w = 0, int h = 0, int l = 0) {
    if (fmtHndl == NULL) return 1;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    XConf *conf = fmtHndl->arguments;

    if (conf && conf->keyExists("-slice-spectrum")) {
        par->position_spectrum = conf->getDimensionSlices("-slice-spectrum");
    }

    // reading blocks
    if (conf && conf->keyExists("-slice-fov")) {
        par->block_id = conf->getValueInt("-slice-fov");
        return viqi_read_block(fmtHndl, par->block_id, x, y, w, h, l);
    }

    if (x == 0 && y == 0 && w == 0 && h == 0 && l == 0 && par->block_id < 0 && par->item_id < 0 && par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X] < 1 && par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y] < 1) {
        par->block_id = 0;
        par->item_id = 0;
    }

    // reading only one item
    if (par->block_id >= 0 && par->item_id >= 0) {
        return viqi_read_item(fmtHndl, par->item_id, l);
    }

    // read tiles and levels combining blocks
    return viqi_read_region(fmtHndl, x, y, w, h, l);
}

//****************************************************************************
// sub-formats: Dream 3D
//****************************************************************************

bim::uint dream3d_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    //bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    // parse all metadata
    if (conf && conf->hasKeyWith("-meta")) {
        try {
            std::vector<double> spacing;
            hdf5_read_vector(par, "/DataContainers/ImageDataContainer/_SIMPL_GEOMETRY/SPACING", spacing);
            hash->set_value(bim::PIXEL_RESOLUTION_X.c_str(), spacing[1]);
            hash->set_value(bim::PIXEL_RESOLUTION_Y.c_str(), spacing[0]);
            hash->set_value(bim::PIXEL_RESOLUTION_Z.c_str(), spacing[2]);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X.c_str(), bim::PIXEL_RESOLUTION_UNIT_MICRONS);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y.c_str(), bim::PIXEL_RESOLUTION_UNIT_MICRONS);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Z.c_str(), bim::PIXEL_RESOLUTION_UNIT_MICRONS);
            hash->set_value("Dream3D/spacing", spacing);
        } catch (...) {
            // do nothing
        }

        try {
            std::vector<double> origin;
            hdf5_read_vector(par, "/DataContainers/ImageDataContainer/_SIMPL_GEOMETRY/ORIGIN", origin);
            hash->set_value("Dream3D/origin", origin);
        } catch (...) {
            // do nothing
        }

        try {
            std::vector<double> dimensions;
            hdf5_read_vector(par, "/DataContainers/ImageDataContainer/_SIMPL_GEOMETRY/DIMENSIONS", dimensions);
            hash->set_value("Dream3D/dimensions", dimensions);
        } catch (...) {
            // do nothing
        }
    }

    return 0;
}

//****************************************************************************
// Base HDF5
//****************************************************************************

void hdf5GetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    //*info = initImageInfo();
    XConf *conf = fmtHndl->arguments;

    if (conf)
        par->path_requested = conf->getValue("-path");

    // determine file sub-type
    fmtHndl->subFormat = FMT_HDF5;
    if (par->file->attrExists("ImarisVersion")) {
        fmtHndl->subFormat = FMT_IMS;
        imsGetImageInfo(fmtHndl);
        return;
    } else if (par->file->attrExists("DREAM3D Version")) {
        fmtHndl->subFormat = FMT_DREAM3D;
        //d3dGetImageInfo(fmtHndl);
    } else if (par->file->attrExists("viqi_storage_type")) {
        bool valid = viqiGetImageInfo(fmtHndl);
        if (valid) {
            fmtHndl->subFormat = FMT_VIQI;
            return; // otherwise read as standrd HDF-5
        }
    }

    // detect first element to list if path is empty
    if (conf) {
        par->path = conf->getValue("-path");
    }
    if (par->path.size() < 1) {
        std::vector<xstring> objects;
        H5::Group root = par->file->openGroup("/");
        hdf5_walker(par, root, "", objects, 1);
        par->path = objects[0];
    }

    H5::DataSet dataset = par->file->openDataSet(par->path.c_str());
    setDataConfiguration(fmtHndl, dataset, info);

    info->tileWidth = par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] > 0 ? par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] : 128;
    info->tileHeight = par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] > 0 ? par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] : 128;

    info->resUnits = bim::ResolutionUnits::RES_IN;
    info->xRes = 0;
    info->yRes = 0;
}

void hdf5CloseImageProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    if (par == NULL) return;
    //par->close();
    xclose(fmtHndl);
    fmtHndl->internalParams = 0;
    delete par;
}

bim::uint hdf5OpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams != NULL) hdf5CloseImageProc(fmtHndl);
    bim::HDF5Params *par = new bim::HDF5Params();
    fmtHndl->internalParams = (void *)par;

    if (io_mode == bim::ImageIOModes::IO_WRITE) {
        return 1;
    }

    try {
        par->open(fmtHndl->fileName, io_mode);
        hdf5GetImageInfo(fmtHndl);
    } catch (...) {
        hdf5CloseImageProc(fmtHndl);
        return 1;
    }

    return 0;
}

bim::uint hdf5GetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    return (bim::uint)info->number_pages;
}

bim::ImageInfo hdf5GetImageInfoProc(FormatHandle *fmtHndl, bim::uint page_num) {
    if (fmtHndl == NULL) return ImageInfo();
    fmtHndl->pageNumber = page_num;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    XConf *conf = fmtHndl->arguments;
    if (conf && par->path_requested != conf->getValue("-path")) {
        hdf5GetImageInfo(fmtHndl);
    }
    return par->i;
}


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

bim::uint hdf5_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (!hash) return 1;
    if (isCustomReading(fmtHndl)) return 1;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    // report original tile sizes
    if (par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X] > 0 && par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y] > 0) {
        hash->set_value("tile_num_original_x", par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_X]);
        hash->set_value("tile_num_original_y", par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Y]);
        if (par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Z]) {
            hash->set_value("tile_num_original_z", par->dimensions_tile[(bim::uint64) bim::ImageDims::DIM_Z]);
        }
    }

    /*
    // append image pyramid meta
    if (o->scales.size() > 1) {
        std::vector<double> scales(o->scales.begin(), o->scales.end());
        std::sort(scales.begin(), scales.end(), std::greater<double>());
        hash->set_value(bim::IMAGE_NUM_RES_L_ACTUAL, (int)scales.size());
        hash->set_value(bim::IMAGE_RES_L_SCALES_ACTUAL, xstring::join(scales, ","));

        // dima: virtual structure
        hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_HIERARCHICAL);
        hash->set_value(bim::IMAGE_NUM_RES_L, (int)o->scales_virtual.size());
        hash->set_value(bim::IMAGE_RES_L_SCALES, xstring::join(o->scales_virtual, ","));
    }


    hash->set_value(bim::IMAGE_NUM_SERIES, bim::max<int>(1, o->scenes.size()));
    */

    if (fmtHndl->subFormat == FMT_IMS) {
        return ims_append_metadata(fmtHndl, hash);
    } else if (fmtHndl->subFormat == FMT_VIQI) {
        return viqi_append_metadata(fmtHndl, hash);
    } else if (fmtHndl->subFormat == FMT_DREAM3D) {
        dream3d_append_metadata(fmtHndl, hash);
    }

    if (par->path.size() > 0) {
        hash->set_value(bim::IMAGE_CURRENT_PATH, par->path);
    }

    // parse the defined path and add attribute of each parent
    if (conf && conf->hasKeyWith("-meta")) {
        xstring p = xstring(" ") + par->path;
        std::vector<xstring> paths = p.split("/");
        paths[0] = "";
        xstring h5path = "";
        bim::xstring meta_path = "HDF5";

        for (size_t i = 0; i < paths.size(); ++i) {
            if (paths[i].size() > 0) {
                h5path += "/";
                h5path += paths[i];
                meta_path += "/";
                meta_path += paths[i];
            }
            hdf5_append_attributes(par, hash, h5path, meta_path);
        }
    }

    // parse at most 10 paths to compatible matrices
    if (conf && conf->hasKeyWith("-meta")) {
        std::vector<xstring> objects;
        xstring h5path = "";
        H5::Group root = par->file->openGroup("/");
        hdf5_walker(par, root, h5path, objects, 10); // dima: max of 10 paths
        // append objects into meta
        for (size_t i = 0; i < objects.size(); ++i) {
            hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), i), objects[i]);
        }
    }

    return 0;
}

//----------------------------------------------------------------------------
// READ
//----------------------------------------------------------------------------

bim::uint hdf5_read_pixels(FormatHandle *fmtHndl, bim::uint page, bim::uint x = 0, bim::uint y = 0, bim::uint w = 0, bim::uint h = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    //XConf *conf = fmtHndl->arguments;

    H5::DataSet dataset = par->file->openDataSet(par->path.c_str());
    H5::DataType dt = dataset.getDataType();
    int z, t;
    hdf5_init_coordinate(fmtHndl, page, z, t);
    if (w > 0 && h > 0) {
        info->width = w;
        info->height = h;
    }

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = dataset.getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    // allocate image
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    // Define the memory dataspace - 2D plane for one channel
    std::vector<hsize_t> dimsm(2, 1);
    dimsm[0] = info->height;
    dimsm[1] = info->width;
    H5::DataSpace memspace(2, &dimsm[0]);

    // Define memory hyperslab - 2D plane for one channel
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = info->height;
    count_out[1] = info->width;
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file

    // read slabs sample by sample
    for (int sample = 0; sample < (int)info->samples; ++sample) {

        // Define hyperslab in the dataset; implicitly passing stride and block as NULL
        hdf5_set_offset_count(fmtHndl, info, dims, rank, sample, z, t, offset, count, x, y);
        dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

        // Read data from hyperslab in the file into the hyperslab in memory
        dataset.read(bmp->bits[sample], dt, memspace, dataspace);
    }

    return 0;
}

bim::uint hdf5ReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    fmtHndl->pageNumber = page;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;

    try {
        if (fmtHndl->subFormat == FMT_IMS) {
            return ims_read_pixels(fmtHndl, page, 0, 0, 0, 0, 0);
        } else if (fmtHndl->subFormat == FMT_VIQI) {
            return viqi_read_pixels(fmtHndl, page, 0, 0, par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X], par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y], 0);
        }
        return hdf5_read_pixels(fmtHndl, page);
    } catch (...) {
        // do nothing
    }
    return 1;
}

bim::uint hdf5ReadImageLevelProc(FormatHandle *fmtHndl, bim::uint page, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    fmtHndl->pageNumber = page;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;

    try {
        if (fmtHndl->subFormat == FMT_IMS) {
            return ims_read_pixels(fmtHndl, page, 0, 0, 0, 0, level);
        } else if (fmtHndl->subFormat == FMT_VIQI) {
            float scale = (float)(1.0 / pow(2.0, (float)level));
            int plane_width = (int)floor(par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_X] * scale);
            int plane_height = (int)floor(par->dimensions_image[(bim::uint64) bim::ImageDims::DIM_Y] * scale);
            return viqi_read_pixels(fmtHndl, page, 0, 0, plane_width, plane_height, level);
        } else if (fmtHndl->subFormat != FMT_IMS && level == 0) {
            return hdf5_read_pixels(fmtHndl, page);
        }
    } catch (...) {
        // do nothing
    }
    return 1;
}

bim::uint hdf5ReadImageTileProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    fmtHndl->pageNumber = page;
    ImageInfo *info = &par->i;

    float scale = (float)(1.0 / pow(2.0, (float)level));
    const int plane_width = (int)std::floor(par->dimensions_image[(bim::uint64)bim::ImageDims::DIM_X] * scale);
    const int plane_height = (int)std::floor(par->dimensions_image[(bim::uint64)bim::ImageDims::DIM_Y] * scale);
    const int x = static_cast<int>(xid * info->tileWidth);
    const int y = static_cast<int>(yid * info->tileHeight);
    const int w = bim::min<int>((int)info->tileWidth, plane_width - x);
    const int h = bim::min<int>((int)info->tileHeight, plane_height - y);

    try {
        if (fmtHndl->subFormat == FMT_IMS) {
            return ims_read_pixels(fmtHndl, page, x, y, w, h, level);
        } else if (fmtHndl->subFormat == FMT_VIQI) {
            return viqi_read_pixels(fmtHndl, page, x, y, w, h, level);
        } else if (fmtHndl->subFormat != FMT_IMS && level == 0) {
            return hdf5_read_pixels(fmtHndl, page, x, y, w, h);
        }
    } catch (...) {
        // do nothing
    }
    return 1;
}

bim::uint hdf5ReadImageRegionProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 x1, bim::uint64 y1, bim::uint64 x2, bim::uint64 y2, bim::uint level) {
    fmtHndl->pageNumber = page;
    try {
        if (fmtHndl->subFormat == FMT_VIQI) {
            return viqi_read_region(fmtHndl, x1, y1, x2 - x1 + 1, y2 - y1 + 1, level);
        }
    } catch (...) {
        // do nothing
    }
    return 1;
}

//****************************************************************************
// exported
//****************************************************************************

#define BIM_HDF5_NUM_FORMATS 4

FormatItem hdf5Items[BIM_HDF5_NUM_FORMATS] = {
    {                               //0
      "HDF5",                       // short name, no spaces
      "ND: HDF-5",                      // Long format name
      "hdf|h5|he2|hdf5|he5|h5ebsd", // pipe "|" separated supported extension list
      1,                            //canRead;      // 0 - NO, 1 - YES
      0,                            //canWrite;     // 0 - NO, 1 - YES
      1,                            //canReadMeta;  // 0 - NO, 1 - YES
      0,                            //canWriteMeta; // 0 - NO, 1 - YES
      0,                            //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {             //1
      "IMARIS5",  // short name, no spaces
      "ND: Imaris 5", // Long format name
      "ims",      // pipe "|" separated supported extension list
      1,          //canRead;      // 0 - NO, 1 - YES
      0,          //canWrite;     // 0 - NO, 1 - YES
      1,          //canReadMeta;  // 0 - NO, 1 - YES
      0,          //canWriteMeta; // 0 - NO, 1 - YES
      0,          //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {            //2
      "DREAM3D", // short name, no spaces
      "ND: Dream3D", // Long format name
      "dream3d", // pipe "|" separated supported extension list
      1,         //canRead;      // 0 - NO, 1 - YES
      0,         //canWrite;     // 0 - NO, 1 - YES
      1,         //canReadMeta;  // 0 - NO, 1 - YES
      0,         //canWriteMeta; // 0 - NO, 1 - YES
      0,         //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {                      // 3
      "ViQi",              // short name, no spaces
      "ViQi Image Format", // Long format name
      "hdf|h5|hdf5|vqi",   // pipe "|" separated supported extension list
      1,                   //canRead;      // 0 - NO, 1 - YES
      0,                   //canWrite;     // 0 - NO, 1 - YES
      1,                   //canReadMeta;  // 0 - NO, 1 - YES
      0,                   //canWriteMeta; // 0 - NO, 1 - YES
      0,                   //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } }
};


FormatHeader hdf5Header = {

    sizeof(FormatHeader),
    "1.10.5",
    "HDF5",
    "HDF-5",

    BIM_FORMAT_HDF5_MAGIC_SIZE,
    { 1, BIM_HDF5_NUM_FORMATS, hdf5Items },

    hdf5ValidateFormatProc,
    hdf5AquireFormatProc, //AquireFormatProc
    hdf5ReleaseFormatProc, //ReleaseFormatProc

    // params
    NULL, //AquireIntParamsProc
    NULL, //LoadFormatParamsProc
    NULL, //StoreFormatParamsProc

    // image begin
    hdf5OpenImageProc,  //OpenImageProc
    hdf5CloseImageProc, //CloseImageProc

    // info
    hdf5GetNumPagesProc,  //GetNumPagesProc
    hdf5GetImageInfoProc, //GetImageInfoProc

    // read/write
    hdf5ReadImageProc,       //ReadImageProc
    NULL,                    //WriteImageProc
    hdf5ReadImageTileProc,   //ReadImageTileProc
    NULL,                    //WriteImageTileProc
    hdf5ReadImageLevelProc,  //ReadImageLevelProc
    NULL,                    //WriteImageLineProc
    hdf5ReadImageRegionProc, //ReadImageRegionProc
    NULL,                    //WriteImageRegionProc
    hdf5_append_metadata,    // AppendMetaDataProc
};

extern "C" {

FormatHeader *hdf5GetFormatHeader(void) {
    return &hdf5Header;
}

} // extern C
