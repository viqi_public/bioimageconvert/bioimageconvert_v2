/*******************************************************************************

  Manager for Image Formats

  Uses DimFiSDK version: 1.2

  Programmer: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  History:
    03/23/2004 18:03 - First creation
    08/04/2004 18:22 - custom stream managment compliant

  ver: 2

*******************************************************************************/

#include <algorithm>
#include <cerrno>
#include <cstring>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include <bim_lcms_parse.h>
#include <bim_metatags.h>
#include <bim_ome_types.h>
#include <xcolor.h>
#include <xstring.h>
#include <xtypes.h>

#include "bim_format_manager.h"

// formats
#include "biorad_pic/bim_biorad_pic_format.h"
#include "bmp/bim_bmp_format.h"
#include "jpeg/bim_jpeg_format.h"
#include "nanoscope/bim_nanoscope_format.h"
#include "png/bim_png_format.h"
#include "tiff/bim_tiff_format.h"
#ifdef BIM_JP2_FORMAT
#include "jp2/bim_jp2_format.h"
#endif
#include "ibw/bim_ibw_format.h"
#include "ome/bim_ome_format.h"
#include "raw/bim_raw_format.h"
#ifdef BIM_FFMPEG_FORMAT
#include "mpeg/bim_ffmpeg_format.h"
#endif
#include "dcraw/bim_dcraw_format.h"
#include "ole/bim_ole_format.h"
#ifdef BIM_GDCM_FORMAT
#include "dicom/bim_dicom_format.h"
#endif
#include "jxr/bim_jxr_format.h"
#include "mrc/bim_mrc_format.h"
#include "nifti/bim_nifti_format.h"
#include "webp/bim_webp_format.h"

#ifdef BIM_CZI_FORMAT
#include "czi/bim_czi_format.h"
#endif

#ifdef BIM_HDF5_FORMAT
#include "hdf5/bim_hdf5_format.h"
#endif

#ifdef BIM_OPENSLIDE_FORMAT
#include "openslide/bim_openslide_format.h"
#endif

#ifdef BIM_ND2_FORMAT
#include "nikon/bim_nd2_format.h"
#endif

#ifdef BIM_COMPOSITE_FORMAT
#include "composite/bim_composite_format.h"
#endif

using namespace bim;

FormatManager::FormatManager(XConf *c) {
    this->setConfiguration(c);
    progress_proc = NULL;
    error_proc = NULL;
    test_abort_proc = NULL;

    session_active = false;
    sessionCurrentPage = 0;
    this->info = ImageInfo();
    sessionHandle = initFormatHandle();
    sessionFormatIndex = -1;
    sessionSubIndex = 0;

    // add static formats, jpeg first updated: dima 07/21/2005 17:11
    addNewFormatHeader(jpegGetFormatHeader());
    addNewFormatHeader(bmpGetFormatHeader());
    addNewFormatHeader(pngGetFormatHeader());
    addNewFormatHeader(nanoscopeGetFormatHeader());
    addNewFormatHeader(ibwGetFormatHeader());
    addNewFormatHeader(omeGetFormatHeader());
    addNewFormatHeader(oleGetFormatHeader());

#ifdef BIM_JP2_FORMAT
    addNewFormatHeader(jp2GetFormatHeader());
#endif

#ifdef BIM_GDCM_FORMAT
    addNewFormatHeader(dicomGetFormatHeader());
#endif

#ifdef BIM_LIBWEBP_FORMAT
    addNewFormatHeader(webpGetFormatHeader());
#endif

#ifdef BIM_CZI_FORMAT
    addNewFormatHeader(cziGetFormatHeader());
#endif

#ifdef BIM_HDF5_FORMAT
    addNewFormatHeader(hdf5GetFormatHeader());
#endif

#ifdef BIM_ND2_FORMAT
    addNewFormatHeader(nd2GetFormatHeader());
#endif

// many TIFF variants
#ifdef BIM_JXRLIB_FORMAT
    addNewFormatHeader(jxrGetFormatHeader());
#endif

#ifdef BIM_OPENSLIDE_FORMAT
    addNewFormatHeader(openslideGetFormatHeader());
#endif

    addNewFormatHeader(tiffGetFormatHeader());
    addNewFormatHeader(dcrawGetFormatHeader());
    // formats that need file extensions to quickly identify themselves
    addNewFormatHeader(rawGetFormatHeader());

#ifdef BIM_NIFTI_FORMAT
    addNewFormatHeader(niftiGetFormatHeader());
#endif

#ifdef BIM_COMPOSITE_FORMAT
    addNewFormatHeader(compositeGetFormatHeader());
#endif

    addNewFormatHeader(mrcGetFormatHeader());
    addNewFormatHeader(bioRadPicGetFormatHeader());
#ifdef BIM_FFMPEG_FORMAT
    addNewFormatHeader(ffMpegGetFormatHeader());
#endif
}

FormatManager::~FormatManager() {
    sessionEnd();
    formatList.clear();
}

/*
inline FormatManager &FormatManager::operator=(FormatManager fm) {
    if (fm.countInstalledFormats() > this->countInstalledFormats()) {
        for (int i = this->countInstalledFormats(); i < fm.countInstalledFormats(); i++) {
            this->addNewFormatHeader(fm.getFormatHeader(i));
        }
    }
    this->setConfiguration(fm.conf);
    return *this;
}
*/

void FormatManager::setConfiguration(XConf *c) {
    this->conf = c;
}

bool FormatManager::isPathSameAsConfig() const {
    if (!this->conf) return false;
    return this->session_path == this->conf->getValue("-path");
}

FormatHeader *FormatManager::getFormatHeader(int i) {
    if (i >= (int)formatList.size()) return NULL;
    return formatList.at(i);
}

unsigned int FormatManager::countInstalledFormats() {
    return (unsigned int)formatList.size();
}

void FormatManager::addNewFormatHeader(FormatHeader *nfh) {
    formatList.push_back(nfh);
    setMaxMagicSize();
}

void FormatManager::setMaxMagicSize() {
    size_t max_magic_size = 0;
    for (unsigned int i = 0; i < formatList.size(); i++) {
        if (formatList.at(i)->neededMagicSize > max_magic_size)
            max_magic_size = formatList.at(i)->neededMagicSize;
    }

    this->magic_number.resize(max_magic_size, 0);
}

bool FormatManager::loadMagic(const bim::Filename fileName) {
    std::fill(this->magic_number.begin(), this->magic_number.end(), 0);

#ifdef BIM_WIN
    xstring fn(fileName);
    FILE *in_stream = _wfopen(fn.toUTF16().c_str(), L"rb");
#else
    FILE *in_stream = fopen(fileName, "rb");
#endif

    if ((in_stream == NULL) || ferror(in_stream)) {
#ifdef DEBUG
        std::cerr << "Error opening '" << fileName << "', error '" << std::strerror(errno) << "'." << std::endl;
#endif
        return false;
    }

    const size_t vNumRead = fread(&this->magic_number[0], 1, this->magic_number.size(), in_stream);
    if (ferror(in_stream) && (vNumRead < 1)) {
#ifdef DEBUG
        std::cerr << "FormatManager::loadMagic(" << fileName << "): File error during reading magic number." << std::endl;
#endif
        fclose(in_stream);
        return false;
    }

    fclose(in_stream);
    return true;
}

bool FormatManager::loadMagic(BIM_STREAM_CLASS *stream, SeekProc seekProc, ReadProc readProc) {
    if ((stream == NULL) || (seekProc == NULL) || (readProc == NULL)) return false;
    if (seekProc(stream, 0, SEEK_SET) != 0) return false;
    readProc(&this->magic_number[0], 1, this->magic_number.size(), stream);
    return true;
}

void FormatManager::getNeededFormatByMagic(const bim::Filename fileName, int &format_index, int &sub_index) {
    format_index = -1;
    sub_index = -1;
    for (unsigned int i = 0; i < formatList.size(); i++) {
        int s = formatList.at(i)->validateFormatProc(&this->magic_number[0], (uint)this->magic_number.size(), fileName);
        if (s > -1) {
            format_index = i;
            sub_index = s;
            break;
        }
    }
}

void FormatManager::getNeededFormatByName(const bim::xstring &formatName, int &format_index, int &sub_index) {
    format_index = -1;
    sub_index = -1;
    xstring fmt = formatName.toLowerCase();

    for (int i = 0; i < (int)formatList.size(); ++i) {
        for (int s = 0; s < (int)formatList.at(i)->supportedFormats.count; ++s) {
            bim::xstring fmt_short_name = formatList.at(i)->supportedFormats.item[s].formatNameShort;
            bim::xstring fmt_long_name = formatList.at(i)->supportedFormats.item[s].formatNameLong;
            if ((fmt_short_name.toLowerCase() == fmt) || (fmt_long_name.toLowerCase() == fmt)) {
                format_index = i;
                sub_index = s;
                return;
            } // if strcmp
        } // for s
    }

    return;
}

FormatItem *FormatManager::getFormatItem(const char *formatName) {
    int format_index=-1, sub_index=-1;
    this->getNeededFormatByName(formatName, format_index, sub_index);
    if (format_index < 0) return NULL;
    if (sub_index < 0) return NULL;

    FormatHeader *selectedFmt = formatList.at(format_index);
    if (sub_index >= (int)selectedFmt->supportedFormats.count) return NULL;
    return &selectedFmt->supportedFormats.item[sub_index];
}

bool FormatManager::isFormatSupported(const char *formatName) {
    FormatItem *fi = getFormatItem(formatName);
    if (fi == NULL) return false;
    return true;
}

bool FormatManager::isFormatSupportsWMP(const char *formatName) {
    FormatItem *fi = getFormatItem(formatName);
    if (fi == NULL) return false;
    if (fi->canWriteMultiPage == 1) return true;
    return false;
}

bool FormatManager::isFormatSupportsR(const char *formatName) {
    FormatItem *fi = getFormatItem(formatName);
    if (fi == NULL) return false;
    if (fi->canRead == 1) return true;
    return false;
}

bool FormatManager::isFormatSupportsW(const char *formatName) {
    FormatItem *fi = getFormatItem(formatName);
    if (fi == NULL) return false;
    if (fi->canWrite == 1) return true;
    return false;
}

bool FormatManager::isFormatSupportsBpcW(const char *formatName, int bpc) {
    FormatItem *fi = getFormatItem(formatName);
    if (fi == NULL) return false;
    if (fi->canWrite == 1) {
        if (fi->constrains.maxBitsPerSample == 0) return true;
        if ((int)fi->constrains.maxBitsPerSample >= bpc) return true;
    }
    return false;
}

const char *FormatManager::getNeededFormatByFileExt(const char *extension) {
    const bim::xstring ext = extension;
    for (size_t i = 0; i < formatList.size(); i++) {
        for (size_t s = 0; s < formatList.at(i)->supportedFormats.count; s++) {

            const bim::xstring exts = formatList.at(i)->supportedFormats.item[s].extensions;
            const std::vector<bim::xstring> isolatedExts = exts.split("|");
            if (std::find(isolatedExts.begin(), isolatedExts.end(), ext) != isolatedExts.end()) {
                return formatList.at(i)->supportedFormats.item[s].formatNameShort;
            }
        }
    }
    return nullptr;
}

void FormatManager::getNeededFormatByFileExt(const bim::Filename fileName, int &format_index, int &sub_index) {
    format_index = 0;
    sub_index = 0;
    (void)fileName;
}

void FormatManager::printAllFormats() {
    unsigned int i, s;

    for (i = 0; i < formatList.size(); i++) {
        std::cout << xstring::xprintf(
            "Format %d: "
            "%s"
            " ver: %s\n",
            i, formatList.at(i)->name, formatList.at(i)->version);
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++) {
            std::cout << xstring::xprintf("  %d: %s [", s, formatList.at(i)->supportedFormats.item[s].formatNameShort);

            if (formatList.at(i)->supportedFormats.item[s].canRead) std::cout << xstring::xprintf("R ");
            if (formatList.at(i)->supportedFormats.item[s].canWrite) std::cout << xstring::xprintf("W ");
            if (formatList.at(i)->supportedFormats.item[s].canReadMeta) std::cout << xstring::xprintf("RM ");
            if (formatList.at(i)->supportedFormats.item[s].canWriteMeta) std::cout << xstring::xprintf("WM ");
            if (formatList.at(i)->supportedFormats.item[s].canWriteMultiPage) std::cout << xstring::xprintf("WMP ");

            std::cout << xstring::xprintf("] <%s>\n", formatList.at(i)->supportedFormats.item[s].extensions);
        }
        std::cout << xstring::xprintf("\n");
    }
}

void FormatManager::printAllFormatsXML() {
    unsigned int i, s;

    for (i = 0; i < formatList.size(); i++) {
        std::cout << xstring::xprintf("<format index=\"%d\" name=\"%s\" version=\"%s\" >\n", i, formatList.at(i)->name, formatList.at(i)->version);

        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++) {
            std::cout << xstring::xprintf("  <codec index=\"%d\" name=\"%s\" >\n", s, formatList.at(i)->supportedFormats.item[s].formatNameShort);

            if (formatList.at(i)->supportedFormats.item[s].canRead)
                std::cout << xstring::xprintf("    <tag name=\"support\" value=\"reading\" />\n");

            if (formatList.at(i)->supportedFormats.item[s].canWrite)
                std::cout << xstring::xprintf("    <tag name=\"support\" value=\"writing\" />\n");

            if (formatList.at(i)->supportedFormats.item[s].canReadMeta)
                std::cout << xstring::xprintf("    <tag name=\"support\" value=\"reading metadata\" />\n");

            if (formatList.at(i)->supportedFormats.item[s].canWriteMeta)
                std::cout << xstring::xprintf("    <tag name=\"support\" value=\"writing metadata\" />\n");

            if (formatList.at(i)->supportedFormats.item[s].canWriteMultiPage)
                std::cout << xstring::xprintf("    <tag name=\"support\" value=\"writing multiple pages\" />\n");

            std::cout << xstring::xprintf("    <tag name=\"extensions\" value=\"%s\" />\n", formatList.at(i)->supportedFormats.item[s].extensions);
            std::cout << xstring::xprintf("    <tag name=\"fullname\" value=\"%s\" />\n", formatList.at(i)->supportedFormats.item[s].formatNameLong);
            std::cout << xstring::xprintf("    <tag name=\"min-samples-per-pixel\" value=\"%d\" />\n", formatList.at(i)->supportedFormats.item[s].constrains.minSamplesPerPixel);
            std::cout << xstring::xprintf("    <tag name=\"max-samples-per-pixel\" value=\"%d\" />\n", formatList.at(i)->supportedFormats.item[s].constrains.maxSamplesPerPixel);
            std::cout << xstring::xprintf("    <tag name=\"min-bits-per-sample\" value=\"%d\" />\n", formatList.at(i)->supportedFormats.item[s].constrains.minBitsPerSample);
            std::cout << xstring::xprintf("    <tag name=\"max-bits-per-sample\" value=\"%d\" />\n", formatList.at(i)->supportedFormats.item[s].constrains.maxBitsPerSample);


            std::cout << xstring::xprintf("  </codec>\n");
        }
        std::cout << xstring::xprintf("</format>\n");
    }
}

std::string FormatManager::getAllFormatsHTML() {
    unsigned int i, s;
    std::string fmts;
    xstring fmt;

    fmts += "<table>\n";
    fmts += "  <tr><th>Codec</th><th>Name</th><th>Features</th><th>Extensions</th></tr>\n";
    for (i = 0; i < formatList.size(); i++) {

        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++) {
            std::string codec = formatList.at(i)->supportedFormats.item[s].formatNameShort;
            std::string name = formatList.at(i)->supportedFormats.item[s].formatNameLong;
            xstring ext = formatList.at(i)->supportedFormats.item[s].extensions;
            xstring features;

            if (formatList.at(i)->supportedFormats.item[s].canRead) features += " R";
            if (formatList.at(i)->supportedFormats.item[s].canWrite) features += " W";
            if (formatList.at(i)->supportedFormats.item[s].canReadMeta) features += " RM";
            if (formatList.at(i)->supportedFormats.item[s].canWriteMeta) features += " WM";
            if (formatList.at(i)->supportedFormats.item[s].canWriteMultiPage) features += " WMP";
            fmt.sprintf("  <tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n", codec.c_str(), name.c_str(), features.c_str(), ext.c_str());
            fmts += fmt;
        }
    }
    fmts += "</table>\n";
    return fmts;
}

void FormatManager::printAllFormatsHTML() {
    std::string str = getAllFormatsHTML();
    std::cout << str;
}

std::string FormatManager::getAllExtensions() {

    unsigned int i, s;
    std::string str;

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++) {
            str += formatList.at(i)->supportedFormats.item[s].extensions;
            str += "|";
        }
    }
    return str;
}

std::string filterizeExtensions(const std::string &_exts) {
    std::string exts = _exts;
    std::string::size_type pos;
    pos = exts.find("|");
    while (pos != std::string::npos) {
        exts.replace(pos, 1, " *.");
        pos = exts.find("|");
    }
    return exts;
}

std::string FormatManager::getQtFilters() {
    unsigned int i, s;
    std::string str;
    std::string t;

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++) {
            str += formatList.at(i)->supportedFormats.item[s].formatNameLong;
            str += " (*.";
            std::string exts = formatList.at(i)->supportedFormats.item[s].extensions;
            str += filterizeExtensions(exts);

            str += ");;";
        }
    }
    return str;
}

std::string FormatManager::getQtReadFilters() {
    unsigned int i, s;
    std::string str;
    std::string t;

    str += "All images (";
    for (const std::string &vExtension : getExtensions()) {
        str += "*." + vExtension + " ";
    }
    str += ");;";

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++)
            if (formatList.at(i)->supportedFormats.item[s].canRead) {
                str += formatList.at(i)->supportedFormats.item[s].formatNameLong;
                str += " (*.";
                std::string exts = formatList.at(i)->supportedFormats.item[s].extensions;
                str += filterizeExtensions(exts);
                str += ");;";
            }
    }
    str += "All files (*.*)";
    return str;
}

std::string FormatManager::getQtWriteFilters() {
    unsigned int i, s;
    std::string str;
    std::string t;

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++)
            if (formatList.at(i)->supportedFormats.item[s].canWrite) {
                str += formatList.at(i)->supportedFormats.item[s].formatNameLong;
                str += " (*.";
                std::string exts = formatList.at(i)->supportedFormats.item[s].extensions;
                str += filterizeExtensions(exts);
                str += ");;";
            }
    }
    return str;
}

std::string FormatManager::getQtWriteMPFilters() {
    unsigned int i, s;
    std::string str;
    std::string t;

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++)
            if (formatList.at(i)->supportedFormats.item[s].canWriteMultiPage) {
                str += formatList.at(i)->supportedFormats.item[s].formatNameLong;
                str += " (*.";
                std::string exts = formatList.at(i)->supportedFormats.item[s].extensions;
                str += filterizeExtensions(exts);
                str += ");;";
            }
    }
    return str;
}

std::vector<std::string> FormatManager::getReadFormats() {
    unsigned int i, s;
    std::string str;
    std::vector<std::string> fmts;

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++)
            if (formatList.at(i)->supportedFormats.item[s].canRead) {
                str = formatList.at(i)->supportedFormats.item[s].formatNameShort;
                fmts.push_back(str);
            }
    }
    return fmts;
}

std::vector<std::string> FormatManager::getWriteFormats() {
    unsigned int i, s;
    std::string str;
    std::vector<std::string> fmts;

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++)
            if (formatList.at(i)->supportedFormats.item[s].canWrite) {
                str = formatList.at(i)->supportedFormats.item[s].formatNameShort;
                fmts.push_back(str);
            }
    }
    return fmts;
}

std::vector<std::string> FormatManager::getWriteMPFormats() {
    unsigned int i, s;
    std::string str;
    std::vector<std::string> fmts;

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++)
            if (formatList.at(i)->supportedFormats.item[s].canWriteMultiPage) {
                str = formatList.at(i)->supportedFormats.item[s].formatNameShort;
                fmts.push_back(str);
            }
    }
    return fmts;
}

std::string FormatManager::getFormatFilter(const char *formatName) {
    FormatItem *fi = getFormatItem(formatName);
    std::string str;
    if (fi == NULL) return str;

    //tr("Images (*.png *.xpm *.jpg)"));
    str += fi->formatNameLong;
    str += " (*.";
    std::string exts = fi->extensions;
    str += filterizeExtensions(exts);
    str += ")";
    return str;
}

std::string FormatManager::getFilterExtensions(const char *formatName) {
    FormatItem *fi = getFormatItem(formatName);
    std::string str;
    if (fi == NULL) return str;
    str += fi->extensions;
    return str;
}

std::string FormatManager::getFilterExtensionFirst(const char *formatName) {
    std::string str = getFilterExtensions(formatName);

    std::string::size_type pos;
    pos = str.find("|");
    if (pos != std::string::npos)
        str.resize(pos);
    return str;
}

std::set<std::string> FormatManager::getExtensions() {
    std::set<std::string> extensions;
    unsigned int i, s;

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++) {
            const std::string vExtensions = formatList.at(i)->supportedFormats.item[s].extensions;
            std::string token;
            std::istringstream tokenStream(vExtensions);
            while (std::getline(tokenStream, token, '|')) {
                extensions.insert(token);
            }
        }
    }

    return extensions;
}

std::set<std::string> FormatManager::getReadExtensions() {
    std::set<std::string> extensions;
    unsigned int i, s;

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++) {
            if (formatList.at(i)->supportedFormats.item[s].canRead) {
                const std::string vExtensions = formatList.at(i)->supportedFormats.item[s].extensions;
                std::string token;
                std::istringstream tokenStream(vExtensions);
                while (std::getline(tokenStream, token, '|')) {
                    extensions.insert(token);
                }
            }
        }
    }

    return extensions;
}

std::set<std::string> FormatManager::getWriteExtensions() {
    std::set<std::string> extensions;
    unsigned int i, s;

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++) {
            if (formatList.at(i)->supportedFormats.item[s].canWrite) {
                const std::string vExtensions = formatList.at(i)->supportedFormats.item[s].extensions;
                std::string token;
                std::istringstream tokenStream(vExtensions);
                while (std::getline(tokenStream, token, '|')) {
                    extensions.insert(token);
                }
            }
        }
    }

    return extensions;
}

std::set<std::string> FormatManager::getWriteMPFExtensions() {
    std::set<std::string> extensions;
    unsigned int i, s;

    for (i = 0; i < formatList.size(); i++) {
        for (s = 0; s < formatList.at(i)->supportedFormats.count; s++) {
            if (formatList.at(i)->supportedFormats.item[s].canWriteMultiPage) {
                const std::string vExtensions = formatList.at(i)->supportedFormats.item[s].extensions;
                std::string token;
                std::istringstream tokenStream(vExtensions);
                while (std::getline(tokenStream, token, '|')) {
                    extensions.insert(token);
                }
            }
        }
    }

    return extensions;
}

//--------------------------------------------------------------------------------------
// simple read/write operations
//--------------------------------------------------------------------------------------

int FormatManager::loadImage(BIM_STREAM_CLASS *stream,
                             ReadProc readProc, SeekProc seekProc, SizeProc sizeProc,
                             TellProc tellProc, EofProc eofProc, CloseProc closeProc,
                             const bim::Filename fileName, ImageBitmap *bmp, int page) {
    int format_index, format_sub;
    FormatHeader *selectedFmt;

    if ((stream != NULL) && (seekProc != NULL) && (readProc != NULL)) {
        if (!loadMagic(stream, seekProc, readProc)) return 1;
    } else {
        if (!loadMagic(fileName)) return 1;
    }

    getNeededFormatByMagic(fileName, format_index, format_sub);
    if (format_index == -1) return 1;
    selectedFmt = formatList.at(format_index);

    FormatHandle fmtParams = selectedFmt->aquireFormatProc();

    format_sub = selectedFmt->validateFormatProc(&this->magic_number[0], (uint)this->magic_number.size(), fileName);
    if (format_sub == -1) return 1;
    sessionHandle.subFormat = format_sub; // initial guess

    fmtParams.showProgressProc = progress_proc;
    fmtParams.showErrorProc = error_proc;
    fmtParams.testAbortProc = test_abort_proc;

    fmtParams.arguments = this->conf;
    fmtParams.stream = stream;
    fmtParams.readProc = readProc;
    fmtParams.writeProc = NULL;
    fmtParams.flushProc = NULL;
    fmtParams.seekProc = seekProc;
    fmtParams.sizeProc = sizeProc;
    fmtParams.tellProc = tellProc;
    fmtParams.eofProc = eofProc;
    fmtParams.closeProc = closeProc;

    fmtParams.fileName = fileName;
    fmtParams.image = bmp;
    fmtParams.magic = &this->magic_number[0];
    fmtParams.io_mode = bim::ImageIOModes::IO_READ;
    fmtParams.pageNumber = page;

    if (selectedFmt->openImageProc(&fmtParams, bim::ImageIOModes::IO_READ) != 0) return 1;

    //bmpInfo = selectedFmt->getImageInfoProc ( &fmtParams, 0 );
    selectedFmt->readImageProc(&fmtParams, page);

    selectedFmt->closeImageProc(&fmtParams);

    // RELEASE FORMAT
    selectedFmt->releaseFormatProc(&fmtParams);
    return 0;
}

int FormatManager::loadImage(const bim::Filename fileName, ImageBitmap *bmp, int page) {
    return loadImage(NULL, NULL, NULL, NULL, NULL, NULL, NULL, fileName, bmp, page);
}

int FormatManager::loadImage(const bim::Filename fileName, ImageBitmap *bmp) {
    return loadImage(NULL, NULL, NULL, NULL, NULL, NULL, NULL, fileName, bmp, 0);
}

/*
void FormatManager::loadBuffer (void *p, int buf_size, ImageBitmap *bmp) {
  MemIOBuf memBuf;
  MemIO_InitBuf( &memBuf, buf_size, (unsigned char *) p );
  loadImage ( &memBuf, MemIO_ReadProc, MemIO_SeekProc, MemIO_SizeProc, MemIO_TellProc, MemIO_EofProc, MemIO_CloseProc, "buffer.dat", bmp, 0 );
  MemIO_DeinitBuf( &memBuf );
}
*/

int FormatManager::writeImage(BIM_STREAM_CLASS *stream, ReadProc readProc,
                              WriteProc writeProc, FlushProc flushProc, SeekProc seekProc,
                              SizeProc sizeProc, TellProc tellProc, EofProc eofProc, CloseProc closeProc,
                              const bim::Filename fileName, ImageBitmap *bmp, const char *formatName,
                              int quality, TagMap *meta, const char *options) {
    int format_index, sub_index;
    FormatHeader *selectedFmt;

    getNeededFormatByName(formatName, format_index, sub_index);
    selectedFmt = formatList.at(format_index);

    FormatHandle fmtParams = selectedFmt->aquireFormatProc();

    fmtParams.showProgressProc = progress_proc;
    fmtParams.showErrorProc = error_proc;
    fmtParams.testAbortProc = test_abort_proc;

    fmtParams.arguments = this->conf;
    fmtParams.stream = stream;
    fmtParams.writeProc = writeProc;
    fmtParams.readProc = readProc;
    fmtParams.flushProc = flushProc;
    fmtParams.seekProc = seekProc;
    fmtParams.sizeProc = sizeProc;
    fmtParams.tellProc = tellProc;
    fmtParams.eofProc = eofProc;
    fmtParams.closeProc = closeProc;

    fmtParams.subFormat = sub_index;
    fmtParams.fileName = fileName;
    fmtParams.image = bmp;
    fmtParams.quality = (unsigned char)quality;
    fmtParams.io_mode = bim::ImageIOModes::IO_WRITE;
    if (meta)
        fmtParams.metaData = meta;
    else
        fmtParams.metaData = &this->metadata;

    if (options != NULL && options[0] != 0) fmtParams.options = (char *)options;

    if (selectedFmt->openImageProc(&fmtParams, bim::ImageIOModes::IO_WRITE) != 0) return 1;

    selectedFmt->writeImageProc(&fmtParams);

    selectedFmt->closeImageProc(&fmtParams);

    // RELEASE FORMAT
    selectedFmt->releaseFormatProc(&fmtParams);
    return 0;
}

int FormatManager::writeImage(const bim::Filename fileName, ImageBitmap *bmp, const char *formatName, int quality, TagMap *meta) {
    return writeImage(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, fileName, bmp, formatName, quality, meta);
}

int FormatManager::writeImage(const bim::Filename fileName, ImageBitmap *bmp, const char *formatName, const char *options, TagMap *meta) {
    return writeImage(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, fileName, bmp, formatName, 100, meta, options);
}

/*
unsigned char *FormatManager::writeBuffer ( ImageBitmap *bmp, const char *formatName, int quality,
                                               TagList *meta, int &buf_size ) {
  DMemIO memBuf;
  unsigned char *p;
  dMemIO_Init( &memBuf, 0, NULL );
  writeImage ( &memBuf, dMemIO_Read, dMemIO_Write, dMemIO_Flush, dMemIO_Seek,
               dMemIO_Size, dMemIO_Tell, dMemIO_Eof, dMemIO_Close,
               "buffer.dat", bmp, formatName, quality, meta );

  p = new unsigned char [ memBuf.size ];
  buf_size = memBuf.size;
  memcpy( p, memBuf.data, memBuf.size );
  dMemIO_Destroy( &memBuf );
  return p;
}

unsigned char *FormatManager::writeBuffer ( ImageBitmap *bmp, const char *formatName, int quality, int &buf_size )
{
  return writeBuffer ( bmp, formatName, quality, NULL, buf_size );
}

unsigned char *FormatManager::writeBuffer ( ImageBitmap *bmp, const char *formatName, int &buf_size )
{
  return writeBuffer ( bmp, formatName, 100, NULL, buf_size );
}
*/

void FormatManager::readImagePreview(const bim::Filename fileName, ImageBitmap *bmp,
                                     bim::uint roiX, bim::uint roiY, bim::uint roiW, bim::uint roiH, bim::uint w, bim::uint h) {
    /*

      int format_index;
      FormatHeader *selectedFmt;

      if (!loadMagic(fileName)) return;

      format_index = getNeededFormatByMagic(fileName);
      selectedFmt = formatList.at( format_index );

      FormatHandle fmtParams = selectedFmt->aquireFormatProc();

      if ( selectedFmt->validateFormatProc(magic_number, max_magic_size, fileName) == -1 ) return;

      fmtParams.roiX = roiX;
      fmtParams.roiY = roiY;
      fmtParams.roiW = roiW;
      fmtParams.roiH = roiH;

      fmtParams.fileName = fileName;
      fmtParams.image = bmp;
      fmtParams.magic = magic_number;
      fmtParams.io_mode = IO_READ;
      if ( selectedFmt->openImageProc ( &fmtParams, IO_READ ) != 0) return;


      //----------------------------------------------------
      // now if function is not implemented, run standard
      // implementation
      //----------------------------------------------------
      if ( selectedFmt->readImagePreviewProc != NULL )
        selectedFmt->readImagePreviewProc ( &fmtParams, w, h );
      else
      {
        // read whole image and resize if needed
        selectedFmt->readImageProc ( &fmtParams, 0 );

        // first extract ROI from full image
        //if ( roiX < 0) roiX = 0; if ( roiY < 0) roiY = 0;
        if ( roiX >= bmp->i.width) roiX = bmp->i.width-1;
        if ( roiY >= bmp->i.height) roiY = bmp->i.height-1;
        if ( roiW >= bmp->i.width-roiX) roiW = bmp->i.width-roiX-1;
        if ( roiY >= bmp->i.height-roiY) roiH = bmp->i.height-roiY-1;

        if ( (roiW>0) && (roiH>0) )
          retreiveImgROI( bmp, roiX, roiY, roiW, roiH );

        // now resize to desired size
        if ( (w>0) && (h>0) )
          resizeImgNearNeighbor( bmp, w, h);


      }
      //----------------------------------------------------

      selectedFmt->closeImageProc ( &fmtParams );

      // RELEASE FORMAT
      selectedFmt->releaseFormatProc ( &fmtParams );

    */
}

void FormatManager::readImageThumb(const bim::Filename fileName, ImageBitmap *bmp, bim::uint w, bim::uint h) {
    /*
      int format_index;
      FormatHeader *selectedFmt;

      if (!loadMagic(fileName)) return;

      format_index = getNeededFormatByMagic(fileName);
      selectedFmt = formatList.at( format_index );

      FormatHandle fmtParams = selectedFmt->aquireFormatProc();

      if ( selectedFmt->validateFormatProc(magic_number, max_magic_size, fileName) == -1 ) return;

      fmtParams.fileName = fileName;
      fmtParams.image = bmp;
      fmtParams.magic = magic_number;
      fmtParams.io_mode = IO_READ;
      if ( selectedFmt->openImageProc ( &fmtParams, IO_READ ) != 0) return;


      //----------------------------------------------------
      // now if function is not implemented, run standard
      // implementation
      //----------------------------------------------------
      if ( selectedFmt->readImagePreviewProc != NULL )
        selectedFmt->readImagePreviewProc ( &fmtParams, w, h );
      else
      {
        // read whole image and resize if needed
        selectedFmt->readImageProc ( &fmtParams, 0 );

        // now resize to desired size
        if ( (w>0) && (h>0) )
          resizeImgNearNeighbor( bmp, w, h);

      }
      //----------------------------------------------------

      selectedFmt->closeImageProc ( &fmtParams );

      // RELEASE FORMAT
      selectedFmt->releaseFormatProc ( &fmtParams );
    */
}


//--------------------------------------------------------------------------------------
// begin: session-wide operations
//--------------------------------------------------------------------------------------

bool FormatManager::sessionIsReading(const std::string &fileName) const {
    if (!session_active) return false;
    if (fileName.size() > 0)
        if (fileName != sessionFileName) return false;
    if (sessionHandle.io_mode == bim::ImageIOModes::IO_READ) return true;
    return false;
}

bool FormatManager::sessionIsWriting(const std::string &fileName) const {
    if (!session_active) return false;
    if (fileName.size() > 0)
        if (fileName != sessionFileName) return false;
    if (sessionHandle.io_mode == bim::ImageIOModes::IO_WRITE) return true;
    return false;
}

int FormatManager::sessionStartRead(BIM_STREAM_CLASS *stream, ReadProc readProc, SeekProc seekProc,
                                    SizeProc sizeProc, TellProc tellProc, EofProc eofProc,
                                    CloseProc closeProc, const bim::Filename fileName, const char *formatName) {

    // enable re-use of previosuly created file managers for further operations
    this->access_time = time(NULL);
    if (session_active && !this->isPathSameAsConfig()) {
        FormatHeader *selectedFmt = formatList.at(sessionFormatIndex);
        this->info = selectedFmt->getImageInfoProc(&sessionHandle, 0);
        if (this->conf)
            this->session_path = this->conf->getValue("-path");
        return 0;
    }
    if (session_active) return 0;
    if (this->conf)
        this->session_path = this->conf->getValue("-path");

    //if (session_active) sessionEnd();
    sessionCurrentPage = 0;
    if (formatName == NULL) {
        if ((stream != NULL) && (seekProc != NULL) && (readProc != NULL)) {
            if (!loadMagic(stream, seekProc, readProc)) {
#ifdef DEBUG
                std::cerr << "FormatManager::sessionStartRead(): Failed to access file magic from stream. Aborted." << std::endl;
#endif
                return 1;
            }
        } else {
            if (!loadMagic(fileName)) {
#ifdef DEBUG
                std::cerr << "FormatManager::sessionStartRead(): Failed to access file magic from file. Aborted." << std::endl;
#endif
                return 1;
            }
        }
        getNeededFormatByMagic(fileName, sessionFormatIndex, sessionSubIndex);
    } else {
        // force specific format
        getNeededFormatByName(formatName, sessionFormatIndex, sessionSubIndex);
    }

    if (sessionFormatIndex < 0) {
#ifdef DEBUG
        std::cerr << "FormatManager::sessionStartRead(): sessionFormatIndex is not set. Aborted." << std::endl;
#endif
        return 1;
    }
    FormatHeader *selectedFmt = formatList.at(sessionFormatIndex);

    sessionHandle = selectedFmt->aquireFormatProc();
    sessionHandle.subFormat = sessionSubIndex; // initial guess

    //if ( selectedFmt->validateFormatProc(magic_number, max_magic_size)==-1 ) return 1;
    sessionHandle.arguments = this->conf;
    sessionHandle.showProgressProc = progress_proc;
    sessionHandle.showErrorProc = error_proc;
    sessionHandle.testAbortProc = test_abort_proc;

    sessionHandle.stream = stream;
    sessionHandle.readProc = readProc;
    sessionHandle.writeProc = NULL;
    sessionHandle.flushProc = NULL;
    sessionHandle.seekProc = seekProc;
    sessionHandle.sizeProc = sizeProc;
    sessionHandle.tellProc = tellProc;
    sessionHandle.eofProc = eofProc;
    sessionHandle.closeProc = closeProc;

    this->got_meta_for_session = -1;
    this->metadata.clear();
    this->info = ImageInfo();

    sessionFileName = fileName;
    sessionHandle.fileName = &sessionFileName[0];
    sessionHandle.magic = &this->magic_number[0];
    sessionHandle.io_mode = bim::ImageIOModes::IO_READ;
    const int res = selectedFmt->openImageProc(&sessionHandle, bim::ImageIOModes::IO_READ);
    sessionSubIndex = sessionHandle.subFormat;
    if (res == 0) {
        session_active = true;
        this->info = selectedFmt->getImageInfoProc(&sessionHandle, 0);
    } else {
#ifdef DEBUG
        std::cerr << "FormatManager::sessionStartRead(): Failed to open file with openImageProc(). Aborted." << std::endl;
#endif
    }
    return res;
}

int FormatManager::sessionStartRead(const bim::Filename fileName, const char *formatName) {
    return sessionStartRead(NULL, NULL, NULL, NULL, NULL, NULL, NULL, fileName, formatName);
}

int FormatManager::sessionStartReadRAW(const bim::Filename fileName, unsigned int header_offset, bool big_endian, bool interleaved) {
    int res = sessionStartRead(fileName, "RAW");
    if (res == 0) {
        RawParams *rp = (RawParams *)sessionHandle.internalParams;
        rp->header_offset = header_offset;
        rp->big_endian = big_endian;
        rp->interleaved = interleaved;
    }
    return res;
}

int FormatManager::sessionStartWrite(const bim::Filename fileName, const char *formatName, const char *options) {
    return sessionStartWrite(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, fileName, formatName, options);
}

int FormatManager::sessionStartWrite(BIM_STREAM_CLASS *stream,
                                     WriteProc writeProc, FlushProc flushProc, SeekProc seekProc,
                                     SizeProc sizeProc, TellProc tellProc, EofProc eofProc, CloseProc closeProc,
                                     const bim::Filename fileName, const char *formatName, const char *options) {
    int res;
    this->access_time = time(NULL);
    if (session_active == true) sessionEnd();
    sessionCurrentPage = 0;

    getNeededFormatByName(formatName, sessionFormatIndex, sessionSubIndex);
    if (sessionFormatIndex < 0) {
        std::cerr << "Error: selected format '" << formatName << "' not found. Formats are not case sensitive, but must be given with the precise string (i.e. 'tif' is not the same as 'tiff'). Aborted." << std::endl;
        return -1;
    }

    FormatHeader *selectedFmt = formatList.at(sessionFormatIndex);

    sessionHandle = selectedFmt->aquireFormatProc();

    sessionHandle.showProgressProc = progress_proc;
    sessionHandle.showErrorProc = error_proc;
    sessionHandle.testAbortProc = test_abort_proc;

    sessionHandle.arguments = this->conf;
    sessionHandle.stream = stream;
    sessionHandle.writeProc = writeProc;
    sessionHandle.readProc = NULL;
    sessionHandle.flushProc = flushProc;
    sessionHandle.seekProc = seekProc;
    sessionHandle.sizeProc = sizeProc;
    sessionHandle.tellProc = tellProc;
    sessionHandle.eofProc = eofProc;
    sessionHandle.closeProc = closeProc;

    sessionFileName = fileName;
    sessionHandle.fileName = &sessionFileName[0];
    sessionHandle.io_mode = bim::ImageIOModes::IO_WRITE;
    sessionHandle.subFormat = sessionSubIndex;

    if (options != NULL && options[0] != 0) sessionHandle.options = (char *)options;

    res = selectedFmt->openImageProc(&sessionHandle, bim::ImageIOModes::IO_WRITE);

    if (res == 0) session_active = true;
    return res;
}

void FormatManager::sessionEnd() {
    if ((sessionFormatIndex >= 0) && (sessionFormatIndex < (int)formatList.size())) {
        FormatHeader *selectedFmt = formatList.at(sessionFormatIndex);
        selectedFmt->closeImageProc(&sessionHandle);
        selectedFmt->releaseFormatProc(&sessionHandle);
    }
    sessionFormatIndex = -1;
    session_active = false;
    sessionCurrentPage = 0;
    this->got_meta_for_session = -1;
}

void FormatManager::sessionSetQuality(int quality) {
    sessionHandle.quality = (unsigned char)quality;
}

void FormatManager::sessionSetNumberOfPages(int num_pages) {
    sessionHandle.numberOfPages = num_pages;
}

int FormatManager::sessionGetFormat() {
    if (session_active != true) return -1;
    return sessionFormatIndex;
}

int FormatManager::sessionGetSubFormat() {
    if (session_active != true) return -1;
    return sessionSubIndex;
}

const char *FormatManager::sessionGetCodecName() {
    if (session_active != true) return NULL;
    return formatList.at(sessionFormatIndex)->name;
}

bool FormatManager::sessionIsCurrentCodec(const char *name) {
    const char *fmt = sessionGetCodecName();
    return (strcmp(fmt, name) == 0);
}

const char *FormatManager::sessionGetFormatName() {
    if (session_active != true) return NULL;
    return formatList.at(sessionFormatIndex)->supportedFormats.item[sessionSubIndex].formatNameShort;
}

bool FormatManager::sessionIsCurrentFormat(const char *name) {
    const char *fmt = sessionGetFormatName();
    return (strcmp(fmt, name) == 0);
}

uint FormatManager::sessionGetNumberOfPages() {
    if (session_active != true) return 0;
    FormatHeader *selectedFmt = formatList.at(sessionFormatIndex);
    return selectedFmt->getNumPagesProc(&sessionHandle);
}

int FormatManager::sessionGetCurrentPage() {
    if (session_active != true) return 0;
    return sessionCurrentPage;
}

int FormatManager::sessionReadImage(ImageBitmap *bmp, bim::uint page) {
    if (session_active != true) return 1;
    this->access_time = time(NULL);
    FormatHeader *selectedFmt = formatList.at(sessionFormatIndex);
    sessionCurrentPage = page;
    sessionHandle.image = bmp;
    sessionHandle.pageNumber = page;
    int r = selectedFmt->readImageProc(&sessionHandle, page);
    sessionHandle.image = NULL;
    this->info = bmp->i;
    return r;
}

int FormatManager::sessionWriteImage(ImageBitmap *bmp, bim::uint page) {
    if (this->session_active != true) return 1;
    this->access_time = time(NULL);
    FormatHeader *selectedFmt = this->formatList.at(this->sessionFormatIndex);
    this->sessionCurrentPage = page;
    this->sessionHandle.image = bmp;
    this->sessionHandle.pageNumber = page;
    this->sessionHandle.metaData = &this->metadata;
    return selectedFmt->writeImageProc(&this->sessionHandle);
}



int FormatManager::sessionReadLevel(ImageBitmap *bmp, bim::uint page, bim::uint level) {
    if (session_active != true) return 1;
    this->access_time = time(NULL);
    FormatHeader *selectedFmt = formatList.at(sessionFormatIndex);
    if (!selectedFmt->readImageLevelProc) return 1;
    sessionHandle.image = bmp;
    int r = selectedFmt->readImageLevelProc(&sessionHandle, page, level);
    sessionHandle.image = NULL;
    return r;
}

int FormatManager::sessionReadTile(ImageBitmap *bmp, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level) {
    if (session_active != true) return 1;
    this->access_time = time(NULL);
    FormatHeader *selectedFmt = formatList.at(sessionFormatIndex);
    if (!selectedFmt->readImageTileProc) return 1;
    sessionHandle.image = bmp;
    int r = selectedFmt->readImageTileProc(&sessionHandle, page, xid, yid, level);
    sessionHandle.image = NULL;
    return r;
}

int FormatManager::sessionReadRegion(ImageBitmap *bmp, bim::uint page, bim::uint64 x1, bim::uint64 y1, bim::uint64 x2, bim::uint64 y2, bim::uint level) {
    if (session_active != true) return 1;
    this->access_time = time(NULL);
    FormatHeader *selectedFmt = formatList.at(sessionFormatIndex);
    if (!selectedFmt->readImageRegionProc) return 1;
    sessionHandle.image = bmp;
    int r = selectedFmt->readImageRegionProc(&sessionHandle, page, x1, y1, x2, y2, level);
    sessionHandle.image = NULL;
    return r;
}




void FormatManager::sessionReadImagePreview(ImageBitmap *bmp,
                                            bim::uint roiX, bim::uint roiY, bim::uint roiW, bim::uint roiH,
                                            bim::uint w, bim::uint h) {
    /*
      if (session_active != true) return;
      this->access_time = time(NULL);
      FormatHeader *selectedFmt = formatList.at( sessionFormatIndex );
      sessionCurrentPage = 0;
      sessionHandle.image = bmp;
      sessionHandle.pageNumber = 0;
      sessionHandle.roiX = roiX;
      sessionHandle.roiY = roiY;
      sessionHandle.roiW = roiW;
      sessionHandle.roiH = roiH;

      //----------------------------------------------------
      // now if function is not implemented, run standard
      // implementation
      //----------------------------------------------------
      if ( selectedFmt->readImagePreviewProc != NULL )
        selectedFmt->readImagePreviewProc ( &sessionHandle, w, h );
      else
      {
        // read whole image and resize if needed
        selectedFmt->readImageProc ( &sessionHandle, 0 );

        // first extract ROI from full image
        //if ( roiX < 0) roiX = 0; if ( roiY < 0) roiY = 0;
        if ( roiX >= bmp->i.width) roiX = bmp->i.width-1;
        if ( roiY >= bmp->i.height) roiY = bmp->i.height-1;
        if ( roiW >= bmp->i.width-roiX) roiW = bmp->i.width-roiX-1;
        if ( roiY >= bmp->i.height-roiY) roiH = bmp->i.height-roiY-1;

        if ( (roiW>0) && (roiH>0) )
          retreiveImgROI( bmp, roiX, roiY, roiW, roiH );

        // now resize to desired size
        if ( (w>0) && (h>0) )
          resizeImgNearNeighbor( bmp, w, h);
      }
    */
}

void FormatManager::sessionReadImageThumb(ImageBitmap *bmp, bim::uint w, bim::uint h) {
    /*
      if (session_active != true) return;
      this->access_time = time(NULL);
      FormatHeader *selectedFmt = formatList.at( sessionFormatIndex );
      sessionCurrentPage = 0;
      sessionHandle.image = bmp;
      sessionHandle.pageNumber = 0;

      //----------------------------------------------------
      // now if function is not implemented, run standard
      // implementation
      //----------------------------------------------------
      if ( selectedFmt->readImagePreviewProc != NULL )
        selectedFmt->readImagePreviewProc ( &sessionHandle, w, h );
      else
      {
        // read whole image and resize if needed
        selectedFmt->readImageProc ( &sessionHandle, 0 );

        // now resize to desired size
        if ( (w>0) && (h>0) )
          resizeImgNearNeighbor( bmp, w, h);

      }
    */
}

//--------------------------------------------------------------------------------------
// metadata
//--------------------------------------------------------------------------------------

void FormatManager::sessionWriteSetMetadata(const TagMap &hash) {
    this->metadata = hash;
    if (this->session_active)
        this->sessionHandle.metaData = &this->metadata;
}

void FormatManager::sessionWriteSetOMEXML(const std::string &omexml) {
    this->metadata.set_value(bim::RAW_TAGS_OMEXML, omexml, bim::RAW_TYPES_OMEXML);
}

void FormatManager::append_channel_names(const std::vector<std::string> &names) {
    for (unsigned int i = 0; i < names.size(); ++i) {
        xstring path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i);
        this->appendMetadata(path + bim::CHANNEL_INFO_NAME, names[i]);
    }
}

std::string FormatManager::constructOMEXML() {
    return bim::constructOMEXML(this->sessionFileName, sessionHandle.image, &metadata);
}

void FormatManager::sessionParseMetaData(bim::uint page) {
    if (this->got_meta_for_session == static_cast<int>(page)) return;

    //channel_names.clear();
    //this->display_lut.clear();
    this->metadata.clear();

    //tagList = sessionReadMetaData(page, -1, -1, -1);
    this->got_meta_for_session = page;

    /* { // parsing stuff from red image

    display_lut.resize((int)bim::NumberDisplayChannels);
    for (int i = 0; i < (int)bim::NumberDisplayChannels; ++i) display_lut[i] = -1;

    if (info.samples == 1)
    for (int i = 0; i < 3; ++i)
    display_lut[i] = 0;
    else
    for (int i = 0; i < bim::min<int>((int)bim::NumberDisplayChannels, info.samples); ++i)
    display_lut[i] = i;
    }*/

    // API v1.7: first check if format has append metadata function, if yes, run
    if (session_active && sessionFormatIndex >= 0 && static_cast<size_t>(sessionFormatIndex) < formatList.size()) {
        FormatHeader *selectedFmt = formatList.at(sessionFormatIndex);
        if (selectedFmt->appendMetaDataProc)
            selectedFmt->appendMetaDataProc(&sessionHandle, &metadata);
    }

    // hack towards new system: in case the format loaded all data directly into the map, refresh static values
    //fill_static_metadata_from_map();

    // All following will only be inserted if tags do not exist

    appendMetadata(bim::IMAGE_FORMAT, this->sessionGetFormatName());


    /*if (this->pixel_size[0] > 0) {
    appendMetadata(bim::PIXEL_RESOLUTION_X, this->pixel_size[0]);
    appendMetadata(bim::PIXEL_RESOLUTION_UNIT_X, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    }

    if (this->pixel_size[1] > 0) {
    appendMetadata(bim::PIXEL_RESOLUTION_Y, this->pixel_size[1]);
    appendMetadata(bim::PIXEL_RESOLUTION_UNIT_Y, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    }

    if (this->pixel_size[2] > 0) {
    appendMetadata(bim::PIXEL_RESOLUTION_Z, this->pixel_size[2]);
    appendMetadata(bim::PIXEL_RESOLUTION_UNIT_Z, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    }

    if (this->pixel_size[3] > 0) {
    appendMetadata(bim::PIXEL_RESOLUTION_T, this->pixel_size[3]);
    appendMetadata(bim::PIXEL_RESOLUTION_UNIT_T, bim::PIXEL_RESOLUTION_UNIT_SECONDS);
    }*/

    // -------------------------------------------------------
    // Image info
    appendMetadata(bim::META_VERSION_TAG, bim::META_VERSION);

    appendMetadata(bim::IMAGE_NUM_X, (int)info.width);
    appendMetadata(bim::IMAGE_NUM_Y, (int)info.height);
    appendMetadata(bim::IMAGE_NUM_Z, std::max<int>(1, (int)info.number_z));
    appendMetadata(bim::IMAGE_NUM_T, std::max<int>(1, (int)info.number_t));
    appendMetadata(bim::IMAGE_NUM_C, (int)info.samples);
    appendMetadata(bim::IMAGE_NUM_P, (int)info.number_pages);
    appendMetadata(bim::PIXEL_DEPTH, (int)info.depth);
    appendMetadata(bim::PIXEL_FORMAT, bim::meta::pixel_format_strings[(int) info.pixelType]);
    appendMetadata(bim::RAW_ENDIAN, (bim::bigendian) ? "big" : "little");

    // overwrite imageMode from metadata colorspace
    if (metadata.hasKey(bim::ICC_TAGS_COLORSPACE)) {
        bim::ImageModes mode = lcms_image_mode(metadata.get_value(bim::ICC_TAGS_COLORSPACE));
        if (!(mode == bim::ImageModes::IM_RGB && info.imageMode == bim::ImageModes::IM_RGBA))
            info.imageMode = mode;
    }
    this->set_metadata_tag(bim::IMAGE_MODE, bim::meta::image_mode_strings[(int)info.imageMode]);
    this->appendMetadata(bim::ICC_TAGS_COLORSPACE, bim::meta::image_mode_strings[(int)info.imageMode]);

    appendMetadata(bim::IMAGE_NUM_RES_L, (int)info.number_levels);
    if (info.tileWidth > 0 && info.tileHeight > 0) {
        appendMetadata(bim::TILE_NUM_X, (int)info.tileWidth);
        appendMetadata(bim::TILE_NUM_Y, (int)info.tileHeight);
    }

    appendMetadata(bim::IMAGE_DIMENSIONS, "XYCZT");

    // Output channel names, only set if reders didn't define their own
    if (info.imageMode == bim::ImageModes::IM_RGB) {
        std::vector<std::string> names({ "Red", "Green", "Blue" });
        this->append_channel_names(names);
    } else if (info.imageMode == bim::ImageModes::IM_RGBA) {
        std::vector<std::string> names({ "Red", "Green", "Blue", "Alpha" });
        this->append_channel_names(names);
    } else if (info.imageMode == bim::ImageModes::IM_HSL) {
        std::vector<std::string> names({ "Hue", "Saturation", "Lightness" });
        this->append_channel_names(names);
    } else if (info.imageMode == bim::ImageModes::IM_HSV) {
        std::vector<std::string> names({ "Hue", "Saturation", "Brightness" });
        this->append_channel_names(names);
    } else if (info.imageMode == bim::ImageModes::IM_CMYK) {
        std::vector<std::string> names({ "Cyan", "Magenta", "Yellow", "Black" });
        this->append_channel_names(names);
    } else if (info.imageMode == bim::ImageModes::IM_YUV) {
        std::vector<std::string> names({ "Y", "U", "V" });
        this->append_channel_names(names);
    } else if (info.imageMode == bim::ImageModes::IM_XYZ) {
        std::vector<std::string> names({ "X", "Y", "Z" });
        this->append_channel_names(names);
    } else if (info.imageMode == bim::ImageModes::IM_LAB) {
        std::vector<std::string> names({ "Lightness", "a*", "b*" });
        this->append_channel_names(names);
    } else if (info.imageMode == bim::ImageModes::IM_CMY) {
        std::vector<std::string> names({ "Cyan", "Magenta", "Yellow" });
        this->append_channel_names(names);
    } else if (info.imageMode == bim::ImageModes::IM_LUV) {
        std::vector<std::string> names({ "Lightness", "u*", "v*" });
        this->append_channel_names(names);
    } else if (info.imageMode == bim::ImageModes::IM_YCbCr) {
        std::vector<std::string> names({ "Luminance", "Cb", "Cr" });
        this->append_channel_names(names);
    }

    for (bim::uint64 i = 0; i < info.samples; ++i) {
        xstring path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i);
        xstring name = this->metadata.get_value(path + bim::CHANNEL_INFO_NAME);
        //if (name.size() == 0) // try old format
        //    name = this->metadata.get_value(xstring::xprintf(bim::CHANNEL_NAME_TEMPLATE.c_str(), i));
        
        xstring color = this->metadata.get_value(path + bim::CHANNEL_INFO_COLOR);
        if (color.size() == 0) {
            bim::Color<float> c = Color<float>::default_color((int)i, name, info.samples);
            if (i == 3 && info.imageMode == bim::ImageModes::IM_RGBA)
                c = bim::Color<float>(0., 0., 0.);

            //this->metadata.append_tag(xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), i), c.to_string_8bit());
            this->metadata.append_tag(path + bim::CHANNEL_INFO_COLOR, c.to_string_float());
        } /* else {
            std::vector<double> c = color.splitDouble(",");
            if (c.size() >= 3) {
                int r = static_cast<int>(c[0] * 255);
                int g = static_cast<int>(c[1] * 255);
                int b = static_cast<int>(c[2] * 255);
                //this->metadata.append_tag(xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), i), bim::xstring::xprintf("%d,%d,%d", r, g, b));
            }
        }*/
    }
}



//--------------------------------------------------------------------------------------
// SessionCache
//--------------------------------------------------------------------------------------

FormatManager *SessionCache::pop(const std::string &filename) {
    FormatManager *fm;
    std::list<FormatManager *>::iterator it;
    for (it = this->cache.begin(); it != this->cache.end(); ++it) {
        fm = *it;
        if (fm->sessionFilename() == filename) break;
    }

    if (it != this->cache.end() && fm) {
        this->cache.erase(it);
        return fm;
    }
    return NULL;
}

bool SessionCache::cached(const std::string &filename) const {
    FormatManager *fm;
    std::list<FormatManager *>::const_iterator it;
    for (it = this->cache.begin(); it != this->cache.end(); ++it) {
        fm = *it;
        if (fm->sessionFilename() == filename) return true;
    }
    return false;
}

void SessionCache::push(FormatManager *cache_item) {
    while (this->cache.size() >= this->max_size) {
        FormatManager *fm = this->cache.front();
        this->cache.pop_front();
        fm->sessionEnd();
        delete fm;
    }
    this->cache.push_back(cache_item);
}

void SessionCache::optimize(time_t max_delta_minutes) {
    //seconds = difftime(timer, mktime(&y2k));
    //time_t fm->sessionGetAccessTime() const { return this->access_time; };
    this->destroy(); // dima: for now just clear whole cache
}

void SessionCache::destroy() {
    while (this->cache.size() > 0) {
        FormatManager *fm = this->cache.back();
        this->cache.pop_back();
        fm->sessionEnd();
        delete fm;
    }
}

//--------------------------------------------------------------------------------------
// FormatManagerCacher
//--------------------------------------------------------------------------------------

FormatManagerCacher::FormatManagerCacher(const std::string &filename, SessionCache *cache_p, XConf *c) {
    this->filename = filename;
    this->cache = (bim::SessionCache *)cache_p;
    if (this->cache && this->filename.size() > 0 && this->cache->cached(this->filename)) {
        this->fm = cache->pop(this->filename);
    }
    if (this->fm) {
        this->fm->setConfiguration(c);
    } else {
        this->fm = new FormatManager(c);
    }
    // dima: do not keep things in cache while operating on them
    //if (cache)
    //    cache->push(this->fm); // add to latest access position
}

FormatManagerCacher::~FormatManagerCacher() {
    //this->destroy_forgotten();
    if (this->cache) {
        this->remember(); // store cache at the very end of the operation
    } else {
        this->destroy();
    }
}

void FormatManagerCacher::forget() { // remove from cache and destory FM
    if (!this->fm) return;
    if (this->cache) {
        FormatManager *p = cache->pop(this->filename);
    }
    this->destroy();
}

void FormatManagerCacher::remember() { // store to cache
    if (!this->fm) return;
    if (!this->cache) return;
    FormatManager *p = cache->pop(this->filename); // remove from whatever position it's in the cache
    this->fm->setConfiguration(NULL);
    this->cache->push(this->fm); // add to latest access position
}


void FormatManagerCacher::destroy_forgotten() { // destroy FM if not in cache
    if (this->cache && cache->cached(this->filename) == true) return;
    this->destroy();
}

void FormatManagerCacher::destroy() { // destroy FM
    if (this->fm == NULL) return;
    if (this->cache && cache->cached(this->filename) == true) {
        cache->pop(this->filename); // remove from whatever position it's in the cache
    }
    delete this->fm;
    this->fm = NULL;
}

void FormatManagerCacher::clear() { // clear FM pointer without destroing FM
    this->fm = NULL;
}
