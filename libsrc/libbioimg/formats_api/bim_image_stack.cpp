/*******************************************************************************

  Defines Image Stack Class

  Programmer: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  History:
    03/23/2004 18:03 - First creation

  ver: 1

*******************************************************************************/

#include <cmath>
#include <cstring>

#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "bim_image_stack.h"

#include <bim_metatags.h>
#include <bim_format_manager.h>
#include <xstring.h>
#include <xtypes.h>

using namespace bim;

//------------------------------------------------------------------------------
// ImageStack
//------------------------------------------------------------------------------

ImageStack::ImageStack() {
    init();
}

ImageStack::~ImageStack() {
    free();
}

ImageStack::ImageStack(const char *fileName, uint64 limit_width, uint64 limit_height, int64 only_channel, XConf *c) {
    init();
    fromFile(fileName, limit_width, limit_height, only_channel, 0, c);
}

ImageStack::ImageStack(const std::string &fileName, uint64 limit_width, uint64 limit_height, int64 only_channel, XConf *c) {
    init();
    fromFile(fileName, limit_width, limit_height, only_channel, 0, c);
}

ImageStack::ImageStack(const std::vector<xstring> &files, uint64 number_channels, const xoperations *ops, XConf *c) {
    init();
    fromFileList(files, number_channels, ops, c);
}

void ImageStack::free() {
    images.clear();
}

void ImageStack::init() {
    cur_position = 0;
    handling_image = false;
    progress_proc = NULL;
    error_proc = NULL;
    test_abort_proc = NULL;
}

ImageStack ImageStack::deepCopy() const {
    ImageStack stack = *this;
    for (size_t i = 0; i < images.size(); ++i)
        stack.images[i] = images[i].deepCopy();
    return stack;
}

bool ImageStack::isEmpty() const {
    if (images.size() == 0 || handling_image) return true;
    return imageAt(0)->isNull();
}

Image *ImageStack::imageAt(const uint64 position) const {
    if ((position >= images.size()) || handling_image)
        return (Image *)&empty_image;
    return (Image *)&images.at(position);
}

Image *ImageStack::image() const {
    if (handling_image) return (Image *)&empty_image;
    return imageAt(cur_position);
}

bool ImageStack::positionNext() {
    if (handling_image) return false;
    if (cur_position + 1 < images.size()) {
        cur_position++;
        return true;
    }
    return false;
}

bool ImageStack::positionPrev() {
    if (handling_image) return false;
    if (cur_position > 0) {
        cur_position--;
        return true;
    }
    return false;
}

bool ImageStack::position0() {
    if (handling_image) return false;
    cur_position = 0;
    return true;
}

bool ImageStack::positionLast() {
    if (handling_image) return false;
    if (images.size() > 0) {
        cur_position = images.size() - 1;
        return true;
    }
    return false;
}

bool ImageStack::positionSet(const uint64 l) {
    if (handling_image) return false;
    if (l >= images.size()) return false;
    cur_position = l;
    return true;
}

//------------------------------------------------------------------------------

bool ImageStack::fromFile(const char *fileName, const uint64 limit_width, const uint64 limit_height, const int64 channel, const xoperations *operations, XConf *c) {
    int res = 0;
    handling_image = true;
    images.clear();
    metadata.clear();
    cur_position = 0;
    FormatManager fm(c);

    if ((res = fm.sessionStartRead((bim::Filename)fileName)) == 0) {

        int pages = fm.sessionGetNumberOfPages();
        for (int page = 0; page < pages; ++page) {
            do_progress(page + 1, pages, "Loading stack");
            if (progress_abort()) break;

            // load page image, needs new clear image due to memory sharing
            Image img;
            if (fm.sessionReadImage(img.imageBitmap(), page) != 0) break;

            // use channel constraint
            if (channel >= 0)
                img.extractChannel(channel);

            // use size limits
            if ((limit_width > 0 || limit_height > 0) && (limit_width < img.width() || limit_height < img.height()))
                img = img.resample(limit_width, limit_height, Image::ResizeMethod::szBiCubic, true);

            if (operations) {
                img = img.ensureTypedDepth();
                img = img.ensureColorSpace();
                img.process(*operations);
            }

            images.push_back(img);

            if (page == 0) {
                fm.sessionParseMetaData(0);
                metadata = fm.get_metadata();
            }
        }
    }
    fm.sessionEnd();

    handling_image = false;
    return (res == 0);
}

bool ImageStack::toFile(const char *fileName, const char *formatName, const char *options) {
    FormatManager fm;
    if (!fm.isFormatSupportsWMP(formatName)) return false;

    this->metadata.set_value("image_num_z", static_cast<unsigned>(numberPlanes()));;
    this->metadata.set_value("image_num_t", 1);

    handling_image = true;
    int pages = (int)images.size();
    int res = 1;
    if ((res = fm.sessionStartWrite((bim::Filename)fileName, formatName, options)) == 0) {
        fm.sessionWriteSetMetadata(this->metadata);
        images[0].imageBitmap()->i.number_z = numberPlanes();
        images[0].imageBitmap()->i.number_t = 1;
        for (int page = 0; page < pages; ++page) {
            do_progress(page + 1, pages, "Writing stack");
            if (progress_abort()) break;
            fm.sessionWriteImage(images[page].imageBitmap(), page);
        }
    }
    fm.sessionEnd();

    handling_image = false;
    return (res == 0);
}

bool ImageStack::fromFileList(const std::vector<xstring> &files, uint64 number_channels, const xoperations *operations, XConf *c) {
    if (files.size() < 2)
        return this->fromFile(files[0], 0, 0, -1, operations, c);

    int res = 0;
    handling_image = true;
    images.clear();
    metadata.clear();
    cur_position = 0;

    size_t pages = files.size();
    size_t page = 0;
    while (page < pages) {
        do_progress(page + 1, pages, "Loading stack");
        if (progress_abort()) break;

        Image img(files[page], 0, c);
        if (img.isEmpty()) break;

        if (number_channels > 1)
            for (uint64 channel = 0; channel < number_channels; ++channel) {
                ++page;
                Image img_c(files[page], 0, c);
                img = img.appendChannels(img_c);
            }

        if (operations) {
            img = img.ensureTypedDepth();
            img = img.ensureColorSpace();
            img.process(*operations);
        }

        images.push_back(img);

        if (page == 0) {
            metadata = img.get_metadata();
            if (number_channels > 1) {
                //img.delete_metadata_tag(xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), 0));
                //img.delete_metadata_tag(xstring::xprintf(bim::CHANNEL_NAME_TEMPLATE.c_str(), 0));

                bim::xstring path = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), 0);
                img.delete_metadata_tag(path + bim::CHANNEL_INFO_NAME);
                img.delete_metadata_tag(path + bim::CHANNEL_INFO_COLOR);
            }
        }
        ++page;
    }

    handling_image = false;
    return (res == 0);
}

bool ImageStack::fromFileManager(FormatManager *m, const std::vector<uint> &pages) {
    int res = 0;
    handling_image = true;
    images.clear();
    metadata.clear();
    cur_position = 0;
    if (!m->sessionIsReading()) return false;
    uint number_pages = m->sessionGetNumberOfPages();

    for (uint i = 0; i < pages.size(); ++i) {
        uint page = pages[i];
        if (page >= number_pages) continue;

#pragma omp master
        do_progress(i + 1, pages.size(), "Loading stack");
        if (progress_abort()) break;

        // load page image, needs new clear image due to memory sharing
        Image img;
        if (m->sessionReadImage(img.imageBitmap(), page) != 0) break;
        images.push_back(img);
    }

    handling_image = false;
    return (res == 0);
}

double ImageStack::bytesInStack() const {
    double bytes = 0;
    if (isEmpty()) return bytes;
    for (const Image &image : images)
        bytes += image.bytesInImage();
    return bytes;
}

//------------------------------------------------------------------------------

void ImageStack::convertToDepth(uint64 depth, Lut::LutType method, bool planes_independent, DataFormat pxtype) {

    if (!planes_independent) {
        StackHistogram stack_histogram(*this);
        if (pxtype == bim::DataFormat::FMT_UNDEFINED) pxtype = this->pixelType();
        ImageHistogram out(this->samples(), depth, pxtype);
        ImageLut lut(stack_histogram, out, method);
        convertToDepth(lut);
    } else {
        for (size_t i = 0; i < images.size(); ++i) {
            do_progress(i, images.size(), "Converting depth");
            images[i] = images[i].convertToDepth(depth, method, pxtype);
        }
    }
}

void ImageStack::convertToDepth(const ImageLut &lut) {
    for (size_t i = 0; i < images.size(); ++i) {
        do_progress(i, images.size(), "Converting depth");
        images[i] = images[i].convertToDepth(lut);
    }
}

void ImageStack::normalize(uint64 to_bpp, bool planes_independent) {
    this->convertToDepth(to_bpp, Lut::ltLinearDataRange, planes_independent, bim::DataFormat::FMT_UNSIGNED);
}

void ImageStack::ensureTypedDepth() {
    for (Image &image : images)
        image = image.ensureTypedDepth();
}

void ImageStack::ensureColorSpace() {
    for (Image &image : images)
        image = image.ensureColorSpace();
}

//------------------------------------------------------------------------------

void ImageStack::process(const xoperations &operations, ImageHistogram *_hist, XConf *c) {
    XConf cc;
    if (!c) c = &cc;
    for (Image &image : images) {
        image.process(operations, _hist, &cc);
    }
}

//------------------------------------------------------------------------------

void ImageStack::negative() {
    for (Image &image : images)
        image = image.negative();
}

/*
void ImageStack::discardLut() {
    for (Image &image : images)
        image.discardLut();
}
*/

void ImageStack::rotate(double deg) {
    for (size_t i = 0; i < images.size(); ++i) {
        do_progress(i, images.size(), "Rotate");
        images[i] = images[i].rotate(deg);
    }
}

void ImageStack::remapChannels(const std::vector<int> &mapping) {
    for (Image &image : images)
        image.remapChannels(mapping);
}

//------------------------------------------------------------------------------

inline double ensure_range(double a, double minv, double maxv) {
    if (a < minv)
        a = minv;
    else if (a > maxv)
        a = maxv;
    return a;
}

void ImageStack::ROI(uint64 x, uint64 y, uint64 z, uint64 w, uint64 h, uint64 d) {

    if (isEmpty()) return;
    if (x == 0 && y == 0 && z == 0 && width() == w && height() == h && numberPlanes() == d) return;

    if (x >= width()) x = 0;
    if (y >= height()) y = 0;
    if (z >= numberPlanes()) z = 0;
    if (w <= 0) w = width();
    if (h <= 0) h = height();
    if (w + x > width()) w = width() - x;
    if (h + y > height()) h = height() - y;
    if (d + z > numberPlanes()) d = numberPlanes() - z;

    // first get rid of unused planes
    if (d + z > 0 && d + z < numberPlanes())
        images.erase(images.begin() + (d + z), images.end());
    if (z > 0)
        images.erase(images.begin(), images.begin() + (z - 1));

    // then run ROI on each plane
    if (x == 0 && y == 0 && width() == w && height() == h) return;
    for (size_t i = 0; i < images.size(); ++i) {
        do_progress(i, images.size(), "ROI");
        images[i] = images[i].ROI(x, y, w, h);
    }
}

ImageStack ImageStack::deepCopy(double x, double y, double z, double w, double h, double d) const {
    if (isEmpty()) return ImageStack();
    if (x == 0 && y == 0 && z == 0 && width() == w && height() == h && numberPlanes() == d) return this->deepCopy();

    const uint64 ix = bim::round<uint64>(ensure_range(x, 0.0, width() - 1.0));
    const uint64 iw = bim::round<uint64>(ensure_range(w, 0.0, width() - x));
    const uint64 iy = bim::round<uint64>(ensure_range(y, 0.0, height() - 1.0));
    const uint64 ih = bim::round<uint64>(ensure_range(h, 0.0, height() - y));
    const uint64 iz = bim::round<uint64>(ensure_range(z, 0.0, numberPlanes() - 1.0));
    const uint64 id = bim::round<uint64>(ensure_range(d, 0.0, numberPlanes() - z));

    ImageStack stack;
    for (uint64 i = iz; i < iz + id; ++i)
        stack.images.push_back(images[i].ROI(ix, iy, iw, ih));
    stack.metadata = this->metadata;
    return stack;
}

//------------------------------------------------------------------------------

Image ImageStack::projectionXZAxis(uint64 y) const {
    if (y >= height()) return Image();

    const uint64 w = width();
    const uint64 h = numberPlanes();
    const uint64 s = samples();
    const uint64 d = depth();
    Image img;
    if (img.alloc(w, h, s, d, this->pixelType()) != 0) return img;

    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (numberPlanes()>BIM_OMP_FOR2)
    for (uint64 z = 0; z < numberPlanes(); ++z) {
        for (uint64 c = 0; c < samples(); ++c) {
            unsigned char *line_in = images[z].scanLine(c, y);
            unsigned char *line_out = img.scanLine(c, z);
            memcpy(line_out, line_in, img.bytesPerLine());
        } // for c
    }     // for z

    return img;
}

Image ImageStack::projectionYZAxis(uint64 x) const {
    if (x >= width()) return Image();

    const uint64 w = height();
    const uint64 h = numberPlanes();
    const uint64 s = samples();
    const uint64 d = depth();
    Image img;
    if (img.alloc(w, h, s, d, this->pixelType()) != 0) return img;

    #pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (numberPlanes() > BIM_OMP_FOR2)
    for (bim::int64 z = 0; z < (bim::int64)numberPlanes(); ++z) {
        std::vector<unsigned char> row_buf(img.bytesPerLine());
        for (uint64 c = 0; c < samples(); ++c) {
            images[z].scanRow(c, x, &row_buf[0]);
            unsigned char *line_out = img.scanLine(c, z);
            memcpy(line_out, &row_buf[0], img.bytesPerLine());
        } // for c
    }     // for z

    return img;
}

TagMap metadataRearrange3D(const TagMap &md, const ImageStack *stack, const ImageStack::RearrangeDimensions &operation) {
    TagMap metadata = md;

    unsigned int sz_x = metadata.get_value_unsigned("image_num_x", 0);
    unsigned int sz_y = metadata.get_value_unsigned("image_num_y", 0);
    double res_x = metadata.get_value_double("pixel_resolution_x", 0);
    double res_y = metadata.get_value_double("pixel_resolution_y", 0);
    xstring units_x = metadata.get_value("pixel_resolution_unit_x");
    xstring units_y = metadata.get_value("pixel_resolution_unit_y");

    xstring image_num_z, pixel_resolution_z, pixel_resolution_unit_z;
    xstring image_num_t, pixel_resolution_t, pixel_resolution_unit_t;

    if (metadata.get_value_unsigned("image_num_z", 1) > 1) { // image is a Z stack
        image_num_z = "image_num_z";
        pixel_resolution_z = "pixel_resolution_z";
        pixel_resolution_unit_z = "pixel_resolution_unit_z";
        image_num_t = "image_num_t";
        pixel_resolution_t = "pixel_resolution_t";
        pixel_resolution_unit_t = "pixel_resolution_unit_t";
    } else if (metadata.get_value_unsigned("image_num_t", 1) > 1) { // image is a T series
        image_num_z = "image_num_t";
        pixel_resolution_z = "pixel_resolution_t";
        pixel_resolution_unit_z = "pixel_resolution_unit_t";
        image_num_t = "image_num_z";
        pixel_resolution_t = "pixel_resolution_z";
        pixel_resolution_unit_t = "pixel_resolution_unit_z";
    }

    if (image_num_z.size() <= 1) return metadata;

    unsigned int sz_z = metadata.get_value_unsigned(image_num_z, 1);
    double res_z = metadata.get_value_double(pixel_resolution_z, 0);
    xstring units_z = metadata.get_value(pixel_resolution_unit_z);

    if (operation == ImageStack::adXZY) { // XYZ -> XZY
        metadata.set_value("image_num_x", sz_x);
        metadata.set_value("image_num_y", sz_z);
        metadata.set_value(image_num_z, sz_y);
        metadata.set_value(image_num_t, 1);
        metadata.set_value("pixel_resolution_x", res_x);
        metadata.set_value("pixel_resolution_y", res_z);
        metadata.set_value(pixel_resolution_z, res_y);
        metadata.set_value(pixel_resolution_t, 0);
        metadata.set_value("pixel_resolution_unit_x", units_x);
        metadata.set_value("pixel_resolution_unit_y", units_z);
        metadata.set_value(pixel_resolution_unit_z, units_y);
        metadata.set_value(pixel_resolution_unit_t, "");
    } else if (operation == ImageStack::adYZX) { // XYZ -> YZX, rotated -90, need to counteract
        metadata.set_value("image_num_x", sz_z);
        metadata.set_value("image_num_y", sz_y);
        metadata.set_value(image_num_z, sz_x);
        metadata.set_value(image_num_t, 1);
        metadata.set_value("pixel_resolution_x", res_z); // set reverse to counteract rotation
        metadata.set_value("pixel_resolution_y", res_y); // set reverse to counteract rotation
        metadata.set_value(pixel_resolution_z, res_x);   // set reverse to counteract rotation
        metadata.set_value(pixel_resolution_t, 0);
        metadata.set_value("pixel_resolution_unit_x", units_z); // set reverse to counteract rotation
        metadata.set_value("pixel_resolution_unit_y", units_y); // set reverse to counteract rotation
        metadata.set_value(pixel_resolution_unit_z, units_x);   // set reverse to counteract rotation
        metadata.set_value(pixel_resolution_unit_t, "");
    }

    return metadata;
}

bool ImageStack::rearrange3DToFile(const RearrangeDimensions &operation, const char *fileName, const char *formatName, const char *options) const {
    uint64 psize = operation == ImageStack::adXZY ? this->height() : this->width();

    FormatManager fm;
    if (!fm.isFormatSupportsWMP(formatName)) return false;
    if (fm.sessionStartWrite((bim::Filename)fileName, formatName, options) == 0) {

        TagMap meta = metadataRearrange3D(this->metadata, this, operation);
        fm.sessionWriteSetMetadata(meta);

        for (uint64 p = 0; p < psize; ++p) {
            Image image;
            if (operation == ImageStack::adXZY)
                image = this->projectionXZAxis(p); // XYZ -> XZY
            else if (operation == ImageStack::adYZX) {
                image = this->projectionYZAxis(p); // XYZ -> YZX
                image = image.rotate(-90);
            }
            // this part should probably be removed in the main library
            bim::ImageInfo *i = &image.imageBitmap()->i;
            i->number_pages = psize;
            i->number_z = meta.get_value_int("image_num_z", 1);
            i->number_t = meta.get_value_int("image_num_t", 1);
            fm.sessionWriteImage(image.imageBitmap(), (bim::uint)p);
        }
        fm.sessionEnd();
    } else {
        fm.sessionEnd();
        return false;
    }
    return true;
}


TagMap resizeMetadata3d(const TagMap &md, unsigned int w_to, unsigned int h_to, unsigned int d_to, unsigned int w_in, unsigned int h_in, unsigned int d_in) {
    TagMap metadata = md;

    metadata.set_value("image_num_z", d_to);

    if (metadata.hasKey("pixel_resolution_x")) {
        double new_res = metadata.get_value_double("pixel_resolution_x", 0) * ((double)w_in / (double)w_to);
        metadata.set_value("pixel_resolution_x", new_res);
    }
    if (metadata.hasKey("pixel_resolution_y")) {
        double new_res = metadata.get_value_double("pixel_resolution_y", 0) * ((double)h_in / (double)h_to);
        metadata.set_value("pixel_resolution_y", new_res);
    }
    if (metadata.hasKey("pixel_resolution_z")) {
        double new_res = metadata.get_value_double("pixel_resolution_z", 0) * ((double)d_in / (double)d_to);
        metadata.set_value("pixel_resolution_z", new_res);
    }
    return metadata;
}

void ImageStack::resize(uint64 w, uint64 h, uint64 d, Image::ResizeMethod method, bool keep_aspect_ratio) {
    const uint64 wor = images[0].width();
    const uint64 hor = images[0].height();
    const uint64 dor = numberPlanes();

    // first interpolate X,Y
    if ((w != 0 || h != 0) && (w != width() || h != height()))
        for (uint64 i = 0; i < images.size(); ++i) {
#pragma omp master
            do_progress(i, images.size(), "Interpolating X/Y");
            images[i] = images[i].resize(w, h, method, keep_aspect_ratio);
        }

    w = images[0].width();
    h = images[0].height();
    if (d == 0)
        d = bim::round<uint64>(numberPlanes() / (wor / (float)w));

    // then interpolate Z
    if (d == 0 || d == numberPlanes()) return;

    // create stack with corect size
    ImageStack stack = *this;
    stack.images.clear();
    for (uint64 z = 0; z < d; ++z)
        stack.append(images[0].deepCopy());

    // now interpolate Z
    for (uint64 y = 0; y < stack.height(); ++y) {
#pragma omp master
        do_progress(y, stack.height(), "Interpolating Z");
        Image img_xz = projectionXZAxis(y);
        img_xz = img_xz.resize(w, d, method, false);

        //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (d>BIM_OMP_FOR2)
        for (uint64 z = 0; z < d; ++z) {
            for (uint64 c = 0; c < stack.samples(); ++c) {
                unsigned char *line_out = stack.images[z].scanLine(c, y);
                unsigned char *line_in = img_xz.scanLine(c, z);
                memcpy(line_out, line_in, img_xz.bytesPerLine());
            } // for c
        }     // for z
    }         // for y

    stack.metadata = resizeMetadata3d(this->metadata, (bim::uint)w, (bim::uint)h, (bim::uint)d, (bim::uint)wor, (bim::uint)hor, (bim::uint)dor);
    *this = stack;
}

void ImageStack::setNumberPlanes(const uint64 n) {
    if (n == numberPlanes()) return;
    if (n < numberPlanes() || numberPlanes() == 0) {
        images.resize(n);
        return;
    }

    int64 d = static_cast<int64>(n) - images.size();
    Image l = images[images.size() - 1];

    for (int64 z = 0; z < d; ++z)
        this->append(l.deepCopy());
}

Image ImageStack::pixelArithmeticMax() const {
    Image p = images[0].deepCopy();
    for (size_t z = 1; z < images.size(); ++z)
        p.imageArithmetic(images[z], Image::ArithmeticOperators::aoMax);
    return p;
}

Image ImageStack::pixelArithmeticMin() const {
    Image p = images[0].deepCopy();
    for (size_t z = 1; z < images.size(); ++z)
        p.imageArithmetic(images[z], Image::ArithmeticOperators::aoMin);
    return p;
}

Image ImageStack::textureAtlas(const xstring &arguments) const {
    int rows = 0, cols = 0;
    std::vector<int> vs = arguments.splitInt(",");
    if (vs.size() > 1) {
        rows = vs[0];
        cols = vs[1];
    }
    return this->textureAtlas(rows, cols);
}

Image ImageStack::textureAtlas(uint64 rows, uint64 cols) const {
    uint64 w = images[0].width();
    uint64 h = images[0].height();
    uint64 n = images.size();
    uint64 ww = 0, hh = 0;

    if (rows == 0 || cols == 0) {
        // start with atlas composed of a row of images
        ww = w * n;
        hh = h;
        double ratio = ww / (double)hh;
        // optimize side to be as close to ratio of 1.0
        for (size_t r = 2; r < images.size(); ++r) {
            double ipr = ceil(n / (double)r);
            double aw = (double)w * ipr;
            double ah = (double)h * r;
            double rr = bim::max<double>(aw, ah) / bim::min<double>(aw, ah);
            if (rr < ratio) {
                ratio = rr;
                ww = (bim::uint64)aw;
                hh = (bim::uint64)ah;
            } else
                break;
        }

        // compose atlas image
        rows = ww / w;
        cols = hh / h;
    } else {
        ww = rows * w;
        hh = cols * h;
    }

    Image atlas(ww, hh, images[0].depth(), images[0].samples(), images[0].pixelType());
    atlas.fill(0);
    uint64 i = 0;
    uint64 y = 0;
    for (uint64 r = 0; r < cols; ++r) {
        uint64 x = 0;
        for (uint64 c = 0; c < rows; ++c) {
            if (i < images.size())
                atlas.setROI(x, y, images[i]);
            x += w;
            i++;
        }
        y += h;
    }
    return atlas;
}

//------------------------------------------------------------------------------
// StackHistogram
//------------------------------------------------------------------------------

StackHistogram::StackHistogram() {
}

StackHistogram::~StackHistogram() {
}

void StackHistogram::fromImageStack(const ImageStack &stack) {
    histograms.clear();
    histograms.resize(stack.samples());

    // iterate first to get stats - slower but will not fail on float data
    for (uint64 c = 0; c < stack.samples(); ++c) {
        histograms[c].init(stack.depth(), stack.pixelType());
        for (uint64 i = 0; i < stack.numberPlanes(); ++i)
            histograms[c].updateStats(stack[i]->bits(c), stack[i]->numPixels());
    } // channels

    // iterate first to create histograms
    for (uint64 c = 0; c < stack.samples(); ++c) {
        for (uint64 i = 0; i < stack.numberPlanes(); ++i)
            histograms[c].addData(stack[i]->bits(c), stack[i]->numPixels());
    } // channels
}

void StackHistogram::fromImageStack(const ImageStack &stack, const Image *mask) {
    if (mask && mask->depth() != 8) return;
    if (mask && mask->samples() > 1 && mask->samples() < stack.samples()) return;
    histograms.clear();
    histograms.resize(stack.samples());

    // iterate first to get stats - slower but will not fail on float data
    for (uint64 c = 0; c < stack.samples(); ++c) {
        histograms[c].init(stack.depth(), stack.pixelType());
        for (uint64 i = 0; i < stack.numberPlanes(); ++i)
            histograms[c].updateStats(stack[i]->bits(c), stack[i]->numPixels(), (unsigned char *)mask->bits(c));
    } // channels

    // iterate first to create histograms
    for (uint64 c = 0; c < stack.samples(); ++c) {
        for (uint64 i = 0; i < stack.numberPlanes(); ++i)
            histograms[c].addData(stack[i]->bits(c), stack[i]->numPixels(), (unsigned char *)mask->bits(c));
    } // channels
}

void StackHistogram::fromImageStack(const ImageStack &stack, const ImageStack *mask) {
    if (mask && mask->depth() != 8) return;
    if (mask && mask->samples() > 1 && mask->samples() < stack.samples()) return;
    if (mask && mask->numberPlanes() < stack.numberPlanes()) return;
    histograms.clear();
    histograms.resize(stack.samples());

    // iterate first to get stats - slower but will not fail on float data
    for (uint64 c = 0; c < stack.samples(); ++c) {
        histograms[c].init(stack.depth(), stack.pixelType());
        for (uint64 i = 0; i < stack.numberPlanes(); ++i)
            histograms[c].updateStats(stack[i]->bits(c), stack[i]->numPixels(), (unsigned char *)mask->imageAt(i)->bits(c));
    } // channels

    // iterate first to create histograms
    for (uint64 c = 0; c < stack.samples(); ++c) {
        for (uint64 i = 0; i < stack.numberPlanes(); ++i)
            histograms[c].addData(stack[i]->bits(c), stack[i]->numPixels(), (unsigned char *)mask->imageAt(i)->bits(c));
    } // channels
}
