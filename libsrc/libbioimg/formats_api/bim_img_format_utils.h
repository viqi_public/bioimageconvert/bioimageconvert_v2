/*******************************************************************************

  Defines Image Format Utilities
  rely on: DimFiSDK version: 1

  Programmer: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  Image file structure:

    1) Page:   each file may contain 1 or more pages, each page is independent

    2) Sample: in each page each pixel can contain 1 or more samples
               preferred number of samples: 1 (GRAY), 3(xRGB) and 4(RGBA)

    3) Depth:  each sample can be of 1 or more bits of depth
                preferred depths are: 8 and 16 bits per sample

    4) Allocation: each sample is allocated having in mind:
               All lines are stored contiguasly from top to bottom where
               having each particular line is byte alligned, i.e.:
               each line is allocated using minimal necessary number of bytes
               to store given depth. Which for the image means:
               size = ceil( ( (width*depth) / 8 ) * height )

  As a result of Sample/Depth structure we can get images of different
  Bits per Pixel as: 1, 8, 24, 32, 48 and any other if needed

  History:
    04/08/2004 11:57 - First creation

  ver: 2

*******************************************************************************/

#ifndef BIM_IMG_FMT_UTL_H
#define BIM_IMG_FMT_UTL_H

#include <string>

#include "bim_img_format_interface.h"

//------------------------------------------------------------------------------
// Safe calls for memory/io prototypes, if they are not supplied then
// standard functions are used
//------------------------------------------------------------------------------

namespace bim {

class TagMap;
class xstring;

void *xmalloc(FormatHandle *fmtHndl, BIM_SIZE_T size);
inline void *xmalloc(BIM_SIZE_T size) {
    return (void *)new char[size];
}

void *xfree(FormatHandle *fmtHndl, void *p);
// overload of dimin free to make a safe delete
inline void xfree(void **p) {
    if (*p == NULL) return;
    delete[] (unsigned char *)*p;
    *p = NULL;
}

void xprogress(FormatHandle *fmtHndl, uint64 done, uint64 total, const char *descr);
void xerror(FormatHandle *fmtHndl, int val, const char *descr);
int xtestAbort(FormatHandle *fmtHndl);

// the stream is specified by FormatHandle
void xopen(FormatHandle *fmtHndl);
int xclose(FormatHandle *fmtHndl);

BIM_SIZE_T xread(FormatHandle *fmtHndl, void *buffer, BIM_SIZE_T size, BIM_SIZE_T count);
BIM_SIZE_T xwrite(FormatHandle *fmtHndl, void *buffer, BIM_SIZE_T size, BIM_SIZE_T count);
int xflush(FormatHandle *fmtHndl);
int xseek(FormatHandle *fmtHndl, BIM_OFFSET_T offset, int origin);
BIM_SIZE_T xsize(FormatHandle *fmtHndl);
BIM_OFFSET_T xtell(FormatHandle *fmtHndl);
int xeof(FormatHandle *fmtHndl);

//------------------------------------------------------------------------------
// file utils
//------------------------------------------------------------------------------

xstring fspath_fix_slashes(const xstring &path);
xstring fspath_get_path(const xstring &filename);
xstring fspath_get_filename(const xstring &filename);
xstring fspath_replace(const xstring &filepath, const xstring &filename);
xstring fspath_join(const xstring &filepath, const xstring &filename);

//------------------------------------------------------------------------------
// common parsing
//------------------------------------------------------------------------------

// parse objective string and fill in proper tags into TagMap
void parse_objective_from_string(const bim::xstring &objective, bim::TagMap *hash);

double objective_parse_magnification(const bim::xstring &s);
double objective_parse_num_aperture(const bim::xstring &s);

// 300 ms
void parse_exposure_from_string(const bim::xstring &str, const bim::xstring &path, bim::TagMap *hash);

//IN - 60 um pinhole (Running)
void parse_pinhole_from_string(const bim::xstring &str, const bim::xstring &path, bim::TagMap *hash);

//------------------------------------------------------------------------------
// tests for provided callbacks
//------------------------------------------------------------------------------
bool isCustomReading(FormatHandle *fmtHndl);
bool isCustomWriting(FormatHandle *fmtHndl);

//------------------------------------------------------------------------------------------------
// misc
//------------------------------------------------------------------------------------------------

FormatHandle initFormatHandle();
//ImageInfo initImageInfo();

//------------------------------------------------------------------------------------------------
// swap
//------------------------------------------------------------------------------------------------
void swapData(bim::DataType type, uint64 size, void *data);

//------------------------------------------------------------------------------------------------
// ImageBitmap
//------------------------------------------------------------------------------------------------

// you must call this function once declared image var
void initImagePlanes(ImageBitmap *bmp);

int allocImg(ImageBitmap *img, bim::uint64 w, bim::uint64 h, bim::uint64 samples, bim::uint64 depth, DataFormat format, ImageModes imageMode = ImageModes::IM_UNKNOWN);
// alloc image using w,h,s,d
int allocImg(FormatHandle *fmtHndl, ImageBitmap *img, bim::uint64 w, bim::uint64 h, bim::uint64 samples, bim::uint64 depth, DataFormat format, ImageModes imageMode = ImageModes::IM_UNKNOWN);
// alloc image using info
int allocImg(FormatHandle *fmtHndl, ImageInfo *info, ImageBitmap *img);
// alloc handle image using info
int allocImg(FormatHandle *fmtHndl, ImageInfo *info);

int init_from_external(ImageBitmap *img, void *data, bim::uint64 w, bim::uint64 h, bim::uint64 samples, bim::uint64 depth, bim::uint64 stride, DataFormat format, ImageModes imageMode);

void deleteImg(ImageBitmap *img);
void deleteImg(FormatHandle *fmtHndl, ImageBitmap *img);

uint64 getLineSizeInBytes(ImageBitmap *img);
uint64 getImgSizeInBytes(ImageBitmap *img);
uint getImgNumColors(ImageBitmap *img);

std::string getImageInfoText(ImageInfo *info);

} // namespace bim

#endif //BIM_IMG_FMT_UTL_H
