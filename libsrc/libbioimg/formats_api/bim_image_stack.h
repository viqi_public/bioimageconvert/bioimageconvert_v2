/*******************************************************************************

  Defines Image Stack Class

  Programmer: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  History:
    03/23/2004 18:03 - First creation

  ver: 1

*******************************************************************************/

#ifndef BIM_IMAGE_STACK_H
#define BIM_IMAGE_STACK_H

#include <map>
#include <vector>

#include "bim_histogram.h"
#include "bim_image.h"
#include "tag_map.h"

namespace bim {

class FormatManager;
class xstring;

//------------------------------------------------------------------------------
// ImageStack
//------------------------------------------------------------------------------

class ImageStack {
public:
    enum RearrangeDimensions {
        adNone = 0,
        adXZY = 1,
        adYZX = 2,
    };

    ImageStack();
    ImageStack(const char *fileName, uint64 limit_width = 0, uint64 limit_height = 0, int64 only_channel = -1, XConf *c = NULL);
    ImageStack(const std::string &fileName, uint64 limit_width = 0, uint64 limit_height = 0, int64 only_channel = -1, XConf *c = NULL);
    ImageStack(const std::vector<xstring> &files, uint64 number_channels = 0, const xoperations *ops = 0, XConf *c = NULL);
    ~ImageStack();

    void free();
    void clear() { free(); }
    ImageStack deepCopy() const;
    ImageStack deepCopy(double x, double y, double z, double w = 0, double h = 0, double d = 0) const;

    bool isReady() const { return !handling_image; } // true if images can be used
    bool isEmpty() const;

    uint64 numberPlanes() const { return images.size(); }
    void setNumberPlanes(uint64 p);
    bool planeInRange(int p) const {
        if (p < 0) return false;
        if (p >= this->numberPlanes()) return false;
        return true;
    }

    double bytesInStack() const;

    uint64 width() const { return numberPlanes() == 0 ? 0 : images[0].width(); }
    uint64 height() const { return numberPlanes() == 0 ? 0 : images[0].height(); }
    uint64 depth() const { return numberPlanes() == 0 ? 0 : images[0].depth(); }
    uint64 samples() const { return numberPlanes() == 0 ? 0 : images[0].samples(); }
    DataFormat pixelType() const { return numberPlanes() <= 0 ? bim::DataFormat::FMT_UNSIGNED : images[0].pixelType(); }

    Image *imageAt(uint64 position) const;
    Image *image() const; // image at the currently selected position
    Image *operator[](uint64 p) const { return imageAt(p); }

    void append(Image img) { images.push_back(std::move(img)); }

    // return true if the level exists
    bool positionPrev();
    bool positionNext();
    bool position0();
    bool positionFirst() { return position0(); }
    bool positionLast();
    bool positionSet(uint64 l);
    uint64 positionCurrent() const { return cur_position; }

    const bim::TagMap *meta() const { return &metadata; }
    const bim::TagMap &get_metadata() const { return metadata; }
    std::string get_metadata_tag(const std::string &key, const std::string &def) const { return metadata.get_value(key, def); }
    int get_metadata_tag_int(const std::string &key, const int def) const { return metadata.get_value_int(key, def); }
    double get_metadata_tag_double(const std::string &key, const double def) const { return metadata.get_value_double(key, def); }

    // I/O

    bool fromFile(const char *fileName, uint64 limit_width = 0, uint64 limit_height = 0, int64 channel = -1, const xoperations *ops = 0, XConf *c = NULL);
    bool fromFile(const std::string &fileName, uint64 limit_width = 0, uint64 limit_height = 0, int64 channel = -1, const xoperations *ops = 0, XConf *c = NULL) {
        return fromFile(fileName.c_str(), limit_width, limit_height, channel, ops, c);
    }

    // use number_channels>0 if channels are stored as separate files
    bool fromFileList(const std::vector<xstring> &files, uint64 number_channels = 0, const xoperations *ops = 0, XConf *c = NULL);

    bool fromFileManager(FormatManager *m, const std::vector<unsigned int> &pages);

    bool toFile(const char *fileName, const char *formatName, const char *options = NULL);
    bool toFile(const std::string &fileName, const std::string &formatName, const std::string &options = "") {
        return toFile(fileName.c_str(), formatName.c_str(), options.c_str());
    }

    // Operations
    void convertToDepth(uint64 depth, Lut::LutType method = Lut::ltLinearFullRange, bool planes_independent = false, DataFormat pxtype = bim::DataFormat::FMT_UNDEFINED);
    void convertToDepth(const ImageLut &);
    void normalize(uint64 to_bpp = 8, bool planes_independent = false);
    void ensureTypedDepth();
    void ensureColorSpace();

    void resize(uint64 w, uint64 h = 0, uint64 d = 0, Image::ResizeMethod method = Image::ResizeMethod::szNearestNeighbor, bool keep_aspect_ratio = false);

    void ROI(uint64 x, uint64 y, uint64 z, uint64 w, uint64 h, uint64 d);

    //--------------------------------------------------------------------------
    // process all images based on command line arguments or a string
    //--------------------------------------------------------------------------

    void process(const xoperations &operations, ImageHistogram *hist = 0, XConf *c = 0);

    // only available values now are +90, -90 and 180
    void rotate(double deg);

    void negative();
    //void discardLut();
    void remapChannels(const std::vector<int> &mapping);

    Image projectionXZAxis(uint64 y) const;
    Image projectionYZAxis(uint64 x) const;

    bool rearrange3DToFile(const RearrangeDimensions &operation, const char *fileName, const char *formatName, const char *options = NULL) const;
    bool rearrange3DToFile(const RearrangeDimensions &operation, const std::string &fileName, const std::string &formatName, const std::string &options = "") const {
        return this->rearrange3DToFile(operation, fileName.c_str(), formatName.c_str(), options.c_str());
    }

    Image pixelArithmeticMax() const;
    Image pixelArithmeticMin() const;

    Image textureAtlas(uint64 rows = 0, uint64 cols = 0) const;
    Image textureAtlas(const xstring &arguments) const;

protected:
    std::vector<Image> images;
    TagMap metadata;

    Image empty_image;
    uint64 cur_position;
    bool handling_image;

protected:
    void init();

    // callbacks
public:
    ProgressProc progress_proc;
    ErrorProc error_proc;
    TestAbortProc test_abort_proc;

protected:
    void do_progress(bim::uint64 done, bim::uint64 total, const char *descr) {
        if (progress_proc) progress_proc(done, total, descr);
    }
    bool progress_abort() {
        if (!test_abort_proc) return false;
        return (test_abort_proc() != 0);
    }
};


//------------------------------------------------------------------------------
// StackHistogram
//------------------------------------------------------------------------------

class StackHistogram : public ImageHistogram {
public:
    StackHistogram();
    StackHistogram(const ImageStack &stack) { fromImageStack(stack); }
    StackHistogram(const ImageStack &stack, const Image *mask) { fromImageStack(stack, mask); }
    StackHistogram(const ImageStack &stack, const ImageStack *mask) { fromImageStack(stack, mask); }
    ~StackHistogram();

    void fromImageStack(const ImageStack &stack);

    // mask has to be an 8bpp image where pixels >128 belong to the object of interest
    // if mask has 1 channel it's going to be used for all channels, otherwise they should match
    void fromImageStack(const ImageStack &, const Image *mask);

    // mask has to be an 8bpp image where pixels >128 belong to the object of interest
    // if mask has 1 channel it's going to be used for all channels, otherwise they should match
    void fromImageStack(const ImageStack &, const ImageStack *mask);
};

} // namespace bim

#endif //BIM_IMAGE_STACK_H
