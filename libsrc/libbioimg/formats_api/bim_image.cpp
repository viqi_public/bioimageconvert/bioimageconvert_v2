/*******************************************************************************

  Implementation of the Image Class, it uses smart pointer technology to implement memory
  sharing, simple cope operations simply point to the same memory addresses

  Author: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>
  Copyright (c) 2018, ViQi Inc

  Image file structure:

    1) Page:   each file may contain 1 or more pages, each page is independent

    2) Sample: in each page each pixel can contain 1 or more samples
               preferred number of samples: 1 (GRAY), 3(xRGB) and 4(RGBA)

    3) Depth:  each sample can be of 1 or more bits of depth
               preferred depths are: 8 and 16 bits per sample

    4) Allocation: each sample is allocated having in mind:
               All lines are stored contiguasly from top to bottom where
               having each particular line is byte alligned, i.e.:
               each line is allocated using minimal necessary number of bytes
               to store given depth. Which for the image means:
               size = ceil( ( (width*depth) / 8 ) * height )

  As a result of Sample/Depth structure we can get images of different
  Bits per Pixel as: 1, 8, 24, 32, 48 and any other if needed

  History:
    03/23/2004 18:03 - First creation

  ver: 1

*******************************************************************************/

#include <algorithm>
#include <fstream>
#include <iostream>
#include <limits>
#include <string>

#include <cmath>
#include <cstring>
#include <cassert>

//#include "bim_buffer.h"
#include "bim_histogram.h"
#include "bim_image.h"
#include "bim_image_5d.h"
#include "bim_image_proxy.h"
#include "bim_image_pyramid.h"
#include "bim_img_format_utils.h"
#include "bim_metatags.h"
#include "bim_primitives.h"
#include "xconf.h"
#include "xtypes.h"

//#include <blob_manager.h>

#ifdef BIM_USE_IMAGEMANAGER
#include <bim_format_manager.h>
#endif //BIM_USE_IMAGEMANAGER

#include "resize.h"
#include "rotate.h"

#include "typeize_buffer.h"

using namespace bim;

#ifdef BIM_USE_TRANSFORMS
Image operation_icc_load(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_icc_save(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_transform_icc_file(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_transform_icc_name(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
#endif //BIM_USE_TRANSFORMS

//------------------------------------------------------------------------------------
// Image defs
//------------------------------------------------------------------------------------

std::vector<ImgRefs *> Image::refs;
const Image::map_modifiers Image::modifiers = Image::create_modifiers();

Image::Image() {
    bmp = NULL;
    connectToNewMemory();
}

Image::~Image() {
    disconnectFromMemory();
}

Image::Image(const Image &img) {
    bmp = NULL;
    connectToMemory(img.bmp);
    if (img.metadata.size() > 0) metadata = img.metadata;
}

Image::Image(bim::uint64 width, bim::uint64 height, bim::uint64 depth, bim::uint64 samples, DataFormat format) {
    bmp = NULL;
    connectToNewMemory();
    create(width, height, depth, samples, format);
}

#ifdef BIM_USE_IMAGEMANAGER
Image::Image(const char *fileName, uint page, XConf *c) {
    this->bmp = NULL;
    this->connectToNewMemory();
    this->fromFile(fileName, page, c);
}

Image::Image(const std::string &fileName, uint page, XConf *c) {
    this->bmp = NULL;
    this->connectToNewMemory();
    this->fromFile(fileName, page, c);
}

Image::Image(const std::string &fileName, uint page, uint64 level, int64 tilex, int64 tiley, uint64 tilesize, XConf *c) {
    this->bmp = NULL;
    this->connectToNewMemory();
    this->fromPyramidFile(fileName, page, level, tilex, tiley, tilesize, c);
}

Image::Image(const std::string &fileName, uint page, uint64 level, uint64 x1, uint64 y1, uint64 x2, uint64 y2, XConf *c) {
    this->bmp = NULL;
    this->connectToNewMemory();
    this->fromPyramidRegion(fileName, page, level, x1, y1, x2, y2, c);
}

Image::Image(const std::string &fileName, uint page, double scale, uint64 x1, uint64 y1, uint64 x2, uint64 y2, XConf *c) {
    this->bmp = NULL;
    this->connectToNewMemory();
    this->fromPyramidRegion(fileName, page, scale, x1, y1, x2, y2, c);
}

#endif //BIM_USE_IMAGEMANAGER

Image &Image::operator=(const Image &img) {
    connectToMemory(img.bmp);
    if (img.metadata.size() > 0) this->metadata = img.metadata;
    return *this;
}

Image Image::deepCopy(bool nohist) const {
    if (bmp == NULL) return Image();
    Image img(this->width(), this->height(), this->depth(), this->samples(), this->pixelType());
    img.bmp->i = this->bmp->i;
    if (img.metadata.size() > 0)
        img.metadata = this->metadata;

    bim::uint64 sample, chan_size = img.bytesPerChan();
    for (sample = 0; sample < bmp->i.samples; sample++) {
        memcpy(img.bmp->bits[sample], bmp->bits[sample], chan_size);
    }
    return img;
}

//------------------------------------------------------------------------------------
// shared memory part
//------------------------------------------------------------------------------------

int Image::getRefId(ImageBitmap *b) const {
    if (b == NULL) return -1;
    for (size_t i = 0; i < refs.size(); ++i)
        if (b == &refs[i]->bmp)
            return (int)i;
    return -1;
}

int Image::getCurrentRefId() const {
    return getRefId(bmp);
}

void Image::connectToMemory(ImageBitmap *b) {
    disconnectFromMemory();

    int ref_id = getRefId(b);
    if (ref_id == -1) return;

    #pragma omp atomic
    refs.at(ref_id)->refs++;
    bmp = &refs.at(ref_id)->bmp;
}

void Image::connectToUnmanagedMemory(ImageBitmap *b) {
    disconnectFromMemory();

    int ref_id = getRefId(b);
    if (ref_id == -1) {
        ImgRefs *new_ref = new ImgRefs;
        new_ref->bmp = *b;
        #pragma omp critical(MEMREF)
        {
            refs.push_back(new_ref);
            ref_id = (int)refs.size() - 1;
        }
    }

    #pragma omp atomic
    refs.at(ref_id)->refs += 2;
    bmp = &refs.at(ref_id)->bmp;
}

void Image::connectToNewMemory() {
    disconnectFromMemory();

    // create a new reference
    ImgRefs *new_ref = new ImgRefs;
    #pragma omp critical(MEMREF)
    {
        refs.push_back(new_ref);
        size_t ref_id = refs.size() - 1;
        refs.at(ref_id)->refs++;
        bmp = &refs.at(ref_id)->bmp;
    }
    initImagePlanes(bmp);
}

void Image::disconnectFromMemory() {
    int ref_id = getCurrentRefId();
    bmp = NULL;

    // decrease the reference to the image
    if (ref_id == -1) return;
    #pragma omp atomic
    refs.at(ref_id)->refs--;

    // check if current reference will be zero, then delete image
    if (refs[ref_id]->refs < 1) {
        ImgRefs *new_ref = refs[ref_id];
        #pragma omp critical(MEMREF)
        {
            deleteImg(&refs.at(ref_id)->bmp);
            refs.erase(refs.begin() + ref_id);
        }
        delete new_ref;
    }
}

//------------------------------------------------------------------------------------
// Arithmetic ops
//------------------------------------------------------------------------------------

template<typename T>
static void lineops_max(void *pdest, const void *psrc1, const void *psrc2,
                        const uint64 w, const uint8 *mask = NULL)
{
    const T *src1 = static_cast<const T*>(psrc1);
    const T *src2 = static_cast<const T*>(psrc2);
    T *dest = (T *)pdest;

    if (mask == NULL) {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = bim::max<T>(src1[x], src2[x]);
    } else {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = mask[x] > 0 ? bim::max<T>(src1[x], src2[x]) : src1[x];
    }
}

template<typename T>
static void lineops_min(void *pdest, const void *psrc1, const void *psrc2,
                        const uint64 w, const uint8 *mask = NULL)
{
    const T *src1 = static_cast<const T*>(psrc1);
    const T *src2 = static_cast<const T*>(psrc2);
    T *dest = (T *)pdest;

    if (mask == NULL) {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = bim::min<T>(src1[x], src2[x]);
    } else {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = mask[x] > 0 ? bim::min<T>(src1[x], src2[x]) : src1[x];
    }
}

template<typename T>
static void lineops_avg(void *pdest, const void *psrc1, const void *psrc2,
                        const uint64 w, const uint8 *mask = NULL)
{
    const T *src1 = static_cast<const T*>(psrc1);
    const T *src2 = static_cast<const T*>(psrc2);
    T *dest = (T *)pdest;

    if (mask == NULL) {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = (src1[x] + src2[x]) / (T)2;
    } else {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = mask[x] > 0 ? (src1[x] + src2[x]) / (T)2 : src1[x];
    }
}

template<typename T>
static void lineops_replace(void *pdest, const void *psrc1, const void *psrc2,
                            const uint64 w, const uint8 *mask = NULL)
{
    const T *src1 = static_cast<const T*>(psrc1);
    const T *src2 = static_cast<const T*>(psrc2);
    T *dest = (T *)pdest;

    if (mask == NULL) {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = src2[x];
    } else {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = mask[x] > 0 ? src2[x] : src1[x];
    }
}

// uses mask weights to blend, without a mask becomes avg
template<typename T>
static void lineops_blend(void *pdest, const void *psrc1, const void *psrc2,
                          const uint64 w, const uint8 *mask = NULL)
{
    const T *src1 = static_cast<const T*>(psrc1);
    const T *src2 = static_cast<const T*>(psrc2);
    T *dest = (T *)pdest;

    if (mask == NULL) {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = (src1[x] + src2[x]) / (T)2;
    } else {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = bim::trim<T, double>((src1[x] * (255.0 - mask[x]) + src2[x] * ((double)mask[x])) / 255.0);
    }
}

template<typename T>
static void lineops_add(void *pdest, const void *psrc1, const void *psrc2,
                        const uint64 w, const uint8 *mask = NULL)
{
    const T *src1 = static_cast<const T*>(psrc1);
    const T *src2 = static_cast<const T*>(psrc2);
    T *dest = (T *)pdest;

    if (mask == NULL) {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = bim::trim<T, T>(src1[x] + src2[x]);
    } else {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = mask[x] > 0 ? bim::trim<T, T>(src1[x] + src2[x]) : src1[x];
    }
}

template<typename T>
static void lineops_sub(void *pdest, const void *psrc1, const void *psrc2, const uint64 w, const uint8 *mask = NULL) {
    const T *src1 = static_cast<const T*>(psrc1);
    const T *src2 = static_cast<const T*>(psrc2);
    T *dest = (T *)pdest;

    if (mask == NULL) {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = bim::trim<T, T>(src1[x] - src2[x]);
    } else {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = mask[x] > 0 ? bim::trim<T, T>(src1[x] - src2[x]) : src1[x];
    }
}

template<typename T>
static void lineops_mul(void *pdest, const void *psrc1, const void *psrc2,
                        const uint64 w, uint8 *mask = NULL)
{
    const T *src1 = static_cast<const T*>(psrc1);
    const T *src2 = static_cast<const T*>(psrc2);
    T *dest = (T *)pdest;

    if (mask == NULL) {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = bim::trim<T, T>(src1[x] * src2[x]);
    } else {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = mask[x] > 0 ? bim::trim<T, T>(src1[x] * src2[x]) : src1[x];
    }
}

template<typename T>
static void lineops_div(void *pdest, const void *psrc1, const void *psrc2,
                        const bim::uint64 w, const uint8 *mask = NULL)
{
    const T *src1 = static_cast<const T*>(psrc1);
    const T *src2 = static_cast<const T*>(psrc2);
    T *dest = (T *)pdest;

    if (mask == NULL) {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = bim::trim<T, T>(src1[x] / src2[x]);
    } else {
        for (uint64 x = 0; x < w; ++x)
            dest[x] = mask[x] > 0 ? bim::trim<T, T>(src1[x] / src2[x]) : src1[x];
    }
}

//------------------------------------------------------------------------------------
// allocation
//------------------------------------------------------------------------------------

int Image::alloc(uint64 w, uint64 h, uint64 samples, uint64 depth, DataFormat format) {
    this->free();
    if (bmp == NULL) return 1;

    bmp->i.width = w;
    bmp->i.height = h;
    bmp->i.samples = (bim::uint32)samples;
    bmp->i.depth = (bim::uint32)depth;
    bmp->i.pixelType = format;
    bmp->i.imageMode = samples == 1 ? bim::ImageModes::IM_GRAYSCALE : bim::ImageModes::IM_MULTI;

    // ensure channel pointers array is properly sized
    if (bmp->bits.size() < samples) {
        bmp->bits.resize(samples);
    }
    
    bim::uint64 size = bytesPerChan();
    for (uint64 sample = 0; sample < this->samples(); ++sample) {
        try {
            bmp->bits[sample] = bim::xmalloc(NULL, size);
        } catch (std::bad_alloc) {
            bmp->bits[sample] = NULL;
            deleteImg(bmp);
            bmp->i = ImageInfo();
            return 1;
        }
    }
    return 0;
}

void Image::free() {
    connectToNewMemory();
    metadata.clear();
}

void *Image::bits(uint64 sample) const {
    if (!bmp || samples() == 0) return NULL;
    uint64 c = bim::trim<uint64, uint64>(sample, 0, samples() - 1);
    return (void *)bmp->bits[c];
}

/*
void Image::setLutColor(bim::uint64 i, RGBA c) {
    if (bmp == NULL) return;
    if (i >= bmp->i.lut.count) return;
    bmp->i.lut.rgba[i] = c;
}

void Image::setLutNumColors(bim::uint64 n) {
    if (bmp == NULL) return;
    if (n > 256) return;
    bmp->i.lut.count = n;
}
*/

Image Image::convertToDepth(const ImageLut &lut) const {

    Image img;
    if (bmp == NULL) return img;
    if (lut.channels() < this->samples()) return *this;

    const uint64 depth = lut.depthOutput();
    const DataFormat pxt = lut.dataFormatOutput();
    const uint64 num_pix = numPixels();

    if (img.alloc(width(), height(), samples(), depth, pxt) == 0) {
        img.bmp->i = this->bmp->i;
        img.bmp->i.depth = (bim::uint32)depth;
        img.bmp->i.pixelType = pxt;
        for (uint64 sample = 0; sample < samples(); ++sample) {
            lut[sample]->apply(bmp->bits[sample], img.bits(sample), num_pix);
        } // sample
    }

    img.metadata = this->metadata;
    return img;
}

Image Image::convertToDepth(uint64 depth, Lut::LutType method, DataFormat pxt, Histogram::ChannelMode mode, ImageHistogram *hist, std::vector<LutParameters> *args) const {
    Image img;
    if (!bmp) return img;
    if (this->depth() == depth && this->pixelType() == pxt && (method == Lut::ltLinearFullRange || method == Lut::ltTypecast))
        return *this;

    // run the whole thing
    const uint64 num_pix = numPixels();

    if (img.alloc(width(), height(), samples(), depth) == 0) {
        img.bmp->i = this->bmp->i;
        img.bmp->i.depth = (bim::uint32)depth;
        if (pxt != bim::DataFormat::FMT_UNDEFINED) img.bmp->i.pixelType = pxt;

        ImageHistogram ih;
        if (mode != Histogram::cmSeparate) ih.setChannelMode(mode);
        if (!hist || !hist->isValid())
            ih.fromImage(*this);
        else
            ih = *hist;

        ImageHistogram oh(img.samples(), img.depth(), img.pixelType());
        ImageLut lut(ih, oh, method, args);

        // dima: LUTs are great for 8 or 16 bit data but not nothing else, add other functionality here
        // otherwise output data will be quanatized
        for (uint64 sample = 0; sample < samples(); ++sample)
            lut[sample]->apply(bmp->bits[sample], img.bits(sample), num_pix);

        // we have to update the hist after the operation
        if (hist && hist->isValid()) {
            for (uint64 sample = 0; sample < samples(); ++sample)
                lut[sample]->apply(*ih[sample], *oh[sample]);
            (*hist) = oh;
        }
    }

    img.metadata = this->metadata;
    return img;
}

static Image operation_stretch(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    Histogram::ChannelMode chan_mode = Histogram::cmSeparate;
    if (img.imageMode() == bim::ImageModes::IM_RGB || img.imageMode() == bim::ImageModes::IM_RGBA)
        chan_mode = Histogram::cmCombined;
    return img.convertToDepth(img.depth(), Lut::ltLinearDataRange, bim::DataFormat::FMT_UNDEFINED, chan_mode, hist);
}

static Image operation_depth(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    Lut::LutType lut_method = Lut::ltLinearFullRange;
    DataFormat out_pixel_format = bim::DataFormat::FMT_UNSIGNED;
    Histogram::ChannelMode chan_mode = Histogram::cmSeparate;
    if (img.imageMode() == bim::ImageModes::IM_RGB || img.imageMode() == bim::ImageModes::IM_RGBA)
        chan_mode = Histogram::cmCombined;

    std::vector<xstring> strl = arguments.split(",");
    int out_depth = strl[0].toInt(0);
    if (strl.size() > 1) {
        if (strl[1].toLowerCase() == "f") lut_method = Lut::ltLinearFullRange;
        if (strl[1].toLowerCase() == "d") lut_method = Lut::ltLinearDataRange;
        if (strl[1].toLowerCase() == "t") lut_method = Lut::ltLinearDataTolerance;
        if (strl[1].toLowerCase() == "e") lut_method = Lut::ltEqualize;
        if (strl[1].toLowerCase() == "c") lut_method = Lut::ltTypecast;
        if (strl[1].toLowerCase() == "n") lut_method = Lut::ltFloat01;
        if (strl[1].toLowerCase() == "l") lut_method = Lut::ltMinMaxGamma;
        if (strl[1].toLowerCase() == "f1") lut_method = Lut::ltLogMild;
        if (strl[1].toLowerCase() == "f2") lut_method = Lut::ltLogNormal;
        if (strl[1].toLowerCase() == "f3") lut_method = Lut::ltLogHeavy;
        if (strl[1].toLowerCase() == "f4") lut_method = Lut::ltLogAggressive;
    }
    if (strl.size() > 2) {
        if (strl[2].toLowerCase() == "u") out_pixel_format = bim::DataFormat::FMT_UNSIGNED;
        if (strl[2].toLowerCase() == "s") out_pixel_format = bim::DataFormat::FMT_SIGNED;
        if (strl[2].toLowerCase() == "f") out_pixel_format = bim::DataFormat::FMT_FLOAT;
    }
    if (strl.size() > 3) {
        if (strl[3].toLowerCase() == "cs") chan_mode = Histogram::cmSeparate;
        if (strl[3].toLowerCase() == "cc") chan_mode = Histogram::cmCombined;
    }

    if (out_depth != 8 && out_depth != 16 && out_depth != 32 && out_depth != 64) {
        std::cout << xstring::xprintf("Output depth (%s bpp) is not supported! Ignored!\n", out_depth);
        return img;
    }

    // if min/max/gamma are provided
    if (strl.size() > 6) {
        lut_method = Lut::ltMinMaxGamma;
        std::vector<LutParameters> args;
        for (size_t p = 4; (p + 2) < strl.size(); p += 3) {
            LutParameters lp;
            lp.in_min = strl[p].toDouble(0.0);     // use 0 to only set gamma
            lp.in_max = strl[p + 1].toDouble(0.0); // use 0 to only set gamma
            lp.gamma = strl[p + 2].toDouble(1.0);
            args.push_back(lp);
        }

        return img.convertToDepth(out_depth, lut_method, out_pixel_format, chan_mode, hist, &args);
    }

    return img.convertToDepth(out_depth, lut_method, out_pixel_format, chan_mode, hist);
}

Image Image::normalize(uint64 to_bpp, ImageHistogram *hist) const {
    if (bmp == NULL) return Image();
    if (depth() == to_bpp) return *this;
    Histogram::ChannelMode chan_mode = Histogram::cmSeparate;
    if (this->imageMode() == bim::ImageModes::IM_RGB || this->imageMode() == bim::ImageModes::IM_RGBA)
        chan_mode = Histogram::cmCombined;
    return convertToDepth(to_bpp, Lut::ltLinearDataRange, bim::DataFormat::FMT_UNSIGNED, chan_mode, hist);
}

static Image operation_norm(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    return img.normalize(8, hist);
}

Image Image::normalizeFloat01(ImageHistogram *hist) const {
    return this->convertToDepth(32, Lut::ltFloat01, bim::DataFormat::FMT_FLOAT, Histogram::cmSeparate, hist);
}

Image Image::ROI(bim::uint64 x, bim::uint64 y, bim::uint64 w, bim::uint64 h) const {

    if (this->bmp == NULL) return *this;
    if (x >= bmp->i.width) return *this;
    if (y >= bmp->i.height) return *this;
    if (w == 0) w = bmp->i.width - x;
    if (h == 0) h = bmp->i.height - y;
    if (w + x > bmp->i.width) w = bmp->i.width - x;
    if (h + y > bmp->i.height) h = bmp->i.height - y;
    if (x == 0 && y == 0 && bmp->i.width == w && bmp->i.height == h) return *this;

    Image img;
    if (img.alloc(w, h, bmp->i.samples, bmp->i.depth, bmp->i.pixelType) == 0) {
        const uint64 newLineSize = img.bytesPerLine();
        const uint64 oldLineSize = this->bytesPerLine();
        const uint64 Bpp = this->bytesInPixels(1);

        for (uint64 sample = 0; sample < samples(); ++sample) {
            unsigned char *pl = (unsigned char *)img.bits(sample);
            unsigned char *plo = ((unsigned char *)this->bmp->bits[sample]) + y * oldLineSize + x * Bpp;
            for (uint64 yi = 0; yi < h; yi++) {
                memcpy(pl, plo, w * Bpp);
                pl += newLineSize;
                plo += oldLineSize;
            } // for yi
        } // sample

        img.bmp->i = this->bmp->i;
        img.bmp->i.width = w;
        img.bmp->i.height = h;
        img.metadata = this->metadata;
        //img.histo = this->histo;
    } // allocated image

    return img;
}

static Image operation_roi(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    std::vector<bim::Rectangle<int>> rois;
    for (const xstring token : arguments.split(";")) {
        std::vector<int> ints = token.splitInt(",", -1);
        int x1 = ints.size() > 0 ? ints[0] : -1;
        int y1 = ints.size() > 1 ? ints[1] : -1;
        int x2 = ints.size() > 2 ? ints[2] : -1;
        int y2 = ints.size() > 3 ? ints[3] : -1;
        if (x1 >= 0 || x2 >= 0 || y1 >= 0 || y2 >= 0)
            rois.push_back(bim::Rectangle<int>(bim::Point<int>(x1, y1), bim::Point<int>(x2, y2)));
    }

    if (rois.size() == 0 || rois[0] < 0) return img;

    xstring template_filename = operations.arguments("-template");
    xstring o_fmt = operations.arguments("-t").toLowerCase();
    xstring options = operations.arguments("-options");

    for (int i = (int)rois.size() - 1; i >= 0; --i) {
        bim::Rectangle<int> r = rois[i];

        // it's allowed to specify only one of the sizes, the other one will be computed
        if (r.p1.x == -1) r.p1.x = 0;
        if (r.p1.y == -1) r.p1.y = 0;
        if (r.p2.x == -1) r.p2.x = (int)img.width() - 1;
        if (r.p2.y == -1) r.p2.y = (int)img.height() - 1;

        if (r.p1.x >= r.p2.x || r.p1.y >= r.p2.y) {
            std::cout << "ROI parameters are invalid, ignored!\n";
            return img;
        }

        if (i == 0) {
            img = img.ROI(r.p1.x, r.p1.y, r.width(), r.height());
        } else {
            std::map<std::string, std::string> vars;
            vars["x1"] = xstring::xprintf("%d", r.p1.x);
            vars["y1"] = xstring::xprintf("%d", r.p1.y);
            vars["x2"] = xstring::xprintf("%d", r.p2.x);
            vars["y2"] = xstring::xprintf("%d", r.p2.y);
            xstring fn = template_filename.processTemplate(vars);

            Image o = img.ROI(r.p1.x, r.p1.y, r.width(), r.height());
            o.toFile(fn, o_fmt, options);
        }
    }
    return img;
}

// set ROI
void render_roi_replace(bim::uint64 x, bim::uint64 y, const Image &img, const Image &roi) {
    bim::uint64 w = roi.width();
    bim::uint64 h = roi.height();
    if (x >= img.width()) x = 0;
    if (y >= img.height()) y = 0;
    if (w + x > img.width()) w = img.width() - x;
    if (h + y > img.height()) h = img.height() - y;

    const uint64 newLineSize = roi.bytesPerLine();
    const uint64 oldLineSize = img.bytesPerLine();
    const uint64 Bpp = img.bytesInPixels(1);

    //#pragma omp parallel for default(shared)
    for (uint64 sample = 0; sample < img.samples(); ++sample) {
        unsigned char *pl = (unsigned char *)roi.bits(sample);
        unsigned char *plo = ((unsigned char *)img.bits(sample)) + y * oldLineSize + x * Bpp;
        //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (h>BIM_OMP_FOR2)
        for (uint64 yi = 0; yi < h; ++yi) {
            unsigned char *ppl = pl + yi * newLineSize;
            unsigned char *pplo = plo + yi * oldLineSize;
            memcpy(pplo, ppl, w * Bpp);
        } // for yi
    }     // sample
}

template<typename T, typename F>
void render_roi(bim::uint64 x, bim::uint64 y, const Image &img, const Image &roi, F func, const Image &mask = Image()) {
    uint64 w = roi.width();
    uint64 h = roi.height();
    if (roi.depth() != img.depth()) return;
    if (roi.samples() != img.samples()) return;
    if (!mask.isEmpty() && mask.depth() != 8) return;
    //if (x >= img.width()) x = 0;
    //if (y >= img.height()) y = 0;
    if (x >= img.width()) return;
    if (y >= img.height()) return;
    if (w + x > img.width()) w = img.width() - x;
    if (h + y > img.height()) h = img.height() - y;

    const uint64 newLineSize = roi.bytesPerLine();
    const uint64 oldLineSize = img.bytesPerLine();
    const uint64 maskLineSize = mask.bytesPerLine();
    const uint64 Bpp = img.bytesInPixels(1);

    //#pragma omp parallel for default(shared)
    for (uint64 sample = 0; sample < img.samples(); ++sample) {
        unsigned char *pl = (unsigned char *)roi.bits(sample);
        unsigned char *plo = ((unsigned char *)img.bits(sample)) + y * oldLineSize + x * Bpp;
        unsigned char *m = (unsigned char *)mask.bits(0);
        //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (h>BIM_OMP_FOR2)
        for (uint64 yi = 0; yi < h; yi++) {
            unsigned char *ppl = pl + yi * newLineSize;
            unsigned char *pplo = plo + yi * oldLineSize;
            unsigned char *pm = m + yi * maskLineSize;
            func(pplo, pplo, ppl, w, pm);
        } // for yi
    }     // sample
}

template<typename T>
void render_roi_func(bim::uint64 x, bim::uint64 y, const Image &img, const Image &roi, Image::FuseMethod method, const Image &mask = Image()) {
    if (method == Image::FuseMethod::fmReplace) {
        render_roi<T>(x, y, img, roi, lineops_replace<T>, mask);
    } else if (method == Image::FuseMethod::fmMax) {
        render_roi<T>(x, y, img, roi, lineops_max<T>, mask);
    } else if (method == Image::FuseMethod::fmMin) {
        render_roi<T>(x, y, img, roi, lineops_min<T>, mask);
    } else if (method == Image::FuseMethod::fmAverage) {
        render_roi<T>(x, y, img, roi, lineops_avg<T>, mask);
    } else if (method == Image::FuseMethod::fmBlend) {
        render_roi<T>(x, y, img, roi, lineops_blend<T>, mask);
    } else if (method == Image::FuseMethod::fmAdd) {
        render_roi<T>(x, y, img, roi, lineops_add<T>, mask);
    } else if (method == Image::FuseMethod::fmSubtract) {
        render_roi<T>(x, y, img, roi, lineops_sub<T>, mask);
    } else if (method == Image::FuseMethod::fmMult) {
        render_roi<T>(x, y, img, roi, lineops_mul<T>, mask);
    } else if (method == Image::FuseMethod::fmDiv) {
        render_roi<T>(x, y, img, roi, lineops_div<T>, mask);
    }
}

void Image::setROI(bim::uint64 x, bim::uint64 y, const Image &img, const Image &mask, Image::FuseMethod method) {
    bim::uint64 w = img.width();
    bim::uint64 h = img.height();
    if (img.depth() != bmp->i.depth) return;
    if (img.samples() != bmp->i.samples) return;
    Image mask2;
    if (!mask.isEmpty() && mask.depth() != 8) {
        mask2 = mask.absolute().normalize(8);
    } else if (!mask.isEmpty()) {
        mask2 = mask;
    }

    if (method == Image::FuseMethod::fmReplace && mask.isEmpty()) {
        render_roi_replace(x, y, *this, img);
    } else if (img.depth() == 8 && img.pixelType() == bim::DataFormat::FMT_UNSIGNED)
        render_roi_func<uint8>(x, y, *this, img, method, mask2);
    else if (img.depth() == 16 && img.pixelType() == bim::DataFormat::FMT_UNSIGNED)
        render_roi_func<uint16>(x, y, *this, img, method, mask2);
    else if (img.depth() == 32 && img.pixelType() == bim::DataFormat::FMT_UNSIGNED)
        render_roi_func<uint32>(x, y, *this, img, method, mask2);
    else if (img.depth() == 64 && img.pixelType() == bim::DataFormat::FMT_UNSIGNED)
        render_roi_func<uint64>(x, y, *this, img, method, mask2);
    else if (img.depth() == 8 && img.pixelType() == bim::DataFormat::FMT_SIGNED)
        render_roi_func<int8>(x, y, *this, img, method, mask2);
    else if (img.depth() == 16 && img.pixelType() == bim::DataFormat::FMT_SIGNED)
        render_roi_func<int16>(x, y, *this, img, method, mask2);
    else if (img.depth() == 32 && img.pixelType() == bim::DataFormat::FMT_SIGNED)
        render_roi_func<int32>(x, y, *this, img, method, mask2);
    else if (img.depth() == 64 && img.pixelType() == bim::DataFormat::FMT_SIGNED)
        render_roi_func<int64>(x, y, *this, img, method, mask2);
    else if (img.depth() == 32 && img.pixelType() == bim::DataFormat::FMT_FLOAT)
        render_roi_func<float32>(x, y, *this, img, method, mask2);
    else if (img.depth() == 64 && img.pixelType() == bim::DataFormat::FMT_FLOAT)
        render_roi_func<float64>(x, y, *this, img, method, mask2);
}

void Image::renderROI(double x, double y, const Image &img, const Image &mask, Image::FuseMethod method) {
    if (x >= this->width()) return;
    if (y >= this->height()) return;
    if (x >= 0 && y >= 0) {
        this->setROI(bim::round<bim::uint64>(x), bim::round<bim::uint64>(y), img, mask, method);
        return;
    }
    if (img.depth() != bmp->i.depth) return;
    if (img.samples() != bmp->i.samples) return;

    // crop ROI to eschew negative parts
    double xx = fabs(std::min<double>(x, 0));
    double yy = fabs(std::min<double>(y, 0));
    double w = img.width() - xx;
    double h = img.height() - yy;
    if (w < 1 || h < 1) return;
    if (x >= this->width() || y >= this->height()) return;

    Image img2 = img.ROI((bim::uint64)xx, (bim::uint64)yy, (bim::uint64)w, (bim::uint64)h);
    if (img2.width() < 1 || img2.height() < 1) return;

    Image mask2;
    if (!mask.isEmpty() && mask.depth() != 8) {
        mask2 = mask.absolute().normalize(8).ROI((bim::uint64)xx, (bim::uint64)yy, (bim::uint64)w, (bim::uint64)h);
    } else if (!mask.isEmpty()) {
        mask2 = mask.ROI((bim::uint64)xx, (bim::uint64)yy, (bim::uint64)w, (bim::uint64)h);
    }

    x = std::max<double>(0, x);
    y = std::max<double>(0, y);
    this->setROI(bim::round<bim::uint64>(x), bim::round<bim::uint64>(y), img2, mask2, method);
}

void Image::setROI(bim::uint64 x, bim::uint64 y, bim::uint64 w, bim::uint64 h, const double &value) {
    Image solid(w, h, this->depth(), this->samples(), this->pixelType());
    solid.fill(value);
    this->setROI(x, y, solid);
}

std::string Image::getTextInfo() const {
    return getImageInfoText(&bmp->i);
}

#ifdef BIM_USE_IMAGEMANAGER
bool Image::fromFile(const char *fileName, int page, XConf *c) {
    this->free();
    if (bmp == NULL) return false;

    FormatManager fm(c);
    bool res = true;

    if (fm.sessionStartRead((bim::Filename)fileName) == 0) {
        fm.sessionReadImage(bmp, page);

        // getting metadata fields
        fm.sessionParseMetaData(0);
        metadata = fm.get_metadata();

    } else
        res = false;

    fm.sessionEnd();

    return res;
}

bool Image::fromFileProxy(const char *fileName, int page, XConf *c,
                          uint64 level, bool power_two_level, double scale,  
                          int64 tilex, int64 tiley, uint64 tilesize,
                          uint64 x1, uint64 y1, uint64 x2, uint64 y2) {
    this->free();
    if (bmp == NULL) return false;

    FormatManager fm(c);
    bool res = true;

    if (fm.sessionStartRead((bim::Filename)fileName) == 0) {
        ImageInfo info = fm.sessionGetInfo();
        if (scale > 0 && info.number_levels > 0 && tilesize == 0 && x2 == 0 && y2 == 0) { // read image resolution using scale
            ImageProxy ip(&fm);
            return ip.readLevel(*this, page, scale);
        } else if (level > 0 && info.number_levels > 0 && tilesize == 0 && x2 == 0 && y2 == 0) { // read image resolution using levels
            ImageProxy ip(&fm);
            return ip.readLevel(*this, page, level, power_two_level);
        } else if (scale > 0 && info.number_levels > 0 && info.tileWidth > 0 && tilesize > 0 && tilex >= 0) { // read image tile using scale
            ImageProxy ip(&fm);
            return ip.readTile(*this, page, tilex, tiley, scale, tilesize);
        } else if (info.number_levels > 0 && level >= 0 && info.tileWidth > 0 && tilesize > 0 && tilex >= 0) { // read image tile using levels
            ImageProxy ip(&fm);
            return ip.readTile(*this, page, tilex, tiley, level, tilesize, power_two_level);
        } else if (scale > 0 && info.number_levels > 0 && info.tileWidth > 0 && x2 > 0 && y2 > 0) { // read image region using scale
            ImageProxy ip(&fm);
            return ip.readRegion(*this, page, x1, y1, x2, y2, scale);
        } else if (info.number_levels > 0 && level >= 0 && info.tileWidth > 0 && x2 > 0 && y2 > 0) { // read image region using levels
            ImageProxy ip(&fm);
            return ip.readRegion(*this, page, x1, y1, x2, y2, level, power_two_level);
        } else { // read image normally
            fm.sessionReadImage(bmp, page);
        }

        // getting metadata fields
        fm.sessionParseMetaData(0);
        metadata = fm.get_metadata();

    } else
        res = false;

    fm.sessionEnd();

    return res;
}

bool Image::fromPyramidFile(const std::string &fileName, uint page, uint64 level, int64 tilex, int64 tiley, uint64 tilesize, XConf *c) {
    this->free();
    if (this->bmp == NULL) return false;

    ImageProxy ip(fileName, c);
    if (!ip.isReady()) return false;

    if (tilex < 0 && level < 1) {
        ip.read(*this, page);
    } else if (tilex < 0 && level > 0) {
        ip.readLevel(*this, page, level, true);
    } else {
        if (tilesize < 1) return false;
        ip.readTile(*this, page, tilex, tiley, level, tilesize, true);
    }

    return true;
}

bool Image::fromPyramidRegion(const std::string &fileName, uint page, uint64 level, uint64 x1, uint64 y1, uint64 x2, uint64 y2, XConf *c) {
    this->free();
    if (this->bmp == NULL) return false;

    ImageProxy ip(fileName, c);
    if (!ip.isReady()) return false;

    return ip.readRegion(*this, page, x1, y1, x2, y2, level, true);
}

bool Image::fromPyramidRegion(const std::string &fileName, uint page, double scale, uint64 x1, uint64 y1, uint64 x2, uint64 y2, XConf *c) {
    this->free();
    if (this->bmp == NULL) return false;

    ImageProxy ip(fileName, c);
    if (!ip.isReady()) return false;

    return ip.readRegion(*this, page, x1, y1, x2, y2, scale);
}

bool Image::toFile(const char *fileName, const char *formatName, const char *options) {
    FormatManager fm;
    fm.writeImage((bim::Filename)fileName, this->bmp, formatName, options);
    return true;
}

#endif //BIM_USE_IMAGEMANAGER


template<typename T>
static void fill_channel(T *p, const T &v, uint64 num_points, double skip_values = std::numeric_limits<double>::quiet_NaN(), double only_values = std::numeric_limits<double>::quiet_NaN()) {
    if (!std::isnan(only_values)) {
        //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (num_points>BIM_OMP_FOR1)
        for (uint64 x = 0; x < num_points; ++x) {
            p[x] = p[x] == only_values ? v : p[x];
        }
        return;
    }

    if (!std::isnan(skip_values)) {
        //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (num_points>BIM_OMP_FOR1)
        for (uint64 x = 0; x < num_points; ++x) {
            p[x] = p[x] != skip_values ? v : p[x];
        }
        return;
    }

    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (num_points>BIM_OMP_FOR1)
    for (uint64 x = 0; x < num_points; ++x) {
        p[x] = v;
    }
}

void Image::fill(double v, double skip_values, double only_values) {
    for (uint64 sample = 0; sample < samples(); ++sample) {
        if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_channel<uint8>((uint8 *)bmp->bits[sample], (uint8)v, bmp->i.width * bmp->i.height, skip_values, only_values);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_channel<uint16>((uint16 *)bmp->bits[sample], (uint16)v, bmp->i.width * bmp->i.height, skip_values, only_values);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_channel<uint32>((uint32 *)bmp->bits[sample], (uint32)v, bmp->i.width * bmp->i.height, skip_values, only_values);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_channel<uint64>((uint64 *)bmp->bits[sample], (uint64)v, bmp->i.width * bmp->i.height, skip_values, only_values);
        else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_channel<int8>((int8 *)bmp->bits[sample], (int8)v, bmp->i.width * bmp->i.height, skip_values, only_values);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_channel<int16>((int16 *)bmp->bits[sample], (int16)v, bmp->i.width * bmp->i.height, skip_values, only_values);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_channel<int32>((int32 *)bmp->bits[sample], (int32)v, bmp->i.width * bmp->i.height, skip_values, only_values);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_channel<int64>((int64 *)bmp->bits[sample], (int64)v, bmp->i.width * bmp->i.height, skip_values, only_values);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            fill_channel<float32>((float32 *)bmp->bits[sample], (float32)v, bmp->i.width * bmp->i.height, skip_values, only_values);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            fill_channel<float64>((float64 *)bmp->bits[sample], (float64)v, bmp->i.width * bmp->i.height, skip_values, only_values);
    } // sample
}

template<typename T>
static void fill_lessthan(T *p, const T &v, uint64 num_points, const T &lessthan) {
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (num_points>BIM_OMP_FOR1)
    for (uint64 x = 0; x < num_points; ++x) {
        p[x] = p[x] < lessthan ? v : p[x];
    }
}

void Image::fillLessThan(const double &lessthan, const double &v) {
    for (uint64 sample = 0; sample < samples(); ++sample) {
        if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_lessthan<uint8>((uint8 *)bmp->bits[sample], (uint8)v, bmp->i.width * bmp->i.height, (uint8)lessthan);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_lessthan<uint16>((uint16 *)bmp->bits[sample], (uint16)v, bmp->i.width * bmp->i.height, (uint16)lessthan);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_lessthan<uint32>((uint32 *)bmp->bits[sample], (uint32)v, bmp->i.width * bmp->i.height, (uint32)lessthan);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_lessthan<uint64>((uint64 *)bmp->bits[sample], (uint64)v, bmp->i.width * bmp->i.height, (uint64)lessthan);
        else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_lessthan<int8>((int8 *)bmp->bits[sample], (int8)v, bmp->i.width * bmp->i.height, (int8)lessthan);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_lessthan<int16>((int16 *)bmp->bits[sample], (int16)v, bmp->i.width * bmp->i.height, (int16)lessthan);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_lessthan<int32>((int32 *)bmp->bits[sample], (int32)v, bmp->i.width * bmp->i.height, (int32)lessthan);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_lessthan<int64>((int64 *)bmp->bits[sample], (int64)v, bmp->i.width * bmp->i.height, (int64)lessthan);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            fill_lessthan<float32>((float32 *)bmp->bits[sample], (float32)v, bmp->i.width * bmp->i.height, (float32)lessthan);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            fill_lessthan<float64>((float64 *)bmp->bits[sample], (float64)v, bmp->i.width * bmp->i.height, (float64)lessthan);
    } // sample
}

template<typename T>
static void fill_morethan(T *p, const T &v, uint64 num_points, const T &morethan) {
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (num_points>BIM_OMP_FOR1)
    for (uint64 x = 0; x < num_points; ++x) {
        p[x] = p[x] > morethan ? v : p[x];
    }
}

void Image::fillMoreThan(const double &morethan, const double &v) {
    for (uint64 sample = 0; sample < samples(); ++sample) {
        if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_morethan<uint8>((uint8 *)bmp->bits[sample], (uint8)v, bmp->i.width * bmp->i.height, (uint8)morethan);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_morethan<uint16>((uint16 *)bmp->bits[sample], (uint16)v, bmp->i.width * bmp->i.height, (uint16)morethan);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_morethan<uint32>((uint32 *)bmp->bits[sample], (uint32)v, bmp->i.width * bmp->i.height, (uint32)morethan);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_morethan<uint64>((uint64 *)bmp->bits[sample], (uint64)v, bmp->i.width * bmp->i.height, (uint64)morethan);
        else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_morethan<int8>((int8 *)bmp->bits[sample], (int8)v, bmp->i.width * bmp->i.height, (int8)morethan);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_morethan<int16>((int16 *)bmp->bits[sample], (int16)v, bmp->i.width * bmp->i.height, (int16)morethan);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_morethan<int32>((int32 *)bmp->bits[sample], (int32)v, bmp->i.width * bmp->i.height, (int32)morethan);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_morethan<int64>((int64 *)bmp->bits[sample], (int64)v, bmp->i.width * bmp->i.height, (int64)morethan);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            fill_morethan<float32>((float32 *)bmp->bits[sample], (float32)v, bmp->i.width * bmp->i.height, (float32)morethan);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            fill_morethan<float64>((float64 *)bmp->bits[sample], (float64)v, bmp->i.width * bmp->i.height, (float64)morethan);
    } // sample
}


template<typename T>
static void fill_binarize(T *p, uint64 num_points, const T &threshold, const T &minv, const T &maxv) {
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (num_points>BIM_OMP_FOR1)
    for (uint64 x = 0; x < num_points; ++x) {
        p[x] = p[x] < threshold ? minv : maxv;
    }
}

void Image::binarize(const double &threshold, const double &minv, const double &maxv) {
    for (uint64 sample = 0; sample < samples(); ++sample) {
        if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_binarize<uint8>((uint8 *)bmp->bits[sample], bmp->i.width * bmp->i.height, (uint8)threshold, (uint8)minv, (uint8)maxv);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_binarize<uint16>((uint16 *)bmp->bits[sample], bmp->i.width * bmp->i.height, (uint16)threshold, (uint16)minv, (uint16)maxv);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_binarize<uint32>((uint32 *)bmp->bits[sample], bmp->i.width * bmp->i.height, (uint32)threshold, (uint32)minv, (uint32)maxv);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            fill_binarize<uint64>((uint64 *)bmp->bits[sample], bmp->i.width * bmp->i.height, (uint64)threshold, (uint64)minv, (uint64)maxv);
        else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_binarize<int8>((int8 *)bmp->bits[sample], bmp->i.width * bmp->i.height, (int8)threshold, (int8)minv, (int8)maxv);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_binarize<int16>((int16 *)bmp->bits[sample], bmp->i.width * bmp->i.height, (int16)threshold, (int16)minv, (int16)maxv);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_binarize<int32>((int32 *)bmp->bits[sample], bmp->i.width * bmp->i.height, (int32)threshold, (int32)minv, (int32)maxv);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            fill_binarize<int64>((int64 *)bmp->bits[sample], bmp->i.width * bmp->i.height, (int64)threshold, (int64)minv, (int64)maxv);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            fill_binarize<float32>((float32 *)bmp->bits[sample], bmp->i.width * bmp->i.height, (float32)threshold, (float32)minv, (float32)maxv);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            fill_binarize<float64>((float64 *)bmp->bits[sample], bmp->i.width * bmp->i.height, (float64)threshold, (float64)minv, (float64)maxv);
    } // sample
}


bool Image::isUnTypedDepth() const {
    if (bmp->i.depth == 8)
        return false;
    else if (bmp->i.depth == 16)
        return false;
    else if (bmp->i.depth == 32)
        return false;
    else if (bmp->i.depth == 64)
        return false;
    else
        return true;
}

Image Image::ensureTypedDepth() const {
    Image img;
    if (bmp == NULL) return img;

    if (bmp->i.depth != 12 && bmp->i.depth != 4 && bmp->i.depth != 1) {
        return *this;
    }

    const uint64 w = width();
    const uint64 out_depth = (depth() <= 8) ? 8 : 16;

    if (img.alloc(w, height(), samples(), out_depth, pixelType()) != 0) {
        return img;
    }

    for (uint64 sample = 0; sample < samples(); ++sample) {
        for (uint64 y = 0; y < height(); ++y) {
            void *dest = img.scanLine(sample, y);
            const void *src = this->scanLine(sample, y);

            if (bmp->i.depth == 1) {
                bim::cnv_buffer_1to8bit((unsigned char *)dest, (const unsigned char *)src, w);
            } else if (bmp->i.depth == 4) {
                bim::cnv_buffer_4to8bit((unsigned char *)dest, (const unsigned char *)src, w);
            } else if (bmp->i.depth == 12) {
                bim::cnv_buffer_12to16bit((unsigned char *)dest, (const unsigned char *)src, w);
            }
        } // for y
    }     // for sample

    img.bmp->i = this->bmp->i;
    img.bmp->i.depth = (bim::uint32)out_depth;
    img.metadata = this->metadata;

    return img;
}

Image Image::ensureColorSpace() const {
#ifdef BIM_USE_TRANSFORMS
    if (this->imageMode() == bim::ImageModes::IM_YCbCr) {
        return this->transform_color(Image::tmcYBRF2RGB);
    }
    if (this->imageMode() == bim::ImageModes::IM_HSV) {
        return this->transform_color(Image::tmcHSV2RGB);
    }
    if (this->imageMode() == bim::ImageModes::IM_CMYK) {
        return this->transform_color(Image::tmcCMYK2RGB);
    }

    //    IM_HSL = 5,  //
    //    IM_RGBE = 13, // Radiance RGBE format defining floating RGB by division with the Exponent
    //    IM_YUV = 14, // YUV444 chromaticity
    //    IM_XYZ = 15, // CIEXYZ
    //    IM_LAB = 16, // CIELab
    //    IM_CMY = 17, // CMY
    //    IM_LUV = 18, // LUV

    //    tmcXYZ2RGB = 6, // not implemented
    //    tmcLAB2RGB = 8, // not implemented

#endif
    return *this;
}

// return a pointer to the buffer of line y formed in iterleaved format xRGB
// the image must be in 8 bpp, otherwise NULL is returned
bim::uchar *Image::scanLineRGB(bim::uint64 y) {
    if (depth() != 8) return 0;
    buf.resize(width() * 3, 0);
    uint64 chans = std::min<uint64>(3, samples());

    for (uint64 s = 0; s < chans; ++s) {
        bim::uchar *line_o = &this->buf[0] + s;
        bim::uchar *line_i = scanLine(s, y);
        for (uint64 x = 0; x < width(); ++x) {
            *line_o = line_i[x];
            line_o += 3;
        } // x
    }     // s

    return &this->buf[0];
}

//------------------------------------------------------------------------------------
// channels
//------------------------------------------------------------------------------------

static TagMap remapMetadata(const TagMap &md, uint64 samples, const std::vector<int> &mapping) {

    if (md.size() == 0) return md;
    TagMap metadata = md;

    std::vector<std::string> channel_names;
    for (unsigned int i = 0; i < samples; ++i)
        channel_names.push_back(metadata.get_value(xstring::xprintf("channel_%u_name", i), xstring::xprintf("%u", i)));

    for (unsigned int i = 0; i < mapping.size(); ++i) {
        const xstring key = xstring::xprintf("channel_%u_name", i);

        if (mapping[i] < 0) {
            metadata.set_value(key, "empty");
            continue;
        }

        if (static_cast<size_t>(mapping[i]) < channel_names.size())
            metadata.set_value(key, channel_names[mapping[i]]);
        else
            metadata.set_value(key, xstring::xprintf("%u", i));
    }

    return metadata;
}

// fast but potentially dangerous function! It will affect all shared references to the same image!!!
// do deepCopy() before if you might have some shared references
// the result will have as many channels as there are entries in mapping
// invalid channel numbers or -1 will become black channels
// all black channels will point to the same area, so do deepCopy() if you'll modify them
void Image::remapChannels(const std::vector<int> &mapping) {

    metadata = remapMetadata(metadata, bmp->i.samples, mapping);

    // check if we have any black channels
    bool empty_channels = false;
    for (const int newidx : mapping)
        if (newidx < 0 || newidx >= this->samples()) {
            empty_channels = true;
            break;
        }

    // if there are empty channels, allocate space for one channel and init it to 0
    bim::uint64 size = bytesPerChan();
    bim::uint8 *empty_buffer = 0;
    if (empty_channels) {
        empty_buffer = (bim::uint8 *) bim::xmalloc(NULL, size);
        memset(empty_buffer, 0, size);
    }

    // create a map to actual channel pointers
    std::vector<void *> channel_map(mapping.size());

    for (size_t i = 0; i < mapping.size(); ++i)
        if (mapping[i] < 0 || (unsigned int)mapping[i] >= bmp->i.samples) {
            channel_map[i] = empty_buffer;
        } else {
            channel_map[i] = bmp->bits[mapping[i]];
        }

    // find unreferenced channels and destroy them
    for (uint64 sample = 0; sample < bmp->i.samples; ++sample) {
        bool found = false;
        for (void *bits : channel_map)
            if (bmp->bits[sample] == bits) {
                found = true;
                break;
            }

        if (!found) {
            bmp->bits[sample] = bim::xfree(NULL, bmp->bits[sample]);
        }
    }

    // reinit channels
    for (uint64 sample = 0; sample < BIM_MAX_CHANNELS; ++sample)
        bmp->bits[sample] = NULL;

    // map channels
    for (uint64 sample = 0; sample < channel_map.size(); ++sample)
        bmp->bits[sample] = channel_map[sample];

    bmp->i.samples = (bim::uint32)channel_map.size();
}

void Image::remapToRGB() {
    if (samples() == 3) return;
    std::vector<int> map;
    if (samples() == 1) {
        map.push_back(0);
        map.push_back(-1);
        map.push_back(-1);
    }
    if (samples() == 2) {
        map.push_back(0);
        map.push_back(1);
        map.push_back(-1);
    }
    if (samples() >= 3) {
        map.push_back(0);
        map.push_back(1);
        map.push_back(2);
    }
    remapChannels(map);
}

void Image::extractChannel(const bim::uint64 &c) {
    std::vector<int> map;
    map.push_back((int)c);
    this->remapChannels(map);
}

Image Image::getChannel(const bim::uint64 &c) const {
    std::vector<std::set<int>> mapping;
    std::set<int> v;
    v.insert((int)c);
    mapping.push_back(v);
    return fuse(std::move(mapping));
}

Image operation_remap(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    std::vector<int> out_channels = arguments.splitInt(",");
    for (unsigned int i = 0; i < out_channels.size(); ++i)
        out_channels[i] = out_channels[i] - 1;

    if (out_channels.size() > 0) {
        img.remapChannels(out_channels); // remap channels is an optimization and may leave channels pointing to the same memory address
        hist->clear();                   // dima: should properly modify instead of clearing
        return img.deepCopy();
    }
    return img;
}

//------------------------------------------------------------------------------------
// resize
//------------------------------------------------------------------------------------

static TagMap resizeMetadata(const TagMap &md, const uint64 w_to, const uint64 h_to, const uint64 w_in, const uint64 h_in) {
    TagMap metadata = md;
    if (metadata.hasKey("pixel_resolution_x")) {
        double new_res = metadata.get_value_double("pixel_resolution_x", 0) * ((double)w_in / (double)w_to);
        metadata.set_value("pixel_resolution_x", new_res);
    }
    if (metadata.hasKey("pixel_resolution_y")) {
        double new_res = metadata.get_value_double("pixel_resolution_y", 0) * ((double)h_in / (double)h_to);
        metadata.set_value("pixel_resolution_y", new_res);
    }
    return metadata;
}

template<typename T>
static void downsample_line(void *pdest, const void *psrc1, const void *psrc2, const uint64 w) {
    const T *src1 = static_cast<const T*>(psrc1);
    const T *src2 = static_cast<const T*>(psrc2);
    T *dest = (T *)pdest;

    uint64 x2 = 0;
    for (uint64 x = 0; x < w; ++x) {
        dest[x] = static_cast<T>((src1[x2] + src1[x2 + 1] + src2[x2] + src2[x2 + 1]) / 4.0);
        x2 += 2;
    }
}

Image Image::downSampleBy2x() const {
    Image img;
    if (bmp == NULL) return img;
    const uint64 w = width() / 2;
    const uint64 h = height() / 2;
    if (img.alloc(w, h, samples(), depth(), pixelType()) != 0) return img;

    for (uint64 sample = 0; sample < this->samples(); ++sample) {
        #pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (h > BIM_OMP_FOR2)
        for (bim::int64 y = 0; y < (bim::int64)h; ++y) {
            uint64 y2 = y * 2;
            void *dest = img.scanLine(sample, y);
            void *src1 = this->scanLine(sample, y2);
            void *src2 = this->scanLine(sample, y2 + 1);

            if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
                downsample_line<uint8>(dest, src1, src2, w);
            else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
                downsample_line<uint16>(dest, src1, src2, w);
            else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
                downsample_line<uint32>(dest, src1, src2, w);
            else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
                downsample_line<int8>(dest, src1, src2, w);
            else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
                downsample_line<int16>(dest, src1, src2, w);
            else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
                downsample_line<int32>(dest, src1, src2, w);
            else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
                downsample_line<float32>(dest, src1, src2, w);
            else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
                downsample_line<float64>(dest, src1, src2, w);
        }
    } // sample

    img.bmp->i = this->bmp->i;
    img.bmp->i.width = w;
    img.bmp->i.height = h;
    img.metadata = resizeMetadata(this->metadata, w, h, width(), height());
    return img;
}

//------------------------------------------------------------------------------------
// Interpolation
//------------------------------------------------------------------------------------

template<typename T, typename Tw>
void image_resample(T *pdest, const uint64 w_to, const uint64 h_to, const uint64 offset_to,
                    const T *psrc, const uint64 w_in, const uint64 h_in, const uint64 offset_in, Image::ResizeMethod method) {

    float smoothing = bim::resize::estimate_smoothing_parameter<uint64>(w_to, h_to, w_in, h_in);

    if (method == Image::ResizeMethod::szNearestNeighbor) {
        bim::image_resample_NN<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, smoothing);
    } else if (method == Image::ResizeMethod::szBiLinear) {
        bim::image_resample_BL<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, smoothing);
    } else if (method == Image::ResizeMethod::szBiCubic) {
        bim::image_resample_BC<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, smoothing);
    } else if (method == Image::ResizeMethod::szLanczos) {
        bim::resize::DLanczosFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szBessel) {
        bim::resize::DBesselFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szPoint) {
        bim::resize::DPointFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szBox) {
        bim::resize::DBoxFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szHermite) {
        bim::resize::DHermiteFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szHanning) {
        bim::resize::DHanningFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szHamming) {
        bim::resize::DHammingFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szGaussian) {
        bim::resize::DGaussianFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szBlackman) {
        bim::resize::DBlackmanFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szQuadratic) {
        bim::resize::DQuadraticFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szCatrom) {
        bim::resize::DCatromFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szMitchell) {
        bim::resize::DMitchellFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szSinc) {
        bim::resize::DSincFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szBlackmanBessel) {
        bim::resize::DBlackmanBesselFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    } else if (method == Image::ResizeMethod::szBlackmanSinc) {
        bim::resize::DBlackmanSincFilter<Tw> filter;
        bim::resize::ResizeImage<T, Tw>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in, filter, smoothing);
    }
}

Image Image::resample(uint64 w, uint64 h, ResizeMethod method, bool keep_aspect_ratio) const {
    Image img;
    if (bmp == NULL) return img;
    if (w == 0 && h == 0) return *this;
    if (width() == w && height() == h) return *this;

    if (keep_aspect_ratio) {
        if ((width() / (float)w) >= (height() / (float)h))
            h = 0;
        else
            w = 0;
    }

    // it's allowed to specify only one of the sizes, the other one will be computed
    if (w == 0)
        w = bim::round<uint64>(width() / (height() / static_cast<float>(h)));
    if (h == 0)
        h = bim::round<uint64>(height() / (width() / static_cast<float>(w)));

    const uint64 depth = this->depth();
    const DataFormat pixelType = this->pixelType();

    if (img.alloc(w, h, samples(), depth, pixelType) != 0)
        return img;

    for (uint64 sample = 0; sample < samples(); ++sample) {
        if (depth == 8 && pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_resample<bim::uchar, float>((bim::uchar *)img.bits(sample), img.width(), img.height(), img.width(),
                                              (bim::uchar *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, method);
        else if (depth == 16 && pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_resample<uint16, float>((uint16 *)img.bits(sample), img.width(), img.height(), img.width(),
                                          (uint16 *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, method);
        else if (depth == 32 && pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_resample<uint32, double>((uint32 *)img.bits(sample), img.width(), img.height(), img.width(),
                                           (uint32 *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, method);
        else if (depth == 8 && pixelType == bim::DataFormat::FMT_SIGNED)
            image_resample<int8, float>((int8 *)img.bits(sample), img.width(), img.height(), img.width(),
                                        (int8 *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, method);
        else if (depth == 16 && pixelType == bim::DataFormat::FMT_SIGNED)
            image_resample<int16, float>((int16 *)img.bits(sample), img.width(), img.height(), img.width(),
                                         (int16 *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, method);
        else if (depth == 32 && pixelType == bim::DataFormat::FMT_SIGNED)
            image_resample<int32, double>((int32 *)img.bits(sample), img.width(), img.height(), img.width(),
                                          (int32 *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, method);
        else if (depth == 32 && pixelType == bim::DataFormat::FMT_FLOAT)
            image_resample<float32, double>((float32 *)img.bits(sample), img.width(), img.height(), img.width(),
                                            (float32 *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, method);
        else if (depth == 64 && pixelType == bim::DataFormat::FMT_FLOAT)
            image_resample<float64, double>((float64 *)img.bits(sample), img.width(), img.height(), img.width(),
                                            (float64 *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, method);

    } // sample

    img.bmp->i = this->bmp->i;
    img.bmp->i.width = w;
    img.bmp->i.height = h;
    img.metadata = resizeMetadata(this->metadata, w, h, width(), height());
    return img;
}

Image Image::resize(uint64 w, uint64 h, ResizeMethod method, bool keep_aspect_ratio) const {
    if (bmp == NULL) return Image();
    if (width() == w && height() == h) return *this;
    if (w == 0 && h == 0) return *this;

    if (keep_aspect_ratio) {
        if ((width() / (float)w) >= (height() / (float)h))
            h = 0;
        else
            w = 0;
    }

    // it's allowed to specify only one of the sizes, the other one will be computed
    if (w == 0)
        w = bim::round<uint64>(width() / (height() / static_cast<float>(h)));
    if (h == 0)
        h = bim::round<uint64>(height() / (width() / static_cast<float>(w)));

    // use pyramid if the size difference is large enough
    double vr = (double)std::max<uint64>(w, width()) / (double)std::min<uint64>(w, width());
    double hr = (double)std::max<uint64>(h, height()) / (double)std::min<uint64>(h, height());
    double rat = std::max(vr, hr);
    if (rat < 1.9 || (width() <= 4096 && height() <= 4096) || (w >= width() && h >= height()))
        return resample(w, h, method, keep_aspect_ratio);

    // use image pyramid
    ImagePyramid pyramid;
    pyramid.createFrom(*this);
    int level = (int)pyramid.levelClosestTop(w, h);
    Image *image = pyramid.imageAt(level);
    if (image->isNull()) return resample(w, h, method, keep_aspect_ratio);
    Image img = image->resample(w, h, method, keep_aspect_ratio);
    pyramid.clear();

    // copy some important info stored in the original image
    img.bmp->i = this->bmp->i;
    img.bmp->i.width = w;
    img.bmp->i.height = h;
    img.metadata = resizeMetadata(this->metadata, w, h, width(), height());
    return img;
}


Image::ResizeMethod Image::resize_method_from_string(const bim::xstring &method_str) {
    bim::xstring method = method_str.toLowerCase();

    if (method == "nn") return Image::ResizeMethod::szNearestNeighbor;
    if (method == "bl") return Image::ResizeMethod::szBiLinear;
    if (method == "bc") return Image::ResizeMethod::szBiCubic;
    if (method == "lanczos") return Image::ResizeMethod::szLanczos;
    if (method == "bessel") return Image::ResizeMethod::szBessel;
    if (method == "point") return Image::ResizeMethod::szPoint;
    if (method == "box") return Image::ResizeMethod::szBox;
    if (method == "hermite") return Image::ResizeMethod::szHermite;
    if (method == "hanning") return Image::ResizeMethod::szHanning;
    if (method == "hamming") return Image::ResizeMethod::szHamming;
    if (method == "blackman") return Image::ResizeMethod::szBlackman;
    if (method == "gaussian") return Image::ResizeMethod::szGaussian;
    if (method == "quadratic") return Image::ResizeMethod::szQuadratic;
    if (method == "catrom") return Image::ResizeMethod::szCatrom;
    if (method == "mitchell") return Image::ResizeMethod::szMitchell;
    if (method == "sinc") return Image::ResizeMethod::szSinc;
    if (method == "blackmanbessel") return Image::ResizeMethod::szBlackmanBessel;
    if (method == "blackmansinc") return Image::ResizeMethod::szBlackmanSinc;

    return Image::ResizeMethod::szNearestNeighbor;
}

Image op_resize_resample(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c, const bim::xstring &operation) {
    std::vector<xstring> strl = arguments.split(",");
    bim::uint64 w = 0, h = 0;
    if (strl.size() >= 2) {
        w = strl[0].toInt(0);
        h = strl[1].toInt(0);
    }

    Image::ResizeMethod resize_method = Image::ResizeMethod::szNearestNeighbor;
    if (strl.size() > 2) {
        resize_method = Image::resize_method_from_string(strl[2]);
    }

    bool resize_preserve_aspect_ratio = false;
    bool resize_no_upsample = false;
    if (strl.size() > 3) {
        if (strl[3].toLowerCase() == "ar") resize_preserve_aspect_ratio = true;
        if (strl[3].toLowerCase() == "mx") {
            resize_preserve_aspect_ratio = true;
            resize_no_upsample = true;
        }
        if (strl[3].toLowerCase() == "noup") {
            resize_preserve_aspect_ratio = true;
            resize_no_upsample = true;
        }
    }

    if (w <= 0 && h <= 0) return img;
    if (resize_no_upsample && img.width() <= w && img.height() <= h) return img;
    if (w <= 0 || h <= 0) resize_preserve_aspect_ratio = false;

    if (resize_preserve_aspect_ratio) {
        if ((img.width() / (float)w) >= (img.height() / (float)h))
            h = 0;
        else
            w = 0;
    }

    // it's allowed to specify only one of the sizes, the other one will be computed
    if (w == 0)
        w = bim::round<uint64>(img.width() / (img.height() / static_cast<float>(h)));
    if (h == 0)
        h = bim::round<uint64>(img.height() / (img.width() / static_cast<float>(w)));

    if (operation == "-resize")
        return img.resize(w, h, resize_method);
    else
        return img.resample(w, h, resize_method);
}

static Image operation_resize(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    return op_resize_resample(img, arguments, operations, hist, c, "-resize");
}

static Image operation_resample(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    return op_resize_resample(img, arguments, operations, hist, c, "-resample");
}


//------------------------------------------------------------------------------------
// Rotation
//------------------------------------------------------------------------------------

TagMap rotateMetadata(const TagMap &md, const double &deg) {
    TagMap metadata = md;
    if (fabs(deg) != 90) return metadata;

    if (metadata.hasKey("pixel_resolution_x") && metadata.hasKey("pixel_resolution_y")) {
        double y_res = metadata.get_value_double("pixel_resolution_x", 0);
        double x_res = metadata.get_value_double("pixel_resolution_y", 0);
        metadata.set_value("pixel_resolution_x", x_res);
        metadata.set_value("pixel_resolution_y", y_res);
    }
    return metadata;
}

template<typename T>
static void image_rotate(T *pdest, const uint64 w_to, const uint64 h_to, const uint64 offset_to,
                         const T *psrc, const uint64 w_in, const uint64 h_in, const uint64 offset_in,
                         double deg)
{
    if (deg == 90)
        image_rotate_right<T>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in);

    if (deg == -90)
        image_rotate_left<T>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in);

    if (deg == 180) {
        image_flip<T>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, psrc, (unsigned int)w_in, (unsigned int)h_in, (unsigned int)offset_in);
        image_mirror<T>(pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to, pdest, (unsigned int)w_to, (unsigned int)h_to, (unsigned int)offset_to);
    }
}

// only available values now are +90, -90 and 180
Image Image::rotate(double deg) const {
    Image img;
    if (bmp == NULL) return img;
    if (deg != 90 && deg != -90 && deg != 180) return *this;

    const uint64 w = (std::fabs(deg) == 90) ? height() : width();
    const uint64 h = (std::fabs(deg) == 90) ? width() : height();
    const uint64 depth = this->depth();

    if (img.alloc(w, h, samples(), depth, pixelType()) == 0)
        for (uint64 sample = 0; sample < samples(); ++sample) {
            if (depth == 8)
                image_rotate<bim::uchar>((bim::uchar *)img.bits(sample), img.width(), img.height(), img.width(),
                                         (bim::uchar *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, deg);
            else if (depth == 16)
                image_rotate<uint16>((uint16 *)img.bits(sample), img.width(), img.height(), img.width(),
                                     (uint16 *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, deg);
            else if (depth == 32)
                image_rotate<uint32>((uint32 *)img.bits(sample), img.width(), img.height(), img.width(),
                                     (uint32 *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, deg);
            else if (depth == 64)
                image_rotate<float64>((float64 *)img.bits(sample), img.width(), img.height(), img.width(),
                                      (float64 *)bmp->bits[sample], bmp->i.width, bmp->i.height, bmp->i.width, deg);
        } // sample

    img.bmp->i = this->bmp->i;
    img.bmp->i.width = w;
    img.bmp->i.height = h;
    img.metadata = rotateMetadata(this->metadata, deg);
    return img;
}

Image Image::rotate_guess() const {
    double angle = 0;
    bool mirror = false;

    xstring orientation = this->metadata.get_value("Exif/Image/Orientation", "");

    if (orientation == "top, left")
        angle = 0; // exif value 1
    else if (orientation == "top, right") {
        angle = 0;
        mirror = true;
    } // exif value 2
    else if (orientation == "bottom, right")
        angle = 180; // exif value 3
    else if (orientation == "bottom, left") {
        angle = 180;
        mirror = true;
    } // exif value 4
    else if (orientation == "left, top") {
        angle = 90;
        mirror = true;
    } // exif value 5
    else if (orientation == "right, top")
        angle = 90; // exif value 6
    else if (orientation == "right, bottom") {
        angle = -90;
        mirror = true;
    } // exif value 7
    else if (orientation == "left, bottom")
        angle = -90; // exif value 8

    Image img = this->rotate(angle);
    if (mirror)
        img = img.mirror();

    // reset orientation tag
    if (metadata.hasKey("Exif/Image/Orientation"))
        img.metadata.set_value("Exif/Image/Orientation", "top, left");

    return img;
}

Image Image::mirror() const {
    if (bmp == NULL) return Image();
    Image img = this->deepCopy();

    for (uint64 sample = 0; sample < samples(); ++sample) {
        if (bmp->i.depth == 8)
            image_mirror<bim::uchar>((bim::uchar *)img.bits(sample), (unsigned int)img.width(), (unsigned int)img.height(), (unsigned int)img.width(),
                                     (bim::uchar *)bmp->bits[sample], (unsigned int)bmp->i.width, (unsigned int)bmp->i.height, (unsigned int)bmp->i.width);
        else if (bmp->i.depth == 16)
            image_mirror<uint16>((uint16 *)img.bits(sample), (unsigned int)img.width(), (unsigned int)img.height(), (unsigned int)img.width(),
                                 (uint16 *)bmp->bits[sample], (unsigned int)bmp->i.width, (unsigned int)bmp->i.height, (unsigned int)bmp->i.width);
        else if (bmp->i.depth == 32)
            image_mirror<uint32>((uint32 *)img.bits(sample), (unsigned int)img.width(), (unsigned int)img.height(), (unsigned int)img.width(),
                                 (uint32 *)bmp->bits[sample], (unsigned int)bmp->i.width, (unsigned int)bmp->i.height, (unsigned int)bmp->i.width);
        else if (bmp->i.depth == 64)
            image_mirror<float64>((float64 *)img.bits(sample), (unsigned int)img.width(), (unsigned int)img.height(), (unsigned int)img.width(),
                                  (float64 *)bmp->bits[sample], (unsigned int)bmp->i.width, (unsigned int)bmp->i.height, (unsigned int)bmp->i.width);
    } // sample

    img.metadata = this->metadata;
    return img;
}

Image Image::flip() const {
    if (bmp == NULL) return Image();
    Image img = this->deepCopy();

    for (uint64 sample = 0; sample < samples(); ++sample) {
        if (bmp->i.depth == 8)
            image_flip<bim::uchar>((bim::uchar *)img.bits(sample), (unsigned int)img.width(), (unsigned int)img.height(), (unsigned int)img.width(),
                                   (bim::uchar *)bmp->bits[sample], (unsigned int)bmp->i.width, (unsigned int)bmp->i.height, (unsigned int)bmp->i.width);
        else if (bmp->i.depth == 16)
            image_flip<uint16>((uint16 *)img.bits(sample), (unsigned int)img.width(), (unsigned int)img.height(), (unsigned int)img.width(),
                               (uint16 *)bmp->bits[sample], (unsigned int)bmp->i.width, (unsigned int)bmp->i.height, (unsigned int)bmp->i.width);
        else if (bmp->i.depth == 32)
            image_flip<uint32>((uint32 *)img.bits(sample), (unsigned int)img.width(), (unsigned int)img.height(), (unsigned int)img.width(),
                               (uint32 *)bmp->bits[sample], (unsigned int)bmp->i.width, (unsigned int)bmp->i.height, (unsigned int)bmp->i.width);
        else if (bmp->i.depth == 64)
            image_flip<float64>((float64 *)img.bits(sample), (unsigned int)img.width(), (unsigned int)img.height(), (unsigned int)img.width(),
                                (float64 *)bmp->bits[sample], (unsigned int)bmp->i.width, (unsigned int)bmp->i.height, (unsigned int)bmp->i.width);
    } // sample

    img.metadata = this->metadata;
    return img;
}

static Image operation_rotate(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    if (arguments.toLowerCase() == "guess") {
        return img.rotate_guess();
    } else if (arguments.toDouble(0) != 0) {
        double rotate_angle = arguments.toDouble(0);
        if (rotate_angle != 0 && rotate_angle != 90 && rotate_angle != -90 && rotate_angle != 180) {
            std::cout << "This rotation angle value is not yet supported...\n";
            return img;
        }

        return img.rotate(rotate_angle);
    }
    return img;
}

static Image operation_mirror(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    return img.mirror();
}

static Image operation_flip(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    return img.flip();
}

//------------------------------------------------------------------------------------
// Pixel Arithmetic
//------------------------------------------------------------------------------------

template<typename T, typename F>
bool image_arithmetic(const Image &img, const Image &ar, F func, const Image &mask) {
    if (ar.width() != img.width()) return false;
    if (ar.height() != img.height()) return false;
    if (ar.samples() != img.samples()) return false;
    if (ar.depth() != img.depth()) return false;
    if (!mask.isEmpty() && mask.depth() != 8) return false;

    const uint64 w = img.width();
    const uint64 h = img.height();

    for (uint64 sample = 0; sample < img.samples(); ++sample) {
#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (h > BIM_OMP_FOR2)
        for (int64 y = 0; y < (bim::int64)h; ++y) {
            void *src = ar.scanLine(sample, y);
            void *dest = img.scanLine(sample, y);
            bim::uint8 *m = mask.isEmpty() ? NULL : mask.scanLine(0, y);
            func(dest, dest, src, w, m);
        }
    } // sample
    return true;
}

template<typename T, typename F>
bool Image::image_arithmetic(const Image &img, F func, const Image &mask) {
    return image_arithmetic<T, F>(*this, img, func, mask);
}

template<typename T>
bool image_arithmetic_func(const Image &img, const Image &ar, const Image &mask, Image::ArithmeticOperators op) {
    if (op == Image::ArithmeticOperators::aoAdd) {
        return image_arithmetic<T>(img, ar, lineops_add<T>, mask);
    } else if (op == Image::ArithmeticOperators::aoSub) {
        return image_arithmetic<T>(img, ar, lineops_sub<T>, mask);
    } else if (op == Image::ArithmeticOperators::aoMul) {
        return image_arithmetic<T>(img, ar, lineops_mul<T>, mask);
    } else if (op == Image::ArithmeticOperators::aoDiv) {
        return image_arithmetic<T>(img, ar, lineops_div<T>, mask);
    } else if (op == Image::ArithmeticOperators::aoMax) {
        return image_arithmetic<T>(img, ar, lineops_max<T>, mask);
    } else if (op == Image::ArithmeticOperators::aoMin) {
        return image_arithmetic<T>(img, ar, lineops_min<T>, mask);
    } else
        return false;
}

bool image_arithmetic_type(const Image &img, const Image &ar, const Image &mask, Image::ArithmeticOperators op) {
    if (img.depth() == 8 && img.pixelType() == bim::DataFormat::FMT_UNSIGNED)
        return image_arithmetic_func<uint8>(img, ar, mask, op);
    else if (img.depth() == 16 && img.pixelType() == bim::DataFormat::FMT_UNSIGNED)
        return image_arithmetic_func<uint16>(img, ar, mask, op);
    else if (img.depth() == 32 && img.pixelType() == bim::DataFormat::FMT_UNSIGNED)
        return image_arithmetic_func<uint32>(img, ar, mask, op);
    else if (img.depth() == 8 && img.pixelType() == bim::DataFormat::FMT_SIGNED)
        return image_arithmetic_func<int8>(img, ar, mask, op);
    else if (img.depth() == 16 && img.pixelType() == bim::DataFormat::FMT_SIGNED)
        return image_arithmetic_func<int16>(img, ar, mask, op);
    else if (img.depth() == 32 && img.pixelType() == bim::DataFormat::FMT_SIGNED)
        return image_arithmetic_func<int32>(img, ar, mask, op);
    else if (img.depth() == 32 && img.pixelType() == bim::DataFormat::FMT_FLOAT)
        return image_arithmetic_func<float32>(img, ar, mask, op);
    else if (img.depth() == 64 && img.pixelType() == bim::DataFormat::FMT_FLOAT)
        return image_arithmetic_func<float64>(img, ar, mask, op);
    else
        return false;
}

//--------------------------------------------------------------------------
// in-place arithmetics
//--------------------------------------------------------------------------
void Image::add(const Image& img) {
    this->imageArithmetic(img, Image::ArithmeticOperators::aoAdd);
}

void Image::sub(const Image &img) {
    this->imageArithmetic(img, Image::ArithmeticOperators::aoSub);
}

void Image::div(const Image &img) {
    this->imageArithmetic(img, Image::ArithmeticOperators::aoDiv);
}

void Image::mul(const Image &img) {
    this->imageArithmetic(img, Image::ArithmeticOperators::aoMul);
}

void Image::add(const double &v) {
    this->operationArithmetic(v, Image::ArithmeticOperators::aoAdd);
}

void Image::sub(const double &v) {
    this->operationArithmetic(v, Image::ArithmeticOperators::aoSub);
}

void Image::div(const double &v) {
    this->operationArithmetic(v, Image::ArithmeticOperators::aoDiv);
}

void Image::mul(const double &v) {
    this->operationArithmetic(v, Image::ArithmeticOperators::aoMul);
}


//--------------------------------------------------------------------------
// image-based operators
//--------------------------------------------------------------------------

bool Image::imageArithmetic(const Image &img, Image::ArithmeticOperators op, const Image &mask) {
    return image_arithmetic_type(*this, img, mask, op);
}

Image Image::operator+(const Image &img) {
    Image r = this->deepCopy();
    r.imageArithmetic(img, Image::ArithmeticOperators::aoAdd);
    return r;
}

Image Image::operator-(const Image &img) {
    Image r = this->deepCopy();
    r.imageArithmetic(img, Image::ArithmeticOperators::aoSub);
    return r;
}

Image Image::operator/(const Image &img) {
    Image r = this->deepCopy();
    r.imageArithmetic(img, Image::ArithmeticOperators::aoDiv);
    return r;
}

Image Image::operator*(const Image &img) {
    Image r = this->deepCopy();
    r.imageArithmetic(img, Image::ArithmeticOperators::aoMul);
    return r;
}

//--------------------------------------------------------------------------
// numeric operators
//--------------------------------------------------------------------------

struct numeric_args {
    double v;
    Image::ArithmeticOperators op;
};

template<typename T>
static void operation_numeric(void *p, const uint64 w, const numeric_args &args, const unsigned char *m) {
    T *src = (T *)p;
    double v = args.v;

    if (args.op == Image::ArithmeticOperators::aoAdd) {
        for (uint64 x = 0; x < w; ++x)
            src[x] = m[x] > 0 ? bim::trim<T, T>(src[x] + (T)v) : src[x];
    } else if (args.op == Image::ArithmeticOperators::aoSub) {
        for (uint64 x = 0; x < w; ++x)
            src[x] = m[x] > 0 ? bim::trim<T, T>(src[x] - (T)v) : src[x];
    } else if (args.op == Image::ArithmeticOperators::aoMul) {
        for (uint64 x = 0; x < w; ++x)
            src[x] = m[x] > 0 ? bim::trim<T, T>(src[x] * (T)v) : src[x];
    } else if (args.op == Image::ArithmeticOperators::aoDiv) {
        for (uint64 x = 0; x < w; ++x)
            src[x] = m[x] > 0 ? bim::trim<T, T>(src[x] / (T)v) : src[x];
    }
}

bool Image::operationArithmetic(const double &v, const Image::ArithmeticOperators &op, const Image &mask) {
    numeric_args args;
    args.v = v;
    args.op = op;

    if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return pixel_operations<uint8>(operation_numeric<uint8>, args, mask);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return pixel_operations<uint16>(operation_numeric<uint16>, args, mask);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return pixel_operations<uint32>(operation_numeric<uint32>, args, mask);
    else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return pixel_operations<int8>(operation_numeric<int8>, args, mask);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return pixel_operations<int16>(operation_numeric<int16>, args, mask);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return pixel_operations<int32>(operation_numeric<int32>, args, mask);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        return pixel_operations<float32>(operation_numeric<float32>, args, mask);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        return pixel_operations<float64>(operation_numeric<float64>, args, mask);
    else
        return false;
}

Image Image::operator+(const double &v) {
    Image r = this->deepCopy();
    r.operationArithmetic(v, Image::ArithmeticOperators::aoAdd);
    return r;
}

Image Image::operator-(const double &v) {
    Image r = this->deepCopy();
    r.operationArithmetic(v, Image::ArithmeticOperators::aoSub);
    return r;
}

Image Image::operator/(const double &v) {
    Image r = this->deepCopy();
    r.operationArithmetic(v, Image::ArithmeticOperators::aoDiv);
    return r;
}

Image Image::operator*(const double &v) {
    Image r = this->deepCopy();
    r.operationArithmetic(v, Image::ArithmeticOperators::aoMul);
    return r;
}

//------------------------------------------------------------------------------------
// Negative
//------------------------------------------------------------------------------------

template<typename T>
static void image_negative(void *pdest, const void *psrc, const uint64 w) {
    const T *src = static_cast<const T*>(psrc);
    T *dest = (T *)pdest;
    T max_val = std::numeric_limits<T>::max();
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (w>BIM_OMP_FOR1)
    for (uint64 x = 0; x < w; ++x)
        dest[x] = max_val - src[x];
}

Image Image::negative() const {
    Image img;
    if (bmp == NULL) return img;
    img = this->deepCopy();
    const uint64 plane_size_pixels = img.numPixels();

    for (uint64 sample = 0; sample < samples(); ++sample) {

        if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_negative<uint8>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_negative<uint16>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_negative<uint32>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_negative<int8>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_negative<int16>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_negative<int32>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_negative<float32>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_negative<float64>(img.bits(sample), bmp->bits[sample], plane_size_pixels);

    } // sample

    return img;
}

static Image operation_negative(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    return img.negative();
}

//------------------------------------------------------------------------------------
// Absolute
//------------------------------------------------------------------------------------

template<typename T>
static void image_absolute(void *pdest, const void *psrc, const uint64 w) {
    const T *src = static_cast<const T*>(psrc);
    T *dest = (T *)pdest;
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (w>BIM_OMP_FOR1)
    for (uint64 x = 0; x < w; ++x)
        dest[x] = std::abs(src[x]);
}

Image Image::absolute() const {
    Image img;
    if (bmp == NULL) return img;
    img = this->deepCopy();
    const uint64 plane_size_pixels = img.numPixels();

    for (uint64 sample = 0; sample < samples(); ++sample) {
        if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_absolute<int8>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_absolute<int16>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_absolute<int32>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_absolute<int64>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_absolute<float32>(img.bits(sample), bmp->bits[sample], plane_size_pixels);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_absolute<float64>(img.bits(sample), bmp->bits[sample], plane_size_pixels);

    } // sample

    return img;
}

static Image operation_absolute(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    return img.absolute();
}

//------------------------------------------------------------------------------------
// Trim
//------------------------------------------------------------------------------------

template<typename T>
static void image_trim(void *pdest, const void *psrc, const uint64 w, double min_v, double max_v) {
    const T *src = static_cast<const T*>(psrc);
    T *dest = (T *)pdest;
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (w>BIM_OMP_FOR1)
    for (uint64 x = 0; x < w; ++x)
        dest[x] = bim::trim<T>(src[x], (T)min_v, (T)max_v);
}

void Image::trim(double min_v, double max_v) const {
    const uint64 plane_size_pixels = numPixels();

    for (uint64 sample = 0; sample < bmp->i.samples; ++sample) {

        if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_trim<uint8>(this->bits(sample), bmp->bits[sample], plane_size_pixels, min_v, max_v);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_trim<uint16>(this->bits(sample), bmp->bits[sample], plane_size_pixels, min_v, max_v);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_trim<uint32>(this->bits(sample), bmp->bits[sample], plane_size_pixels, min_v, max_v);
        else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_trim<int8>(this->bits(sample), bmp->bits[sample], plane_size_pixels, min_v, max_v);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_trim<int16>(this->bits(sample), bmp->bits[sample], plane_size_pixels, min_v, max_v);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_trim<int32>(this->bits(sample), bmp->bits[sample], plane_size_pixels, min_v, max_v);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_trim<float32>(this->bits(sample), bmp->bits[sample], plane_size_pixels, min_v, max_v);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_trim<float64>(this->bits(sample), bmp->bits[sample], plane_size_pixels, min_v, max_v);

    } // sample
}


//------------------------------------------------------------------------------------
// color_levels
//------------------------------------------------------------------------------------

template<typename T>
static void image_color_levels(void *pdest, const void *psrc, const uint64 w, double val_min, double val_max, double gamma) {
    double out_min = std::numeric_limits<T>::is_integer ? bim::lowest<T>() : val_min;
    double out_max = std::numeric_limits<T>::is_integer ? std::numeric_limits<T>::max() : val_max;
    double out_range = out_max - out_min + 1;

    gamma = 1.0 / gamma;
    double range = val_max - val_min;

    const T *src = static_cast<const T*>(psrc);
    T *dest = (T *)pdest;
#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (w > BIM_OMP_FOR1)
    for (int64 x = 0; x < (bim::int64)w; ++x) {
        double vpx = pow(((double)src[x] - val_min) / range, gamma) * out_range + out_min;
        dest[x] = (T) bim::trim<double>(bim::round<double>(vpx), out_min, out_max);
    }
}

// same as photoshop levels command, uses in_min, in_max and gamma from LutParameters
// if in_min == in_max == 0 then they will be computed form data min/max
// the number of args should be either one to be used for all channels or equal number as channels in the image

void Image::color_levels(const std::vector<bim::LutParameters> &args, ImageHistogram *h) {
    ImageHistogram hist;
    if (h && h->isValid()) {
        hist = *h;
    }

    if (!hist.isValid() && (args.size() < 1 || args[0].in_min == args[0].in_max)) {
        hist.fromImage(*this);
    }

    const uint64 plane_size_pixels = numPixels();
    for (uint64 sample = 0; sample < samples(); ++sample) {
        double gamma = 1.0;
        double vmin = 0;
        double vmax = 0;

        if (args.size() > 0) {
            size_t i = args.size() > sample ? sample : 0;
            vmin = args[i].in_min;
            vmax = args[i].in_max;
            gamma = args[i].gamma;
        }

        if (hist.isValid() && vmin == vmax) {
            vmin = hist[sample]->min_value();
            vmax = hist[sample]->max_value();
        }

        if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_color_levels<uint8>(this->bits(sample), bmp->bits[sample], plane_size_pixels, vmin, vmax, gamma);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_color_levels<uint16>(this->bits(sample), bmp->bits[sample], plane_size_pixels, vmin, vmax, gamma);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_color_levels<uint32>(this->bits(sample), bmp->bits[sample], plane_size_pixels, vmin, vmax, gamma);
        else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_color_levels<int8>(this->bits(sample), bmp->bits[sample], plane_size_pixels, vmin, vmax, gamma);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_color_levels<int16>(this->bits(sample), bmp->bits[sample], plane_size_pixels, vmin, vmax, gamma);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_color_levels<int32>(this->bits(sample), bmp->bits[sample], plane_size_pixels, vmin, vmax, gamma);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_color_levels<float32>(this->bits(sample), bmp->bits[sample], plane_size_pixels, vmin, vmax, gamma);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_color_levels<float64>(this->bits(sample), bmp->bits[sample], plane_size_pixels, vmin, vmax, gamma);

    } // sample
}

static Image operation_levels(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    double gamma = 1;
    double minv = 0, maxv = 0;
    std::vector<double> strl = arguments.splitDouble(",");
    if (strl.size() > 0) minv = strl[0];
    if (strl.size() > 1) maxv = strl[1];
    if (strl.size() > 2) gamma = strl[2];

    // if min/max/gamma are provided
    std::vector<bim::LutParameters> args;
    for (size_t p = 0; (p + 2) < strl.size(); p += 3) {
        bim::LutParameters lp;
        lp.in_min = strl[p + 0]; // use 0 to only set gamma
        lp.in_max = strl[p + 1]; // use 0 to only set gamma
        lp.gamma = strl[p + 2];
        args.push_back(lp);
    }

    img.color_levels(args, hist);
    return img;
}


//------------------------------------------------------------------------------------
// color_levels
//------------------------------------------------------------------------------------

template<typename T>
static void image_brightness_contrast(void *pdest, const void *psrc, const uint64 w, double b, double c, Histogram *h) {
    double mx = h->max_value();
    double mu = h->average();
    double g = 1;
    if (b > 0)
        g = 1.0 - b;
    else
        g = 1.0 / (1.0 + b);

    const T *src = static_cast<const T*>(psrc);
    T *dest = (T *)pdest;
#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (w > BIM_OMP_FOR1)
    for (int64 x = 0; x < (bim::int64)w; ++x) {
        double px = ((double)src[x]);
        px = (px - mu) * c + mu;
        px = pow(px / mx, g) * mx;
        dest[x] = bim::trim<T, double>(px);
    }
}

// same as photoshop brightness/contrast command, both values in range [-100, 100]
void Image::color_brightness_contrast(int brightness, int contrast, ImageHistogram *h) {
    uint64 plane_size_pixels = numPixels();
    ImageHistogram hist;
    if (h && h->isValid())
        hist = *h;
    else
        hist.fromImage(*this);

    double b = (brightness / 200.0);
    double c = contrast >= 0 ? (contrast / 200.0) + 1 : 1.0 - (contrast / -200.0);

    for (uint64 sample = 0; sample < bmp->i.samples; ++sample) {
        if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_brightness_contrast<uint8>(this->bits(sample), bmp->bits[sample], plane_size_pixels, b, c, hist[sample]);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_brightness_contrast<uint16>(this->bits(sample), bmp->bits[sample], plane_size_pixels, b, c, hist[sample]);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_brightness_contrast<uint32>(this->bits(sample), bmp->bits[sample], plane_size_pixels, b, c, hist[sample]);
        else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_brightness_contrast<int8>(this->bits(sample), bmp->bits[sample], plane_size_pixels, b, c, hist[sample]);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_brightness_contrast<int16>(this->bits(sample), bmp->bits[sample], plane_size_pixels, b, c, hist[sample]);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_brightness_contrast<int32>(this->bits(sample), bmp->bits[sample], plane_size_pixels, b, c, hist[sample]);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_brightness_contrast<float32>(this->bits(sample), bmp->bits[sample], plane_size_pixels, b, c, hist[sample]);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_brightness_contrast<float64>(this->bits(sample), bmp->bits[sample], plane_size_pixels, b, c, hist[sample]);

    } // sample
}

static Image operation_brightnesscontrast(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    int brightness = 0;
    int contrast = 0;
    std::vector<int> strl = arguments.splitInt(",");
    if (strl.size() > 0) brightness = strl[0];
    if (strl.size() > 1) contrast = strl[1];

    if (brightness != 0.0 || contrast != 0.0)
        img.color_brightness_contrast(brightness, contrast, hist);
    return img;
}

//------------------------------------------------------------------------------------
// pixel_counter
//------------------------------------------------------------------------------------

template<typename T>
static uint64 image_pixel_counter(const Image &img, const uint64 sample, const double threshold_above) {
    std::vector<bim::uint64> c(img.height(), 0);
#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (img.height() > BIM_OMP_FOR2)
    for (int64 y = 0; y < (bim::int64)img.height(); ++y) {
        T *src = (T *)img.scanLine(sample, y);
        for (uint64 x = 0; x < img.width(); ++x) {
            if (src[x] > threshold_above)
                c[y]++;
        }
    }

    uint64 count = 0;
    for (uint64 y = 0; y < img.height(); ++y) {
        count += c[y];
    }
    return count;
}

// returns a number of pixels above the threshold
uint64 Image::pixel_counter(uint64 sample, double threshold_above) {
    const uint64 plane_size_pixels = numPixels();

    if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return image_pixel_counter<uint8>(*this, sample, threshold_above);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return image_pixel_counter<uint16>(*this, sample, threshold_above);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return image_pixel_counter<uint32>(*this, sample, threshold_above);
    else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return image_pixel_counter<int8>(*this, sample, threshold_above);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return image_pixel_counter<int16>(*this, sample, threshold_above);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return image_pixel_counter<int32>(*this, sample, threshold_above);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        return image_pixel_counter<float32>(*this, sample, threshold_above);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        return image_pixel_counter<float64>(*this, sample, threshold_above);

    return 0;
}

std::vector<uint64> Image::pixel_counter(double threshold_above) {
    std::vector<uint64> counts(this->samples());
    for (uint64 sample = 0; sample < bmp->i.samples; ++sample)
        counts[sample] = this->pixel_counter(sample, threshold_above);
    return counts;
}

//------------------------------------------------------------------------------------
// Row scan
//------------------------------------------------------------------------------------
template<typename T>
static void image_row_scan(void *pdest, const Image &img, const uint64 sample, const uint64 x) {
    T *dest = (T *)pdest;
    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (img.height()>BIM_OMP_FOR2)
    for (uint64 y = 0; y < img.height(); ++y) {
        T *src = (T *)img.scanLine(sample, y);
        dest[y] = src[x];
    }
}

void Image::scanRow(uint64 sample, uint64 x, uint8 *buf) const {
    if (bmp->i.depth == 8)
        image_row_scan<uint8>(buf, *this, sample, x);
    else if (bmp->i.depth == 16)
        image_row_scan<uint16>(buf, *this, sample, x);
    else if (bmp->i.depth == 32 && bmp->i.pixelType != bim::DataFormat::FMT_FLOAT)
        image_row_scan<uint32>(buf, *this, sample, x);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        image_row_scan<float32>(buf, *this, sample, x);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        image_row_scan<float64>(buf, *this, sample, x);
}

//------------------------------------------------------------------------------------
// Operations generics
//------------------------------------------------------------------------------------

template<typename T, typename F, typename A>
bool Image::pixel_operations(F func, const A &args, const Image &mask) {
    if (bmp == NULL) return false;
    const uint64 w = width();
    const uint64 h = height();
    std::vector<unsigned char> mm(w, 255);

    for (uint64 sample = 0; sample < samples(); ++sample) {
        #pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (h > BIM_OMP_FOR2)
        for (int64 y = 0; y < (bim::int64)h; ++y) {
            void *src = this->scanLine(sample, y);
            unsigned char *m = !mask.isEmpty() ? mask.scanLine(0, y) : &mm[0];
            func(src, w, args, m);
        }
    } // sample
    return true;
}

//------------------------------------------------------------------------------------
// Threshold
//------------------------------------------------------------------------------------

struct threshold_args {
    double th;
    Image::ThresholdTypes method;
};

template<typename T>
void do_threshold(void *p, const uint64 w, const threshold_args &args, const unsigned char *m) {
    double th = args.th;
    T *src = (T *)p;
    T max_val = std::numeric_limits<T>::max();
    T min_val = bim::lowest<T>();

    if (args.method == Image::ThresholdTypes::ttLower) {
        for (uint64 x = 0; x < w; ++x)
            if (src[x] < th && m[x] > 0)
                src[x] = min_val;
    } else if (args.method == Image::ThresholdTypes::ttUpper) {
        for (uint64 x = 0; x < w; ++x)
            if (src[x] >= th && m[x] > 0)
                src[x] = max_val;
    } else if (args.method == Image::ThresholdTypes::ttBoth) {
        for (uint64 x = 0; x < w; ++x)
            if (m[x] > 0) {
                if (src[x] < th)
                    src[x] = min_val;
                else
                    src[x] = max_val;
            }
    }
}

bool Image::operationThreshold(const double &th, const Image::ThresholdTypes &method, const Image &mask) {
    threshold_args args;
    args.th = th;
    args.method = method;

    if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return pixel_operations<uint8>(do_threshold<uint8>, args, mask);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return pixel_operations<uint16>(do_threshold<uint16>, args, mask);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return pixel_operations<uint32>(do_threshold<uint32>, args, mask);
    else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return pixel_operations<int8>(do_threshold<int8>, args, mask);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return pixel_operations<int16>(do_threshold<int16>, args, mask);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return pixel_operations<int32>(do_threshold<int32>, args, mask);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        return pixel_operations<float32>(do_threshold<float32>, args, mask);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        return pixel_operations<float64>(do_threshold<float64>, args, mask);
    else
        return false;
}

static Image operation_threshold(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    std::vector<xstring> strl = arguments.split(",");
    double threshold = strl[0].toDouble(0);
    Image::ThresholdTypes threshold_operation = Image::ThresholdTypes::ttNone;
    if (strl.size() > 1) {
        if (strl[1].toLowerCase() == "lower") threshold_operation = Image::ThresholdTypes::ttLower;
        if (strl[1].toLowerCase() == "upper") threshold_operation = Image::ThresholdTypes::ttUpper;
        if (strl[1].toLowerCase() == "both") threshold_operation = Image::ThresholdTypes::ttBoth;
    }

    if (threshold_operation != Image::ThresholdTypes::ttNone)
        img.operationThreshold(threshold, threshold_operation);
    return img;
}


//------------------------------------------------------------------------------------
// Corrections: Bright-field
//------------------------------------------------------------------------------------

/*
img = mat_cast(img, dt) # use matlab equivalent cast
*/

template<typename T, typename Tw>
static void image_correct_brightfield(void *pdest, const void *pbf, const void *pff, const uint64 w) {
    T *dest = (T *)pdest;
    T *src = dest;
    const double *bf = static_cast<const double*>(pbf);
    const double *ff = static_cast<const double*>(pff);
    for (uint64 x = 0; x < w; ++x)
        dest[x] = bim::trim<T, double>(((Tw)src[x] - bf[x]) * ff[x]);
}

void Image::correction_brightfield(const Image &img_bf, const Image &img_ff) {
    bim::Image img_bf_dbl = img_bf.convertToDepth(64, Lut::ltTypecast, bim::DataFormat::FMT_FLOAT);
    bim::Image img_ff_dbl = img_ff.convertToDepth(64, Lut::ltTypecast, bim::DataFormat::FMT_FLOAT);
    const uint64 plane_size_pixels = numPixels();

    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (plane_size_pixels>BIM_OMP_FOR1)
    for (uint64 sample = 0; sample < samples(); ++sample) {
        if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_correct_brightfield<bim::uint8, double>(this->bits(sample), img_bf_dbl.bits(sample), img_ff_dbl.bits(sample), plane_size_pixels);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_correct_brightfield<bim::uint16, double>(this->bits(sample), img_bf_dbl.bits(sample), img_ff_dbl.bits(sample), plane_size_pixels);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_correct_brightfield<bim::uint32, double>(this->bits(sample), img_bf_dbl.bits(sample), img_ff_dbl.bits(sample), plane_size_pixels);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
            image_correct_brightfield<bim::uint64, double>(this->bits(sample), img_bf_dbl.bits(sample), img_ff_dbl.bits(sample), plane_size_pixels);
        else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_correct_brightfield<bim::int8, double>(this->bits(sample), img_bf_dbl.bits(sample), img_ff_dbl.bits(sample), plane_size_pixels);
        else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_correct_brightfield<bim::int16, double>(this->bits(sample), img_bf_dbl.bits(sample), img_ff_dbl.bits(sample), plane_size_pixels);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_correct_brightfield<bim::int32, double>(this->bits(sample), img_bf_dbl.bits(sample), img_ff_dbl.bits(sample), plane_size_pixels);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
            image_correct_brightfield<bim::int64, double>(this->bits(sample), img_bf_dbl.bits(sample), img_ff_dbl.bits(sample), plane_size_pixels);
        else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_correct_brightfield<bim::float32, double>(this->bits(sample), img_bf_dbl.bits(sample), img_ff_dbl.bits(sample), plane_size_pixels);
        else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
            image_correct_brightfield<bim::float64, double>(this->bits(sample), img_bf_dbl.bits(sample), img_ff_dbl.bits(sample), plane_size_pixels);

    } // sample
}

static Image operation_correction_brightfield(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    // -correction_bf "path1/file_bf;path2/file_ff" - single file with all channels
    // -correction_bf "path1/file_bf_ch1,path2/file_bf_ch2,path3/file_bf_ch3;path1/file_ff_ch1,path2/file_ff_ch2,path3/file_ff_ch3" - file per channel
    std::vector<xstring> strl = arguments.split(";");
    Image img_bf;
    Image img_ff;

    // bf must be present
    if (strl.size() < 1)
        return img;
    if (strl[0].contains(",")) {
        std::vector<xstring> fls = strl[0].split(",");
        img_bf.fromFile(fls[0]);
        for (size_t c = 1; c < fls.size(); ++c) {
            img_bf = img_bf.appendChannels(Image{fls[c]});
        }
    } else {
        img_bf.fromFile(strl[0]);
    }

    // if ff is not present set to 1.0
    if (strl.size() < 2) {
        img_ff.create(img.width(), img.height(), 64, img.samples(), bim::DataFormat::FMT_FLOAT);
        img_ff.fill(1.0);
    } else {
        if (strl[1].contains(",")) {
            std::vector<xstring> fls = strl[1].split(",");
            img_ff.fromFile(fls[0]);
            for (size_t c = 1; c < fls.size(); ++c) {
                img_ff = img_ff.appendChannels(Image{fls[c]});
            }
        } else {
            img_ff.fromFile(strl[1]);
        }
    }

    // test numbers of channels
    if (img_bf.width() != img.width()) return img;
    if (img_bf.height() != img.height()) return img;
    if (img_bf.samples() != img.samples()) return img;
    if (img_ff.width() != img.width()) return img;
    if (img_ff.height() != img.height()) return img;
    if (img_ff.samples() != img.samples()) return img;

    // run
    img_bf = img_bf.convertToDepth(64, Lut::ltTypecast, bim::DataFormat::FMT_FLOAT);
    img_ff = img_ff.convertToDepth(64, Lut::ltTypecast, bim::DataFormat::FMT_FLOAT);
    img.correction_brightfield(img_bf, img_ff);

    return img;
}


//------------------------------------------------------------------------------------
// Channel fusion
//------------------------------------------------------------------------------------

static std::vector<bim::ColorF32> init_fusion_from_meta(const Image &img) {
    std::vector<bim::ColorF32> out_weighted_fuse_channels;
    bim::TagMap m = img.get_metadata();
    uint64 num_samples = img.samples();
    if (img.imageMode() == bim::ImageModes::IM_RGBA || (img.imageMode() == bim::ImageModes::IM_RGB && num_samples == 4)) {
        num_samples = 3;
    }
    for (uint64 i = 0; i < num_samples; ++i) {
        xstring key = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i) + bim::CHANNEL_INFO_COLOR;
        if (m.hasKey(key)) {
            xstring t = m.get_value(key, "");
            std::vector<double> cmp = t.splitDouble(",");
            cmp.resize(3, 0);

            xstring modality = m.get_value(xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i) + bim::CHANNEL_INFO_MODALITY, "");
            if (modality.toLowerCase().contains("bright") && cmp[0] == 1.0 && cmp[1] == 1.0 && cmp[2] == 1.0 && num_samples > 1) {
                // brightfield channel in the multi-channel image
                cmp[0] = 0.6;
                cmp[1] = 0.6;
                cmp[2] = 0.6;
            }

            out_weighted_fuse_channels.push_back(bim::ColorF32::from_fRGBA((float)cmp[0], (float)cmp[1], (float)cmp[2]));
        } else if (i < bim::meta::channel_colors_default.size()) {
            out_weighted_fuse_channels.push_back(bim::ColorF32::default_color((const int)i));
        }
    }
    return out_weighted_fuse_channels;
}

static void optimize_channel_map(std::set<int> &channels, const Image &img) {
    std::set<int> newval;

    // ensure all channels are valid
    for  (const int ch : channels)
        newval.insert((int)bim::trim<uint64>(ch, -1, img.samples() - 1));

    // remove -1 if any other value is present
    if (*newval.rbegin() > -1)
        newval.erase(-1);

    channels = newval;
}

template<typename T>
static void fuse_channels(T *dest, const Image &srci, const std::set<int> &channels) {
    for (const int ch : channels) {
        T *src = (T *)srci.bits(ch);
        //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (s>BIM_OMP_FOR1)
        for (uint64 x = 0; x < srci.numPixels(); ++x)
            dest[x] = std::max(dest[x], src[x]);
    }
}

Image Image::fuse(const std::vector<std::set<int>> &map) const {
    Image img;
    if (bmp == NULL) return img;
    
    std::vector<std::set<int>> mapping = map;
    for (std::set<int> channels : mapping)
        optimize_channel_map(channels, *this);

    const uint64 c = mapping.size();
    const uint64 depth = this->depth();
    const DataFormat pixelType = this->pixelType();

    if (img.alloc(width(), height(), c, depth, pixelType) == 0) {
        img.fill(0);

        for (uint64 sample = 0; sample < img.samples(); ++sample) {
            const std::set<int> &channels = mapping[sample];

            if (*channels.begin() == -1)
                continue;                     // if mapping only has -1 then the output channel should be black
            else if (channels.size() == 1) // if no fusion is going on simply copy the data
                memcpy(img.bits(sample), bmp->bits[*channels.begin()], img.bytesPerChan());
            else // ok, here we fuse
                if (depth == 8 && pixelType == bim::DataFormat::FMT_UNSIGNED)
                fuse_channels((uint8 *)img.bits(sample), *this, channels);
            else if (depth == 16 && pixelType == bim::DataFormat::FMT_UNSIGNED)
                fuse_channels((uint16 *)img.bits(sample), *this, channels);
            else if (depth == 32 && pixelType == bim::DataFormat::FMT_UNSIGNED)
                fuse_channels((uint32 *)img.bits(sample), *this, channels);
            else if (depth == 8 && pixelType == bim::DataFormat::FMT_SIGNED)
                fuse_channels((int8 *)img.bits(sample), *this, channels);
            else if (depth == 16 && pixelType == bim::DataFormat::FMT_SIGNED)
                fuse_channels((int16 *)img.bits(sample), *this, channels);
            else if (depth == 32 && pixelType == bim::DataFormat::FMT_SIGNED)
                fuse_channels((int32 *)img.bits(sample), *this, channels);
            else if (depth == 32 && pixelType == bim::DataFormat::FMT_FLOAT)
                fuse_channels((float32 *)img.bits(sample), *this, channels);
            else if (depth == 64 && pixelType == bim::DataFormat::FMT_FLOAT)
                fuse_channels((float64 *)img.bits(sample), *this, channels);

        } // sample
    }

    img.bmp->i = this->bmp->i;
    img.bmp->i.samples = (bim::uint32)c;
    img.metadata = this->metadata;
    return img;
}

Image Image::fuse(int red, int green, int blue, int yellow, int magenta, int cyan, int gray) const {
    std::vector<std::set<int>> mapping;
    std::set<int> rv, gv, bv;
    // Color mixing: Y=R+G, M=R+B, C=B+G, GR=R+G+B

    rv.insert(red);
    rv.insert(yellow);
    rv.insert(magenta);
    rv.insert(gray);

    gv.insert(green);
    gv.insert(yellow);
    gv.insert(cyan);
    gv.insert(gray);

    bv.insert(blue);
    bv.insert(magenta);
    bv.insert(cyan);
    bv.insert(gray);

    mapping.push_back(rv);
    mapping.push_back(gv);
    mapping.push_back(bv);

    // each of the vectors will be optimized by the fuse function, no need to take care of that here
    return fuse(std::move(mapping));
}

//------------------------------------------------------------------------------------
// Channel fusion
//------------------------------------------------------------------------------------

static TagMap fuseMetadata(const TagMap &md, const uint64 samples, const std::vector<std::vector<std::pair<int, float>>> &map) {

    if (md.size() == 0) return md;
    TagMap metadata = md;

    std::vector<std::string> channel_names;
    for (unsigned int i = 0; i < samples; ++i)
        channel_names.push_back(metadata.get_value(xstring::xprintf("channel_%u_name", i), xstring::xprintf("%u", i)));

    for (unsigned int i = 0; i < map.size(); ++i) {
        xstring new_name;
        for (const std::pair<int64, float> p : map[i]) {
            if (p.second > 0 && p.first > 0 && static_cast<size_t>(p.first) < channel_names.size()) {
                if (!new_name.empty())
                    new_name += " + ";
                new_name += channel_names[p.first];
            }
            //new_name = xstring::xprintf("%d",i);
        }
        metadata.set_value(xstring::xprintf("channel_%u_name", i), new_name);
    }

    return metadata;
}

/*
template <typename T, typename To>
void norm_cpy ( Image *img, const Image &srci, double sh, double mu ) {
  for (unsigned int sample=0; sample<srci.samples(); ++sample ) {
    for (unsigned int y=0; y<srci.height(); ++y) {
        T *src = (T*) srci.scanLine(sample, y);
        To *dest = (To*) img->scanLine(sample, y);
        for (unsigned int x=0; x<srci.width(); ++x)
            dest[x] = bim::trim<To, double>( (src[x]+sh)*mu, bim::lowest<T>(), std::numeric_limits<To>::max() );
    } // y
  } // sample
}

// this only works for DOUBLE srci!!!
void norm_copy ( Image *img, const Image &srci, double sh, double mu ) {
    if (img->depth()==8 && img->pixelType()==bim::DataFormat::FMT_UNSIGNED)
      norm_cpy<BIM_DOUBLE, uint8> ( img, srci, sh, mu );
    else
    if (img->depth()==16 && img->pixelType()==bim::DataFormat::FMT_UNSIGNED)
      norm_cpy<BIM_DOUBLE, uint16> ( img, srci, sh, mu );
    else
    if (img->depth()==32 && img->pixelType()==bim::DataFormat::FMT_UNSIGNED)
      norm_cpy<BIM_DOUBLE, uint32> ( img, srci, sh, mu );
    else
    if (img->depth()==8 && img->pixelType()==bim::DataFormat::FMT_SIGNED)
      norm_cpy<BIM_DOUBLE, int8> ( img, srci, sh, mu );
    else
    if (img->depth()==16 && img->pixelType()==bim::DataFormat::FMT_SIGNED)
      norm_cpy<BIM_DOUBLE, int16> ( img, srci, sh, mu );
    else
    if (img->depth()==32 && img->pixelType()==bim::DataFormat::FMT_SIGNED)
      norm_cpy<BIM_DOUBLE, int32> ( img, srci, sh, mu );
    else
    if (img->depth()==32 && img->pixelType()==bim::DataFormat::FMT_FLOAT)
      norm_cpy<BIM_DOUBLE, float32> ( img, srci, sh, mu );
    else
    if (img->depth()==64 && img->pixelType()==bim::DataFormat::FMT_FLOAT)
      norm_cpy<BIM_DOUBLE, float64> ( img, srci, sh, mu );
}
*/

template<typename T, typename To>
void fuse_channels_weights(Image *img, uint64 sample, const Image &srci, const std::vector<std::pair<int, float>> &mapping, double shift = 0.0, double mult = 1.0, Image::FuseMethod method = Image::FuseMethod::fmAverage) {
    double weight_sum = 0;
    for (const std::pair<int, float> p : mapping) {
        //  weight_sum += i->second;
        //if (weight_sum==0) weight_sum=1;
        weight_sum++;
    }

#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (srci.height() > BIM_OMP_FOR2)
    for (int64 y = 0; y < (bim::int64)srci.height(); ++y) {
        std::vector<T> black(srci.width(), 0);
        std::vector<double> line(srci.width());
        To *dest = (To *)img->scanLine(sample, y);
        memset(&line[0], 0, srci.width() * sizeof(double));

        for (const std::pair<int, float> p : mapping) {
            T *src = &black[0];
            if (p.first >= 0)
                src = (T *)srci.scanLine(p.first, y);

            if (method == Image::FuseMethod::fmAverage) {
                for (uint64 x = 0; x < srci.width(); ++x)
                    line[x] += ((double)src[x]) * p.second;
            } else if (method == Image::FuseMethod::fmMax) {
                for (uint64 x = 0; x < srci.width(); ++x)
                    line[x] = std::max<double>(((double)src[x]) * p.second, line[x]);
            }
        } // map

        if (method == Image::FuseMethod::fmMax) {
            for (uint64 x = 0; x < srci.width(); ++x)
                dest[x] = (To)line[x];
        } else if (method == Image::FuseMethod::fmAverage) {
            for (uint64 x = 0; x < srci.width(); ++x)
                dest[x] = bim::trim<To, double>(((line[x] / weight_sum) + shift) * mult, bim::lowest<T>(), std::numeric_limits<To>::max());
        }

    } // y
}

Image Image::fuse(const std::vector<std::vector<std::pair<int, float>>> &map, Image::FuseMethod method, ImageHistogram *hin) const {
    Image img;
    if (bmp == NULL) return img;

    // check input map for invalid channels
    std::vector<std::vector<std::pair<int, float>>> mapping = map;
    for (std::vector<std::pair<int, float>> &item : mapping) {
        for (std::pair<int, float> &p : item) {
            if (p.first < 0 || static_cast<uint64>(p.first) >= samples()) {
                p.first = (int)samples() - 1;
                p.second = 0;
            }
        }
    }

    // compute the input image levels
    ImageHistogram hist;
    if (!hin || !hin->isValid())
        hist.fromImage(*this);
    else
        hist = *hin;

    double in_max = hist.max_value();
    double in_min = hist.min_value();
    double fu_min = in_max;
    double fu_max = in_min;
    for (std::vector<std::pair<int, float>> &item : mapping) {
        double camax = 0, camin = 0, canum = 0;
        for (const std::pair<int, float> p : item) {
            if (p.first >= 0 && p.second > 0) {
                camax += hist[p.first]->max_value() * p.second;
                camin += hist[p.first]->min_value() * p.second;
            }
            canum += 1;
        }
        fu_max = std::max(fu_max, camax / canum);
        fu_min = std::min(fu_min, camin / canum);
    }
    double shift = in_min - fu_min;
    double mult = (in_max - in_min) / (fu_max - fu_min);


    const uint64 c = mapping.size();
    const uint64 depth = this->depth();
    const DataFormat pixelType = this->pixelType();

    if (img.alloc(width(), height(), c, depth, pixelType) == 0) {
        //if ( img.alloc( bmp->i.width, bmp->i.height, c, 64, bim::DataFormat::FMT_FLOAT ) == 0 ) { // create a temp double image
        img.fill(0);
        for (uint64 sample = 0; sample < img.samples(); ++sample) {
            if (mapping[sample].size() == 1 && mapping[sample].begin()->first < 0) // if no output is needed for this channel
                continue;
            else if (mapping[sample].size() == 1) // if no fusion is going on simply copy the data
                memcpy(img.bits(sample), bmp->bits[mapping[sample].begin()->first], img.bytesPerChan());
            else // ok, here we fuse
                if (depth == 8 && pixelType == bim::DataFormat::FMT_UNSIGNED)
                fuse_channels_weights<uint8, uint8>(&img, sample, *this, mapping[sample], shift, mult, method);
            else if (depth == 16 && pixelType == bim::DataFormat::FMT_UNSIGNED)
                fuse_channels_weights<uint16, uint16>(&img, sample, *this, mapping[sample], shift, mult, method);
            else if (depth == 32 && pixelType == bim::DataFormat::FMT_UNSIGNED)
                fuse_channels_weights<uint32, uint32>(&img, sample, *this, mapping[sample], shift, mult, method);
            else if (depth == 8 && pixelType == bim::DataFormat::FMT_SIGNED)
                fuse_channels_weights<int8, int8>(&img, sample, *this, mapping[sample], shift, mult, method);
            else if (depth == 16 && pixelType == bim::DataFormat::FMT_SIGNED)
                fuse_channels_weights<int16, int16>(&img, sample, *this, mapping[sample], shift, mult, method);
            else if (depth == 32 && pixelType == bim::DataFormat::FMT_SIGNED)
                fuse_channels_weights<int32, int32>(&img, sample, *this, mapping[sample], shift, mult, method);
            else if (depth == 32 && pixelType == bim::DataFormat::FMT_FLOAT)
                fuse_channels_weights<float32, float32>(&img, sample, *this, mapping[sample], shift, mult, method);
            else if (depth == 64 && pixelType == bim::DataFormat::FMT_FLOAT)
                fuse_channels_weights<float64, float64>(&img, sample, *this, mapping[sample], shift, mult, method);
        } // sample
    }

    // final touches
    img.bmp->i = this->bmp->i;
    img.bmp->i.samples = (bim::uint32)c;
    //out.metadata = this->metadata;
    img.metadata = fuseMetadata(this->metadata, bmp->i.samples, mapping);
    return img;

    /*
    // bring the levels up to the original max
    double o_max = hist.max_value();
    double o_min = hist.min_value();

    ImageHistogram fhist(img);
    double f_max = fhist.max_value();
    double f_min = fhist.min_value();

    double shift = o_min-f_min;
    double mult  = (o_max-o_min)/(f_max-f_min);
    */

    /*
    Image out;
    if (out.alloc( bmp->i.width, bmp->i.height, c, bmp->i.depth, bmp->i.pixelType)==0) {
        norm_copy ( &out, img, shift, mult );

        // final touches
        out.bmp->i = this->bmp->i;
        out.bmp->i.samples = c;
        //out.metadata = this->metadata;
        out.metadata = fuseMetadata( this->metadata, bmp->i.samples, map );
    }
    return out;
    */
}

Image Image::fuseToGrayscale() const {
    std::vector<std::vector<std::pair<int, float>>> mapping;
    std::vector<std::pair<int, float>> gray;
    bool is_rgb = ((this->samples() == 3 && this->imageMode() == bim::ImageModes::IM_RGB) || (this->samples() == 4 && this->imageMode() == bim::ImageModes::IM_RGBA));
    if (is_rgb) {
        gray.push_back(std::make_pair(0, 0.3f));
        gray.push_back(std::make_pair(1, 0.59f));
        gray.push_back(std::make_pair(2, 0.11f));
    } else {
        for (int i = 0; i < (int)this->samples(); ++i)
            gray.push_back(std::make_pair(i, 1.0f));
    }

    mapping.push_back(std::move(gray));
    return fuse(mapping);
}

// in this case we are producing an xRGB image for display, the mapping should contain xRGB values for each image channel
// max value of rgb components defines max contribution, 0 or negative means no contribution
Image Image::fuseToRGB(const std::vector<bim::ColorF32> &mapping, Image::FuseMethod method, ImageHistogram *hist) const {
    std::vector<std::vector<std::pair<int, float>>> map;
    std::vector<std::pair<int, float>> red, green, blue;

    float max_contribution = 0; // used to divide each components contribution
    for (const ColorF32 color : mapping) {
        max_contribution = std::max<float>(max_contribution, color.r);
        max_contribution = std::max<float>(max_contribution, color.g);
        max_contribution = std::max<float>(max_contribution, color.b);
    }

    for (int i = 0; i < (int) mapping.size(); ++i) {
        if (mapping[i].r > 0)
            red.emplace_back(i, mapping[i].r / max_contribution);
        else
            red.emplace_back(-1, mapping[i].r / max_contribution);
        if (mapping[i].g > 0)
            green.emplace_back(i, mapping[i].g / max_contribution);
        else
            green.emplace_back(-1, mapping[i].g / max_contribution);
        if (mapping[i].b > 0)
            blue.emplace_back(i, mapping[i].b / max_contribution);
        else
            blue.emplace_back(-1, mapping[i].b / max_contribution);
    }

    map.push_back(std::move(red));
    map.push_back(std::move(green));
    map.push_back(std::move(blue));
    return fuse(map, method, hist);
}

static Image operation_fusegrey(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    hist->clear(); // dima: should properly modify instead of clearing
    return img.fuseToGrayscale();
}

static Image operation_fusergb(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    Image::FuseMethod fuse_method = Image::FuseMethod::fmAverage;
    if (operations.arguments("-fusemethod").toLowerCase() == "m") fuse_method = Image::FuseMethod::fmMax; // dima: should be moved into command args

    std::vector<xstring> ch = arguments.split(";");
    // trim trailing empty channels
    for (int c = (int)ch.size() - 1; c >= 0; --c) {
        if (ch[c] == "")
            ch.resize(c);
        else
            break;
    }
    std::vector<bim::ColorF32> out_weighted_fuse_channels;
    for (const xstring &token : ch) {
        std::vector<int> cmp = token.splitInt(",");
        cmp.resize(3, 0);
        out_weighted_fuse_channels.push_back(bim::ColorF32::from_RGBA(cmp[0], cmp[1], cmp[2]));
    }

    img = img.fuseToRGB(out_weighted_fuse_channels, fuse_method, hist);
    hist->clear(); // dima: should properly modify instead of clearing
    return img;
}

static Image operation_fusemeta(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    Image::FuseMethod fuse_method = Image::FuseMethod::fmAverage;
    if (operations.arguments("-fusemethod").toLowerCase() == "m") fuse_method = Image::FuseMethod::fmMax; // dima: should be moved into command args

    std::vector<bim::ColorF32> out_weighted_fuse_channels = init_fusion_from_meta(img);
    img = img.fuseToRGB(out_weighted_fuse_channels, fuse_method, hist);
    hist->clear(); // dima: should properly modify instead of clearing
    return img;
}

static Image operation_fuse(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    Image::FuseMethod fuse_method = Image::FuseMethod::fmAverage;
    if (operations.arguments("-fusemethod").toLowerCase() == "m") fuse_method = Image::FuseMethod::fmMax; // dima: should be moved into command args

    std::vector<std::set<int>> out_fuse_channels;
    for (const xstring token : arguments.split(",")) {
        std::set<int> s;
        for (const xstring subtoken : token.split("+"))
            s.insert(subtoken.toInt() - 1);
        out_fuse_channels.push_back(std::move(s));
    }

    if (out_fuse_channels.size() > 0) {
        img = img.fuse(out_fuse_channels);
        hist->clear(); // dima: should properly modify instead of clearing
    }
    return img;
}

static Image operation_fuse6(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    Image::FuseMethod fuse_method = Image::FuseMethod::fmAverage;
    if (operations.arguments("-fusemethod").toLowerCase() == "m") fuse_method = Image::FuseMethod::fmMax; // dima: should be moved into command args

    std::vector<int> ch = arguments.splitInt(",");
    ch.resize(7, 0);
    std::set<int> rv, gv, bv;
    rv.insert(ch[0] - 1);
    rv.insert(ch[3] - 1);
    rv.insert(ch[4] - 1);
    rv.insert(ch[6] - 1);
    gv.insert(ch[1] - 1);
    gv.insert(ch[3] - 1);
    gv.insert(ch[5] - 1);
    gv.insert(ch[6] - 1);
    bv.insert(ch[2] - 1);
    bv.insert(ch[4] - 1);
    bv.insert(ch[5] - 1);
    bv.insert(ch[6] - 1);

    std::vector<std::set<int>> out_fuse_channels;
    out_fuse_channels.push_back(std::move(rv));
    out_fuse_channels.push_back(std::move(gv));
    out_fuse_channels.push_back(std::move(bv));

    if (out_fuse_channels.size() > 0) {
        img = img.fuse(out_fuse_channels);
        hist->clear(); // dima: should properly modify instead of clearing
    }
    return img;
}

//------------------------------------------------------------------------------------
//Append channels
//------------------------------------------------------------------------------------

static void appendChannelMetadata(TagMap &md, const TagMap &i2md, uint64 c1, uint64 c2) {
    for (uint i = 0; i < c2; ++i) {
        xstring key1 = xstring::xprintf("channel_%u_name", c1 + i);
        xstring key2 = xstring::xprintf("channel_%u_name", i);
        if (i2md.hasKey(key2))
            md.set_value(key1, i2md.get_value(key2));
    }
}


Image Image::appendChannels(const Image &i2) const {
    Image img;
    if (bmp == NULL) return img;

    if (this->width() != i2.width() || this->height() != i2.height() ||
        this->depth() != i2.depth() || this->pixelType() != i2.pixelType()) return img;
    const uint64 c = this->samples() + i2.samples();

    if (img.alloc(bmp->i.width, bmp->i.height, c, bmp->i.depth, bmp->i.pixelType) == 0) {
        int out_sample = 0;
        for (uint64 sample = 0; sample < this->samples(); ++sample) {
            memcpy(img.bits(out_sample), this->bits(sample), this->bytesPerChan());
            ++out_sample;
        }
        for (uint64 sample = 0; sample < i2.samples(); ++sample) {
            memcpy(img.bits(out_sample), i2.bits(sample), i2.bytesPerChan());
            ++out_sample;
        }
    }

    img.bmp->i = this->bmp->i;
    img.bmp->i.samples = (bim::uint32)c;
    img.bmp->i.imageMode = bim::ImageModes::IM_MULTI;

    img.metadata = this->metadata;
    appendChannelMetadata(img.metadata, i2.metadata, this->samples(), i2.samples());
    img.metadata.set_value(bim::IMAGE_MODE, bim::ICC_TAGS_COLORSPACE_MULTICHANNEL);
    img.metadata.set_value(bim::ICC_TAGS_COLORSPACE, bim::ICC_TAGS_COLORSPACE_MULTICHANNEL);
    return img;
}

void Image::copy(const Image &i2) {
    if (this->width() != i2.width() || this->height() != i2.height() ||
        this->depth() != i2.depth() || this->pixelType() != i2.pixelType()) return;

    uint64 samples = std::min(this->samples(), i2.samples());
    for (uint64 sample = 0; sample < samples; ++sample) {
        memcpy(this->bits(sample), i2.bits(sample), this->bytesPerChan());
    }

    this->bmp->i = i2.bmp->i = this->bmp->i;
    this->metadata = i2.metadata;
}

//------------------------------------------------------------------------------------
//Append channels
//------------------------------------------------------------------------------------

void Image::updateGeometry(const uint64 z, const uint64 t, const uint64 c) {
    if (bmp == NULL) return;

    constexpr unsigned int uint_max = std::numeric_limits<unsigned int>::max();
    assert(z <= uint_max && "Image::z too large for metadata");
    assert(t <= uint_max && "Image::t too large for metadata");
    assert(c <= uint_max && "Image::c too large for metadata");

    if (z > 0) {
        bmp->i.number_z = z;
        metadata.set_value("image_num_z", static_cast<unsigned>(z));
    }

    if (t > 0) {
        bmp->i.number_t = t;
        metadata.set_value("image_num_t", static_cast<unsigned>(t));
    }

    if (c > 1) {
        bmp->i.samples = (bim::uint32)c;
        metadata.set_value("image_num_c", static_cast<unsigned>(c));
    }
}

void Image::updateResolution(const std::array<double, 4> &r) {
    if (bmp == NULL) return;
    bmp->i.xRes = r[0];
    bmp->i.yRes = r[1];
    bmp->i.resUnits = bim::ResolutionUnits::RES_um;

    metadata.set_value("pixel_resolution_x", r[0]);
    metadata.set_value("pixel_resolution_y", r[1]);
    metadata.set_value("pixel_resolution_z", r[2]);
    metadata.set_value("pixel_resolution_t", r[3]);
    metadata.set_value("pixel_resolution_unit_x", "microns");
    metadata.set_value("pixel_resolution_unit_y", "microns");
    metadata.set_value("pixel_resolution_unit_z", "microns");
    metadata.set_value("pixel_resolution_unit_t", "seconds");
}


//------------------------------------------------------------------------------------
// Filters
//------------------------------------------------------------------------------------

template<typename T>
void deinterlace_operation(Image *img, const Image::DeinterlaceMethod &method) {

    const uint64 w = img->width();
    const uint64 h = img->height();
    const uint64 s = img->samples();
    const uint64 offset = 1;

    if (h < 2 || w < offset)
        return;

    for (uint64 sample = 0; sample < s; ++sample) {
#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (h > BIM_OMP_FOR2)
        for (int64 y = 0; y < (bim::int64)(h - 2); y += 2) {
            T *src1 = (T *)img->scanLine(sample, y);
            T *src2 = (T *)img->scanLine(sample, y + 1);
            T *src3 = (T *)img->scanLine(sample, y + 2);

            if (method == Image::DeinterlaceMethod::deOdd) {
                for (uint64 x = 0; x < w; ++x) {
                    src2[x] = src1[x];
                }
            } else if (method == Image::DeinterlaceMethod::deEven) {
                for (uint64 x = 0; x < w; ++x) {
                    src1[x] = src2[x];
                }
            } else if (method == Image::DeinterlaceMethod::deAverage) {
                for (uint64 x = 0; x < w; ++x) {
                    src2[x] = (src1[x] + src3[x]) / 2;
                }
            } else if (method == Image::DeinterlaceMethod::deOffset) {
                for (uint64 x = 0; x < w - offset; ++x) {
                    src1[x] = src1[x + offset];
                }
            }
        }
    } // sample
}

void Image::deinterlace(const Image::DeinterlaceMethod &method) {

    if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        deinterlace_operation<uint8>(this, method);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        deinterlace_operation<uint16>(this, method);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        deinterlace_operation<uint32>(this, method);
    else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        deinterlace_operation<int8>(this, method);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        deinterlace_operation<int16>(this, method);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        deinterlace_operation<int32>(this, method);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        deinterlace_operation<float32>(this, method);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        deinterlace_operation<float64>(this, method);
}

static Image operation_deinterlace(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    Image::DeinterlaceMethod deinterlace_method = Image::DeinterlaceMethod::deAverage;
    if (arguments.toLowerCase() == "odd") deinterlace_method = Image::DeinterlaceMethod::deOdd;
    if (arguments.toLowerCase() == "even") deinterlace_method = Image::DeinterlaceMethod::deEven;
    if (arguments.toLowerCase() == "avg") deinterlace_method = Image::DeinterlaceMethod::deAverage;
    if (arguments.toLowerCase() == "offset") deinterlace_method = Image::DeinterlaceMethod::deOffset;

    img.deinterlace(deinterlace_method);
    return img;
}

#ifdef BIM_USE_EIGEN


template<typename T>
Image do_convolution(const Image *in, const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> &filter) {
    /*
        bim::uint64 w = in->width();
        bim::uint64 h = in->height();
        bim::uint64 s = in->samples();
        Image out = in->deepCopy();

        for (unsigned int sample=0; sample<s; ++sample ) {
            #pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (h>BIM_OMP_FOR2)
            for (unsigned int y=0; y<h-2; y+=2 ) {
                T *src = (T*) img->scanLine( sample, y );

                for (unsigned int x=0; x<w; ++x) {
                    src2[x] = src1[x];
                }

            }
        } // sample



        unsigned long x, y, xx, yy;
        long i, j;
        long height2=filter.rows()/2;
        long width2=filter.cols()/2;
        double tmp;


        writeablePixels pix_plane = WriteablePixels();
        for (x = 0; x < width; ++x) {
            for (y = 0; y < height; ++y) {
                tmp=0.0;
                for (i = -width2; i <= width2; ++i) {
                    xx=x+i;
                    if (xx < width && xx >= 0) {
                        for(j = -height2; j <= height2; ++j) {
                            yy=y+j;
                            if (yy >= 0 && yy < height) {
                                tmp += filter (j+height2, i+width2) * copy_pix_plane(yy,xx);
                            }
                        }
                    }
                }
                pix_plane (y,x) = stats.add(tmp);
            }
        }

      */
    return Image();
}

Image Image::convolve(const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> &filter) const {
    if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return do_convolution<uint8>(this, filter);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return do_convolution<uint16>(this, filter);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return do_convolution<uint32>(this, filter);
    else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return do_convolution<int8>(this, filter);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return do_convolution<int16>(this, filter);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return do_convolution<int32>(this, filter);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        return do_convolution<float32>(this, filter);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        return do_convolution<float64>(this, filter);
    else
        return Image();
}

#endif //BIM_USE_EIGEN


template<typename T>
Image get_otsu(const Image *in) {
    /*

        bim::uint64 w = in->width();
        bim::uint64 h = in->height();
        bim::uint64 s = in->samples();
        Image out = in->deepCopy();

        for (unsigned int sample=0; sample<s; ++sample ) {
            for (unsigned int y=0; y<h-2; y+=2 ) {
                T *src = (T*) img->scanLine( sample, y );

                for (unsigned int x=0; x<w; ++x) {
                    src2[x] = src1[x];
                }

            }
        } // sample

        #define OTSU_LEVELS 1024
         // binarization by Otsu's method based on maximization of inter-class variance
        double hist[OTSU_LEVELS];
        double omega[OTSU_LEVELS];
        double myu[OTSU_LEVELS];
        double max_sigma, sigma[OTSU_LEVELS]; // inter-class variance
        int i;
        int threshold;
        double min_val,max_val; // pixel range

        if (!dynamic_range) {
            histogram(hist,OTSU_LEVELS,true);
            min_val = 0.0;
            max_val = pow(2.0,bits)-1;
        } else {
            // to keep this const method from modifying the object, we use GetStats on a local Moments2 object
            Moments2 local_stats;
            GetStats (local_stats);
            min_val = local_stats.min();
            max_val = local_stats.max();
            histogram(hist,OTSU_LEVELS,false);
        }

        // omega & myu generation
        omega[0] = hist[0] / (width * height);
        myu[0] = 0.0;
        for (i = 1; i < OTSU_LEVELS; i++) {
            omega[i] = omega[i-1] + (hist[i] / (width * height));
            myu[i] = myu[i-1] + i*(hist[i] / (width * height));
        }

        // maximization of inter-class variance
        threshold = 0;
        max_sigma = 0.0;
        for (i = 0; i < OTSU_LEVELS-1; i++) {
            if (omega[i] != 0.0 && omega[i] != 1.0)
                sigma[i] = pow(myu[OTSU_LEVELS-1]*omega[i] - myu[i], 2) /
                    (omega[i]*(1.0 - omega[i]));
            else
                sigma[i] = 0.0;
            if (sigma[i] > max_sigma) {
                max_sigma = sigma[i];
                threshold = i;
            }
        }

        // threshold is a histogram index - needs to be scaled to a pixel value.
        return ( (((double)threshold / (double)(OTSU_LEVELS-1)) * (max_val - min_val)) + min_val );
    */

    return Image();
}

//------------------------------------------------------------------------------
// process based on a set of operations
// currently not 100% of operations are supported
//------------------------------------------------------------------------------

#ifdef BIM_USE_TRANSFORMS
Image operation_enhancemeta(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    //if (this->get_metadata_tag("DICOM/Rescale Type (0028,1054)", "") != "HU") continue;

    // read window center and width from the metadata
    //xstring modality = img.get_metadata_tag("DICOM/Modality (0008,0060)", "");
    //double wnd_center = img.get_metadata_tag_double("DICOM/Window Center (0028,1050)", 0.0);
    //double wnd_width = img.get_metadata_tag_double("DICOM/Window Width (0028,1051)", 0.0);
    //double slope = img.get_metadata_tag_double("DICOM/Rescale Slope (0028,1053)", 1.0);
    //double intercept = img.get_metadata_tag_double("DICOM/Rescale Intercept (0028,1052)", -1024.0);
    xstring modality = img.get_metadata_tag("DICOM/Modality", "");
    double wnd_center = img.get_metadata_tag_double("DICOM/Window Center", 0.0);
    double wnd_width = img.get_metadata_tag_double("DICOM/Window Width", 0.0);
    double slope = img.get_metadata_tag_double("DICOM/Rescale Slope", 1.0);
    double intercept = img.get_metadata_tag_double("DICOM/Rescale Intercept", -1024.0);
    if (modality != "CT" || wnd_width == 0) return img;

    const uint64 depth = img.depth();
    DataFormat pf = img.pixelType();
    if (depth < 16 || pf == bim::DataFormat::FMT_UNSIGNED) return img;

    if (!img.transform_hounsfield_inplace(slope, intercept)) {
        img = img.transform_hounsfield(slope, intercept);
    }
    img = img.enhance_hounsfield(depth, pf, wnd_center, wnd_width);
    img = img.convertToDepth(depth, bim::Lut::ltLinearDataRange, bim::DataFormat::FMT_UNSIGNED);
    return img;
}
#endif

#ifdef BIM_USE_TRANSFORMS
Image operation_filter(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_transform_color(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_deconvolve_rgb(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_mix_brightfield_rgb(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_transform(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_hounsfield(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_lut_2d(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_fuse_lut2d(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_unmix(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
#endif
#ifdef BIM_USE_FILTERS
Image operation_superpixels(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_log(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_gaussian(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_median(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_region_grow(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_mrfilter(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
Image operation_flatfield(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c);
#endif

static Image operation_meta_remove(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    bim::TagMap *meta = (bim::TagMap *)img.meta();
    for (const xstring tag : arguments.split(",")) {
        meta->delete_tag(tag);
    }
    return img;
}

static Image operation_meta_keep(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    // rapid lookup for tag names
    bim::TagMap keys;
    for (const xstring tag : arguments.split(",")) {
        keys.append_tag(tag, "");
    }

    // remove all tags except given
    bim::TagMap *meta = (bim::TagMap *)img.meta();
    bim::TagMap::iterator it = meta->begin();
    while (it != meta->end()) {
        if (!keys.hasKey((*it).first)) {
            bim::TagMap::iterator delit = it;
            ++it;
            meta->erase(delit);
        } else {
            ++it;
        }
    }
    return img;
}

std::map<std::string, ImageModifierProc> Image::create_modifiers() {
    std::map<std::string, ImageModifierProc> ops;
    ops["-stretch"] = operation_stretch;
    ops["-levels"] = operation_levels;
    ops["-brightnesscontrast"] = operation_brightnesscontrast;
    ops["-depth"] = operation_depth;
    ops["-norm"] = operation_norm;
    ops["-remap"] = operation_remap;
    ops["-fusegrey"] = operation_fusegrey;
    ops["-fusergb"] = operation_fusergb;
    ops["-fusemeta"] = operation_fusemeta;
    ops["-display"] = operation_fusemeta;
    ops["-fuse"] = operation_fuse;
    ops["-fuse6"] = operation_fuse6;

    ops["-deinterlace"] = operation_deinterlace;
    ops["-roi"] = operation_roi;
    ops["-resize"] = operation_resize;
    ops["-resample"] = operation_resample;
    ops["-rotate"] = operation_rotate;
    ops["-mirror"] = operation_mirror;
    ops["-flip"] = operation_flip;
    ops["-negative"] = operation_negative;
    ops["-absolute"] = operation_absolute;
    ops["-threshold"] = operation_threshold; //ops["-threshold"] = operation_stretch;
    ops["-correction_bf"] = operation_correction_brightfield;

#ifdef BIM_USE_TRANSFORMS
    ops["-transform_color"] = operation_transform_color;
    ops["-deconvolve_rgb"] = operation_deconvolve_rgb;
    ops["-mixbf"] = operation_mix_brightfield_rgb;
    ops["-transform"] = operation_transform;
    ops["-hounsfield"] = operation_hounsfield;
    ops["-enhancemeta"] = operation_enhancemeta;
    ops["-icc-load"] = operation_icc_load;
    ops["-icc-save"] = operation_icc_save;
    ops["-icc-transform-name"] = operation_transform_icc_name;
    ops["-icc-transform-file"] = operation_transform_icc_file;
    ops["-lut-2d"] = operation_lut_2d;
    ops["-fuse-lut2d"] = operation_fuse_lut2d;
    ops["-unmix"] = operation_unmix;
#endif

#ifdef BIM_USE_FILTERS
#ifdef BIM_USE_TRANSFORMS
    ops["-filter"] = operation_filter;
#endif
    ops["-superpixels"] = operation_superpixels;
    ops["-log"] = operation_log;
    ops["-gaussian"] = operation_gaussian;
    ops["-median"] = operation_median;
    ops["-mrfilter"] = operation_mrfilter;
    ops["-flatfield"] = operation_flatfield;
    ops["-regiongrow"] = operation_region_grow;
#endif

    // metadata
    ops["-meta-keep"] = operation_meta_keep;
    ops["-meta-remove"] = operation_meta_remove;

    return ops;
}

void Image::process(const xoperations &operations, ImageHistogram *_hist, XConf *c) {
    ImageHistogram hist;
    if (!_hist)
        hist.fromImage(*this);
    else
        hist = *_hist;
    XConf cc;
    if (!c) c = &cc;

    for (const xoperation &op : operations) {
        const std::string &operation = op.first;
        const xstring &arguments = op.second;
        const auto fit = modifiers.find(operation);
        if (fit != modifiers.cend()) {
            c->print(xstring::xprintf("About to run %s", operation.c_str()), 2);
            c->timerStart();
            ImageModifierProc f = fit->second;
            *this = (*f)(*this, arguments, operations, &hist, &cc);
            c->printElapsed(xstring::xprintf("%s ran in: ", operation.c_str()), 2);
        }
    }
}

//*****************************************************************************
//  pixel values
//*****************************************************************************

double Image::getPixelValue(uint64 sample, bim::uint64 x, bim::uint64 y) const {
    if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return this->pixel<uint8>(sample, x, y);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return this->pixel<uint16>(sample, x, y);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return this->pixel<uint32>(sample, x, y);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        return (double)this->pixel<uint64>(sample, x, y);
    else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return this->pixel<int8>(sample, x, y);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return this->pixel<int16>(sample, x, y);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return this->pixel<int32>(sample, x, y);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        return (double)this->pixel<int64>(sample, x, y);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        return this->pixel<float32>(sample, x, y);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        return this->pixel<float64>(sample, x, y);
    else
        return std::numeric_limits<double>::quiet_NaN();
}

double Image::getPixelValue(uint64 sample, double x, double y) const {
    bim::int64 xx = bim::round<bim::int64>(x);
    bim::int64 yy = bim::round<bim::int64>(y);
    if (xx < 0 || yy < 0)
        return std::numeric_limits<double>::quiet_NaN();
    return this->getPixelValue(sample, (bim::uint64) xx, (bim::uint64) yy);
}

void Image::setPixelValue(uint64 sample, bim::uint64 x, bim::uint64 y, const double &v) {
    if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        this->set_pixel<uint8>(sample, x, y, v);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        this->set_pixel<uint16>(sample, x, y, v);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        this->set_pixel<uint32>(sample, x, y, v);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_UNSIGNED)
        this->set_pixel<uint64>(sample, x, y, v);
    else if (bmp->i.depth == 8 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        this->set_pixel<int8>(sample, x, y, v);
    else if (bmp->i.depth == 16 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        this->set_pixel<int16>(sample, x, y, v);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        this->set_pixel<int32>(sample, x, y, v);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_SIGNED)
        this->set_pixel<int64>(sample, x, y, v);
    else if (bmp->i.depth == 32 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        this->set_pixel<float32>(sample, x, y, v);
    else if (bmp->i.depth == 64 && bmp->i.pixelType == bim::DataFormat::FMT_FLOAT)
        this->set_pixel<float64>(sample, x, y, v);
}

void Image::setPixelValue(uint64 sample, double x, double y, const double &v) {
    bim::int64 xx = bim::round<bim::int64>(x);
    bim::int64 yy = bim::round<bim::int64>(y);
    if (xx < 0 || yy < 0) return;
    this->setPixelValue(sample, (bim::uint64) xx, (bim::uint64) yy, v);
}

//*****************************************************************************
//  ImageHistogram
//*****************************************************************************

ImageHistogram::ImageHistogram(const uint64 channels, const uint64 bpp, const DataFormat fmt)
    : histograms(channels)
    , channel_mode(Histogram::cmSeparate)
{
    if (bpp > 0) {
        for (Histogram &hist : histograms)
            hist.init(bpp, fmt);
    }
}

ImageHistogram::ImageHistogram(const Image &img, const Image *mask)
    : channel_mode(Histogram::cmSeparate)
{
    fromImage(img, mask);
}

ImageHistogram::~ImageHistogram() = default;

// mask has to be an 8bpp image where pixels >128 belong to the object of interest
// if mask has 1 channel it's going to be used for all channels, otherwise they should match
void ImageHistogram::fromImage(const Image &img, const Image *mask) {
    if (mask && mask->depth() != 8) return;
    if (mask && mask->samples() > 1 && mask->samples() < img.samples()) return;

    histograms.resize(img.samples());

    if (this->channel_mode == Histogram::cmSeparate) {
        if (mask)
            for (uint64 c = 0; c < img.samples(); ++c)
                histograms[c].newData(img.depth(), img.bits(c), img.numPixels(), img.pixelType(), (unsigned char *)mask->bits(c));
        else
            for (uint64 c = 0; c < img.samples(); ++c)
                histograms[c].newData(img.depth(), img.bits(c), img.numPixels(), img.pixelType());
    } else { // for combined channel histogram computation
        if (mask) {
            histograms[0].newData(img.depth(), img.bits(0), img.numPixels(), img.pixelType(), (unsigned char *)mask->bits(0));
            for (uint64 c = 1; c < img.samples(); ++c)
                histograms[0].addData(img.bits(c), img.numPixels(), (unsigned char *)mask->bits(c));
        } else {
            histograms[0].newData(img.depth(), img.bits(0), img.numPixels(), img.pixelType());
            for (uint64 c = 1; c < img.samples(); ++c)
                histograms[0].addData(img.bits(c), img.numPixels());
        }
        for (uint64 c = 1; c < img.samples(); ++c)
            histograms[c] = histograms[0];
    }
}

void ImageHistogram::updateStats(const Image &img, const Image *mask) {
    if (mask && mask->depth() != 8) return;
    if (mask && mask->samples() > 1 && mask->samples() < img.samples()) return;
    if (histograms.size() < img.samples()) return;

    if (this->channel_mode == Histogram::cmSeparate) {
        if (mask)
            for (uint64 c = 0; c < img.samples(); ++c)
                histograms[c].updateStats(img.bits(c), img.numPixels(), (unsigned char *)mask->bits(c));
        else
            for (uint64 c = 0; c < img.samples(); ++c)
                histograms[c].updateStats(img.bits(c), img.numPixels());
    } else { // for combined channel histogram computation
        if (mask) {
            histograms[0].updateStats(img.bits(0), img.numPixels(), (unsigned char *)mask->bits(0));
            for (uint64 c = 1; c < img.samples(); ++c)
                histograms[0].updateStats(img.bits(c), img.numPixels(), (unsigned char *)mask->bits(c));
        } else {
            histograms[0].updateStats(img.bits(0), img.numPixels());
            for (uint64 c = 1; c < img.samples(); ++c)
                histograms[0].updateStats(img.bits(c), img.numPixels());
        }
        for (uint64 c = 1; c < img.samples(); ++c)
            histograms[c] = histograms[0];
    }
}

void ImageHistogram::addData(const Image &img, const Image *mask) {
    if (mask && mask->depth() != 8) return;
    if (mask && mask->samples() > 1 && mask->samples() < img.samples()) return;
    if (histograms.size() < img.samples()) return;

    if (this->channel_mode == Histogram::cmSeparate) {
        if (mask)
            for (uint64 c = 0; c < img.samples(); ++c)
                histograms[c].addData(img.bits(c), img.numPixels(), (unsigned char *)mask->bits(c));
        else
            for (uint64 c = 0; c < img.samples(); ++c)
                histograms[c].addData(img.bits(c), img.numPixels());
    } else { // for combined channel histogram computation
        if (mask) {
            histograms[0].addData(img.bits(0), img.numPixels(), (unsigned char *)mask->bits(0));
            for (uint64 c = 1; c < img.samples(); ++c)
                histograms[0].addData(img.bits(c), img.numPixels(), (unsigned char *)mask->bits(c));
        } else {
            histograms[0].addData(img.bits(0), img.numPixels());
            for (uint64 c = 1; c < img.samples(); ++c)
                histograms[0].addData(img.bits(c), img.numPixels());
        }
        for (uint64 c = 1; c < img.samples(); ++c)
            histograms[c] = histograms[0];
    }
}


//------------------------------------------------------------------------------
// I/O
//------------------------------------------------------------------------------

/*
Histogram binary content:
0x00 'BIM1' - 4 bytes header
0x04 'IHS1' - 4 bytes spec
0x08 NUM    - 1xUINT32 number of histograms in the file (num channels in the image)
0xXX        - histograms
*/

const char IHistogram_mgk[4] = { 'B', 'I', 'M', '1' };
const char IHistogram_spc[4] = { 'I', 'H', 'S', '1' };

bool ImageHistogram::to(const std::string &fileName) {
    std::ofstream f(fileName.c_str(), std::ios_base::binary);
    return this->to(&f);
}

bool ImageHistogram::to(std::ostream *s) {
    // write header
    s->write(IHistogram_mgk, sizeof(IHistogram_mgk));
    s->write(IHistogram_spc, sizeof(IHistogram_spc));

    // write number of histograms
    uint32 sz = (uint32)this->histograms.size();
    s->write((const char *)&sz, sizeof(uint32));

    // write histograms
    for (Histogram &hist : histograms)
        hist.to(s);

    s->flush();
    return true;
}

bool ImageHistogram::from(const std::string &fileName) {
    std::ifstream f(fileName.c_str(), std::ios_base::binary);
    return this->from(&f);
}

bool ImageHistogram::from(std::istream *s) {
    // read header
    char hts_hdr[sizeof(IHistogram_mgk)];
    char hts_spc[sizeof(IHistogram_spc)];

    s->read(hts_hdr, sizeof(IHistogram_mgk));
    if (memcmp(hts_hdr, IHistogram_mgk, sizeof(IHistogram_mgk)) != 0) return false;

    s->read(hts_spc, sizeof(IHistogram_spc));
    if (memcmp(hts_spc, IHistogram_spc, sizeof(IHistogram_spc))) return false;

    // read number of histograms
    uint32 sz;
    s->read((char *)&sz, sizeof(uint32));
    this->histograms.resize(sz);

    // read histograms
    for (Histogram &hist : histograms)
        hist.from(s);

    return true;
}

bool ImageHistogram::toXML(const std::string &fileName) {
    std::ofstream f(fileName.c_str(), std::ios_base::binary);
    return this->toXML(&f);
}

inline void write_string(std::ostream *s, const std::string &str) {
    s->write(str.c_str(), str.size());
}

bool ImageHistogram::toXML(std::ostream *s) {
    // write header
    write_string(s, "<resource>");

    // write histograms
    for (uint64 c = 0; c < histograms.size(); ++c) {
        write_string(s, xstring::xprintf("<histogram name=\"channel\" value=\"%d\">", c));
        this->histograms[c].toXML(s);
        write_string(s, "</histogram>");
    }
    write_string(s, "</resource>");

    s->flush();
    return true;
}

double ImageHistogram::max_value() const {
    double v = this->histograms[0].max_value();
    for (uint64 c = 1; c < histograms.size(); ++c)
        v = std::max(v, this->histograms[c].max_value());
    return v;
}

double ImageHistogram::min_value() const {
    double v = this->histograms[0].min_value();
    for (uint64 c = 1; c < histograms.size(); ++c)
        v = std::min(v, this->histograms[c].min_value());
    return v;
}

//*****************************************************************************
//  ImageLut
//*****************************************************************************

void ImageLut::init(const ImageHistogram &in, const ImageHistogram &out) {
    if (in.size() != out.size()) return;
    luts.resize(in.size());
    for (uint64 i = 0; i < in.size(); ++i) {
        luts[i].init(*in[i], *out[i]);
    }
}

void ImageLut::init(const ImageHistogram &in, const ImageHistogram &out, Lut::LutType type, std::vector<LutParameters> *args) {
    if (in.size() != out.size()) return;
    luts.resize(in.size());
    for (uint64 i = 0; i < in.size(); ++i) {
        LutParameters *p = args ? &args->at(0) : NULL;
        if (args && args->size() > i) p = &args->at(i);
        luts[i].init(*in[i], *out[i], type, p);
    }
}

void ImageLut::init(const ImageHistogram &in, const ImageHistogram &out, Lut::LutGenerator custom_generator, std::vector<LutParameters> *args) {
    if (in.size() != out.size()) return;
    luts.resize(in.size());
    for (uint64 i = 0; i < in.size(); ++i) {
        LutParameters *p = args ? &args->at(0) : NULL;
        if (args && args->size() > i) p = &args->at(i);
        luts[i].init(*in[i], *out[i], custom_generator, p);
    }
}
