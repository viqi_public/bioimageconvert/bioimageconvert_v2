/*******************************************************************************

  Implementation of the Image Filters

  Copyright: ViQi Inc, 2023
  Notes: Contains proprietary implementations

  Authors: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>
           Mike Quinn <mquinn@viqi.org>

  History:
    2011-05-11 08:32:12 - First creation

  ver: 1

*******************************************************************************/

#ifdef BIM_USE_FILTERS
#pragma message("bim::Image: Filters")

#include "bim_image.h"

#include <algorithm>
#include <cmath>
#include <cstring>
#include <functional>
#include <limits>
#include <set>
#include <vector>

#include "mrs_filter.h"
#include "slic.h"

using namespace bim;


//------------------------------------------------------------------------------------
// Edge
//------------------------------------------------------------------------------------

template<typename T>
static void edge_filter(const Image &in, Image &out) {
    const uint64 w = in.width();
    const uint64 h = in.height();

    if (w <= 2 || h <= 2) return;
    if (w != out.width()) return;
    if (h != out.height()) return;
    if (in.samples() != out.samples()) return;
    if (in.depth() != out.depth()) return;

    for (uint64 sample = 0; sample < in.samples(); sample++) {
#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (h > BIM_OMP_FOR2)
        for (bim::int64 y = 1; y < (bim::int64)h - 1; ++y) {
            T *src_m1 = (T *)in.scanLine(sample, y - 1);
            T *src = (T *)in.scanLine(sample, y);
            T *src_p1 = (T *)in.scanLine(sample, y + 1);
            T *dst = (T *)out.scanLine(sample, y);
            for (uint64 x = 1; (x + 1) < w; ++x) {
                T max_y = (T)bim::max<double>(fabs((double)src[x] - src_m1[x]), fabs((double)src[x] - src_p1[x]));
                T max_x = (T)bim::max<double>(fabs((double)src[x] - src[x - 1]), fabs((double)src[x] - src[x + 1]));
                dst[x] = bim::max<T>(max_x, max_y);
            }
        }
    } // for samples
}

Image Image::filter_edge() const {
    Image out = this->deepCopy(true);
    out.fill(0);

    if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
        edge_filter<bim::uint8>(*this, out);
    else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
        edge_filter<bim::uint16>(*this, out);
    else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
        edge_filter<bim::uint32>(*this, out);
    else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
        edge_filter<bim::uint64>(*this, out);
    else if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
        edge_filter<bim::int8>(*this, out);
    else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
        edge_filter<bim::int16>(*this, out);
    else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
        edge_filter<bim::int32>(*this, out);
    else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
        edge_filter<bim::int64>(*this, out);
    else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
        edge_filter<bim::float32>(*this, out);
    else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
        edge_filter<bim::float64>(*this, out);

    return out;
}

//------------------------------------------------------------------------------------
// Superpixels
//------------------------------------------------------------------------------------

// regionSize in pixels, regularization 0-1, with 1 the shape is most regular
Image Image::superpixels(bim::uint64 regionSize, float regularization, float min_size_ratio) const {
    Image out(this->width(), this->height(), 32, 1, bim::DataFormat::FMT_UNSIGNED);

    bim::uint32 *seg = (bim::uint32 *)out.bits(0);
    bim::uint64 minRegionSize = (bim::uint64)bim::round<double>(regionSize * min_size_ratio);

    regularization = regularization * (regionSize * regionSize);

    if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
        slic_segment<bim::uint8, float>(seg, this, this->width(), this->height(), this->samples(), regionSize, regularization, minRegionSize);
    else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
        slic_segment<bim::uint16, float>(seg, this, this->width(), this->height(), this->samples(), regionSize, regularization, minRegionSize);
    else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
        slic_segment<bim::uint32, float>(seg, this, this->width(), this->height(), this->samples(), regionSize, regularization, minRegionSize);
    else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
        slic_segment<bim::uint64, float>(seg, this, this->width(), this->height(), this->samples(), regionSize, regularization, minRegionSize);
    else if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
        slic_segment<bim::int8, float>(seg, this, this->width(), this->height(), this->samples(), regionSize, regularization, minRegionSize);
    else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
        slic_segment<bim::int16, float>(seg, this, this->width(), this->height(), this->samples(), regionSize, regularization, minRegionSize);
    else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
        slic_segment<bim::int32, float>(seg, this, this->width(), this->height(), this->samples(), regionSize, regularization, minRegionSize);
    else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
        slic_segment<bim::int64, float>(seg, this, this->width(), this->height(), this->samples(), regionSize, regularization, minRegionSize);
    else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
        slic_segment<bim::float32, float>(seg, this, this->width(), this->height(), this->samples(), regionSize, regularization, minRegionSize);
    else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
        slic_segment<bim::float64, float>(seg, this, this->width(), this->height(), this->samples(), regionSize, regularization, minRegionSize);

    return out;
}

Image operation_superpixels(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    int superpixels = 0;
    float superpixels_regularization = 0.0;
    std::vector<double> vals = arguments.splitDouble(",", 0.0);
    if (vals.size() > 0)
        superpixels = (int)vals[0];
    if (vals.size() > 1)
        superpixels_regularization = (float)vals[1];
    float min_size_ratio = 0.7f;
    if (vals.size() > 2)
        min_size_ratio = (float)vals[2];
    if (superpixels > 0)
        return img.superpixels(superpixels, superpixels_regularization, min_size_ratio);
    return img;
}


//------------------------------------------------------------------------------------
// gaussian and LoG
//------------------------------------------------------------------------------------

template<typename T, typename Tw>
Tw vec_sum(const std::vector<T> &vec) {
    // sum the elements of a 1D array
    Tw outsum = 0.0;
    for (size_t i = 0; i < vec.size(); ++i) {
        outsum += vec[i];
    }
    return outsum;
}

// create a 1D Gaussian filter
template<typename T, typename Tw>
void create_gaussian_filter_1d(const Tw &sigma, std::vector<T> &kernel_out) {

    const int ksize = (const int)kernel_out.size();
    int gsize = (ksize - 1) / 2;
    Tw xval = (Tw)-gsize;

    // create kernel flipped right away
    for (int i = ksize - 1; i >= 0; --i) {
        kernel_out[i] = (T)exp(-xval * xval / (2.0 * sigma * sigma));
        xval += 1.0;
    }

    T maxval = bim::max<T>(kernel_out);
    for (size_t i = 0; i < ksize; ++i) {
        kernel_out[i] = (kernel_out[i] < maxval * std::numeric_limits<T>::epsilon()) ? (T)0.0 : kernel_out[i];
    }

    Tw sumk = vec_sum<T, Tw>(kernel_out);
    if (sumk != 0.0) {
        for (size_t i = 0; i < ksize; ++i) {
            kernel_out[i] = kernel_out[i] / sumk;
        }
    }
}

// create a 1D LoG filter
template<typename T, typename Tw>
void create_log_filter_1d(const Tw sigma, const std::vector<T> &gaussian, std::vector<T> &kernel_out) {

    const int ksize = (const int)kernel_out.size();
    int gsize = (ksize - 1) / 2;
    Tw xval = (Tw)-gsize;

    memcpy(&kernel_out[0], &gaussian[0], ksize * sizeof(T));

    // create kernel flipped right away
    xval = (Tw)-gsize;
    for (int i = ksize - 1; i >= 0; --i) {
        kernel_out[i] = kernel_out[i] * (xval * xval - 2 * sigma * sigma) / (sigma * sigma * sigma * sigma);
        xval += 1.0;
    }

    Tw norm = vec_sum<T, Tw>(kernel_out) / ksize;
    for (size_t i = 0; i < ksize; ++i) {
        kernel_out[i] = kernel_out[i] - norm;
    }
}

template<typename T, typename Tw>
void create_filter_kernels(const Tw sigma,
                           std::vector<T> &LoGx, std::vector<T> &Gx, std::vector<T> &LoGy, std::vector<T> &Gy) {

    // create the 1D LoG and Gaussian kernels
    create_gaussian_filter_1d<T, Tw>(sigma, Gx);
    create_gaussian_filter_1d<T, Tw>(sigma, Gy);
    create_log_filter_1d<T, Tw>(sigma, Gx, LoGx);
    create_log_filter_1d<T, Tw>(sigma, Gy, LoGy);
}

const unsigned int PADDING_MODE_ZERO = 0;
const unsigned int PADDING_MODE_REFLECTION = 1;

template<typename T>
void pad_array(const size_t sz, const size_t padding, const T *a_in, T *a_out, int mode = PADDING_MODE_REFLECTION) {
    memcpy(((uint8_t *)a_out) + padding * sizeof(T), a_in, sz * sizeof(T));

    if (mode == PADDING_MODE_REFLECTION) {
        // reflect left
        int pI = 0;
        for (int pO = (int)padding; pO >= 0; --pO) {
            a_out[pO] = a_in[pI];
            ++pI;
        }

        // reflect right
        pI = (int)sz - 1;
        int pO = static_cast<int>(sz + padding);
        for (int i = 0; i < (int)padding; ++i) {
            a_out[pO] = a_in[pI];
            --pI;
            ++pO;
        }
    }
}

// apply a 1D kernel in the x-direction
template<typename T, typename Tw>
void convolve_x(const size_t y, const size_t x, const size_t stride_bytes,
                const uint8_t *image_in, std::vector<Tw> &kernel, uint8_t *image_out) {

    const int ksize = (const int)kernel.size();
    int half_k = (int)floor(((Tw)ksize) / 2.0);

#pragma omp parallel for default(shared)
    for (int i = 0; i < (int)y; ++i) { // using int here because of openmp limitation in some versions to parallelize unsigned

        T *r_in = (T *)(image_in + i * stride_bytes);
        T *r_out = (T *)(image_out + i * stride_bytes);

        // allocations must all happen per row in the parallel case
        // create padding right here per line
        std::vector<T> temp_row(x + 2 * half_k + 1, 0);
        pad_array<T>(x, half_k, r_in, &temp_row[0], PADDING_MODE_REFLECTION);

        // sweep across the row performing the convolution
        for (size_t j = 0; j < x; ++j) {
            // go from ksize-half_k to ksize+half_k
            Tw temp = 0.0;
            Tw *kk = &kernel[0];
            for (size_t k = 0; k < ksize; ++k) {
                temp += (*kk) * (Tw)temp_row[j + k];
                ++kk;
            }
            r_out[j] = (T)temp;
        }
    }
}

// apply a 1D kernel in the y-direction
template<typename T, typename Tw>
void convolve_y(const size_t y, const size_t x, const size_t stride_bytes,
                const uint8_t *image_in, std::vector<Tw> &kernel, uint8_t *image_out) {

    const int ksize = (const int)kernel.size();
    int half_k = (int)floor(((Tw)ksize) / 2.0);

#pragma omp parallel for default(shared)
    for (int j = 0; j < (int)x; ++j) { // using int here because of openmp limitation in some versions to parallelize unsigned
        // allocations must all happen per row in the parallel case
        // create padding right here per line
        std::vector<T> temp_in(y);
        std::vector<T> temp_row(y + 2 * half_k + 1, 0);

        // create consecutive col
        for (size_t i = 0; i < y; ++i) {
            T *r_in = (T *)(image_in + i * stride_bytes);
            temp_in[i] = r_in[j];
        }
        pad_array<T>(y, half_k, &temp_in[0], &temp_row[0], PADDING_MODE_REFLECTION);

        // sweep across the row performing the convolution
        for (size_t i = 0; i < y; ++i) {
            T *r_out = (T *)(image_out + i * stride_bytes);

            // go from ksize-half_k to ksize+half_k
            Tw temp = 0.0;
            Tw *kk = &kernel[0];
            for (size_t k = 0; k < ksize; ++k) {
                temp += (*kk) * (Tw)temp_row[i + k];
                ++kk;
            }
            r_out[j] = (T)temp;
        }
    }
}


//------------------------------------------------------------------------------------
// gaussian
//------------------------------------------------------------------------------------


template<typename T, typename Tw>
void gaussian_filter_2D(const size_t y, const size_t x, const size_t stride_bytes,
                        const size_t filter_h, const size_t filter_w, const double sigma,
                        uint8_t *image_out) {

    std::vector<Tw> LoGx(filter_w);
    std::vector<Tw> LoGy(filter_h);
    std::vector<Tw> Gx(filter_w);
    std::vector<Tw> Gy(filter_h);
    create_filter_kernels<Tw>((Tw)sigma, LoGx, Gx, LoGy, Gy);

    convolve_x<T, Tw>(y, x, stride_bytes, image_out, Gx, image_out);
    convolve_y<T, Tw>(y, x, stride_bytes, image_out, Gy, image_out);
}

Image Image::gaussian(const unsigned int ksz_x, const unsigned int ksz_y, const double &sigma) const {
    Image out = this->deepCopy(true);

    for (uint64 sample = 0; sample < this->samples(); sample++) {
        bim::uint8 *image_in = (bim::uint8 *)this->bits(sample);
        bim::uint8 *image_out = (bim::uint8 *)out.bits(sample);
        const size_t y = this->height();
        const size_t x = this->width();
        const size_t stride_bytes = this->bytesPerLine();

        if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            gaussian_filter_2D<bim::uint8, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            gaussian_filter_2D<bim::uint16, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            gaussian_filter_2D<bim::uint32, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            gaussian_filter_2D<bim::uint64, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            gaussian_filter_2D<bim::int8, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            gaussian_filter_2D<bim::int16, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            gaussian_filter_2D<bim::int32, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            gaussian_filter_2D<bim::int64, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
            gaussian_filter_2D<bim::float32, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
            gaussian_filter_2D<bim::float64, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
    }
    return out;
}

Image operation_gaussian(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    unsigned int ksz_x = 3;
    unsigned int ksz_y = 3;
    double sigma = 1.0;
    std::vector<double> vals = arguments.splitDouble(",", 0.0);
    if (vals.size() > 0)
        ksz_x = (bim::uint)vals[0];
    if (vals.size() > 1)
        ksz_y = (bim::uint)vals[1];
    if (vals.size() > 2)
        sigma = vals[2];
    if (ksz_x > 0 && ksz_y > 0)
        return img.gaussian(ksz_x, ksz_y, sigma);
    return img;
}

//------------------------------------------------------------------------------------
// Laplacian of Gaussian
//------------------------------------------------------------------------------------

template<typename T, typename Tw>
void log_filter_2D(const size_t y, const size_t x, const size_t stride_bytes,
                   const size_t filter_h, const size_t filter_w, const double sigma,
                   uint8_t *image_out) {

    std::vector<Tw> LoGx(filter_w);
    std::vector<Tw> LoGy(filter_h);
    std::vector<Tw> Gx(filter_w);
    std::vector<Tw> Gy(filter_h);
    create_filter_kernels<Tw>((Tw)sigma, LoGx, Gx, LoGy, Gy);

    std::vector<uint8_t> temp1(y * stride_bytes);
    memcpy(&temp1[0], image_out, y * stride_bytes);
    std::vector<uint8_t> temp2(y * stride_bytes);
    memcpy(&temp2[0], image_out, y * stride_bytes);

    // perform convolution 1
    convolve_x<T, Tw>(y, x, stride_bytes, &temp1[0], LoGx, (uint8_t *)&temp1[0]);
    convolve_y<T, Tw>(y, x, stride_bytes, &temp1[0], Gy, (uint8_t *)&temp1[0]);

    // perform convolution 2
    convolve_y<T, Tw>(y, x, stride_bytes, &temp2[0], LoGy, (uint8_t *)&temp2[0]);
    convolve_x<T, Tw>(y, x, stride_bytes, &temp2[0], Gx, (uint8_t *)&temp2[0]);

// add the images into the result
// dima: this may or may not be beneficial to run in parallel
#pragma omp parallel for default(shared)
    for (int i = 0; i < (int)y; ++i) {
        T *t1 = (T *)((uint8_t *)&temp1[0] + i * stride_bytes);
        T *t2 = (T *)((uint8_t *)&temp2[0] + i * stride_bytes);
        T *r_out = (T *)(image_out + i * stride_bytes);

        for (size_t j = 0; j < x; ++j) {
            r_out[j] = t1[j] + t2[j];
        }
    }
}

Image Image::LoG(const unsigned int ksz_x, const unsigned int ksz_y, const double &sigma) const {
    Image out = this->deepCopy(true);

    for (uint64 sample = 0; sample < this->samples(); sample++) {
        bim::uint8 *image_out = (bim::uint8 *)out.bits(sample);
        const size_t y = this->height();
        const size_t x = this->width();
        const size_t stride_bytes = this->bytesPerLine();

        if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            log_filter_2D<bim::uint8, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            log_filter_2D<bim::uint16, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            log_filter_2D<bim::uint32, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            log_filter_2D<bim::uint64, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            log_filter_2D<bim::int8, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            log_filter_2D<bim::int16, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            log_filter_2D<bim::int32, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            log_filter_2D<bim::int64, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
            log_filter_2D<bim::float32, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
            log_filter_2D<bim::float64, double>(y, x, stride_bytes, ksz_y, ksz_x, sigma, (uint8_t *)image_out);
    }
    return out;
}

Image operation_log(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    unsigned int ksz_x = 3;
    unsigned int ksz_y = 3;
    double sigma = 1.0;
    std::vector<double> vals = arguments.splitDouble(",", 0.0);
    if (vals.size() > 0)
        ksz_x = (bim::uint)vals[0];
    if (vals.size() > 1)
        ksz_y = (bim::uint)vals[1];
    if (vals.size() > 2)
        sigma = vals[2];
    if (ksz_x > 0 && ksz_y > 0)
        return img.LoG(ksz_x, ksz_y, sigma);
    return img;
}

//------------------------------------------------------------------------------------
// median
//------------------------------------------------------------------------------------

template<typename T, typename Tw>
void median_2d(const size_t ksz, const size_t y, const size_t x, const size_t stride_bytes,
               const uint8_t *image_in, uint8_t *image_out) {

    int hsz = (int)ksz / 2;
    int ksize = (hsz + hsz + 1) * (hsz + hsz + 1);
    int mid_p = ksize / 2;

#pragma omp parallel for default(shared)
    for (int i = 0 + hsz; i < (int)y - hsz - 1; ++i) { // using int here because of openmp limitation in some versions to parallelize unsigned
        std::vector<T> kernel(ksize);
        T *r_out = (T *)(image_out + i * stride_bytes);
        for (size_t j = 0 + hsz; j < x - hsz - 1; ++j) {
            T *kk = &kernel[0];
            for (size_t ii = i - hsz; ii < i + hsz + 1; ++ii) {
                T *r_in = (T *)(image_in + ii * stride_bytes);
                for (size_t jj = j - hsz; jj < j + hsz + 1; ++jj) {
                    //*kk = r_in[jj];
                    //++kk;
                    *(kk++) = r_in[jj];
                }
            }

            //std::sort(kernel.begin(), kernel.end());
            //r_out[j] = kernel[mid_p];

            typename std::vector<T>::iterator median_iter = kernel.begin() + mid_p;
            std::nth_element(kernel.begin(), median_iter, kernel.end());
            r_out[j] = *median_iter;
        }
    }
}

Image Image::median(const unsigned int ksz) const {
    Image out = this->deepCopy(true);

    for (uint64 sample = 0; sample < this->samples(); sample++) {
        bim::uint8 *image_in = (bim::uint8 *)this->bits(sample);
        bim::uint8 *image_out = (bim::uint8 *)out.bits(sample);
        const size_t y = this->height();
        const size_t x = this->width();
        const size_t stride_bytes = this->bytesPerLine();

        if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            median_2d<bim::uint8, double>(ksz, y, x, stride_bytes, (uint8_t *)image_in, (uint8_t *)image_out);
        else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            median_2d<bim::uint16, double>(ksz, y, x, stride_bytes, (uint8_t *)image_in, (uint8_t *)image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            median_2d<bim::uint32, double>(ksz, y, x, stride_bytes, (uint8_t *)image_in, (uint8_t *)image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            median_2d<bim::uint64, double>(ksz, y, x, stride_bytes, (uint8_t *)image_in, (uint8_t *)image_out);
        else if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            median_2d<bim::int8, double>(ksz, y, x, stride_bytes, (uint8_t *)image_in, (uint8_t *)image_out);
        else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            median_2d<bim::int16, double>(ksz, y, x, stride_bytes, (uint8_t *)image_in, (uint8_t *)image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            median_2d<bim::int32, double>(ksz, y, x, stride_bytes, (uint8_t *)image_in, (uint8_t *)image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            median_2d<bim::int64, double>(ksz, y, x, stride_bytes, (uint8_t *)image_in, (uint8_t *)image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
            median_2d<bim::float32, double>(ksz, y, x, stride_bytes, (uint8_t *)image_in, (uint8_t *)image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
            median_2d<bim::float64, double>(ksz, y, x, stride_bytes, (uint8_t *)image_in, (uint8_t *)image_out);
    }
    return out;
}

Image operation_median(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    unsigned int ksz = arguments.toInt();
    return img.median(ksz);
}


//------------------------------------------------------------------------------------
// region growing - proprietary ViQi Inc implementation
//------------------------------------------------------------------------------------

// create the 9x9 weighted disk kernel
// created in Python and hard-coded here
float weighted_disk_9x9[9][9] = {
    { 0.0f, 0.0f, 0.0f, 0.0f, 0.24253563f, 0.0f, 0.0f, 0.0f, 0.0f },
    { 0.0f, 0.0f, 0.24253563f, 0.24253563f, 0.34299717f, 0.24253563f, 0.24253563f, 0.0f, 0.0f },
    { 0.0f, 0.24253563f, 0.34299717f, 0.48507125f, 0.54232614f, 0.48507125f, 0.34299717f, 0.24253563f, 0.0f },
    { 0.0f, 0.24253563f, 0.48507125f, 0.68599434f, 0.76696499f, 0.68599434f, 0.48507125f, 0.24253563f, 0.0f },
    { 0.24253563f, 0.34299717f, 0.54232614f, 0.76696499f, 1.0f, 0.76696499f, 0.54232614f, 0.34299717f, 0.24253563f },
    { 0.0f, 0.24253563f, 0.48507125f, 0.68599434f, 0.76696499f, 0.68599434f, 0.48507125f, 0.24253563f, 0.0f },
    { 0.0f, 0.24253563f, 0.34299717f, 0.48507125f, 0.54232614f, 0.48507125f, 0.34299717f, 0.24253563f, 0.0f },
    { 0.0f, 0.0f, 0.24253563f, 0.24253563f, 0.34299717f, 0.24253563f, 0.24253563f, 0.0f, 0.0f },
    { 0.0f, 0.0f, 0.0f, 0.0f, 0.24253563f, 0.0f, 0.0f, 0.0f, 0.0f }
};
int half_h = 4;
int half_w = 4;

template<typename T>
class NeighborHistogram {
    // Access specifier
    std::vector<float> histogram;
    T max_value_index = 0;

public:
    NeighborHistogram(T max_value) {
        for (size_t i = 0; (T)i < max_value + 1; i++) {
            this->histogram.push_back(0.0);
        }
    }

    void print_values() const {
        for (size_t i = 0; i < this->histogram.size(); i++) {
            std::cout << i << ": " << this->histogram[i] << std::endl;
        }
    }

    void add_value(T index, float value) {
        this->histogram[(unsigned int)index] += value;
        if (this->histogram[(unsigned int)index] > this->histogram[(unsigned int)max_value_index]) {
            max_value_index = index;
        }
    }

    void clear() {
        // set all bins to zero
        memset(this->histogram.data(), 0, sizeof(float) * this->histogram.size());
    }

    T get_max_bin() const {
        return max_value_index;
    }
};

template<typename T>
void create_histogram(const T *im,
                      const size_t image_size,
                      std::vector<unsigned int> &count) {

    // NOTE: we assume the input image is a label image with values [0,1,2,3,...N]
    // If image is double/float with non-integer values, this function may not function well
    for (size_t i = 0; i < image_size; ++i) {
        ++count[(unsigned int)im[i]];
    }
}

// code for convex hull stuff
//
//
//
struct point_cart {
    // cartesian coordinates
    double x = 0.0;
    double y = 0.0;

    point_cart() {
        x = 0.0;
        y = 0.0;
    }

    point_cart(double x_in, double y_in) {
        x = x_in;
        y = y_in;
    }
    bool operator<(const point_cart &str) const {
        // used in duplicate detection
        return (x < str.x);
    }
    bool operator==(const point_cart &str0) const {
        return ((str0.x == x) && (str0.y == y));
    }
    point_cart operator+(const point_cart &pt2) {
        return point_cart(x + pt2.x, y + pt2.y);
    }
};

struct point_pol {
    // polar coordinates
    double rho = 0.0;
    double phi = 0.0;

    point_pol() {
        rho = 0.0;
        phi = 0.0;
    }

    point_pol(double rho_in, double phi_in) {
        rho = rho_in;
        phi = phi_in;
    }

    bool operator<(const point_pol &str) const {
        //  used for sorting points by angle
        return (phi < str.phi);
    }
};

point_pol cart2pol(point_cart pt) {
    // cartesian to polar coordinates
    return point_pol(sqrt(pt.x * pt.x + pt.y * pt.y), atan2(pt.y, pt.x));
}

point_cart pol2cart(point_pol pt) {
    // polar to cartesian coordinates
    return point_cart(pt.rho * cos(pt.phi), pt.rho * sin(pt.phi));
}


// Graham's scan algorithm from https://cp-algorithms.com/geometry/convex-hull.html
//
// functions orientation, cw, collinear, convex_hull
//
int orientation(point_cart a, point_cart b, point_cart c) {
    double v = a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y);
    if (v < 0) return -1; // clockwise
    if (v > 0) return +1; // counter-clockwise
    return 0;
}

bool cw(point_cart a, point_cart b, point_cart c, bool include_collinear) {
    int o = orientation(a, b, c);
    return o < 0 || (include_collinear && o == 0);
}

bool collinear(point_cart a, point_cart b, point_cart c) { 
    return orientation(a, b, c) == 0; 
}

void convex_hull(std::vector<point_cart> &a, bool include_collinear = false) {
    // Graham's scan algorithm from https://cp-algorithms.com/geometry/convex-hull.html
    //
    point_cart p0 = *std::min_element(a.begin(), a.end(), [](point_cart a, point_cart b) {
        return std::make_pair(a.y, a.x) < std::make_pair(b.y, b.x);
    });
    sort(a.begin(), a.end(), [&p0](const point_cart &a, const point_cart &b) {
        int o = orientation(p0, a, b);
        if (o == 0)
            return (p0.x - a.x) * (p0.x - a.x) + (p0.y - a.y) * (p0.y - a.y) < (p0.x - b.x) * (p0.x - b.x) + (p0.y - b.y) * (p0.y - b.y);
        return o < 0;
    });
    if (include_collinear) {
        int i = (int)a.size() - 1;
        while (i >= 0 && collinear(p0, a[i], a.back())) i--;
        reverse(a.begin() + i + 1, a.end());
    }
    std::vector<point_cart> st;
    for (int i = 0; i < (int)a.size(); i++) {
        while (st.size() > 1 && !cw(st[st.size() - 2], st.back(), a[i], include_collinear))
            st.pop_back();
        st.push_back(a[i]);
    }
    a = st;
}

//class EdgeContour2 : public std::vector<point_cart> {
//public:
//    EdgeContour2(){};
//};

class EdgeContour {
    // hold the coordinates of the edge points from the region
    std::vector<point_cart> points;
    std::vector<point_cart> points_reordered;
    std::vector<point_cart> hull_points;

public:
    //int num_points = 0;
    //int num_hull_points = 0;

    EdgeContour() {
        this->points.reserve(200);
        this->points_reordered.reserve(200);
        this->hull_points.reserve(200);
    }

    void add_point(double x, double y) {
        this->points.push_back(point_cart(x, y));
    }

    point_cart get_point(size_t ii) {
        return this->points[ii];
    }

    void reorder_pairs() {
        // calculate centroid
        // around centroid, sort points by polar coordinates angle (phi)
        // convert back to cartesian

        // temp vector to store the polar coordinates
        std::vector<point_pol> temp_pol_v;


        // compute mean of all points
        double xc = 0.0;
        double yc = 0.0;

        point_cart *p = &points[0];
        for (size_t n = 0; n < this->points.size(); n++) {
            xc += p->x;
            yc += p->y;
            ++p;
        }
        xc = xc / (double)this->points.size();
        yc = yc / (double)this->points.size();

        // convert to polar
        point_cart temp_xy;
        p = &points[0];
        for (size_t n = 0; n < this->points.size(); n++) {
            temp_xy.x = p->x - xc;
            temp_xy.y = p->y - yc;
            temp_pol_v.push_back(cart2pol(temp_xy));
            ++p;
        }

        // sort points by phi (angle)
        std::sort(temp_pol_v.begin(), temp_pol_v.end());

        //convert back to cartesian and put in points
        point_cart pt_c = point_cart(xc, yc);
        for (size_t n = 0; n < this->points.size(); n++) {
            points_reordered.push_back(pol2cart(temp_pol_v[n]) + pt_c);
        }
    }

    void print_values() {
        // for dev/debug
        for (size_t n = 0; n < this->points.size(); n++) {
            std::cout << n << ": (" << this->points[n].x << "," << this->points[n].y << ")" << std::endl;
        }
    }

    void print_reordered_values() {
        // for dev/debug
        for (size_t n = 0; n < this->points.size(); n++) {
            std::cout << n << ": (" << this->points_reordered[n].x << "," << this->points_reordered[n].y << ")" << std::endl;
        }
    }

    void print_hull_values() {
        // for dev/debug
        for (size_t n = 0; n < this->hull_points.size(); n++) {
            std::cout << n << ": (" << this->hull_points[n].x << "," << this->hull_points[n].y << ")" << std::endl;
        }
    }

    size_t get_length() {
        return this->points.size();
    }

    double get_area() {
        // algorithm taken from viqi_analysis_sdk
        this->points_reordered.push_back(this->points_reordered[0]);

        double d = 0.0;
        //for (size_t i = 0; i < this->points_reordered.size()-1; i++) {
        //    d += this->points_reordered[i].x * this->points_reordered[i + 1].y - this->points_reordered[i].y * this->points_reordered[i + 1].x;
        //}

        point_cart *p = &points_reordered[0];
        for (size_t i = 0; i < this->points_reordered.size() - 1; i++) {
            d += p->x * (p + 1)->y - p->y * (p + 1)->x;
            ++p;
        }

        this->points_reordered.pop_back();
        return 0.5 * std::fabs(d);
    }

    double get_hull_area() {
        // algorithm taken from viqi_analysis_sdk
        this->hull_points.push_back(this->hull_points[0]);
        double d = 0.0;

        //for (size_t i = 0; i < this->hull_points.size()-1 ; i++) {
        //    d += this->hull_points[i].x * this->hull_points[i + 1].y - this->hull_points[i].y * this->hull_points[i + 1].x;
        //}

        //alternate way, using the pointer to the vector buffer
        point_cart *p = &hull_points[0];
        for (size_t i = 0; i < this->hull_points.size() - 1; i++) {
            d += p->x * (p + 1)->y - p->y * (p + 1)->x;
            ++p;
        }


        this->hull_points.pop_back();
        return 0.5 * std::fabs(d);
    }

    void compute_hull() {
        this->hull_points = this->points;
        convex_hull(this->hull_points, false);
    }

    void remove_duplicates() {
        sort(this->points.begin(), this->points.end());
        this->points.erase(unique(this->points.begin(), this->points.end()), this->points.end());
    }
};



template<typename T>
void find_edge_pixels(const T *im, size_t num_regions,
                      const size_t image_height, const size_t image_width,
                      const size_t stride_bytes,
                      std::vector<EdgeContour> &all_edge_pixels) {

    // **********************
    // 
    // This is the serial version.
    // Parallel version is down below named find_edge_pixels_parallel()
    //
    // **********************



    // Using 4-connectedness to determine edge points
    for (int i = 0; i < (int)image_height; i++) {
        T *r_in = (T *)(((uint8_t *)im) + i * stride_bytes);
        bool done = false;

        for (size_t j = 0; j < image_width; j++) {
            if (r_in[j] > 0) {

                done = false;
                if (!done && i > 0) {
                    if (((T *)((uint8_t *)im + (i - 1) * (int)stride_bytes))[j] != r_in[j]) {
                        all_edge_pixels[(int)r_in[j]].add_point((double)j, (double)i);
                        done = true;
                    }
                }
                if (!done && i < (int)image_height - 1) {
                    if (((T *)((uint8_t *)im + (i + 1) * (int)stride_bytes))[j] != r_in[j]) {
                        all_edge_pixels[(int)r_in[j]].add_point((double)j, (double)i);
                        done = true;
                    }
                }
                if (!done && j > 0) {
                    if (((T *)((uint8_t *)im + i * (int)stride_bytes))[j - 1] != r_in[j]) {
                        all_edge_pixels[(int)r_in[j]].add_point((double)j, (double)i);
                        done = true;
                    }
                }
                if (!done && j < image_width - 1) {
                    if (((T *)((uint8_t *)im + i * (int)stride_bytes))[j + 1] != r_in[j]) {
                        all_edge_pixels[(int)r_in[j]].add_point((double)j, (double)i);
                        done = true;
                    }
                }
                if ((i==0) || (i==image_height-1) || (j==0) || (j==image_width-1)) {
                    if (((T *)((uint8_t *)im + i * (int)stride_bytes))[j + 1] != r_in[j]) {
                        all_edge_pixels[(int)r_in[j]].add_point((double)j, (double)i);
                        done = true;
                    }
                }
            }
        }
    }
}

template<typename T>
void find_edge_pixels_parallel(const T *im, size_t num_regions,
                      const size_t image_height, const size_t image_width,
                      const size_t stride_bytes,
                      std::vector<EdgeContour> &all_edge_pixels) {

#pragma omp parallel for default(shared)
    // Using 4-connectedness to determine edge points
    for (int i = 0; i < (int)image_height; i++) {
        T *r_in = (T *)(((uint8_t *)im) + i * stride_bytes);
        bool done = false;

        std::vector<EdgeContour> X(all_edge_pixels.size());

        for (size_t j = 0; j < image_width; j++) {
            if (r_in[j] > 0) {

                done = false;
                if (!done && i > 0) {
                    if (((T *)((uint8_t *)im + (i - 1) * (int)stride_bytes))[j] != r_in[j]) {
                        //all_edge_pixels[(int)r_in[j]].add_point((double)j, (double)i);
                        done = true;
                    }
                }
                if (!done && i < (int)image_height - 1) {
                    if (((T *)((uint8_t *)im + (i + 1) * (int)stride_bytes))[j] != r_in[j]) {
                        //all_edge_pixels[(int)r_in[j]].add_point((double)j, (double)i);
                        done = true;
                    }
                }
                if (!done && j > 0) {
                    if (((T *)((uint8_t *)im + i * (int)stride_bytes))[j - 1] != r_in[j]) {
                        //all_edge_pixels[(int)r_in[j]].add_point((double)j, (double)i);
                        done = true;
                    }
                }
                if (!done && j < image_width - 1) {
                    if (((T *)((uint8_t *)im + i * (int)stride_bytes))[j + 1] != r_in[j]) {
                        //all_edge_pixels[(int)r_in[j]].add_point((double)j, (double)i);
                        done = true;
                    }
                }
                if ((i == 0) || (i == image_height - 1) || (j == 0) || (j == image_width - 1)) {
                    if (((T *)((uint8_t *)im + i * (int)stride_bytes))[j + 1] != r_in[j]) {
                        //all_edge_pixels[(int)r_in[j]].add_point((double)j, (double)i);
                        done = true;
                    }
                }
                if (done) {
                    X[(int)r_in[j]].add_point((double)j, (double)i);
                }
            }
        }
        //// lock and add all the points to all_edge_pixels
        //omp_set_lock(&writelock);
        #pragma omp critical
        {
            for (size_t ii = 0; ii < all_edge_pixels.size(); ii++) {
                for (size_t jj = 0; jj < X[ii].get_length(); jj++) {
                    all_edge_pixels[ii].add_point(X[ii].get_point(jj).x, X[ii].get_point(jj).y);
                }
            }
        }
        //omp_unset_lock(&writelock);
    }
}

template<typename T>
void compute_convexity(const T *im,
                       const size_t image_height,
                       const size_t image_width,
                       const size_t stride_bytes,
                       const size_t num_regions,
                       std::vector<double> &convexity) {
    /*
    For each region in im:
    1. extract the edge pixels
    2. extract the pixels' coordinates
    3. reorder the points into a continuous contour

    From the ordered coordinates (xi,yi), compute:
    1. polygon area
    2. convex hull
    3. convex hull area
    4. convexity = polygon_area/convex_hull_area
    */
    double region_area;
    double convex_area;

    std::vector<EdgeContour> all_edge_points(num_regions + 1);
    find_edge_pixels(im, num_regions, image_height, image_width, stride_bytes, all_edge_points);

#pragma omp parallel for default(shared)
    for (int region_num = 1; region_num <= (int)num_regions; region_num++) {
        all_edge_points[region_num].remove_duplicates();

        all_edge_points[region_num].reorder_pairs();

        all_edge_points[region_num].compute_hull();

        region_area = all_edge_points[region_num].get_area();

        convex_area = all_edge_points[region_num].get_hull_area();

        convexity[region_num] = region_area / convex_area;
    }
}


template<typename T>
// void get_new_label(image_in, image_height, image_width, i, j, label_available)
T get_new_label(const uint8_t *image_in, const size_t image_height, const size_t image_width,
                const size_t maximum_region_area,
                const size_t stride_bytes, size_t i, size_t j,
                NeighborHistogram<T> neighbor_histogram,
                std::vector<bool> &label_available) {

    // (i,j) in [i-4, i, i+4]x[j-4, j, j+4]
    size_t imin = i > 4 ? i - 4 : 0;
    size_t imax = i < image_height - 5 ? i + 4 : image_height - 1;
    size_t jmin = j > 4 ? j - 4 : 0;
    size_t jmax = j < image_width - 5 ? j + 4 : image_width - 1;

    neighbor_histogram.clear();

    // Create the weighted histogram of neighbor labels
    for (int ic = (int)i - half_h; ic <= (int)i + half_h; ic++) {
        if ((ic >= 0) && (ic < image_height)) {
            T *rcur = (T *)(image_in + ic * stride_bytes);
            for (int jc = (int)j - half_w; jc <= (int)j + half_w; jc++) {
                if ((jc >= 0) && (jc < image_width)) {
                    float cur_weight = weighted_disk_9x9[ic - (int)i + half_h][jc - (int)j + half_w];
                    if ((rcur[jc] > (T)0) && (cur_weight > 0) && (label_available[(unsigned int)rcur[jc]])) {
                        neighbor_histogram.add_value(rcur[jc], cur_weight);
                    }
                }
            }
        }
    }

    return neighbor_histogram.get_max_bin();
}

template<typename T, typename Tw>
void do_region_grow(const size_t region_size,
                    const double minimum_convexity,
                    const uint8_t *boundary_probability,
                    const size_t image_height, const size_t image_width,
                    const size_t stride_bytes,
                    const void *image_in_p,
                    void *image_out_p) {

    // Go through all unassigned pixels (value = 0) and assign a label if a region is a neighbor (4 or 8-connectivity).
    // Loop through until the maximum area is reached on every region
    // Update the histogram counts after each iteration.

    const T *image_in = (const T *)image_in_p;
    T *image_out = (T *)image_out_p;
    size_t image_size = image_height * image_width;

    // initialize the count vector
    T maxval = bim::max<T>(image_in, image_size);

    std::vector<unsigned int> count_prev((unsigned int)maxval + 1, 0);
    std::vector<unsigned int> count_cur((unsigned int)maxval + 1, 0);
    std::vector<double> convexity_cur((unsigned int)maxval + 1, 0);
    std::vector<bool> label_available((unsigned int)maxval + 1, 0);


    // create a temp label image, copy of image_in to begin
    std::vector<T> image_temp(image_size);

    void *buff = &image_temp[0];
    std::memcpy(image_out, image_in, image_size * sizeof(T));

    create_histogram<T>(image_in, image_size, count_prev);

    //compute_convexity<T>((T*)image_in, image_height, image_width, stride_bytes, maxval, convexity_cur);

    bool all_regions_max_met = false;
    bool no_region_change = false;

    int64_t nonzero_count_cur = 0;
    int64_t nonzero_count_prev = 0;


    // initialize nonzero count
    for (int i = 1; i < count_prev.size(); i++) {
        nonzero_count_cur += count_prev[i];
    }

    int iteration_number = 0;

    double conv_thresh;
    while ((!all_regions_max_met) && (!no_region_change)) {

        // Ramp up the convexity threshold to allow smaller regions to grow
        if (iteration_number == 1) {
            conv_thresh = 0.7 * minimum_convexity;
        } else if (iteration_number == 2) {
            conv_thresh = 0.85 * minimum_convexity;
        } else {
            conv_thresh = minimum_convexity;
        }

        // copy output_label_image to temp label image
        std::memcpy(buff, image_out, image_size * sizeof(T));

        nonzero_count_prev = nonzero_count_cur;
        nonzero_count_cur = 0;

        // for iteration 0, ignore the convexity. Small pixel numbers may cause the calculation to return nan or inf which will break things.
        for (int ii = 1; ii < count_prev.size(); ii++) {
            if (iteration_number == 0) {
                label_available[ii] = count_prev[ii] < region_size;
            } else {
                label_available[ii] = count_prev[ii] < region_size && convexity_cur[ii] >= conv_thresh;
            }
        }


#pragma omp parallel for default(shared)
        for (int i = 0; i < (int)image_height; ++i) { // using int here because of openmp limitation in some versions to parallelize unsigned
            // vector to hold neighbors' labels
            NeighborHistogram<T> histogram_temp(maxval);

            T *r_in = (T *)(((uint8_t *)buff) + i * stride_bytes);
            T *r_out = (T *)(((uint8_t *)image_out) + i * stride_bytes);

            for (size_t j = 0; j < image_width; j++) {
                if (((r_in[j] == 0) && (boundary_probability == NULL)) || ((r_in[j] == 0) && ((boundary_probability + i * image_width)[j] < 128))) {
                    r_out[j] = get_new_label<T>((uint8_t *)buff, image_height, image_width, region_size, stride_bytes, (size_t)i, j, histogram_temp, label_available);
                }
            }
        }

        // compute the histogram and convexity again for the next pass
        create_histogram<T>(image_out, image_size, count_cur);
        compute_convexity<T>(image_out, image_height, image_width, stride_bytes, (size_t)maxval, convexity_cur);

        // calculate number of pixels changed
        if (count_prev[0] - count_cur[0] == 0) {
            no_region_change = true;
        }

        bool all_full = true;
        for (int i = 1; i < count_cur.size(); i++) {
            all_full = all_full && (count_cur[i] >= region_size);
        }
        if (all_full) {
            all_regions_max_met = true;
        }

        count_prev = count_cur;
        iteration_number++;
        memset(&count_cur[0], 0, sizeof(unsigned int) * count_cur.size());
    }
}

Image Image::region_grow(const unsigned int region_size, const double minimum_convexity, const bim::uint8 *boundary_probability) const {
    Image out = this->deepCopy(true);

    for (uint64 sample = 0; sample < this->samples(); sample++) {
        void *image_in = (void *)this->bits(sample);
        void *image_out = (void *)out.bits(sample);
        const size_t image_height = this->height();
        const size_t image_width = this->width();
        const size_t stride_bytes = this->bytesPerLine();

        if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            do_region_grow<bim::uint8, double>(region_size, minimum_convexity, boundary_probability, image_height, image_width, stride_bytes, image_in, image_out);
        else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            do_region_grow<bim::uint16, double>(region_size, minimum_convexity, boundary_probability, image_height, image_width, stride_bytes, image_in, image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            do_region_grow<bim::uint32, double>(region_size, minimum_convexity, boundary_probability, image_height, image_width, stride_bytes, image_in, image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_UNSIGNED)
            do_region_grow<bim::uint64, double>(region_size, minimum_convexity, boundary_probability, image_height, image_width, stride_bytes, image_in, image_out);
        else if (this->depth() == 8 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            do_region_grow<bim::int8, double>(region_size, minimum_convexity, boundary_probability, image_height, image_width, stride_bytes, image_in, image_out);
        else if (this->depth() == 16 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            do_region_grow<bim::int16, double>(region_size, minimum_convexity, boundary_probability, image_height, image_width, stride_bytes, image_in, image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            do_region_grow<bim::int32, double>(region_size, minimum_convexity, boundary_probability, image_height, image_width, stride_bytes, image_in, image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_SIGNED)
            do_region_grow<bim::int64, double>(region_size, minimum_convexity, boundary_probability, image_height, image_width, stride_bytes, image_in, image_out);
        else if (this->depth() == 32 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
            do_region_grow<bim::float32, double>(region_size, minimum_convexity, boundary_probability, image_height, image_width, stride_bytes, image_in, image_out);
        else if (this->depth() == 64 && this->pixelType() == bim::DataFormat::FMT_FLOAT)
            do_region_grow<bim::float64, double>(region_size, minimum_convexity, boundary_probability, image_height, image_width, stride_bytes, image_in, image_out);
    }
    return out;
}

Image operation_region_grow(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    unsigned int region_size = 0;
    double minimum_convexity = 1;
    std::vector<int> vals = arguments.splitInt(",", 0);

    if (vals.size() < 1)
        return img;
    if (vals.size() > 0)
        region_size = vals[0];
    if (vals.size() > 1)
        minimum_convexity = vals[1];
    if (minimum_convexity < 0 || minimum_convexity > 1)
        return img;

    return img.region_grow(region_size, minimum_convexity);
}

//------------------------------------------------------------------------------------
// Multi-resolution filters - proprietary ViQi Inc implementation
//------------------------------------------------------------------------------------

// multi-resolution image filter that can compress dynamic range by adjusting the low resolution level and remove noise by adjusting the highest resolution level
// levels - indicates how many levels split image into, 0 is fully automatic, >0 exact number of levels, <0 automatic - requested_number_of_levels
// noise - multiplier for high resolution level, 0 - all removed, 1 - all left
// background - multiplier for low resolution level, 0 - all removed, 1 - all left
// base - base intensity, use 0.5 for brighfield images and 0 for fluorescence
// midrange - is a list of levels to remove mid-range aberrations, eg: [4,5]

Image Image::mrs_filter(int levels, float noise, float background, float base,
                        bool normalize_in, bool normalize_out, const bim::xstring &midrange, bool trim) const {
    Image out = *this;
    if (normalize_in == true)
        out = this->normalizeFloat01();

    out = mr_filter<bim::float32>(out, levels, noise, background, base, midrange, trim);

    if (normalize_out == true)
        out = out.convertToDepth(this->depth(), bim::Lut::ltLinearDataRange, this->pixelType());
    return out;
}

Image operation_mrfilter(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    int levels = 0;
    float noise = 1.0;
    float background = 1.0;
    float base = 0.0;
    bim::xstring midrange;

    // "l=-4;n=1.0;bg=0.0;b=0.5;mid=3,4,5"
    std::vector<xstring> a = arguments.toLowerCase().split(";");
    for (int i = 0; i < a.size(); ++i) {
        std::vector<xstring> aa = a[i].split("=");
        if (aa.size() != 2) continue;

        if (aa[0] == "l")
            levels = aa[1].toInt();
        else if (aa[0] == "n")
            noise = (float)aa[1].toDouble();
        else if (aa[0] == "bg")
            background = (float)aa[1].toDouble();
        else if (aa[0] == "b")
            base = (float)aa[1].toDouble();
        else if (aa[0] == "mid")
            midrange = aa[1];
    }

    if (noise < 1.0 || background < 1.0)
        return img.mrs_filter(levels, noise, background, base, true, true, midrange);

    return img;
}

// multi-resolution flatfield correction filter
// levels - indicates how many levels split image into, 0 is fully automatic, >0 exact number of levels, <0 automatic - requested_number_of_levels
// base - base intensity, use 0.5 for brighfield images and 0 for fluorescence
// midrange - is a list of levels to remove mid-range aberrations, eg: [4,5]

Image Image::flatfield_correction(int levels, float base, const bim::xstring &midrange) const {
    float noise = 1.0;
    float background = 0.0;
    Image out = this->normalizeFloat01();
    if (base > 0)
        out.mul(0.5);
    out = out.mrs_filter(levels, noise, background, base, false, false, midrange);
    out = out.convertToDepth(this->depth(), bim::Lut::ltLinearDataRange, this->pixelType());
    return out;
}

Image operation_flatfield(Image &img, const bim::xstring &arguments, const xoperations &operations, ImageHistogram *hist, XConf *c) {
    int levels = -3;
    float base = 0.0;
    bim::xstring midrange;

    // "l=-4;b=0.5;mid=3,4,5"
    std::vector<xstring> a = arguments.toLowerCase().split(";");
    for (int i = 0; i < a.size(); ++i) {
        std::vector<xstring> aa = a[i].split("=");
        if (aa.size() != 2) continue;

        if (aa[0] == "l")
            levels = aa[1].toInt();
        else if (aa[0] == "b")
            base = (float)aa[1].toDouble();
        else if (aa[0] == "mid")
            midrange = aa[1];
    }

    return img.flatfield_correction(levels, base, midrange);
}


#endif //BIM_USE_FILTERS
