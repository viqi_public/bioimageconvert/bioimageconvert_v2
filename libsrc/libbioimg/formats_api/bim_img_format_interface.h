/*******************************************************************************

  libBioIMage Format Interface version: 3.0

  Author: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  Image file structure:

    1) Page:   each file may contain 1 or more pages, each page is independent

    2) Sample: in each page each pixel can contain 1 or more samples
               preferred number of samples: 1 (GRAY), 3(xRGB) and 4(RGBA)

    3) Depth:  each sample can be of 1 or more bits of depth
               preferred depths are: 8 and 16 bits per sample

    4) Allocation: each sample is allocated having in mind:
               All lines are stored contiguasly from top to bottom where
               having each particular line is byte alligned, i.e.:
               each line is allocated using minimal necessary number of bytes
               to store given depth. Which for the image means:
               size = ceil( ( (width*depth) / 8 ) * height )

  As a result of Sample/Depth structure we can get images of different
  Bits per Pixel as: 1, 8, 24, 32, 48 and any other if needed

  History:
    2004-03-23 18:03 - API v1.0
    2005-09-12 17:06 - updated API to v1.3
    2005-12-01 16:24 - updated API to v1.4
    2008-04-04 14:11 - updated API to v1.6
    2009-06-29 16:21 - updated API to v1.7
    2010-01-25 16:45 - updated API to v1.8
    2012-01-01 16:45 - updated API to v2.0
    2021-08-11 14:00 - updated API to v3.0

  ver: 21

*******************************************************************************/

#ifndef BIM_IMG_FMT_IFC_H
#define BIM_IMG_FMT_IFC_H

#include <cstdio>

#include <xtypes.h>

namespace bim {

// forward declarations:
struct ImageBitmap;
void initImagePlanes(ImageBitmap *bmp);


// 0:R, 1:G, 2:B, 3:R+G=Yellow, 4:R+B=Purple, 5:B+G=Cyan, 6:R+B+G=Gray
enum class DisplayChannels : unsigned int { 
    Red = 0,
    Green = 1,
    Blue = 2,
    Alpha = 3,
    Yellow = 3,
    Magenta = 4,
    Cyan = 5,
    Gray = 6 
};

// HERE DEFINED DATA TYPES IN CONFORMANCE WITH TIFF STRUCTURE
// This types are used in tiff tags andmany independent structures
// like EXIF, let's stick to the it too
enum class DataType : unsigned int {
    TAG_NOTYPE = 0,     // placeholder
    TAG_BYTE = 1,       // 8-bit unsigned integer
    TAG_ASCII = 2,      // 8-bit bytes w/ last byte null
    TAG_SHORT = 3,      // 16-bit unsigned integer
    TAG_LONG = 4,       // 32-bit unsigned integer
    TAG_RATIONAL = 5,   // 64-bit unsigned fraction
    TAG_SBYTE = 6,      // 8-bit signed integer
    TAG_UNDEFINED = 7,  // 8-bit untyped data
    TAG_SSHORT = 8,     // 16-bit signed integer
    TAG_SLONG = 9,      // 32-bit signed integer
    TAG_SRATIONAL = 10, // 64-bit signed fraction
    TAG_FLOAT = 11,     // 32-bit IEEE floating point
    TAG_DOUBLE = 12     // 64-bit IEEE floating point
};

// Data storage format used along with pixel depth (BPP)
enum class DataFormat : unsigned int {
    FMT_UNDEFINED = 0, // placeholder type
    FMT_UNSIGNED = 1,  // unsigned integer
    FMT_SIGNED = 2,    // signed integer
    FMT_FLOAT = 3,     // floating point
    FMT_COMPLEX = 4    // complex number containing two values
};

static DataFormat string_to_pixel_format(const std::string &fmt_s, const DataFormat def = DataFormat::FMT_UNSIGNED) {
    if (fmt_s == "uint8") {
        return DataFormat::FMT_UNSIGNED;
    } else if (fmt_s == "uint16") {
        return DataFormat::FMT_UNSIGNED;
    } else if (fmt_s == "uint32") {
        return DataFormat::FMT_UNSIGNED;
    } else if (fmt_s == "uint64") {
        return DataFormat::FMT_UNSIGNED;
    } else if (fmt_s == "int8") {
        return DataFormat::FMT_SIGNED;
    } else if (fmt_s == "int16") {
        return DataFormat::FMT_SIGNED;
    } else if (fmt_s == "int32") {
        return DataFormat::FMT_SIGNED;
    } else if (fmt_s == "int64") {
        return DataFormat::FMT_SIGNED;
    } else if (fmt_s == "float32") {
        return DataFormat::FMT_FLOAT;
    } else if (fmt_s == "float64") {
        return DataFormat::FMT_FLOAT;
    }
    return def;
}

static int string_to_pixel_depth(const std::string &fmt_s, const int def = 8) {
    if (fmt_s == "uint8") {
        return 8;
    } else if (fmt_s == "uint16") {
        return 16;
    } else if (fmt_s == "uint32") {
        return 32;
    } else if (fmt_s == "uint64") {
        return 64;
    } else if (fmt_s == "int8") {
        return 8;
    } else if (fmt_s == "int16") {
        return 16;
    } else if (fmt_s == "int32") {
        return 32;
    } else if (fmt_s == "int64") {
        return 64;
    } else if (fmt_s == "float32") {
        return 32;
    } else if (fmt_s == "float64") {
        return 64;
    }
    return def;
}

// modes are declared similarly to Adobe TIFF with differences and extensions
enum class ImageModes : unsigned int {
    IM_BITMAP = 0,    // 1-bit
    IM_GRAYSCALE = 1, //
    IM_INDEXED = 2,   // 8-bit
    IM_RGB = 3,       //
    //IM_BGR       = 4,  // removed inverted channel organizations, individual drives will provide channels always in RGB sequence
    IM_HSL = 5,  //
    IM_HSV = 6,  //
    IM_RGBA = 7, //
    //IM_ABGR      = 8,  // removed inverted channel organizations, individual drives will provide channels always in RGBA sequence
    IM_CMYK = 9, //
    // 10
    // 11
    IM_MULTI = 12,    // image with many separate grayscale channels
    IM_RGBE = 13,     // Radiance RGBE format defining floating RGB by division with the Exponent
    IM_YUV = 14,      // YUV444 chromaticity
    IM_XYZ = 15,      // CIEXYZ
    IM_LAB = 16,      // CIELab
    IM_CMY = 17,      // CMY
    IM_LUV = 18,      // LUV
    IM_YCbCr = 19,    // YCbCr
    IM_SPECTRAL = 20, // image contains many channels interpreted as spectral bands, used for hyper-spectral, spectrometry and similar data
    IM_MASK = 21,     // has a particualr meaning for 0-value as being empty
    IM_HEATMAP = 22,  // has a particualr meaning for 0-value as being empty
    IM_UNKNOWN = 999  //
};

enum class ImageIOModes : unsigned int {
    IO_READ = 0,
    IO_WRITE = 1
};

// this strange numbering is due to be equal to TIFF metric units
enum class ResolutionUnits : unsigned int {
    RES_m = 0,    // Meters = 0.0254 of inch
    RES_NONE = 1, // inches
    RES_IN = 2,   // inches
    RES_cm = 3,   // Centi Meters 10^^-2 = 2.54 of inch
    RES_mm = 4,   // Mili  Meters 10^^-3
    RES_um = 5,   // Micro Meters 10^^-6
    RES_nm = 6,   // Nano  Meters 10^^-9
    RES_Gm = 7,   // Giga  Meters 10^^9
    RES_Mm = 8,   // Mega  Meters 10^^6
    RES_km = 9,   // Kilo  Meters 10^^3
    RES_hm = 10   // Hecto Meters 10^^2
};

//------------------------------------------------------------------------------
// exemplar structures, you might use anything different
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PALETTE
//------------------------------------------------------------------------------

typedef uint32 RGBA;
typedef struct LUT {
    uint count = 0;
    RGBA rgba[256] = { 0 };
} LUT;

inline unsigned char xR(RGBA rgb) {
    return (unsigned char)((rgb >> 16) & 0xff);
}

inline unsigned char xG(RGBA rgb) {
    return (unsigned char)((rgb >> 8) & 0xff);
}

inline unsigned char xB(RGBA rgb) {
    return (unsigned char)(rgb & 0xff);
}

inline unsigned char xA(RGBA rgb) {
    return (unsigned char)((rgb >> 24) & 0xff);
}

inline RGBA xRGB(int r, int g, int b) {
    return (0xff << 24) | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
}

inline RGBA xRGBA(int r, int g, int b, int a) {
    return ((a & 0xff) << 24) | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
}

inline int xGray(int r, int g, int b) {
    return (r * 11 + g * 16 + b * 5) / 32;
}

inline int xGray(RGBA rgb) {
    return xGray(xR(rgb), xG(rgb), xB(rgb));
}


//------------------------------------------------------------------------------
// DIMENSIONS v1.4
// The default dimension order is: XYCZT
// each image MUST have XYC dimenstions as first in the list!!!
// C MUST be third dimention in order for the paged approach to work
// image is constituted of pages with XYC dimentions
// then the sequence of pages is interpreted as Z, T, ZT
//------------------------------------------------------------------------------

constexpr unsigned int BIM_MAX_DIMS = 16;

enum class ImageDims : unsigned int {
    DIM_X = 0,
    DIM_Y = 1,
    DIM_C = 2,
    DIM_Z = 3,
    DIM_T = 4,
    DIM_SERIE = 5,
    DIM_FOV = 6,
    DIM_ROTATION = 7,
    DIM_SCENE = 8,
    DIM_ILLUM = 9,
    DIM_PHASE = 10,
    DIM_VIEW = 11,
    DIM_ITEM = 12,
    DIM_SPECTRA = 13,
    DIM_MEASURE = 14,
    DIM_NONE = 15
};

//------------------------------------------------------------------------------
// IMAGE
//------------------------------------------------------------------------------

typedef struct ImageInfo {
    uint32 ver = 0; // must be == sizeof(ImageInfo)

    uint64 width = 0;
    uint64 height = 0;

    uint64 number_pages = 0;  // number of images within the file // to be deprectaetd in v3.0, use TagMap for this
    uint64 number_levels = 1; // v1.1 number of levels in resolution pyramid // to be deprectaetd in v3.0, use TagMap for this

    // interpretative parameters, not needed for actual image decoding
    uint64 number_t = 0;                 // v1.4 number of time points // to be deprectaetd in v3.0, use TagMap for this
    uint64 number_z = 0;                 // v1.4 number of z depth points // to be deprectaetd in v3.0, use TagMap for this
    uint32 number_dims = 2;              // v1.4 number of dimensions // to be deprectaetd in v3.0, use TagMap for this

    uint32 samples = 0;                               // Samples per pixel
    uint32 depth = 0;                                 // Bits per sample, currently must be 8 or 16
    ImageModes imageMode = ImageModes::IM_GRAYSCALE;  // Image mode as declared in bim::ImageModes
    DataFormat pixelType = DataFormat::FMT_UNSIGNED; // type in which the pixels are stored, changed in v1.8
                                                      // related to depth but differentiate signed/unsigned/floats etc.
    DataType rowAlignment = DataType::TAG_NOTYPE;     // type to which each row is aligned v1.5 to compute line strides

    LUT lut; // LUT, only used for Indexed Color images // to be deprectaetd in v3.0, use TagMap for this

    ResolutionUnits resUnits = ResolutionUnits::RES_m; // resolution units defined by: BIM_ResolutionUnits // to be deprectaetd in v3.0, use TagMap for this
    float64 xRes = 0.f;  // pixels per unit // to be deprectaetd in v3.0, use TagMap for this
    float64 yRes = 0.f;  // pixels per unit // to be deprectaetd in v3.0, use TagMap for this

    uint64 tileWidth = 0;  // The width of the tiles. Zero if not set // to be deprectaetd in v3.0, use TagMap for this
    uint64 tileHeight = 0; // The height of the tiles. Zero if not set // to be deprectaetd in v3.0, use TagMap for this
    ImageInfo() {
        this->ver = sizeof(ImageInfo);
    }
} ImageInfo;

// here we should store samples like this: 0, 1, 2...
// sample meaning is defined by imageMode in ImageInfo

#define BIM_MAX_CHANNELS 64 // currently number of channels greater than 32 provokes switching to spectral mode

struct ImageBitmap {
    ImageInfo i;
    //void *bits[BIM_MAX_CHANNELS]; // pointer to RAW data by samples i.e. plane by plane, now restricted to 512 planes
    std::vector<void*> bits;

    ImageBitmap() {
        this->bits.resize(BIM_MAX_CHANNELS, 0);
        bim::initImagePlanes(this);
    }

    void *plane(bim::uint64 i) { return this->bits[i]; } // access plane memory, useful from outside of dynamic library if exported
};


//******************************************************************************
// interface wide data type macros, define them using data types you desire
//******************************************************************************

class TagMap; // v1.7 the class for textual metadata storage
class XConf;  // v2.3 the class for input arguments

// some macros for data types used by interface
#define BIM_IMAGE_CLASS ImageBitmap
#define BIM_STRING_CLASS const char

//#define BIM_STREAM_CLASS        FILE
#define BIM_STREAM_CLASS void

#define BIM_PARAM_STREAM_CLASS FILE
#define BIM_INTPARAMS_CLASS void
#define BIM_PARENT_WIN_CLASS void
#define BIM_DIALOG_WIN_CLASS void
#define BIM_MAGIC_STREAM void // could be stream or just memory buffer...
#define BIM_IMG_SERVICES void // by now it's empty
#define BIM_FORMAT_ID uint32  // this number is a number in FormatList

#define BIM_OPTIONS_CLASS char
#define BIM_METADATA_CLASS TagMap // v1.7
#define BIM_ARGUMENTS_CLASS XConf // v2.3

#define BIM_SIZE_T uint64
#define BIM_OFFSET_T int64


//******************************************************************************
// plug-in receves from the host program
//******************************************************************************

//-----------------------------------------------------
// generic functions that host provides
//-----------------------------------------------------
// host shows progress of the executon
// done  - value in a range of [0..total]
// total - range maximum
// descr - any text describing the executed procedure
typedef void (*ProgressProc)(uint64 done, uint64 total, const char *descr);

// host shows error of the executon
// val   - error code, not treated at the moment
// descr - any text describing the error
typedef void (*ErrorProc)(int val, const char *descr);

// plugin should use this to test is processing should be interrupted...
// If it returns true, the operation should be aborted
typedef int (*TestAbortProc)(void);


//-----------------------------------------------------
// memory allocation prototypes, if these are null then
// formats should use standard way of memory allocation
// using malloc...
//-----------------------------------------------------
typedef void *(*MallocProc)(uint64 size);
typedef void *(*FreeProc)(void *p);

//-----------------------------------------------------
// io prototypes, if these are null then formats should
// use standard C io functions: fwrite, fread...
//-----------------------------------------------------

// (fread)  read from stream
typedef uint64 (*ReadProc)(void *buffer, uint64 size, uint64 count, BIM_STREAM_CLASS *stream);

// (fwrite) write into stream
typedef uint64 (*WriteProc)(void *buffer, uint64 size, uint64 count, BIM_STREAM_CLASS *stream);

// (flush) flushes a stream
typedef int (*FlushProc)(BIM_STREAM_CLASS *stream);

// (fseek)  seek within stream
typedef int (*SeekProc)(BIM_STREAM_CLASS *stream, int64 offset, int origin);

// get file "stream" size (libtiff askes for it)
typedef uint64 (*SizeProc)(BIM_STREAM_CLASS *stream);

// (ftell) gets the current position in stream
typedef int64 (*TellProc)(BIM_STREAM_CLASS *stream);

// (feof) tests for end-of-file on a stream
typedef int (*EofProc)(BIM_STREAM_CLASS *stream);

// (feof) tests for end-of-file on a stream
typedef int (*CloseProc)(BIM_STREAM_CLASS *stream);

//-----------------------------------------------------


//******************************************************************************
// structure passed by host to a format function
//******************************************************************************

typedef char *Filename;

typedef struct FormatHandle {

    uint32 ver = 0; // must be == sizeof(FormatHandle)

    // IN
    ProgressProc showProgressProc = 0; // function provided by host to show plugin progress
    ErrorProc showErrorProc = 0;       // function provided by host to show plugin error
    TestAbortProc testAbortProc = 0;   // function provided by host to test if plugin should interrupt processing

    MallocProc mallocProc = 0; // function provided by host to allocate memory
    FreeProc freeProc = 0;     // function provided by host to free memory

    // some standard parameters are defined here, any specific goes inside internalParams
    BIM_MAGIC_STREAM *magic = 0;
    BIM_FORMAT_ID subFormat = 0;  // sub format used to read or write, same as FormatItem
    uint64 pageNumber = 0;        // page number to retreive/write, if number is invalid thenpage 0 is retreived
    uint32 resolutionLevel = 0;   // v1.1 resolution level retreive/write, considered 0 (highest) as default
    uchar quality = 0;            // number between 0 and 100 to specify quality (read/write)
    uint32 compression = 0;       // 0 - specify lossless method
    uint32 order = 0;             // progressive, normal, interlaced...
    uint32 roiX = 0;              // v1.1 Region Of Interest: Top Left X (pixels), ONLY used for reading!!!
    uint32 roiY = 0;              // v1.1 Region Of Interest: Top Left Y (pixels), ONLY used for reading!!!
    uint32 roiW = 0;              // v1.1 Region Of Interest: Width (pixels), ONLY used for reading!!!
    uint32 roiH = 0;              // v1.1 Region Of Interest: Height (pixels), ONLY used for reading!!!
    BIM_METADATA_CLASS *metaData = 0; // v1.7 pointer to TagMap for textual metadata, ONLY used for writing, reading is done by AppendMetaDataProc

    BIM_IMG_SERVICES *imageServicesProcs = 0; // The suite of image processing services callbacks
    BIM_INTPARAMS_CLASS *internalParams = 0;  // internal plugin parameters

    BIM_PARENT_WIN_CLASS *parent = 0; // pointer to parent window
    void *hDllInstance = 0;           // handle to loaded dynamic library instance (plug-in)

    bim::Filename fileName = 0;   // file name
    BIM_STREAM_CLASS *stream = 0; // pointer to a file stream that might be open by host
    ImageIOModes io_mode = ImageIOModes::IO_READ; // v1.2 io mode for opened stream

    // I/O callbacks
    ReadProc readProc = 0; // v1.2 (fread)  read from stream
    WriteProc writeProc = 0; // v1.2 (fwrite) write into stream
    FlushProc flushProc = 0; // v1.2 (flush) flushes a stream
    SeekProc seekProc = 0;   // v1.2 (fseek)  seek within stream
    SizeProc sizeProc = 0;   // v1.2 () get file "stream" size (libtiff askes for it)
    TellProc tellProc = 0;   // v1.2 (ftell) gets the current position in stream
    EofProc eofProc = 0;     // v1.2 (feof) tests for end-of-file on a stream
    CloseProc closeProc = 0; // v1.2 (fclose) close stream

    // IN/OUT
    BIM_IMAGE_CLASS *image = 0;     // pointer to an image structure to read/write
    BIM_OPTIONS_CLASS *options = 0; // v1.6 pointer to encoding options string
    BIM_ARGUMENTS_CLASS *arguments = 0; // v2.3 pointer to XConf for input aruments passed to readers

    uint64 numberOfPages = 0; // v2.4 total number of pages to write

} FormatHandle;
//-----------------------------------------------------


//******************************************************************************
// plug-in provides to the host program
//******************************************************************************

//------------------------------------------------------------------------------
//  FORMAT
//------------------------------------------------------------------------------

// using BIM_DEF instead of direct assigmnet of a default value due to GCC 
// inability to do static assigmnets with inited values

// in all restrictions 0 means no restriction
typedef struct FormatConstrains {
    uint32 maxWidth BIM_DEF(0);
    uint32 maxHeight BIM_DEF(0);
    uint32 maxPageNumber BIM_DEF(0);
    uint32 minSamplesPerPixel BIM_DEF(0); // v1.3
    uint32 maxSamplesPerPixel BIM_DEF(0); // v1.3
    uint32 minBitsPerSample BIM_DEF(0);   // v1.3
    uint32 maxBitsPerSample BIM_DEF(0);   // v1.3
    bool lutNotSupported BIM_DEF(false);      // v1.3
} FormatConstrains;

typedef struct FormatItem {
    const char *formatNameShort BIM_DEF(0); // short name, no spaces
    const char *formatNameLong BIM_DEF(0);  // Long format name
    const char *extensions BIM_DEF(0);      // pipe "|" separated supported extension list
    bool canRead BIM_DEF(false);                // 0 - NO, 1 - YES v1.3
    bool canWrite BIM_DEF(false);               // 0 - NO, 1 - YES v1.3
    bool canReadMeta BIM_DEF(false);            // 0 - NO, 1 - YES v1.3
    bool canWriteMeta BIM_DEF(false);           // 0 - NO, 1 - YES v1.3
    bool canWriteMultiPage BIM_DEF(false);      // 0 - NO, 1 - YES v1.3
    FormatConstrains constrains;     //  v1.3
} FormatItem;

typedef struct FormatList {
    uint32 version BIM_DEF(0);
    uint32 count BIM_DEF(0);
    FormatItem *item;
} FormatList;

//------------------------------------------------------------------------------
// NULL for any of this function means unavailable function
//   ex: only reader shuld return NULL for all reader functions
//------------------------------------------------------------------------------

// this wil return if image can be opened, v 1.9
typedef int (*ValidateFormatProc)(BIM_MAGIC_STREAM *magic, uint length, const bim::Filename fileName);

//------------------------------------------------------------------------------
// FORMAT PARAMS and ALLOCATION
//------------------------------------------------------------------------------

// allocate space for parameters stuct and return it
typedef FormatHandle (*AquireFormatProc)();

// delete parameters struct
typedef void (*ReleaseFormatProc)(FormatHandle *fmtHndl);

// return dialog window class for parameter initialization or create the window by itself
typedef BIM_DIALOG_WIN_CLASS *(*AquireIntParamsProc)(FormatHandle *fmtHndl);

// load/save parameters into stream
typedef void (*LoadFormatParamsProc)(FormatHandle *fmtHndl, BIM_PARAM_STREAM_CLASS *stream);
typedef void (*StoreFormatParamsProc)(FormatHandle *fmtHndl, BIM_PARAM_STREAM_CLASS *stream);

//------------------------------------------------------------------------------
// OPEN/CLOSE IMAGE PROCs
//------------------------------------------------------------------------------

typedef uint (*OpenImageProc)(FormatHandle *fmtHndl, ImageIOModes io_mode);
typedef uint (*FOpenImageProc)(FormatHandle *fmtHndl, Filename fileName, ImageIOModes io_mode);
typedef uint (*IOpenImageProc)(FormatHandle *fmtHndl, Filename fileName,
                               BIM_IMAGE_CLASS *image, ImageIOModes io_mode);

typedef void (*CloseImageProc)(FormatHandle *fmtHndl);

//------------------------------------------------------------------------------
// IMAGE INFO PROCs
//------------------------------------------------------------------------------

// image info procs
typedef uint (*GetNumPagesProc)(FormatHandle *fmtHndl);
// image info for each page proc
typedef ImageInfo (*GetImageInfoProc)(FormatHandle *fmtHndl, uint page_num);


//------------------------------------------------------------------------------
// READ/WRITE PROCs
//------------------------------------------------------------------------------

// these are the actual read/write procs
typedef uint (*ReadImageProc)(FormatHandle *fmtHndl, uint page);
typedef uint (*WriteImageProc)(FormatHandle *fmtHndl);

typedef uint (*ReadImageTileProc)(FormatHandle *fmtHndl, uint page, uint64 xid, uint64 yid, uint level);  // v2.0, modified
typedef uint (*WriteImageTileProc)(FormatHandle *fmtHndl, uint page, uint64 xid, uint64 yid, uint level); // v2.0, modified

typedef uint (*ReadImageLevelProc)(FormatHandle *fmtHndl, uint page, uint level);  // v2.0, redesignated
typedef uint (*WriteImageLevelProc)(FormatHandle *fmtHndl, uint page, uint level); // v2.0, redesignated

typedef uint (*ReadImageRegionProc)(FormatHandle *fmtHndl, uint page, uint64 x1, uint64 y1, uint64 x2, uint64 y2, uint level);  // v2.6
typedef uint (*WriteImageRegionProc)(FormatHandle *fmtHndl, uint page, uint64 x1, uint64 y1, uint64 x2, uint64 y2, uint level); // v2.6

//------------------------------------------------------------------------------
// METADATA PROCs
//------------------------------------------------------------------------------

// v1.7 New format metadata appending function, that should append fields
// directly into provided HashTable, this allows performing all parsing on the reader side
typedef uint (*AppendMetaDataProc)(FormatHandle *fmtHndl, BIM_METADATA_CLASS *hash);


//------------------------------------------------------------------------------
// *** HEADER
// structure that plug-in should provide for the host program
//------------------------------------------------------------------------------

typedef struct FormatHeader {
    bim::uint32 ver BIM_DEF(0); // must be == sizeof(FormatHeader)
    const char *version BIM_DEF(0); // plugin version, e.g. "1.0.0"
    const char *name BIM_DEF(0);    // plugin name
    const char *description BIM_DEF(0); // plugin description

    bim::uint32 neededMagicSize BIM_DEF(0); // 0 or more, specify number of bytes needed to identify the file
    FormatList supportedFormats;

    ValidateFormatProc validateFormatProc BIM_DEF(0); // return true if can handle file
    AquireFormatProc aquireFormatProc BIM_DEF(0);
    ReleaseFormatProc releaseFormatProc BIM_DEF(0);

    // params
    AquireIntParamsProc aquireParamsProc BIM_DEF(0);
    LoadFormatParamsProc loadParamsPro BIM_DEF(0);
    StoreFormatParamsProc storeParamsProc BIM_DEF(0);

    // image begin
    OpenImageProc openImageProc BIM_DEF(0);
    CloseImageProc closeImageProc BIM_DEF(0);

    // info
    GetNumPagesProc getNumPagesProc BIM_DEF(0);
    GetImageInfoProc getImageInfoProc BIM_DEF(0);

    // read/write
    ReadImageProc readImageProc BIM_DEF(0);
    WriteImageProc writeImageProc BIM_DEF(0);
    ReadImageTileProc readImageTileProc BIM_DEF(0); // v2.0, modified
    WriteImageTileProc writeImageTileProc BIM_DEF(0); // v2.0, modified
    ReadImageLevelProc readImageLevelProc BIM_DEF(0); // v2.0, reassigned
    WriteImageLevelProc writeImageLevelProc BIM_DEF(0); // v2.0, reassigned
    ReadImageRegionProc readImageRegionProc BIM_DEF(0); // v2.6, reassigned
    WriteImageRegionProc writeImageRegionProc BIM_DEF(0); // v2.6, reassigned

    // metadata
    AppendMetaDataProc appendMetaDataProc BIM_DEF(0); // v1.7
} FormatHeader;


// -----------------------------------------------------------------------------
// function prototypes
// -----------------------------------------------------------------------------

// This functions must be exported so plug-in could be accepted as valid by host
typedef FormatHeader *(*GetFormatHeader)(void);

} // namespace bim

#endif //BIM_IMG_FMT_IFC_H
