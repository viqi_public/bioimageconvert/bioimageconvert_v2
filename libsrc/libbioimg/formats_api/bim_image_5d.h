/*******************************************************************************

  5D image - defines a 5D image navigable by Z and T coordinates

  image is constructed as a list of T points each represented by an image stack
  each Z location in a stack is represented by an image where pixels can be
  accessed per channel

  image also contains all associated metadata as tags

  time points are cached in-memory by the last access time (based on allowed cache size)

  Author: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  History:
     - First creation

  ver: 1

*******************************************************************************/

#ifndef BIM_IMAGE_5D_H
#define BIM_IMAGE_5D_H

#include <list>
#include <vector>

#include "bim_image.h"
#include "bim_image_stack.h"
#include <bim_format_manager.h>


namespace bim {

class Image5D {
public:
    const int number_axis;
    enum Axis { x = 0,
                y = 1,
                z = 2,
                t = 3,
                c = 4 };

public:
    Image5D();
    ~Image5D();

    void init();
    void clear();

    bool isLoaded() const;
    bool isLoaded(const std::string &fileName) const;
    bool isCached() const;
    bool isCached(uint64 t) const;
    Image *imageAt(uint64 t, uint64 z);
    ImageStack *stackAt(uint64 t);
    //inline Image *operator[](unsigned int p) const { return imageAt(p); }

    ImageStack *stack();               // stack at the currently selected position
    const ImageStack *current() const; // difference with stack() is that current() is not going to load the image
    Image *image();                    // image at the currently selected position
    void resetPosition();
    bool nextTimePoint();
    bool prevTimePoint();
    bool setTimePoint(int64 t);
    bool nextZPosition();
    bool prevZPosition();
    bool setZPosition(int64 z);

    uint64 currentT() const { return current_position[Image5D::t]; }
    uint64 currentZ() const { return current_position[Image5D::z]; }
    uint64 numberT() const { return image_size[Image5D::t]; }
    uint64 numberZ() const { return image_size[Image5D::z]; }

    uint64 numberC() const { return image_size[Image5D::c]; }
    uint64 numberX() const { return image_size[Image5D::x]; }
    uint64 numberY() const { return image_size[Image5D::y]; }

    bool getTimeLooping() const { return loop_time; }
    void setTimeLooping(bool v) { loop_time = v; }

    bool fromFile(const char *fileName) { return fromFile(fileName); }
    bool fromFile(const std::string &fileName);

    double getMaximumCacheSize() const { return maximum_cache_size; }
    void setMaximumCacheSize(const double &bytes) { maximum_cache_size = bytes; }

    const TagMap *getMetadata() const { return &metadata; }
    std::string fileName() const { return fm.sessionFilename(); }

protected:
    // prohibit copy-constructor
    Image5D(const Image5D &) : number_axis(5) {}

protected:
    std::vector<uint64> image_size;
    std::vector<uint64> current_position;
    double maximum_cache_size=0;
    bool loop_time=0;

    std::vector<ImageStack> stacks;
    std::list<ImageStack *> cache_priority; // images in the front are the most likely to be removed
    TagMap metadata;

private:
    FormatManager fm;
    void initFromMeta();
    void updateCache();
    std::vector<unsigned int> pagesOf(unsigned int t) const;
    double memorySize() const;

    // callbacks
public:
    ProgressProc progress_proc = 0;
    ErrorProc error_proc = 0;
    TestAbortProc test_abort_proc = 0;

protected:
    void do_progress(long done, long total, char *descr) {
        if (progress_proc) progress_proc(done, total, descr);
    }
    bool progress_abort() {
        if (!test_abort_proc) return false;
        return (test_abort_proc() != 0);
    }
};

} // namespace bim

#endif // BIM_IMAGE_5D_H
