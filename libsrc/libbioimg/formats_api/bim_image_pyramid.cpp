/*******************************************************************************

  Defines Image Pyramid Class

  Programmer: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  History:
    03/23/2004 18:03 - First creation

  ver: 1

*******************************************************************************/

#include <cmath>

#include "bim_image_pyramid.h"
#include <xstring.h>
#include <xtypes.h>

using namespace bim;

ImagePyramid::ImagePyramid() {
    init();
}

ImagePyramid::ImagePyramid(const Image &img, const uint64 v) {
    init();
    if (v > 0)
        setMinImageSize(v);
    this->createFrom(img);
}

ImagePyramid::~ImagePyramid() {
    free();
}

void ImagePyramid::init() {
    ImageStack::init();
    min_width = d_min_image_size;
    min_height = d_min_image_size;
}

uint64 ImagePyramid::levelClosestTop(const uint64 w, const uint64 h) const {
    if (handling_image) return 0;
    for (uint64 i = images.size(); i > 0; --i) {
        if ((images.at(i-1).width() >= w) ||
            (images.at(i-1).height() >= h)) return i-1;
    }
    return 0;
}

uint64 ImagePyramid::levelClosestBottom(const uint64 w, const uint64 h) const {
    if (handling_image) return 0;
    if (images.size() <= 1) return 0;

    for (uint64 i = images.size() - 2; i > 0; --i) {
        if ((images.at(i-1).width() > w) ||
            (images.at(i-1).height() > h)) return i;
    }
    return 0;
}


void ImagePyramid::createFrom(const Image &img) {
    handling_image = true;
    images.clear();
    images.push_back(img);
    cur_position = 0;

    uint64 side = std::max(images[0].width(), images[0].height());
    uint64 max_levels = (uint64)std::ceil(bim::log2<double>((double)side));
    uint64 i = 0;
    while (std::max(images[i].width(), images[i].height()) > min_width) {
        xstring x;
        x.sprintf("Constructing pyramid, level %d", i + 1);
        do_progress(i + 1, max_levels, (char *)x.c_str());
        images.push_back(images[i].downSampleBy2x());
        ++i;
    }
    handling_image = false;
}

bool ImagePyramid::fromFile(const char *fileName, int page, XConf *c) {
    cur_position = 0;
    images.clear();
    Image img;
    bool res = img.fromFile(fileName, page, c);
    if (res) {
        img = img.ensureTypedDepth();
        img = img.ensureColorSpace();
        createFrom(img);
    }
    return res;
}
