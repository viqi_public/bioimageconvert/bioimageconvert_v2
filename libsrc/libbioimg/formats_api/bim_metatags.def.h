/*****************************************************************************
 Tag names for metadata

 Author: Dmitry Fedorov <mailto:dima@viqi.org> <http://www.viqi.org/>
 Copyright (c) 2019, ViQI Inc

 History:
   2010-07-29 17:18:22 - First creation

 Ver : 1
*****************************************************************************/

// Ensure this header is not included directly, only via its companion
#if !defined(BIM_IMG_META_TAGS) || !defined(DECLARE_STR)
static_assert(false, "Please do not include this header directly. Instead include bim_metatags.h");
#endif

DECLARE_STR(META_VERSION_TAG, "metadata_version")
DECLARE_STR(META_VERSION, "3.3.0")

// image - used to annotate image parameters
//DECLARE_STR(IMAGE_DATE_TIME, "date_time") // deprecated as of v3 in favor of DOCUMENT_DATETIME
DECLARE_STR(IMAGE_FORMAT, "format")
//DECLARE_STR(IMAGE_NAME_TEMPLATE, "image_%d_name") // deprecated as of v3 in favor of DOCUMENT_DESCRIPTION

// image geometry - describes image parameters
DECLARE_STR(IMAGE_NUM_X, "image_num_x") // number pixels in X
DECLARE_STR(IMAGE_NUM_Y, "image_num_y") // number pixels in Y
DECLARE_STR(IMAGE_NUM_Z, "image_num_z") // number pixels (voxels) in Z
DECLARE_STR(IMAGE_NUM_T, "image_num_t") // number of time points
DECLARE_STR(IMAGE_NUM_C, "image_num_c") // number of channels
DECLARE_STR(IMAGE_NUM_P, "image_num_p") // total number of pages in the image (combines T, Z and possibly C and other dimensions)

DECLARE_STR(IMAGE_NUM_SERIES, "image_num_series")     // number of independent N-D images within the package, series will be pointed by paths, where paths may simply be numbers
DECLARE_STR(IMAGE_NUM_FOV, "image_num_fovs")          // number of Fileds Of View - arbitrarily positioned tile blocks
DECLARE_STR(IMAGE_NUM_RT, "image_num_rotations")      // rotations: for SPIM devices
DECLARE_STR(IMAGE_NUM_SC, "image_num_scenes")         // scenes - Zeiss concept of non-overlapping regions
DECLARE_STR(IMAGE_NUM_IL, "image_num_illuminations")  // SPIM illuminations
DECLARE_STR(IMAGE_NUM_PH, "image_num_phases")         // phases
DECLARE_STR(IMAGE_NUM_VIEW, "image_num_views")        // sub-views of the same scene, like present in Zeiss CZI
DECLARE_STR(IMAGE_NUM_LABELS, "image_num_labels")     // label images attached, like barcode images in wholeslides
DECLARE_STR(IMAGE_NUM_PREVIEWS, "image_num_previews") // preview images attached, like glass slide images in wholeslides
DECLARE_STR(IMAGE_NUM_ITEMS, "image_num_items")       // sparce object images stored within
DECLARE_STR(IMAGE_NUM_SPECTRA, "image_num_spectra")   // for spectral image data, use dimension-like access
DECLARE_STR(IMAGE_NUM_MEASURES, "image_num_measures")   // for heatmap and mask data

// new definition for series access
DECLARE_STR(IMAGE_SERIES_PATHS, "image_series_paths") // paths to access sub-series, can simply be numeric or may be full paths like: group0/sub1/sub2/series3
DECLARE_STR(IMAGE_CURRENT_PATH, "image_path")

DECLARE_STR(IMAGE_NUM_RES_L, "image_num_resolution_levels")
DECLARE_STR(IMAGE_NUM_RES_L_ACTUAL, "image_num_resolution_levels_actual")
DECLARE_STR(IMAGE_RES_L_SCALES, "image_resolution_level_scales")
DECLARE_STR(IMAGE_RES_L_SCALES_ACTUAL, "image_resolution_level_scales_actual")
DECLARE_STR(TILE_NUM_X, "tile_num_x")
DECLARE_STR(TILE_NUM_Y, "tile_num_y")
DECLARE_STR(TILE_SIZE_X, "tile_size_x")
DECLARE_STR(TILE_SIZE_Y, "tile_size_y")
DECLARE_STR(IMAGE_RES_STRUCTURE, "image_resolution_level_structure")
DECLARE_STR(IMAGE_REGION_MAX_WIDTH, "image_region_max_width")
DECLARE_STR(IMAGE_REGION_MAX_HEIGHT, "image_region_max_height")

// there are possible structures:
DECLARE_STR(IMAGE_RES_STRUCTURE_HIERARCHICAL, "hierarchical") // default: structure where tile sizes remain constant
DECLARE_STR(IMAGE_RES_STRUCTURE_FLAT, "flat")                 // structure where tiles get progressively smaller
DECLARE_STR(IMAGE_RES_STRUCTURE_ARBITRARY, "arbitrary")       // arbitrarily positioned and scaled blocks in the common space

// image pixels - describes pixel parameters
DECLARE_STR(PIXEL_DEPTH, "image_pixel_depth")   // bit depth of one pixel in a channel
DECLARE_STR(PIXEL_FORMAT, "image_pixel_format") // unsigned, signed, float
DECLARE_STR(IMAGE_MODE, "image_mode")           // RGB, YUV, LAB...
DECLARE_STR(PIXEL_SIGNIFICANT_BITS, "image_pixel_significant_bits")   // bit depth of the data, may be less than the storage pixel depth


DECLARE_STR(RAW_ENDIAN, "raw_endian")             // little, big
DECLARE_STR(IMAGE_DIMENSIONS, "image_dimensions") // XYCZT, XYZ, XYC, ...

DECLARE_STR(IMAGE_DIMENSION_X, "x")                       //
DECLARE_STR(IMAGE_DIMENSION_Y, "y")                       //
DECLARE_STR(IMAGE_DIMENSION_C, "c")                       //
DECLARE_STR(IMAGE_DIMENSION_Z, "z")                       //
DECLARE_STR(IMAGE_DIMENSION_T, "t")                       //
DECLARE_STR(IMAGE_DIMENSION_FOV, "fov")                   // number of Fileds Of View - arbitrarily positioned tile blocks
DECLARE_STR(IMAGE_DIMENSION_SERIE, "serie")               // number of independent N-D images within the package, series will be pointed by paths, where paths may simply be numbers
DECLARE_STR(IMAGE_DIMENSION_ROTATION, "rotation")         // rotations: for SPIM devices
DECLARE_STR(IMAGE_DIMENSION_SCENE, "scene")               // scenes - Zeiss concept of non-overlapping regions
DECLARE_STR(IMAGE_DIMENSION_ILLUMINATION, "illumination") // SPIM illuminations
DECLARE_STR(IMAGE_DIMENSION_PHASE, "phase")               // phases
DECLARE_STR(IMAGE_DIMENSION_VIEW, "view")                 // sub-views of the same scene, like present in Zeiss CZI
DECLARE_STR(IMAGE_DIMENSION_LABEL, "label")               // label images attached, like barcode images in wholeslides
DECLARE_STR(IMAGE_DIMENSION_PREVIEW, "preview")           // preview images attached, like glass slide images in wholeslides
DECLARE_STR(IMAGE_DIMENSION_ITEM, "item")                 // sparce object images stored within
DECLARE_STR(IMAGE_DIMENSION_SPECTRUM, "spectrum")         // for spectral image data, use dimension-like access and metadata to know what's available
DECLARE_STR(IMAGE_DIMENSION_MEASURES, "measures")         // like spectra, carry values of some measurements, use dimension-like access and metadata to know what's available

// spectral info
DECLARE_STR(IMAGE_SPECTRAL_WAVELENGTHS, "image_spectral_wavelengths")           // an array of wavelengths for spectral bands, must be same size
DECLARE_STR(IMAGE_SPECTRAL_WAVELENGTH_UNITS, "image_spectral_wavelength_units") // string with units of the wavelengths

// resolution - used to provide resolution values for pixels/voxels
DECLARE_STR(PIXEL_RESOLUTION_X, "pixel_resolution_x")
DECLARE_STR(PIXEL_RESOLUTION_Y, "pixel_resolution_y")
DECLARE_STR(PIXEL_RESOLUTION_Z, "pixel_resolution_z")
DECLARE_STR(PIXEL_RESOLUTION_T, "pixel_resolution_t")

// resolution units - used to provide units for resolution values of pixels/voxels
DECLARE_STR(PIXEL_RESOLUTION_UNIT_X, "pixel_resolution_unit_x")
DECLARE_STR(PIXEL_RESOLUTION_UNIT_Y, "pixel_resolution_unit_y")
DECLARE_STR(PIXEL_RESOLUTION_UNIT_Z, "pixel_resolution_unit_z")
DECLARE_STR(PIXEL_RESOLUTION_UNIT_T, "pixel_resolution_unit_t")
DECLARE_STR(PIXEL_RESOLUTION_UNIT_C, "pixel_resolution_unit_c")

DECLARE_STR(PIXEL_RESOLUTION_UNIT_MICRONS, "microns")
DECLARE_STR(PIXEL_RESOLUTION_UNIT_SECONDS, "seconds")
DECLARE_STR(PIXEL_RESOLUTION_UNIT_METERS, "meters")

// some typical image descriptions
//DECLARE_STR(IMAGE_LABEL, "image/label")             // deprecate as of v3.2
//DECLARE_STR(IMAGE_DESCRIPTION, "image/description") // deprecate as of v3.2
//DECLARE_STR(IMAGE_NOTES, "image/notes")             // deprecate as of v3.2
//DECLARE_STR(IMAGE_ZOOM, "image/zoom")               // deprecate as of v3.2
//DECLARE_STR(IMAGE_USER, "image/user")               // deprecate as of v3.2
//DECLARE_STR(IMAGE_POSITION, "image/position")       // deprecate as of v3.2
//DECLARE_STR(IMAGE_SIZE, "image/size")               // deprecate as of v3.2

DECLARE_STR(DOCUMENT_COMPUTER, "document/computer_name")
DECLARE_STR(DOCUMENT_USER, "document/username")
DECLARE_STR(DOCUMENT_ASSAY, "document/assay_name")
DECLARE_STR(DOCUMENT_SCREEN, "document/screen_name")
DECLARE_STR(DOCUMENT_PLATE, "document/plate_name")
DECLARE_STR(DOCUMENT_PLATE_DESCR, "document/plate_description")
DECLARE_STR(DOCUMENT_PLATE_ID, "document/plate_id")
DECLARE_STR(DOCUMENT_VENDOR, "document/vendor")
DECLARE_STR(DOCUMENT_INSTRUMENT, "document/instrument_name")
DECLARE_STR(DOCUMENT_DATETIME, "document/acquisition_date")
DECLARE_STR(DOCUMENT_APPLICATION, "document/application")
DECLARE_STR(DOCUMENT_APPLICATION_VERSION, "document/application_version")
DECLARE_STR(DOCUMENT_SLIDE_ID, "document/slide_id")
DECLARE_STR(DOCUMENT_GUID, "document/GUID")
DECLARE_STR(DOCUMENT_DESCRIPTION, "document/description")
DECLARE_STR(DOCUMENT_NOTES, "document/notes")
DECLARE_STR(DOCUMENT_QUICKHASH, "document/quickhash")
DECLARE_STR(DOCUMENT_BARCODE, "document/barcode")
DECLARE_STR(DOCUMENT_VERSION, "document/version")
DECLARE_STR(DOCUMENT_BACKGROUND_COLOR, "document/background_color")

DECLARE_STR(DOCUMENT_WELL_NAME, "document/well_label") // v3 renamed from DOCUMENT_WELL
DECLARE_STR(DOCUMENT_WELL_ROW, "document/well_row")
DECLARE_STR(DOCUMENT_WELL_COL, "document/well_col")
DECLARE_STR(DOCUMENT_WELL_SITE, "document/well_site")
DECLARE_STR(DOCUMENT_WELL_IJ, "document/well_ij")

DECLARE_STR(DOCUMENT_BOUNDS_X, "document/bounds_x")
DECLARE_STR(DOCUMENT_BOUNDS_Y, "document/bounds_y")
DECLARE_STR(DOCUMENT_BOUNDS_W, "document/bounds_width")
DECLARE_STR(DOCUMENT_BOUNDS_H, "document/bounds_height")

// objective - information about objective lens
DECLARE_STR(OBJECTIVE_DESCRIPTION, "objectives/objective:0/name")
DECLARE_STR(OBJECTIVE_MAGNIFICATION, "objectives/objective:0/magnification")
DECLARE_STR(OBJECTIVE_NUM_APERTURE, "objectives/objective:0/numerical_aperture")
DECLARE_STR(OBJECTIVE_REF_INDEX, "objectives/objective:0/refractive_index")
DECLARE_STR(OBJECTIVE_FOCAL_LENGTH, "objectives/objective:0/focal_length")
DECLARE_STR(OBJECTIVE0_PSF_SIGMA, "objectives/objective:0/psf_sigma")

// objective v3
DECLARE_STR(OBJECTIVE_INFO_TEMPLATE, "objectives/objective:%d/")
DECLARE_STR(OBJECTIVE_NAME, "name")
DECLARE_STR(OBJECTIVE_MAGNIFICATION_X, "magnification")
DECLARE_STR(OBJECTIVE_NUMERICAL_APERTURE, "numerical_aperture")
DECLARE_STR(OBJECTIVE_IMMERSION, "immersion")
DECLARE_STR(OBJECTIVE_REFRACTIVE_INDEX, "refractive_index")
DECLARE_STR(OBJECTIVE_PSF_SIGMA, "psf_sigma")

// measures v3
DECLARE_STR(MEASURE_TEMPLATE, "measures/measure:%d/")
DECLARE_STR(MEASURE_TEMPLATE_NAME, "measures/measure:%d/name")
DECLARE_STR(MEASURE_TEMPLATE_RANGE, "measures/measure:%d/range")
DECLARE_STR(MEASURE_TEMPLATE_FORMAT, "measures/measure:%d/format")
DECLARE_STR(MEASURE_TEMPLATE_CLASS_ID, "measures/measure:%d/class_id")
DECLARE_STR(MEASURE_TEMPLATE_CLASS_LABEL, "measures/measure:%d/class_label")
DECLARE_STR(MEASURE_TEMPLATE_INTERPOLATION, "measures/measure:%d/interpolation")
DECLARE_STR(MEASURE_TEMPLATE_RANDOMIZER, "measures/measure:%d/randomized")
DECLARE_STR(MEASURE_TEMPLATE_VALUES_LABEL_IDS, "measures/measure:%d/values_label_ids")

DECLARE_STR(MEASURE_0_NAME, "measures/measure:0/name")
DECLARE_STR(MEASURE_0_RANGE, "measures/measure:0/range")
DECLARE_STR(MEASURE_0_FORMAT, "measures/measure:0/format")

// class labels v3
DECLARE_STR(CLASS_LABELS_TEMPLATE, "labels/label:%d/")
DECLARE_STR(CLASS_LABELS_TEMPLATE_NAME, "labels/label:%d/name")
DECLARE_STR(CLASS_LABELS_TEMPLATE_ID, "labels/label:%d/id")

// channels - used for textual descriptions about image channels
//DECLARE_STR(CHANNEL_NAME_0, "channel_0_name")         // deprecated as of v3
//DECLARE_STR(CHANNEL_NAME_1, "channel_1_name")         // deprecated as of v3
//DECLARE_STR(CHANNEL_NAME_2, "channel_2_name")         // deprecated as of v3
//DECLARE_STR(CHANNEL_NAME_3, "channel_3_name")         // deprecated as of v3
//DECLARE_STR(CHANNEL_NAME_4, "channel_4_name")         // deprecated as of v3
//DECLARE_STR(CHANNEL_NAME_TEMPLATE, "channel_%d_name") // deprecated as of v3

// channel colors - used to map image channels to exact display colors
// channel_color_0: 255,255,0
//DECLARE_STR(CHANNEL_COLOR_0, "channel_color_0")         // deprecated as of v3
//DECLARE_STR(CHANNEL_COLOR_1, "channel_color_1")         // deprecated as of v3
//DECLARE_STR(CHANNEL_COLOR_2, "channel_color_2")         // deprecated as of v3
//DECLARE_STR(CHANNEL_COLOR_3, "channel_color_3")         // deprecated as of v3
//DECLARE_STR(CHANNEL_COLOR_4, "channel_color_4")         // deprecated as of v3
//DECLARE_STR(CHANNEL_COLOR_TEMPLATE, "channel_color_%d") // deprecated as of v3

// new style channel information
//DECLARE_STR(CHANNEL_INFO_TEMPLATE, "channels/channel_%.5d/") // deprecated as of v3
DECLARE_STR(CHANNEL_INFO_TEMPLATE, "channels/channel:%d/")  // new name as of v3
DECLARE_STR(CHANNEL_INFO_NAME, "name")
DECLARE_STR(CHANNEL_INFO_DATETIME, "datetime")
DECLARE_STR(CHANNEL_INFO_DESCRIPTION, "description")
DECLARE_STR(CHANNEL_INFO_COLOR, "color") // channels/channel_00001/color: 1.0,1.0,0.5 # old: 255,255,255
DECLARE_STR(CHANNEL_INFO_OPACITY, "opacity")
DECLARE_STR(CHANNEL_INFO_GAMMA, "gamma")
DECLARE_STR(CHANNEL_INFO_COLOR_RANGE, "range") // defined signal min/max range which defins maximum bounds for the values coming from the sensor
                                               // often used for 12 or 14 bit data packaged into 16 bit values
DECLARE_STR(CHANNEL_INFO_DISPLAY_RANGE, "display_range") // used to define suggested display min/max within [0,1] enhancement range
DECLARE_STR(CHANNEL_INFO_FLUOR, "fluor")
DECLARE_STR(CHANNEL_INFO_DYE, "dye")
DECLARE_STR(CHANNEL_INFO_BIOMARKER, "biomarker")
DECLARE_STR(CHANNEL_INFO_EM_WAVELENGTH, "emission_wavelength")
DECLARE_STR(CHANNEL_INFO_EX_WAVELENGTH, "excitation_wavelength")
DECLARE_STR(CHANNEL_INFO_PINHOLE_RADIUS, "pinhole_radius")
DECLARE_STR(CHANNEL_INFO_PINHOLE_RADIUS_UNITS, "pinhole_radius_units")
DECLARE_STR(CHANNEL_INFO_OBJECTIVE, "objective")
DECLARE_STR(CHANNEL_INFO_DETECTOR_GAIN, "detector_gain")
DECLARE_STR(CHANNEL_INFO_AMPLIFIER_GAIN, "amplifier_gain")
DECLARE_STR(CHANNEL_INFO_AMPLIFIER_OFFS, "amplifier_offset")
DECLARE_STR(CHANNEL_INFO_FILTER_NAME, "filter")
DECLARE_STR(CHANNEL_INFO_POWER, "power")
DECLARE_STR(CHANNEL_INFO_GAIN, "gain")
DECLARE_STR(CHANNEL_INFO_EXPOSURE, "exposure")
DECLARE_STR(CHANNEL_INFO_EXPOSURE_UNITS, "exposure_units")
DECLARE_STR(CHANNEL_INFO_MODALITY, "modality")
DECLARE_STR(CHANNEL_INFO_SPECTRAL_BAND, "spectral_band")
DECLARE_STR(CHANNEL_INFO_ZOOM, "zoom")
DECLARE_STR(CHANNEL_INFO_BINNING, "binning")
DECLARE_STR(CHANNEL_INFO_CAMERA, "camera")
DECLARE_STR(CHANNEL_INFO_LAMP, "lamp")
DECLARE_STR(CHANNEL_INFO_INSTRUMENT, "instrument")
DECLARE_STR(CHANNEL_INFO_RESPONSE, "response")

// video - information describing the video parameters
DECLARE_STR(VIDEO_FORMAT_NAME, "video_format_name")
DECLARE_STR(VIDEO_CODEC_NAME, "video_codec_name")
DECLARE_STR(VIDEO_FRAMES_PER_SECOND, "video_frames_per_second")

// custom - any other tags in proprietary files should go further prefixed by the custom parent
//DECLARE_STR( CUSTOM_TAGS_PREFIX, "custom/" ) // is deprecated as of v2.9
DECLARE_STR(RAW_TAGS_PREFIX, "raw/")

// planes - values per plane of the image
//DECLARE_STR(PLANE_DATE_TIME, "plane_date_time") // deprecated as of v3.2
//DECLARE_STR(PLANE_DATE_TIME_TEMPLATE, "plane_date_time/%d") // deprecated as of v3.2

// stage
//DECLARE_STR(STAGE_POSITION_X, "stage_position_x") // will be deprecated as of v3 in favor of COORDINATES_POINTS_FOVS
//DECLARE_STR(STAGE_POSITION_Y, "stage_position_y") // will be deprecated as of v3 in favor of COORDINATES_POINTS_FOVS
//DECLARE_STR(STAGE_POSITION_Z, "stage_position_z") // will be deprecated as of v3 in favor of COORDINATES_POINTS_FOVS
//DECLARE_STR(STAGE_DISTANCE_Z, "stage_distance_z") // will be deprecated as of v3 in favor of COORDINATES_POINTS_FOVS

//DECLARE_STR(STAGE_POSITION_TEMPLATE_X, "stage_position/%d/x") // will be deprecated as of v3 in favor of COORDINATES_POINTS_FOVS
//DECLARE_STR(STAGE_POSITION_TEMPLATE_Y, "stage_position/%d/y") // will be deprecated as of v3 in favor of COORDINATES_POINTS_FOVS
//DECLARE_STR(STAGE_POSITION_TEMPLATE_Z, "stage_position/%d/z") // will be deprecated as of v3 in favor of COORDINATES_POINTS_FOVS

//DECLARE_STR(CAMERA_SENSOR_X, "camera_sensor_x") // will be deprecated as of v3 in favor of COORDINATES_POSITIONS_SENSOR
//DECLARE_STR(CAMERA_SENSOR_Y, "camera_sensor_y") // will be deprecated as of v3 in favor of COORDINATES_POSITIONS_SENSOR

// coordinate transformations, new in v2.4
DECLARE_STR(COORDINATES_AXIS_ORIGIN, "coordinates/axis_origin")           // top_left center bottom_right ...
DECLARE_STR(COORDINATES_AXIS_ORIENTATION, "coordinates/axis_orientation") // ascending,descending for each dimension
DECLARE_STR(COORDINATES_TRANSFORM, "coordinates/transformation")          // transformation matrix
DECLARE_STR(COORDINATES_UNITS, "coordinates/units")                       // units for given oordinates
DECLARE_STR(COORDINATES_POINTS_TOPLEFT, "coordinates/tie_points/top_left")
DECLARE_STR(COORDINATES_POINTS_CENTER, "coordinates/tie_points/center")
DECLARE_STR(COORDINATES_POINTS_BOTTOMRIGHT, "coordinates/tie_points/bottom_right")

// coordinates for each fov in the image, instead of "stage_position", stored as:
// coordinates/tie_points/fovs/00000: x,y,z
// coordinates/tie_points/fovs/00001: x,y,z
// coordinates/tie_points/fovs/00002: x,y,z
// DECLARE_STR(COORDINATES_POINTS_FOVS, "coordinates/tie_points/fovs") // deprecated in favor of COORDINATES_POSITIONS_FOVS

DECLARE_STR(COORDINATES_POSITIONS_DATETIME, "coordinates/positions/datetime") // datetime for each plane/block in the image
DECLARE_STR(COORDINATES_POSITIONS_ZOOM, "coordinates/positions/zoom") // zoom for each plane/block in the image
//DECLARE_STR(COORDINATES_POSITIONS_Z, "coordinates/positions/z")               // coordinates for each plane/block in the image - use COORDINATES_POSITIONS_STAGE
//DECLARE_STR(COORDINATES_POSITIONS_T, "coordinates/positions/t")               // coordinates for each plane/block in the image - use COORDINATES_POSITIONS_DATETIME
DECLARE_STR(COORDINATES_POSITIONS_STAGE, "coordinates/positions/stage")       // coordinates for each plane/block in the image: x,y,z;x,y,z;x,y,z
DECLARE_STR(COORDINATES_POSITIONS_SENSOR, "coordinates/positions/sensor")     // coordinates for each plane/block in the image: x,y,z;x,y,z;x,y,z
DECLARE_STR(COORDINATES_POSITIONS_FOCUS, "coordinates/positions/focus")         // focal position
DECLARE_STR(COORDINATES_POSITIONS_FOVS, "coordinates/positions/fovs")     // coordinates for each FOV in the image: x,y,z;x,y,z;x,y,z

// Color Profile
DECLARE_STR(ICC_TAGS_PREFIX, "ColorProfile/")
DECLARE_STR(ICC_TAGS_DEFINITION, "ColorProfile/profile")
DECLARE_STR(ICC_TAGS_DEFINITION_EMBEDDED, "embedded_icc")
DECLARE_STR(ICC_TAGS_DEFINITION_SRGB, "sRGB")

DECLARE_STR(ICC_TAGS_COLORSPACE, "ColorProfile/color_space")
DECLARE_STR(ICC_TAGS_COLORSPACE_MULTICHANNEL, "multichannel")
DECLARE_STR(ICC_TAGS_COLORSPACE_RGB, "RGB")
DECLARE_STR(ICC_TAGS_COLORSPACE_XYZ, "XYZ")
DECLARE_STR(ICC_TAGS_COLORSPACE_LAB, "Lab")
DECLARE_STR(ICC_TAGS_COLORSPACE_LUV, "Luv")
DECLARE_STR(ICC_TAGS_COLORSPACE_YCBCR, "YCbCr")
DECLARE_STR(ICC_TAGS_COLORSPACE_YXY, "Yxy")
DECLARE_STR(ICC_TAGS_COLORSPACE_GRAY, "grayscale")
DECLARE_STR(ICC_TAGS_COLORSPACE_HSV, "HSV")
DECLARE_STR(ICC_TAGS_COLORSPACE_HSL, "HLS")
DECLARE_STR(ICC_TAGS_COLORSPACE_CMYK, "CMYK")
DECLARE_STR(ICC_TAGS_COLORSPACE_CMY, "CMY")

// additional image modes
DECLARE_STR(ICC_TAGS_COLORSPACE_MONO, "monochrome")
DECLARE_STR(ICC_TAGS_COLORSPACE_INDEXED, "indexed")
DECLARE_STR(ICC_TAGS_COLORSPACE_RGBA, "RGBA")
DECLARE_STR(ICC_TAGS_COLORSPACE_RGBE, "RGBE")
DECLARE_STR(ICC_TAGS_COLORSPACE_YUV, "YUV")
DECLARE_STR(ICC_TAGS_COLORSPACE_SPECTRAL, "spectral") // this will require some normalization parameters provided along
DECLARE_STR(ICC_TAGS_COLORSPACE_MASK, "mask")
DECLARE_STR(ICC_TAGS_COLORSPACE_HEATMAP, "heatmap")

DECLARE_STR(ICC_TAGS_VERSION, "ColorProfile/version")
DECLARE_STR(ICC_TAGS_DESCRIPTION, "ColorProfile/description")
DECLARE_STR(ICC_TAGS_SIZE, "ColorProfile/size")

// Geo
DECLARE_STR(GEO_TAGS_PREFIX, "Geo/")

// raw tag definitions

DECLARE_STR(RAW_TAGS_OMEXML, "raw/ome_xml")
DECLARE_STR(RAW_TYPES_OMEXML, "string,ome_xml")

DECLARE_STR(RAW_TAGS_GEOTIFF, "raw/geotiff")
DECLARE_STR(RAW_TYPES_GEOTIFF, "binary,geotiff")

DECLARE_STR(RAW_TAGS_ICC, "raw/icc_profile")
DECLARE_STR(RAW_TYPES_ICC, "binary,color_profile")

DECLARE_STR(RAW_TAGS_XMP, "raw/xmp")
DECLARE_STR(RAW_TYPES_XMP, "string,xmp")

DECLARE_STR(RAW_TAGS_IPTC, "raw/iptc")
DECLARE_STR(RAW_TYPES_IPTC, "binary,iptc")

DECLARE_STR(RAW_TAGS_PHOTOSHOP, "raw/photoshop")
DECLARE_STR(RAW_TYPES_PHOTOSHOP, "binary,photoshop")

DECLARE_STR(RAW_TAGS_EXIF, "raw/exif") // TIFF image block with EXIF and GPS IFDs
DECLARE_STR(RAW_TYPES_EXIF, "binary,exif")

DECLARE_STR(RAW_TAGS_PALETTE, "raw/palette")
DECLARE_STR(RAW_TYPES_PALETTE, "binary,palette") // stored as floats in sequence: RGB RGB RGB ...
