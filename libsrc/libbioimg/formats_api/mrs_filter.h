/*******************************************************************************
Multi-Resolution Filter
Author: Dima Fedorov <mailto:dima@viqiai.com>
Copyright (C) 2022 ViQi Inc

History:
10/13/2006 16:00 - First creation

ver : 4
*******************************************************************************/

#include <cmath>
#include <cstdio>
#include <cstring>

#include <vector>
#include <algorithm>
#include <limits>
#include <iostream>
#include <fstream>

#include "xtypes.h"
#include "bim_image.h"

bim::Image reduce( const bim::Image &im ) {
    bim::uint64 w = (bim::uint64)ceil((double)im.width() / 2.0);
    bim::uint64 h = (bim::uint64)ceil((double)im.height() / 2.0);
    return im.resample(w, h, bim::Image::ResizeMethod::szBiCubic);
}

bim::Image expand( const bim::Image &im, const bim::Image &g ) {
    bim::uint64 w = g.width();
    bim::uint64 h = g.height();
    return im.resample(w, h, bim::Image::ResizeMethod::szBiCubic);
}

template <typename T>
bim::Image splice( const std::vector< std::vector<bim::Image> > &l, int level, const bim::Image &map ) {
    bim::Image s = l.at(0).at(level).deepCopy();
    bim::uint64 w = s.width();
    bim::uint64 h = s.height();

    for (unsigned int sample=0; sample<s.samples(); ++sample ) {
        #pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (h>BIM_OMP_FOR2)
        for (int y=0; y<(int)h; ++y) {
            bim::float32 *mp = (bim::float32*) map.scanLine(0, y); 
            T *dst = (T*) s.scanLine(sample, y); 
            for (unsigned int x=0; x<w; ++x) {
                int i1 = floor(mp[x]);
                int i2 = ceil(mp[x]);
                T *l1 = (T*) l.at(i1).at(level).scanLine(sample, y); 
                T *l2 = (T*) l.at(i2).at(level).scanLine(sample, y); 
                if (i1==i2) {
                    dst[x] = l2[x];
                } else {
                    T w1 = (T) i2 - mp[x];
                    T w2 = 1.0 - w1;
                    dst[x] = w1*l1[x] + w2*l2[x];
                }
            }
        }
    }
    return s;
}

void pyramid_gaussian( const bim::Image &im, std::vector<bim::Image> *g, const int &levels ) {
    g->resize(levels);

    /*
    bim::ImagePyramid pyr;
    pyr.setMinImageSize( 2 );
    pyr.createFrom(im.deepCopy());
    for (int i=0; i<levels; ++i) {
        g->at(i) = *pyr.imageAt(i);
    }
    */
    
    g->at(0) = im; //im.deepCopy();
    for (int i=1; i<levels; ++i) {
        g->at(i) = reduce(g->at(i-1));
    }
}

void pyramid_laplacian( std::vector<bim::Image> *g, std::vector<bim::Image> *l, const int &levels ) {
    l->resize(levels);
    l->at(levels - 1) = g->at(levels - 1).convertToDepth(32, bim::Lut::ltTypecast, bim::DataFormat::FMT_FLOAT);
    for (int i=levels-2; i>=0; --i) {
        bim::Image gi1 = g->at(i + 1).convertToDepth(32, bim::Lut::ltTypecast, bim::DataFormat::FMT_FLOAT);
        g->at(i + 1).clear(); // save memory from unused elements
        bim::Image gi = g->at(i).convertToDepth(32, bim::Lut::ltTypecast, bim::DataFormat::FMT_FLOAT);
        if (i==0) g->at(i).clear(); // save memory from unused elements
        l->at(i) = gi - expand(gi1, gi);
    }
}

// multi-resolution image filter that can compress dynamic range by adjusting the low resolution level and remove noise by adjusting the highest resolution level
// levels - indicates how many levels split image into, 0 is fully automatic, >0 exact number of levels, <0 automatic - requested_number_of_levels
// noise - multiplier for high resolution level, 0 - all removed, 1 - all left
// background - multiplier for low resolution level, 0 - all removed, 1 - all left
// base - base intensity, use 0.5 for brighfield images and 0 for fluorescence
// midrange - is a list of levels to remove mid-range aberrations, eg: "4,5"

template <typename T>
bim::Image mr_filter(bim::Image & img, int levels = 0, const float noise = 1.0, const float background = 1.0, const float base = 0.0, const bim::xstring &midrange = "", bool trim=true) {

    if (levels == 0) {
        levels = (int)floor(bim::log2<double>((double)std::max<bim::uint64>(img.width(), img.height())));
    } else if (levels < 0) {
        levels = (int)floor(bim::log2<double>((double)std::max<bim::uint64>(img.width(), img.height()))) + levels;
    }
    if (levels < 2) return img;

    // Compute gaussian and laplacian pyramids
    std::vector<bim::Image> g(levels);
    std::vector<bim::Image> l(levels);
    pyramid_gaussian( img, &g, levels );
    pyramid_laplacian( &g, &l, levels );
    g.clear(); // save memory from unused elements

    // background adjustment and reduction
    bim::Image *t = &l[levels-1];
    if (background < 1.0) {
        t->mul(background);
    }
    if (base > 0) {
        t->add(base);
    }

    // noise reduction
    if (noise < 1.0) {
        t = &l[0];
        t->mul(noise);
    }

    if (midrange.size() > 0) {
        std::vector<int> midl = midrange.splitInt(",");
        for (int i = 0; i < midl.size(); ++i) {
            int ii = midl[i];
            if (ii >= l.size()) continue;
            bim::Image med = l.at(ii).median(3);
            l.at(ii).sub(med);
        }
    }

    // Reverse the pyramid to restore the spliced image
    std::vector<bim::Image> gC(levels);
    gC.at(levels-1) = l.at(levels-1);
    for (int i=levels-2; i>=0; --i) {
        gC.at(i) = l.at(i) + expand (gC.at(i+1), l.at(i));
        l.at(i).clear(); // save memory from unused elements
        gC.at(i+1).clear(); // save memory from unused elements
    }
    l.clear(); // save memory from unused elements

    // Trim and convert to input pixel format
    bim::Image out = gC.at(0);
    gC.clear(); // save memory from unused elements

    if (trim == true)
        out.trim(0, std::numeric_limits<T>::max());

    /*
    bim::DataFormat pf = bim::FMT_UNSIGNED;
    if (std::numeric_limits<T>::is_signed)
        pf = bim::FMT_SIGNED;
    else if (!std::numeric_limits<T>::is_integer)
        pf = bim::FMT_FLOAT;
    out = out.convertToDepth(sizeof(T)*8, bim::Lut::ltTypecast, pf);
    */
    return out;
}

