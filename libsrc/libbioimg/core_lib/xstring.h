/*****************************************************************************
 Extended String Class

 DEFINITION

 Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

 History:
   12/05/2005 23:38 - First creation

 Ver : 7
*****************************************************************************/

#ifndef XSTRING_H
#define XSTRING_H
//#pragma message(">>>>>  xstring: included enhanced string")

#include <cstdarg>
#include <map>
#include <string>
#include <vector>

#include "xtypes.h"

#if defined(BIM_WIN) && !defined(BIM_USE_CODECVT)
#define BIM_USE_CODECVT
#endif

#if defined(__GNUC__) || defined(__clang__)
#define BIM_PRINTF_ATTRIBUTE(string_index, first_to_check) \
  __attribute__((__format__(__printf__, string_index, first_to_check)))
#define BIM_SCANF_ATTRIBUTE(string_index, first_to_check) \
    __attribute__((__format__(__scanf__, string_index, first_to_check)))
#else
#define BIM_PRINTF_ATTRIBUTE(string_index, first_to_check)
#define BIM_SCANF_ATTRIBUTE(string_index, first_to_check)
#endif

namespace bim {

class xstring : public std::string {
public:
    // constructors
    explicit xstring() : std::string() {}
    xstring(const char *cstr) : std::string() { *this += cstr; }
#ifdef BIM_USE_CODECVT
    xstring(const std::wstring &utf16) : std::string() { this->fromUTF16(utf16); }
    xstring(const wchar_t *utf16): std::string() { this->fromUTF16(utf16); }
#endif

    /*
    xstring( const value_type* _Ptr,
             size_type _Count = npos ) : std::string( _Ptr, _Count ) {}
    */
    xstring(const std::string &_Right,
            size_type _Roff = 0,
            size_type _Count = npos) : std::string(_Right, _Roff, _Count) {}

    xstring(size_type _Count,
            value_type _Ch) : std::string(_Count, _Ch) {}

    template<class InputIterator>
    xstring(InputIterator _First,
            InputIterator _Last) : std::string(_First, _Last) {}

    // members
    xstring &sprintf(const char *fmt, ...) BIM_PRINTF_ATTRIBUTE(2, 3);
    xstring &saddf(const char *fmt, ...) BIM_PRINTF_ATTRIBUTE(2, 3);

    xstring &insertAfterLast(const char *p, const xstring &s);

    int sscanf(const char *fmt, ...) const BIM_SCANF_ATTRIBUTE(2, 3);

    xstring rstrip(const xstring &chars) const; // strips trailing chars
    xstring lstrip(const xstring &chars) const; // strips leading chars
    xstring strip(const xstring &chars) const;  // strips leading and trailing chars

    xstring erase_zeros() const; // removes all 0 chars from the string

    xstring removeSpacesLeft() const { return lstrip(" "); }
    xstring removeSpacesRight() const { return rstrip(" "); }
    xstring removeSpacesBoth() const { return strip(" "); }

    bool toBool(bool def = false) const; // converts from either numeric 0 or 1 or from textual 'true' or 'false'
    int toInt(int def = 0) const;
    unsigned int toUInt(unsigned int def = 0) const;
    bim::int64 toInt64(bim::int64 def = 0) const;
    bim::uint64 toUInt64(bim::uint64 def = 0) const;
    int toIntFromHex(int def) const;
    double toDouble(double def = 0.0) const;

    bool convertableToBool() const;
    bool convertableToInt() const;
    bool convertableToDouble() const;

    xstring toLowerCase() const;
    xstring toUpperCase() const;

    bool operator==(const xstring &s) const;
    bool operator==(const char *s) const;
    bool operator!=(const xstring &s) const { return !(*this == s); }
    bool operator!=(const char *s) const { return !(*this == s); }

    bool startsWith(const xstring &s, bool strict = true) const;
    bool endsWith(const xstring &s) const;

    int compare(const xstring &s) const;

    std::vector<xstring> split(const xstring &separator, const bool &ignore_empty = true) const;
    std::vector<int> splitInt(const xstring &separator, const int &def = 0) const;
    std::vector<double> splitDouble(const xstring &separator, const double &def = 0.0) const;

    xstring left(std::string::size_type pos) const;
    xstring left(const xstring &sub) const;
    xstring right(std::string::size_type pos) const;
    xstring right(const xstring &sub) const;
    xstring section(std::string::size_type start, std::string::size_type end) const;
    xstring section(const xstring &start, const xstring &end, std::string::size_type pos = 0) const;

    xstring consume_token(const xstring &separator, std::string::size_type pos = 0);
    xstring trim_after(const xstring &sub) const;

    bool contains(const xstring &str) const;
    bool contains(const char &ch) const;

    using std::string::replace;
    xstring replace(const xstring &what, const xstring &with) const;
    xstring substring(size_type _Off = 0, size_type _Count = npos) const {
        return this->substr(_Off, _Count);
    }

    // replaces variables encased in {} with the values from the map
    xstring processTemplate(const std::map<std::string, std::string> &vars) const;

    // I/O
    bool toFile(const xstring &file_name) const;
    bool fromFile(const xstring &file_name);

    // encodings
    xstring toPercentEncoding(const bim::xstring &skip = "") const;
    void fromPercentEncoding(const bim::xstring &str);

    xstring toUrlEncoding(const bim::xstring &skip = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_.~") const;
    void fromUrlEncoding(const bim::xstring &str);

    xstring toMetaEncoding() const;
    xstring fromMetaEncoding() const;

// the xstring holds utf8 encoded text
#ifdef BIM_USE_CODECVT
    std::wstring toUTF16() const;
    void fromUTF16(const std::wstring &utf16);

//std::string toLatin1() const;
//void fromLatin1(const std::string &str);
#endif

public:
    static xstring xprintf(const char *fmt, ...) BIM_PRINTF_ATTRIBUTE(1, 2);

    static xstring fromInt(const bim::int64 &v);
    static xstring fromUInt(const bim::uint64 &v);
    static xstring fromFloat(const float &v);
    static xstring fromDouble(const double &v);

    static xstring join(const std::vector<xstring> &v, const xstring &separator);
    static xstring join(const std::vector<int> &v, const xstring &separator);
    static xstring join(const std::vector<double> &v, const xstring &separator);
    static xstring join(const std::vector<unsigned char> &v, const xstring &separator);

    static xstring join(const std::vector<bim::Coord2i> &v, const xstring &separator1, const xstring &separator2);
    static xstring join(const std::vector<bim::Coord3i> &v, const xstring &separator1, const xstring &separator2);
    static xstring join(const std::vector<bim::Coord4i> &v, const xstring &separator1, const xstring &separator2);

    static xstring join(const std::vector<bim::Coord2f> &v, const xstring &separator1, const xstring &separator2);
    static xstring join(const std::vector<bim::Coord3f> &v, const xstring &separator1, const xstring &separator2);
    static xstring join(const std::vector<bim::Coord4f> &v, const xstring &separator1, const xstring &separator2);

    static inline int compare(const xstring &s1, const xstring &s2) { return s1.compare(s2); }

#ifdef BIM_USE_CODECVT
    static xstring FromUTF16(const std::wstring &utf16);
#endif

    // convert from Fixed 2 byte wide representation used in some file formats
    static xstring FromFixed16(const char *f16, const size_t size);
    static xstring FromFixed16(const std::vector<unsigned char> &f16);

private:
    xstring &_sprintf(const char *format, va_list ap);
    int _sscanf(const char *format, va_list ap) const;
};

} // namespace bim

#endif // XSTRING_H
