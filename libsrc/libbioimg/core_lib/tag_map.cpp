/*******************************************************************************

  Map for tag/value pairs

  Author: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  History:
  2008-03-18 17:13 - First creation
  2009-07-08 19:24 - INI parsing, setting values, iterative removing
  2015-07-28 15:44 - Extended values to variants and added types

  ver: 4

  *******************************************************************************/

#include <cstring>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

#include "xstring.h"
#include "xtypes.h"

#include "tag_map.h"

using namespace bim;

namespace bim {

namespace VariantLimits {
static const int max_bool_string_sz = 20;
static const int max_int_string_sz = 40;
static const int max_float_string_sz = 40;
} // namespace VariantLimits
} // namespace bim

//******************************************************************************
// Variant
//******************************************************************************

std::vector<char> Variant::as_vector() const {
    return v;
}

const char *Variant::as_binary() const {
    return &this->v[0];
}

xstring Variant::as_string(const std::string &def) const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    if (tt.startsWith("string") && sz > 0) {
        const char *cc = &this->v[0];
        if (cc[sz - 1] == 0) return xstring(cc);
        xstring str(sz + 1, 0);
        memcpy(&str[0], cc, sz);
        return str;
    }

    if (tt.startsWith("uint64", true)) {
        return xstring::fromUInt(this->as_uint64(0));
    }
    if (tt.startsWith("int64", true)) {
        return xstring::fromInt(this->as_int64(0));
    }
    if (tt.startsWith("int", true)) {
        return xstring::xprintf("%d", this->as_int(0));
    }
    if (tt.startsWith("unsigned", true)) {
        return xstring::xprintf("%d", this->as_unsigned(0));
    }
    if (tt.startsWith("float", true)) {
        return xstring::xprintf("%g", this->as_float(0));
    }
    if (tt.startsWith("double", true)) {
        return xstring::xprintf("%g", this->as_double(0));
    }
    if (tt.startsWith("boolean", true)) {
        return this->as_boolean(false) ? "true" : "false";
    }
    if (tt == ("vector,string")) {
        return xstring::join(this->as_vector_string(), ",");
    }
    if (tt == ("vector,double2")) {
        return xstring::join(this->as_vector_double2(), ";", ",");
    }
    if (tt == ("vector,double3")) {
        return xstring::join(this->as_vector_double3(), ";", ",");
    }
    if (tt == ("vector,double4")) {
        return xstring::join(this->as_vector_double4(), ";", ",");
    }
    if (tt == ("vector,double")) {
        return xstring::join(this->as_vector_double(), ",");
    }
    if (tt == ("vector,int2")) {
        return xstring::join(this->as_vector_int2(), ";", ",");
    }
    if (tt == ("vector,int3")) {
        return xstring::join(this->as_vector_int3(), ";", ",");
    }
    if (tt == ("vector,int4")) {
        return xstring::join(this->as_vector_int4(), ";", ",");
    }
    if (tt == ("vector,int")) {
        return xstring::join(this->as_vector_int(), ",");
    } else {
        xstring octets;
        for (const char &c : this->v) {
            octets += xstring::xprintf("%X,", c);
        }
        // remove trailing comma
        if (octets.length() > 0)
            octets.resize(octets.size()-1);
        return octets;
    }

    return def;
}

int Variant::as_int(const int &def) const {
    xstring tt = this->t;
    size_t sz = this->v.size();

    if (tt.startsWith("int") && sz >= sizeof(int)) {
        int *v = (int *)&this->v[0];
        return *v;
    } else if (tt.startsWith("unsigned") && sz >= sizeof(unsigned int)) {
        unsigned int *v = (unsigned int *)&this->v[0];
        return (int)*v;
    } else if (sz < VariantLimits::max_int_string_sz) {
        xstring str = this->as_string("");
        return str.toInt(def);
    }
    return def;
}

unsigned int Variant::as_unsigned(const unsigned int &def) const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    if (tt.startsWith("unsigned") && sz >= sizeof(unsigned int)) {
        unsigned int *v = (unsigned int *)&this->v[0];
        return *v;
    } else if (tt.startsWith("int") && sz >= sizeof(int)) {
        int *v = (int *)&this->v[0];
        return (unsigned int)*v;
    } else if (sz < VariantLimits::max_int_string_sz) {
        xstring str = this->as_string("");
        return str.toInt(def);
    }
    return def;
}

bim::uint64 Variant::as_uint64(const bim::uint64 &def) const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    if (tt.startsWith("uint64") && sz >= sizeof(bim::uint64)) {
        bim::uint64 *v = (bim::uint64 *)&this->v[0];
        return *v;
    } else if (tt.startsWith("int64") && sz >= sizeof(bim::int64)) {
        bim::int64 *v = (bim::int64 *)&this->v[0];
        return (bim::uint64) *v;
    } else if (sz < VariantLimits::max_int_string_sz) {
        xstring str = this->as_string("");
        return str.toUInt64(def);
    }
    return def;
}

bim::int64 Variant::as_int64(const bim::int64 &def) const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    if (tt.startsWith("int64") && sz >= sizeof(bim::int64)) {
        bim::int64 *v = (bim::int64 *)&this->v[0];
        return *v;
    } else if (tt.startsWith("uint64") && sz >= sizeof(bim::uint64)) {
        bim::uint64 *v = (bim::uint64 *)&this->v[0];
        return (bim::int64)*v;
    } else if (sz < VariantLimits::max_int_string_sz) {
        xstring str = this->as_string("");
        return str.toInt64(def);
    }
    return def;
}

double Variant::as_double(const double &def) const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    if (tt.startsWith("double") && sz >= sizeof(double)) {
        double *v = (double *)&this->v[0];
        return *v;
    } else if (tt.startsWith("float") && sz >= sizeof(float)) {
        float *v = (float *)&this->v[0];
        return (double)*v;
    } else if (sz < VariantLimits::max_float_string_sz) {
        xstring str = this->as_string("");
        return str.toDouble(def);
    }
    return def;
}

float Variant::as_float(const float &def) const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    if (tt.startsWith("float") && sz >= sizeof(float)) {
        float *v = (float *)&this->v[0];
        return *v;
    } else if (tt.startsWith("double") && sz >= sizeof(double)) {
        double *v = (double *)&this->v[0];
        return (float)*v;
    } else if (sz < VariantLimits::max_float_string_sz) {
        xstring str = this->as_string("");
        return (float)str.toDouble(def);
    }
    return def;
}

bool Variant::as_boolean(const bool &def) const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    if (tt.startsWith("boolean") && sz >= sizeof(bool)) {
        bool *v = (bool *)&this->v[0];
        return *v;
    } else if (sz < VariantLimits::max_bool_string_sz) {
        xstring str = this->as_string("");
        return str.toLowerCase() == "true";
    }
    return def;
}

std::vector<bim::xstring> Variant::as_vector_string() const {
    xstring tt = this->t;
    std::vector<bim::xstring> def;

    if (!tt.startsWith("vector,string")) return def;
    int pos = 0;
    for (const unsigned &sz : this->sub_sizes) {
        bim::xstring s(sz, 0);
        memcpy(&s[0], &this->v[pos], sz);
        pos += sz;
        pos += 1;
        def.push_back(s);
    }
    return def;
}

std::vector<int> Variant::as_vector_int() const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    std::vector<int> def;

    if (tt == "vector,int") {
        size_t count = sz / sizeof(int);
        def.resize(count, 0);
        memcpy(&def[0], &this->v[0], sz);
    }

    return def;
}

std::vector<bim::Coord2i> Variant::as_vector_int2() const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    std::vector<bim::Coord2i> def;

    if (tt == "vector,int2") {
        size_t count = sz / sizeof(bim::Coord2i);
        def.resize(count);
        memcpy(&def[0], &this->v[0], sz);
    }

    return def;
}

std::vector<bim::Coord3i> Variant::as_vector_int3() const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    std::vector<bim::Coord3i> def;

    if (tt == "vector,int3") {
        size_t count = sz / sizeof(bim::Coord3i);
        def.resize(count);
        memcpy(&def[0], &this->v[0], sz);
    }

    return def;
}

std::vector<bim::Coord4i> Variant::as_vector_int4() const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    std::vector<bim::Coord4i> def;

    if (tt == "vector,int4") {
        size_t count = sz / sizeof(bim::Coord4i);
        def.resize(count);
        memcpy(&def[0], &this->v[0], sz);
    }

    return def;
}

std::vector<double> Variant::as_vector_double() const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    std::vector<double> def;

    if (tt == "vector,double") {
        size_t count = sz / sizeof(double);
        def.resize(count, 0);
        memcpy(&def[0], &this->v[0], sz);
    }

    return def;
}

std::vector<bim::Coord2f> Variant::as_vector_double2() const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    std::vector<bim::Coord2f> def;

    if (tt == "vector,double2") {
        size_t count = sz / sizeof(bim::Coord2f);
        def.resize(count);
        memcpy(&def[0], &this->v[0], sz);
    }

    return def;
}

std::vector<bim::Coord3f> Variant::as_vector_double3() const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    std::vector<bim::Coord3f> def;

    if (tt == "vector,double3") {
        size_t count = sz / sizeof(bim::Coord3f);
        def.resize(count);
        memcpy(&def[0], &this->v[0], sz);
    }

    return def;
}

std::vector<bim::Coord4f> Variant::as_vector_double4() const {
    xstring tt = this->t;
    size_t sz = this->v.size();
    std::vector<bim::Coord4f> def;

    if (tt == "vector,double4") {
        size_t count = sz / sizeof(bim::Coord4f);
        def.resize(count);
        memcpy(&def[0], &this->v[0], sz);
    }

    return def;
}

TagType Variant::type() const {
    return this->t;
}

unsigned int Variant::size() const {
    return (unsigned int)this->v.size();
}

void Variant::set(const TagContainer &value, const TagType &type) {
    this->v = value;
    this->t = type;
}

template<typename T>
void Variant::set_value(const T& value, const TagType& type) {
    this->t = type;
    this->v.resize(sizeof(value));
    memcpy(&this->v[0], &value, sizeof(value));
}

template<typename T>
void Variant::set_vector(const std::vector<T> &value, const TagType &type) {
    size_t sz = value.size() * sizeof(T);
    this->t = type;
    this->v.resize(sz);
    memcpy(&this->v[0], &value[0], sz);
}

void Variant::set(const char *value, unsigned int size, const TagType &type) {
    if (!value || size < 1) return;
    this->v.resize(size);
    this->t = type;
    memcpy(&this->v[0], value, size);
}

void Variant::set(const std::string &value, const TagType &type) {
    this->t = type;
    if (value.size() > 0) {
        this->v.resize(value.size() + 1);
        this->v[value.size()] = 0;
        memcpy(&this->v[0], value.c_str(), value.size());
    }
}

void Variant::set(const int &value, const TagType &type) {
    this->set_value(value, type);
}

void Variant::set(const unsigned int &value, const TagType &type) {
    this->set_value(value, type);
}

void Variant::set(const bim::int64& value, const TagType& type) {
    this->set_value(value, type);
}

void Variant::set(const bim::uint64& value, const TagType& type) {
    this->set_value(value, type);
}

void Variant::set(const double &value, const TagType &type) {
    this->set_value(value, type);
}

void Variant::set(const float &value, const TagType &type) {
    this->set_value(value, type);
}

void Variant::set(const bool &value, const TagType &type) {
    this->set_value(value, type);
}

void Variant::set(const std::vector<int> &value, const TagType &type) {
    this->set_vector(value, type);
}

void Variant::set(const std::vector<double> &value, const TagType &type) {
    this->set_vector(value, type);
}

void Variant::set(const std::vector<bim::xstring> &value, const TagType &type) {
    this->sub_sizes.resize(0);
    this->t = type;
    size_t sz = 0;
    for (const bim::xstring &item : value) {
        this->sub_sizes.push_back((unsigned int)item.size());
        sz += item.size();
        sz += 1;
    }
    this->v.resize(sz, 0);
    size_t pos = 0;
    for (const bim::xstring &item : value) {
        memcpy(&this->v[pos], &item[0], item.size());
        pos += item.size();
        pos += 1;
    }
}

void Variant::set(const std::vector<bim::Coord2i> &value, const TagType &type) {
    this->set_vector(value, type);
}

void Variant::set(const std::vector<bim::Coord3i> &value, const TagType &type) {
    this->set_vector(value, type);
}

void Variant::set(const std::vector<bim::Coord4i> &value, const TagType &type) {
    this->set_vector(value, type);
}

void Variant::set(const std::vector<bim::Coord2f> &value, const TagType &type) {
    this->set_vector(value, type);
}

void Variant::set(const std::vector<bim::Coord3f> &value, const TagType &type) {
    this->set_vector(value, type);
}

void Variant::set(const std::vector<bim::Coord4f> &value, const TagType &type) {
    this->set_vector(value, type);
}

//******************************************************************************
// TagMap
//******************************************************************************

bool TagMap::keyExists(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    return (it != this->end());
}

//******************************************************************************

void TagMap::append_tag(const std::string &key, const Variant &value) {
    this->insert(make_pair(key, value));
}

void TagMap::append_tag(const std::string &key, const char *value, unsigned int size, const TagType &type) {
    this->insert(make_pair(key, Variant(value, size, type)));
}

void TagMap::append_tag(const std::string &key, const std::vector<char> &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const char *value, const TagType &type) {
    xstring s = value;
    this->insert(make_pair(key, Variant(s, type)));
}

void TagMap::append_tag(const std::string &key, const std::string &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const int &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const unsigned int &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const bim::int64 &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const bim::uint64 &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const double &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const float &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const bool &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const std::vector<int> &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const std::vector<double> &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const std::vector<bim::Coord2i> &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string &key, const std::vector<bim::Coord3i> &value, const TagType &type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string& key, const std::vector<bim::Coord4i>& value, const TagType& type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string& key, const std::vector<bim::Coord2f>& value, const TagType& type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string& key, const std::vector<bim::Coord3f>& value, const TagType& type) {
    this->insert(make_pair(key, Variant(value, type)));
}

void TagMap::append_tag(const std::string& key, const std::vector<bim::Coord4f>& value, const TagType& type) {
    this->insert(make_pair(key, Variant(value, type)));
}


//******************************************************************************

void TagMap::set_value(const std::string& key, const Variant& value) {
    (*this)[key] = value;
}

void TagMap::set_value(const std::string& key, const std::vector<char>& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const char* value, unsigned int size, const TagType& type) {
    (*this)[key] = Variant(value, size, type);
}

void TagMap::set_value(const std::string& key, const char* value, const TagType& type) {
    xstring s = value;
    (*this)[key] = Variant(s, type);
}

void TagMap::set_value(const std::string& key, const std::string& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const int& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const unsigned int& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const bim::int64& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const bim::uint64& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const double& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const float& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const bool& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const std::vector<bim::xstring>& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const std::vector<int>& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const std::vector<double>& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const std::vector<bim::Coord2i>& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const std::vector<bim::Coord3i>& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const std::vector<bim::Coord4i>& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const std::vector<bim::Coord2f>& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const std::vector<bim::Coord3f>& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value(const std::string& key, const std::vector<bim::Coord4f>& value, const TagType& type) {
    (*this)[key] = Variant(value, type);
}

void TagMap::set_value_guess_type(const std::string& key, const bim::xstring& value) {
    if (value.convertableToBool()) {
        this->set_value(key, value.toBool());
    } else if (value.convertableToInt()) {
        this->set_value(key, value.toInt());
    } else if (value.convertableToDouble()) {
        this->set_value(key, value.toDouble());
    }
    this->set_value(key, value);
}

//******************************************************************************

TagType TagMap::get_type(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).type();
    return TagType();
}

unsigned int TagMap::get_size(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).size();
    return 0;
}

std::vector<char> TagMap::get_value_vec(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_vector();
    return std::vector<char>();
}

const char *TagMap::get_value_bin(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_binary();
    return 0;
}

xstring TagMap::get_value(const std::string &key, const std::string &def) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_string();
    return def;
}

int TagMap::get_value_int(const std::string &key, const int &def) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_int(def);
    return def;
}

unsigned int TagMap::get_value_unsigned(const std::string &key, const unsigned int &def) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_unsigned(def);
    return def;
}

bim::int64 TagMap::get_value_int64(const std::string& key, const bim::int64& def) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_int64(def);
    return def;
}

bim::uint64 TagMap::get_value_uint64(const std::string& key, const bim::uint64& def) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_uint64(def);
    return def;
}

double TagMap::get_value_double(const std::string &key, const double &def) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_double(def);
    return def;
}

float TagMap::get_value_float(const std::string &key, const float &def) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_float(def);
    return def;
}

bool TagMap::get_value_bool(const std::string &key, const bool &def) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_boolean(def);
    return def;
}

std::vector<bim::xstring> TagMap::get_value_vector_string(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_vector_string();
    return std::vector<bim::xstring>();
}

std::vector<int> TagMap::get_value_vector_int(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_vector_int();
    return std::vector<int>();
}

std::vector<double> TagMap::get_value_vector_double(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_vector_double();
    return std::vector<double>();
}

std::vector<bim::Coord2i> TagMap::get_value_vector_int2(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_vector_int2();
    return std::vector<bim::Coord2i>();
}

std::vector<bim::Coord3i> TagMap::get_value_vector_int3(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_vector_int3();
    return std::vector<bim::Coord3i>();
}

std::vector<bim::Coord4i> TagMap::get_value_vector_int4(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_vector_int4();
    return std::vector<bim::Coord4i>();
}

std::vector<bim::Coord2f> TagMap::get_value_vector_double2(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_vector_double2();
    return std::vector<bim::Coord2f>();
}

std::vector<bim::Coord3f> TagMap::get_value_vector_double3(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_vector_double3();
    return std::vector<bim::Coord3f>();
}

std::vector<bim::Coord4f> TagMap::get_value_vector_double4(const std::string &key) const {
    TagMap::const_iterator it = this->find(key);
    if (it != this->end())
        return ((*it).second).as_vector_double4();
    return std::vector<bim::Coord4f>();
}

//******************************************************************************

//std::string get_key(const std::vector<char> &val) const;

std::string TagMap::get_key(const std::string &val) const {
    TagMap::const_iterator it = this->begin();
    while (it != this->end()) {
        xstring tt = it->second.type();
        if (tt.startsWith("string") && this->get_value(it->first) == xstring(val)) return it->first;
        ++it;
    }
    return std::string();
}

std::string TagMap::get_key(const int &value) const {
    return get_key(xstring::xprintf("%d", value));
}

std::string TagMap::get_key(const unsigned int &value) const {
    return get_key(xstring::xprintf("%u", value));
}

std::string TagMap::get_key(const double &value) const {
    return get_key(xstring::xprintf("%f", value));
}

std::string TagMap::get_key(const float &value) const {
    return get_key(xstring::xprintf("%f", value));
}

//******************************************************************************
std::string TagMap::get_key_where_value_startsWith(const std::string &val) const {
    TagMap::const_iterator it = this->begin();
    while (it != this->end()) {
        xstring tt = it->second.type();
        if (tt.startsWith("string")) {
            xstring s = this->get_value(it->first);
            if (s.startsWith(val)) return it->first;
        }
        ++it;
    }
    return std::string();
}

std::string TagMap::get_key_where_value_endsWith(const std::string &val) const {
    TagMap::const_iterator it = this->begin();
    while (it != this->end()) {
        xstring tt = it->second.type();
        if (tt.endsWith("string")) {
            xstring s = this->get_value(it->first);
            if (s.startsWith(val)) return it->first;
        }
        ++it;
    }
    return std::string();
}

void TagMap::rename_key_startsWith(const std::string &old_suffix, const std::string &new_suffix) {
    if (old_suffix == new_suffix) return;
    std::deque<bim::xstring> found;
    TagMap::const_iterator it = this->begin();
    while (it != this->end()) {
        xstring key = it->first;
        if (key.startsWith(old_suffix)) {
            found.push_back(key);
            (*this)[key.replace(old_suffix, new_suffix)] = it->second;
        }
        ++it;
    }

    for (const bim::xstring &item : found) {
        this->delete_tag(item);
    }
}

void TagMap::rename_key(const std::string &old_key, const std::string &new_key) {
    if (old_key == new_key) return;
    TagMap::iterator it = this->find(old_key);
    if (it == this->end()) return;
    (*this)[new_key] = it->second;
    this->delete_tag(old_key);
}

//******************************************************************************

void TagMap::set_values(const std::map<std::string, TagValue> &tags, const std::string &prefix) {
    TagMap::const_iterator it = tags.begin();
    while (it != tags.end()) {
        set_value(prefix + it->first, it->second);
        it++;
    }
}

void TagMap::append_tags(const std::map<std::string, TagValue> &tags, const std::string &prefix) {
    TagMap::const_iterator it = tags.begin();
    while (it != tags.end()) {
        append_tag(prefix + it->first, it->second);
        it++;
    }
}

//******************************************************************************

std::string TagMap::readline(const std::string &str, size_t &pos) {
    std::string line;
    std::string::const_iterator it = str.begin() + pos;
    while (it < str.end() && *it != 0xA) {
        if (*it != 0xD)
            line += *it;
        else
            ++pos;
        ++it;
    }
    pos += line.size();
    if (it < str.end() && *it == 0xA) ++pos;
    return line;
}

void TagMap::parse_ini(const std::string &ini, const std::string &separator, const std::string &prefix, const std::string &stop_at_dir) {
    std::string dir;
    size_t pos = 0;
    while (pos < ini.size()) {
        std::string line = TagMap::readline(ini, pos);
        if (line.size() <= 0) continue;

        // if directory
        if (*line.begin() == '[' && line.size() >= 1 && *(line.end() - 1) == ']') {
            dir = line.substr(1, line.size() - 2);
            if (stop_at_dir.size() > 0 && dir == stop_at_dir) return;
            continue;
        }

        // if tag-value pair
        size_t eq_pos = line.find(separator);
        if (eq_pos != std::string::npos) { // found '='
            std::string key = line.substr(0, eq_pos);
            if (dir != "") key = dir + "/" + line.substr(0, eq_pos);
            std::string val = "";
            if (line.size() > eq_pos + 1) val = line.substr(eq_pos + 1);
            if (val.size() > 0 && *val.begin() == '"' && *(val.end() - 1) == '"') val = val.substr(1, val.size() - 2);
            this->set_value(prefix + key, val);
        }

    } // while
}

std::deque<std::string> TagMap::iniGetBlock(const std::string &ini, const std::string &dir_name) {
    std::deque<std::string> d;
    size_t pos = 0;
    bool found = false;
    while (pos < ini.size()) {
        std::string line = TagMap::readline(ini, pos);
        if (line.size() <= 0) continue;

        if (*line.begin() == '[' && line.size() >= 1 && *(line.end() - 1) == ']') { // if directory
            found = (dir_name == line.substr(1, line.size() - 2));
            continue;
        }

        if (found) d.push_back(line);
    } // while

    return d;
}

//******************************************************************************

void TagMap::delete_tag(const std::string &key) {
    TagMap::iterator it = this->find(key);
    if (it != this->end()) {
        this->erase(it);
    }
}

void TagMap::eraseKeysStartingWith(const std::string &str) {
    TagMap::iterator it = this->begin();
    while (it != this->end()) {
        xstring s = it->first;
        TagMap::iterator toerase = it;
        ++it;
        if (s.startsWith(str))
            this->erase(toerase);
    }
}

//******************************************************************************

xstring TagMap::pop_tag(const std::string &key, const std::string &def) {
    xstring v = this->get_value(key, def);
    this->delete_tag(key);
    return v;
}

std::vector<char> TagMap::pop_tag_vec(const std::string &key) {
    std::vector<char> v = this->get_value_vec(key);
    this->delete_tag(key);
    return v;
}

const char *TagMap::pop_tag_bin(const std::string &key) {
    const char *v = this->get_value_bin(key);
    this->delete_tag(key);
    return v;
}

int TagMap::pop_tag_int(const std::string &key, const int &def) {
    int v = this->get_value_int(key, def);
    this->delete_tag(key);
    return v;
}

unsigned int TagMap::pop_tag_unsigned(const std::string &key, const unsigned int &def) {
    unsigned int v = this->get_value_unsigned(key, def);
    this->delete_tag(key);
    return v;
}

bim::int64 TagMap::pop_tag_int64(const std::string &key, const bim::int64 &def) {
    bim::int64 v = this->get_value_int64(key, def);
    this->delete_tag(key);
    return v;
}

bim::uint64 TagMap::pop_tag_uint64(const std::string &key, const bim::uint64 &def) {
    bim::uint64 v = this->get_value_uint64(key, def);
    this->delete_tag(key);
    return v;
}

double TagMap::pop_tag_double(const std::string &key, const double &def) {
    double v = this->get_value_double(key, def);
    this->delete_tag(key);
    return v;
}

float TagMap::pop_tag_float(const std::string &key, const float &def) {
    float v = this->get_value_float(key, def);
    this->delete_tag(key);
    return v;
}

bool TagMap::pop_tag_bool(const std::string &key, const bool &def) {
    bool v = this->get_value_bool(key, def);
    this->delete_tag(key);
    return v;
}

std::vector<int> TagMap::pop_tag_vector_int(const std::string &key) {
    std::vector<int> v = this->get_value_vector_int(key);
    this->delete_tag(key);
    return v;
}

std::vector<double> TagMap::pop_tag_vector_double(const std::string &key) {
    std::vector<double> v = this->get_value_vector_double(key);
    this->delete_tag(key);
    return v;
}

std::vector<bim::Coord2i> TagMap::pop_tag_vector_int2(const std::string &key) {
    std::vector<bim::Coord2i> v = this->get_value_vector_int2(key);
    this->delete_tag(key);
    return v;
}

std::vector<bim::Coord3i> TagMap::pop_tag_vector_int3(const std::string &key) {
    std::vector<bim::Coord3i> v = this->get_value_vector_int3(key);
    this->delete_tag(key);
    return v;
}

std::vector<bim::Coord4i> TagMap::pop_tag_vector_int4(const std::string &key) {
    std::vector<bim::Coord4i> v = this->get_value_vector_int4(key);
    this->delete_tag(key);
    return v;
}

std::vector<bim::Coord2f> TagMap::pop_tag_vector_double2(const std::string &key) {
    std::vector<bim::Coord2f> v = this->get_value_vector_double2(key);
    this->delete_tag(key);
    return v;
}

std::vector<bim::Coord3f> TagMap::pop_tag_vector_double3(const std::string &key) {
    std::vector<bim::Coord3f> v = this->get_value_vector_double3(key);
    this->delete_tag(key);
    return v;
}

std::vector<bim::Coord4f> TagMap::pop_tag_vector_double4(const std::string &key) {
    std::vector<bim::Coord4f> v = this->get_value_vector_double4(key);
    this->delete_tag(key);
    return v;
}


//******************************************************************************
std::string TagMap::join(const std::string &line_sep, const std::string &tag_val_sep) const {
    std::string s;
    TagMap::const_iterator it = this->begin();
    while (it != this->end()) {
        s += xstring(it->first).toMetaEncoding();
        s += tag_val_sep;
        s += this->get_value(it->first).toMetaEncoding();
        s += line_sep;
        ++it;
    }
    return s;
}


//******************************************************************************
// I/O
//******************************************************************************

bool TagMap::toFile(const std::string &file_name, const std::string &sep) const {
    std::ofstream f;
    f.open(file_name.c_str());
    if (!f.is_open()) return false;
    f << this->join("\n");
    f.close();
    return true;
}

bool TagMap::fromFile(const std::string &file_name, const std::string &sep) {
    std::ifstream f(file_name.c_str());
    if (!f.is_open()) return false;
    while (!f.eof()) {
        std::string line;
        std::getline(f, line);
        this->parse_ini(line, sep);
    }
    f.close();
    return true;
}

bool TagMap::fromString(const std::string &str, const std::string &seplines, const std::string &sepkey) {
    xstring s = str;
    for (xstring line : s.split(seplines)) {
        std::vector<xstring> e = line.split(sepkey);
        if (e.size() > 1) {
            this->set_value_guess_type(e[0].fromMetaEncoding(), e[1].fromMetaEncoding());
        } else if (e.size() > 0) {
            this->set_value(e[0].fromMetaEncoding(), "");
        }
    }
    return true;
}

void TagMap::set_value_from_old_key(const std::string &old_key, const std::string &new_key) {
    TagMap::iterator it = this->find(old_key);
    if (it != this->end()) {
        set_value(new_key, it->second);
    }
}

void TagMap::set_value_from_external(const std::string& ext_key, const TagMap& hash, const std::string& new_key) {
    TagMap::const_iterator it = hash.find(ext_key);
    if (it != hash.end()) {
        set_value(new_key, it->second);
    }
}

bim::TagMap bim::filterByPrefix(const bim::TagMap* hash, const std::string& prefix) {
    bim::TagMap filtered;

    if (hash) {
        TagMap::const_iterator it = hash->begin();
        while (it != hash->end()) {
            const std::string& key = it->first;
            if (key.length() > prefix.length() && key.substr(0, prefix.length()) == prefix)
                filtered.append_tag(key.substr(prefix.length()), hash->get_value(key));
            ++it;
        }
    }

    return filtered;
}
