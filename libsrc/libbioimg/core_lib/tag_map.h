/*******************************************************************************

  Map for tag/value pairs

  Author: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  Description:

  Mapping between tag names and their values
  Names: are any string with possible hierarchy encoded with the "/" slash character
  Values: are variant type typically storing strings, but possibly vectors, numbers, etc...

  types: indicate basic storage of the element and may be followed by additional
  user-given type info, multiple types are comma separated
  basic types: binary|string|int|unsigned|double|float|boolean
  example of a user defined type:
      "string,dicom:name:8082"
      "double,microns"

  History:
  2008-03-18 17:13 - First creation
  2009-07-08 19:24 - INI parsing, setting values, iterative removing
  2015-07-28 15:44 - Extended values to variants and added types

  ver: 4

  *******************************************************************************/

#ifndef BIM_TAG_MAP_H
#define BIM_TAG_MAP_H

#include <deque>
#include <map>
#include <string>

#include "xstring.h"
#include "xtypes.h"

namespace bim {

typedef std::vector<char> TagContainer;
typedef std::string TagType;

class Variant {
public:
    Variant() = default;
    Variant(const TagContainer &value, const TagType &_type = "binary") { this->set(value, _type); }
    Variant(const char *value, unsigned int size, const TagType &_type = "binary") { this->set(value, size, _type); }
    Variant(const std::string &value, const TagType &_type = "string") { this->set(value, _type); }
    Variant(const int &value, const TagType &_type = "int") { this->set(value, _type); }
    Variant(const unsigned int &value, const TagType &_type = "unsigned") { this->set(value, _type); }

    Variant(const bim::int64 &value, const TagType &_type = "int64") { this->set(value, _type); }
    Variant(const bim::uint64 &value, const TagType &_type = "uint64") { this->set(value, _type); }

    Variant(const double &value, const TagType &_type = "double") { this->set(value, _type); }
    Variant(const float &value, const TagType &_type = "float") { this->set(value, _type); }
    Variant(const bool &value, const TagType &_type = "boolean") { this->set(value, _type); }
    Variant(const std::vector<int> &value, const TagType &_type = "vector,int") { this->set(value, _type); }
    Variant(const std::vector<double> &value, const TagType &_type = "vector,double") { this->set(value, _type); }
    Variant(const std::vector<bim::xstring> &value, const TagType &_type = "vector,string") { this->set(value, _type); }

    Variant(const std::vector<bim::Coord2i> &value, const TagType &_type = "vector,int2") { this->set(value, _type); }
    Variant(const std::vector<bim::Coord3i> &value, const TagType &_type = "vector,int3") { this->set(value, _type); }
    Variant(const std::vector<bim::Coord4i> &value, const TagType &_type = "vector,int4") { this->set(value, _type); }

    Variant(const std::vector<bim::Coord2f> &value, const TagType &_type = "vector,double2") { this->set(value, _type); }
    Variant(const std::vector<bim::Coord3f> &value, const TagType &_type = "vector,double3") { this->set(value, _type); }
    Variant(const std::vector<bim::Coord4f> &value, const TagType &_type = "vector,double4") { this->set(value, _type); }

    TagContainer as_vector() const;
    const char *as_binary() const;
    xstring as_string(const std::string &def = "") const;
    int as_int(const int &def) const;
    unsigned int as_unsigned(const unsigned int &def) const;

    bim::int64 as_int64(const bim::int64 &def) const;
    bim::uint64 as_uint64(const bim::uint64 &def) const;

    double as_double(const double &def) const;
    float as_float(const float &def) const;
    bool as_boolean(const bool &def) const;
    std::vector<int> as_vector_int() const;
    std::vector<double> as_vector_double() const;
    std::vector<bim::xstring> as_vector_string() const;

    std::vector<bim::Coord2i> as_vector_int2() const;
    std::vector<bim::Coord3i> as_vector_int3() const;
    std::vector<bim::Coord4i> as_vector_int4() const;

    std::vector<bim::Coord2f> as_vector_double2() const;
    std::vector<bim::Coord3f> as_vector_double3() const;
    std::vector<bim::Coord4f> as_vector_double4() const;

    TagType type() const;
    unsigned int size() const;

    void set(const TagContainer &value, const TagType &type = "binary");
    void set(const char *value, unsigned int size, const TagType &type = "binary");
    void set(const std::string &value, const TagType &type = "string");
    void set(const int &value, const TagType &type = "int");
    void set(const unsigned int &value, const TagType &type = "unsigned");
    void set(const double &value, const TagType &type = "double");
    void set(const float &value, const TagType &type = "float");
    void set(const bool &value, const TagType &type = "boolean");
    void set(const bim::int64 &value, const TagType &type = "int64");
    void set(const bim::uint64 &value, const TagType &type = "uint64");

    void set(const std::vector<int> &value, const TagType &type = "vector,int");
    void set(const std::vector<double> &value, const TagType &type = "vector,double");
    void set(const std::vector<bim::xstring> &value, const TagType &type = "vector,string");

    void set(const std::vector<bim::Coord2i> &value, const TagType &type = "vector,int2");
    void set(const std::vector<bim::Coord3i> &value, const TagType &type = "vector,int3");
    void set(const std::vector<bim::Coord4i> &value, const TagType &type = "vector,int4");

    void set(const std::vector<bim::Coord2f> &value, const TagType &type = "vector,double2");
    void set(const std::vector<bim::Coord3f> &value, const TagType &type = "vector,double3");
    void set(const std::vector<bim::Coord4f> &value, const TagType &type = "vector,double4");

private:
    TagContainer v;
    TagType t;
    std::vector<unsigned int> sub_sizes;

    template<typename T>
    void set_value(const T &value, const TagType &type);

    template<typename T>
    void set_vector(const std::vector<T> &value, const TagType &type);
};


typedef Variant TagValue;
class TagMap : public std::map<std::string, TagValue> {
public:
    // constructors
    TagMap() = default;
    TagMap(const std::string &str, const std::string &seplines = ",", const std::string &sepkey = " ") { this->fromString(str, seplines, sepkey); }

    bool keyExists(const std::string &key) const;
    bool hasKey(const std::string &key) const { return keyExists(key); }


    // additional methods
    xstring get_value(const std::string &key, const std::string &def = "") const;
    std::vector<char> get_value_vec(const std::string &key) const;
    const char *get_value_bin(const std::string &key) const;
    int get_value_int(const std::string &key, const int &def) const;
    unsigned int get_value_unsigned(const std::string &key, const unsigned int &def) const;
    bim::int64 get_value_int64(const std::string &key, const bim::int64 &def) const;
    bim::uint64 get_value_uint64(const std::string &key, const bim::uint64 &def) const;
    double get_value_double(const std::string &key, const double &def) const;
    float get_value_float(const std::string &key, const float &def) const;
    bool get_value_bool(const std::string &key, const bool &def) const;
    std::vector<int> get_value_vector_int(const std::string &key) const;
    std::vector<double> get_value_vector_double(const std::string &key) const;
    std::vector<bim::xstring> get_value_vector_string(const std::string &key) const;

    std::vector<bim::Coord2i> get_value_vector_int2(const std::string &key) const;
    std::vector<bim::Coord3i> get_value_vector_int3(const std::string &key) const;
    std::vector<bim::Coord4i> get_value_vector_int4(const std::string &key) const;

    std::vector<bim::Coord2f> get_value_vector_double2(const std::string &key) const;
    std::vector<bim::Coord3f> get_value_vector_double3(const std::string &key) const;
    std::vector<bim::Coord4f> get_value_vector_double4(const std::string &key) const;

    TagType get_type(const std::string &key) const;
    unsigned int get_size(const std::string &key) const;

    //std::string get_key(const std::vector<char> &val) const;
    std::string get_key(const std::string &val) const;
    std::string get_key(const int &value) const;
    std::string get_key(const unsigned int &value) const;
    std::string get_key(const double &value) const;
    std::string get_key(const float &value) const;

    std::string get_key_where_value_startsWith(const std::string &val) const;
    std::string get_key_where_value_endsWith(const std::string &val) const;

    void rename_key(const std::string &old_key, const std::string &new_key);
    void rename_key_startsWith(const std::string &old_suffix, const std::string &new_suffix);

    // Sets the value of a tag, if the key exists its value is modified
    void set_value_guess_type(const std::string &key, const bim::xstring &value);

    void set_value(const std::string &key, const Variant &value);
    void set_value(const std::string &key, const std::vector<char> &value, const TagType &type = "binary");
    void set_value(const std::string &key, const char *value, unsigned int size, const TagType &type = "binary");
    void set_value(const std::string &key, const char *value, const TagType &type = "string");
    void set_value(const std::string &key, const std::string &value, const TagType &type = "string");
    void set_value(const std::string &key, const int &value, const TagType &type = "int");
    void set_value(const std::string &key, const unsigned int &value, const TagType &type = "unsigned");
    void set_value(const std::string &key, const bim::int64 &value, const TagType &type = "int64");
    void set_value(const std::string &key, const bim::uint64 &value, const TagType &type = "uint64");
    void set_value(const std::string &key, const double &value, const TagType &type = "double");
    void set_value(const std::string &key, const float &value, const TagType &type = "float");
    void set_value(const std::string &key, const bool &value, const TagType &type = "boolean");
    void set_value(const std::string &key, const std::vector<int> &value, const TagType &type = "vector,int");
    void set_value(const std::string &key, const std::vector<double> &value, const TagType &type = "vector,double");
    void set_value(const std::string &key, const std::vector<bim::xstring> &value, const TagType &type = "vector,string");

    void set_value(const std::string &key, const std::vector<bim::Coord2i> &value, const TagType &type = "vector,int2");
    void set_value(const std::string &key, const std::vector<bim::Coord3i> &value, const TagType &type = "vector,int3");
    void set_value(const std::string &key, const std::vector<bim::Coord4i> &value, const TagType &type = "vector,int4");

    void set_value(const std::string &key, const std::vector<bim::Coord2f> &value, const TagType &type = "vector,double2");
    void set_value(const std::string &key, const std::vector<bim::Coord3f> &value, const TagType &type = "vector,double3");
    void set_value(const std::string &key, const std::vector<bim::Coord4f> &value, const TagType &type = "vector,double4");

    // Appends a tag if the key does not exist yet
    void append_tag(const std::string &key, const Variant &value);
    void append_tag(const std::string &key, const std::vector<char> &value, const TagType &type = "binary");
    void append_tag(const std::string &key, const char *value, unsigned int size, const TagType &type = "binary");
    void append_tag(const std::string &key, const char *value, const TagType &type = "string");
    void append_tag(const std::string &key, const std::string &value, const TagType &type = "string");
    void append_tag(const std::string &key, const int &value, const TagType &type = "int");
    void append_tag(const std::string &key, const unsigned int &value, const TagType &type = "unsigned");
    void append_tag(const std::string &key, const bim::int64 &value, const TagType &type = "int64");
    void append_tag(const std::string &key, const bim::uint64 &value, const TagType &type = "uint64");
    void append_tag(const std::string &key, const double &value, const TagType &type = "double");
    void append_tag(const std::string &key, const float &value, const TagType &type = "float");
    void append_tag(const std::string &key, const bool &value, const TagType &type = "boolean");
    void append_tag(const std::string &key, const std::vector<int> &value, const TagType &type = "vector,int");
    void append_tag(const std::string &key, const std::vector<double> &value, const TagType &type = "vector,double");
    void append_tag(const std::string &key, const std::vector<bim::xstring> &value, const TagType &type = "vector,string");

    void append_tag(const std::string &key, const std::vector<bim::Coord2i> &value, const TagType &type = "vector,int2");
    void append_tag(const std::string &key, const std::vector<bim::Coord3i> &value, const TagType &type = "vector,int3");
    void append_tag(const std::string &key, const std::vector<bim::Coord4i> &value, const TagType &type = "vector,int4");

    void append_tag(const std::string &key, const std::vector<bim::Coord2f> &value, const TagType &type = "vector,double2");
    void append_tag(const std::string &key, const std::vector<bim::Coord3f> &value, const TagType &type = "vector,double3");
    void append_tag(const std::string &key, const std::vector<bim::Coord4f> &value, const TagType &type = "vector,double4");

    void delete_tag(const std::string &key);
    void erase_tag(const std::string &key) { this->delete_tag(key); };

    xstring pop_tag(const std::string &key, const std::string &def = "");
    std::vector<char> pop_tag_vec(const std::string &key);
    const char *pop_tag_bin(const std::string &key);
    int pop_tag_int(const std::string &key, const int &def);
    unsigned int pop_tag_unsigned(const std::string &key, const unsigned int &def);
    bim::int64 pop_tag_int64(const std::string &key, const bim::int64 &def);
    bim::uint64 pop_tag_uint64(const std::string &key, const bim::uint64 &def);
    double pop_tag_double(const std::string &key, const double &def);
    float pop_tag_float(const std::string &key, const float &def);
    bool pop_tag_bool(const std::string &key, const bool &def);
    std::vector<int> pop_tag_vector_int(const std::string &key);
    std::vector<double> pop_tag_vector_double(const std::string &key);
    std::vector<bim::xstring> pop_tag_vector_string(const std::string &key);

    std::vector<bim::Coord2i> pop_tag_vector_int2(const std::string &key);
    std::vector<bim::Coord3i> pop_tag_vector_int3(const std::string &key);
    std::vector<bim::Coord4i> pop_tag_vector_int4(const std::string &key);

    std::vector<bim::Coord2f> pop_tag_vector_double2(const std::string &key);
    std::vector<bim::Coord3f> pop_tag_vector_double3(const std::string &key);
    std::vector<bim::Coord4f> pop_tag_vector_double4(const std::string &key);

    void eraseKeysStartingWith(const std::string &str);

    // set tags from another hashtable using a prefix for a tag name
    void set_values(const std::map<std::string, TagValue> &tags, const std::string &prefix = "");

    // append tags from another hashtable, using a prefix for a tag name: prefix+name
    void append_tags(const std::map<std::string, TagValue> &tags, const std::string &prefix = "");

    // sets a value form old key to a new name, keeps the old one
    void set_value_from_old_key(const std::string &old_key, const std::string &new_key);
    void set_value_from_external(const std::string &ext_key, const TagMap &hash, const std::string &new_key);

    // parse a string containing an INI formatted variables, using a prefix for a tag name: prefix+name
    void parse_ini(const std::string &ini, const std::string &separator = "=", const std::string &prefix = "", const std::string &stop_at_dir = "");
    static std::deque<std::string> iniGetBlock(const std::string &ini, const std::string &dir_name);

    std::string join(const std::string &line_sep, const std::string &tag_val_sep = ":") const;
    std::string toString() const { 
        return this->join("\n"); 
    }

    // I/O
    bool toFile(const std::string &file_name, const std::string &sep = ": ") const;
    bool fromFile(const std::string &file_name, const std::string &sep = ": ");

    bool fromString(const std::string &str, const std::string &seplines = "\n", const std::string &sepkey = ":");

private:
    static std::string readline(const std::string &str, size_t &pos);
};

TagMap filterByPrefix(const TagMap* hash, const std::string& prefix);

} // namespace bim

#endif // BIM_TAG_MAP_H
