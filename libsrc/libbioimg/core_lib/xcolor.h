/*****************************************************************************
 Color definition and conversions

 Copyright (c) 2019-2020, ViQI Inc

 Author: Dmitry Fedorov <mailto:dima@viqi.org> <http://www.viqi.org/>

 History:
 2019-05-23 11:50:40 - First creation

 Ver : 1
*****************************************************************************/

#ifndef BIM_COLOR
#define BIM_COLOR

#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <ctime>
#include <limits>
#include <stdexcept>
#include <sys/types.h>
#include <vector>

#include "xtypes.h"
#include "xstring.h"

namespace bim {

//------------------------------------------------------------------------------
// color
// Typical color for emission wavelengths:
//     Violet Below 450 nm
//     Blue 450 - 500 nm 
//     Green 500 - 570 nm 
//     Yellow 570 - 590 nm 
//     Orange 590 - 610 nm 
//     Red 610 - 700 nm
//------------------------------------------------------------------------------

template<typename T>
inline bim::uint8 f_2_ui8(T v) {
    return bim::trim<bim::uint8, int>(bim::round<int>(v * 255));
}

template<typename T>
class Color {
public:
    T r = 0;
    T g = 0;
    T b = 0;
    T a = 1.0;

public:
    bool operator==(Color<T> &c) const { return (this->r == c.r && this->g == c.g && this->b == c.b && this->a == c.a); }
    bool operator>=(Color<T> &c) const { return (this->r >= c.r && this->g >= c.g && this->b >= c.b && this->a >= c.a); }
    bool operator<=(Color<T> &c) const { return (this->r <= c.r && this->g <= c.g && this->b <= c.b && this->a <= c.a); }

public:
    Color(T r = 0, T g = 0, T b = 0, T a = 1) {
        this->r = r;
        this->g = g;
        this->b = b;
        this->a = a;
    }

    Color(const bim::uint32 &v) {
        *this = Color::from_RGB(v);
    }

    inline T getRed() const {
        return this->r;
    }

    inline T getGreen() const {
        return this->g;
    }

    inline T getBlue() const {
        return this->b;
    }

    inline T getAlpha() const {
        return this->a;
    }

    inline bool isBlack() const {
        return this->r == 0 && this->g == 0 && this->b == 0;
    }

    inline bool isWhite() const {
        return this->r == 1 && this->g == 1 && this->b == 1;
    }

    // to be deprectated as of v3
    //inline bim::DisplayColor toDisplayColor() const {
    //    return bim::DisplayColor(f_2_ui8(this->r), f_2_ui8(this->g), f_2_ui8(this->b));
    //}

    // R (0..1), G (0..1), B (0..1) [, A (0..1)]
    bim::xstring to_string_float() const {
        return xstring::xprintf("%.2f,%.2f,%.2f", this->r, this->g, this->b);
    }

    // #RRGGBBAA
    bim::xstring to_string_hex() const {
        return xstring::xprintf("#%X%X%X%X", f_2_ui8(this->r), f_2_ui8(this->g), f_2_ui8(this->b), f_2_ui8(this->a));
    }

    // R (0..255), G (0..255), B (0..255)
    bim::xstring to_string_8bit() const {
        return xstring::xprintf("%d,%d,%d", f_2_ui8(this->r), f_2_ui8(this->g), f_2_ui8(this->b));
    }

    // A (0..255), R (0..255), G (0..255), B (0..255)
    bim::uint32 to_ARGB_32bit() const {
        bim::uint32 r = static_cast<bim::uint32>(this->r * 255.0);
        bim::uint32 g = static_cast<bim::uint32>(this->g * 255.0);
        bim::uint32 b = static_cast<bim::uint32>(this->b * 255.0);
        bim::uint32 a = static_cast<bim::uint32>(this->a * 255.0);
        return ((a & 0xff) << 24) | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
    }

    T to_gray() const {
        return 0.3 * this->r + 0.59 * this->g + 0.11 * this->b;
    }

    // Hue component(0..359), Saturation component(0..1), Lightness component(0..1)
    std::vector<T> to_HSL() const {
        T cmax = bim::max<T>(this->r, bim::max<T>(this->g, this->b)); // maximum of r, g, b
        T cmin = bim::min<T>(this->r, bim::min<T>(this->g, this->b)); // minimum of r, g, b
        T delta = cmax - cmin;                                        // diff of cmax and cmin
        T h = 0, s = 0, l = static_cast<T>(0.5 * ((double)cmax + cmin));

        if (cmin != cmax) {
            s = (l < 0.5) ? delta / (cmax + cmin) : delta / (2 - cmax - cmin);
            if (this->r == cmax) {
                h = 60 * (this->g - this->b) / delta;
            } else if (g == cmax) {
                h = 120 + 60 * (this->b - this->r) / delta;
            } else {
                h = 240 + 60 * (this->r - this->g) / delta;
            }
            if (h < 0) {
                h += 360;
            }
            if (h >= 360) {
                h -= 360;
            }
        }

        std::vector<T> hsl = { h, s, l };
        return hsl;
    }

    Color<T> to_contrast() const {
        T Y = this->to_gray();
        if (Y < 70)
            return Color<T>(1., 1., 1., this->a);
        return Color<T>(0., 0., 0., this->a);
    }

public:
    // R (0..255), G (0..255), B (0..255)
    static Color<T> from_RGB(const bim::uint32 &v) {
        Color<T> c;
        bim::uint8 A = static_cast<bim::uint8>((v >> 24U) & 0xffU);
        bim::uint8 B = static_cast<bim::uint8>((v >> 16U) & 0xffU);
        bim::uint8 G = static_cast<bim::uint8>((v >> 8U) & 0xffU);
        bim::uint8 R = static_cast<bim::uint8>((v >> 0) & 0xffU);
        c.r = static_cast<T>(R / 255.0);
        c.g = static_cast<T>(G / 255.0);
        c.b = static_cast<T>(B / 255.0);
        c.a = 1.0;
        return c;
    }

    // R (0..255), G (0..255), B (0..255), A (0..255)
    static Color<T> from_RGBA(const bim::uint32 &v) {
        Color<T> c;
        bim::uint8 A = static_cast<bim::uint8>((v >> 24U) & 0xffU);
        bim::uint8 B = static_cast<bim::uint8>((v >> 16U) & 0xffU);
        bim::uint8 G = static_cast<bim::uint8>((v >> 8U) & 0xffU);
        bim::uint8 R = static_cast<bim::uint8>((v >> 0) & 0xffU);
        c.r = static_cast<T>(R / 255.0);
        c.g = static_cast<T>(G / 255.0);
        c.b = static_cast<T>(B / 255.0);
        c.a = static_cast<T>(A / 255.0);
        return c;
    }

    // R (0..255), G (0..255), B (0..255), A (0..255)
    static Color<T> from_RGBA(const int &R, const int &G, const int &B, const int &A = 255) {
        Color<T> c;
        c.r = static_cast<T>(R / 255.0);
        c.g = static_cast<T>(G / 255.0);
        c.b = static_cast<T>(B / 255.0);
        c.a = static_cast<T>(A / 255.0);
        return c;
    }

    // R (0..1), G (0..1), B (0..1), A (0..1)
    static Color<T> from_fRGBA(const float &R, const float &G, const float &B, const float &A = 1.0) {
        Color<T> c;
        c.r = static_cast<T>(R);
        c.g = static_cast<T>(G);
        c.b = static_cast<T>(B);
        c.a = static_cast<T>(A);
        return c;
    }

    // Hue (0..359), Saturation (0..1), Lightness (0..1)
    static Color<T> from_HSL(const T &h, const T &s, const T &l) {
        if (s == 0 || h == 0) {
            return Color<T>(l, l, l);
        }

        T H = static_cast<T>(h / 60.0);
        T C = s * (1 - abs(2 * l - 1));
        T X = C * (1 - abs(H - 2 * floor(H / 2) - 1));
        T m = l - C / 2;
        T fh = floor(H);
        T R = 0, G = 0, B = 0;
        if (fh == 0) {
            R = C;
            G = X;
            B = 0;
        } else if (fh == 1) {
            R = X;
            G = C;
            B = 0;
        } else if (fh == 2) {
            R = 0;
            G = C;
            B = X;
        } else if (fh == 3) {
            R = 0;
            G = X;
            B = C;
        } else if (fh == 4) {
            R = X;
            G = 0;
            B = C;
        } else if (fh == 5) {
            R = C;
            G = 0;
            B = X;
        }

        return Color<T>(R + m, G + m, B + m);
    }

    // Hue (0..359), Saturation (0..1), Lightness (0..1)
    static Color<T> from_HSL(const std::vector<T> &hsl) {
        if (hsl.size() < 0) return Color<T>();
        return bim::Color<T>::from_HSL(hsl[0], hsl[1], hsl[2]);
    }

    // R (0..1), G (0..1), B (0..1) [, A (0..1)]
    static Color<T> from_string_float(const bim::xstring &v, const bim::xstring &sep = ",") {
        std::vector<double> vv = v.splitDouble(sep);
        if (vv.size() < 3) return Color<T>();
        T a = 1.0;
        if (vv.size() > 3) a = vv[3];
        return Color<T>(vv[0], vv[1], vv[2], a);
    }

    // #RRGGBBAA or #RRGGBB or #RGB
    static Color<T> from_string_hex(const bim::xstring &v) {
        if (v[0] != '#') return Color<T>();
        if (v.size() == 4) {
            int r = v.substring(1, 1).toIntFromHex(0);
            int g = v.substring(2, 1).toIntFromHex(0);
            int b = v.substring(3, 1).toIntFromHex(0);
            r += (r * 16);
            g += (g * 16);
            b += (b * 16);
            return Color<T>::from_RGBA(r, g, b);
        } else if (v.size() == 7) {
            int r = v.substring(1, 2).toIntFromHex(0);
            int g = v.substring(3, 2).toIntFromHex(0);
            int b = v.substring(5, 2).toIntFromHex(0);
            return Color<T>::from_RGBA(r, g, b);
        } else if (v.size() == 9) {
            int r = v.substring(1, 2).toIntFromHex(0);
            int g = v.substring(3, 2).toIntFromHex(0);
            int b = v.substring(5, 2).toIntFromHex(0);
            int a = v.substring(7, 2).toIntFromHex(0);
            return Color<T>::from_RGBA(r, g, b, a);
        }
        return Color<T>();
    }

    static Color<T> from_string_hex_ARGB(const bim::xstring &v) {
        if (v[0] != '#') return Color<T>();
        if (v.size() == 9) {
            int a = v.substring(1, 2).toIntFromHex(0);
            int r = v.substring(3, 2).toIntFromHex(0);
            int g = v.substring(5, 2).toIntFromHex(0);
            int b = v.substring(7, 2).toIntFromHex(0);
            return Color<T>::from_RGBA(r, g, b, a);
        }
        return Color<T>();
    }

    // R (0..255), G (0..255), B (0..255)[, A (0..255)]
    static Color<T> from_string_8bit(const bim::xstring &v, const bim::xstring &sep = ",") {
        std::vector<int> vv = v.splitInt(sep);
        if (vv.size() < 3) return Color<T>();
        int a = (vv.size() > 3) ? vv[3] : 255;
        return Color<T>::from_RGBA(vv[0], vv[1], vv[2], a);
    }

    // hashes string to produce a color
    static Color<T> from_text(const bim::xstring &str, bool darker = false) {
        int hash = 0;

        for (const char &c : str) {
            hash = int(c) + ((hash << 5) - hash);
        }

        std::vector<int> rgb(3, 0);
        for (int i = 0; i < 3; ++i) {
            int value = (hash >> (i * 8)) & 0xFF;
            rgb[i] = value;
        }

        Color<T> c = Color<T>::from_RGBA(rgb[0], rgb[1], rgb[2]);
        if (!darker) {
            if (c.isBlack()) c = Color<T>(1, 1, 1);
            std::vector<T> hsl = c.to_HSL();
            if (hsl[2] < 0.5) {
                return Color<T>::from_HSL(hsl[0], hsl[1], 0.5);
            }
        } else {
            if (c.isWhite()) c = Color<T>(0, 0, 0);
            std::vector<T> hsl = c.to_HSL();
            if (hsl[2] > 0.35) {
                return Color<T>::from_HSL(hsl[0], hsl[1], static_cast<T>(0.35));
            }
        }
        return c;
    }

    static Color<T> random(bool darker = false) {
        // Hue component(0..359), Saturation component(0..1), Lightness component(0..1)
        T S = static_cast<T>(0.9);
        T L = static_cast<T>(darker ? 0.2 : 0.8);
        std::srand(static_cast<unsigned int>(std::time(nullptr)));
        T H = static_cast<T>((std::rand() / (double)RAND_MAX) * 359.0);
        return Color<T>::from_HSL(H, S, L);
    }

    static Color<T> default_color(const int &i, const xstring &text = "", int samples = 3) {
        if (i == 0 && samples == 1)
            return bim::Color<T>(1, 1, 1);
        else if (i == 0)
            return bim::Color<T>(1, 0, 0);
        else if (i == 1)
            return bim::Color<T>(0, 1, 0);
        else if (i == 2)
            return bim::Color<T>(0, 0, 1);
        else if (i == 3)
            return bim::Color<T>(1, 1, 1);
        else if (i == 4)
            return bim::Color<T>(1, 0, 1);
        else if (i == 5)
            return bim::Color<T>(0, 1, 1);
        else if (i == 6)
            return bim::Color<T>(1, 1, 0);
        else if (i == 7)
            return bim::Color<T>(1, 1, 0);
        if (text.size() > 0)
            return bim::Color<T>::from_text(text);
        return bim::Color<T>::random();
    }

    static Color<T> fromEmissionWavelength(const float &em_wavelength) {
        //     Violet Below 450 nm
        //     Blue 450 - 500 nm
        //     Green 500 - 570 nm
        //     Yellow 570 - 590 nm
        //     Orange 590 - 610 nm
        //     Red 610 - 700 nm
        if (em_wavelength < 450)
            return bim::Color<T>(1, 0, 1);
        else if (em_wavelength < 500)
            return bim::Color<T>(0, 0, 1);
        else if (em_wavelength < 570)
            return bim::Color<T>(0, 1, 0);
        else if (em_wavelength < 590)
            return bim::Color<T>(1, 1, 0);
        else if (em_wavelength < 610)
            return bim::Color<T>(1, 0.5, 0);
        return bim::Color<T>(1, 0, 0);
    }

    static Color<T> fromExcitationWavelength(const float &ex_wavelength) {
        // this is an attempt to map excitations to typical emissions 
        // only used to derive a color if emission wavelength is unknown
        float em_wavelength = ex_wavelength + 25;
        return Color<T>::fromEmissionWavelength(em_wavelength);
    }
};

typedef Color<bim::float32> ColorF32;

} // namespace bim

#endif // BIM_COLOR
