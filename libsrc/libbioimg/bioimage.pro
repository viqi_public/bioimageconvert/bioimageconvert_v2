######################################################################
# Manually generated !!!
# libBioImage v 1.55 Project file
# run:
#   qmake -r bioimage.pro - in order to generate Makefile for your platform
#   make all - to compile the library
#
#
# Copyright (c) 2005-2010, Bio-Image Informatic Center, UCSB
#
# To generate Makefiles on any platform:
#   qmake bioimage.pro
#
# To generate VisualStudio project files:
#   qmake -t vcapp -spec win32-msvc2005 bioimage.pro
#   qmake -t vcapp -spec win32-msvc.net bioimage.pro
#   qmake -t vcapp -spec win32-msvc bioimage.pro
#   qmake -spec win32-icc bioimage.pro # to use pure Intel Compiler
#
# To generate xcode project files:
#   qmake -spec macx-xcode bioimage.pro
#
# To generate Makefiles on MacOSX with binary install:
#   qmake -spec macx-g++ bioimage.pro
#
######################################################################

win32 {
    *-g++* {
        message(detected platform Windows with compiler gcc (typically MinGW, MinGW64, Cygwin, MSYS2, or similar))
        CONFIG += mingw
        GCCMACHINETYPE=$$system("gcc -dumpmachine")
        contains(GCCMACHINETYPE, x86_64.*):CONFIG += win64
    }
}

#---------------------------------------------------------------------
# configuration: editable
#---------------------------------------------------------------------

TEMPLATE = lib
VERSION = 3.15.0

CONFIG += staticlib
CONFIG += release
CONFIG += warn_off


CONFIG += libbioimage_transforms # if fftw3 is available

CONFIG += lib_libtiff # we use a patched version
#CONFIG += sys_libtiff

CONFIG += lib_libgeotiff # patched libtiff requires this
#CONFIG += sys_libgeotiff # patched libtiff requires this

CONFIG += lib_proj # libgeotiff requires this
#CONFIG += sys_proj # libgeotiff requires this

#CONFIG += lib_libjpeg # pick one or the other
#CONFIG += lib_libjpeg_turbo # pick one or the other
CONFIG += sys_libjpeg_turbo

# CONFIG += lib_libpng # needed on older debian for static openslide build with newer libpng
CONFIG += sys_libpng

#CONFIG += lib_zlib
CONFIG += sys_zlib

CONFIG += lib_exiv2 # we use a patched version for JXR support
#CONFIG += sys_exiv2

CONFIG += lib_eigen

#CONFIG += lib_libraw
CONFIG += sys_libraw

CONFIG += lib_pugixml # patched version needed for compatibility with libczi

#CONFIG += lib_openjpeg
CONFIG += sys_openjpeg

CONFIG += lib_jxrlib # patched version needed for compatibility with libczi
#CONFIG += sys_jxrlib

#CONFIG += lib_libwebp
CONFIG += sys_libwebp

#CONFIG += lib_lcms2
CONFIG += sys_lcms2

#CONFIG += lib_gdcm
CONFIG += sys_gdcm

CONFIG += lib_libczi
#CONFIG += sys_libczi

CONFIG += sys_libhdf5

# CONFIG += sys_openslide
CONFIG += lib_openslide

CONFIG += lib_nikon_nd2

!macx:CONFIG += sys_ffmpeg
macx:CONFIG += lib_ffmpeg




CONFIG(debug, debug|release) {
  message(Building in DEBUG mode!)
  DEFINES += DEBUG _DEBUG _DEBUG_
}

macx {
  QMAKE_CFLAGS_RELEASE += -m64 -fPIC -lpthread -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0
  QMAKE_CXXFLAGS_RELEASE += -m64 -fPIC -lpthread -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0
  QMAKE_LFLAGS_RELEASE += -m64 -fPIC -lpthread -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0
  QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.11
} else {
  QMAKE_CFLAGS_DEBUG += -pg -fPIC -lpthread -pthread -ggdb -D_FILE_OFFSET_BITS=64
  QMAKE_CXXFLAGS_DEBUG += -pg -fPIC -lpthread -pthread -ggdb -std=c++11 -D_FILE_OFFSET_BITS=64
  QMAKE_LFLAGS_DEBUG += -pg -fPIC -lpthread -pthread -ggdb

  QMAKE_CFLAGS_RELEASE += -fPIC -lpthread -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0 -D_FILE_OFFSET_BITS=64
  QMAKE_CXXFLAGS_RELEASE += -fPIC -lpthread -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0 -std=c++11 -D_FILE_OFFSET_BITS=64
  QMAKE_LFLAGS_RELEASE += -fPIC -lpthread -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0

  QMAKE_CXXFLAGS += \
    -pedantic -Wextra \
    -Werror=return-type -Werror=ignored-qualifiers -Werror=write-strings \
    -Werror=uninitialized -Werror=pointer-arith \
    -Werror=type-limits \
    -Wsign-compare -Wundef -Wshadow -Wunknown-pragmas
}

#---------------------------------------------------------------------
# configuration paths: editable
#---------------------------------------------------------------------

BIM_ROOT = $${_PRO_FILE_PWD_}/../..
BIM_LSRC = $${BIM_ROOT}/libsrc
BIM_PROJECTS = $${BIM_ROOT}/projects
BIM_LIBS = $${BIM_ROOT}/libs
BIM_BUILD = $${BIM_ROOT}/build
BIM_BIN = $${BIM_ROOT}/bin
HOSTTYPE = $$(HOSTTYPE)

BIM_LIBS_PLTFM = $$BIM_LIBS
mingw {
    BIM_LIBS_PLTFM = $$BIM_LIBS/mingw
} else:win32 {
    BIM_LIBS_PLTFM = $$BIM_LIBS/win
} else:macx {
    BIM_LIBS_PLTFM = $$BIM_LIBS/macosx
} else:HOSTTYPE {
    BIM_LIBS_PLTFM = $$BIM_LIBS/linux_$$HOSTTYPE
} else {
    BIM_LIBS_PLTFM = $$BIM_LIBS/linux
}

mingw {
    BIM_GENS = ../../.generated/$$HOSTTYPE
    BIM_OBJ = $$BIM_GENS/obj # path for object files
    BIM_BIN = $$BIM_GENS # path for generated binary
} else {
    BIM_GENS = $${BIM_BUILD}/.generated
    BIM_OBJ = $$BIM_GENS/obj # path for object files
    BIM_BIN = $$BIM_LIBS_PLTFM # path for generated library
}

#---------------------------------------------------------------------
# configuration: automatic
#---------------------------------------------------------------------

win32:!mingw {
    DEFINES += _CRT_SECURE_NO_WARNINGS
}

unix {
  !exists( $$BIM_GENS ) {
    message( "Cannot find directory: $$BIM_GENS, creating..." )
    system( mkdir -p $$BIM_GENS )
  }
  !exists( $$BIM_OBJ ) {
    message( "Cannot find directory: $$BIM_OBJ, creating..." )
    system( mkdir -p $$BIM_OBJ )
  }
}


#---------------------------------------------------------------------
# configs
#---------------------------------------------------------------------

CONFIG -= qt x11 windows

MOC_DIR = $$BIM_GENS
DESTDIR = $$BIM_BIN
OBJECTS_DIR = $$BIM_OBJ
INCLUDEPATH += $$BIM_GENS

#---------------------------------------------------------------------
# libbioimage
#---------------------------------------------------------------------

DEFINES += BIM_USE_OPENMP
DEFINES += BIM_USE_CODECVT
DEFINES += BIM_COMPOSITE_FORMAT
DEFINES += BIM_USE_FILTERS

BIM_LIB_BIO    = $$BIM_LSRC/libbioimg
BIM_CORE       = $$BIM_LIB_BIO/core_lib
BIM_FMTS       = $$BIM_LIB_BIO/formats
BIM_FMTS_API   = $$BIM_LIB_BIO/formats_api
BIM_TRANSFORMS = $$BIM_LIB_BIO/transforms

INCLUDEPATH += $$BIM_LIB_BIO
INCLUDEPATH += $$BIM_FMTS_API
INCLUDEPATH += $$BIM_FMTS
INCLUDEPATH += $$BIM_FMTS/tiff
INCLUDEPATH += $$BIM_CORE

#core
SOURCES += $$BIM_CORE/xstring.cpp $$BIM_CORE/xtypes.cpp \
           $$BIM_CORE/tag_map.cpp $$BIM_CORE/xconf.cpp

HEADERS += $$BIM_CORE/blob_manager.h $$BIM_CORE/tag_map.h \
           $$BIM_CORE/xconf.h $$BIM_CORE/xpointer.h \
           $$BIM_CORE/xstring.h $$BIM_CORE/xtypes.h

#Formats API
SOURCES += $$BIM_FMTS_API/bim_img_format_utils.cpp \
           $$BIM_FMTS_API/bim_histogram.cpp \
           $$BIM_FMTS_API/bim_metatags.cpp \
           $$BIM_FMTS_API/bim_image.cpp \
           $$BIM_FMTS_API/bim_image_filters.cpp \
           $$BIM_FMTS_API/bim_image_opencv.cpp \
           $$BIM_FMTS_API/bim_image_pyramid.cpp \
           $$BIM_FMTS_API/bim_image_proxy.cpp \
           $$BIM_FMTS_API/bim_image_stack.cpp \
           $$BIM_FMTS_API/typeize_buffer.cpp

HEADERS += $$BIM_FMTS_API/bim_histogram.h \
           $$BIM_FMTS_API/bim_image_5d.h \
           $$BIM_FMTS_API/bim_image.h \
           $$BIM_FMTS_API/bim_image_pyramid.h \
           $$BIM_FMTS_API/bim_image_stack.h \
           $$BIM_FMTS_API/bim_img_format_interface.h \
           $$BIM_FMTS_API/bim_img_format_utils.h \
           $$BIM_FMTS_API/bim_metatags.h \
           $$BIM_FMTS_API/bim_primitives.h \
           $$BIM_FMTS_API/bim_qt_utils.h \
           $$BIM_FMTS_API/resize.h \
           $$BIM_FMTS_API/rotate.h \
           $$BIM_FMTS_API/slic.h \
           $$BIM_FMTS_API/typeize_buffer.h

#Formats
SOURCES += $$BIM_FMTS/bim_format_manager.cpp \
           $$BIM_FMTS/bim_ome_types.cpp \
           $$BIM_FMTS/biorad_pic/bim_biorad_pic_format.cpp \
           $$BIM_FMTS/bmp/bim_bmp_format.cpp \
           $$BIM_FMTS/nanoscope/bim_nanoscope_format.cpp \
           $$BIM_FMTS/raw/bim_raw_format.cpp \
           $$BIM_FMTS/ibw/bim_ibw_format.cpp \
           $$BIM_FMTS/ome/bim_ome_format.cpp \
           $$BIM_FMTS/mrc/bim_mrc_format.cpp \
           $$BIM_FMTS/composite/bim_composite_format.cpp

HEADERS += $$BIM_FMTS/dcraw/bim_dcraw_format.h \
           $$BIM_FMTS/bmp/bim_bmp_format.h \
           $$BIM_FMTS/raw/bim_raw_format.h \
           $$BIM_FMTS/ibw/bim_ibw_format.h \
           $$BIM_FMTS/biorad_pic/bim_biorad_pic_format.h \
           $$BIM_FMTS/bim_format_manager.h \
           $$BIM_FMTS/nanoscope/bim_nanoscope_format.h \
           $$BIM_FMTS/ome/bim_ome_format.h \
           $$BIM_FMTS/mrc/bim_mrc_format.h \
           $$BIM_FMTS/composite/bim_composite_format.h

#---------------------------------------------------------------------
# Transforms
#---------------------------------------------------------------------

DEFINES += BIM_USE_FILTERS

libbioimage_transforms {
    DEFINES += BIM_USE_TRANSFORMS
    BIM_LIB_FFT = $$BIM_LSRC/libfftw/src
    macx:INCLUDEPATH += $$BIM_LIB_FFT/api

    SOURCES += $$BIM_FMTS_API/bim_image_transforms.cpp

    SOURCES += $$BIM_TRANSFORMS/chebyshev.cpp \
               $$BIM_TRANSFORMS/FuzzyCalc.cpp \
               $$BIM_TRANSFORMS/radon.cpp \
               $$BIM_TRANSFORMS/wavelet/Common.cpp \
               $$BIM_TRANSFORMS/wavelet/convolution.cpp \
               $$BIM_TRANSFORMS/wavelet/DataGrid2D.cpp \
               $$BIM_TRANSFORMS/wavelet/DataGrid3D.cpp \
               $$BIM_TRANSFORMS/wavelet/Filter.cpp \
               $$BIM_TRANSFORMS/wavelet/FilterSet.cpp \
               $$BIM_TRANSFORMS/wavelet/Symlet5.cpp \
               $$BIM_TRANSFORMS/wavelet/Wavelet.cpp \
               $$BIM_TRANSFORMS/wavelet/WaveletHigh.cpp \
               $$BIM_TRANSFORMS/wavelet/WaveletLow.cpp \
               $$BIM_TRANSFORMS/wavelet/WaveletMedium.cpp \
               $$BIM_TRANSFORMS/wavelet/wt.cpp

    HEADERS += $$BIM_FMTS/wavelet/DataGrid.h $$BIM_FMTS/wavelet/wt.h \
               $$BIM_FMTS/wavelet/DataGrid3D.h $$BIM_FMTS/wavelet/WaveletLow.h \
               $$BIM_FMTS/wavelet/Common.h $$BIM_FMTS/wavelet/convolution.h \
               $$BIM_FMTS/wavelet/WaveletHigh.h $$BIM_FMTS/wavelet/FilterSet.h \
               $$BIM_FMTS/wavelet/DataGrid2D.h $$BIM_FMTS/wavelet/Symlet5.h \
               $$BIM_FMTS/wavelet/Filter.h $$BIM_FMTS/wavelet/Wavelet.h \
               $$BIM_FMTS/wavelet/WaveletMedium.h $$BIM_FMTS/FuzzyCalc.h \
               $$BIM_FMTS/chebyshev.h $$BIM_FMTS/radon.h
}

#---------------------------------------------------------------------
# Pole
#---------------------------------------------------------------------

BIM_LIB_POLE = $$BIM_LSRC/pole
INCLUDEPATH += $$BIM_LIB_POLE
SOURCES += $$BIM_LIB_POLE/pole.cpp

HEADERS += $$BIM_FMTS/ole/oib.h $$BIM_FMTS/ole/zvi.h \
           $$BIM_FMTS/ole/bim_ole_format.h

SOURCES += $$BIM_FMTS/ole/bim_ole_format.cpp \
           $$BIM_FMTS/ole/bim_oib_format_io.cpp \
           $$BIM_FMTS/ole/bim_zvi_format_io.cpp \
           $$BIM_FMTS/ole/zvi.cpp

#---------------------------------------------------------------------
# Jzon
#---------------------------------------------------------------------

BIM_LIB_JZON = $$BIM_LSRC/jzon
INCLUDEPATH += $$BIM_LIB_JZON
SOURCES += $$BIM_LIB_JZON/Jzon.cpp

#---------------------------------------------------------------------
# Pugixml
#---------------------------------------------------------------------

lib_pugixml {
    BIM_LIB_PUGIXML = $$BIM_LSRC/pugixml/src
    INCLUDEPATH += $$BIM_LIB_PUGIXML
    SOURCES += $$BIM_LIB_PUGIXML/pugixml.cpp
} else:sys_pugixml {

}

#---------------------------------------------------------------------
# NifTI
#---------------------------------------------------------------------

DEFINES += BIM_NIFTI_FORMAT
DEFINES += HAVE_ZLIB

BIM_LIB_NIFTI_LIB = $$BIM_LSRC/nifti_clib/nifti2
BIM_LIB_NIFTI_ZNZ = $$BIM_LSRC/nifti_clib/znzlib
INCLUDEPATH += $$BIM_LIB_NIFTI_LIB
INCLUDEPATH += $$BIM_LIB_NIFTI_ZNZ

# niftilib
SOURCES += $$BIM_LIB_NIFTI_LIB/nifti2_io.c
HEADERS += $$BIM_LIB_NIFTI_LIB/nifti1.h \
           $$BIM_LIB_NIFTI_LIB/nifti2.h \
           $$BIM_LIB_NIFTI_LIB/nifti2_io.h

# znzlib
SOURCES += $$BIM_LIB_NIFTI_ZNZ/znzlib.c
HEADERS += $$BIM_LIB_NIFTI_ZNZ/znzlib.h

SOURCES += $$BIM_FMTS/nifti/bim_nifti_format.cpp

#---------------------------------------------------------------------
# ffmpeg
#---------------------------------------------------------------------

lib_ffmpeg|sys_ffmpeg {
    BIM_LIB_FFMPEG = $$BIM_LSRC/ffmpeg
    BIM_FMT_FFMPEG = $$BIM_FMTS/mpeg
    DEFINES += BIM_FFMPEG_FORMAT FFMPEG_VIDEO_DISABLE_MATLAB __STDC_CONSTANT_MACROS

    HEADERS += $$BIM_FMTS/mpeg/parse.h \
        $$BIM_FMTS/mpeg/FfmpegCommon.h \
        $$BIM_FMTS/mpeg/IVideo.h \
        $$BIM_FMTS/mpeg/bim_ffmpeg_format.h \
        $$BIM_FMTS/mpeg/FfmpegIVideo.h \
        $$BIM_FMTS/mpeg/registry.h \
        $$BIM_FMTS/mpeg/matarray.h \
        $$BIM_FMTS/mpeg/FfmpegOVideo.h \
        $$BIM_FMTS/mpeg/debug.h \
        $$BIM_FMTS/mpeg/handle.h \
        $$BIM_FMTS/mpeg/OVideo.h

    SOURCES += $$BIM_FMT_FFMPEG/debug.cpp \
        $$BIM_FMT_FFMPEG/bim_ffmpeg_format.cpp \
        $$BIM_FMT_FFMPEG/FfmpegCommon.cpp \
        $$BIM_FMT_FFMPEG/FfmpegIVideo.cpp \
        $$BIM_FMT_FFMPEG/FfmpegOVideo.cpp \
        $$BIM_FMT_FFMPEG/registry.cpp
}

lib_ffmpeg {
    INCLUDEPATH += $$BIM_LIB_FFMPEG/include
    win32:!mingw:INCLUDEPATH += $$BIM_LIB_FFMPEG/include-win
} else:sys_ffmpeg {
    INCLUDEPATH += /usr/include/x86_64-linux-gnu
}

#---------------------------------------------------------------------
# GDCM
#---------------------------------------------------------------------

lib_gdcm|sys_gdcm {
    DEFINES += BIM_GDCM_FORMAT
    BIM_LIB_GDCM = $$BIM_LSRC/gdcm

    HEADERS += $$BIM_FMTS/dicom/bim_dicom_format.h
    SOURCES += $$BIM_FMTS/dicom/bim_dicom_format.cpp
}

lib_gdcm {
    DEFINES += OPJ_STATIC

    unix|mingw {
        INCLUDEPATH += $$BIM_LIB_GDCM/projects/unix
    } else {
        INCLUDEPATH += $$BIM_LIB_GDCM/projects/win64
    }

    INCLUDEPATH += $$BIM_LIB_GDCM/Source/Common
    INCLUDEPATH += $$BIM_LIB_GDCM/Source/DataDictionary
    INCLUDEPATH += $$BIM_LIB_GDCM/Source/DataStructureAndEncodingDefinition
    INCLUDEPATH += $$BIM_LIB_GDCM/Source/InformationObjectDefinition
    INCLUDEPATH += $$BIM_LIB_GDCM/Source/MediaStorageAndFileFormat
} else:sys_gdcm {
    INCLUDEPATH += /usr/include/gdcm-3.0 # ubuntu 20 and newer debian
    INCLUDEPATH += /usr/include/gdcm-2.8 # ubuntu 18 and newer debian
    INCLUDEPATH += /usr/include/gdcm-2.6 # ubuntu 16 and newer debian
    INCLUDEPATH += /usr/include/gdcm-2.4 # fix for an older debian system
}

#---------------------------------------------------------------------
# OPENJPEG
#---------------------------------------------------------------------

lib_openjpeg|sys_openjpeg {
    DEFINES += BIM_JP2_FORMAT
    BIM_LIB_OPENJPEG = $$BIM_LSRC/openjpeg/src

    HEADERS += $$BIM_FMTS/jp2/bim_jp2_format.h
    SOURCES += $$BIM_FMTS/jp2/bim_jp2_format.cpp
}

lib_openjpeg {
    DEFINES += OPJ_HAVE_LIBLCMS2
    INCLUDEPATH += $${BIM_LIB_OPENJPEG}/lib
    INCLUDEPATH += $${BIM_LIB_OPENJPEG}/lib/openjp2
    INCLUDEPATH += $${BIM_LIB_OPENJPEG}/bin
    INCLUDEPATH += $${BIM_LIB_OPENJPEG}/bin/common
} else:sys_openjpeg {
    INCLUDEPATH += /usr/include/openjpeg-2.1
    INCLUDEPATH += /usr/include/openjpeg-2.3
    INCLUDEPATH += /usr/include/openjpeg-2.4
}

#---------------------------------------------------------------------
# libraw
#---------------------------------------------------------------------

lib_libraw|sys_libraw {
    DEFINES += BIM_DCRAW_FORMAT
    BIM_LIB_RAW = $$BIM_LSRC/libraw

    SOURCES += $$BIM_FMTS/dcraw/bim_dcraw_format.cpp
}

lib_libraw {
    DEFINES += LIBRAW_BUILDLIB LIBRAW_NODLL USE_JPEG USE_ZLIB USE_LCMS2
    INCLUDEPATH += $$BIM_LIB_RAW

} else:sys_libraw {
    INCLUDEPATH += /usr/include/libraw # ubuntu 18 and newer debian
}

#---------------------------------------------------------------------
# Now adding static libraries
#---------------------------------------------------------------------

#some configs first
unix | mingw:DEFINES += HAVE_UNISTD_H
unix | mingw:DEFINES -= HAVE_IO_H
win32:!mingw:DEFINES += HAVE_IO_H

macx:DEFINES += HAVE_UNISTD_H
#macx:DEFINES += WORDS_BIGENDIAN
macx:DEFINES -= HAVE_IO_H

#---------------------------------------------------------------------
# LZMA - XZ Utils
#---------------------------------------------------------------------

lib_lzma {
    BIM_LIB_LZMA = $$BIM_LSRC/liblzma
    INCLUDEPATH += $$BIM_LIB_LZMA/liblzma/api
}

#---------------------------------------------------------------------
# libTiff
#---------------------------------------------------------------------

BIM_LIB_TIF = $$(BIM_LIB_TIF)
isEmpty(BIM_LIB_TIF) {
    message("No external libtiff source directory given, will use internal libtiff.")
    BIM_LIB_TIF = $$BIM_LSRC/libtiff
    !exists($$BIM_LIB_TIF/libtiff/tiff.h):error("Missing internal libtiff header at $$BIM_LIB_TIF/libtiff/tiff.h, please initialize the git submodules, or provide a separate checkout.")
} else {
    message("External libtiff source directory given at '$${BIM_LIB_TIF}'.")
    !exists($$BIM_LIB_TIF/libtiff/tiff.h):error("Can not find tiff.h in external libtiff header at $$BIM_LIB_TIF/libtiff/tiff.h, please specify the root of the source tree.")
}

lib_libtiff|sys_libtiff {
    DEFINES += BIM_TIFF_FORMAT
    HEADERS += $$BIM_FMTS/tiff/bim_ometiff_format.h \
        $$BIM_FMTS/tiff/round.h \
        $$BIM_FMTS/tiff/bim_xtiffio.h \
        $$BIM_FMTS/tiff/bim_tiff_format.h \
        $$BIM_FMTS/tiff/bim_tiny_tiff.h \
        $$BIM_FMTS/tiff/bim_stk_format.h \
        $$BIM_FMTS/tiff/memio.h \
        $$BIM_FMTS/tiff/bim_fluoview_format.h \
        $$BIM_FMTS/tiff/bim_psia_format.h \
        $$BIM_FMTS/tiff/bim_perkinelmer_qp_format.h \
        $$BIM_FMTS/tiff/bim_cz_lsm_format.h

    SOURCES += $$BIM_FMTS/tiff/bim_tiny_tiff.cpp \
        $$BIM_FMTS/tiff/bim_tiff_format.cpp \
        $$BIM_FMTS/tiff/bim_tiff_format_io.cpp \
        $$BIM_FMTS/tiff/bim_ometiff_format_io.cpp \
        $$BIM_FMTS/tiff/bim_cz_lsm_format_io.cpp \
        $$BIM_FMTS/tiff/bim_fluoview_format_io.cpp \
        $$BIM_FMTS/tiff/bim_psia_format_io.cpp \
        $$BIM_FMTS/tiff/bim_stk_format_io.cpp \
        $$BIM_FMTS/tiff/bim_geotiff_parse.cpp \
        $$BIM_FMTS/tiff/bim_perkinelmer_qp_format.cpp \
        $$BIM_FMTS/tiff/xtiff.c \
        $$BIM_FMTS/tiff/memio.c
}

lib_libtiff {
    DEFINES += TIF_HAVE_DIR64
    INCLUDEPATH += $$BIM_LIB_TIF/libtiff
} else:sys_libtiff {
    #INCLUDEPATH += /usr/include/libtiff
}

#---------------------------------------------------------------------
# libproj
#---------------------------------------------------------------------

lib_proj|sys_proj {
    DEFINES += BIM_PROJ4_FORMAT
    BIM_LIB_PROJ = $$BIM_LSRC/proj/src
}

lib_proj {
    INCLUDEPATH += $$BIM_PROJ4_FORMAT
} else:sys_proj {
    #INCLUDEPATH += /usr/include/libpng
}

#---------------------------------------------------------------------
# libgeotiff
#---------------------------------------------------------------------

lib_libgeotiff|sys_libgeotiff {
    DEFINES += BIM_GEOTIFF_FORMAT
    BIM_LIB_PROJ = $$BIM_LSRC/proj/src
}

lib_libgeotiff {
    BIM_LIB_GEOTIF = $$BIM_LSRC/libgeotiff/libgeotiff
    INCLUDEPATH += $$BIM_LIB_GEOTIF
} else:sys_libgeotiff {
    INCLUDEPATH += /usr/include/geotiff
}

#---------------------------------------------------------------------
# libpng
#---------------------------------------------------------------------

lib_libpng|sys_libpng {
    DEFINES += BIM_PNG_FORMAT
    BIM_LIB_PNG = $$BIM_LSRC/libpng
    HEADERS += $$BIM_FMTS/png/bim_png_format.h
    SOURCES += $$BIM_FMTS/png/bim_png_format.cpp
}

lib_libpng {
    INCLUDEPATH += $$BIM_LIB_PNG
} else:sys_libpng {
    INCLUDEPATH += /usr/include/libpng
}

#---------------------------------------------------------------------
# ZLib
#---------------------------------------------------------------------

lib_zlib {
    BIM_LIB_Z = $$BIM_LSRC/zlib
    INCLUDEPATH += $$BIM_LIB_Z
} else:sys_zlib {

}

#---------------------------------------------------------------------
# libjpeg
#---------------------------------------------------------------------

lib_libjpeg|sys_libjpeg {
    DEFINES += BIM_JPEG_FORMAT
    BIM_LIB_JPG = $$BIM_LSRC/libjpeg

    SOURCES += $$BIM_FMTS/jpeg/bim_jpeg_format.cpp
    HEADERS += $$BIM_FMTS/jpeg/bim_jpeg_format.h
}

lib_libjpeg {
    INCLUDEPATH += $$BIM_LIB_JPG
}

#---------------------------------------------------------------------
# libjpeg-turbo
#---------------------------------------------------------------------

lib_libjpeg_turbo|sys_libjpeg_turbo {
    DEFINES += BIM_JPEG_FORMAT
    BIM_LIB_JPG = $$BIM_LSRC/libjpeg-turbo

    SOURCES += $$BIM_FMTS/jpeg/bim_jpeg_format.cpp
    HEADERS += $$BIM_FMTS/jpeg/bim_jpeg_format.h
}

lib_libjpeg_turbo {
    INCLUDEPATH += $$BIM_LIB_JPG
}

#---------------------------------------------------------------------
# exiv2
#---------------------------------------------------------------------

lib_exiv2|sys_exiv2 {
    DEFINES += BIM_USE_EXIV2
    BIM_LIB_EXIV2 = $$BIM_LSRC/exiv2

    SOURCES += $$BIM_FMTS/bim_exiv_parse.cpp
}

lib_exiv2 {
    DEFINES += SUPPRESS_WARNINGS
    INCLUDEPATH += $$BIM_LIB_EXIV2/include
    INCLUDEPATH += $$BIM_BUILD/exiv2
}

#---------------------------------------------------------------------
# eigen
#---------------------------------------------------------------------

lib_eigen {
    DEFINES += BIM_USE_EIGEN
    BIM_LIB_EIGEN = $$BIM_LSRC/eigen
    INCLUDEPATH += $$BIM_LIB_EIGEN
}

#---------------------------------------------------------------------
# jxrlib
#---------------------------------------------------------------------

sys_jxrlib|lib_jxrlib {
    BIM_LIB_JXRLIB = $$BIM_LSRC/jxrlib
    DEFINES += BIM_JXRLIB_FORMAT

    SOURCES += $$BIM_FMTS/jxr/bim_jxr_format.cpp
}

lib_jxrlib {
    INCLUDEPATH += $$BIM_LIB_JXRLIB/common/include
    INCLUDEPATH += $$BIM_LIB_JXRLIB/image/sys
    INCLUDEPATH += $$BIM_LIB_JXRLIB/jxrgluelib
}

#---------------------------------------------------------------------
# libwebp
#---------------------------------------------------------------------

lib_libwebp|sys_libwebp {
    DEFINES += BIM_LIBWEBP_FORMAT
    SOURCES += $$BIM_FMTS/webp/bim_webp_format.cpp
}

lib_libwebp {
    BIM_LIB_LIBWEBP = $$BIM_LSRC/libwebp
    INCLUDEPATH += $$BIM_LIB_LIBWEBP/src
}

#---------------------------------------------------------------------
# lcms2
#---------------------------------------------------------------------

lib_lcms2|sys_lcms2 {
    BIM_LIB_LCMS2 = $$BIM_LSRC/lcms2

    SOURCES += $$BIM_FMTS/bim_lcms_parse.cpp
}

lib_lcms2 {
    INCLUDEPATH += $$BIM_LIB_LCMS2/include
}

#---------------------------------------------------------------------
# libczi
#---------------------------------------------------------------------

lib_libczi|sys_libczi {
    BIM_LIB_CZI = $$BIM_LSRC/libczi
    DEFINES += BIM_CZI_FORMAT
    SOURCES += $$BIM_FMTS/czi/bim_czi_format.cpp
}

lib_libczi {
    INCLUDEPATH += $$BIM_LIB_CZI/Src/libCZI
}

#---------------------------------------------------------------------
# libhdf5
#---------------------------------------------------------------------

lib_libhdf5|sys_libhdf5 {
    DEFINES += BIM_HDF5_FORMAT
    SOURCES += $$BIM_FMTS/hdf5/bim_hdf5_format.cpp
}

lib_libhdf5 {
    BIM_LIB_HDF5 = $$BIM_LSRC/libhdf5
    INCLUDEPATH += $$BIM_LIB_HDF5/include
} else:sys_libhdf5 {
    INCLUDEPATH += /usr/include/hdf5/serial
}


#---------------------------------------------------------------------
# openslide
#---------------------------------------------------------------------

lib_openslide {
    INCLUDEPATH += $$BIM_LSRC/openslide/src
} else:sys_openslide {
    INCLUDEPATH += /usr/include/openslide
}

lib_openslide|sys_openslide {
    DEFINES += BIM_OPENSLIDE_FORMAT
    SOURCES += $$BIM_FMTS/openslide/bim_openslide_format.cpp
}

#---------------------------------------------------------------------
# Nikon ND2
#---------------------------------------------------------------------

lib_nikon_nd2 {
    DEFINES += BIM_ND2_FORMAT
    SOURCES += $$BIM_FMTS/nikon/bim_nd2_format.cpp
}
