######################################################################
# BioImageConvert v3.0.0
# run:
#   qmake -r imgcnv.pro - in order to generate Makefile for your platform
#   make all - to compile the library
#
# Copyright (c) 2021, ViQi Inc
#
# To generate Makefiles on any platform:
#   qmake imgcnv.pro
#
# To generate xcode project files:
#   qmake -spec macx-xcode imgcnv.pro
#
# To generate Makefiles on MacOSX with binary install:
#   qmake -spec macx-g++ imgcnv.pro
#
######################################################################

win32 {
    *-g++* {
        message(detected Windows with gcc, likely: MinGW, MinGW64, Cygwin, MSYS2, or similar)
        CONFIG += mingw
        GCCMACHINETYPE=$$system("gcc -dumpmachine")
        contains(GCCMACHINETYPE, x86_64.*):CONFIG += win64
    }
}

#---------------------------------------------------------------------
# configuration: editable
#---------------------------------------------------------------------

APP_NAME = imgcnv
TARGET = $$APP_NAME

TEMPLATE = app
VERSION = 3.15.0

CONFIG += console

CONFIG += release
#CONFIG += debug
CONFIG += warn_off


CONFIG += libbioimage_transforms # if fftw3 is available

CONFIG += lib_libtiff # we use a patched version
#CONFIG += sys_libtiff

CONFIG += lib_libgeotiff # patched libtiff requires this
#CONFIG += sys_libgeotiff # patched libtiff requires this

CONFIG += lib_proj # libgeotiff requires this
#CONFIG += sys_proj # libgeotiff requires this

CONFIG += sys_lzma

#CONFIG += lib_libjpeg # pick one or the other
#CONFIG += lib_libjpeg_turbo # pick one or the other
CONFIG += sys_libjpeg_turbo

#CONFIG += lib_libpng # needed on older debian for static openslide build with newer libpng
CONFIG += sys_libpng

#CONFIG += lib_zlib
CONFIG += sys_zlib

CONFIG += lib_exiv2 # we use a patched version for JXR support
#CONFIG += sys_exiv2

CONFIG += lib_eigen

#CONFIG += lib_libraw
CONFIG += sys_libraw

CONFIG += lib_pugixml # patched version needed for compatibility with libczi

#CONFIG += lib_openjpeg
CONFIG += sys_openjpeg

CONFIG += lib_jxrlib # patched version needed for compatibility with libczi
#CONFIG += sys_jxrlib

#CONFIG += lib_libwebp
CONFIG += sys_libwebp

#CONFIG += lib_lcms2
CONFIG += sys_lcms2

#CONFIG += lib_gdcm
CONFIG += sys_gdcm

CONFIG += lib_libczi
#CONFIG += sys_libczi

CONFIG += sys_libhdf5

# CONFIG += sys_openslide
CONFIG += lib_openslide

CONFIG += lib_nikon_nd2

!macx:CONFIG += sys_ffmpeg
macx:CONFIG += lib_ffmpeg

CONFIG(debug, debug|release) {
    message(Building in DEBUG mode!)
    DEFINES += DEBUG _DEBUG _DEBUG_
}

macx {
  QMAKE_CFLAGS_RELEASE += -m64 -fPIC -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0
  QMAKE_CXXFLAGS_RELEASE += -m64 -fPIC -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0
  QMAKE_LFLAGS_RELEASE += -m64 -fPIC -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0
  QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.11
} else {
  QMAKE_CFLAGS_DEBUG += -pg -fPIC -pthread -ggdb -D_FILE_OFFSET_BITS=64
  QMAKE_CXXFLAGS_DEBUG += -pg -fPIC -pthread -ggdb -std=c++11 -D_FILE_OFFSET_BITS=64
  QMAKE_LFLAGS_DEBUG += -pg -fPIC -pthread -ggdb

  QMAKE_CFLAGS_RELEASE += -fPIC -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0 -D_FILE_OFFSET_BITS=64
  QMAKE_CXXFLAGS_RELEASE += -fPIC -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0 -std=c++11 -D_FILE_OFFSET_BITS=64
  QMAKE_LFLAGS_RELEASE += -fPIC -pthread -fopenmp -O3 -ftree-vectorize -msse2 -ftree-vectorizer-verbose=0
}

#---------------------------------------------------------------------
# configuration paths: editable
#---------------------------------------------------------------------

BIM_ROOT = $${_PRO_FILE_PWD_}/..
BIM_LSRC = $${BIM_ROOT}/libsrc
BIM_LIBS = $${BIM_ROOT}/libs
BIM_PROJECTS = $${BIM_ROOT}/projects
BIM_BUILD = $${BIM_ROOT}/build
BIM_BIN = $${BIM_ROOT}/bin
HOSTTYPE = $$(HOSTTYPE)

BIM_LIBS_PLTFM = $$BIM_LIBS
mingw {
    BIM_LIBS_PLTFM = $$BIM_LIBS/mingw
} else:win32 {
    BIM_LIBS_PLTFM = $$BIM_LIBS/win
} else:macx {
    BIM_LIBS_PLTFM = $$BIM_LIBS/macosx
} else:HOSTTYPE {
    BIM_LIBS_PLTFM = $$BIM_LIBS/linux_$$HOSTTYPE
} else {
    BIM_LIBS_PLTFM = $$BIM_LIBS/linux
}

mingw {
    BIM_GENS = ../.generated/$$HOSTTYPE
    BIM_OBJ = $$BIM_GENS/obj # path for object files
    BIM_BIN = $$BIM_GENS # path for generated binary
} else {
    BIM_GENS = $${BIM_BUILD}/.generated
    BIM_OBJ = $${BIM_BUILD}/obj # path for object files
}

#---------------------------------------------------------------------
# configuration: automatic
#---------------------------------------------------------------------

win32:!mingw {
  DEFINES += _CRT_SECURE_NO_WARNINGS
}

!exists( $$BIM_GENS ) {
    message( "Cannot find directory: $$BIM_GENS, creating..." )
    system( mkdir -p $$BIM_GENS )
}
!exists( $$BIM_OBJ ) {
    message( "Cannot find directory: $$BIM_OBJ, creating..." )
    system( mkdir -p $$BIM_OBJ )
}

#---------------------------------------------------------------------
# generation: fixed
#---------------------------------------------------------------------

CONFIG -= qt x11 windows

DESTDIR = $$BIM_BIN
OBJECTS_DIR = $$BIM_OBJ
INCLUDEPATH += $$BIM_GENS

#---------------------------------------------------------------------
# main sources
#---------------------------------------------------------------------

BIM_SRC  = $${BIM_ROOT}/src

SOURCES += $$BIM_SRC/main.cpp
SOURCES += $$BIM_SRC/reg/registration.cpp

#---------------------------------------------------------------------
# libbioimage
#---------------------------------------------------------------------

DEFINES += BIM_USE_FILTERS BIM_USE_TRANSFORMS
BIM_LIB_BIO = $$BIM_LSRC/libbioimg
INCLUDEPATH += $$BIM_LIB_BIO $$BIM_LIB_BIO/core_lib $$BIM_LIB_BIO/formats_api $$BIM_LIB_BIO/formats

LIBS += $$BIM_LIBS_PLTFM/libbioimage.a

#---------------------------------------------------------------------
# eigen
#---------------------------------------------------------------------

#lib_eigen {
#    INCLUDEPATH += $$BIM_LIB_EIGEN
#}

#---------------------------------------------------------------------
# pugixml
#---------------------------------------------------------------------

sys_pugixml {
    LIBS += -lpugixml
}

#---------------------------------------------------------------------
# libTIFF
#---------------------------------------------------------------------

sys_libtiff {
    LIBS += -ltiff -ltiffxx
} else:lib_libtiff {
    LIBS += $$BIM_LIBS_PLTFM/libtiff.a
    LIBS += $$BIM_LIBS_PLTFM/libtiffxx.a
    LIBS += -ljbig -ldeflate -lLerc
}

#---------------------------------------------------------------------
# libgeotiff
#---------------------------------------------------------------------

sys_libgeotiff {
    LIBS += -lgeotiff
} else:lib_libgeotiff {
    LIBS += $$BIM_LIBS_PLTFM/libgeotiff.a
}

#---------------------------------------------------------------------
# libproj
#---------------------------------------------------------------------

sys_proj {
    LIBS += -lproj
} else:lib_proj {
    LIBS += $$BIM_LIBS_PLTFM/libproj.a
    LIBS += -lsqlite3
}

#---------------------------------------------------------------------
# libpng
#---------------------------------------------------------------------

sys_libpng {
    LIBS += -lpng
} else:lib_libpng {
    LIBS += $$BIM_LIBS_PLTFM/libpng.a
}

#---------------------------------------------------------------------
# ZLib
#---------------------------------------------------------------------

sys_zlib {
    LIBS += -lz
} else:lib_zlib {
    LIBS += $$BIM_LIBS_PLTFM/libz.a
}

#---------------------------------------------------------------------
# jpeg-turbo
#---------------------------------------------------------------------

sys_libjpeg_turbo {
    LIBS += -ljpeg
} else:lib_libjpeg_turbo {
    LIBS += $$BIM_LIBS_PLTFM/turbojpeg-static.lib
}

#---------------------------------------------------------------------
# LZMA
#---------------------------------------------------------------------

sys_lzma {
    LIBS += -llzma
} else:lib_lzma {
    LIBS += $$BIM_LIBS_PLTFM/liblzma.lib
}

#---------------------------------------------------------------------
# exiv2
#---------------------------------------------------------------------

sys_exiv2 {
    LIBS += -lexiv2
} else:lib_exiv2 {
    LIBS += $$BIM_LIBS_PLTFM/libexiv2.a
    # LIBS += $$BIM_LIBS_PLTFM/libexiv2-xmp.a
    LIBS += -lexpat
}

#---------------------------------------------------------------------
# ffmpeg
#---------------------------------------------------------------------

sys_ffmpeg {
    LIBS += -lavformat
    LIBS += -lavcodec
    LIBS += -lavutil
    LIBS += -lswresample
    LIBS += -lswscale
    LIBS += -lpthread
} else:lib_ffmpeg {
    LIBS += $$BIM_LIBS_PLTFM/libavformat.a
    LIBS += $$BIM_LIBS_PLTFM/libavcodec.a
    LIBS += $$BIM_LIBS_PLTFM/libswresample.a
    LIBS += $$BIM_LIBS_PLTFM/libswscale.a
    LIBS += $$BIM_LIBS_PLTFM/libavutil.a
    LIBS += $$BIM_LIBS_PLTFM/libvpx.a
    LIBS += $$BIM_LIBS_PLTFM/libx264.a
    LIBS += $$BIM_LIBS_PLTFM/libx265.a
    LIBS += -lpthread -lxvidcore -lopenjpeg -lschroedinger-1.0 -ltheora -ltheoraenc -ltheoradec
}

#---------------------------------------------------------------------
# openjpeg
#---------------------------------------------------------------------

sys_openjpeg {
    LIBS += -lopenjp2
} else:lib_openjpeg {
    LIBS += $$BIM_LIBS_PLTFM/libopenjp2.a
}

#---------------------------------------------------------------------
# jxrlib
#---------------------------------------------------------------------

sys_jxrlib {
    LIBS += -ljxr
} else:lib_jxrlib {
    LIBS += $$BIM_LIBS_PLTFM/libjxrglue.a
    LIBS += $$BIM_LIBS_PLTFM/libjpegxr.a
}

#---------------------------------------------------------------------
# libwebp
#---------------------------------------------------------------------

sys_libwebp {
    LIBS += -lwebp -lwebpdemux -lwebpmux
} else:lib_libwebp {
    LIBS += $$BIM_LIBS_PLTFM/libwebp.a
    LIBS += $$BIM_LIBS_PLTFM/libwebpmux.a
    LIBS += $$BIM_LIBS_PLTFM/libwebpdemux.a
}

#---------------------------------------------------------------------
# GDCM
#---------------------------------------------------------------------

sys_gdcm {
    LIBS += -lgdcmDICT
    LIBS += -lgdcmMSFF
    LIBS += -lgdcmCommon
    LIBS += -lgdcmDSED
    LIBS += -lgdcmIOD
    LIBS += -lgdcmjpeg8
    LIBS += -lgdcmjpeg12
    LIBS += -lgdcmjpeg16
} else:lib_gdcm {
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmDICT.a
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmMSFF.a
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmCommon.a
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmDSED.a
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmIOD.a
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmcharls.a
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmexpat.a
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmjpeg8.a
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmjpeg12.a
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmjpeg16.a
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmopenjpeg.a
    LIBS += $$BIM_LIBS_PLTFM/gdcm/libgdcmzlib.a

    macx:LIBS += -framework CoreFoundation
}

#---------------------------------------------------------------------
# lcms2
#---------------------------------------------------------------------

sys_lcms2 {
    LIBS += -llcms2
} else:lib_lcms2 {
    LIBS += $$BIM_LIBS_PLTFM/liblcms2.a
}

#---------------------------------------------------------------------
# libczi
#---------------------------------------------------------------------

sys_libczi {
    LIBS += -lCZI
} else:lib_libczi {
    LIBS += $$BIM_LIBS_PLTFM/libczi.a
    LIBS += -lzstd
}

#---------------------------------------------------------------------
# libhdf5
#---------------------------------------------------------------------

sys_libhdf5 {
    #LIBS += -lhdf5_serial -lhdf5_cpp
    LIBS += -lhdf5_serial

    # ubuntu 20
    exists( /usr/lib/x86_64-linux-gnu/libhdf5_cpp.so ) {
        LIBS += -lhdf5_cpp
    }

    # ubuntu 21
    exists( /usr/lib/x86_64-linux-gnu/libhdf5_serial_cpp.so ) {
        LIBS += -lhdf5_serial_cpp
    }

} else:lib_libhdf5 {
    LIBS += $$BIM_LIBS_PLTFM/libhdf5.a $$BIM_LIBS_PLTFM/libhdf5_cpp.a
}

#---------------------------------------------------------------------
# openslide
#---------------------------------------------------------------------

sys_openslide {
    LIBS += -lopenslide
} else:lib_openslide {
    # LIBS += $$BIM_LIBS_PLTFM/libopenslide-common.a
    LIBS += $$BIM_LIBS_PLTFM/libopenslide.a
    LIBS += $$BIM_LIBS_PLTFM/libdicom.a
    LIBS += -lxml2 -lglib-2.0 -lgobject-2.0 -lgio-2.0 -lcairo -lgdk_pixbuf-2.0 -lsqlite3 -lpthread
}

#---------------------------------------------------------------------
# libraw
#---------------------------------------------------------------------

sys_libraw {
    LIBS += -lraw
} else:lib_libraw {
    LIBS += $$BIM_LIBS_PLTFM/libraw.a
}

#---------------------------------------------------------------------
# required libs
#---------------------------------------------------------------------

LIBS += -lbz2
LIBS += -ldl
LIBS += -lm

macx {
  LIBS += $$BIM_LIBS_PLTFM/libfftw3.a
  LIBS += -liconv
  LIBS += -lz
} else {
  LIBS += -lfftw3
}
