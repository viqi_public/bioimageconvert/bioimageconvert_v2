# BioImageConvertor and libbioimage

libbioimage is a C++ image I/O and basic processing library for most life-sciences image formats produced by HCA, whole-slide, 3D, AFM and other devices by most major manufacturers like Zeiss, Bruker, Nikon, Olympus, BioTek, Oxford Instruments, etc.

Libbioimage uses an N-D image model supporting 4 spatial and temporal dimensions, very large number of image channels or unlimited in a spectral case and other non-spatial or radiometric dimensions like views. It also supports any pixel format per channel covering 8,12,14,16,32,64 bits in signed, unsigned and floating point formats. All of those configurations are supported by all processing algorithms and most processing is fully parallelized.

BioImageConverter is a command line front-end for libbioimage and supports most standard image image conversion operations.

## Build

Use existing docker scripts for build environments, found in: docker

```
make -j
```

will produce:
imgcnv
libimgcnv.so

## Test

```
cd testing
python runtest.py
```

## Usage of command-line imgcnv tool

```
BioImageConvertor ver: 3.9.0

Author: Dima V. Fedorov <http://www.dimin.net/>

Arguments: [[-i | -o] FILE_NAME | -t FORMAT_NAME ]

Ex: imgcnv -i 1.jpg -o 2.tif -t TIFF

-brightnesscontrast   - color brightness/contrast adjustment: brightness,contrast, each in range [-100,100], ex: -brightnesscontrast 50,-40


-c                    - additional channels input file name, multiple -c are allowed, in which case multiple channels will be added, -c image must have the same size

-correction_bf        - apply bright filed and dark noise corrections to the image, ex: -correction_bf "path1/file_bf;path2/file_ff"
  bright filed and dark noise are separated by ;
    correction file may contain same number of channels, ex: -correction_bf "path1/file_bf;path2/file_ff"
    channels may also be specified separately, ex: -correction_bf "path1/file_bf_ch1,path2/file_bf_ch2,path3/file_bf_ch3;path1/file_ff_ch1,path2/file_ff_ch2,path3/file_ff_ch3"


-create               - creates a new image with w-width, h-height, z-num z, t-num t, c - channels, d-bits per channel, ex: -create 100,100,1,1,3,8

-deconvolve_rgb       - deconvolves input RGB image into 3 uncorrelated channels according to passed conversion matrix, ex: -deconvolve_rgb 0.65,0.70,0.29,0.07,0.99,0.11,0.27,0.57,0.78
The matrix will be normalized and inverted prior to the operation.
Instead of a transformation matrix there might be pre-defined transformation like:
    he - immunohistochemical (IHC) -> Hematoxylin, Eosin (H&E)
    he2 - immunohistochemical (IHC) -> Hematoxylin, Eosin (H&E 2)
    hed - immunohistochemical (IHC) -> Hematoxylin, Eosin, DAB (HED)
    hdx - immunohistochemical (IHC) -> Hematoxylin, DAB
    fgx - immunohistochemical (IHC) -> Feulgen, Light Green
    bex - immunohistochemical (IHC) -> Giemsa stain: Methyl Blue, Eosin
    rbd - immunohistochemical (IHC) -> FastRed, FastBlue, DAB
    gdx - immunohistochemical (IHC) -> Methyl Green, DAB
    hax - immunohistochemical (IHC) -> Hematoxylin, AEC
    bro - immunohistochemical (IHC) -> Mallory-Azan: Anilline Blue, Azocarmine G, Orange G
    bpx - immunohistochemical (IHC) -> Masson Trichrome: Methyl Blue, Ponceau Fuchsin
    ahx - immunohistochemical (IHC) -> Alcian Blue, Hematoxylin
    hpx - immunohistochemical (IHC) -> Hematoxylin, PAS
    bb - immunohistochemical (IHC) -> Brilliant Blue


-deinterlace          - deinterlaces input image with one of the available methods, ex: -deinterlace avg
    odd  - Uses odd lines
    even - Uses even lines
    avg  - Averages lines


-depth                - output depth (in bits) per channel, allowed values now are: 8,16,32,64, ex: -depth 8,D,U
  if followed by comma and [F|D|T|E] allowes to choose LUT method
    F - Linear full range
    D - Linear data range (default)
    T - Linear data range with tolerance ignoring very low values
    E - equalized
    C - type cast
    N - floating point number [0, 1]
    G - Gamma correction, requires setting -gamma
    L - Levels: Min, Max and Gamma correction, requires setting -gamma, -maxv and -minv
    f1 - Linear data range with filtered log tolerance at mild level
    f2 - Linear data range with filtered log tolerance at normal level
    f3 - Linear data range with filtered log tolerance at heavy level
    f4 - Linear data range with filtered log tolerance at aggressive level
  if followed by comma and [U|S|F] the type of output image can be defined
    U - Unsigned integer (with depths: 8,16,32,64) (default)
    S - Signed integer (with depths: 8,16,32,64)
    F - Float (with depths: 32,64,80)
  if followed by comma and [CS|CC] sets channel mode
    CS - channels separate, each channel enhanced separately (default)
    CC - channels combined, channels enhanced together preserving mutual relationships
  if followed by comma and min,max,gamma[,min,max,gamma]... will apply levels correction, ex: -depth 8,L,U,CS,15,200,1.2
  single min,max,gamma will apply to all channels, multiple will apply to respective channels


-display              - creates 3 channel image with preferred channel mapping

-enhancemeta          - Enhances an image beased on preferred settings, currently only CT hounsfield mode is supported, ex: -enhancemeta

-filter               - filters input image, ex: -filter edge
    edge - first derivative
    otsu - b/w masked image
    wndchrmcolor - color quantized hue image

-flatfield            - flatfield correction filter, ex: -flatfield l=-4;b=0.5;mid=4,5
    l: levels - indicates how many levels split image into, 0 is fully automatic, >0 exact number of levels, <0 automatic - requested_number_of_levels
    b: base - base intensity, use 0.5 for brighfield images and 0 for fluorescence
    mid: midrange - is a list of levels to remove mid-range aberrations, eg: "4,5"


-flip                 - flip the image vertically

-fmt                  - print supported formats

-fmthtml              - print supported formats in HTML

-fmtxml               - print supported formats in XML

-fuse                 - Changes order and number of channels in the output additionally allowing combining channels
Channels separated by comma specifying output channel order (0 means empty channel)
multiple channels can be added using + sign, ex: -fuse 1+4,2+4+5,3

-fuse-lut2d           - Available LUTs include:
    jet, parula, hsv, hot, cool, spring, summer, autumn, winter, bone, copper, pink
    colormap, heatmap, lsm, rainbow, fluorescence, greentored, bluetored, gold
    blackbody, endoscopy, flow, ired, perfusion, pet, ratio, spectrum, vr_bones, vr_muscles_bones, vr_red_vessels
    gray, red, green, blue, yellow, magenta, cyan, black


-fuse6                - Produces 3 channel image from up to 6 channels
Channels separated by comma in the following order: Red,Green,Blue,Yellow,Magenta,Cyan,Gray
(0 or empty value means empty channel), ex: -fuse6 1,2,3,4


-fusegrey             - Produces 1 channel image averaging all input channels, uses RGB weights for 3 channel images and equal weights for all others, ex: -fusegrey

-fusemeta             - Produces 3 channel image getting fusion weights from embedded metadata, ex: -fusemeta

-fusemethod           - Defines fusion method, ex: -fusemethod a
  should be followed by comma and [a|m]
    a - Average
    m - Maximum


-fusergb              - Produces 3 channel image from N channels, for each channel an RGB weight should be given
Component contribution are separated by comma and channels are separated by semicolon:
(0 or empty value means no output), ex: -fusergb 100,0,0;0,100,100;0;0,0,100
Here ch1 will go to red, ch2 to cyan, ch3 not rendered and ch4 to blue


-gaussian             - Gaussian convolution, ex: -gaussian 5,5,1.0
    kernel size in x - should be an odd number, ex: 3,5,7,11
    kernel size in y - should be an odd number, ex: 3,5,7,11
    sigma - spread of the gaussian function

-geometry             - redefines geometry for any incoming image with: z-num z, t-num t and optionally c-num channels, ex: -geometry 5,1 or -geometry 5,1,3

-histogram            - estimate a histogram for N-D image and write it to the Binary (BIH) and XML files defined by the specified filename, xml will be appended to the binary file name

-hounsfield           - enhances CT image using hounsfield scale, ex: -hounsfield 8,U,40,80
  output depth (in bits) per channel, allowed values now are: 8,16,32,64
  followed by comma and [U|S|F] the type of output image can be defined
    U - Unsigned integer (with depths: 8,16,32,64) (default)
    S - Signed integer (with depths: 8,16,32,64)
    F - Float (with depths: 32,64,80)
  followed by comma and window center
  followed by comma and window width
  optionally followed by comma and slope
  followed by comma and intercept, ex: -hounsfield 8,U,40,80,1.0,-1024.0
  if slope and intercept are not set, their values would be red from DICOM metadata, defaulting to 1 and -1024


-i                    - input file name, multiple -i are allowed, but in multiple case each will be interpreted as a 1 page image.

-icc-load             - Load ICC profile from a file

-icc-save             - Save ICC profile into a file if present

-icc-transform-file   - Transform image to ICC profile loaded from a file

-icc-transform-name   - Transform image to ICC profile given by a name: srgb|lab|xyz|cmyk

-ihst                 - read image histogram from the file and use for nhancement operations

-il                   - list input file name, containing input file name per line of the text file

-info                 - print image info

-interpolation        - should be followed by the interpolation method, ex: -interpolation lanczos
  it may be used by routines that may need to use interpolation, like heatmap rendering
    NN - Nearest neighbor (default)
    BL - Bilinear
    BC - Bicubic
    lanczos
    bessel
    point
    box
    hermite
    hanning
    hamming
    blackman
    gaussian
    quadratic
    catrom
    mitchell
    sinc
    blackmanbessel
    blackmansinc


-levels               - color levels adjustment: min,max,gamma[,min,max,gamma]..., ex: -levels 15,200,1.2
  single min,max,gamma will apply to all channels, multiple will apply to respective channels


-loadomexml           - reads OME-XML from a file and writes if output format is OME-TIFF

-log                  - Laplacian of Gaussian convolution, ex: -log 5,5,1.0
    kernel size in x - should be an odd number, ex: 3,5,7,11
    kernel size in y - should be an odd number, ex: 3,5,7,11
    sigma - spread of the gaussian function

-lut-2d               - Available LUTs include:
    jet, parula, hsv, hot, cool, spring, summer, autumn, winter, bone, copper, pink
    colormap, heatmap, lsm, rainbow, fluorescence, greentored, bluetored, gold
    blackbody, endoscopy, flow, ired, perfusion, pet, ratio, spectrum, vr_bones, vr_muscles_bones, vr_red_vessels
    gray, red, green, blue, yellow, magenta, cyan, black


-median               - Median filter, ex: -median 3
    kernel size - should be an odd number, ex: 3,5,7,11


-meta                 - print image's meta-data

-meta-custom          - print image's custom meta-data fields

-meta-keep            - removes all except provided comma separated tags, ex: -meta-keep pixel_resolution,raw/icc_profile

-meta-parsed          - print image's parsed meta-data, excluding custom fields

-meta-raw             - print image's raw meta-data in one huge pile

-meta-remove          - removes provided comma separated tags, ex: -meta-remove pixel_resolution,raw/icc_profile

-meta-tag             - prints contents of a requested tag, ex: -meta-tag pixel_resolution

-mirror               - mirror the image horizontally

-mosaic               - compose an image from aligned tiles, ex: -mosaic 512,20,11
  Arguments are defined as SZ,NX,NY where:
    SZ: defines the size of the tile in pixels with width equal to height
    NX - number of tile images in X direction    NY - number of tile images in Y direction

-mrfilter             - MR filter, ex: -mrfilter l=-4;n=1.0;bg=0.0;b=0.5;mid=3,4,5
    l: levels - indicates how many levels split image into, 0 is fully automatic, >0 exact number of levels, <0 automatic - requested_number_of_levels
    n: noise - multiplier for high resolution level, 0 - all removed, 1 - all left
    bg: background - multiplier for low resolution level, 0 - all removed, 1 - all left
    b: base - base intensity, use 0.5 for brighfield images and 0 for fluorescence
    mid: midrange - is a list of levels to remove mid-range aberrations, eg: "4,5"


-multi                - creates a multi-paged image if possible (TIFF,AVI), enabled by default

-negative             - returns negative of input image

-no-overlap           - Skips frames that overlap with the previous non-overlapping frame, ex: -no-overlap 5
  argument defines maximum allowed overlap in %, in the example it is 5%


-norm                 - normalize input into 8 bits output

-o                    - output file name

-ohst                 - write image histogram to the file

-ohstxml              - write image histogram to the XML file

-options              - specify encoder specific options, ex: -options "fps 15 bitrate 1000"

Video files AVI, SWF, MPEG, etc. encoder options:
  fps N - specify Frames per Second, where N is a float number, if empty or 0 uses default, ex: -options "fps 29.9"
  bitrate N - specify bitrate in Mb, where N is an integer number, if empty or 0 uses default, ex: -options "bitrate 10000000"

JPEG encoder options:
  quality N - specify encoding quality 0-100, where 100 is best, ex: -options "quality 90"
  progressive no - disables progressive JPEG encoding
  progressive yes - enables progressive JPEG encoding (default)

TIFF encoder options:
  compression N - where N can be: none, packbits, lzw, fax, jpeg, zip, lzma, jxr. ex: -options "compression lzw"
  quality N - specify encoding quality 0-100, where 100 is best, ex: -options "quality 90"
  tiles N - write tiled TIFF where N defined tile size, ex: tiles -options "512"
  pyramid N - writes TIFF pyramid where N is a storage type: subdirs, topdirs, ex: -options "compression lzw tiles 512 pyramid subdirs"

JPEG-2000 encoder options:
  tiles N - write tiled TIFF where N defined tile size, ex: tiles -options "2048"
  quality N - specify encoding quality 0-100, where 100 is lossless, ex: -options "quality 90"
JPEG-XR encoder options:
  quality N - specify encoding quality 0-100, where 100 is lossless, ex: -options "quality 90"
WebP encoder options:
  quality N - specify encoding quality 0-100, where 100 is lossless, ex: -options "quality 90"


-overlap-sampling     - Defines sampling after overlap detected until no overlap, used to reduce sampling if overlapping, ex: -overlap-sampling 5


-page                 - pages to extract, should be followed by page numbers separated by comma, ex: -page 1,2,5
  page enumeration starts at 1 and ends at number_of_pages
  page number can be a dash where dash will be substituted by a range of values, ex: -page 1,-,5  if dash is not followed by any number, maximum will be used, ex: '-page 1,-' means '-page 1,-,number_of_pages'
  if dash is a first caracter, 1 will be used, ex: '-page -,5' means '-page 1,-,5'

-path                 - Path to a serie to extract from a multi-serie file, ex: -path group1/matrix2

-pixelcounts          - counts pixels above and below a given threshold, requires output file name to store resultant XML file, ex: -pixelcounts 120


-project              - combines by MAX all inout frames into one

-projectmax           - combines by MAX all inout frames into one

-projectmin           - combines by MIN all inout frames into one

-pyr-level            - extract an exact pyramidal level, not necessarily power-of-two sizes, ex: -pyr-level 3
    L - is a resolution level, L=0 is a first level, L=1 is a second, and so on    the exact scale of each level can be obtained using metadata

-raw                  - reads RAW image with w,h,c,d,p,e,t,interleaved ex: -raw 100,100,3,8,10,0,uint8,1
  w-width, h-height, c - channels, d-bits per channel, p-pages
  e-endianness(0-little,1-big), if in doubt choose 0
  t-pixel type: int8|uint8|int16|uint16|int32|uint32|float|double, if in doubt choose uint8
  interleaved - (0-planar or RRRGGGBBB, 1-interleaved or RGBRGBRGB)

-rawmeta              - print image's raw meta-data in one huge pile

-rearrange3d          - Re-arranges dimensions of a 3D image, ex: -rearrange3d xzy
  should be followed by comma and [xzy|yzx]
    xzy - rearranges XYZ -> XZY
    yzx - rearranges XYZ -> YZX


-reg-points           - Defines quality for image alignment in number of starting points, ex: -reg-points 200
  Suggested range is in between 32 and 512, more points slow down the processing


-regiongrow           - Region growing for label image, ex: -regiongrow 200,4
    maximum region size in pixels
    neighbor connectivity (4 - croiss or 8 - square)


-remap                - Changes order and number of channels in the output, channel numbers are separated by comma (0 means empty channel), ex: -remap 1,2,3

-res-level            - extract a specified power-of-two pyramidal level, ex: -res-level 4
    L - is a resolution level, L=0 is native resolution, L=1 is 2X smaller, L=2 is 4X smaller, and so on

-resample             - Is the same as resize, the difference is resample is brute force and resize uses image pyramid for speed

-resize               - should be followed by: width and height of the new image, ex: -resize 640,480
  if one of the numbers is ommited or 0, it will be computed preserving aspect ratio, ex: -resize 640,,NN
  if followed by comma and [NN|BL|BC|...] allowes to choose interpolation method, ex: -resize 640,480,NN
    NN - Nearest neighbor (default)
    BL - Bilinear
    BC - Bicubic
    lanczos
    bessel
    point
    box
    hermite
    hanning
    hamming
    blackman
    gaussian
    quadratic
    catrom
    mitchell
    sinc
    blackmanbessel
    blackmansinc
  if followed by comma [AR|MX|NOUP], the sizes will be limited:
    AR - resize preserving aspect ratio, ex: 640,640,NN,AR
    MX|NOUP - size will be used as maximum bounding box, preserving aspect ratio and not upsampling, ex: 640,640,NN,MX

-resize3d             - performs 3D interpolation on an input image, ex: -resize3d 640,480,16
  if one of the W/H numbers is ommited or 0, it will be computed preserving aspect ratio, ex: -resize3d 640,,16,NN
  if followed by comma and [NN|BL|BC] allowes to choose interpolation method, ex: -resize3d 640,480,16,BC
    NN - Nearest neighbor (default)
    TL - Trilinear
    TC - Tricubic
  if followed by comma AR, the size will be used as maximum bounding box to resize preserving aspect ratio, ex: 640,640,16,BC,AR

-resolution           - redefines resolution for any incoming image with: x,y,z,t where x,y,z are in microns and t in seconds  ex: -resolution 0.012,0.012,1,0

-roi                  - regions of interest, should be followed by: x1,y1,x2,y2 that defines ROI rectangle, ex: -roi 10,10,100,100
  if x1 or y1 are ommited they will be set to 0, ex: -roi ,,100,100 means 0,0,100,100
  if x2 or y2 are ommited they will be set to image size, ex: -roi 10,10,, means 10,10,width-1,height-1
  if more than one region of interest is desired, specify separated by ';', ex: -roi 10,10,100,100;20,20,120,120
  in case of multiple regions, specify a template for output file creation with following variables, ex: -template {output_filename}_{x1}.{y1}.{x2}.{y2}.tif

-rotate               - rotates the image by deg degrees, only accepted valueas now are: 90, -90, 180, guess
guess will extract suggested rotation from EXIF

-sampleframes         - samples for reading every Nth frame (useful for videos), ex: -sampleframes 5

-scale                - extract a specified scale from a multi-resolution image, ex: -scale 0.125
    when specified it will override -res-level and -pyr-level

-single               - disables multi-page creation mode

-skip-frames-leading  - skip N initial frames of a sequence, ex: -skip-frames-leading 5

-skip-frames-trailing - skip N final frames of a sequence, ex: -skip-frames-trailing 5

-slice                - N-D slices to extract, should be followed by dimension positions separated by comma, ex: -slice z:2,fov:42,spectrum:200;220;333
  The available dimensions will depend on the image, the list includes: z,t,serie,fov,rotation,scene,illumination,phase,view,item,spectrum,measure

-speed                - specify read-cache file location used to speed reading of supported formats, ex: -speed "my_cache.speed"

Speed files may contain pre-parse access trees or other info that helps in faster access of interanl data


-stretch              - stretch data to it's full range

-superpixels          - Segments image using SLIC superpixel method, takes region size and regularization, ex: -superpixels 16,0.2[,0.7]
    region size is in pixels
    regularization - [0-1], where 0 means shape is least regular    minimum size - [0-1], is optional and defines the minimum region size computed from region size, 1 will ensure minimum size at region size. Default value is 0.7

-supported            - prints yes/no if the file can be decoded

-t                    - output format

-template             - Define a template for file names, ex: -template {output_filename}_{n}.tif
  templates specify variables inside {} blocks, available variables vary for different processing

-textureatlas         - Produces a texture atlas 2D image for 3D input images

-texturegrid          - Creates custom texture atlas with: rows,cols ex: -texturegrid 5,7


-threshold            - thresholds the image, ex: -threshold 120,upper
  value is followed by comma and [lower|upper|both] to selet thresholding method
    lower - sets pixels below the threshold to lowest possible value
    upper - sets pixels above or equal to the threshold to highest possible value
    both - sets pixels below the threshold to lowest possible value and above or equal to highest


-tile                 - tile the image and store tiles in the output directory, ex: -tile 256
  argument defines the size of the tiles in pixels
  tiles will be created based on the output file name with inserted L, X, Y, where    L - is a resolution level, L=0 is native resolution, L=1 is 2x smaller, and so on    X and Y - are tile indices in X and Y, where the first tile is 0,0, second in X is: 1,0 and so on  ex: '-o my_file.jpg' will produce files: 'my_file_LLL_XXX_YYY.jpg'

  Providing more arguments will instruct extraction of embedded tiles with -tile SZ,XID,YID,L ex: -tile 256,2,4,3
    SZ: defines the size of the tile in pixels
    XID and YID - are tile indices in X and Y, where the first tile is 0,0, second in X is: 1,0 and so on    L - is a resolution level, L=0 is native resolution, L=1 is 2x smaller, and so on

-tile-roi             - region of interest, should be followed by: x1,y1,x2,y2[,L][,S] that defines ROI rectangle, ex: -tile-roi 10,10,100,100,0
the difference from -roi is in how the image is loaded, in this case if operating on a tiled image
only the required sub-region will be loaded, similar to tile interface but with arbitrary position
this means that all enhancements will be local to the ROI and glogal histogram will be needed
L is the pyramid level, 0=100%, 1=50%, 2=25%, etc...
S is the scale, 1.0=100%, 0.5=50%, 0.25=25%, etc..., ex: -tile-roi 10,10,100,100,0.5


-transform            - transforms input image, ex: -transform fft
    chebyshev - outputs a transformed image in double precision
    fft - outputs a transformed image in double precision
    radon - outputs a transformed image in double precision
    wavelet - outputs a transformed image in double precision

-transform_color      - transforms input image 3 channel image in color space, ex: -transform_color rgb2hsv
    hsv2rgb - converts HSV -> RGB
    rgb2hsv - converts RGB -> HSV
    rgb2wndchrm - converts RGB -> WndChrmColor
    rgb2xyz - converts RGB -> XYZ
    rgb2lab - converts RGB -> Lab
    rgb2ycbcr - converts RGB -> YcBcR (for full [0..255] range)
    ycbcr2rgb - converts YcBcR -> RGB  (for full [0..255] range)
    rgb2ycbcrClamp - converts RGB -> YcBcR (for clamped range)
    ycbcrClamp2rgb - converts YcBcR -> RGB (for clamped range)
    rgb2ycbcrHDTV - converts RGB -> YcBcR (for HDTV range)
    ycbcrHDTV2rgb - converts YcBcR -> RGB (for HDTV range)
    rgb2cmyk - converts RGB -> CMYK (for full [0..255] range)
    cmyk2rgb - converts CMYK -> RGB (for full [0..255] range)


-unmix                - unmixes input image channles into uncorrelated channels according to passed weights and normalizer matrices, ex: -unmix w1,w2,w3,...,wX[;n1,n2,n3,...,nY[;32]]
The matrix must be normalized and inverted prior to the operation.
the weights matrix must be a square matrix equal to the number of channels in the image.
normalizers if given must be equal to the number of channels in the image
unmixing operation produces output channels weighted from all the input channels like this:
[U] = [V] * [W] * [N]
V - original values
W - square weights matrix
N - normalizer array


-v                    - prints version

-verbose              - output information about the processing progress, ex: -verbose
  verbose allows argument that defines the amount of info, currently: 0, 1 and 2
  where: 1 is the light info output, 2 is full output
```
