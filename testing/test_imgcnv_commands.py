#!/usr/bin/python
__author__    = "Dmitry Fedorov"
__copyright__ = "ViQi Inc"

from bim_test_base import setup_tests, assert_image_commands

class TestBioImageConvertCommands():

    @classmethod
    def setup_class(cls):
        setup_tests()

    @classmethod
    def teardown_class(cls):
        pass

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    def test_imgcnv_commands_rotate_jpeg_guess (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_x'] = 3264
        meta_test['image_num_y'] = 2448
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-rotate', 'guess'], "IMG_0562.JPG", meta_test )

    def test_imgcnv_commands_rotate_jpeg_guess_2 (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_x'] = 2448
        meta_test['image_num_y'] = 3264
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-rotate', 'guess'], "IMG_0593.JPG", meta_test )

    def test_imgcnv_commands_rotate_jpeg_guess_3 (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_x'] = 3264
        meta_test['image_num_y'] = 2448
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-rotate', 'guess'], "IMG_1003.JPG", meta_test )

    def test_imgcnv_commands_superpixels_8bit (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 768
        meta_test['image_pixel_depth'] = 32
        assert_image_commands( ['-superpixels', 16], 'flowers_8bit_gray.png', meta_test )

    def test_imgcnv_commands_superpixels_24bit (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 768
        meta_test['image_pixel_depth'] = 32
        assert_image_commands( ['-superpixels', 16], 'flowers_24bit_nointr.png', meta_test )

    def test_imgcnv_commands_resize_3d (self):
        meta_test = {}
        meta_test['image_num_c'] = 2
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_z'] = 7
        meta_test['image_pixel_depth'] = 16
        meta_test['pixel_resolution_x'] = 0.41432
        meta_test['pixel_resolution_y'] = 0.41432
        meta_test['pixel_resolution_z'] = 1.85714
        meta_test['channels/channel:0/name'] = 'FITC'
        meta_test['channels/channel:1/name'] = 'Cy3'
        assert_image_commands( ['-t', 'ome-tiff', '-resize3d', '256,0,0,TC'], '161pkcvampz1Live2-17-2004_11-57-21_AM.tif', meta_test )

    def test_imgcnv_commands_rearrange_3d_xzy (self):
        meta_test = {}
        meta_test['image_num_p'] = 256
        meta_test['image_num_c'] = 2
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 13
        meta_test['image_num_z'] = 256
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 16
        meta_test['pixel_resolution_x'] = 0.20716
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_z'] = 0.41432
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        # meta_test['pixel_resolution_unit_t'] = 'seconds' # removed units for 0 sized dims
        meta_test['channels/channel:0/name'] = 'FITC'
        meta_test['channels/channel:1/name'] = 'Cy3'
        assert_image_commands( ['-t', 'ome-tiff', '-rearrange3d', 'xzy'], 'cells.ome.tif', meta_test )

    def test_imgcnv_commands_rearrange_3d_yzx (self):
        meta_test = {}
        meta_test['image_num_p'] = 512
        meta_test['image_num_c'] = 2
        meta_test['image_num_x'] = 13
        meta_test['image_num_y'] = 256
        meta_test['image_num_z'] = 512
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 16
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 0.41432
        meta_test['pixel_resolution_z'] = 0.20716
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        # meta_test['pixel_resolution_unit_t'] = 'seconds' # removed units for 0 sized dims
        meta_test['channels/channel:0/name'] = 'FITC'
        meta_test['channels/channel:1/name'] = 'Cy3'
        assert_image_commands( ['-t', 'ome-tiff', '-rearrange3d', 'yzx'], 'cells.ome.tif', meta_test )
