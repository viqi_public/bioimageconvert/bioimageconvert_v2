#!/usr/bin/python
__author__    = "Dmitry Fedorov"
__copyright__ = "ViQi Inc"

from bim_test_base import setup_tests, assert_image_transforms

class TestBioImageConvertTransforms():

    @classmethod
    def setup_class(cls):
        setup_tests()

    @classmethod
    def teardown_class(cls):
        pass

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    def test_imgcnv_transforms_edge (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 3
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 768
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        assert_image_transforms( '-filter', 'edge', "flowers_24bit_nointr.png", meta_test )

    def test_imgcnv_transforms_wndchrmcolor (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 768
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        assert_image_transforms( '-filter', 'wndchrmcolor', "flowers_24bit_nointr.png", meta_test )

    def test_imgcnv_transforms_rgb2hsv (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 3
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 768
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        assert_image_transforms( '-transform_color', 'rgb2hsv', "flowers_24bit_nointr.png", meta_test )

    def test_imgcnv_transforms_hsv2rgb (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 3
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 768
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        assert_image_transforms( '-transform_color', 'hsv2rgb', 'flowers_24bit_hsv.tif', meta_test )

    def test_imgcnv_transforms_chebyshev (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['image_num_x'] = 768
        meta_test['image_num_y'] = 768
        meta_test['image_pixel_depth'] = 64
        meta_test['image_pixel_format'] = 'floating point'
        assert_image_transforms( '-transform', 'chebyshev', 'flowers_8bit_gray.png', meta_test )

    def test_imgcnv_transforms_fft (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 768
        meta_test['image_pixel_depth'] = 64
        meta_test['image_pixel_format'] = 'floating point'
        assert_image_transforms( '-transform', 'fft', 'flowers_8bit_gray.png', meta_test )

    def test_imgcnv_transforms_wavelet (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['image_num_x'] = 1032
        meta_test['image_num_y'] = 776
        meta_test['image_pixel_depth'] = 64
        meta_test['image_pixel_format'] = 'floating point'
        assert_image_transforms( '-transform', 'wavelet', 'flowers_8bit_gray.png', meta_test )

    def test_imgcnv_transforms_radon (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['image_num_x'] = 180
        meta_test['image_num_y'] = 1283
        meta_test['image_pixel_depth'] = 64
        meta_test['image_pixel_format'] = 'floating point'
        assert_image_transforms( '-transform', 'radon', 'flowers_8bit_gray.png', meta_test )
