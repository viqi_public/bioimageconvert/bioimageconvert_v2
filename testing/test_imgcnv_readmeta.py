#!/usr/bin/python
__author__    = "Dmitry Fedorov"
__copyright__ = "ViQi Inc"

from bim_test_base import setup_tests, assert_metadata_read

mode = []

class TestBioImageConvertReadMeta():

    @classmethod
    def setup_class(cls):
        setup_tests()

    @classmethod
    def teardown_class(cls):
        pass

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    def test_imgcnv_readmeta_jpeg_exif (self):
        meta_test = {}
        meta_test['Exif/GPSInfo/GPSLatitudeRef'] = 'North'
        meta_test['Geo/Coordinates/center'] = [34.437833,-119.711833,14.000000]
        assert_metadata_read( "JPEG EXIF", "IMG_0488.JPG", meta_test )

    def test_imgcnv_readmeta_jpeg_iptc (self):
        meta_test = {}
        meta_test['Iptc/Application2/City']    = 'Santa Barbara'
        meta_test['Iptc/Application2/ProvinceState'] = 'CA'
        assert_metadata_read( "JPEG IPTC", "IMG_0184.JPG", meta_test )

    def test_imgcnv_readmeta_png_rgba (self):
        meta_test = {}
        meta_test['image_mode'] = 'RGBA'
        meta_test['ColorProfile/color_space'] = 'RGB'
        meta_test['ColorProfile/description'] = 'sRGB IEC61966-2.1'
        meta_test['ColorProfile/size'] = '3144'
        meta_test['channels/channel:0/color'] = [1.00,0.00,0.00]
        meta_test['channels/channel:1/color'] = [0.00,1.00,0.00]
        meta_test['channels/channel:2/color'] = [0.00,0.00,1.00]
        meta_test['channels/channel:3/color'] = [0.00,0.00,0.00]
        meta_test['channels/channel:0/name'] = 'Red'
        meta_test['channels/channel:1/name'] = 'Green'
        meta_test['channels/channel:2/name'] = 'Blue'
        meta_test['channels/channel:3/name'] = 'Alpha'
        assert_metadata_read( "PNG RGBA", "IMG_0184_RGBA.png", meta_test )

    def test_imgcnv_readmeta_oib_v2 (self):
        meta_test = {}
        meta_test['document/acquisition_date'] = '2010-06-04T08:26:06'
        meta_test['pixel_resolution_x'] = 0.206798
        meta_test['pixel_resolution_y'] = 0.206798
        meta_test['pixel_resolution_z'] = 0.428571
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        meta_test['channels/channel:0/name'] = 'Cy2'
        meta_test['channels/channel:1/name'] = 'Cy3'
        meta_test['channels/channel:2/name'] = 'Cy5'
        meta_test['channels/channel:0/color'] = [0.00,1.00,0.00]
        meta_test['channels/channel:1/color'] = [1.00,0.00,0.00]
        meta_test['channels/channel:2/color'] = [0.00,0.00,1.00]
        assert_metadata_read( "OIB.2", "CSR 4 mo COX g M cone r PNA b z1.oib", meta_test )

    def test_imgcnv_readmeta_oib_large (self):
        meta_test = {}
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 1024
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 528
        meta_test['pixel_resolution_x'] = 1.24079
        meta_test['pixel_resolution_y'] = 1.24079
        meta_test['pixel_resolution_z'] = 1.23765
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        meta_test['document/acquisition_date'] = '2012-01-25T15:35:02'
        meta_test['channels/channel:0/name'] = 'None'
        meta_test['channels/channel:0/color'] = [1.00,1.00,1.00]
        assert_metadata_read( "OIB.Large", "112811B_5.oib", meta_test )

    def test_imgcnv_readmeta_oib_strange_z (self):
        # reading metadata from OIB with strange Z planes and only actual one Z image
        meta_test = {}
        meta_test['image_num_c'] = 4
        meta_test['image_num_p'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_x'] = 640
        meta_test['image_num_y'] = 640
        meta_test['image_pixel_depth'] = 16
        meta_test['document/acquisition_date'] = '2010-11-19T14:32:33'
        meta_test['pixel_resolution_x'] = 0.496223
        meta_test['pixel_resolution_y'] = 0.496223
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['channels/channel:0/name'] = 'Alexa Fluor 405'
        meta_test['channels/channel:1/name'] = 'Cy2'
        meta_test['channels/channel:2/name'] = 'Cy3'
        meta_test['channels/channel:3/name'] = 'Cy5'
        meta_test['channels/channel:0/color'] = [1.00,1.00,1.00]
        meta_test['channels/channel:1/color'] = [0.00,1.00,0.00]
        meta_test['channels/channel:2/color'] = [1.00,0.00,0.00]
        meta_test['channels/channel:3/color'] = [0.00,0.00,1.00]
        assert_metadata_read( "OIB 2.0 One plane", "Retina 4 top.oib", meta_test )

    def test_imgcnv_readmeta_nanoscope (self):
        meta_test = {}
        meta_test['image_num_c'] = 2
        meta_test['image_num_p'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'signed integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['format'] = 'NANOSCOPE'
        meta_test['document/acquisition_date'] = '2004-12-17T14:27:29'
        meta_test['pixel_resolution_x'] = 0.0585938
        meta_test['pixel_resolution_y'] = 0.0585938
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['channels/channel:0/color'] = [1.00,0.00,0.00]
        meta_test['channels/channel:0/name'] = 'Height'
        meta_test['channels/channel:1/color'] = [0.00,1.00,0.00]
        meta_test['channels/channel:1/name'] = 'Amplitude'
        assert_metadata_read( "NANOSCOPE", "AXONEME.002", meta_test )

    def test_imgcnv_readmeta_zvi (self):
        meta_test = {}
        meta_test['pixel_resolution_x'] = 0.157153
        meta_test['pixel_resolution_y'] = 0.157153
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['channels/channel:0/name'] = 'Cy5'
        meta_test['channels/channel:1/name'] = 'TRITC'
        meta_test['channels/channel:2/name'] = 'FITC'
        meta_test['channels/channel:3/name'] = 'DAPI'
        meta_test['document/acquisition_date'] = '2006-06-22T15:27:13'
        meta_test['objectives/objective:0/name'] = 'Plan Neofluar 40x/1.30 Oil Ph3 (DIC III) (440451)'
        meta_test['objectives/objective:0/magnification'] = 40.0
        meta_test['objectives/objective:0/numerical_aperture'] = 1.3
        assert_metadata_read( "Zeiss ZVI", "23D3HA-cy3 psd-gfp-488 Homer-647 DIV 14 - 3.zvi", meta_test )

    def test_imgcnv_readmeta_zvi_2 (self):
        meta_test = {}
        meta_test['image_num_c'] = 2
        meta_test['image_num_p'] = 14
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 14
        meta_test['image_pixel_depth'] = 16
        meta_test['image_num_x'] = 1388
        meta_test['image_num_y'] = 1040
        meta_test['document/acquisition_date'] = '2010-01-06T11:53:37'
        meta_test['pixel_resolution_x'] = 0.102381
        meta_test['pixel_resolution_y'] = 0.102381
        meta_test['pixel_resolution_z'] = 0.32
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        meta_test['channels/channel:0/name'] = 'DsRed'
        meta_test['channels/channel:1/name'] = 'eGFP'
        meta_test['channels/channel:0/color'] = [1.00,0.00,0.00]
        meta_test['channels/channel:1/color'] = [0.00,1.00,0.00]
        meta_test['objectives/objective:0/name'] = 'C-Apochromat 63x/1.20 W Korr UV VIS IR'
        meta_test['objectives/objective:0/magnification'] = 63.0
        meta_test['objectives/objective:0/numerical_aperture'] = 1.2
        assert_metadata_read( "Zeiss ZVI", "0022.zvi", meta_test )

    def test_imgcnv_readmeta_stk (self):
        meta_test = {}
        meta_test['image_num_z'] = 105
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['image_pixel_depth'] = 16
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['document/acquisition_date'] = '2008-03-27T11:15:51'
        meta_test['channels/channel:0/name'] = 'RGB_488nm'
        meta_test['channels/channel:0/color'] = [1.00,1.00,1.00]
        meta_test['channels/channel:0/exposure'] = 100
        meta_test['channels/channel:0/exposure_units'] = 'ms'
        meta_test['objectives/objective:0/magnification'] = 25
        meta_test['pixel_resolution_x'] = 0.43
        meta_test['pixel_resolution_y'] = 0.43
        meta_test['pixel_resolution_z'] = 0.488
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        meta_test['coordinates/positions/sensor'] = '0.000000,0.000000;1.000000,0.000000;1.930951,-0.127155;0.000000,0.000000;10.045736,0.019531;0.000255,0.000216;3.000000,0.000001;0.000001,0.000001;0.000000,0.000000;0.000000,0.000000;0.085938,0.187500;0.000000,0.000000;0.200000,0.000003;0.000004,0.173190;0.000000,0.000000;0.000004,0.000005;0.000005,0.000005;0.000000,0.000000;0.000000,0.000000;0.000006,0.000008;0.000008,0.000008;0.000000,0.000000;0.000009,0.000010;5.876465,0.000010;0.000010,0.200043;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000'
        #meta_test['coordinates/positions/stage'] = '3712.200000,-2970.340000;0.000000,0.000000;1.000000,0.000000;1.930951,-0.127155;0.000000,0.000000;10.045736,0.019531;0.000255,0.000216;3.000000,0.000001;0.000001,0.000001;0.000000,0.000000;0.000000,0.000000;0.085938,0.187500;0.000000,0.000000;0.200000,0.000003;0.000004,0.173190;0.000000,0.000000;0.000004,0.000005;0.000005,0.000005;0.000000,0.000000;0.000000,0.000000;0.000006,0.000008;0.000008,0.000008;0.000000,0.000000;0.000009,0.000010;5.876465,0.000010;0.000010,0.200043;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000;-0.732674,0.200000'
        #meta_test['coordinates/positions/z'] = '25.252000,0.000000,0.000000,0.940000,0.000000,0.000255,0.000216,3.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.035294,0.000000,0.085938,0.187500,0.000000,0.000214,0.200000,0.000000,0.000000,0.173077,4.750000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000001,0.000001,0.000001,0.000001,0.000001,0.000001,0.000000,0.000001,0.000001,5.875000,0.000001,0.000001,-2841.399957,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000,303.425972,-2841.400000'
        meta_test['coordinates/positions/stage'] = '3712.200000,-2970.340000,25.252000;0.000000,0.000000,0.000000;1.000000,0.000000,0.000000;1.930951,-0.127155,0.940000;0.000000,0.000000,0.000000;10.045736,0.019531,0.000255;0.000255,0.000216,0.000216;3.000000,0.000001,3.000000;0.000001,0.000001,0.000000;0.000000,0.000000,0.000000;0.000000,0.000000,0.000000;0.085938,0.187500,0.000000;0.000000,0.000000,0.000000;0.200000,0.000003,0.035294;0.000004,0.173190,0.000000;0.000000,0.000000,0.085938;0.000004,0.000005,0.187500;0.000005,0.000005,0.000000;0.000000,0.000000,0.000214;0.000000,0.000000,0.200000;0.000006,0.000008,0.000000;0.000008,0.000008,0.000000;0.000000,0.000000,0.173077;0.000009,0.000010,4.750000;5.876465,0.000010,0.000000;0.000010,0.200043,0.000000;-0.732674,0.200000,0.000000;-0.732674,0.200000,0.000000;-0.732674,0.200000,0.000000;-0.732674,0.200000,0.000000;-0.732674,0.200000,0.000000;-0.732674,0.200000,0.000000;-0.732674,0.200000,0.000001;-0.732674,0.200000,0.000001;-0.732674,0.200000,0.000001;-0.732674,0.200000,0.000001;-0.732674,0.200000,0.000001;-0.732674,0.200000,0.000001;-0.732674,0.200000,0.000000;-0.732674,0.200000,0.000001;-0.732674,0.200000,0.000001;-0.732674,0.200000,5.875000;-0.732674,0.200000,0.000001;-0.732674,0.200000,0.000001;-0.732674,0.200000,-2841.399957;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000;-0.732674,0.200000,303.425972;-0.732674,0.200000,-2841.400000'
        assert_metadata_read( "STK", "sxn3_w1RGB-488nm_s1.stk", meta_test )

    def test_imgcnv_readmeta_andor (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_p'] = 12
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['document/acquisition_date'] = '2010-06-08T10:10:36'
        meta_test['pixel_resolution_x'] = 0.479042
        meta_test['pixel_resolution_y'] = 0.479042
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['coordinates/positions/stage'] = '923.497006,-1660.502994,49.990000;1156.502994,-1660.502994,49.990000;923.497006,-1427.497006,49.990000;1156.502994,-1427.497006,49.990000;1792.497006,-2023.502994,49.990000;2025.502994,-2023.502994,49.990000;1792.497006,-1790.497006,49.990000;2025.502994,-1790.497006,49.990000;1087.497006,-2473.502994,49.990000;1320.502994,-2473.502994,49.990000;1087.497006,-2240.497006,49.990000;1320.502994,-2240.497006,49.990000'
        assert_metadata_read( "Andor", "MF Mon 2x2.tif", meta_test )

    def test_imgcnv_readmeta_ome_tiff_micromanager (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 3
        meta_test['image_num_z'] = 3
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_z'] = 1
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        meta_test['MicroManager/Objective-Label'] = 'Nikon 10X S Fluor'
        meta_test['MicroManager/Camera-CameraName'] = 'DemoCamera-MultiMode'

        meta_test['channels/channel:0/name'] = 'Cy5'
        meta_test['channels/channel:0/fluor'] = 'Cy5'
        meta_test['channels/channel:0/color'] = [1.00,1.00,0.00]

        meta_test['channels/channel:1/name'] = 'DAPI'
        meta_test['channels/channel:1/fluor'] = 'DAPI'
        meta_test['channels/channel:1/color'] = [1.00,0.00,0.80]

        meta_test['channels/channel:2/name'] = 'FITC'
        meta_test['channels/channel:2/fluor'] = 'FITC'
        meta_test['channels/channel:2/color'] = [1.00,1.00,0.00]

        assert_metadata_read( "MicroManager OME-TIFF", "Untitled_MMImages_Pos0.ome.tif", meta_test )

    def test_imgcnv_readmeta_dcraw_dng (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 2616
        meta_test['image_num_y'] = 1960
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 16
        if 'no_exiv' not in mode:
            meta_test['Exif/Image/Make'] = 'Canon'
            meta_test['Exif/Image/Model'] = 'Canon PowerShot G5'
            meta_test['Exif/Photo/ExposureTime'] = '1/160 s'
            meta_test['Exif/Photo/FNumber'] = 'F4'
            meta_test['Exif/Photo/FocalLength'] = '7.2 mm'
            meta_test['Exif/Photo/ISOSpeedRatings'] = '50'
        assert_metadata_read( "Adobe DNG", "CRW_0136_COMPR.dng", meta_test )

    def test_imgcnv_readmeta_dcraw_cr2 (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 3522
        meta_test['image_num_y'] = 2348
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 16
        if 'no_exiv' not in mode:
            meta_test['Exif/Photo/ISOSpeedRatings'] = '100'
            meta_test['Exif/Photo/ExposureTime'] = '0.3 s'
            meta_test['Exif/Photo/FNumber'] = 'F5.6'
        assert_metadata_read( "Canon CR2", "IMG_0040.CR2", meta_test )

    def test_imgcnv_readmeta_dcraw_cr2_5dsr (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 8736
        meta_test['image_num_y'] = 5856
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 16
        meta_test['format'] = 'CANON-RAW'
        meta_test['image_mode'] = 'RGB'
        meta_test['DCRAW/aperture'] = '10.000000'
        meta_test['DCRAW/focal_length'] = 13
        meta_test['DCRAW/iso_speed'] = 100
        meta_test['DCRAW/make'] = 'Canon'
        meta_test['DCRAW/model'] = 'EOS 5DS R'
        meta_test['DCRAW/shutter'] = 0.005
        meta_test['DCRAW/aperture'] = 10
        if 'no_exiv' not in mode:
            meta_test['document/acquisition_date'] = '2016-05-24T15:16:35'
            meta_test['Exif/Photo/Flash'] = 'No, compulsory'
            meta_test['Exif/Photo/FocalLength'] = '13.0 mm'
            meta_test['Exif/Photo/FNumber'] = 'F10'
        assert_metadata_read( "Canon 5DSr CR2", "6J0A3548.CR2", meta_test )

    def test_imgcnv_readmeta_dcraw_crw (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 2616
        meta_test['image_num_y'] = 1960
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 16
        if 'no_exiv' not in mode:
            meta_test['Exif/Image/Make'] = 'Canon'
            meta_test['Exif/Image/Model'] = 'Canon PowerShot G5'
            meta_test['Exif/Photo/ExposureTime'] = '1/156 s'
            meta_test['Exif/Photo/FNumber'] = 'F4'
        assert_metadata_read( "Canon CRW", "CRW_0136.CRW", meta_test )

    def test_imgcnv_readmeta_dcraw_cr3 (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        # meta_test['image_num_x'] = 8191 # CR3 support in LibRAW changes size with changes to interpolation filters, don't check for now
        # meta_test['image_num_y'] = 5463
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 16
        meta_test['format'] = 'CANON-RAW'
        meta_test['image_mode'] = 'RGB'
        meta_test['DCRAW/aperture'] = 7.1
        meta_test['DCRAW/focal_length'] = 14
        meta_test['DCRAW/iso_speed'] = 125
        meta_test['DCRAW/make'] = 'Canon'
        meta_test['DCRAW/model'] = 'EOS R5'
        meta_test['DCRAW/shutter'] = 0.01

        assert_metadata_read( "Canon R5 CR3", "DR3A1199.CR3", meta_test )

    def test_imgcnv_readmeta_dcraw_pentax (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 3008
        meta_test['image_num_y'] = 2000
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 8
        if 'no_exiv' not in mode:
            meta_test['Exif/Photo/ExposureTime'] = '1/125 s'
            meta_test['Exif/Photo/FNumber'] = 'F8'
            meta_test['Exif/Photo/ISOSpeedRatings'] = 200
        assert_metadata_read( "Pentax", "PENTAX_IMGP1618.JPG", meta_test )

    def test_imgcnv_readmeta_dcraw_minolta (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 3016
        meta_test['image_num_y'] = 2008
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 16
        if 'no_exiv' not in mode:
            meta_test['Exif/Photo/ExposureTime'] = '1/80 s'
            meta_test['Exif/Photo/FNumber'] = 'F20'
            meta_test['Exif/Photo/ISOSpeedRatings'] = 100
        assert_metadata_read( "Minolta", "PICT1694.MRW", meta_test )

    def test_imgcnv_readmeta_dcraw_nikon_nef (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 2576
        meta_test['image_num_y'] = 1924
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 16
        if 'no_exiv' not in mode:
            meta_test['Exif/Photo/ExposureTime'] = '0.769231 s'
            meta_test['Exif/Photo/FNumber'] = 'F4.2'
        assert_metadata_read( "Nikon", "DSCN0041.NEF", meta_test )

    def test_imgcnv_readmeta_dcraw_olympus_orf (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 3088
        meta_test['image_num_y'] = 2310
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 16
        if 'no_exiv' not in mode:
            meta_test['Exif/Photo/ExposureTime'] = '1/30 s'
            meta_test['Exif/Photo/FNumber'] = 'F2.8'
            meta_test['Exif/Photo/ISOSpeedRatings'] = 125
        assert_metadata_read( "Olympus", "P1110010.ORF", meta_test )

    def test_imgcnv_readmeta_geotiff (self):
        meta_test = {}
        meta_test['image_num_x'] = 1281
        meta_test['image_num_y'] = 1037
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 8
        meta_test['Geo/Tags/ModelPixelScaleTag'] = '0.179859484777536,0.179826422372659,0'
        meta_test['Geo/Tags/ModelTiepointTag'] = '0,0,0;719865.328538339,9859359.12604843,0'
        meta_test['Geo/Model/projection'] = '16136 (UTM zone 36S)'
        meta_test['Geo/Model/proj4_definition'] = '+proj=tmerc +lat_0=0.000000000 +lon_0=33.000000000 +k=0.999600 +x_0=500000.000 +y_0=10000000.000 +ellps=WGS84 +units=m'
        meta_test['Geo/Coordinates/center'] = '-1.2725008,34.9769992'
        meta_test['Geo/Coordinates/center_model'] = '719980.529,9859265.886'
        meta_test['Geo/Coordinates/upper_left'] = '-1.2716586,34.9759636'
        meta_test['Geo/Coordinates/upper_left_model'] = '719865.329,9859359.126'
        assert_metadata_read( "GeoTIFF", "test4.tif", meta_test )

    def test_imgcnv_readmeta_jxr (self):
        meta_test = {}
        meta_test['image_num_x'] = 1200
        meta_test['image_num_y'] = 1650
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_metadata_read( "JPEG-XR", "219j_q050.jxr", meta_test )

    def test_imgcnv_readmeta_jpeg2000 (self):
        meta_test = {}
        meta_test['image_num_x'] = 1200
        meta_test['image_num_y'] = 1650
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_metadata_read( "JPEG-2000", "219j.jp2", meta_test )

    def test_imgcnv_readmeta_webp (self):
        meta_test = {}
        meta_test['image_num_x'] = 1200
        meta_test['image_num_y'] = 1650
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_metadata_read( "WEBP", "219j_q080.webp", meta_test )

    def test_imgcnv_readmeta_dicom_ct (self):
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format'] = 'DICOM'
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'signed integer'
        meta_test['pixel_resolution_x'] = 0.439453
        meta_test['pixel_resolution_y'] = 0.439453
        meta_test['pixel_resolution_z'] = 5.0
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        meta_test['DICOM/Rescale Intercept'] = -1024
        meta_test['DICOM/Rescale Slope'] = 1
        meta_test['DICOM/Rescale Type'] = 'HU'
        meta_test['DICOM/Scan Options'] = 'AXIAL MODE'
        meta_test['DICOM/Series Description'] = 'HEAD ST W/O'
        meta_test['DICOM/Window Center'] = 40
        meta_test['DICOM/Window Width'] = 80
        meta_test['DICOM/Patient\'s Age'] = '035Y'
        assert_metadata_read( "DICOM CT", "10", meta_test )

    def test_imgcnv_readmeta_dicom_mr (self):
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 16
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_z'] = 10.0
        meta_test['pixel_resolution_t'] = 69.47
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        meta_test['pixel_resolution_unit_t'] = 'seconds'
        meta_test['DICOM/Modality'] = 'MR'
        meta_test['DICOM/Study Description'] = 'MRI'
        assert_metadata_read( "DICOM MR", "MR-MONO2-8-16x-heart", meta_test )

    def test_imgcnv_readmeta_dicom_us (self):
        meta_test = {}
        meta_test['image_num_x'] = 128
        meta_test['image_num_y'] = 120
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 8
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 3
        meta_test['pixel_resolution_y'] = 4
        meta_test['pixel_resolution_t'] = 100
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_t'] = 'seconds'
        meta_test['DICOM/Modality'] = 'US'
        meta_test['DICOM/Study Description'] = 'Exercise Echocardiogram'
        assert_metadata_read( "DICOM EXECHO", "US-MONO2-8-8x-execho", meta_test )

    def test_imgcnv_readmeta_dicom_us_2 (self):
        meta_test = {}
        meta_test['image_num_x'] = 600
        meta_test['image_num_y'] = 430
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 10
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_t'] = 1
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_t'] = 'seconds'
        meta_test['DICOM/Modality'] = 'US'
        meta_test['DICOM/Study Description'] = 'Echocardiogram'
        assert_metadata_read( "DICOM ECHO", "US-PAL-8-10x-echo", meta_test )

    def test_imgcnv_readmeta_dicom_rf (self):
        meta_test = {}
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 1024
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['DICOM/Modality'] = 'RF'
        meta_test['DICOM/Patient\'s Sex'] = 'F'
        assert_metadata_read( "DICOM RF", "0015.DCM", meta_test )

    def test_imgcnv_readmeta_dicom_us_3 (self):
        meta_test = {}
        meta_test['image_num_x'] = 600
        meta_test['image_num_y'] = 430
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 11
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_t'] = 1
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_t'] = 'seconds'
        meta_test['DICOM/Modality'] = 'US'
        meta_test['DICOM/Patient\'s Sex'] = 'F'
        meta_test['DICOM/Study Description'] = 'Echocardiogram'
        assert_metadata_read( "DICOM US", "0020.DCM", meta_test )

    def test_imgcnv_readmeta_dicom_adni (self):
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'signed integer'
        meta_test['pixel_resolution_x'] = 1.01562
        meta_test['pixel_resolution_y'] = 1.01562
        meta_test['pixel_resolution_z'] = 5
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        meta_test['DICOM/Modality'] = 'MR'
        meta_test['DICOM/Patient\'s Sex'] = 'M'
        meta_test['DICOM/Study Description'] = 'ADNI RESEARCH STUDY'
        meta_test['DICOM/Slice Location'] = -46.90000153
        assert_metadata_read( "DICOM ADNI", "ADNI_002_S_0295_MR_3-plane_localizer__br_raw_20060418193538653_1_S13402_I13712.dcm", meta_test )

    def test_imgcnv_readmeta_dicom_mr_2 (self):
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 1.09375
        meta_test['pixel_resolution_y'] = 1.09375
        meta_test['pixel_resolution_z'] = 10
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        meta_test['DICOM/Modality'] = 'MR'
        meta_test['DICOM/Patient\'s Sex'] = 'F'
        meta_test['DICOM/Study Description'] = 'RaiGoe^standaard'
        meta_test['DICOM/Slice Location'] = 0
        assert_metadata_read( "DICOM MR", "BetSog_20040312_Goebel_C2-0001-0001-0001.dcm", meta_test )

    def test_imgcnv_readmeta_dicom_ct_2 (self):
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 0.361328
        meta_test['pixel_resolution_y'] = 0.361328
        meta_test['pixel_resolution_z'] = 0.75
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        meta_test['DICOM/Modality'] = 'CT'
        meta_test['DICOM/Window Center'] = '400\\\\700'
        meta_test['DICOM/Window Width'] = '2000\\\\4000'
        assert_metadata_read( "DICOM CT", "IM-0001-0001.dcm", meta_test )

    def test_imgcnv_readmeta_nifti (self):
        meta_test = {}
        meta_test['image_num_x'] = 64
        meta_test['image_num_y'] = 64
        meta_test['image_num_z'] = 21
        meta_test['image_num_t'] = 180
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'NIFTI'
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'signed integer'
        meta_test['pixel_resolution_x'] = 4
        meta_test['pixel_resolution_y'] = 4
        meta_test['pixel_resolution_z'] = 6
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        assert_metadata_read( "NIFTI functional", "filtered_func_data.nii", meta_test )

    def test_imgcnv_readmeta_nifti_qform (self):
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_z'] = 32
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'NIFTI'
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 0.439453
        meta_test['pixel_resolution_y'] = 0.439453
        meta_test['pixel_resolution_z'] = 5
        #meta_test['pixel_resolution_unit_x'] = 'mm' # not stored in the image
        #meta_test['pixel_resolution_unit_y'] = 'mm'
        #meta_test['pixel_resolution_unit_z'] = 'mm'
        #meta_test['NIFTI/affine_transform'] = '0.439453,0.000000,0.000000,-112.060516;0.000000,0.439453,0.000000,-112.060516;0.000000,0.000000,5.000000,-75.000000'
        meta_test['NIFTI/transform_qform_to_ijk'] = '2.275556,0.000000,0.000000,0.000000;0.000000,2.275556,0.000000,0.000000;0.000000,0.000000,0.200000,0.000000;0.000000,0.000000,0.000000,1.000000'
        meta_test['NIFTI/transform_qform_to_xyz'] = '0.439453,0.000000,0.000000,0.000000;0.000000,0.439453,0.000000,0.000000;0.000000,0.000000,5.000000,0.000000;0.000000,0.000000,0.000000,1.000000'
        meta_test['NIFTI/transform_sform_to_ijk'] = '2.275556,0.000000,0.000000,255.000000;0.000000,2.275556,0.000000,255.000000;0.000000,0.000000,0.200000,15.000000;0.000000,0.000000,0.000000,1.000000'
        meta_test['NIFTI/transform_sform_to_xyz'] = '0.439453,0.000000,0.000000,-112.060516;0.000000,0.439453,0.000000,-112.060516;0.000000,0.000000,5.000000,-75.000000;0.000000,0.000000,0.000000,1.000000'
        assert_metadata_read( "NIFTI transforms", "16_day1_1_patient_29C93FK6_1.nii", meta_test )

    def test_imgcnv_readmeta_nifti_xml (self):
        meta_test = {}
        meta_test['image_num_x'] = 64
        meta_test['image_num_y'] = 64
        meta_test['image_num_z'] = 35
        meta_test['image_num_t'] = 147
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'NIFTI'
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'signed integer'
        meta_test['pixel_resolution_x'] = 3.4375
        meta_test['pixel_resolution_y'] = 3.4375
        meta_test['pixel_resolution_z'] = 3.99942
        #meta_test['pixel_resolution_unit_x'] = 'mm' # not stored in the image
        #meta_test['pixel_resolution_unit_y'] = 'mm'
        #meta_test['pixel_resolution_unit_z'] = 'mm'
        meta_test['pixel_resolution_unit_t'] = 'ms'
        meta_test['NIFTI/quaternion'] = '0.000000,0.025615,0.999672;108.281250,103.739151,-83.445091'
        meta_test['XCEDE/study/series/acquisition_protocol/parameters/receivecoilname'] = 'HEAD'
        meta_test['XCEDE/study/series/id'] = 1
        meta_test['XCEDE/subject/id'] = 103
        meta_test['XCEDE/study/series/scanner/manufacturer'] = 'GE'
        meta_test['XCEDE/study/series/scanner/model'] = 'LX NVi 4T'
        assert_metadata_read( "NIFTI XCEDE", "newsirp_final_XML.nii", meta_test )

    def test_imgcnv_readmeta_nifti_gz (self):
        meta_test = {}
        meta_test['image_num_x'] = 260
        meta_test['image_num_y'] = 311
        meta_test['image_num_z'] = 260
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'NIFTI'
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['pixel_resolution_x'] = 0.7
        meta_test['pixel_resolution_y'] = 0.7
        meta_test['pixel_resolution_z'] = 0.7
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        #meta_test['NIFTI/affine_transform'] = '-0.700000,0.000000,0.000000,90.000000;0.000000,0.700000,0.000000,-126.000000;0.000000,0.000000,0.700000,-72.000000'
        meta_test['NIFTI/transform_qform_to_ijk'] = '-1.428571,0.000000,-0.000000,128.571431;0.000000,1.428571,0.000000,180.000003;-0.000000,-0.000000,1.428571,102.857145;0.000000,0.000000,0.000000,1.000000'
        meta_test['NIFTI/transform_qform_to_xyz'] = '-0.700000,0.000000,-0.000000,90.000000;0.000000,0.700000,-0.000000,-126.000000;0.000000,0.000000,0.700000,-72.000000;0.000000,0.000000,0.000000,1.000000'
        meta_test['NIFTI/transform_sform_to_ijk'] = '-1.428571,-0.000000,-0.000000,128.571431;-0.000000,1.428571,-0.000000,180.000003;-0.000000,-0.000000,1.428571,102.857145;0.000000,0.000000,0.000000,1.000000'
        meta_test['NIFTI/transform_sform_to_xyz'] = '-0.700000,0.000000,0.000000,90.000000;0.000000,0.700000,0.000000,-126.000000;0.000000,0.000000,0.700000,-72.000000;0.000000,0.000000,0.000000,1.000000'
        assert_metadata_read( "NIFTI GZ", "T1w.nii.gz", meta_test )

    def test_imgcnv_readmeta_jxr_exif (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 5472
        meta_test['image_num_y'] = 3648
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 16
        meta_test['ColorProfile/description'] = 'ProPhoto RGB'
        if 'no_exiv' not in mode:
            meta_test['Exif/Photo/ExposureTime'] = '1/400 s'
            meta_test['Exif/Photo/FNumber'] = 'F11'
            meta_test['Exif/Photo/ISOSpeedRatings'] = 100
            meta_test['Iptc/Application2/ProvinceState'] = 'California'
            meta_test['Xmp/photoshop/City'] = 'Santa Barbara'
            # meta_test['Exif/GPSInfo/GPSLatitude'] = '34 deg 29\' 7.53\"'
            meta_test['Geo/Coordinates/center'] = [34.485425,-119.692847,1118.411600]
        assert_metadata_read( "JPEG-XR", "IMG_1913_16bit_prophoto_q90.jxr", meta_test )

    def test_imgcnv_readmeta_jpeg2000_exif (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 5472
        meta_test['image_num_y'] = 3648
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 16

        meta_test['Xmp/photoshop/ICCProfile'] = 'ProPhoto RGB'
        meta_test['Xmp/exif/ExposureTime'] = '1/400'
        meta_test['Xmp/exif/FNumber'] = 'F11'
        meta_test['Xmp/exif/GPSAltitude'] = '11184116/10000'
        meta_test['Xmp/exif/GPSLatitude'] = '34,29.1255N'
        meta_test['Xmp/exif/GPSLongitude'] = '119,41.5708W'

        # meta_test['Exif/Photo/ExposureTime'] = '1/400 s'
        # meta_test['Exif/Photo/FNumber'] = 'F11'
        # meta_test['Exif/Photo/ISOSpeedRatings'] = '100'
        # meta_test['Iptc/Application2/ProvinceState'] = 'California'
        # meta_test['Exif/GPSInfo/GPSLatitude'] = '34deg 29.12550'
        assert_metadata_read( "JPEG-2000", "IMG_1913_16bit_prophoto_ts1024_q90.jp2", meta_test )

    def test_imgcnv_readmeta_webp_exif (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 5472
        meta_test['image_num_y'] = 3648
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 8

        meta_test['ColorProfile/description'] = 'ProPhoto RGB'
        meta_test['Geo/Coordinates/center'] = [34.485425,-119.692847,1118.411600]

        meta_test['Exif/Photo/ExposureTime'] = '1/400 s'
        meta_test['Exif/Photo/FNumber'] = 'F11'
        meta_test['Exif/Photo/ISOSpeedRatings'] = '100'
        # meta_test['Iptc/Application2/ProvinceState'] = 'California'

        # Exif/GPSInfo/GPSLatitude: 34 deg 29' 7.53%22
        # Exif/GPSInfo/GPSLatitudeRef: North
        # Exif/GPSInfo/GPSLongitude: 119 deg 41' 34.25%22
        # Exif/GPSInfo/GPSLongitudeRef: West

        assert_metadata_read( "WebP", "IMG_1913_prophoto_q90.webp", meta_test )

    def test_imgcnv_readmeta_mrc (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 5120
        meta_test['image_num_y'] = 3840
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['pixel_resolution_x'] = 2.51
        meta_test['pixel_resolution_y'] = 2.51
        meta_test['pixel_resolution_z'] = 1
        meta_test['pixel_resolution_unit_x'] = 'angstroms'
        meta_test['MRC/alpha'] = 90
        meta_test['MRC/amean'] = 3.30166
        meta_test['MRC/label_00'] = 'EMAN 9/26/2015 10:08'
        assert_metadata_read( "MRC", "20150917_05195_DNA-TET-25k-DE20_raw.region_000.sum-all_003-072.mrc", meta_test )

    def test_imgcnv_readmeta_mrc_2 (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 32
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'signed integer'
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_z'] = 1
        meta_test['pixel_resolution_unit_x'] = 'angstroms'
        meta_test['MRC/alpha'] = 90
        meta_test['MRC/amean'] = 83.7333
        assert_metadata_read( "MRC", "golgi.mrc", meta_test )

    def test_imgcnv_readmeta_mrc_3 (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 2048
        meta_test['image_num_y'] = 2048
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 2.35294e-007
        meta_test['pixel_resolution_y'] = 2.35294e-007
        #meta_test['pixel_resolution_z'] = 2.35294e-007
        meta_test['pixel_resolution_unit_x'] = 'meters'
        meta_test['pixel_resolution_unit_y'] = 'meters'
        meta_test['MRC/alpha'] = 0
        meta_test['FEI/magnification'] = 84
        meta_test['FEI/a_tilt'] = -6.6316e-005
        meta_test['FEI/x_stage'] = -0.000343477
        meta_test['FEI/y_stage'] = -0.000862657
        meta_test['FEI/z_stage'] = -0.000210128
        assert_metadata_read( "MRC", "Tile_19491580_0_1.mrc", meta_test )

    def test_imgcnv_readmeta_mrc_rec (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 78
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 572
        meta_test['image_num_y'] = 378
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'signed integer'
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_z'] = 1
        meta_test['pixel_resolution_unit_x'] = 'angstroms'
        meta_test['MRC/alpha'] = 90
        meta_test['MRC/label_00'] = 'Clip: 3D FFT                                            22-Aug-00  11:46:25'
        assert_metadata_read( "MRC", "dual.rec", meta_test )

    def test_imgcnv_readmeta_mrc_4 (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 3838
        meta_test['image_num_y'] = 3710
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        #meta_test['pixel_resolution_x'] = 2.51
        #meta_test['pixel_resolution_y'] = 2.51
        #meta_test['pixel_resolution_z'] = 1
        #meta_test['pixel_resolution_unit_x'] = 'angstroms'
        meta_test['MRC/alpha'] = 90
        meta_test['MRC/amean'] = 26.4308
        assert_metadata_read( "MRC", "29kx_30epi_058_aligned.mrc", meta_test )

    def test_imgcnv_readmeta_mrc_map (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 25
        meta_test['image_num_x'] = 73
        meta_test['image_num_y'] = 43
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['pixel_resolution_x'] = 0.44825
        meta_test['pixel_resolution_y'] = 0.3925
        meta_test['pixel_resolution_z'] = 0.45875
        meta_test['pixel_resolution_unit_x'] = 'angstroms'
        meta_test['pixel_resolution_unit_y'] = 'angstroms'
        meta_test['pixel_resolution_unit_z'] = 'angstroms'
        meta_test['MRC/alpha'] = 90
        meta_test['MRC/amean'] = 0.000532967
        meta_test['MRC/label_00'] = '::::EMDATABANK.org::::EMD-3001::::'
        assert_metadata_read( "MRC", "EMD-3001.map", meta_test )

    def test_imgcnv_readmeta_mrc_map_2 (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 20
        meta_test['image_num_x'] = 20
        meta_test['image_num_y'] = 20
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['pixel_resolution_x'] = 11.4
        meta_test['pixel_resolution_y'] = 11.4
        meta_test['pixel_resolution_z'] = 11.4
        meta_test['pixel_resolution_unit_x'] = 'angstroms'
        meta_test['pixel_resolution_unit_y'] = 'angstroms'
        meta_test['pixel_resolution_unit_z'] = 'angstroms'
        meta_test['MRC/alpha'] = 90
        meta_test['MRC/amean'] = 0.783612
        meta_test['MRC/label_00'] = '::::EMDATABANK.org::::EMD-3197::::'
        assert_metadata_read( "MRC", "EMD-3197.map", meta_test )

    def test_imgcnv_readmeta_czi_fluo_3D (self):
        meta_test = {}
        meta_test['image_num_c'] = 2
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 97
        meta_test['image_num_x'] = 400
        meta_test['image_num_y'] = 400
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_num_fovs'] = 1
        meta_test['image_num_series'] = 1
        meta_test['image_mode'] = 'multichannel'
        meta_test['fov_height'] = 400
        meta_test['fov_width'] = 400
        meta_test['image_logical_x'] = 0
        meta_test['image_logical_y'] = 0
        meta_test['pixel_resolution_x'] = 0.16125
        meta_test['pixel_resolution_y'] = 0.16125
        meta_test['pixel_resolution_z'] = 0.2
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['pixel_resolution_unit_z'] = 'um'
        meta_test['document/acquisition_date'] = '2013-01-22T10:30:36.8295292+01:00'
        meta_test['document/application'] = 'ZEN 2012 (blue edition) v1.1.0.0'
        meta_test['document/username'] = 'zhmne'
        meta_test['document/description'] = 'Rat brain section, DAPI, GFAP-Alexa488'
        meta_test['objectives/objective:0/magnification'] = 40
        meta_test['objectives/objective:0/numerical_aperture'] = 1.4
        meta_test['objectives/objective:0/name'] = 'Plan-Apochromat 40x/1.4 Oil DIC (UV) VIS-IR M27'
        meta_test['channels/channel:1/acquisition_mode'] = 'StructuredIllumination'
        meta_test['channels/channel:1/color'] = '0.00,0.00,1.00'
        meta_test['channels/channel:1/contrast_method'] = 'Fluorescence'
        meta_test['channels/channel:1/modality'] = 'Fluorescence'
        meta_test['channels/channel:1/exposure'] = 22
        meta_test['channels/channel:1/exposure_units'] = 'ms'
        meta_test['channels/channel:1/name'] = 'DAPI'
        assert_metadata_read( "Zeiss CZI", "40x_RatBrain-AT-2ch-Z-wf.czi", meta_test )

    def test_imgcnv_readmeta_czi_fluo_3D_2 (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 41
        meta_test['image_num_x'] = 692
        meta_test['image_num_y'] = 520
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_num_fovs'] = 1
        meta_test['image_num_series'] = 1
        meta_test['image_mode'] = 'multichannel'
        meta_test['fov_height'] = 520
        meta_test['fov_width'] = 692
        meta_test['image_logical_x'] = 0
        meta_test['image_logical_y'] = 0
        meta_test['pixel_resolution_x'] = 0.204762
        meta_test['pixel_resolution_y'] = 0.204762
        meta_test['pixel_resolution_z'] = 0.24
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['pixel_resolution_unit_z'] = 'um'
        meta_test['document/acquisition_date'] = '2012-07-09T11:13:42.816077+02:00'
        meta_test['document/username'] = 'zhmne'
        meta_test['document/description'] = 'Bovine Pulmonary Artery Endothelial Cells; Mitotracker Red CMXRos (Mitochrondria), Alexa Fluore 488-Phalloidine (Actin filaments), DAPI (Nuclei)'
        meta_test['document/keywords'] = 'binning 2x2, Image: Markus Neumann, CZ Microscopy 2011'
        meta_test['objectives/objective:0/magnification'] = 63
        meta_test['objectives/objective:0/numerical_aperture'] = 1.4
        meta_test['objectives/objective:0/name'] = 'Plan-Apochromat 63x/1.40 Oil DIC M27'
        meta_test['channels/channel:1/acquisition_mode'] = 'WideField'
        meta_test['channels/channel:1/color'] = '0.00,1.00,0.00'
        meta_test['channels/channel:1/contrast_method'] = 'Fluorescence'
        meta_test['channels/channel:1/modality'] = 'Fluorescence'
        meta_test['channels/channel:1/exposure'] = 80
        meta_test['channels/channel:1/exposure_units'] = 'ms'
        meta_test['channels/channel:1/name'] = 'Actin (SYTOX Green)'
        assert_metadata_read( "Zeiss CZI", "BPAE-cells-bin2x2_3chTZ(WF).czi", meta_test )

    def test_imgcnv_readmeta_czi_fluo_pyramid (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 18
        meta_test['image_num_x'] = 995
        meta_test['image_num_y'] = 993
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_num_fovs'] = 4
        meta_test['image_num_series'] = 1
        meta_test['image_mode'] = 'multichannel'
        meta_test['fov_height'] = 512
        meta_test['fov_width'] = 512
        meta_test['image_logical_x'] = 0
        meta_test['image_logical_y'] = 0
        meta_test['pixel_resolution_x'] = 0.3225
        meta_test['pixel_resolution_y'] = 0.3225
        meta_test['pixel_resolution_z'] = 0.6
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['pixel_resolution_unit_z'] = 'um'
        meta_test['image_num_resolution_levels'] = 7
        meta_test['image_num_resolution_levels_actual'] = 3
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625]
        meta_test['image_resolution_level_scales_actual'] = [1.000000,0.333984,0.111328]
        meta_test['document/acquisition_date'] = '2012-07-09T11:50:20.28377+02:00'
        meta_test['document/username'] = 'zhmne'
        meta_test['document/keywords'] = 'Image: Markus Neumann, CZ Microscopy 2011, acquired with ZEN 2011 Betarelease (FIT)'
        #meta_test['channels/channel:1/acquisition_mode'] = 'WideField'
        meta_test['channels/channel:1/color'] = '1.00,0.34,0.00'
        #meta_test['channels/channel:1/contrast_method'] = 'Fluorescence'
        meta_test['channels/channel:1/emission_wavelength'] = 603
        meta_test['channels/channel:1/excitation_wavelength'] = 577
        meta_test['channels/channel:1/name'] = 'Actin sceleton (Alexa Fluor 568)'
        assert_metadata_read( "Zeiss CZI", "Mouse_stomach_20x_ROI_3chZTiles(WF).czi", meta_test )

    def test_imgcnv_readmeta_czi_zstack_16bit (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 20
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['pixel_resolution_x'] = 0.106822
        meta_test['pixel_resolution_y'] = 0.106822
        meta_test['pixel_resolution_z'] = 0.5
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['pixel_resolution_unit_z'] = 'um'
        meta_test['channels/channel:1/color'] = '1.00,0.82,0.00'
        meta_test['channels/channel:1/emission_wavelength'] = 580
        meta_test['channels/channel:1/excitation_wavelength'] = 550
        meta_test['channels/channel:1/name'] = 'Rhodamin'
        assert_metadata_read( "Zeiss CZI", "16Bit-ZStack.czi", meta_test )

    def test_imgcnv_readmeta_czi_zstack_8bit (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 104
        meta_test['image_num_x'] = 364
        meta_test['image_num_y'] = 365
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['pixel_resolution_x'] = 0.282
        meta_test['pixel_resolution_y'] = 0.282
        meta_test['pixel_resolution_z'] = 0.9
        meta_test['channels/channel:0/color'] = [0.25,1.00,0.00]
        meta_test['channels/channel:0/emission_wavelength'] = 525
        meta_test['channels/channel:0/excitation_wavelength'] = 490
        meta_test['channels/channel:0/name'] = 'FITC'
        assert_metadata_read( "Zeiss CZI", "8Bit-ZStack.czi", meta_test )

    def test_imgcnv_readmeta_czi_4D_annotations (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 3
        meta_test['image_num_z'] = 3
        meta_test['image_num_x'] = 200
        meta_test['image_num_y'] = 250
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGB'
        meta_test['channels/channel:1/color'] = '0.00,1.00,0.00'
        meta_test['channels/channel:1/name'] = 'Green CHANNEL_3'
        assert_metadata_read( "Zeiss CZI", "CZT-Stack-Anno.czi", meta_test )

    def test_imgcnv_readmeta_czi_zstack_8bit_2 (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 140
        meta_test['image_num_x'] = 348
        meta_test['image_num_y'] = 345
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        assert_metadata_read( "Zeiss CZI", "Z-Stack.czi", meta_test )

    def test_imgcnv_readmeta_czi_rgba (self):
        meta_test = {}
        meta_test['image_num_c'] = 4
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 10
        meta_test['image_num_x'] = 768
        meta_test['image_num_y'] = 576
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGBA'
        meta_test['channels/channel:0/color'] = [1.00,0.00,0.00]
        meta_test['channels/channel:1/name'] = 'Green 1'
        assert_metadata_read( "Zeiss CZI", "Z-Stack-Anno(RGB).czi", meta_test )

    def test_imgcnv_readmeta_hdf5_image (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 227
        meta_test['image_num_y'] = 149
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGB'
        meta_test['image_series_paths/00000'] = '/image24bitpixel'
        meta_test['image_series_paths/00001'] = '/image8bit'
        assert_metadata_read( "HDF5 image2", "ex_image2.h5", meta_test )

    def test_imgcnv_readmeta_hdf5_image_2 (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 841
        meta_test['image_num_y'] = 721
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'indexed'
        meta_test['image_series_paths/00000'] = '/All data'
        meta_test['image_series_paths/00001'] = '/Land data'
        meta_test['image_series_paths/00002'] = '/Sea data'
        assert_metadata_read( "HDF5 image2", "ex_image3.h5", meta_test )

    def test_imgcnv_readmeta_hdf5_neon (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 426
        meta_test['image_num_x'] = 477
        meta_test['image_num_y'] = 502
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'signed integer'
        meta_test['image_mode'] = 'grayscale'
        meta_test['image_series_paths/00000'] = '/Reflectance'
        meta_test['HDF5/Reflectance/Description'] = 'Atmospherically corrected reflectance.'
        assert_metadata_read( "HDF5 volume", "NEONDSImagingSpectrometerData.h5", meta_test )

    def test_imgcnv_readmeta_hdf5_dream3D (self):
        meta_test = {}
        meta_test['image_num_c'] = 2
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 80
        meta_test['image_num_x'] = 284
        meta_test['image_num_y'] = 334
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['image_mode'] = 'multichannel'
        meta_test['image_series_paths/00000'] = '/DataContainers/ImageDataContainer/CellData/AspectRatios_cell'
        meta_test['image_series_paths/00007'] = '/DataContainers/ImageDataContainer/CellData/IPFColor'
        meta_test['HDF5/DataContainers/ImageDataContainer/CellData/AspectRatios_cell/TupleDimensions'] = '284,334,80'
        assert_metadata_read( "Dream3D volume", "murks_lens.h5", meta_test )

    def test_imgcnv_readmeta_hdf5_dream3D_path_3c (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 80
        meta_test['image_num_x'] = 284
        meta_test['image_num_y'] = 334
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['image_mode'] = 'multichannel'
        meta_test['pixel_resolution_x'] = 3
        meta_test['pixel_resolution_y'] = 3
        meta_test['pixel_resolution_z'] = 3
        assert_metadata_read( "Dream3D path 3c", "murks_lens.h5", meta_test, extra=['-path', '/DataContainers/ImageDataContainer/CellData/EulerAngles'])

    def test_imgcnv_readmeta_hdf5_dream3D_path_1c (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 80
        meta_test['image_num_x'] = 284
        meta_test['image_num_y'] = 334
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['image_mode'] = 'grayscale'
        meta_test['pixel_resolution_x'] = 3
        meta_test['pixel_resolution_y'] = 3
        meta_test['pixel_resolution_z'] = 3
        assert_metadata_read( "Dream3D path 1c", "murks_lens.h5", meta_test, extra=['-path', '/DataContainers/ImageDataContainer/CellData/Image Quality'])

    def test_imgcnv_readmeta_imaris (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 30
        meta_test['image_num_z'] = 6
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'grayscale'
        meta_test['format'] = 'IMARIS5'
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_x'] = 0.817973
        meta_test['pixel_resolution_y'] = 0.817973
        meta_test['pixel_resolution_z'] = 5.38483
        meta_test['tile_num_original_x'] = 256
        meta_test['tile_num_original_y'] = 256
        meta_test['tile_num_original_z'] = 16
        meta_test['channels/channel:0/name'] = '(name not specified)'
        meta_test['channels/channel:0/pinhole_radius'] = 500.000
        meta_test['channels/channel:0/emission_wavelength'] = 458.000
        assert_metadata_read( "Imaris", "R18Demo.ims", meta_test )

    def test_imgcnv_readmeta_imaris_2 (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 47
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 1024
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'grayscale'
        meta_test['format'] = 'IMARIS5'
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000]
        meta_test['image_resolution_level_structure'] = 'hierarchical'
        #meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_x'] = 0.13
        meta_test['pixel_resolution_y'] = 0.13
        meta_test['pixel_resolution_z'] = 0.0978723
        meta_test['tile_num_original_x'] = 128
        meta_test['tile_num_original_y'] = 128
        meta_test['tile_num_original_z'] = 8
        meta_test['document/acquisition_date'] = '2016-07-26T17:11:07'
        meta_test['objectives/objective:0/numerical_aperture'] = 1.4
        meta_test['channels/channel:0/name'] = 'GFP iXon Confocal'
        meta_test['channels/channel:0/pinhole_radius'] = 40
        meta_test['channels/channel:0/emission_wavelength'] = '488 nm nm'
        meta_test['channels/channel:0/color'] = [0.22,1.00,0.00]
        assert_metadata_read( "Imaris", "2016-07-26_17.11.03_Sample Prefix_5P298C2_Protocol.ims", meta_test )

    def test_imgcnv_readmeta_vqi_cyx_objects (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_pixel_depth'] = 32 # support feature reading
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'mask'
        meta_test['format'] = 'ViQi'
        meta_test['image_num_fovs'] = 1
        meta_test['image_num_items'] = 108
        meta_test['image_num_measures'] = 17
        meta_test['image_num_resolution_levels'] = 5
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500]
        meta_test['channels/channel:0/color'] = [1.00,1.00,1.00]
        meta_test['image_series_paths/00000'] = '/cells'
        assert_metadata_read( "VQI CYX", "vqi_objects_CYX.h5", meta_test )

    def test_imgcnv_readmeta_nikon_nd2_zstack (self):
        meta_test = {}
        meta_test['image_num_c'] = 5
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 21
        meta_test['image_num_x'] = 1019
        meta_test['image_num_y'] = 1019
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['format'] = 'ND2'
        meta_test['pixel_resolution_x'] = 1.01531
        meta_test['pixel_resolution_y'] = 1.01531
        meta_test['pixel_resolution_z'] = 5
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        meta_test['document/acquisition_date'] = '2017-06-06T11:15:06'
        meta_test['document/version'] = '3.0'
        meta_test['objectives/objective:0/magnification'] = 20
        meta_test['objectives/objective:0/numerical_aperture'] = 0.75
        meta_test['objectives/objective:0/refractive_index'] = 1
        meta_test['channels/channel:0/name'] = 'CSU far red RNA'
        meta_test['channels/channel:0/exposure'] = 100
        meta_test['channels/channel:0/exposure_units'] = 'ms'
        meta_test['channels/channel:0/color'] = [1.00,1.00,1.00]
        meta_test['channels/channel:0/fluor'] = 'CSU far red RNA'
        meta_test['channels/channel:0/modality'] = 'Widefield Fluorescence'
        meta_test['channels/channel:4/name'] = 'CSU BF'
        meta_test['channels/channel:4/color'] = [1.00,1.00,1.00]
        meta_test['channels/channel:4/modality'] = 'Brightfield, Spinning Disk Confocal'
        assert_metadata_read( "Nikon ND2", "sample_image.nd2", meta_test )

    def test_imgcnv_readmeta_nikon_nd2_time (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 42
        meta_test['image_num_z'] = 7
        meta_test['image_num_x'] = 624
        meta_test['image_num_y'] = 464
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['format'] = 'ND2'
        meta_test['pixel_resolution_t'] = 300
        meta_test['pixel_resolution_z'] = 3
        meta_test['pixel_resolution_unit_t'] = 'seconds'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        meta_test['document/acquisition_date'] = '2009-08-16T12:23:18' #'16/08/2009  12:23:18'
        meta_test['document/version'] = '2.1'
        meta_test['objectives/objective:0/magnification'] = 20
        meta_test['objectives/objective:0/numerical_aperture'] = .45
        meta_test['objectives/objective:0/refractive_index'] = 1
        meta_test['channels/channel:0/name'] = 'CFP/dapi'
        meta_test['channels/channel:0/fluor'] = 'CFP/dapi'
        meta_test['channels/channel:0/emission_wavelength'] = '435-485'
        meta_test['channels/channel:0/excitation_wavelength'] = '385-415'
        meta_test['channels/channel:0/color'] = [0.00,0.00,0.67]
        assert_metadata_read( "Nikon ND2", "old_cells_02.nd2", meta_test )

    def test_imgcnv_readmeta_metamorph_tiff (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 361
        meta_test['image_num_y'] = 267
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 0.343
        meta_test['pixel_resolution_y'] = 0.342282
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['document/acquisition_date'] = '2015-09-03T15:16:24'
        meta_test['document/vendor'] = 'Molecular Devices'
        meta_test['document/application'] = 'MetaMorph'
        meta_test['document/barcode'] = '240-65'
        meta_test['objectives/objective:0/name'] = '20X Plan Apo'
        meta_test['objectives/objective:0/magnification'] = 20
        meta_test['objectives/objective:0/numerical_aperture'] = 0.75
        meta_test['objectives/objective:0/refractive_index'] = 1
        meta_test['channels/channel:0/name'] = 'DAPI'
        meta_test['channels/channel:0/fluor'] = 'DAPI'
        meta_test['channels/channel:0/emission_wavelength'] = 447
        meta_test['channels/channel:0/exposure'] = 300
        meta_test['channels/channel:0/exposure_units'] = 'ms'
        meta_test['channels/channel:0/pinhole_radius'] = 60
        meta_test['channels/channel:0/pinhole_radius_units'] = 'um'
        assert_metadata_read( "MD HTD old format", "MD_HTD_OLD_w1.TIF", meta_test )

    def test_imgcnv_readmeta_metamorph_tiff_xml (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 2048
        meta_test['image_num_y'] = 2048
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 0.343
        meta_test['pixel_resolution_y'] = 0.342282
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['document/acquisition_date'] = '2015-09-03T15:16:22'
        meta_test['document/vendor'] = 'Molecular Devices'
        meta_test['document/application'] = 'MetaMorph'
        meta_test['document/barcode'] = '240-65'
        meta_test['document/well_label'] = 'D07'
        meta_test['objectives/objective:0/name'] = '20X Plan Apo'
        meta_test['objectives/objective:0/magnification'] = 20
        meta_test['objectives/objective:0/numerical_aperture'] = 0.75
        meta_test['objectives/objective:0/refractive_index'] = 1
        meta_test['channels/channel:0/name'] = 'DAPI'
        meta_test['channels/channel:0/fluor'] = 'DAPI'
        meta_test['channels/channel:0/emission_wavelength'] = 447
        meta_test['channels/channel:0/exposure'] = 300
        meta_test['channels/channel:0/exposure_units'] = 'ms'
        meta_test['channels/channel:0/pinhole_radius'] = 60
        meta_test['channels/channel:0/pinhole_radius_units'] = 'um'
        meta_test['coordinates/positions/stage'] = [68636.000000,37706.000000,9173.120000]
        assert_metadata_read( "MD HTD XML format", "MD_HTD_XML_w1.TIF", meta_test )

    def test_imgcnv_readmeta_cellomics_dib (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 0
        meta_test['pixel_resolution_y'] = 0
        #meta_test['pixel_resolution_unit_x'] = 'um'
        #meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['document/well_label'] = 'P24'
        meta_test['document/well_ij'] = '16,24'
        assert_metadata_read( "Cellomics DIB", "AS_09125_050117050001_P24f00d0.DIB", meta_test )

    def test_imgcnv_readmeta_cellomics_c01 (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 2208
        meta_test['image_num_y'] = 2208
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 0.206364
        meta_test['pixel_resolution_y'] = 0.206364
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['document/well_label'] = 'B06'
        meta_test['document/well_ij'] = '2,6'
        assert_metadata_read( "Cellomics C01", "localhost210727110002_B06f01d0.C01", meta_test )

    def test_imgcnv_readmeta_bimr (self):
        meta_test = {}
        meta_test['image_num_c'] = 2
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 1056
        meta_test['image_num_y'] = 1056
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        assert_metadata_read( "BIMR", 'combinedsubtractions.bimr', meta_test )

    def test_imgcnv_readmeta_biotek_tiff (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 1224
        meta_test['image_num_y'] = 904
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'grayscale'
        meta_test['pixel_resolution_x'] = 0.321895
        meta_test['pixel_resolution_y'] = 0.321903
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['document/well_label'] = 'A3'
        meta_test['document/well_ij'] = '1,3'
        meta_test['document/well_col'] = 3
        meta_test['document/well_row'] = 1
        meta_test['document/well_site'] = 1
        meta_test['document/acquisition_date'] = '2021-09-28T10:20:44'
        meta_test['document/assay_name'] = 'Phase Contrast'
        meta_test['document/description'] = 'Costar 96 white opaque'
        meta_test['document/instrument_name'] = 'Cytation5'
        meta_test['document/plate_name'] = 'Plate 1'
        meta_test['document/vendor'] = 'BioTek'
        meta_test['objectives/objective:0/name'] = 'Olympus 20x'
        meta_test['objectives/objective:0/magnification'] = 20
        meta_test['objectives/objective:0/numerical_aperture'] = 0.45
        meta_test['objectives/objective:0/psf_sigma'] = 0.806
        meta_test['channels/channel:0/name'] = 'Phase Contrast'
        meta_test['channels/channel:0/exposure'] = 100
        meta_test['channels/channel:0/exposure_units'] = 'ms'
        meta_test['channels/channel:0/modality'] = 'Brightfield PhaseContrast'
        meta_test['coordinates/positions/stage'] = [-1103.000000,105.000000,3243.000000]
        meta_test['coordinates/positions/focus'] = 3243.0
        assert_metadata_read( "BioTek TIFF", 'A3_01_1_2_Phase_Contrast_001.tif', meta_test )

    def test_imgcnv_readmeta_akoya_qptiff_2c (self):
        meta_test = {}
        meta_test['format'] = 'QPTIFF'
        meta_test['image_num_c'] = 2
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 1864
        meta_test['image_num_y'] = 1400
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['image_mode'] = 'multichannel'
        meta_test['image_num_resolution_levels'] = 1
        meta_test['image_resolution_level_scales'] = 1.000000
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/thumbnail'
        meta_test['pixel_resolution_x'] = 0.498893
        meta_test['pixel_resolution_y'] = 0.498893
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['document/slide_id'] = 'HnE_3'
        meta_test['document/instrument_name'] = 'Jenga'
        meta_test['document/application'] = 'Jenga'
        meta_test['document/vendor'] = 'Akoya'
        meta_test['objectives/objective:0/name'] = '20x [20x]'
        meta_test['objectives/objective:0/magnification'] = 20
        meta_test['channels/channel:0/name'] = 'Eosin'
        meta_test['channels/channel:0/color'] = [1.00,0.24,0.62]
        meta_test['channels/channel:0/color_original'] = [255,60,157]
        meta_test['channels/channel:0/modality'] = 'Fluorescence'
        meta_test['channels/channel:1/name'] = 'Hematoxylin'
        meta_test['channels/channel:1/color'] = [0.20,0.27,0.62]
        meta_test['channels/channel:1/color_original'] = [50,69,157]
        meta_test['channels/channel:1/modality'] = 'Fluorescence'
        assert_metadata_read( "Akoya QPTIFF", 'HnE_3_1x1component_data.tif', meta_test )

    def test_imgcnv_readmeta_akoya_qptiff_8c (self):
        meta_test = {}
        meta_test['format'] = 'QPTIFF'
        meta_test['image_num_c'] = 8
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 1868
        meta_test['image_num_y'] = 1400
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['image_mode'] = 'multichannel'
        meta_test['image_num_resolution_levels'] = 1
        meta_test['image_resolution_level_scales'] = 1.000000
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/thumbnail'
        meta_test['pixel_resolution_x'] = 0.497995
        meta_test['pixel_resolution_y'] = 0.497995
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['document/slide_id'] = 'LuCa-7color_[13860,52919]'
        meta_test['document/instrument_name'] = 'Jenga'
        meta_test['document/application'] = 'Jenga'
        meta_test['document/vendor'] = 'Akoya'
        meta_test['objectives/objective:0/name'] = '10x [10x]'
        meta_test['objectives/objective:0/magnification'] = 10

        meta_test['channels/channel:0/name'] = 'PDL1 (Opal 520)'
        meta_test['channels/channel:0/color'] = [1.00,0.00,0.00]
        meta_test['channels/channel:0/color_original'] = [255,0,0]
        meta_test['channels/channel:0/modality'] = 'Fluorescence'

        meta_test['channels/channel:1/name'] = 'CD8 (Opal 540)'
        meta_test['channels/channel:1/color'] = [1.00,1.00,0.00]
        meta_test['channels/channel:1/color_original'] = [255,255,0]
        meta_test['channels/channel:1/modality'] = 'Fluorescence'

        meta_test['channels/channel:2/name'] = 'FoxP3 (Opal 570)'
        meta_test['channels/channel:2/color'] = [1.00,0.50,0.00]
        meta_test['channels/channel:2/color_original'] = [255,128,0]
        meta_test['channels/channel:2/modality'] = 'Fluorescence'

        meta_test['channels/channel:3/name'] = 'CD68 (Opal 620)'
        meta_test['channels/channel:3/color'] = [1.00,0.00,1.00]
        meta_test['channels/channel:3/color_original'] = [255,0,255]
        meta_test['channels/channel:3/modality'] = 'Fluorescence'

        meta_test['channels/channel:4/name'] = 'PD1 (Opal 650)'
        meta_test['channels/channel:4/color'] = [0.00,1.00,0.00]
        meta_test['channels/channel:4/color_original'] = [0,255,0]
        meta_test['channels/channel:4/modality'] = 'Fluorescence'

        meta_test['channels/channel:5/name'] = 'CK (Opal 690)'
        meta_test['channels/channel:5/color'] = [0.00,1.00,1.00]
        meta_test['channels/channel:5/color_original'] = [0,255,255]
        meta_test['channels/channel:5/modality'] = 'Fluorescence'

        meta_test['channels/channel:6/name'] = 'DAPI'
        meta_test['channels/channel:6/color'] = [0.00,0.00,1.00]
        meta_test['channels/channel:6/color_original'] = [0,0,255]
        meta_test['channels/channel:6/modality'] = 'Fluorescence'

        meta_test['channels/channel:7/name'] = 'Autofluorescence'
        meta_test['channels/channel:7/color'] = [0.00,0.00,0.00]
        meta_test['channels/channel:7/color_original'] = [0,0,0]
        meta_test['channels/channel:7/modality'] = 'Fluorescence'

        assert_metadata_read( "Akoya QPTIFF", 'LuCa-7color_[13860,52919]_1x1component_data.tif', meta_test )

    def test_imgcnv_readmeta_perkinelmer_hcs_tiff (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 1080
        meta_test['image_num_y'] = 1080
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'grayscale'
        meta_test['pixel_resolution_x'] = 6.5
        meta_test['pixel_resolution_y'] = 6.5
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['document/well_label'] = 'B3'
        meta_test['document/well_ij'] = '2,3'
        meta_test['document/well_col'] = 3
        meta_test['document/well_row'] = 2
        meta_test['document/acquisition_date'] = '2023-04-14T00:00:00'
        meta_test['document/instrument_name'] = 'Phenix'
        meta_test['document/vendor'] = 'PerkinElmer'
        meta_test['objectives/objective:0/name'] = 'WPlan_Apochromat_20x/1.0_70mm'
        meta_test['objectives/objective:0/magnification'] = 20
        meta_test['objectives/objective:0/numerical_aperture'] = 1
        meta_test['channels/channel:0/name'] = 'TMRE'
        meta_test['channels/channel:0/exposure'] = 0.04
        meta_test['channels/channel:0/exposure_units'] = 's'
        meta_test['channels/channel:0/modality'] = 'Epifluorescence'
        meta_test['channels/channel:0/binning'] = 2
        meta_test['channels/channel:0/excitation_wavelength'] = 561
        meta_test['channels/channel:0/color'] = [1.00,1.00,0.00]
        meta_test['coordinates/positions/stage'] = [-322.906999,-322.906999,-5.000000]
        meta_test['coordinates/positions/focus'] = -5
        meta_test['coordinates/units'] = 'microns'
        assert_metadata_read( "PerkinElmer HCS TIFF", 'r02c03f01p01-ch3sk1fk1fl1.tiff', meta_test )

    def test_imgcnv_readmeta_keyence_hcs_tiff (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_p'] = 1
        meta_test['image_num_x'] = 1920
        meta_test['image_num_y'] = 1440
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'grayscale'
        meta_test['pixel_resolution_x'] = 0.377442
        meta_test['pixel_resolution_y'] = 0.377442
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        # meta_test['document/well_label'] = 'B3'
        # meta_test['document/well_ij'] = '2,3'
        # meta_test['document/well_col'] = 3
        # meta_test['document/well_row'] = 2
        # meta_test['document/acquisition_date'] = '2023-04-14T00:00:00'
        meta_test['document/vendor'] = 'Keyence'
        meta_test['objectives/objective:0/name'] = 'PlanFluor 20x 0.45/8.80-7.50mm Ph1 :Default'
        meta_test['objectives/objective:0/magnification'] = 20
        meta_test['objectives/objective:0/numerical_aperture'] = 0.45
        meta_test['channels/channel:0/name'] = 'Channel4'
        meta_test['channels/channel:0/exposure'] = 0.025
        meta_test['channels/channel:0/exposure_units'] = 's'
        meta_test['channels/channel:0/modality'] = 'BrightField'
        meta_test['channels/channel:0/binning'] = 'Off'
        meta_test['channels/channel:0/color'] = [1.00,1.00,1.00]
        meta_test['coordinates/positions/stage'] = [9985.438000,9239.078000,4375.230000]
        meta_test['coordinates/positions/focus'] = 0.020388
        meta_test['coordinates/units'] = 'microns'
        assert_metadata_read( "Keyence HCS TIFF", 'Image_W096_P00025_CH4.tif', meta_test )

    def test_imgcnv_readmeta_thermo_fisher_evos_tiff (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_p'] = 1
        meta_test['image_num_x'] = 2048
        meta_test['image_num_y'] = 1536
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_pixel_significant_bits'] = 12
        meta_test['image_mode'] = 'grayscale'
        meta_test['pixel_resolution_x'] = 0.313181
        meta_test['pixel_resolution_y'] = 0.313181
        meta_test['pixel_resolution_z'] = 2.59259
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        meta_test['document/plate_name'] = '96 Well NUNCLON 167425'
        meta_test['document/plate_description'] = 'Custom Plate'
        meta_test['document/plate_id'] = '632f7f6c-4264-43a3-986d-3fe6d4d0331f'
        meta_test['document/well_label'] = 'H12'
        meta_test['document/well_col'] = 12
        meta_test['document/well_row'] = 8
        meta_test['document/well_site'] = 15
        meta_test['document/acquisition_date'] = '2024-11-05T19:06:33.8152893Z'
        meta_test['channels/channel:0/exposure'] = 0.016
        meta_test['channels/channel:0/exposure_units'] = 's'
        meta_test['channels/channel:0/color'] = [1.00,1.00,1.00]
        # meta_test['coordinates/positions/stage'] = [641.278015,962.19635,-97.03]
        # meta_test['coordinates/positions/focus'] = -97.03
        # meta_test['coordinates/units'] = 'microns'
        meta_test['OME/Image/Name'] = 'Trans'
        assert_metadata_read( "ThermoFisher EVOS TIFF", 'scan_Plate_R_p00_0_H12f15d4.TIF', meta_test )

    def test_imgcnv_readmeta_biotek_cytation_c10 (self):
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 1960
        meta_test['image_num_y'] = 1960
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'grayscale'
        meta_test['pixel_resolution_x'] = 0.325
        meta_test['pixel_resolution_y'] = 0.325
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['document/well_label'] = 'A1'
        meta_test['document/well_ij'] = '1,1'
        meta_test['document/well_col'] = 1
        meta_test['document/well_row'] = 1
        meta_test['document/well_site'] = 1
        meta_test['document/acquisition_date'] = '2025-02-06T14:39:37'
        meta_test['document/assay_name'] = 'Bright Field'
        meta_test['document/description'] = 'Corning 96 well flat bottom-3603'
        meta_test['document/instrument_name'] = 'CytationC10'
        meta_test['document/plate_name'] = 'Plate 1'
        meta_test['document/vendor'] = 'BioTek'
        meta_test['objectives/objective:0/name'] = 'Olympus 20x'
        meta_test['objectives/objective:0/magnification'] = 20
        meta_test['objectives/objective:0/numerical_aperture'] = 0.45
        meta_test['objectives/objective:0/psf_sigma'] = 0.806
        meta_test['channels/channel:0/name'] = 'Bright Field'
        meta_test['channels/channel:0/exposure'] = 6
        meta_test['channels/channel:0/exposure_units'] = 'ms'
        meta_test['channels/channel:0/modality'] = 'Brightfield'
        meta_test['coordinates/positions/stage'] = [0.0,0.0,3304.4]
        meta_test['coordinates/positions/focus'] = 3304.4
        assert_metadata_read( "BioTek CytationC10", 'A1_02_1_13_Bright Field_001.tif', meta_test )
