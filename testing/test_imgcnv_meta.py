#!/usr/bin/python
__author__    = "Dmitry Fedorov"
__copyright__ = "ViQi Inc"

from copy import deepcopy

from bim_test_base import setup_tests, assert_image_metadata

class TestBioImageConvertReadConvertMeta():

    @classmethod
    def setup_class(cls):
        setup_tests()

    @classmethod
    def teardown_class(cls):
        pass

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    def test_imgcnv_meta_biorad_pic_zstack (self):
        meta_test = {}
        meta_test['image_num_z'] = 6
        meta_test['image_num_t'] = 1
        meta_test['pixel_resolution_x'] = 0.192406
        meta_test['pixel_resolution_y'] = 0.192406
        meta_test['pixel_resolution_z'] = 1.185
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        assert_image_metadata( "PIC", "MZ2.PIC", meta_test )

    def test_imgcnv_meta_flouview_tiff (self):
        meta_test = {}
        meta_test['image_num_z'] = 13
        meta_test['image_num_t'] = 1
        meta_test['pixel_resolution_x'] = 0.20716
        meta_test['pixel_resolution_y'] = 0.20716
        meta_test['pixel_resolution_z'] = 1
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        meta_test['channels/channel:0/name'] = 'FITC'
        meta_test['channels/channel:1/name'] = 'Cy3'

        # the following fields will not be tested for conversion into OME-TIFF
        meta_test_full = deepcopy(meta_test)
        meta_test_full['objectives/objective:0/name'] = 'UPLAPO 40XO'
        meta_test_full['objectives/objective:0/magnification'] = 40
        assert_image_metadata( "TIFF Fluoview", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", meta_test_full, meta_test )

    def test_imgcnv_meta_zeiss_lsm (self):
        meta_test = {}
        meta_test['image_num_z'] = 30
        meta_test['image_num_t'] = 1
        meta_test['pixel_resolution_x'] = 0.138661
        meta_test['pixel_resolution_y'] = 0.138661
        meta_test['pixel_resolution_z'] = 1
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'

        # the following fields will not be tested for conversion into OME-TIFF
        meta_test_full = deepcopy(meta_test)
        meta_test_full['objectives/objective:0/name'] = 'Achroplan 63x/0.95 W'
        meta_test_full['objectives/objective:0/magnification'] = 63.0
        meta_test_full['objectives/objective:0/numerical_aperture'] = 0.95
        meta_test_full['channels/channel:0/color'] = [0.00,1.00,0.00]
        meta_test_full['channels/channel:0/excitation_wavelength'] = 514.0
        meta_test_full['channels/channel:0/pinhole_radius'] = 185.0
        meta_test_full['channels/channel:0/name'] = 'Ch3-T3'
        meta_test_full['channels/channel:0/power'] = 38.8
        meta_test_full['channels/channel:1/color'] = [1.00,0.00,0.00]
        meta_test_full['channels/channel:1/excitation_wavelength'] = 458.0
        meta_test_full['channels/channel:1/pinhole_radius'] = 255.5
        meta_test_full['channels/channel:1/name'] = 'Ch2-T4'
        meta_test_full['channels/channel:1/power'] = 63.5
        assert_image_metadata( "TIFF Zeiss LSM", "combinedsubtractions.lsm", meta_test_full, meta_test )

    def test_imgcnv_meta_ome_tiff (self):
        meta_test = {}
        meta_test['image_num_z'] = 152
        meta_test['image_num_t'] = 1
        meta_test['pixel_resolution_x'] = 0.124
        meta_test['pixel_resolution_y'] = 0.124
        meta_test['pixel_resolution_z'] = 0.35
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'

        # the following fields will not be tested for conversion into OME-TIFF
        meta_test_full = deepcopy(meta_test)
        meta_test_full['channels/channel:0/name'] = 'Alexa Fluor 488 - 488nm'
        meta_test_full['channels/channel:0/fluor'] = 'Alexa Fluor 488'
        meta_test_full['channels/channel:0/modality'] = 'Epifluorescence'
        meta_test_full['channels/channel:0/emission_wavelength'] = 520
        meta_test_full['channels/channel:0/excitation_wavelength'] = 488
        meta_test_full['channels/channel:1/name'] = 'Alexa Fluor 546 - 543nm'
        meta_test_full['channels/channel:1/fluor'] = 'Alexa Fluor 546'
        meta_test_full['channels/channel:1/modality'] = 'Epifluorescence'
        meta_test_full['channels/channel:1/emission_wavelength'] = 572
        meta_test_full['channels/channel:1/excitation_wavelength'] = 543
        meta_test_full['channels/channel:2/name'] = 'DRAQ5 - 633nm'
        meta_test_full['channels/channel:2/fluor'] = 'DRAQ5'
        meta_test_full['channels/channel:2/modality'] = 'Epifluorescence'
        meta_test_full['channels/channel:2/emission_wavelength'] = 683
        meta_test_full['channels/channel:2/excitation_wavelength'] = 633
        meta_test_full['objectives/objective:0/name'] = 'UPLFLN    40X O  NA:1.30'
        meta_test_full['objectives/objective:0/magnification'] = 40
        meta_test_full['objectives/objective:0/numerical_aperture'] = 1.3
        assert_image_metadata( "OME-TIFF", "wta.ome.tif", meta_test_full, meta_test )

    def test_imgcnv_meta_stk_time (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 31
        meta_test['pixel_resolution_t'] = 4
        assert_image_metadata( "STK", "K560-tax-6-7-7-1.stk", meta_test )

    def test_imgcnv_meta_stk_zstack (self):
        meta_test = {}
        meta_test['image_num_z'] = 105
        meta_test['image_num_t'] = 1
        meta_test['pixel_resolution_x'] = 0.43
        meta_test['pixel_resolution_y'] = 0.43
        meta_test['pixel_resolution_z'] = 0.488
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['pixel_resolution_unit_z'] = 'microns'

        # the following fields will not be tested for conversion into OME-TIFF
        meta_test_full = deepcopy(meta_test)
        #meta_test_full['stage_distance_z'] = 0.488 # converted these to full arrays, need to test them
        #meta_test['coordinates/tie_points/fovs/00000'] = [3712.2,-2970.34,25.252]
        assert_image_metadata( "STK", "sxn3_w1RGB-488nm_s1.stk", meta_test_full, meta_test )

    def test_imgcnv_meta_olympus_oib (self):
        meta_test = {}
        meta_test['image_num_z'] = 16
        meta_test['image_num_t'] = 1
        meta_test['pixel_resolution_x'] = 2.48341
        meta_test['pixel_resolution_y'] = 2.48019
        meta_test['pixel_resolution_z'] = 0.9375
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        meta_test['channels/channel:0/name'] = 'Alexa Fluor 405'
        meta_test['channels/channel:1/name'] = 'Cy3'
        assert_image_metadata( "OIB", "MB_10X_20100303.oib", meta_test )

    def test_imgcnv_meta_psia_tiff (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['pixel_resolution_x'] = 0.177539
        meta_test['pixel_resolution_y'] = 0.177539
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        assert_image_metadata( "PSIA", "040130Topography001.tif", meta_test )

    def test_imgcnv_meta_nanoscope (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 2
        meta_test['pixel_resolution_x'] = 0.0585938
        meta_test['pixel_resolution_y'] = 0.0585938
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        assert_image_metadata( "NANOSCOPE", "AXONEME.002", meta_test )

    def test_imgcnv_meta_ibw (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        #meta_test['image_num_p'] = 3
        assert_image_metadata( "IBW", "tubule20000.ibw", meta_test )

    def test_imgcnv_meta_ome_big_tiff (self):
        meta_test = {}
        meta_test['image_num_z'] = 13
        meta_test['image_num_t'] = 1
        meta_test['pixel_resolution_x'] = 0.20716
        meta_test['pixel_resolution_y'] = 0.20716
        meta_test['pixel_resolution_z'] = 1
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['pixel_resolution_unit_z'] = 'microns'
        meta_test['channels/channel:0/name'] = 'FITC'
        meta_test['channels/channel:1/name'] = 'Cy3'
        assert_image_metadata( "OME-BigTIFF", "bigtiff.ome.btf", meta_test )

    def test_imgcnv_meta_tiff_float (self):
        meta_test = {}
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 1
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        assert_image_metadata( "TIFF float", "autocorrelation.tif", meta_test )
