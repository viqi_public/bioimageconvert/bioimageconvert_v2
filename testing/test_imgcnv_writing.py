#!/usr/bin/python
__author__    = "Dmitry Fedorov"
__copyright__ = "ViQi Inc"

from bim_test_base import setup_tests, assert_image_write

mode = []

class TestBioImageConvertWriting():

    @classmethod
    def setup_class(cls):
        setup_tests()

    @classmethod
    def teardown_class(cls):
        pass

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    def test_imgcnv_write_jpeg (self):
        assert_image_write( "JPEG", "flowers_24bit_nointr.png" )

    def test_imgcnv_write_png (self):
        assert_image_write( "PNG", "flowers_24bit_nointr.png" )

    def test_imgcnv_write_bmp (self):
        assert_image_write( "BMP", "flowers_24bit_nointr.png" )

    def test_imgcnv_write_tiff (self):
        assert_image_write( "TIFF", "flowers_24bit_nointr.png" )

    def test_imgcnv_write_ome_tiff (self):
        assert_image_write( "OME-TIFF", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )

    def test_imgcnv_write_big_tiff (self):
        assert_image_write( "BigTIFF", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )

    def test_imgcnv_write_ome_big_tiff (self):
        assert_image_write( "OME-BigTIFF", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )

    def test_imgcnv_write_jxr (self):
        assert_image_write( "JXR", "flowers_24bit_nointr.png" )

    def test_imgcnv_write_webp (self):
        assert_image_write( "WEBP", "flowers_24bit_nointr.png" )

    def test_imgcnv_write_jp2 (self):
        assert_image_write( "JP2", "flowers_24bit_nointr.png" )
